# Siriuso #

Движок умной социальной сети Техноком - первого блока Всемирной автоматизированной системы Техноком

Чтобы присоединиться к работе над движком напишите, пожалуйста, на Технокоме Владимиру Левадному https://tehnokom.su/im/direct/13

## Описание структуры ##

Описание структуры движка находится в Вики соотвествующего мезопроекта https://pm.tehnokom.su/project/p13-2/wiki/dvizhok-siriuso-na-django

## Использование Docker ##

В корне проекта находится файл docker-compose.yaml который отвечает за запуск проекта.

Настройки базы в settings.py
```
'HOST': 'postgres',
'PORT': '5432',
'NAME': 'siriuso',
'USER': 'siriuso',
'PASSWORD': 'oGLOWo8nd3',
```
После запуска будет развернуто два контейнера:
1. Postgresql с доступностом по localhost:5432
2. Django-webserver с доступом по localhost:8000

### Linux ###

На Linux потребуется установить docker и docker-compose

Для запуска нужно перейти в корень проекта и выполнить
```
sudo docker-compose up --build
```
После чего нужно подождать пока скачаются образы с docker hub и соберутся контейнеры

Для управления можно использовать те же комманды что и обычно с доступом в контейнер
```
sudo docker exec -it siriuso_django_1 python3 manage.py
```
**Например:**
```
sudo docker exec -it siriuso_django_1 python3 manage.py makemigrations  # Подготовка миграций
sudo docker exec -it siriuso_django_1 python3 manage.py migrate         # Применение миграцй
sudo docker exec -it siriuso_django_1 python3 manage.py createsuperuser # Создание супер пользователя
sudo docker exec -it siriuso_django_1 python3 manage.py loaddata        # Загрузка начальных данных
```

### Windows ###

У кого Windows напишите инструкцию

### PyCharm ###
