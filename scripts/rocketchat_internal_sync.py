from main.models import Uzanto
from django.db.models import Q
from rocketchat_auth.core import RocketClient
from rocketchat_auth.models import RocketUser


def run():
    rocket = RocketClient()

    if rocket.connectAsAdmin():
        users = RocketUser.objects.all()

        total = users.count()
        current = 0

        for user in users:
            current += 1
            print('{} of {}'.format(current, total), user.uzanto)
            response = rocket.users_list(query={'_id': user.rocket_user})

            if not response['count']:
                user.delete()
                print('Delete user record')

        rocket.disconnect()
