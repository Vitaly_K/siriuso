from main.models import Uzanto
from django.db.models import Q
from rocketchat_auth.core import RocketClient
from rocketchat_auth.models import RocketUser
from rocketchat_auth.views import generate_password


def run(user_id=None):
    rocket = RocketClient()

    if rocket.connectAsAdmin():
        users = Uzanto.objects.filter(~Q(tipo__kodo='honora'), is_active=True)

        if user_id:
            users = users.filter(id=user_id)

        total = users.count()
        current = 0

        for user in users:
            current += 1
            rocket_user = rocket.users_list(query={'emails.address': user.chefa_retposhto})

            fullname = ' '.join([user.first_name, user.last_name]) \
                .strip()

            if not rocket_user['count']:
                print('{} of {} ({})'.format(current, total, user.id), user, 'Create new Rocket.Chat user!')
                password = generate_password()

                response = rocket.users_create(user.chefa_retposhto, fullname, password, str(user.id),
                                verified=user.konfirmita)
                if response['success']:
                    new_user = RocketUser(uzanto=user, rocket_user=response['user']['_id'], password=password)
                    new_user.save()
                else:
                    print('!!! Creation Error !!! - {}'.format(response))

            elif rocket_user['users'][0]['emails'][0]['verified'] != user.konfirmita:
                print('{} of {} ({})'.format(current, total, user.id), user, 'update user')

                try:
                    map_user = RocketUser.objects.get(uzanto=user)
                    rocket.users_update(map_user.rocket_user, {'verified': user.konfirmita,
                                                               'name': fullname,
                                                               'username': str(user.id)})
                except RocketUser.DoesNotExist:
                    password = generate_password()
                    new_user = RocketUser(uzanto=user, rocket_user=rocket_user['users'][0]['_id'], password=password)
                    new_user.save()
                    rocket.users_update(rocket_user['users'][0]['_id'],
                                        {'username': str(user.id),
                                         'name': fullname,
                                         'password': password,
                                         'verified': user.konfirmita})
            else:
                try:
                    map_user = RocketUser.objects.get(uzanto=user)
                    rocket.users_update(map_user.rocket_user, {'password': map_user.password,
                                                               'name': fullname,
                                                               'username': str(user.id)})
                except RocketUser.DoesNotExist:
                    password = generate_password()
                    new_user = RocketUser(uzanto=user, rocket_user=rocket_user['users'][0]['_id'], password=password)
                    new_user.save()
                    rocket.users_update(rocket_user['users'][0]['_id'], {'password': password,
                                                                         'name': fullname,
                                                                         'username': str(user.id)})

                print('{} of {} ({})'.format(current, total, user.id), user, 'already exists.')

        rocket.disconnect()
