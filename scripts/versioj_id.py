from versioj.models import *


def run():
    for model in (VersioUzantoEnskribo, VersioKomunumoEnskribo):
        print(model.__name__)
        for posedanto_uuid in set(
                model.objects.all().only('posedanto').values_list('posedanto_id', flat=True)
        ):
            versioj = model.objects.filter(posedanto_id=posedanto_uuid).order_by('krea_dato')
            for versio in versioj:
                versio.save()
