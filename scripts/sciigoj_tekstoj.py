# Исправляем старые фразы уведомлений на новый лад
from uzantoj.models import UzantojSciigoj


def run():
    ss = UzantojSciigoj.objects.filter(teksto__istartswith='Sur la muro de la komunumo <em>',
                                       teksto__iendswith='</em> publikigis novan eniron')

    print(ss.count())

    ss.update(teksto='Sur la muro de la komunumo publikigis novan eniron')
