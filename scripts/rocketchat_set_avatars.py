from main.models import Uzanto
from django.db.models import Q
from uzantoj.models import UzantojAvataro
from rocketchat_auth.core import RocketClient
import time


def get_avatar(uzanto):
    avataro = UzantojAvataro.objects.filter(posedanto=uzanto, forigo=False).order_by('-krea_dato')

    if avataro.count():
        avataro_url = 'https://tehnokom.su/media/{}'.format(avataro[0].bildo)
    else:
        avataro_url = 'https://tehnokom.su/static/main/images/vira-avataro.png' if uzanto.sekso == 'vira' else 'https://tehnokom.su/static/main/images/virina-avataro.png'
    return avataro_url


def get_uzantoj(user_id):
    args = [~Q(tipo__kodo='honora'),]
    if user_id is not None:
        args.append(Q(id=user_id))

    uzantoj = Uzanto.objects.filter(*args, is_active=True)
    return uzantoj


def run(user_id=None):
    rocket = RocketClient()

    if rocket.connectAsAdmin():
        uzantoj = get_uzantoj(user_id)
        total = uzantoj.count()
        cnt = 0

        for uzanto in uzantoj:
            cnt += 1
            status = rocket.users_info(username=str(uzanto.id))

            if status['success']:
                avataro = get_avatar(uzanto)
                success = False
                count = 0
                print('{} of {}'.format(cnt, total), uzanto, avataro)

                while not success and count < 10:
                    try:
                        count += 1
                        result = rocket.users_setAvatar(avataro, username=str(uzanto.id))
                        success = True
                        print(result['success'])
                    except:
                        print('!!! Error !!!')
                        rocket.disconnect()
                        time.sleep(3)
                        print('=== Reconnect ===')
                        rocket.connectAsAdmin()
            else:
                print('{} of {}'.format(cnt, total), uzanto, '!!! User not found in Rocket.Chat !!!')

        rocket.disconnect()
