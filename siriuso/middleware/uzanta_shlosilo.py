"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.utils.deprecation import MiddlewareMixin
from django.utils.http import http_date
from django.conf import settings
from django.contrib import auth
from django.contrib.auth.models import AnonymousUser
from django.http import HttpResponse
from siriuso.utils import get_token_session
from importlib import import_module

import time


def get_uzanto(request):
    uzanto = None

    for backend_path in settings.AUTHENTICATION_BACKENDS:
        backend = auth.load_backend(backend_path)
        try:
            uzanto = backend.authenticate(request)
            if uzanto:
                setattr(uzanto, 'backend', backend)
                break
        except:
            pass
    return uzanto or AnonymousUser()


class SiriusoUzantoTokenAuth(MiddlewareMixin):
    def __init__(self, get_response=None):
        self.get_response = get_response
        engine = import_module(settings.SESSION_ENGINE)
        self.SessionStore = engine.SessionStore

    def process_request(self, request):
        if hasattr(request, 'META') and 'HTTP_X_AUTH_TOKEN' in request.META:
            token = request.META.get('HTTP_X_AUTH_TOKEN')
            session_key = get_token_session(token)

            if session_key:
                request.COOKIES[settings.SESSION_COOKIE_NAME] = session_key

    def process_response(self, request, response):
        if hasattr(request, 'META') and 'HTTP_X_AUTH_TOKEN' in request.META:
            token = request.META.get('HTTP_X_AUTH_TOKEN')
            session_key = get_token_session(token)

            if session_key:
                if response.status_code != 500:
                    if request.session.get_expire_at_browser_close():
                        max_age = None
                        expires = None
                    else:
                        max_age = request.session.get_expiry_age()
                        expires_time = time.time() + max_age
                        expires = http_date(expires_time)

                    response.set_cookie(
                        settings.SESSION_COOKIE_NAME,
                        request.session.session_key, max_age=max_age,
                        expires=expires, domain=settings.SESSION_COOKIE_DOMAIN,
                        path=settings.SESSION_COOKIE_PATH,
                        secure=settings.SESSION_COOKIE_SECURE or None,
                        httponly=settings.SESSION_COOKIE_HTTPONLY or None,
                        samesite=settings.SESSION_COOKIE_SAMESITE,
                    )

        return response
