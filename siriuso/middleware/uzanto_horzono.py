"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.utils.deprecation import MiddlewareMixin
from django.contrib.gis.geoip2 import GeoIP2
from django.utils import timezone
import pytz


class SiriusoUzantoHorzono(MiddlewareMixin):
    """
    Данная прослойка должна устанавливать для сессии  часовой пояс
    пользователя.
    Алгоритм определения:
        1. Проверяем наличие HTTP заголовка X-CLIENT-TZ, если заголовок есть,
            то проверяем корректность имени часового пояса и выставляем его,
            обновляем часовой пояс в сессии
        2. Проверяем наличие часового пояса в сессии, если есть, то выставляем его
        3. Если пользователь аутентифицирован, то используем часовой пояс из профиля
        4. Иначе пытаемся определить часовой пояс по IP клиента
        5. Оставляем часовой пояс сервера
    """

    def __get_client_ip(self, request):
        if 'HTTP_X_REAL_IP' in request.META:
            client_ip = request.META.get('HTTP_X_REAL_IP')
        elif 'HTTP_X_FORWARDED_FOR' in request.META:
            client_ip = request.META.get('HTTP_X_FORWARDED_FOR').split(',')[0]
        else:
            client_ip = request.META.get('REMOTE_ADDR')

        return client_ip

    def __get_timezone_name(self, request):
        tzname = None

        if ('HTTP_X_CLIENT_TZ' in request.META and
            request.META.get('HTTP_X_CLIENT_TZ') in pytz.all_timezones):
            # Кэшируем часовой пояс в сессии
            request.session['uzanto_horzono'] = request.META.get('HTTP_X_CLIENT_TZ')
            return request.META.get('HTTP_X_CLIENT_TZ')

        if ('uzanto_horzono' in request.session.keys() and
            request.session.get('uzanto_horzono') in pytz.all_timezones):
            return request.session.get('uzanto_horzono')

        # Тут ещё надо реализовать часовой пояс из профиля пользователя
        if request.user.is_authenticated:
            pass

        if tzname is None:
            # Определяем по IP
            try:
                geoip = GeoIP2()
                city = geoip.city(self.__get_client_ip(request))

                if city is not None:
                    tzname = city.get('time_zone')
                    # Кэшируем часовой пояс в сессии
                    request.session['uzanto_horzono'] = tzname
            except:
                pass

        return tzname

    def process_request(self, request):
        # Тут нужно определить имя часового пояса
        # Можно взять его из профиля пользователя,
        # если он авторизирован

        tzname = self.__get_timezone_name(request)

        if tzname is not None:
            timezone.activate(pytz.timezone(tzname))

        return None

    def process_response(self, request, response):
        timezone.deactivate()
        return response
