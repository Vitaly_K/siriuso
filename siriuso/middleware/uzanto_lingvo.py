"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""


from django.utils.deprecation import MiddlewareMixin
from django.utils import translation


class SiriusoUzantoLingvo(MiddlewareMixin):
    def process_request(self, request):
        if 'HTTP_X_CLIENT_LANG' in request.META:
            lingvo = request.META['HTTP_X_CLIENT_LANG']

            if translation.check_for_language(lingvo):
                translation.activate(lingvo)

                if hasattr(request, 'session'):
                    request.session[translation.LANGUAGE_SESSION_KEY] = lingvo
                    request.session.modified = True

        if 'HTTP_X_CONTENT_LANG' in request.META:
            content_lingvo = request.META['HTTP_X_CONTENT_LANG']

            if translation.check_for_language(content_lingvo):
                request.CONTENT_LANGUAGE_CODE = content_lingvo
