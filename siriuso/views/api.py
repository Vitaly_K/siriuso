"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from rest_framework import viewsets
from uuid import UUID


class SiriusoUuidIdModelViewSet(viewsets.ReadOnlyModelViewSet):

    def get_object(self):
        if self.lookup_field and self.lookup_field in self.kwargs:
            ido = self.kwargs[self.lookup_field]

            if ido.isnumeric():
                self.kwargs = {'id': ido}
                self.lookup_field = 'id'
            else:
                try:
                    uuid = UUID(self.kwargs.get(self.lookup_field), version=4)
                except ValueError:
                    pass

        return super().get_object()
