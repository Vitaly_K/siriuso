"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.shortcuts import redirect


def auth_user_redirect(function=None, redirect_page=None):
    """
    Декоратор для представлений страниц, который переадресует
    зарегистрированны пользователей на указанную страницу.
    Если имя страницы не указано, то переадресует на стартовую страницу
    """

    def wrapped(request, *args, **kwargs):
        if request.user.is_authenticated:
            response = redirect('start_page') if redirect_page is None else redirect(redirect_page)
            return response

        return function(request, *args, **kwargs)

    if function is not None:
        return wrapped

    def decorator(view_function):
        return wrapped

    return decorator
