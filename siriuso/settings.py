"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

import os
import re
from django.utils.translation import ugettext_lazy as _
from corsheaders.defaults import default_headers

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'cjy)r3@=u68ls_nbew+6&3klq#ea4j*+(e(^=z=tc^ldqmrn)9'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['wp.tehnokom.su', 'localhost', '10.168.1.26', 'tehnokom.su', '127.0.0.1', 'django', '10.0.2.2']

# Proxy protocol
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Use proxy header X_FORWARDED_HOST
USE_X_FORWARDED_HOST = True

# Max uploads fields
DATA_UPLOAD_MAX_NUMBER_FIELDS = 10000

# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.flatpages',
    'django.contrib.postgres',
    'django.contrib.gis',
    'django_celery_beat',
    'corsheaders',
    'django_hosts',
    'graphene_django',
    'captcha',
    'channels',
    'embed_video',
    'rocketchat_auth',
    'akademio.apps.AkademioConfig',
    'dekretumoj.apps.DekretumojConfig',
    'dokumentoj.apps.DokumentojConfig',
    'enciklopedio.apps.EnciklopedioConfig',
    'enhavo.apps.EnhavoConfig',
    'esploradoj.apps.EsploradojConfig',
    'eventoj.apps.EventojConfig',
    'forumoj.apps.ForumojConfig',
    'fotoj.apps.FotojConfig',
    'ideoj.apps.IdeojConfig',
    'informiloj.apps.InformilojConfig',
    'internacia_plano.apps.InternaciaPlanoConfig',
    'komunumoj.apps.KomunumojConfig',
    'kombatantoj.apps.KombatantojConfig',
    'konferencoj.apps.KonferencojConfig',
    'kodo.apps.KodoConfig',
    'lokalizo.apps.LokalizoConfig',
    'ludoj.apps.LudojConfig',
    'main.apps.MainConfig',
    'mesagxilo.apps.MesagxiloConfig',
    'memorkartoj.apps.MemorkartojConfig',
    'muroj.apps.MuroConfig',
    'novayoj.apps.NovayojConfig',
    'premioj.apps.PremiojConfig',
    'tamagocxi.apps.TamagocxiConfig',
    'registrado.apps.RegistradoConfig',
    'resursoj.apps.ResursojConfig',
    'sano.apps.SanoConfig',
    'scioj.apps.SciojConfig',
    'sontrakoj.apps.SontrakojConfig',
    'statistiko.apps.StatistikoConfig',
    'taskoj.apps.TaskojConfig',
    'teknika_subteno.apps.TeknikaSubtenoConfig',
    'uzantoj.apps.UzantojConfig',
    'videoj.apps.VideojConfig',
    'versioj.apps.VersiojConfig',
    'geo.apps.GeoConfig',
    # Universo apps
    'universo_uzantoj.apps.UniversoUzantoConfig',
    'universo_bazo.apps.UniversoBazoConfig',
    'universo_dokumentoj.apps.UniversoDokumentojConfig',
    'universo_kosmo.apps.UniversoKosmoConfig',
    'universo_mono.apps.UniversoMonoConfig',
    'universo_objektoj.apps.UniversoObjektojConfig',
    'universo_organizoj.apps.UniversoOrganizojConfig',
    'universo_rajtoj.apps.UniversoRajtojConfig',
    'universo_resursoj.apps.UniversoResursojConfig',
    'universo_scipovoj.apps.UniversoScipovojConfig',
    'universo_sxablonoj.apps.UniversoSxablonoConfig',
    'universo_taskoj.apps.UniversoTaskojConfig',
]

MIDDLEWARE = [
    'django.middleware.common.BrokenLinkEmailsMiddleware',
    'django_hosts.middleware.HostsRequestMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'siriuso.middleware.SiriusoUzantoTokenAuth',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'siriuso.middleware.SiriusoUzantoLingvo',
    'django.middleware.locale.LocaleMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    'django_hosts.middleware.HostsResponseMiddleware',
    'siriuso.middleware.SiriusoAutomataKunteksto',
    'siriuso.middleware.SiriusoPorteblaKliento',
    'siriuso.middleware.SiriusoUzantoHorzono'
]

# Graphene
GRAPHENE = {
    'SCHEMA': 'siriuso.api.schema.schema'
}

# Channels
ASGI_APPLICATION = "siriuso.routing.application"
CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [("redis", 6379)],
        },
    },
}

# CORS: https://github.com/ottoyiu/django-cors-headers#configuration

# Разрешать все запросы при отладке
CORS_ORIGIN_ALLOW_ALL = DEBUG

CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_WHITELIST = (
    'localhost:8080',
    'localhost:8000',
    'localhost:3000',
    '127.0.0.1:8080',
    '127.0.0.1:8000',
    '127.0.0.1:3000'
)

# Разрешенные заголовки
CORS_ALLOW_HEADERS = default_headers + (
    'x-auth-token',
    'x-client-tz',
    'x-client-lang',
    'x-content-lang',
)

ADMINS = (
    # ('pinballwizard', 'mentenebris@gmail.com'),
    ('Равиль Сарваритдинов', 'ra9oaj@osfk.ru'),
    ('admin-web', 'admin-web@tehnokom.su'),
    ('archive', 'arhiv@tehnokom.su'),
)

# MANAGERS = (
#    ('Владимир Левадный', 'miru-mir@tehnokom.su'),
# )


ROOT_URLCONF = 'siriuso.urls'

ROOT_HOSTCONF = 'siriuso.hosts'  # Change `mysite` to the name of your project

DEFAULT_HOST = 'www'  # Name of the default host, we will create it in the next steps

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'main.context_processors.siriuso_context',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'siriuso.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST': 'postgres',
        'PORT': '5432',
        'NAME': 'siriuso',
        'USER': 'siriuso',
        'PASSWORD': 'oGLOWo8nd3',
        'TEST': {
            'NAME': 'test_siriuso',
        },
    },
    # 'osm': {
    #     'ENGINE': 'django.contrib.gis.db.backends.postgis',
    #     'HOST': 'osm',
    #     'PORT': '5432',
    #     'NAME': 'siriuso',
    #     'USER': 'siriuso',
    #     'PASSWORD': '12345',
    # },
}

# Redis Cache
CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://redis:6379/0',
        'TIMEOUT': 300,
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    }
}

SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "default"


# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTHENTICATION_BACKENDS = [
    # 'django.contrib.auth.backends.ModelBackend',
    'siriuso.backends.UzantoAuthBackend',
    'siriuso.backends.TokenAuthBackend',
]


# Internationalization

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LANGUAGES = (
    ('eo-xx', _('Internacia (Esperanto)')),
    ('ru-ru', _('Rusa')),
    ('en-gb', _('Angla')),
    ('sr-rs', _('Serba')),
    ('et-ee', _('Estona')),
    ('lv-lv', _('Latva')),
    ('de-de', _('Germana'))
)

# SMTP settings
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_USE_SSL = False
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'no-reply@tehnokom.su'
EMAIL_HOST_PASSWORD = ''

# настройка ящиков
SERVER_EMAIL = 'no-reply@tehnokom.su'
DEFAULT_FROM_EMAIL = 'no-reply@tehnokom.su'
EMAIL_NO_REPLY = 'no-reply@tehnokom.su'

# https://docs.djangoproject.com/en/dev/ref/settings/#login-url
LOGIN_URL = '/ensaluti/'

# месторасположение файлов перевода
LOCALE_PATHS = (
    'locale',
)

# Django Sites Framework
SITE_ID = 1

STATIC_URL = '/static/'
STATIC_ROOT = 'static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = 'media/'


AUTH_USER_MODEL = 'main.Uzanto'

# Embed video settings
# http://django-embed-video.readthedocs.io/en/v1.1.0/api/embed_video.settings.html
EMBED_VIDEO_BACKENDS = (
    'embed_video.backends.YoutubeBackend',
    # 'embed_video.backends.VimeoBackend',
    # 'embed_video.backends.SoundCloudBackend',
)


# Путь до БД GeoIP2 (https://dev.maxmind.com/geoip/geoip2/geolite2/)
GEOIP_PATH = '/opt/GeoIP/GeoLite2-City_20180206/GeoLite2-City.mmdb'

# RECAPTCHA MODULE SETTINGS
NOCAPTCHA = True
# Test keys
RECAPTCHA_PUBLIC_KEY = '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
RECAPTCHA_PRIVATE_KEY = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe'
SILENCED_SYSTEM_CHECKS = ['captcha.recaptcha_test_key_error']

# CELERY SETTINGS
CELERY_BROKER_URL = 'amqp://siriuso:siriuso@rabbitmq:5672//'
CELERY_RESULT_BACKEND = 'redis://redis:6379/1'
CELERY_TASK_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_ANNOTATIONS = {'tasks.add': {'rate_limit': '10/s'}}

# RocketChat Auth
MONGO_DB = 'mongodb:27017'
ROCKETCHAT = 'http://localhost:3030/im'
ROCKETCHAT_LAN = 'http://rocketchat:3030/im'
ROCKETCHAT_SSL_VERIFY = True
ROCKETCHAT_ADMIN_USERNAME = 'admin'
ROCKETCHAT_ADMIN_PASSWORD = '12345678'

# Logging

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
    },
    'handlers': {
        'file': {
            'level': 'INFO',
            'filters': ['require_debug_false'],
            'class': 'logging.FileHandler',
            'filename': '/var/log/django_debug.log',
            'formatter': 'verbose'
        },
        'console': {
            'level': 'INFO',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        }
    },
    'loggers': {
        'django': {
            'handlers': ['file', 'console', 'mail_admins'],
            'level': 'DEBUG',
            'propagate': True,
        },
        # 'django.server': {
        #     'handlers': ['mail_admins'],
        #     'level': 'ERROR',
        #     'propagate': True,
        # },
    },
}


IGNORABLE_404_URLS = [
    re.compile(r'^/wp-content/'),
    re.compile(r'^/wp-admin/'),
    re.compile(r'^/im/'),
    re.compile(r'^/sockjs/'),
    re.compile(r'^/packages/'),
    re.compile(r'^/redmine/')
]
