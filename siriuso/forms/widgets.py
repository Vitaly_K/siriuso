"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.forms import widgets
from informiloj.models import InformilojLingvo


class LingvoTextWidget(widgets.Widget):
    template_name = 'siriuso/forms/widgets/lingvo_text.html'

    def get_context(self, name, value, attrs):
        context = {
            'lingvoj': InformilojLingvo.objects.filter(forigo=False).order_by('nomo')
        }

        context.update(super(LingvoTextWidget, self).get_context(name, value,attrs))
        return context


class LingvoInputWidget(LingvoTextWidget):
    template_name = 'siriuso/forms/widgets/lingvo_input.html'
