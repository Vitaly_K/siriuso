"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from celery import current_app
from django.utils.translation import gettext_lazy as _
from main.models import SistemaPeto


class TaskDoesNotExistError(Exception):
    pass


def peti(tasko, autoro=None, args=None, kwargs=None):
    nomo_funk = {
        nomo: func
        for nomo, func in current_app.tasks.items()
        if not nomo.startswith('celery.') and (nomo == tasko or func == tasko)
    }

    if len(nomo_funk):
        tasko_nomo, tasko_funk = nomo_funk.popitem()
        funk_args = args or list()
        funk_kwargs = kwargs or dict()
        t = tasko_funk.apply_async(countdown=1, args=funk_args, kwargs=funk_kwargs)
        SistemaPeto.objects.create(
            tasko_id=t.id,
            autoro=autoro,
            tasko=tasko_nomo,
            statuso=t.status,
            parametroj={
                'args': args or list(),
                'kwargs': kwargs or dict(),
            }
        )
        return t
    else:
        raise TaskDoesNotExistError(_('Task "%s" does not exist') % str(tasko))
