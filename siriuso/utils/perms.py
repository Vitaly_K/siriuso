"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import Permission
from django.db.models import Q
from django.apps import apps as sys_apps
import functools


def get_special_perms(spec_grupo, return_objects=False, apps=None):
    SpecialajGrupoj = sys_apps.get_model('main', 'SpecialajGrupoj')

    try:
        if not spec_grupo:
            raise ValueError(_('Argumento "spec_gupo" ne devas esti malplena ĉeno'))
        perms = SpecialajGrupoj.objects.none()

        spec = SpecialajGrupoj.objects.get(kodo=spec_grupo)
        for grupo in spec.grupoj.all():
            cond = Q() if not apps else Q(content_type__app_label__in=apps)
            perms |= grupo.permissions.filter(cond).values_list('content_type__app_label', 'codename').order_by()

        return {'%s.%s' % (ctx, perms) for ctx, perms in perms} if not return_objects else perms
    except SpecialajGrupoj.DoesNotExist:
        return set() if not return_objects else Permission.objects.none()


# Получаем список прав для не зарегистрированных пользователей
user_neregistrita_perms = functools.partial(get_special_perms, 'neregistrita')
# Получаем список прав зарегистрированного пользователя
user_registrita_perms = functools.partial(get_special_perms, 'registrita')
# Получаем список прав для страницы пользователя
user_page_perms = functools.partial(get_special_perms, 'uzanta_pagho')
# Получаем список прав для админа сообществ
user_komunumo_adm_perms = functools.partial(get_special_perms, 'komunumano-adm')
# Получаем список прав для модератора сообществ
user_komunumo_mod_perms = functools.partial(get_special_perms, 'komunumano-mode')


def has_registrita_perm(perm_name):
    return perm_name in user_registrita_perms()


def has_user_page_perm(perm_name):
    return perm_name in user_page_perms()


def has_komunumo_adm_perms(perm_name):
    return perm_name in user_komunumo_adm_perms()


def has_komunumo_mod_perms(perm_name):
    return perm_name in user_komunumo_mod_perms()


def has_neregistrita_perm(perm_name):
    return perm_name in user_neregistrita_perms()
