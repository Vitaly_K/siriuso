"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from informiloj.api.schema import InformiloQuery
from uzantoj.api.mutations import Mutations as UzantoMutations
from uzantoj.api.schema import UzantoQuery
from uzantoj.api.sciigoj import SciigojQuery
from komunumoj.api.mutations import Mutations as KomMutations
from statistiko.api.queries import Queries as StatQueries
from forumoj.api.schema import ForumoQuery
from forumoj.api.mutations import ForumojMutations
from komunumoj.api.schema import KomunumoQuery
from fotoj.api.schema import FotojQuery
from fotoj.api.mutations import FotojMutations
from akademio.api.schema import AkademioQuery
from akademio.api.mutations import AkademioMutations
from enciklopedio.api.schema import EnciklopedioQuery
from enciklopedio.api.mutations import EnciklopedioMutations
from kodo.api.schema import KodoQuery
from kodo.api.mutations import KodoMutations
from konferencoj.api.schema import KonferencojQuery
from konferencoj.api.mutations import KonferencojMutations
from esploradoj.api.schema import EsploradojQuery
from esploradoj.api.mutations import EsploradojMutations
from mesagxilo.api.schema import MesagxiloQuery
from mesagxilo.api.mutations import MesagxiloMutations
from memorkartoj.api.schema import MemorkartojQuery
from memorkartoj.api.mutations import MemorkartojMutations
from muroj.api.schema import MuroQuery
from muroj.api.mutations import Mutations as MurojMutations
from versioj.api.mutations import VersioMutations

from taskoj.api.schema import Query as TaskojQuery
from taskoj.api.mutations import TaskojMutations
from premioj.api.schema import PremiojQuery
from premioj.api.mutations import PremiojMutations
from tamagocxi.api.schema import TamagocxiQuery
from tamagocxi.api.mutations import TamagocxiMutations
from lokalizo.api.schema import LokalizoQuery
from lokalizo.api.mutations import LokalizoMutations

from main.api.schema import SiriusoQuery
from main.api.mutations import SiriusoMutations

from kombatantoj.api.scheme import KombatantoQuery
from kombatantoj.api.mutation import KombatantoMutation
from dokumentoj.api.scheme import DokumentoQuery
from dokumentoj.api.mutation import DokumentoMutation
from resursoj.api.scheme import ResursoQuery
from resursoj.api.mutation import ResursoMutation
from universo_uzantoj.api.schema import UniversoUzantoQuery
from universo_uzantoj.api.mutations import UniversoUzantoMutations
from universo_bazo.api.schema import UniversoBazoQuery
from universo_bazo.api.mutations import UniversoBazoMutations
from universo_resursoj.api.schema import UniversoResursojQuery
from universo_resursoj.api.mutations import UniversoResursoMutations
from universo_organizoj.api.schema import UniversoOrganizojQuery
from universo_organizoj.api.mutations import UniversoOrganizoMutations
from universo_objektoj.api.schema import UniversoObjektojQuery
from universo_objektoj.api.mutations import UniversoObjektoMutations
from universo_taskoj.api.schema import UniversoTaskojQuery
from universo_taskoj.api.mutations import UniversoTaskojMutations
from universo_kosmo.api.schema import UniversoKosmoQuery
from universo_kosmo.api.mutations import UniversoKosmoMutations
from universo_mono.api.schema import UniversoMonoQuery
from universo_mono.api.mutations import UniversoMonoMutations
from universo_sxablonoj.api.schema import UniversoSxablonoQuery
from universo_scipovoj.api.schema import UniversoScipovojQuery
from universo_scipovoj.api.mutations import UniversoScipovoMutations
from universo_rajtoj.api.schema import UniversoRajtojQuery
from universo_rajtoj.api.mutations import UniversoRajtojMutations

from mesagxilo.api.subscription import MesagxiloSubscription

import graphene


class GlobalQuery(UzantoQuery, StatQueries, KomunumoQuery, ForumoQuery, InformiloQuery, MuroQuery,
                  TaskojQuery, AkademioQuery, EnciklopedioQuery, KonferencojQuery, FotojQuery, KodoQuery, SciigojQuery,
                  PremiojQuery, SiriusoQuery, KombatantoQuery, DokumentoQuery, ResursoQuery, EsploradojQuery,
                  MesagxiloQuery, MemorkartojQuery, TamagocxiQuery, LokalizoQuery, UniversoUzantoQuery,
                  UniversoBazoQuery, UniversoResursojQuery, UniversoOrganizojQuery, UniversoObjektojQuery,
                  UniversoTaskojQuery, UniversoKosmoQuery, UniversoMonoQuery, UniversoSxablonoQuery,
                  UniversoScipovojQuery, UniversoRajtojQuery, graphene.ObjectType):
    pass


class GlobalMutations(UzantoMutations, KomMutations, ForumojMutations, MurojMutations,
                      FotojMutations, AkademioMutations, EnciklopedioMutations, KonferencojMutations,
                      MesagxiloMutations, TaskojMutations, VersioMutations, KodoMutations, PremiojMutations,
                      SiriusoMutations, KombatantoMutation, DokumentoMutation, ResursoMutation, EsploradojMutations,
                      MemorkartojMutations, TamagocxiMutations, LokalizoMutations, UniversoUzantoMutations,
                      UniversoBazoMutations, UniversoResursoMutations, UniversoOrganizoMutations,
                      UniversoObjektoMutations, UniversoTaskojMutations, UniversoKosmoMutations,
                      UniversoMonoMutations, UniversoScipovoMutations, UniversoRajtojMutations,
                      graphene.ObjectType):
    pass


class GlobalSubsriptions(MesagxiloSubscription, graphene.ObjectType):
    pass


schema = graphene.Schema(query=GlobalQuery, mutation=GlobalMutations, subscription=GlobalSubsriptions)
