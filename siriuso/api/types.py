"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from graphene import ObjectType, String, Boolean, List
from siriuso.utils import get_enhavo, get_lang_kodo


class SiriusoLingvo(ObjectType):
    lingvoj = List(String)
    lingvo = String()
    enhavo = String()
    chefa_varianto = Boolean()

    @classmethod
    def resolve_lingvoj(cls, root, info):
        if root:
            return root.get('lingvo')

        return None

    @classmethod
    def resolve_lingvo(cls, root, info):
        # Тут нужно доделать определение локали
        lingvo = get_lang_kodo(info.context)

        if root:
            result = get_enhavo(root, lingvo) or get_enhavo(root, empty_values=True)

            if result:
                return result[1]

        return None

    @classmethod
    def resolve_enhavo(cls, root, info):
        # Тут нужно доделать определение локали
        lingvo = get_lang_kodo(info.context)

        if root:
            result = get_enhavo(root, lingvo) or get_enhavo(root, empty_values=True)

            if result:
                return result[0]

        return None

    @classmethod
    def resolve_chefa_varianto(cls, root, info):
        lingvo = cls.resolve_lingvo(root, info)

        if lingvo:
            return lingvo == root['chefa_varianto']

        return None


class ErrorNode(ObjectType):
    field = String()
    message = String()
