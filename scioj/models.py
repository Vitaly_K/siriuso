"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres import fields as pgfields
from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo
from main.models import SiriusoBazaAbstrakta3


# Функционал знаний
# Университеты, использует абстрактный класс SiriusoBazaAbstrakta3
class SciojUniversitato(SiriusoBazaAbstrakta3):

    # название в таблице названий общественных проектов, от туда будет браться название с нужным языковым тегом
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание в таблице описаний общественных проектов, от туда будет браться описание с нужным языковым тегом
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'scioj_universitatoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Universitato')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Universitatoj')
