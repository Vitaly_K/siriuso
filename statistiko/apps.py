from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class StatistikoConfig(AppConfig):
    name = 'statistiko'
    verbose_name = _('Statistiko')
