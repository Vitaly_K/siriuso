"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from django.db import models
from django.db.models import Max, Q
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres import fields as pgfields
from django.contrib.auth.models import Permission

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from universo_bazo.models import UniversoBazaMaks
from universo_uzantoj.models import UniversoUzanto
from universo_objektoj.models import UniversoObjekto
from universo_mono.models import UniversoMonoKonto
from universo_taskoj.models import UniversoProjekto


# Глобальные шаблоны Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoSxablono(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # связанные шаблоны объектов
    objekto = models.ManyToManyField(UniversoObjekto, verbose_name=_('Ŝablono de objekto'), blank=True, 
                                    db_table='universo_sxablonoj_objectoj_sxablonoj_ligiloj')

    # связанные шаблоны счетов (кошельков)
    konto = models.ManyToManyField(UniversoMonoKonto, verbose_name=_('Ŝablono de konto'), blank=True, 
                                   db_table='universo_sxablonoj_kontoj_sxablonoj_ligiloj')

    # связанные шаблоны проектов
    projekto = models.ManyToManyField(UniversoProjekto, verbose_name=_('Ŝablono de projekto'), blank=True, 
                                   db_table='universo_sxablonoj_projektoj_sxablonoj_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_sxablonoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ŝablono de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ŝablono de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_sxablonoj', _('Povas vidi ŝablonoj de Universo')),
            ('povas_krei_universo_sxablonoj', _('Povas krei ŝablonoj de Universo')),
            ('povas_forigi_universo_sxablonoj', _('Povas forigi ŝablonoj de Universo')),
            ('povas_shangxi_universo_sxablonoj', _('Povas ŝanĝi ŝablonoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoSxablono, self).save(force_insert=force_insert, force_update=force_update,
                                           using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_sxablonoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_sxablonoj.povas_vidi_universo_sxablonoj',
                'universo_sxablonoj.povas_krei_universo_sxablonoj',
                'universo_sxablonoj.povas_forigi_universo_sxablonoj',
                'universo_sxablonoj.povas_shangxi_universo_sxablonoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_sxablonoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_sxablonoj.povas_vidi_universo_sxablonoj')
                    or user_obj.has_perm('universo_sxablonoj.povas_vidi_universo_sxablonoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_sxablonoj.povas_vidi_universo_sxablonoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
