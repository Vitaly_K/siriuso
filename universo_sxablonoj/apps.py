from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UniversoSxablonoConfig(AppConfig):
    name = 'universo_sxablonoj'
    verbose_name = _('Ŝablonoj de Universo')

    def ready(self):
        import universo_sxablonoj.signals
