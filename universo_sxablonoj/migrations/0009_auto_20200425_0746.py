# Generated by Django 2.2.11 on 2020-04-25 07:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('universo_sxablonoj', '0008_auto_20200424_1548'),
    ]

    operations = [
        migrations.AlterField(
            model_name='universosxablono',
            name='konto',
            field=models.ManyToManyField(blank=True, db_table='universo_sxablonoj_kontoj_sxablonoj_ligiloj', to='universo_mono.UniversoMonoKonto', verbose_name='Ŝablono de konto'),
        ),
        migrations.AlterField(
            model_name='universosxablono',
            name='objekto',
            field=models.ManyToManyField(blank=True, db_table='universo_sxablonoj_objectoj_sxablonoj_ligiloj', to='universo_objektoj.UniversoObjekto', verbose_name='Ŝablono de objekto'),
        ),
        migrations.AlterField(
            model_name='universosxablono',
            name='projekto',
            field=models.ManyToManyField(blank=True, db_table='universo_sxablonoj_projektoj_sxablonoj_ligiloj', to='universo_taskoj.UniversoProjekto', verbose_name='Ŝablono de projekto'),
        ),
    ]
