from django.urls import path, re_path, include
from geo.views import RegionShape

urlpatterns = [
    path('border', RegionShape.as_view()),
]