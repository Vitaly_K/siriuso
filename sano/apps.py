from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class SanoConfig(AppConfig):
    name = 'sano'
    verbose_name = _('Sano')
