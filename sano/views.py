"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.shortcuts import render, get_object_or_404
from django.template.response import TemplateResponse
from django.utils import timezone
from .models import *
from main.models import Uzanto
from siriuso.views import SiriusoTemplateView


# Задачи здоровья
class SanoTasko(SiriusoTemplateView):

    template_name = 'sano/sano_uzanto.html'
    mobile_template_name = 'sano/portebla/t_sano_uzanto.html'

    http_method_names = ['get']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        queryset_uzanto = Uzanto.objects.values('id', 'uuid', 'sekso', 'unua_nomo__enhavo', 'familinomo__enhavo',
                                                'uzantojavataro__bildo', 'uzantojkovrilo__bildo',
                                                'uzantojstatuso__teksto__enhavo')

        uzanto = get_object_or_404(queryset_uzanto, id=kwargs['uzanto_id'], is_active=True, konfirmita=True,
                                   is_superuser=False)

        taskoj = SanoXeneralaTasko.objects \
            .filter(krea_dato__lte=timezone.now(), publikigo=True, arkivo=False, forigo=False,
                    autoro__komunumojorganizomembro__posedanto__tipo__kodo='gvardio') \
            .order_by('krea_dato').values('uuid', 'id', 'priskribo__enhavo', 'autoro__id',
                                          'autoro__unua_nomo__enhavo', 'autoro__familinomo__enhavo',
                                          'autoro__uzantojavataro__bildo_min',
                                          'autoro__komunumojorganizomembro__laborarolo__nomo__enhavo')

        context.update({'taskoj': taskoj})

        return context
