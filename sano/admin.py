"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from .models import *


# Функционал задач здоровья

# Типы реализаций задач здоровья
class SanoTaskoRealigoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = SanoTaskoRealigoTipo

admin.site.register(SanoTaskoRealigoTipo, SanoTaskoRealigoTipoAdmin)


# Типы направлений задач здоровья
class SanoDirektoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = SanoDirektoTipo

admin.site.register(SanoDirektoTipo, SanoDirektoTipoAdmin)


# Виды направлений задач здоровья
class SanoDirektoSpecoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = SanoDirektoSpeco

admin.site.register(SanoDirektoSpeco, SanoDirektoSpecoAdmin)


# Типы этапов задач здоровья
class SanoEtapoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = SanoEtapoTipo

admin.site.register(SanoEtapoTipo, SanoEtapoTipoAdmin)


# Виды этапов задач здоровья
class SanoEtapoSpecoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = SanoEtapoSpeco

admin.site.register(SanoEtapoSpeco, SanoEtapoSpecoAdmin)


# Типы задач здоровья
class SanoTaskoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = SanoTaskoTipo

admin.site.register(SanoTaskoTipo, SanoTaskoTipoAdmin)


# Виды задач здоровья
class SanoTaskoSpecoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = SanoTaskoSpeco

admin.site.register(SanoTaskoSpeco, SanoTaskoSpecoAdmin)


# Глобальные задачи здоровья

# Направления глобальных задач здоровья
class SanoXeneralaDirektoAdmin(admin.ModelAdmin):
    list_display = ('id', 'priskribo', 'publikigo', 'uuid',)
    exclude = ('id', 'priskribo',)

    class Meta:
        model = SanoXeneralaDirekto

admin.site.register(SanoXeneralaDirekto, SanoXeneralaDirektoAdmin)


# Этапы глобальных задач здоровья
class SanoXeneralaEtapoAdmin(admin.ModelAdmin):
    list_display = ('id', 'priskribo', 'publikigo', 'uuid',)
    exclude = ('id', 'priskribo',)

    class Meta:
        model = SanoXeneralaEtapo

admin.site.register(SanoXeneralaEtapo, SanoXeneralaEtapoAdmin)


# Глобальные задачи здоровья
class SanoXeneralaTaskoAdmin(admin.ModelAdmin):
    list_display = ('id', 'autoro', 'publikigo', 'uuid',)
    exclude = ('id', 'priskribo',)

    class Meta:
        model = SanoXeneralaTasko

admin.site.register(SanoXeneralaTasko, SanoXeneralaTaskoAdmin)
