"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib.auth.signals import user_logged_out, user_logged_in
from django.contrib.sites.shortcuts import get_current_site
from django.dispatch import receiver
from django.conf import settings
from django.db.models.signals import post_save, post_delete
from registrado.views import uzanto_registrita, uzanto_konfirmita

from main.models import Uzanto
from uzantoj.models import UzantojAvataro
from .core import RocketClient
from .models import RocketUser
from .views import generate_password
from urllib.parse import urljoin
from .core.common import get_avatar


@receiver(user_logged_in)
def login(sender, user, request, **kwargs):
    try:
        rocket_user = RocketUser.objects.get(uzanto=user)

        if not rocket_user.auth_token\
                or (isinstance(rocket_user.auth_token, str) and not len(rocket_user.auth_token)):
            rocket = RocketClient()
            response = rocket.login(rocket_user.uzanto.chefa_retposhto, rocket_user.password)

            if response:
                rocket_user.auth_token = response['token']
                rocket_user.save()

    except:
        pass


@receiver(user_logged_out)
def logout(sender, user, request, **kwargs):
    try:
        rocket_user = RocketUser.objects.get(uzanto=user)

        if isinstance(rocket_user.auth_token, str) and len(rocket_user.auth_token):
            rocket = RocketClient()

            if rocket.connectByAuthToken(rocket_user.rocket_user,
                                         rocket_user.auth_token):
                rocket.logout()

            rocket_user.auth_token = None
            rocket_user.save()
    except:
        pass


@receiver(post_save, sender=UzantojAvataro)
def update_avatar(instance, **kwargs):
    if instance.bildo_maks and instance.bildo and instance.bildo_min:
        avatar_url = 'https://{}{}'.format(get_current_site(request=None).domain,
                                            urljoin(settings.MEDIA_URL, str(instance.bildo)))
        rocketchat = RocketClient()

        if rocketchat.connectAsAdmin():
            try:
                user = RocketUser.objects.get(uzanto=instance.posedanto)
                response = rocketchat.users_setAvatar(avatar_url, user_id=user.rocket_user)
            except:
                pass

            rocketchat.disconnect()


@receiver(post_delete, sender=Uzanto)
def delete_user(instance, **kwargs):
    try:
        rocketchat = RocketClient()

        if rocketchat.connectAsAdmin():
            response = rocketchat.users_delete(user_id=instance.id)
            rocketchat.disconnect()
    except:
        pass


@receiver(post_save, sender=Uzanto)
def update_user(instance, **kwargs):
    try:
        user = RocketUser.objects.get(uzanto=instance)
        rocket = RocketClient()

        if rocket.connectAsAdmin():
            fullname = ' '.join([instance.first_name, instance.last_name]).strip()
            rocket.users_update(user.rocket_user, {'name': fullname, 'email': instance.chefa_retposhto})
            rocket.disconnect()

    except:
        pass


@receiver(uzanto_registrita)
def user_registration(uzanto, request, **kwargs):
    rocketchat = RocketClient()
    try:
        if rocketchat.connectAsAdmin():
            fullname = ' '.join([uzanto.first_name, uzanto.last_name]) \
                .strip()
            password = generate_password()
            response = rocketchat.users_create(uzanto.chefa_retposhto, fullname, password, str(uzanto.id),
                                               verified=False)

            if response['success']:
                new_user = RocketUser(uzanto=uzanto, password=password, rocket_user=response['user']['_id'])
                new_user.save()

            rocketchat.disconnect()
    except:
        pass


@receiver(uzanto_konfirmita)
def user_confirmation(uzanto, request, **kwargs):
    rocketchat = RocketClient()

    try:
        if rocketchat.connectAsAdmin():
            try:
                user = RocketUser.objects.get(uzanto=uzanto)
                rocketchat.users_update(user.rocket_user, {'verified': True})
                rocketchat.users_setAvatar(get_avatar(uzanto, request), user_id=user.rocket_user)
            except:
                pass

            rocketchat.disconnect()
    except:
        pass
