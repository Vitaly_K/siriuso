"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from uzantoj.models import UzantojAvataro
from urllib.parse import urljoin, urlparse
from django.conf import settings


def get_avatar(user, request):
    avataro = UzantojAvataro.objects.filter(posedanto=user, forigo=False).order_by('-krea_dato')

    if avataro.count():
        avataro = avataro[0]
        avataro_url = request.build_absolute_uri(urljoin(settings.MEDIA_URL, str(avataro.bildo)))
    else:
        avataro = ('/static/main/images/vira-avataro.png' if user.sekso == 'vira'
                   else '/static/main/images/virina-avataro.png')
        avataro_url = request.build_absolute_uri(avataro)

    if settings.DEBUG:
        url = urlparse(avataro_url)
        host_port = url.netloc.split(':')

        if host_port[0] in ['localhost', '127.0.0.1']:
            new_host = 'django'
            server_port = request.META['SERVER_PORT']

            if not url.port and server_port and server_port not in [80, 443]:
                new_host += ':{}'.format(server_port)

            avataro_url = avataro_url.replace(host_port[0], new_host, 1)

    return avataro_url
