"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

import json
from urllib.parse import urlencode


def login(self, user, password):
    body = {'user': user, 'password': password}
    response = self.request('POST', 'login', body)

    if response.status == 200 and isinstance(response.data, dict):
        return {'token': response.data['data']['authToken'], 'user_id': response.data['data']['userId']}
    return False


def logout(self):
    if self.is_connected:
        response = self.request('POST', 'logout')
        return (response.status == 200 and isinstance(response.data, dict) and response.data['status'] == 'success')
    return False


def commands_get(self, command):
    if self.is_connected:
        response = self.request('GET', 'commands.get?command={}'.format(command))
        return response


def commands_list(self):
    if self.is_connected:
        response = self.request('GET', 'commands.list')
        return response


def users_create(self, email, name, password, username, **kwargs):
    if self.is_connected:
        allowed = ['active', 'roles', 'joinDefaultChannels', 'requirePasswordChange', 'sendWelcomeEmail', 'verified',
                   'customFields']
        api_body = {'email': email, 'name': name, 'password': password, 'username': username}

        for key, val in kwargs.items():
            if key in allowed:
                if key == 'roles' and not isinstance(val, list):
                    raise TypeError('The argument \'roles\' must be a \'list\' type')
                if key == 'customFields' and not isinstance(kwargs[key], dict):
                    raise TypeError('The argument \'customFields\' must be a \'dict\' type')

                api_body.update({key: val})

        response = self.request('POST', 'users.create', api_body)

        if response.status in [200, 400]:
            return response.data

    return False


def users_update(self, user_id, data):
    if self.is_connected:
        if isinstance(data, dict):
            api_body = {'userId': user_id, 'data': {}}

            for key, val in data.items():
                if key in ['email', 'name', 'password', 'username', 'active', 'roles', 'joinDefaultChannels',
                           'requirePasswordChange', 'sendWelcomeEmail', 'verified', 'customFields']:
                    if key == 'roles' and not isinstance(val, list):
                        raise TypeError('The argument \'roles\' must be a \'list\' type')
                    if key == 'customFields' and not isinstance(data[val], dict):
                        raise TypeError('The argument \'customFields\' must be a \'dict\' type')

                    api_body['data'].update({key: val})

            response = self.request('POST', 'users.update', api_body)
        else:
            raise TypeError('The argument \'data\' must be a \'dict\' type')

        if response.status in [200, 400]:
            return response.data

        return False


def users_setAvatar(self, avatar_url, username=None, user_id=None):
    if self.is_connected:
        api_body = {'avatarUrl': avatar_url}

        if username:
            api_body.update({'username': username})
        elif user_id:
            api_body.update({'userId': user_id})

        response = self.request('POST', 'users.setAvatar', api_body)

        if response.status in [200, 400]:
            return response.data

    return False


def users_info(self, user_id=None, username=None):
    if self.is_connected:
        if user_id:
            query = {'userId': user_id}
        elif username:
            query = {'username': username}
        else:
            return False

        response = self.request('GET', 'users.info?{}'.format(urlencode(query)))

        if response.status in [200, 400]:
            return response.data

    return False


def users_list(self, fields=None, query=None):
    if self.is_connected:
        api_query = {}

        if fields:
            if not isinstance(fields, dict):
                raise TypeError('The argument \'fields\' must be a \'dict\' type')

            api_query['fields'] = json.dumps(fields)

        if query:
            if not isinstance(query, dict):
                raise TypeError('The argument \'query\' must be a \'dict\' type')

            if api_query != '':
                api_query['query'] = json.dumps(query)
            else:
                api_query['query'] = json.dumps(query)

        response = self.request('GET', 'users.list{}'.format('?' + urlencode(api_query)))

        if response.status in [200, 400]:
            return response.data

    return False


def users_delete(self, user_id):
    if self.is_connected:
        if user_id:
            api_query = {'userId': user_id}
            response = self.request('POST', 'users.delete', api_query)

            if response.status in [200, 400]:
                return response.data

        return False
