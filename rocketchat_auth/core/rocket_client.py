"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.http.request import HttpRequest
from django.contrib.auth.models import User
from django.conf import settings
from urllib.parse import urlparse
import http.client
import json
import ssl
import _ssl


def method_maker(self, ext_func):
    """
    Декоратор, передающий сторонней функции контекст объекта,
    что делает эту функцию подобно методу класса
    """
    def func_decorator(func):
        def wrapper(*args, **kwargs):
            return func(self, ext_func, *args, **kwargs)
        return wrapper
    return func_decorator


class LoadMethodsError(Exception):
    pass


class RocketApiResponse:
    __response = None
    __data = None

    def __init__(self, response):
        if isinstance(response, http.client.HTTPResponse):
            self.__response = response
            data = response.read()
            data = data.decode('utf-8') if isinstance(data, bytes) else data

            try:
                self.__data = json.loads(data)
            except json.JSONDecodeError:
                self.__data = data
        else:
            raise TypeError('{}: The argument must be an instance of {}'.format(
                self.__class__.__name__,
                http.client.HTTPResponse.__name__
            ))

    def __str__(self):
        return '<{}: {}>'.format(self.__class__.__name__, self.__data)

    @property
    def status(self):
        return self.__response.status

    @property
    def reason(self):
        return self.__response.reason

    @property
    def response(self):
        return self.__response

    @property
    def data(self):
        return self.__data


class RocketClient:
    __tokens = None
    __methods = None
    __rocket_url = None
    __api_version = None
    __connector = None

    def __init__(self, api_version='1', *args, **kwargs):
        self.__api_version = api_version

        # Подгружаем методы для api Rocket.Chat
        if self.__load_methods__():
            pass
        else:
            # Бросаем исключение, если не удалось
            # загрузить ни одного метода для версии API
            raise LoadMethodsError('{}: Could not load any methods for API version \'{}\''.format(
                self.__class__.__name__,
                self.api_version
            ))

        if 'ROCKETCHAT_LAN' in dir(settings):
            url = getattr(settings, 'ROCKETCHAT_LAN')
        else:
            url = settings.ROCKETCHAT

        url = urlparse(url[:-1] if url.endswith('/') else url)
        self.__rocket_url = urlparse('{url.scheme}://{url.netloc}{url.path}/api/v{api_v}'.format(
            url=url, api_v=self.api_version))

        super().__init__(*args, **kwargs)

    def __load_methods__(self):
        try:
            app, core = __name__.split('.')[:2]
            api_module = __import__('{}.{}.rocket_api.v{}'.format(app, core, self.api_version), globals(), locals())
            api_module = getattr(api_module.core.rocket_api, 'v{}'.format(self.api_version))

            self.__methods = {}

            for cur in dir(api_module):
                p = getattr(api_module, cur)
                if callable(p):

                    self.__methods.update({p.__name__: p})

            return bool(len(self.__methods))
        except ImportError:
            pass

        return False

    @property
    def api_url(self):
        return self.__rocket_url

    @property
    def connector(self):
        if self.__connector:
            return self.__connector

        if self.__rocket_url.scheme.upper() == 'HTTPS':
            if 'ROCKETCHAT_SSL_VERIFY' in dir(settings) and not getattr(settings, 'ROCKETCHAT_SSL_VERIFY'):
                context = ssl.create_default_context()
                context.check_hostname = False
                context.verify_mode = _ssl.CERT_NONE
            else:
                context = None

            self.__connector = http.client.HTTPSConnection(self.__rocket_url.netloc.split(':')[0],
                                                           self.__rocket_url.port, timeout=10,
                                                           context=context)
        else:
            self.__connector = http.client.HTTPConnection(self.__rocket_url.netloc.split(':')[0],
                                                          self.__rocket_url.port, timeout=10)
        return self.__connector

    def request(self, method, api_path, body=None, headers=None):
        conn = self.connector

        if self.is_connected:
            api_headers = {'x-auth-token': self.__tokens['token'],
                           'x-user-id': self.__tokens['user_id']}
        else:
            api_headers = {}

        if headers:
            if isinstance(headers, dict):
                for k,v in headers.items():
                    api_headers.update({k.lower(): v})
            else:
                raise TypeError('{}: The argument \'headers\' must be an instance of \'dict\''.format(
                    self.__class__.__name__,
                ))

        if body:
            if isinstance(body, dict):
                api_body = json.dumps(body)
                if 'content-type' not in api_headers:
                    api_headers['content-type'] = 'application/json'
            elif isinstance(body, str):
                api_body = body
            else:
                raise TypeError('{}: The argument \'body\' must be an instance of \'dict\' or \'str\''.format(
                    self.__class__.__name__,
                ))
        else:
            api_body = None

        conn.request(method, self.__rocket_url.path + '/{}'.format(api_path), api_body, headers=api_headers)
        return RocketApiResponse(conn.getresponse())

    def __getattr__(self, item):
        if item in self.__methods:
            # Декорируем функцию, превращая её в метод
            @method_maker(self, self.__methods[item])
            def run_method(obj, ext_func, *args, **kwargs):
                return ext_func(obj, *args, **kwargs)
            return run_method

        return super().__getattribute__(item)

    @property
    def is_connected(self):
        return bool(self.__tokens and len(self.__tokens)) and bool(self.__methods and len(self.__methods))

    @property
    def api_version(self):
        return self.__api_version

    def connect(self, arg, password=None):
        # Определяем модель пользователя

        if dir(settings).index('AUTH_USER_MODEL') >= 0:
            try:
                app_name, model_name = settings.AUTH_USER_MODEL.split('.')
                module = __import__(app_name, globals(), locals(), [])
                module = getattr(module, 'models')
                user_model = getattr(module, model_name)
            except (ImportError, AttributeError):
                user_model = User
        else:
            user_model = User

        # Определяем имя пользователя, исходя из переденного объекта
        if isinstance(arg, user_model):
            # Если объект - это пользователь
            user = getattr(arg, 'username')
            passwd = password if password else getattr(arg, 'password')
        elif isinstance(arg, str):
            # Если объект - строка, то используем как логин
            user = arg
            passwd = password
        elif isinstance(arg, HttpRequest):
            # Если объект - HttpRequest
            user = arg.user.username
            passwd = password if password else arg.user.password
        else:
            raise TypeError('{}: The argument must be an instance of {} or {}'.format(
                self.__class__.__name__,
                user_model.__name__,
                HttpRequest.__name__
            ))

        response = getattr(self, 'login')(user, passwd)
        if isinstance(response, dict):
            self.__tokens = response
            return True
        return False

    def connectByAuthToken(self, user_id, auth_token):
        self.__tokens = {'user_id': user_id, 'token': auth_token}
        response = self.users_info(user_id=user_id)

        if response and response['success']:
            return True
        else:
            self.__tokens = None

        return False

    def connectAsAdmin(self):
        if 'ROCKETCHAT_ADMIN_USERNAME' in dir(settings) and 'ROCKETCHAT_ADMIN_PASSWORD' in dir(settings):
            return self.connect(settings.ROCKETCHAT_ADMIN_USERNAME, settings.ROCKETCHAT_ADMIN_PASSWORD)
        return False

    def disconnect(self):
        if self.is_connected:
            logout = getattr(self, 'logout')

            try:
                logout()
            except:
                pass

            self.__tokens = None
            self.__connector.close()
            return True
        return False
