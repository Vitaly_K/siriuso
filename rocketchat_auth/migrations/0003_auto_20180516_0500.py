# Generated by Django 2.0.5 on 2018-05-16 05:00

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('rocketchat_auth', '0002_rocketuser_auth_token'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rocketuser',
            name='uzanto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL, unique=True, verbose_name='Uzanto'),
        ),
    ]
