"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.db import models
from main.models import Uzanto
from uuid import uuid4
from django.utils.translation import ugettext_lazy as _


class RocketUser(models.Model):

    # UUID записи
    uuid = models.UUIDField(_('UUID'), default=uuid4, primary_key=True, editable=False)
    # ID пользователя Siriuso
    uzanto = models.OneToOneField(Uzanto, verbose_name=_('Uzanto'), blank=False, on_delete=models.CASCADE)
    # ID ползователя Rocket.Chat
    rocket_user = models.CharField(_('Rocket.Chat user ID'), max_length=32, blank=False)
    # Дата время последнего изменения
    last_update_date = models.DateTimeField(_('Last update date'), auto_now=True, blank=True, null=True)
    # Пароль для пользователя Rocket.Chat
    password = models.CharField(_('Password'), max_length=32, blank=False)
    # Текущий токен для браузера
    auth_token = models.CharField(_('Auth Token'), max_length=64, blank=True, null=True)

    class Meta:
        db_table = 'rocketchat_users'
        verbose_name = _('Rocket.Chat user')
        verbose_name_plural = _('Rocket.Chat users')
