"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.shortcuts import render, redirect
from django.conf import settings
from django.contrib.auth import authenticate
from django.http import HttpResponse, JsonResponse, HttpResponseServerError
from django.views.decorators.csrf import csrf_exempt

from .core import RocketClient
from .core.common import get_avatar
from .models import RocketUser

import hashlib
import bcrypt
import string
import random
import json
import re

from .decorators import cors_allow_credentials


def home(request):
    return render(request, 'home.html')


def redirect_rocketchat(request):
    return redirect(settings.ROCKETCHAT)


def generate_password():
    n = 16
    chars = string.ascii_letters + string.digits
    random_id = ''.join(random.SystemRandom().choice(chars) for _ in range(n))

    return random_id


@csrf_exempt
@cors_allow_credentials()
def api(request):
    """
    Implements API for Rocket.Chat IFrame authentication
    """
    if not request.user.is_authenticated:
        return HttpResponse(status=401)

    rocket = RocketClient()

    if rocket.connectAsAdmin():
        try:
            user = RocketUser.objects.get(uzanto=request.user)

            if isinstance(user.auth_token, str) and len(user.auth_token):
                rocket_con = RocketClient()
                rocket_con.connectByAuthToken(user.rocket_user, user.auth_token)
                response = rocket_con.users_info(user_id=user.rocket_user)

                if response and response['success']:
                    rocket.disconnect()

                    return JsonResponse({
                        'loginToken': user.auth_token,
                    })

        except RocketUser.DoesNotExist:
            fullname = ' '.join([request.user.first_name, request.user.last_name]).strip()
            password = generate_password()
            response = rocket.users_create(request.user.chefa_retposhto, fullname, password, str(request.user.id))

            if response['success']:
                new_user = RocketUser(uzanto=request.user, rocket_user=response['user']['_id'], password=password)
                new_user.save()

                rocket.users_setAvatar(get_avatar(request.user, request))
                user = RocketUser.objects.get(uzanto=request.user)

            else:
                return HttpResponseServerError()

        response = rocket.login(user.uzanto.chefa_retposhto, user.password)
        rocket.disconnect()

        if response:
            user.auth_token = response['token']
            user.save()

            return JsonResponse({
                'loginToken': response['token'],
            })
        else:
            return HttpResponse(status=401)

    return HttpResponseServerError()


@csrf_exempt
@cors_allow_credentials()
def api_login(request):

    if request.method == 'POST' and request.body:
        body = request.body.decode('utf-8')

        if 'CONTENT_TYPE' in request.META and re.search(r'application/json', request.META['CONTENT_TYPE'].lower()):
            try:
                body = json.loads(body)
            except json.JSONDecodeError:
                body = None
        else:
            temp = body.split('&')
            body = {}

            for cur in temp:
                pair = cur.split('=')
                body[pair[0]] = pair[1]

            body = body if len(body) else None

        if isinstance(body, dict):
            for key in body.keys():
                if key not in ['user', 'username', 'password']:
                    body = None
                    break

        if body:
            uzanto = authenticate(username=(body['user'] if 'user' in body else body['username']),
                                password=body['password'])
            if uzanto:
                rocket = RocketClient()

                try:
                    user = RocketUser.objects.get(uzanto=uzanto)
                except RocketUser.DoesNotExist:
                    return JsonResponse({'status': 'error', 'error': 'Unauthorized', 'message': 'Unauthorized'},
                                        status=401)

                result = rocket.login(str(uzanto.id), user.password)

                # Синхронизация пароля в случае, если пользователь умудрился установить свой пароль
                if not result:
                    admin = RocketClient()

                    if admin.connectAsAdmin():
                        if admin.users_update(user.rocket_user, {'password': user.password}):
                            result = rocket.login(str(uzanto.id), user.password)

                        admin.disconnect()
                # ----------------------------------------------------------------------------------

                if result:
                    response = {'status': 'success',
                                'data': {
                                    'authToken': result['token'],
                                    'userId': result['user_id']
                                }}
                    return JsonResponse(response)

                return JsonResponse({'status': 'error', 'error': 'Unauthorized', 'message': 'Unauthorized'},
                                    status=401)

    return JsonResponse({'status': 'error', 'error': 400, 'message': 'Unrecognized options for login request'},
                        status=400)
