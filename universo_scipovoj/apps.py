from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UniversoScipovojConfig(AppConfig):
    name = 'universo_scipovoj'
    verbose_name = _('Scipovoj de Universo')
