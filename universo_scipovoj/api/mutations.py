"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
16.05.2019
"""
from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from universo_uzantoj.models import UniversoUzanto
from .schema import *
from ..models import *


# Модель категорий навыков Универсо
class RedaktuUniversoScipovoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_scipovoj_kategorioj = graphene.Field(UniversoScipovoKategorioNode,
        description=_('Созданная/изменённая категория навыков в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_scipovoj_kategorioj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_scipovoj.povas_krei_universo_scipovoj_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_scipovoj_kategorioj = UniversoScipovoKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_scipovoj_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_scipovoj_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_scipovoj_kategorioj.realeco.set(realeco)
                                else:
                                    universo_scipovoj_kategorioj.realeco.clear()

                            universo_scipovoj_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_scipovoj_kategorioj = UniversoScipovoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_scipovoj.povas_forigi_universo_scipovoj_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_scipovoj.povas_shanghi_universo_scipovoj_kategorioj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_scipovoj_kategorioj.forigo = kwargs.get('forigo', universo_scipovoj_kategorioj.forigo)
                                universo_scipovoj_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_scipovoj_kategorioj.arkivo = kwargs.get('arkivo', universo_scipovoj_kategorioj.arkivo)
                                universo_scipovoj_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_scipovoj_kategorioj.publikigo = kwargs.get('publikigo', universo_scipovoj_kategorioj.publikigo)
                                universo_scipovoj_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_scipovoj_kategorioj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_scipovoj_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_scipovoj_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_scipovoj_kategorioj.realeco.set(realeco)
                                    else:
                                        universo_scipovoj_kategorioj.realeco.clear()

                                universo_scipovoj_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoScipovoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoScipovoKategorio(status=status, message=message, universo_scipovoj_kategorioj=universo_scipovoj_kategorioj)


# Модель типов навыков Универсо
class RedaktuUniversoScipovoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_scipovoj_tipoj = graphene.Field(UniversoScipovoTipoNode, 
        description=_('Созданный/изменённый тип навыков в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_scipovoj_tipoj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_scipovoj.povas_krei_universo_scipovoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_scipovoj_tipoj = UniversoScipovoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_scipovoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_scipovoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_scipovoj_tipoj.realeco.set(realeco)
                                else:
                                    universo_scipovoj_tipoj.realeco.clear()

                            universo_scipovoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_scipovoj_tipoj = UniversoScipovoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_scipovoj.povas_forigi_universo_scipovoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_scipovoj.povas_shanghi_universo_scipovoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_scipovoj_tipoj.forigo = kwargs.get('forigo', universo_scipovoj_tipoj.forigo)
                                universo_scipovoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_scipovoj_tipoj.arkivo = kwargs.get('arkivo', universo_scipovoj_tipoj.arkivo)
                                universo_scipovoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_scipovoj_tipoj.publikigo = kwargs.get('publikigo', universo_scipovoj_tipoj.publikigo)
                                universo_scipovoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_scipovoj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_scipovoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_scipovoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_scipovoj_tipoj.realeco.set(realeco)
                                    else:
                                        universo_scipovoj_tipoj.realeco.clear()

                                universo_scipovoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoScipovoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoScipovoTipo(status=status, message=message, universo_scipovoj_tipoj=universo_scipovoj_tipoj)


# Модель уровней навыков Универсо
class RedaktuUniversoScipovoNivelo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_scipovoj_tipoj = graphene.Field(UniversoScipovoNiveloNode, 
        description=_('Созданный/изменённый уровень навыков Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nivelo = graphene.Int(description=_('Значение уровня'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_scipovoj_tipoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_scipovoj.povas_krei_universo_scipovoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_scipovoj_tipoj = UniversoScipovoNivelo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                nivelo = kwargs.get('nivelo', 0),
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            universo_scipovoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nivelo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_scipovoj_tipoj = UniversoScipovoNivelo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_scipovoj.povas_forigi_universo_scipovoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_scipovoj.povas_shanghi_universo_scipovoj_tipoj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_scipovoj_tipoj.forigo = kwargs.get('forigo', universo_scipovoj_tipoj.forigo)
                                universo_scipovoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_scipovoj_tipoj.arkivo = kwargs.get('arkivo', universo_scipovoj_tipoj.arkivo)
                                universo_scipovoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_scipovoj_tipoj.publikigo = kwargs.get('publikigo', universo_scipovoj_tipoj.publikigo)
                                universo_scipovoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_scipovoj_tipoj.autoro = autoro
                                universo_scipovoj_tipoj.nivelo = kwargs.get('nivelo', universo_scipovoj_tipoj.nivelo)

                                universo_scipovoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoScipovoNivelo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoScipovoNivelo(status=status, message=message, universo_scipovoj_tipoj=universo_scipovoj_tipoj)


# Модель навыков Универсо
class RedaktuUniversoScipovo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_scipovoj = graphene.Field(UniversoScipovoNode, 
        description=_('Созданный/изменённый навык Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий навыков Универсо'))
        tipo_id = graphene.Int(description=_('Код типа навыков Универсо'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_scipovoj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        kategorio = UniversoScipovoKategorio.objects.none()
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_scipovoj.povas_krei_universo_scipovoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одно значение'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            if 'kategorio' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                            elif not len(kwargs.get('kategorio')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одно значение'),'kategorio')
                            else:
                                kategorio_id = set(kwargs.get('kategorio'))

                                if len(kategorio_id):
                                    kategorio = UniversoScipovoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие категория навыков Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoScipovoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoScipovoTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип навыков Универсо'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип навыков Универсо'),'tipo_id')

                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_scipovoj = UniversoScipovo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                tipo = tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_scipovoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_scipovoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_scipovoj.realeco.set(realeco)
                                else:
                                    universo_scipovoj.realeco.clear()

                            if 'kategorio' in kwargs:
                                if kategorio:
                                    universo_scipovoj.kategorio.set(kategorio)
                                else:
                                    universo_scipovoj.kategorio.clear()

                            universo_scipovoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False) or kwargs.get('kategorio', False)
                            or kwargs.get('tipo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_scipovoj = UniversoScipovo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_scipovoj.povas_forigi_universo_scipovoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_scipovoj.povas_shanghi_universo_scipovoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))

                                    if len(kategorio_id):
                                        kategorio = UniversoScipovoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категория навыков Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoScipovoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoScipovoTipo.DoesNotExist:
                                        message = _('Неверный тип навыков Универсо')
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_scipovoj.forigo = kwargs.get('forigo', universo_scipovoj.forigo)
                                universo_scipovoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_scipovoj.arkivo = kwargs.get('arkivo', universo_scipovoj.arkivo)
                                universo_scipovoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_scipovoj.publikigo = kwargs.get('publikigo', universo_scipovoj.publikigo)
                                universo_scipovoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_scipovoj.autoro = autoro
                                universo_scipovoj.tipo = tipo if kwargs.get('tipo_id', False) else universo_scipovoj.tipo

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_scipovoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_scipovoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_scipovoj.realeco.set(realeco)
                                    else:
                                        universo_scipovoj.realeco.clear()
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        universo_scipovoj.kategorio.set(kategorio)
                                    else:
                                        universo_scipovoj.kategorio.clear()

                                universo_scipovoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoScipovo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoScipovo(status=status, message=message, universo_scipovoj=universo_scipovoj)


# Модель навыков объектов Универсо
class RedaktuUniversoScipovoObjekto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_scipovoj_objektoj = graphene.Field(UniversoScipovoObjektoNode, 
        description=_('Созданный/изменённый навык объекта Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        objekto_uuid = graphene.String(description=_('UUID объекта Универсо'))
        nivelo_uuid = graphene.String(description=_('UUID уровня навыка Универсо'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        scipovo_id = graphene.Int(description=_('ID навыка Универсо'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_scipovoj_objektoj = None
        autoro = None
        objekto = None
        nivelo = None
        scipovo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_scipovoj.povas_krei_universo_scipovoj_objektoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'scipovo_id' in kwargs:
                                try:
                                    scipovo = UniversoScipovo.objects.get(id=kwargs.get('scipovo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoScipovo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный навык Универсо'),'scipovo_id')
                            else:
                                message = '{} "{}"'.format(_('Не указан навык Универсо'),'scipovo_id')

                        if not message:
                            if 'objekto_uuid' in kwargs:
                                try:
                                    objekto = UniversoObjekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoObjekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект Универсо'),'objekto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Не указан объект Универсо'),'objekto_uuid')

                        if not message:
                            if 'nivelo_uuid' in kwargs:
                                try:
                                    nivelo = UniversoScipovoNivelo.objects.get(uuid=kwargs.get('nivelo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoScipovoNivelo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный уровень навыка Универсо'),'nivelo_uuid')
                            else:
                                message = '{} "{}"'.format(_('Не указан уровень навыка Универсо'),'nivelo_uuid')

                        if not message:
                            universo_scipovoj_objektoj = UniversoScipovoObjekto.objects.create(
                                forigo=False,
                                arkivo=False,
                                objekto = objekto,
                                scipovo = scipovo,
                                nivelo = nivelo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            universo_scipovoj_objektoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('scipovo_id', False) or kwargs.get('nivelo_uuid', False)
                            or kwargs.get('objekto_uuid', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_scipovoj_objektoj = UniversoScipovoObjekto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_scipovoj.povas_forigi_universo_scipovoj_objektoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_scipovoj.povas_shanghi_universo_scipovoj_objektoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'scipovo_id' in kwargs:
                                    try:
                                        scipovo = UniversoScipovo.objects.get(id=kwargs.get('scipovo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoScipovo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный навык Универсо'),'scipovo_id')

                            if not message:
                                if 'objekto_uuid' in kwargs:
                                    try:
                                        objekto = UniversoObjekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoObjekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект Универсо'),'objekto_uuid')

                            if not message:
                                if 'nivelo_uuid' in kwargs:
                                    try:
                                        nivelo = UniversoScipovoNivelo.objects.get(uuid=kwargs.get('nivelo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoScipovoNivelo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный уровень навыка Универсо'),'nivelo_uuid')

                            if not message:

                                universo_scipovoj_objektoj.forigo = kwargs.get('forigo', universo_scipovoj_objektoj.forigo)
                                universo_scipovoj_objektoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_scipovoj_objektoj.arkivo = kwargs.get('arkivo', universo_scipovoj_objektoj.arkivo)
                                universo_scipovoj_objektoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_scipovoj_objektoj.publikigo = kwargs.get('publikigo', universo_scipovoj_objektoj.publikigo)
                                universo_scipovoj_objektoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_scipovoj_objektoj.scipovo = scipovo if kwargs.get('scipovo_id', False) else universo_scipovoj_objektoj.scipovo
                                universo_scipovoj_objektoj.objekto = objekto if kwargs.get('objekto_uuid', False) else universo_scipovoj_objektoj.objekto
                                universo_scipovoj_objektoj.nivelo = nivelo if kwargs.get('nivelo_uuid', False) else universo_scipovoj_objektoj.nivelo

                                universo_scipovoj_objektoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoScipovoObjekto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoScipovoObjekto(status=status, message=message, universo_scipovoj_objektoj=universo_scipovoj_objektoj)


class UniversoScipovoMutations(graphene.ObjectType):
    redaktu_universo_scipovo_kategorio = RedaktuUniversoScipovoKategorio.Field(
        description=_('''Создаёт или редактирует категории навыков Универсо''')
    )
    redaktu_universo_scipovo_tipo = RedaktuUniversoScipovoTipo.Field(
        description=_('''Создаёт или редактирует типы навыков Универсо''')
    )
    redaktu_universo_scipovo_stokejoj_tipo = RedaktuUniversoScipovoNivelo.Field(
        description=_('''Создаёт или редактирует уровни навыков Универсо''')
    )
    redaktu_universo_scipovo = RedaktuUniversoScipovo.Field(
        description=_('''Создаёт или редактирует навыки Универсо''')
    )
    redaktu_universo_scipovo_objekto = RedaktuUniversoScipovoObjekto.Field(
        description=_('''Создаёт или редактирует навыки объектов Универсо''')
    )
