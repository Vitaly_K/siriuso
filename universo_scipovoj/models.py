"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres import fields as pgfields
from django.contrib.auth.models import Permission
from django.db.models import Max, Q

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from universo_bazo.models import UniversoBazaMaks, UniversoBazaRealeco, UniversoRealeco
from universo_objektoj.models import UniversoObjekto
from universo_uzantoj.models import UniversoUzanto


# Категории навыков Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoScipovoKategorio(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_scipovoj_kategorioj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_scipovoj_kategorioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio de scipovoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj de scipovoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_scipovoj_kategorioj', _('Povas vidi kategorioj de scipovoj de Universo')),
            ('povas_krei_universo_scipovoj_kategorioj', _('Povas krei kategorioj de scipovoj de Universo')),
            ('povas_forigi_universo_scipovoj_kategorioj', _('Povas forigu kategorioj de scipovoj de Universo')),
            ('povas_shanghi_universo_scipovoj_kategorioj', _('Povas ŝanĝi kategorioj de scipovoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoScipovoKategorio, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                                   update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_scipovoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_scipovoj.povas_vidi_universo_scipovoj_kategorioj',
                'universo_scipovoj.povas_krei_universo_scipovoj_kategorioj',
                'universo_scipovoj.povas_forigi_universo_scipovoj_kategorioj',
                'universo_scipovoj.povas_shanghi_universo_scipovoj_kategorioj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_scipovoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_scipovoj.povas_vidi_universo_scipovoj_kategorioj')
                    or user_obj.has_perm('universo_scipovoj.povas_vidi_universo_scipovoj_kategorioj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_scipovoj.povas_vidi_universo_scipovoj_kategorioj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы навыков Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoScipovoTipo(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_scipovoj_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_scipovoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de scipovoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de scipovoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_scipovoj_tipoj', _('Povas vidi tipoj de scipovoj de Universo')),
            ('povas_krei_universo_scipovoj_tipoj', _('Povas krei tipoj de scipovoj de Universo')),
            ('povas_forigi_universo_scipovoj_tipoj', _('Povas forigu tipoj de scipovoj de Universo')),
            ('povas_shanghi_universo_scipovoj_tipoj', _('Povas ŝanĝi tipoj de scipovoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoScipovoTipo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                              update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_scipovoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_scipovoj.povas_vidi_universo_scipovoj_tipoj',
                'universo_scipovoj.povas_krei_universo_scipovoj_tipoj',
                'universo_scipovoj.povas_forigi_universo_scipovoj_tipoj',
                'universo_scipovoj.povas_shanghi_universo_scipovoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_scipovoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_scipovoj.povas_vidi_universo_scipovoj_tipoj')
                    or user_obj.has_perm('universo_scipovoj.povas_vidi_universo_scipovoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_scipovoj.povas_vidi_universo_scipovoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Уровни навыков Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoScipovoNivelo(UniversoBazaMaks):

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # значение уровня
    nivelo = models.IntegerField(_('Nivelo'), unique=True, default=0)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_scipovoj_niveloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Nivelo de scipovoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Niveloj de scipovoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_scipovoj_niveloj', _('Povas vidi nivelo de scipovoj de universo')),
            ('povas_krei_universo_scipovoj_niveloj', _('Povas krei nivelo de scipovoj de universo')),
            ('povas_forigi_universo_scipovoj_niveloj', _('Povas forigu nivelo de scipovoj de universo')),
            ('povas_shanghi_universo_scipovoj_niveloj', _('Povas ŝanĝi nivelo de scipovoj de universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nivelo этой модели
        return '{}'.format(self.nivelo)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_scipovoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_scipovoj.povas_vidi_universo_scipovoj_niveloj',
                'universo_scipovoj.povas_krei_universo_scipovoj_nivelo',
                'universo_scipovoj.povas_forigi_universo_scipovoj_niveloj',
                'universo_scipovoj.povas_shanghi_universo_scipovoj_niveloj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_scipovoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_scipovoj.povas_vidi_universo_scipovoj_niveloj')
                    or user_obj.has_perm('universo_scipovoj.povas_vidi_universo_scipovoj_niveloj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_scipovoj.povas_vidi_universo_scipovoj_niveloj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Справочник навыков Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoScipovo(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # категория навыков Универсо
    kategorio = models.ManyToManyField(UniversoScipovoKategorio, verbose_name=_('Kategorio'),
                                       db_table='universo_scipovoj_kategorioj_ligiloj')

    # тип навыков Универсо
    tipo = models.ForeignKey(UniversoScipovoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_scipovoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_scipovoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Scipovo de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Scipovoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_scipovoj', _('Povas vidi scipovoj de Universo')),
            ('povas_krei_universo_scipovoj', _('Povas krei scipovoj de Universo')),
            ('povas_forigi_universo_scipovo', _('Povas forigu scipovoj de Universo')),
            ('povas_shanghi_universo_scipovoj', _('Povas ŝanĝi scipovoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoScipovo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                              update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_scipovoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_scipovoj.povas_vidi_universo_scipovoj',
                'universo_scipovoj.povas_krei_universo_scipovoj',
                'universo_scipovoj.povas_forigi_universo_scipovoj',
                'universo_scipovoj.povas_shanghi_universo_scipovoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_scipovoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_scipovoj.povas_vidi_universo_scipovoj')
                    or user_obj.has_perm('universo_scipovoj.povas_vidi_universo_scipovoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_scipovoj.povas_vidi_universo_scipovo'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Навыки объектов, касается роботов-аватаров и не только, использует абстрактный класс UniversoBazaRealeco
class UniversoScipovoObjekto(UniversoBazaRealeco):

    # объект Универсо
    objekto = models.ForeignKey(UniversoObjekto, verbose_name=_('Objekto'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # навык Универсо
    scipovo = models.ForeignKey(UniversoScipovo, verbose_name=_('Scipovo'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # уровень навыка Универсо
    nivelo = models.ForeignKey(UniversoScipovoNivelo, verbose_name=_('Nivelo'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_scipovoj_objekto'
        # читабельное название модели, в единственном числе
        verbose_name = _('Scipovo de objektoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Scipovoj de objektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_scipovoj_objektoj', _('Povas vidi scipovoj de objektoj de Universo')),
            ('povas_krei_universo_scipovoj_objektoj', _('Povas krei scipovoj de objektoj de Universo')),
            ('povas_forigi_universo_scipovo_objektoj', _('Povas forigu scipovoj de objektoj de Universo')),
            ('povas_shanghi_universo_scipovoj_objektoj', _('Povas ŝanĝi scipovoj de objektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле objekto этой модели
        return '{}'.format(self.objekto)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_scipovoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_scipovoj.povas_vidi_universo_scipovoj_objektoj',
                'universo_scipovoj.povas_krei_universo_scipovoj_objektoj',
                'universo_scipovoj.povas_forigi_universo_scipovoj_objektoj',
                'universo_scipovoj.povas_shanghi_universo_scipovoj_objektoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_scipovoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_scipovoj.povas_vidi_universo_scipovoj_objektoj')
                    or user_obj.has_perm('universo_scipovoj.povas_vidi_universo_scipovoj_objektoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_scipovoj.povas_vidi_universo_scipovo_objektoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
