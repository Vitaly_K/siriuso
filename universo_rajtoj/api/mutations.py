"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
16.05.2019
"""
from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from universo_uzantoj.models import UniversoUzanto
from .schema import *
from ..models import *


# Модель категорий прав в Универсо
class RedaktuUniversoRajtoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_rajto_kategorioj = graphene.Field(UniversoRajtoKategorioNode,
        description=_('Созданная/изменённая категория прав в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_rajto_kategorioj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_rajtoj.povas_krei_universo_rajtoj_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_rajto_kategorioj = UniversoRajtoKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_rajto_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_rajto_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_rajto_kategorioj.realeco.set(realeco)
                                else:
                                    universo_rajto_kategorioj.realeco.clear()

                            universo_rajto_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_rajto_kategorioj = UniversoRajtoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_rajtoj.povas_forigi_universo_rajtoj_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_rajtoj.povas_shanghi_universo_rajtoj_kategorioj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_rajto_kategorioj.forigo = kwargs.get('forigo', universo_rajto_kategorioj.forigo)
                                universo_rajto_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_rajto_kategorioj.arkivo = kwargs.get('arkivo', universo_rajto_kategorioj.arkivo)
                                universo_rajto_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_rajto_kategorioj.publikigo = kwargs.get('publikigo', universo_rajto_kategorioj.publikigo)
                                universo_rajto_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_rajto_kategorioj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_rajto_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_rajto_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_rajto_kategorioj.realeco.set(realeco)
                                    else:
                                        universo_rajto_kategorioj.realeco.clear()

                                universo_rajto_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoRajtoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoRajtoKategorio(status=status, message=message, universo_rajto_kategorioj=universo_rajto_kategorioj)


# Модель типы прав в Универсо
class RedaktuUniversoRajtoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_rajto_tipoj = graphene.Field(UniversoRajtoTipoNode,
        description=_('Созданный/изменённый тип прав в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_rajto_tipoj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_rajtoj.povas_krei_universo_rajtoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_rajto_tipoj = UniversoRajtoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_rajto_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_rajto_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_rajto_tipoj.realeco.set(realeco)
                                else:
                                    universo_rajto_tipoj.realeco.clear()

                            universo_rajto_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_rajto_tipoj = UniversoRajtoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_rajtoj.povas_forigi_universo_rajtoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_rajtoj.povas_shanghi_universo_rajtoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_rajto_tipoj.forigo = kwargs.get('forigo', universo_rajto_tipoj.forigo)
                                universo_rajto_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_rajto_tipoj.arkivo = kwargs.get('arkivo', universo_rajto_tipoj.arkivo)
                                universo_rajto_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_rajto_tipoj.publikigo = kwargs.get('publikigo', universo_rajto_tipoj.publikigo)
                                universo_rajto_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_rajto_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_rajto_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_rajto_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_rajto_tipoj.realeco.set(realeco)
                                    else:
                                        universo_rajto_tipoj.realeco.clear()

                                universo_rajto_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoRajtoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoRajtoTipo(status=status, message=message, universo_rajto_tipoj=universo_rajto_tipoj)


# Модель прав в Универсо
class RedaktuUniversoRajto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_rajto = graphene.Field(UniversoRajtoNode,
        description=_('Созданные/изменённые права в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий прав Универсо'))
        tipo_id = graphene.Int(description=_('Тип права Универсо'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_rajto = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user
        kategorio = UniversoRajtoKategorio.objects.none()

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_rajtoj.povas_krei_universo_rajtoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        if not message:
                            if 'kategorio' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                            elif not len(kwargs.get('kategorio')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'kategorio')
                            else:
                                kategorio_id = set(kwargs.get('kategorio'))

                                if len(kategorio_id):
                                    kategorio = UniversoRajtoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')
                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoRajtoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoRajtoTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип прав Универсо'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            universo_rajto = UniversoRajto.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                tipo = tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_rajto.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_rajto.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_rajto.realeco.set(realeco)
                                else:
                                    universo_rajto.realeco.clear()

                            if 'kategorio' in kwargs:
                                if kategorio:
                                    universo_rajto.kategorio.set(kategorio)
                                else:
                                    universo_rajto.kategorio.clear()

                            universo_rajto.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False) or kwargs.get('kategorio', False)
                            or kwargs.get('tipo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_rajto = UniversoRajto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_rajtoj.povas_forigi_universo_rajtoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_rajtoj.povas_shanghi_universo_rajtoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))

                                    if len(kategorio_id):
                                        kategorio = UniversoRajtoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoRajtoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoRajtoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип прав Универсо'),'tipo_id')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')
                            if not message:

                                universo_rajto.forigo = kwargs.get('forigo', universo_rajto.forigo)
                                universo_rajto.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_rajto.arkivo = kwargs.get('arkivo', universo_rajto.arkivo)
                                universo_rajto.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_rajto.publikigo = kwargs.get('publikigo', universo_rajto.publikigo)
                                universo_rajto.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_rajto.tipo = tipo if kwargs.get('tipo_id', False) else universo_rajto.tipo
                                universo_rajto.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_rajto.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_rajto.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_rajto.realeco.set(realeco)
                                    else:
                                        universo_rajto.realeco.clear()
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        universo_rajto.kategorio.set(kategorio)
                                    else:
                                        universo_rajto.kategorio.clear()

                                universo_rajto.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoRajto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoRajto(status=status, message=message, universo_rajto=universo_rajto)


# Модель наделение правами в Универсо
class RedaktuUniversoRajtoRajtigo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_rajto_rajtigo = graphene.Field(UniversoRajtoRajtigoNode,
        description=_('Созданные/изменённые наделение правами в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        rajto_id = graphene.Int(description=_('Право Универсо'))
        posedanto_uzanto_siriuso_uzanto_id = graphene.Int(description=_('Пользователь получивший право Универсо'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация получившая право Универсо'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_rajto_rajtigo = None
        rajto = None
        posedanto_uzanto = None
        posedanto_organizo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_rajto.povas_krei_universo_rajto_rajtigoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_uzanto_siriuso_uzanto_id' in kwargs:
                                try:
                                    posedanto_uzanto = UniversoUzanto.objects.get(
                                        siriuso_uzanto=kwargs.get('posedanto_uzanto_siriuso_uzanto_id'), 
                                        publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_uzanto_siriuso_uzanto_id')
                        if not message:
                            if 'rajto_id' in kwargs:
                                try:
                                    rajto = UniversoRajto.objects.get(id=kwargs.get('rajto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoRajto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверные права Универсо'),'rajto_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'rajto_id')
                        if not message:
                            if 'posedanto_organizo_uuid' in kwargs:
                                try:
                                    posedanto_organizo = UniversoOrganizo.objects.get(uuid=kwargs.get('posedanto_organizo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoOrganizo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная организация Универсо'),'posedanto_organizo_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_organizo_uuid')

                        if not message:
                            universo_rajto_rajtigo = UniversoRajtoRajtigo.objects.create(
                                forigo=False,
                                arkivo=False,
                                rajto=rajto,
                                posedanto_uzanto=posedanto_uzanto,
                                posedanto_organizo=posedanto_organizo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            universo_rajto_rajtigo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto_organizo_uuid', False) or kwargs.get('rajto_id', False)
                            or kwargs.get('posedanto_uzanto_siriuso_uzanto_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_rajto_rajtigo = UniversoRajtoRajtigo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_rajto.povas_forigi_universo_rajto_rajtigoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_rajto.povas_shanghi_universo_rajto_rajtigoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_uzanto_siriuso_uzanto_id' in kwargs:
                                    try:
                                        posedanto_uzanto = UniversoUzanto.objects.get(
                                            siriuso_uzanto=kwargs.get('posedanto_uzanto_siriuso_uzanto_id'), 
                                            publikigo=True)
                                    except UniversoUzanto.DoesNotExist:
                                        message = _('Неверный пользователь Universo')
                            if not message:
                                if 'rajto_id' in kwargs:
                                    try:
                                        rajto = UniversoRajto.objects.get(id=kwargs.get('rajto_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoRajto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверные права Универсо'),'rajto_id')
                            if not message:
                                if 'posedanto_organizo_uuid' in kwargs:
                                    try:
                                        posedanto_organizo = UniversoOrganizo.objects.get(uuid=kwargs.get('posedanto_organizo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoOrganizo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная организация Универсо'),'posedanto_organizo_uuid')
                            if not message:

                                universo_rajto_rajtigo.forigo = kwargs.get('forigo', universo_rajto_rajtigo.forigo)
                                universo_rajto_rajtigo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_rajto_rajtigo.arkivo = kwargs.get('arkivo', universo_rajto_rajtigo.arkivo)
                                universo_rajto_rajtigo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_rajto_rajtigo.publikigo = kwargs.get('publikigo', universo_rajto_rajtigo.publikigo)
                                universo_rajto_rajtigo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_rajto_rajtigo.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_siriuso_uzanto_id', False) else universo_rajto_rajtigo.posedanto_uzanto
                                universo_rajto_rajtigo.rajto = rajto if kwargs.get('rajto_id', False) else universo_rajto_rajtigo.rajto
                                universo_rajto_rajtigo.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) else universo_rajto_rajtigo.posedanto_organizo

                                universo_rajto_rajtigo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoRajtoRajtigo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoRajtoRajtigo(status=status, message=message, universo_rajto_rajtigo=universo_rajto_rajtigo)


class UniversoRajtojMutations(graphene.ObjectType):
    redaktu_universo_rajto_kategorio = RedaktuUniversoRajtoKategorio.Field(
        description=_('''Создаёт или редактирует категории прав в Универсо''')
    )
    redaktu_universo_rajto_tipo = RedaktuUniversoRajtoTipo.Field(
        description=_('''Создаёт или редактирует типы прав в Универсо''')
    )
    redaktu_universo_rajto = RedaktuUniversoRajto.Field(
        description=_('''Создаёт или редактирует права в Универсо''')
    )
    redaktu_universo_rajto_rajtigo = RedaktuUniversoRajtoRajtigo.Field(
        description=_('''Создаёт или редактирует наделение правами в Универсо''')
    )
