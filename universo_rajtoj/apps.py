"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UniversoRajtojConfig(AppConfig):
    name = 'universo_rajtoj'
    verbose_name = _('Rajtoj de Universo')
