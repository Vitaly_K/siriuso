"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres import fields as pgfields
from django.contrib.auth.models import Permission
from django.db.models import Max, Q

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from universo_bazo.models import UniversoBazaMaks, UniversoBazaRealeco, UniversoRealeco
from universo_uzantoj.models import UniversoUzanto
from universo_organizoj.models import UniversoOrganizo


# Категории прав Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoRajtoKategorio(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_rajtoj_kategorioj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_rajtoj_kategorioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio de rajtoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj de rajtoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_rajtoj_kategorioj', _('Povas vidi kategorioj de rajtoj de Universo')),
            ('povas_krei_universo_rajtoj_kategorioj', _('Povas krei kategorioj de rajtoj de Universo')),
            ('povas_forigi_universo_rajtoj_kategorioj', _('Povas forigi kategorioj de rajtoj de Universo')),
            ('povas_shangxi_universo_rajtoj_kategorioj', _('Povas ŝanĝi kategorioj de rajtoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoRajtoKategorio, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_rajtoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_rajtoj.povas_vidi_universo_rajtoj_kategorioj', 
                'universo_rajtoj.povas_krei_universo_rajtoj_kategorioj',
                'universo_rajtoj.povas_forigi_universo_rajtoj_kategorioj',
                'universo_rajtoj.povas_shangxi_universo_rajtoj_kategorioj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_rajtoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_rajtoj.povas_vidi_universo_rajtoj_kategorioj')
                    or user_obj.has_perm('universo_rajtoj.povas_vidi_universo_rajtoj_kategorioj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_rajtoj.povas_vidi_universo_rajtoj_kategorioj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы прав в Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoRajtoTipo(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_rajtoj_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_rajtoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de rajtoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de rajtoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_rajtoj_tipoj', _('Povas vidi tipoj de rajtoj de Universo')),
            ('povas_krei_universo_rajtoj_tipoj', _('Povas krei tipoj de rajtoj de Universo')),
            ('povas_forigi_universo_rajtoj_tipoj', _('Povas forigi tipoj de rajtoj de Universo')),
            ('povas_shangxi_universo_rajtoj_tipoj', _('Povas ŝanĝi tipoj de rajtoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoRajtoTipo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_rajtoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_rajtoj.povas_vidi_universo_rajtoj_tipoj', 
                'universo_rajtoj.povas_krei_universo_rajtoj_tipoj',
                'universo_rajtoj.povas_forigi_universo_rajtoj_tipoj',
                'universo_rajtoj.povas_shangxi_universo_rajtoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_rajtoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_rajtoj.povas_vidi_universo_rajtoj_tipoj')
                    or user_obj.has_perm('universo_rajtoj.povas_vidi_universo_rajtoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_rajtoj.povas_vidi_universo_rajtoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Справочник прав в Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoRajto(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # категория прав Универсо
    kategorio = models.ManyToManyField(UniversoRajtoKategorio, verbose_name=_('Kategorio'),
                                       db_table='universo_rajtoj_kategorioj_ligiloj')

    # тип прав Универсо
    tipo = models.ForeignKey(UniversoRajtoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_rajtoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_rajtoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Rajto de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Rajtoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_rajtoj', _('Povas vidi rajtoj de Universo')),
            ('povas_krei_universo_rajtoj', _('Povas krei rajtoj de Universo')),
            ('povas_forigi_universo_rajtoj', _('Povas forigi rajtoj de Universo')),
            ('povas_shangxi_universo_rajtoj', _('Povas ŝanĝi rajtoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoRajto, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                          update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_rajtoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_rajtoj.povas_vidi_universo_rajtoj', 
                'universo_rajtoj.povas_krei_universo_rajtoj',
                'universo_rajtoj.povas_forigi_universo_rajtoj',
                'universo_rajtoj.povas_shangxi_universo_rajtoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_rajtoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_rajtoj.povas_vidi_universo_rajtoj')
                    or user_obj.has_perm('universo_rajtoj.povas_vidi_universo_rajtoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_rajtoj.povas_vidi_universo_rajtoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Наделение правами, использует абстрактный класс UniversoBazaMaks
class UniversoRajtoRajtigo(UniversoBazaMaks):

    # право
    rajto = models.ForeignKey(UniversoRajto, verbose_name=_('Rajto'), blank=False, null=False,
                                  default=None, on_delete=models.CASCADE)

    # пользователь получивший право
    posedanto_uzanto = models.ForeignKey(UniversoUzanto, verbose_name=_('Uzanto'), blank=True, null=True,
                                         default=None, on_delete=models.CASCADE)

    # организация получившая право
    posedanto_organizo = models.ForeignKey(UniversoOrganizo, verbose_name=_('Organizo'), blank=True,
                                           null=True, default=None, on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_rajtoj_rajtigoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Rajtigo de rajtoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Rajtigoj de rajtoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_rajtoj_rajtigoj', _('Povas vidi rajtigoj de rajtoj de Universo')),
            ('povas_krei_universo_rajtoj_rajtigoj', _('Povas krei rajtigoj de rajtoj de Universo')),
            ('povas_forigi_universo_rajtoj_rajtigoj', _('Povas forigi rajtigoj de rajtoj de Universo')),
            ('povas_sxangxi_universo_rajtoj_rajtigoj', _('Povas ŝanĝi rajtigoj de rajtoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_rajtoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_rajtoj.povas_vidi_universo_rajtoj_rajtigoj',
                'universo_rajtoj.povas_krei_universo_rajtoj_rajtigoj',
                'universo_rajtoj.povas_forigi_universo_rajtoj_rajtigoj',
                'universo_rajtoj.povas_sxangxi_universo_rajtoj_rajtigoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_rajtoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_rajtoj.povas_vidi_universo_rajtoj_rajtigoj')
                    or user_obj.has_perm('universo_rajtoj.povas_vidi_universo_rajtoj_rajtigoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_rajtoj.povas_vidi_universo_rajtoj_rajtigoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
