"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres import fields as pgfields
from django.contrib.auth.models import Permission
from django.db.models import Max, Q

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from universo_bazo.models import UniversoBazaMaks, UniversoBazaRealeco, UniversoRealeco
from universo_uzantoj.models import UniversoUzanto
from universo_organizoj.models import UniversoOrganizo


# Категории ресурсов Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoResursoKategorio(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_resursoj_kategorioj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_resursoj_kategorioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio de resursoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj de resursoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_resursoj_kategorioj', _('Povas vidi kategorioj de resursoj de Universo')),
            ('povas_krei_universo_resursoj_kategorioj', _('Povas krei kategorioj de resursoj de Universo')),
            ('povas_forigi_universo_resursoj_kategorioj', _('Povas forigi kategorioj de resursoj de Universo')),
            ('povas_shangxi_universo_resursoj_kategorioj', _('Povas ŝanĝi kategorioj de resursoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoResursoKategorio, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_resursoj.povas_vidi_universo_resursoj_kategorioj', 
                'universo_resursoj.povas_krei_universo_resursoj_kategorioj',
                'universo_resursoj.povas_forigi_universo_resursoj_kategorioj',
                'universo_resursoj.povas_shangxi_universo_resursoj_kategorioj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_resursoj.povas_vidi_universo_resursoj_kategorioj')
                    or user_obj.has_perm('universo_resursoj.povas_vidi_universo_resursoj_kategorioj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_resursoj.povas_vidi_universo_resursoj_kategorioj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы связей категорий ресурсов между собой, использует абстрактный класс UniversoBazaMaks
class UniversoResursoKategorioLigiloTipo(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_resursoj_kategorioj_resursoj_ligiloj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ligiloj de kategorioj de resursoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ligiloj de kategorioj de resursoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_resursoj_kategorioj_resursoj_ligiloj_tipoj',
             _('Povas vidi tipoj de ligiloj de kategorioj de resursoj de Universo')),
            ('povas_krei_universo_resursoj_kategorioj_resursoj_ligiloj_tipoj',
             _('Povas krei tipoj de ligiloj de kategorioj de resursoj de Universo')),
            ('povas_forigi_universo_resursoj_kategorioj_resursoj_ligiloj_tipoj',
             _('Povas forigi tipoj de ligiloj de kategorioj de resursoj de Universo')),
            ('povas_sxangxi_universo_resursoj_kategorioj_resursoj_ligiloj_tipoj',
             _('Povas ŝanĝi tipoj de ligiloj de kategorioj de resursoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoResursoKategorioLigiloTipo, self).save(force_insert=force_insert, force_update=force_update,
                                          using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_resursoj.povas_vidi_universo_resursoj_kategorioj_resursoj_ligiloj_tipoj',
                'universo_resursoj.povas_krei_universo_resursoj_kategorioj_resursoj_ligiloj_tipoj',
                'universo_resursoj.povas_forigi_universo_resursoj_kategorioj_resursoj_ligiloj_tipoj',
                'universo_resursoj.povas_sxangxi_universo_resursoj_kategorioj_resursoj_ligiloj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_resursoj.povas_vidi_universo_resursoj_kategorioj_resursoj_ligiloj_tipoj')
                    or user_obj.has_perm('universo_resursoj.povas_vidi_universo_resursoj_kategorioj_resursoj_ligiloj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_resursoj.povas_vidi_universo_resursoj_kategorioj_resursoj_ligiloj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь категорий ресурсов между собой, использует абстрактный класс UniversoBazaMaks
class UniversoResursoKategorioLigilo(UniversoBazaMaks):

    # категория ресурсов владелец связи
    posedanto = models.ForeignKey(UniversoResursoKategorio, verbose_name=_('Kategorio - posedanto'), blank=False,
                                  null=False, default=None, on_delete=models.CASCADE)

    # связываемая категория ресурсов
    ligilo = models.ForeignKey(UniversoResursoKategorio, verbose_name=_('Kategorio - ligilo'), blank=False, null=False,
                               default=None, related_name='%(app_label)s_%(class)s_ligilo', on_delete=models.CASCADE)

    # тип связи категорий ресурсов
    tipo = models.ForeignKey(UniversoResursoKategorioLigiloTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_resursoj_kategorioj_resursoj_ligiloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ligilo de kategorioj de resursoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ligiloj de kategorioj de resursoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_resursoj_kategorioj_resursoj_ligiloj',
             _('Povas vidi ligiloj de kategorioj de resursoj de Universo')),
            ('povas_krei_universo_resursoj_kategorioj_resursoj_ligiloj',
             _('Povas krei ligiloj de kategorioj de resursoj de Universo')),
            ('povas_forigi_universo_resursoj_kategorioj_resursoj_ligiloj',
             _('Povas forigi ligiloj de kategorioj de resursoj de Universo')),
            ('povas_sxangxi_universo_resursoj_kategorioj_resursoj_ligiloj',
             _('Povas ŝanĝi ligiloj de kategorioj de resursoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_resursoj.povas_vidi_universo_resursoj_kategorioj_resursoj_ligiloj',
                'universo_resursoj.povas_krei_universo_resursoj_kategorioj_resursoj_ligiloj',
                'universo_resursoj.povas_forigi_universo_resursoj_kategorioj_resursoj_ligiloj',
                'universo_resursoj.povas_sxangxi_universo_resursoj_kategorioj_resursoj_ligiloj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_resursoj.povas_vidi_universo_resursoj_kategorioj_resursoj_ligiloj')
                    or user_obj.has_perm('universo_resursoj.povas_vidi_universo_resursoj_kategorioj_resursoj_ligiloj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_resursoj.povas_vidi_universo_resursoj_kategorioj_resursoj_ligiloj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы ресурсов в Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoResursoTipo(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_resursoj_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_resursoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de resursoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de resursoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_resursoj_tipoj', _('Povas vidi tipoj de resursoj de Universo')),
            ('povas_krei_universo_resursoj_tipoj', _('Povas krei tipoj de resursoj de Universo')),
            ('povas_forigi_universo_resursoj_tipoj', _('Povas forigi tipoj de resursoj de Universo')),
            ('povas_shangxi_universo_resursoj_tipoj', _('Povas ŝanĝi tipoj de resursoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoResursoTipo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_resursoj.povas_vidi_universo_resursoj_tipoj', 
                'universo_resursoj.povas_krei_universo_resursoj_tipoj',
                'universo_resursoj.povas_forigi_universo_resursoj_tipoj',
                'universo_resursoj.povas_shangxi_universo_resursoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_resursoj.povas_vidi_universo_resursoj_tipoj')
                    or user_obj.has_perm('universo_resursoj.povas_vidi_universo_resursoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_resursoj.povas_vidi_universo_resursoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Классы ресурсов в Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoResursoKlaso(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_resursoj_klasoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_resursoj_klasoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Klaso de resursoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Klasoj de resursoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_resursoj_klasoj', _('Povas vidi klasoj de resursoj de Universo')),
            ('povas_krei_universo_resursoj_klasoj', _('Povas krei klasoj de resursoj de Universo')),
            ('povas_forigi_universo_resursoj_klasoj', _('Povas forigi klasoj de resursoj de Universo')),
            ('povas_shangxi_universo_resursoj_klasoj', _('Povas ŝanĝi klasoj de resursoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoResursoKlaso, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_resursoj.povas_vidi_universo_resursoj_klasoj',
                'universo_resursoj.povas_krei_universo_resursoj_klasoj',
                'universo_resursoj.povas_forigi_universo_resursoj_klasoj',
                'universo_resursoj.povas_shangxi_universo_resursoj_klasoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_resursoj.povas_vidi_universo_resursoj_klasoj')
                    or user_obj.has_perm('universo_resursoj.povas_vidi_universo_resursoj_klasoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_resursoj.povas_vidi_universo_resursoj_klasoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Справочник ресурсов в Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoResurso(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # категория ресурсов Универсо
    kategorio = models.ManyToManyField(UniversoResursoKategorio, verbose_name=_('Kategorio'),
                                       db_table='universo_resursoj_kategorioj_ligiloj')

    # тип ресурсов Универсо
    tipo = models.ForeignKey(UniversoResursoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # класс ресурсов Универсо
    klaso = models.ForeignKey(UniversoResursoKlaso, verbose_name=_('Klaso'), blank=True, null=True, default=None,
                             on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_resursoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_resursoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Resurso de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Resursoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_resursoj', _('Povas vidi resursoj de Universo')),
            ('povas_krei_universo_resursoj', _('Povas krei resursoj de Universo')),
            ('povas_forigi_universo_resursoj', _('Povas forigi resursoj de Universo')),
            ('povas_shangxi_universo_resursoj', _('Povas ŝanĝi resursoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoResurso, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                          update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_resursoj.povas_vidi_universo_resursoj', 
                'universo_resursoj.povas_krei_universo_resursoj',
                'universo_resursoj.povas_forigi_universo_resursoj',
                'universo_resursoj.povas_shangxi_universo_resursoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_resursoj.povas_vidi_universo_resursoj')
                    or user_obj.has_perm('universo_resursoj.povas_vidi_universo_resursoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_resursoj.povas_vidi_universo_resursoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Модификации ресурсов и их характеристики UniversoBazaMaks
class UniversoResursoModifo(UniversoBazaMaks):

    # ресурс владелец модификации
    posedanto = models.ForeignKey(UniversoResurso, verbose_name=_('Posedanto'), blank=False, null=False,
                                  default=None, on_delete=models.CASCADE)

    # уникальный личный ID модификации в рамках ресурса
    id = models.IntegerField(_('ID'), blank=True, null=True, default=None)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # целостность ресурса
    integreco = models.IntegerField(_('Integreco'), blank=True, null=True, default=None)

    # мощность ресурса
    potenco = models.IntegerField(_('Potenco'), blank=True, null=True, default=None)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_resursoj_modifoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Modifo de resursoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Modifoj de resursoj de Universo')
        # установка уникальности личного ID места хранения в рамках объекта
        unique_together = ("id", "posedanto")
        # права
        permissions = (
            ('povas_vidi_universo_resursoj_modifoj', _('Povas vidi modifoj de resursoj de Universo')),
            ('povas_krei_universo_resursoj_modifoj', _('Povas krei modifoj de resursoj de Universo')),
            ('povas_forigi_universo_resursoj_modifoj', _('Povas forigi modifoj de resursoj de Universo')),
            ('povas_sxangxi_universo_resursoj_modifoj', _('Povas ŝanĝi modifoj de resursoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{} ({})'.format(self.uuid, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(posedanto=self.posedanto).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoResursoModifo, self).save(force_insert=force_insert, force_update=force_update,
                                                using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_resursoj.povas_vidi_universo_resursoj_modifoj',
                'universo_resursoj.povas_krei_universo_resursoj_modifoj',
                'universo_resursoj.povas_forigi_universo_resursoj_modifoj',
                'universo_resursoj.povas_sxangxi_universo_resursoj_modifoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_resursoj.povas_vidi_universo_resursoj_modifoj')
                    or user_obj.has_perm('universo_resursoj.povas_vidi_universo_resursoj_modifoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_resursoj.povas_vidi_universo_resursoj_modifoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Состояния модификаций ресурсов, в зависимости от уровня поверждения UniversoBazaMaks
class UniversoResursoModifoStato(UniversoBazaMaks):

    # модификация ресурса владелец состояния
    posedanto = models.ForeignKey(UniversoResursoModifo, verbose_name=_('Posedanto'), blank=False, null=False,
                                  default=None, on_delete=models.CASCADE)

    # уникальный личный ID состояния в рамках модификации ресурса
    id = models.IntegerField(_('ID'), blank=True, null=True, default=None)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # целостность ресурса
    integreco = models.IntegerField(_('Integreco'), blank=True, null=True, default=None)

    # мощность ресурса
    potenco = models.IntegerField(_('Potenco'), blank=True, null=True, default=None)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_resursoj_modifoj_statoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Stato de modifoj de resursoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statoj de modifoj de resursoj de Universo')
        # установка уникальности личного ID места хранения в рамках объекта
        unique_together = ("id", "posedanto")
        # права
        permissions = (
            ('povas_vidi_universo_resursoj_modifoj_statoj',
             _('Povas vidi statoj de modifoj de resursoj de Universo')),
            ('povas_krei_universo_resursoj_modifoj_statoj',
             _('Povas krei statoj de modifoj de resursoj de Universo')),
            ('povas_forigi_universo_resursoj_modifoj_statoj',
             _('Povas forigi statoj de modifoj de resursoj de Universo')),
            ('povas_sxangxi_universo_resursoj_modifoj_statoj',
             _('Povas ŝanĝi statoj de modifoj de resursoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{} ({})'.format(self.uuid, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(posedanto=self.posedanto).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoResursoModifoStato, self).save(force_insert=force_insert, force_update=force_update,
                                                     using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_resursoj.povas_vidi_universo_resursoj_modifoj_statoj',
                'universo_resursoj.povas_krei_universo_resursoj_modifoj_statoj',
                'universo_resursoj.povas_forigi_universo_resursoj_modifoj_statoj',
                'universo_resursoj.povas_sxangxi_universo_resursoj_modifoj_statoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_resursoj.povas_vidi_universo_resursoj_modifoj_statoj')
                    or user_obj.has_perm('universo_resursoj.povas_vidi_universo_resursoj_modifoj_statoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_resursoj.povas_vidi_universo_resursoj_modifoj_statoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы мест хранения ресурсов, использует абстрактный класс UniversoBazaMaks
class UniversoResursoStokejoTipo(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_resursoj_stokejoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de stokejoj de resursoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de stokejoj de resursoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_resursoj_stokejoj_tipoj',
             _('Povas vidi tipoj de stokejoj de resursoj de Universo')),
            ('povas_krei_universo_resursoj_stokejoj_tipoj',
             _('Povas krei tipoj de stokejoj de resursoj de Universo')),
            ('povas_forigi_universo_resursoj_stokejoj_tipoj',
             _('Povas forigi tipoj de stokejoj de resursoj de Universo')),
            ('povas_shangxi_universo_resursoj_stokejoj_tipoj',
             _('Povas ŝanĝi tipoj de stokejoj de resursoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoResursoStokejoTipo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_resursoj.povas_vidi_universo_resursoj_stokejoj_tipoj',
                'universo_resursoj.povas_krei_universo_resursoj_stokejoj_tipoj',
                'universo_resursoj.povas_forigi_universo_resursoj_stokejoj_tipoj',
                'universo_resursoj.povas_shangxi_universo_resursoj_stokejoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_resursoj.povas_vidi_universo_resursoj_stokejoj_tipoj')
                    or user_obj.has_perm('universo_resursoj.povas_vidi_universo_resursoj_stokejoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_resursoj.povas_vidi_universo_resursoj_stokejoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Логические места хранения в ресурсах, использует абстрактный класс UniversoBazaMaks
class UniversoResursoStokejo(UniversoBazaMaks):

    # ресурс владелец места хранения
    posedanto = models.ForeignKey(UniversoResurso, verbose_name=_('Posedanto'), blank=False, null=False,
                                  default=None, on_delete=models.CASCADE)

    # уникальный личный ID места хранения в рамках ресурса
    id = models.IntegerField(_('ID'), blank=True, null=True, default=None)

    # тип места хранения ресурсов на основе которого создано это место хранения в ресурсе
    tipo = models.ForeignKey(UniversoResursoStokejoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_resursoj_stokejoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Stokejo de resursoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Stokejoj de resursoj de Universo')
        # установка уникальности личного ID места хранения в рамках объекта
        unique_together = ("id", "posedanto")
        # права
        permissions = (
            ('povas_vidi_universo_resursoj_stokejoj', _('Povas vidi stokejoj de resursoj de Universo')),
            ('povas_krei_universo_resursoj_stokejoj', _('Povas krei stokejoj de resursoj de Universo')),
            ('povas_forigi_universo_resursoj_stokejoj', _('Povas forigi stokejoj de resursoj de Universo')),
            ('povas_sxangxi_universo_resursoj_stokejoj', _('Povas ŝanĝi stokejoj de resursoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{} ({})'.format(self.uuid, get_enhavo(self.priskribo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(posedanto=self.posedanto).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoResursoStokejo, self).save(force_insert=force_insert, force_update=force_update,
                                                 using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_resursoj.povas_vidi_universo_resursoj_stokejoj',
                'universo_resursoj.povas_krei_universo_resursoj_stokejoj',
                'universo_resursoj.povas_forigi_universo_resursoj_stokejoj',
                'universo_resursoj.povas_sxangxi_universo_resursoj_stokejoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_resursoj.povas_vidi_universo_resursoj_stokejoj')
                    or user_obj.has_perm('universo_resursoj.povas_vidi_universo_resursoj_stokejoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_resursoj.povas_vidi_universo_resursoj_stokejoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы цен ресурсов, использует абстрактный класс UniversoBazaMaks
class UniversoResursoPrezoTipo(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_resursoj_prezoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de prezoj de resursoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de prezoj de resursoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_resursoj_prezoj_tipoj', _('Povas vidi tipoj de prezoj de resursoj de Universo')),
            ('povas_krei_universo_resursoj_prezoj_tipoj', _('Povas krei tipoj de prezoj de resursoj de Universo')),
            ('povas_forigi_universo_resursoj_prezoj_tipoj', _('Povas forigi tipoj de prezoj de resursoj de Universo')),
            ('povas_sxangxi_universo_resursoj_prezoj_tipoj', _('Povas ŝanĝi tipoj de prezoj de resursoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoResursoPrezoTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                   using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_resursoj.povas_vidi_universo_resursoj_prezoj_tipoj',
                'universo_resursoj.povas_krei_universo_resursoj_prezoj_tipoj',
                'universo_resursoj.povas_forigi_universo_resursoj_prezoj_tipoj',
                'universo_resursoj.povas_sxangxi_universo_resursoj_prezoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_resursoj.povas_vidi_universo_resursoj_prezoj_tipoj')
                    or user_obj.has_perm('universo_resursoj.povas_vidi_universo_resursoj_prezoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_resursoj.povas_vidi_universo_resursoj_prezoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Виды цен ресурсов, использует абстрактный класс UniversoBazaMaks
class UniversoResursoPrezoSpeco(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_resursoj_prezoj_specoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Speco de prezoj de resursoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Specoj de prezoj de resursoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_resursoj_prezoj_specoj', _('Povas vidi specoj de prezoj de resursoj de Universo')),
            ('povas_krei_universo_resursoj_prezoj_specoj', _('Povas krei specoj de prezoj de resursoj de Universo')),
            ('povas_forigi_universo_resursoj_prezoj_specoj', _('Povas forigi specoj de prezoj de resursoj de Universo')),
            ('povas_sxangxi_universo_resursoj_prezoj_specoj', _('Povas ŝanĝi specoj de prezoj de resursoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoResursoPrezoSpeco, self).save(force_insert=force_insert, force_update=force_update,
                                                   using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_resursoj.povas_vidi_universo_resursoj_prezoj_specoj',
                'universo_resursoj.povas_krei_universo_resursoj_prezoj_specoj',
                'universo_resursoj.povas_forigi_universo_resursoj_prezoj_specoj',
                'universo_resursoj.povas_sxangxi_universo_resursoj_prezoj_specoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_resursoj.povas_vidi_universo_resursoj_prezoj_specoj')
                    or user_obj.has_perm('universo_resursoj.povas_vidi_universo_resursoj_prezoj_specoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_resursoj.povas_vidi_universo_resursoj_prezoj_specoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Статус цены ресурса, использует абстрактный класс UniversoBazaMaks
class UniversoResursoPrezoStatuso(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_resursoj_prezoj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso de prezoj de resursoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj de prezoj de resursoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_resursoj_prezoj_statusoj',
             _('Povas vidi statusoj de prezoj de resursoj de Universo')),
            ('povas_krei_universo_resursoj_prezoj_statusoj',
             _('Povas krei statusoj de prezoj de resursoj de Universo')),
            ('povas_forigi_universo_resursoj_prezoj_statusoj',
             _('Povas forigi statusoj de prezoj de resursoj de Universo')),
            ('povas_sxangxi_universo_resursoj_prezoj_statusoj',
             _('Povas ŝanĝi statusoj de prezoj de resursoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoResursoPrezoStatuso, self).save(force_insert=force_insert, force_update=force_update,
                                                      using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_resursoj.povas_vidi_universo_resursoj_prezoj_statusoj',
                'universo_resursoj.povas_krei_universo_resursoj_prezoj_statusoj',
                'universo_resursoj.povas_forigi_universo_resursoj_prezoj_statusoj',
                'universo_resursoj.povas_sxangxi_universo_resursoj_prezoj_statusoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_resursoj.povas_vidi_universo_resursoj_prezoj_statusoj')
                    or user_obj.has_perm('universo_resursoj.povas_vidi_universo_resursoj_prezoj_statusoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_resursoj.povas_vidi_universo_resursoj_prezoj_statusoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Цены ресурсов, использует абстрактный класс UniversoBazaRealeco
class UniversoResursoPrezo(UniversoBazaRealeco):

    # ресурс для которого устанавливается цена
    resurso = models.ForeignKey(UniversoResurso, verbose_name=_('Resurso'), blank=False, null=False,
                                default=None, on_delete=models.CASCADE)

    # пользователь владелец цены ресурса
    posedanto_uzanto = models.ForeignKey(UniversoUzanto, verbose_name=_('Posedanta uzanto'), blank=True, null=True,
                                         default=None, on_delete=models.CASCADE)

    # организация владелец цены ресурса
    posedanto_organizo = models.ForeignKey(UniversoOrganizo, verbose_name=_('Posedanta organizo'), blank=True,
                                           null=True, default=None, on_delete=models.CASCADE)

    # значение цены, максимальное значение 15 знаков, из них 2 после точки
    prezo = models.DecimalField(_('Prezo'), max_digits=15, decimal_places=2, blank=True, null=True, default=0)

    # тип цены ресурса
    tipo = models.ForeignKey(UniversoResursoPrezoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # вид цены ресурса
    speco = models.ForeignKey(UniversoResursoPrezoSpeco, verbose_name=_('Speco'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # статус цены ресурса
    statuso = models.ForeignKey(UniversoResursoPrezoStatuso, verbose_name=_('Statuso'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_resursoj_prezoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Prezo de resurso de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Prezoj de resursoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_resursoj_prezoj', _('Povas vidi prezoj de resursoj de Universo')),
            ('povas_krei_universo_resursoj_prezoj', _('Povas krei prezoj de resursoj de Universo')),
            ('povas_forigi_universo_resursoj_prezoj', _('Povas forigi prezoj de resursoj de Universo')),
            ('povas_sxangxi_universo_resursoj_prezoj', _('Povas ŝanĝi prezoj de resursoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле id владельца Siriuso и/или nomo организации владельца этой модели
        prezo = "{}".format(self.resurso.uuid)
        start = True
        if self.posedanto_uzanto:
            prezo = "{}: {}".format(prezo, self.posedanto_uzanto.siriuso_uzanto.id)
            start = False
        if self.posedanto_organizo:
            if start:
                prezo = "{}: {}".format(prezo, get_enhavo(self.posedanto_organizo.nomo, empty_values=True)[0])
            else:
                prezo = "{}; {}".format(prezo, get_enhavo(self.posedanto_organizo.nomo, empty_values=True)[0])
        return prezo

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(UniversoResursoPrezo, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_resursoj.povas_vidi_universo_resursoj_prezoj',
                'universo_resursoj.povas_krei_universo_resursoj_prezoj',
                'universo_resursoj.povas_forigi_universo_resursoj_prezoj',
                'universo_resursoj.povas_sxangxi_universo_resursoj_prezoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_resursoj.povas_vidi_universo_resursoj_prezoj')
                    or user_obj.has_perm('universo_resursoj.povas_vidi_universo_resursoj_prezoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_resursoj.povas_vidi_universo_resursoj_prezoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы связей ресурсов между собой, использует абстрактный класс UniversoBazaMaks
class UniversoResursoLigiloTipo(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_resursoj_ligiloj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ligiloj de resursoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ligiloj de resursoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_resursoj_ligiloj_tipoj',
             _('Povas vidi tipoj de ligiloj de resursoj de Universo')),
            ('povas_krei_universo_resursoj_ligiloj_tipoj',
             _('Povas krei tipoj de ligiloj de resursoj de Universo')),
            ('povas_forigi_universo_resursoj_ligiloj_tipoj',
             _('Povas forigi tipoj de ligiloj de resursoj de Universo')),
            ('povas_sxangxi_universo_resursoj_ligiloj_tipoj',
             _('Povas ŝanĝi tipoj de ligiloj de resursoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoResursoLigiloTipo, self).save(force_insert=force_insert, force_update=force_update,
                                          using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_resursoj.povas_vidi_universo_resursoj_ligiloj_tipoj', 
                'universo_resursoj.povas_krei_universo_resursoj_ligiloj_tipoj',
                'universo_resursoj.povas_forigi_universo_resursoj_ligiloj_tipoj',
                'universo_resursoj.povas_sxangxi_universo_resursoj_ligiloj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_resursoj.povas_vidi_universo_resursoj_ligiloj_tipoj')
                    or user_obj.has_perm('universo_resursoj.povas_vidi_universo_resursoj_ligiloj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_resursoj.povas_vidi_universo_resursoj_ligiloj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь ресурсов между собой, использует абстрактный класс UniversoBazaMaks
class UniversoResursoLigilo(UniversoBazaMaks):

    # ресурс владелец связи
    posedanto = models.ForeignKey(UniversoResurso, verbose_name=_('Resurso - posedanto'), blank=False, null=False,
                                          default=None, on_delete=models.CASCADE)

    # место хранения владельца связи
    posedanto_stokejo = models.ForeignKey(UniversoResursoStokejo, verbose_name=_('Stokejo de posedanto'), blank=True,
                                          null=True, default=None, on_delete=models.CASCADE)

    # разъём (слот), который используется у родительского ресурса
    konektilo_posedanto = models.IntegerField(_('Konektilo - posedanto'), blank=True, null=True, default=None)

    # связываемый ресурс
    ligilo = models.ForeignKey(UniversoResurso, verbose_name=_('Resurso - ligilo'), blank=False, null=False,
                                       default=None, related_name='%(app_label)s_%(class)s_ligilo',
                                       on_delete=models.CASCADE)

    # разъём (слот), который используется у связываемого ресурса
    konektilo_ligilo = models.IntegerField(_('Konektilo - ligilo'), blank=True, null=True, default=None)

    # тип связи ресурсов
    tipo = models.ForeignKey(UniversoResursoLigiloTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_resursoj_ligiloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ligilo de resursoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ligiloj de resursoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_resursoj_ligiloj', _('Povas vidi ligiloj de resursoj de Universo')),
            ('povas_krei_universo_resursoj_ligiloj', _('Povas krei ligiloj de resursoj de Universo')),
            ('povas_forigi_universo_resursoj_ligiloj', _('Povas forigi ligiloj de resursoj de Universo')),
            ('povas_sxangxi_universo_resursoj_ligiloj', _('Povas ŝanĝi ligiloj de resursoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_resursoj.povas_vidi_universo_resursoj_ligiloj', 
                'universo_resursoj.povas_krei_universo_resursoj_ligiloj',
                'universo_resursoj.povas_forigi_universo_resursoj_ligiloj',
                'universo_resursoj.povas_sxangxi_universo_resursoj_ligiloj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_resursoj.povas_vidi_universo_resursoj_ligiloj')
                    or user_obj.has_perm('universo_resursoj.povas_vidi_universo_resursoj_ligiloj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_resursoj.povas_vidi_universo_resursoj_ligiloj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
