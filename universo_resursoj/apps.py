from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UniversoResursojConfig(AppConfig):
    name = 'universo_resursoj'
    verbose_name = _('Resursoj de Universo')
