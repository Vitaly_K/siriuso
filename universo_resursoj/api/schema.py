import graphene  # сам Graphene
from django.utils.translation import ugettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import *  # модели приложения


# Модель связей категорий ресурсов между собой
class UniversoResursoKategorioLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = UniversoResursoKategorioLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__id': ['exact'],
            'ligilo__id': ['exact'],
            'tipo__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель категорий ресурсов Универсо
class UniversoResursoKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    # Связь категорий ресурсов между собой
    ligilo =  SiriusoFilterConnectionField(UniversoResursoKategorioLigiloNode,
        description=_('Выводит связи категорий ресурсов между собой'))

    class Meta:
        model = UniversoResursoKategorio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact'],
            'universo_resursoj_universoresursokategorioligilo_ligilo': ['isnull']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_ligilo(self, info, **kwargs):
        return UniversoResursoKategorioLigilo.objects.filter(posedanto=self, forigo=False, arkivo=False, publikigo=True)


# Модель типов связей категорий ресурсов между собой
class UniversoResursoKategorioLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoResursoKategorioLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов ресурсов Универсо
class UniversoResursoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoResursoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель классов ресурсов Универсо
class UniversoResursoKlasoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoResursoKlaso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель модификации ресурсов Универсо
class UniversoResursoModifoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoResursoModifo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__id': ['exact'],
            'posedanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель состояния модификаций ресурсов Универсо
class UniversoResursoModifoStatoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoResursoModifoStato
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов мест хранения ресурсов Универсо
class UniversoResursoStokejoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoResursoStokejoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель логических мест хранения ресурсов Универсо
class UniversoResursoStokejoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoResursoStokejo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'tipo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов цен ресурсов Универсо
class UniversoResursoPrezoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoResursoPrezoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель видов цен ресурсов Универсо
class UniversoResursoPrezoSpecoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoResursoPrezoSpeco
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов цен ресурсов Универсо
class UniversoResursoPrezoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoResursoPrezoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель цен ресурсов Универсо
class UniversoResursoPrezoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = UniversoResursoPrezo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель типов связей ресурсов между собой
class UniversoResursoLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoResursoLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связей ресурсов между собой
class UniversoResursoLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoResursoLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'posedanto_stokejo__uuid': ['exact'],
            'ligilo__uuid': ['exact'],
            'tipo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель ресурсов Универсо
class UniversoResursoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    # Связь ресурсов между собой
    ligilo =  SiriusoFilterConnectionField(UniversoResursoLigiloNode,
        description=_('Выводит связи ресурсов между собой, владельцами которых является ресурс'))

    class Meta:
        model = UniversoResurso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'klaso__id': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_ligilo(self, info, **kwargs):
        return UniversoResursoLigilo.objects.filter(posedanto=self, forigo=False, arkivo=False, 
            publikigo=True)


class UniversoResursojQuery(graphene.ObjectType):
    universo_resurso_kategorio = SiriusoFilterConnectionField(
        UniversoResursoKategorioNode,
        description=_('Выводит все доступные модели категорий ресурсов Универсо')
    )
    universo_resurso_kategorio_ligiloj_tipoj = SiriusoFilterConnectionField(
        UniversoResursoKategorioLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей категорий ресурсов между собой')
    )
    universo_resurso_kategorio_ligiloj = SiriusoFilterConnectionField(
        UniversoResursoKategorioLigiloNode,
        description=_('Выводит все доступные модели связей категорий ресурсов между собой')
    )
    universo_resurso_tipo = SiriusoFilterConnectionField(
        UniversoResursoTipoNode,
        description=_('Выводит все доступные модели типов ресурсов Универсо')
    )
    universo_resurso_klaso = SiriusoFilterConnectionField(
        UniversoResursoKlasoNode,
        description=_('Выводит все доступные модели классов ресурсов Универсо')
    )
    universo_resurso_modifo = SiriusoFilterConnectionField(
        UniversoResursoModifoNode,
        description=_('Выводит все доступные модели модификации ресурсов Универсо')
    )
    universo_resurso_modifo_stato = SiriusoFilterConnectionField(
        UniversoResursoModifoStatoNode,
        description=_('Выводит все доступные модели состояния модификаций ресурсов Универсо')
    )
    universo_resurso_stokejo_tipo = SiriusoFilterConnectionField(
        UniversoResursoStokejoTipoNode,
        description=_('Выводит все доступные модели типов мест хранения ресурсов Универсо')
    )
    universo_resurso_stokejo = SiriusoFilterConnectionField(
        UniversoResursoStokejoNode,
        description=_('Выводит все доступные модели логических мест хранения ресурсов Универсо')
    )
    universo_resurso_prezo_tipo = SiriusoFilterConnectionField(
        UniversoResursoPrezoTipoNode,
        description=_('Выводит все доступные модели типов цен ресурсов Универсо')
    )
    universo_resurso_prezo_speco = SiriusoFilterConnectionField(
        UniversoResursoPrezoSpecoNode,
        description=_('Выводит все доступные модели видов цен ресурсов Универсо')
    )
    universo_resurso_prezo_statuso = SiriusoFilterConnectionField(
        UniversoResursoPrezoStatusoNode,
        description=_('Выводит все доступные модели статусов цен ресурсов Универсо')
    )
    universo_resurso_prezo = SiriusoFilterConnectionField(
        UniversoResursoPrezoNode,
        description=_('Выводит все доступные модели цен ресурсов Универсо')
    )
    universo_resurso = SiriusoFilterConnectionField(
        UniversoResursoNode,
        description=_('Выводит все доступные ресурсы Универсо')
    )
    universo_resurso_ligilo_tipo = SiriusoFilterConnectionField(
        UniversoResursoLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей ресурсов между собой')
    )
    universo_resurso_ligilo = SiriusoFilterConnectionField(
        UniversoResursoLigiloNode,
        description=_('Выводит все доступные модели связей ресурсов между собой')
    )
