"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
16.05.2019
"""
from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from universo_uzantoj.models import UniversoUzanto
from .schema import *
from ..models import *


# Модель категорий ресурсов Универсо
class RedaktuUniversoResursoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_resursoj_kategorioj = graphene.Field(UniversoResursoKategorioNode,
        description=_('Созданная/изменённая категория ресурсов в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_resursoj_kategorioj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_resursoj.povas_krei_universo_resursoj_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_resursoj_kategorioj = UniversoResursoKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_resursoj_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_resursoj_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_resursoj_kategorioj.realeco.set(realeco)
                                else:
                                    universo_resursoj_kategorioj.realeco.clear()

                            universo_resursoj_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_resursoj_kategorioj = UniversoResursoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_resursoj.povas_forigi_universo_resursoj_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_resursoj.povas_shanghi_universo_resursoj_kategorioj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_resursoj_kategorioj.forigo = kwargs.get('forigo', universo_resursoj_kategorioj.forigo)
                                universo_resursoj_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_resursoj_kategorioj.arkivo = kwargs.get('arkivo', universo_resursoj_kategorioj.arkivo)
                                universo_resursoj_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_resursoj_kategorioj.publikigo = kwargs.get('publikigo', universo_resursoj_kategorioj.publikigo)
                                universo_resursoj_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_resursoj_kategorioj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_resursoj_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_resursoj_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_resursoj_kategorioj.realeco.set(realeco)
                                    else:
                                        universo_resursoj_kategorioj.realeco.clear()

                                universo_resursoj_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoResursoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoResursoKategorio(status=status, message=message, universo_resursoj_kategorioj=universo_resursoj_kategorioj)


# Модель типов связей категорий ресурсов между собой
class RedaktuUniversoResursoKategorioLigiloTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_resursoj_kategorioj_resursoj_ligiloj_tipoj = graphene.Field(UniversoResursoKategorioLigiloTipoNode,
        description=_('Созданная/изменённая модель типов связей категорий ресурсов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_resursoj_kategorioj_resursoj_ligiloj_tipoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_resursoj.povas_krei_universo_resursoj_kategorioj_resursoj_ligiloj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_resursoj_kategorioj_resursoj_ligiloj_tipoj = UniversoResursoKategorioLigiloTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_resursoj_kategorioj_resursoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_resursoj_kategorioj_resursoj_ligiloj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_resursoj_kategorioj_resursoj_ligiloj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_resursoj_kategorioj_resursoj_ligiloj_tipoj = UniversoResursoKategorioLigiloTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_resursoj.povas_forigi_universo_resursoj_kategorioj_resursoj_ligiloj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_resursoj.povas_shanghi_universo_resursoj_kategorioj_resursoj_ligiloj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_resursoj_kategorioj_resursoj_ligiloj_tipoj.forigo = kwargs.get('forigo', universo_resursoj_kategorioj_resursoj_ligiloj_tipoj.forigo)
                                universo_resursoj_kategorioj_resursoj_ligiloj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_resursoj_kategorioj_resursoj_ligiloj_tipoj.arkivo = kwargs.get('arkivo', universo_resursoj_kategorioj_resursoj_ligiloj_tipoj.arkivo)
                                universo_resursoj_kategorioj_resursoj_ligiloj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_resursoj_kategorioj_resursoj_ligiloj_tipoj.publikigo = kwargs.get('publikigo', universo_resursoj_kategorioj_resursoj_ligiloj_tipoj.publikigo)
                                universo_resursoj_kategorioj_resursoj_ligiloj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_resursoj_kategorioj_resursoj_ligiloj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_resursoj_kategorioj_resursoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_resursoj_kategorioj_resursoj_ligiloj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_resursoj_kategorioj_resursoj_ligiloj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoResursoKategorioLigiloTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoResursoKategorioLigiloTipo(status=status, message=message, universo_resursoj_kategorioj_resursoj_ligiloj_tipoj=universo_resursoj_kategorioj_resursoj_ligiloj_tipoj)


# Модель связей категорий ресурсов между собой
class RedaktuUniversoResursoKategorioLigilo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_resursoj_kategorioj_resursoj_ligiloj = graphene.Field(UniversoResursoKategorioLigiloNode,
        description=_('Созданная/изменённая модель связей категорий ресурсов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        posedanto_id = graphene.String(description=_('ID категории ресурсов владельца связи'))
        ligilo_id = graphene.String(description=_('ID связываемой категории ресурсов'))
        tipo_id = graphene.String(description=_('ID типа связи категорий ресурсов'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_resursoj_kategorioj_resursoj_ligiloj = None
        posedanto = None
        ligilo = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_resursoj.povas_krei_universo_resursoj_kategorioj_resursoj_ligiloj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_id' in kwargs:
                                try:
                                    posedanto = UniversoResursoKategorio.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoResursoKategorio.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверная категория ресурсов владельца связи'),'posedanto_id')
                            else:
                                message = '{}: {}'.format(_('Не указана категория ресурсов владелец связи'),'posedanto_id')

                        if not message:
                            if 'ligilo_id' in kwargs:
                                try:
                                    ligilo = UniversoResursoKategorio.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoResursoKategorio.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверная связываемая категория ресурсов'),'ligilo_id')
                            else:
                                message = '{}: {}'.format(_('Не указана связываемая категория ресурсов'),'ligilo_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoResursoKategorioLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoResursoKategorioLigiloTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип связи категорий ресурсов'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип связи категорий ресурсов'),'tipo_id')

                        if not message:
                            universo_resursoj_kategorioj_resursoj_ligiloj = UniversoResursoKategorioLigilo.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto=posedanto,
                                ligilo=ligilo,
                                tipo=tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            universo_resursoj_kategorioj_resursoj_ligiloj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto', False) or kwargs.get('ligilo', False)
                            or kwargs.get('tipo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_resursoj_kategorioj_resursoj_ligiloj = UniversoResursoKategorioLigilo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_resursoj.povas_forigi_universo_resursoj_kategorioj_resursoj_ligiloj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_resursoj.povas_shanghi_universo_resursoj_kategorioj_resursoj_ligiloj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_id' in kwargs:
                                    try:
                                        posedanto = UniversoResursoKategorio.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoResursoKategorio.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверная категория ресурсов владельца связи'),'posedanto_id')

                            if not message:
                                if 'ligilo_id' in kwargs:
                                    try:
                                        ligilo = UniversoResursoKategorio.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoResursoKategorio.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверная связываемая категория ресурсов'),'ligilo_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoResursoKategorioLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoResursoKategorioLigiloTipo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный тип связи категорий ресурсов'),'tipo_id')

                            if not message:

                                universo_resursoj_kategorioj_resursoj_ligiloj.forigo = kwargs.get('forigo', universo_resursoj_kategorioj_resursoj_ligiloj.forigo)
                                universo_resursoj_kategorioj_resursoj_ligiloj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_resursoj_kategorioj_resursoj_ligiloj.arkivo = kwargs.get('arkivo', universo_resursoj_kategorioj_resursoj_ligiloj.arkivo)
                                universo_resursoj_kategorioj_resursoj_ligiloj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_resursoj_kategorioj_resursoj_ligiloj.publikigo = kwargs.get('publikigo', universo_resursoj_kategorioj_resursoj_ligiloj.publikigo)
                                universo_resursoj_kategorioj_resursoj_ligiloj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_resursoj_kategorioj_resursoj_ligiloj.posedanto = posedanto if kwargs.get('posedanto_id', False) else universo_resursoj_kategorioj_resursoj_ligiloj.posedanto
                                universo_resursoj_kategorioj_resursoj_ligiloj.ligilo = ligilo if kwargs.get('ligilo_id', False) else universo_resursoj_kategorioj_resursoj_ligiloj.ligilo
                                universo_resursoj_kategorioj_resursoj_ligiloj.tipo = tipo if kwargs.get('tipo_id', False) else universo_resursoj_kategorioj_resursoj_ligiloj.tipo

                                universo_resursoj_kategorioj_resursoj_ligiloj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoResursoKategorioLigilo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoResursoKategorioLigilo(status=status, message=message, universo_resursoj_kategorioj_resursoj_ligiloj=universo_resursoj_kategorioj_resursoj_ligiloj)


# Модель типов ресурсов Универсо
class RedaktuUniversoResursoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_resursoj_tipoj = graphene.Field(UniversoResursoTipoNode, 
        description=_('Созданный/изменённый тип ресурсов в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_resursoj_tipoj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_resursoj.povas_krei_universo_resursoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_resursoj_tipoj = UniversoResursoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_resursoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_resursoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_resursoj_tipoj.realeco.set(realeco)
                                else:
                                    universo_resursoj_tipoj.realeco.clear()

                            universo_resursoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_resursoj_tipoj = UniversoResursoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_resursoj.povas_forigi_universo_resursoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_resursoj.povas_shanghi_universo_resursoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_resursoj_tipoj.forigo = kwargs.get('forigo', universo_resursoj_tipoj.forigo)
                                universo_resursoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_resursoj_tipoj.arkivo = kwargs.get('arkivo', universo_resursoj_tipoj.arkivo)
                                universo_resursoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_resursoj_tipoj.publikigo = kwargs.get('publikigo', universo_resursoj_tipoj.publikigo)
                                universo_resursoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_resursoj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_resursoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_resursoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_resursoj_tipoj.realeco.set(realeco)
                                    else:
                                        universo_resursoj_tipoj.realeco.clear()

                                universo_resursoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoResursoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoResursoTipo(status=status, message=message, universo_resursoj_tipoj=universo_resursoj_tipoj)


# Модель классов ресурсов Универсо
class RedaktuUniversoResursoKlaso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_resursoj_klasoj = graphene.Field(UniversoResursoKlasoNode, 
        description=_('Созданный/изменённый класс ресурсов в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_resursoj_klasoj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_resursoj.povas_krei_universo_resursoj_klasoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_resursoj_klasoj = UniversoResursoKlaso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_resursoj_klasoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_resursoj_klasoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_resursoj_klasoj.realeco.set(realeco)
                                else:
                                    universo_resursoj_klasoj.realeco.clear()

                            universo_resursoj_klasoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_resursoj_klasoj = UniversoResursoKlaso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_resursoj.povas_forigi_universo_resursoj_klasoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_resursoj.povas_shanghi_universo_resursoj_klasoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_resursoj_klasoj.forigo = kwargs.get('forigo', universo_resursoj_klasoj.forigo)
                                universo_resursoj_klasoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_resursoj_klasoj.arkivo = kwargs.get('arkivo', universo_resursoj_klasoj.arkivo)
                                universo_resursoj_klasoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_resursoj_klasoj.publikigo = kwargs.get('publikigo', universo_resursoj_klasoj.publikigo)
                                universo_resursoj_klasoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_resursoj_klasoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_resursoj_klasoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_resursoj_klasoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_resursoj_klasoj.realeco.set(realeco)
                                    else:
                                        universo_resursoj_klasoj.realeco.clear()

                                universo_resursoj_klasoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoResursoKlaso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoResursoKlaso(status=status, message=message, universo_resursoj_klasoj=universo_resursoj_klasoj)


# Модель типов мест хранения ресурсов Универсо
class RedaktuUniversoResursoStokejoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_resursoj_stokejoj_tipoj = graphene.Field(UniversoResursoStokejoTipoNode, 
        description=_('Созданный/изменённый тип мест хранения ресурсов Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_resursoj_stokejoj_tipoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_resursoj.povas_krei_universo_resursoj_stokejoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_resursoj_stokejoj_tipoj = UniversoResursoStokejoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_resursoj_stokejoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_resursoj_stokejoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_resursoj_stokejoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_resursoj_stokejoj_tipoj = UniversoResursoStokejoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_resursoj.povas_forigi_universo_resursoj_stokejoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_resursoj.povas_shanghi_universo_resursoj_stokejoj_tipoj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_resursoj_stokejoj_tipoj.forigo = kwargs.get('forigo', universo_resursoj_stokejoj_tipoj.forigo)
                                universo_resursoj_stokejoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_resursoj_stokejoj_tipoj.arkivo = kwargs.get('arkivo', universo_resursoj_stokejoj_tipoj.arkivo)
                                universo_resursoj_stokejoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_resursoj_stokejoj_tipoj.publikigo = kwargs.get('publikigo', universo_resursoj_stokejoj_tipoj.publikigo)
                                universo_resursoj_stokejoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_resursoj_stokejoj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_resursoj_stokejoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_resursoj_stokejoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_resursoj_stokejoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoResursoStokejoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoResursoStokejoTipo(status=status, message=message, universo_resursoj_stokejoj_tipoj=universo_resursoj_stokejoj_tipoj)


# Модель ресурсов Универсо
class RedaktuUniversoResurso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_resursoj = graphene.Field(UniversoResursoNode, 
        description=_('Созданный/изменённый ресурс Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий ресурсов Универсо'))
        tipo_id = graphene.Int(description=_('Код типа ресурсов Универсо'))
        klaso_id = graphene.Int(description=_('Код класса ресурсов Универсо'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_resursoj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        kategorio = UniversoResursoKategorio.objects.none()
        tipo = None
        klaso = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_resursoj.povas_krei_universo_resursoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одно значение'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            if 'kategorio' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                            elif not len(kwargs.get('kategorio')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одно значение'),'kategorio')
                            else:
                                kategorio_id = set(kwargs.get('kategorio'))

                                if len(kategorio_id):
                                    kategorio = UniversoResursoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие категория ресурсов Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoResursoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoResursoTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип ресурсов Универсо'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип ресурсов Универсо'),'tipo_id')

                        if not message:
                            if 'klaso_id' in kwargs:
                                try:
                                    klaso = UniversoResursoKlaso.objects.get(id=kwargs.get('klaso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoResursoKlaso.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный класс ресурсов Универсо'),'klaso_id')
                            else:
                                message = '{}: {}'.format(_('Не указан класс ресурсов Универсо'),'klaso_id')

                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_resursoj = UniversoResurso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                tipo = tipo,
                                klaso = klaso,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_resursoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_resursoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_resursoj.realeco.set(realeco)
                                else:
                                    universo_resursoj.realeco.clear()

                            if 'kategorio' in kwargs:
                                if kategorio:
                                    universo_resursoj.kategorio.set(kategorio)
                                else:
                                    universo_resursoj.kategorio.clear()

                            universo_resursoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False) or kwargs.get('kategorio', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('klaso_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_resursoj = UniversoResurso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_resursoj.povas_forigi_universo_resursoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_resursoj.povas_shanghi_universo_resursoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))

                                    if len(kategorio_id):
                                        kategorio = UniversoResursoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категория ресурсов Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoResursoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoResursoTipo.DoesNotExist:
                                        message = _('Неверный тип ресурсов Универсо')
                            if not message:
                                if 'klaso_id' in kwargs:
                                    try:
                                        klaso = UniversoResursoKlaso.objects.get(id=kwargs.get('klaso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoResursoKlaso.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный класс ресурсов Универсо'),'klaso_id')

                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_resursoj.forigo = kwargs.get('forigo', universo_resursoj.forigo)
                                universo_resursoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_resursoj.arkivo = kwargs.get('arkivo', universo_resursoj.arkivo)
                                universo_resursoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_resursoj.publikigo = kwargs.get('publikigo', universo_resursoj.publikigo)
                                universo_resursoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_resursoj.autoro = autoro
                                universo_resursoj.tipo = tipo if kwargs.get('tipo_id', False) else universo_resursoj.tipo
                                universo_resursoj.klaso = klaso if kwargs.get('klaso_id', False) else universo_resursoj.klaso

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_resursoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_resursoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_resursoj.realeco.set(realeco)
                                    else:
                                        universo_resursoj.realeco.clear()
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        universo_resursoj.kategorio.set(kategorio)
                                    else:
                                        universo_resursoj.kategorio.clear()

                                universo_resursoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoResurso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoResurso(status=status, message=message, universo_resursoj=universo_resursoj)


# Модель логических мест хранения ресурсов Универсо
class RedaktuUniversoResursoStokejo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_resursoj_stokejoj = graphene.Field(UniversoResursoStokejoNode, 
        description=_('Созданные/изменённые логические места хранения ресурсов Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        posedanto_uuid = graphene.String(description=_('Ресурс владелец места хранения'))
        tipo_id = graphene.Int(description=_('Тип места хранения ресурсов на основе которого создано это место хранения в ресурсе'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        posedanto = None
        tipo = None
        universo_resursoj_stokejoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_resursoj.povas_krei_universo_resursoj_stokejoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')
                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoResursoStokejoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoResursoStokejoTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип места хранения ресурсов Универсо'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип места хранения ресурсов Универсо'),'tipo_id')

                        if not message:
                            if 'posedanto_uuid' in kwargs:
                                try:
                                    posedanto = UniversoResurso.objects.get(id=kwargs.get('posedanto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoResurso.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный ресурс владелец места хранения Универсо'),'posedanto_uuid')
                            else:
                                message = '{}: {}'.format(_('Не указан ресурс владелец места хранения Универсо'),'posedanto_uuid')

                        if not message:
                            universo_resursoj_stokejoj = UniversoResursoStokejo.objects.create(
                                forigo=False,
                                arkivo=False,
                                tipo=tipo,
                                posedanto=posedanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_resursoj_stokejoj.nomo, 
                                           kwargs.get('nomo'), 
                                           info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_resursoj_stokejoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_resursoj_stokejoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('priskribo', False)
                            or kwargs.get('posedanto_uuid', False)
                            or kwargs.get('nomo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_resursoj_stokejoj = UniversoResursoStokejo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_resursoj.povas_forigi_universo_resursoj_stokejoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_resursoj.povas_shanghi_universo_resursoj_stokejoj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoResursoStokejoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoResursoStokejoTipo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный тип места хранения ресурсов Универсо'),'tipo_id')
                                else:
                                    message = '{}: {}'.format(_('Не указан тип места хранения ресурсов Универсо'),'tipo_id')

                            if not message:
                                if 'posedanto_uuid' in kwargs:
                                    try:
                                        posedanto = UniversoResurso.objects.get(id=kwargs.get('posedanto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoResurso.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный ресурс владелец места хранения Универсо'),'posedanto_uuid')
                                else:
                                    message = '{}: {}'.format(_('Не указан ресурс владелец места хранения Универсо'),'posedanto_uuid')

                            if not message:

                                universo_resursoj_stokejoj.forigo = kwargs.get('forigo', universo_resursoj_stokejoj.forigo)
                                universo_resursoj_stokejoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_resursoj_stokejoj.arkivo = kwargs.get('arkivo', universo_resursoj_stokejoj.arkivo)
                                universo_resursoj_stokejoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_resursoj_stokejoj.publikigo = kwargs.get('publikigo', universo_resursoj_stokejoj.publikigo)
                                universo_resursoj_stokejoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_resursoj_stokejoj.tipo = tipo if kwargs.get('tipo_id', False) else universo_resursoj_stokejoj.tipo
                                universo_resursoj_stokejoj.posedanto = posedanto if kwargs.get('posedanto_uuid', False) else universo_resursoj_stokejoj.posedanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_resursoj_stokejoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_resursoj_stokejoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_resursoj_stokejoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoResursoStokejo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoResursoStokejo(status=status, message=message, universo_resursoj_stokejoj=universo_resursoj_stokejoj)


# Модель типов цен ресурсов
class RedaktuUniversoResursoPrezoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_resursoj_prezoj_tipoj = graphene.Field(UniversoResursoPrezoTipoNode, 
        description=_('Созданная/изменённая модель типов цен ресурсов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_resursoj_prezoj_tipoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_resursoj.povas_krei_universo_resursoj_prezoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_resursoj_prezoj_tipoj = UniversoResursoPrezoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_resursoj_prezoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_resursoj_prezoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_resursoj_prezoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_resursoj_prezoj_tipoj = UniversoResursoPrezoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_resursoj.povas_forigi_universo_resursoj_prezoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_resursoj.povas_shanghi_universo_resursoj_prezoj_tipoj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_resursoj_prezoj_tipoj.forigo = kwargs.get('forigo', universo_resursoj_prezoj_tipoj.forigo)
                                universo_resursoj_prezoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_resursoj_prezoj_tipoj.arkivo = kwargs.get('arkivo', universo_resursoj_prezoj_tipoj.arkivo)
                                universo_resursoj_prezoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_resursoj_prezoj_tipoj.publikigo = kwargs.get('publikigo', universo_resursoj_prezoj_tipoj.publikigo)
                                universo_resursoj_prezoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_resursoj_prezoj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_resursoj_prezoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_resursoj_prezoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_resursoj_prezoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoResursoPrezoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoResursoPrezoTipo(status=status, message=message, universo_resursoj_prezoj_tipoj=universo_resursoj_prezoj_tipoj)


# Модель видов цен ресурсов
class RedaktuUniversoResursoPrezoSpeco(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_resursoj_prezoj_specoj = graphene.Field(UniversoResursoPrezoSpecoNode, 
        description=_('Созданная/изменённая модель видов цен ресурсов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_resursoj_prezoj_specoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_resursoj.povas_krei_universo_resursoj_prezoj_specoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_resursoj_prezoj_specoj = UniversoResursoPrezoSpeco.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_resursoj_prezoj_specoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_resursoj_prezoj_specoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_resursoj_prezoj_specoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_resursoj_prezoj_specoj = UniversoResursoPrezoSpeco.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_resursoj.povas_forigi_universo_resursoj_prezoj_specoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_resursoj.povas_shanghi_universo_resursoj_prezoj_specoj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_resursoj_prezoj_specoj.forigo = kwargs.get('forigo', universo_resursoj_prezoj_specoj.forigo)
                                universo_resursoj_prezoj_specoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_resursoj_prezoj_specoj.arkivo = kwargs.get('arkivo', universo_resursoj_prezoj_specoj.arkivo)
                                universo_resursoj_prezoj_specoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_resursoj_prezoj_specoj.publikigo = kwargs.get('publikigo', universo_resursoj_prezoj_specoj.publikigo)
                                universo_resursoj_prezoj_specoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_resursoj_prezoj_specoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_resursoj_prezoj_specoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_resursoj_prezoj_specoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_resursoj_prezoj_specoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoResursoPrezoSpeco.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoResursoPrezoSpeco(status=status, message=message, universo_resursoj_prezoj_specoj=universo_resursoj_prezoj_specoj)


# Модель статусов цен ресурсов
class RedaktuUniversoResursoPrezoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_resursoj_prezoj_statusoj = graphene.Field(UniversoResursoPrezoStatusoNode, 
        description=_('Созданная/изменённая модель статусов цен ресурсов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_resursoj_prezoj_statusoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_resursoj.povas_krei_universo_resursoj_prezoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_resursoj_prezoj_statusoj = UniversoResursoPrezoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_resursoj_prezoj_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_resursoj_prezoj_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_resursoj_prezoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_resursoj_prezoj_statusoj = UniversoResursoPrezoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_resursoj.povas_forigi_universo_resursoj_prezoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_resursoj.povas_shanghi_universo_resursoj_prezoj_statusoj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_resursoj_prezoj_statusoj.forigo = kwargs.get('forigo', universo_resursoj_prezoj_statusoj.forigo)
                                universo_resursoj_prezoj_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_resursoj_prezoj_statusoj.arkivo = kwargs.get('arkivo', universo_resursoj_prezoj_statusoj.arkivo)
                                universo_resursoj_prezoj_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_resursoj_prezoj_statusoj.publikigo = kwargs.get('publikigo', universo_resursoj_prezoj_statusoj.publikigo)
                                universo_resursoj_prezoj_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_resursoj_prezoj_statusoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_resursoj_prezoj_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_resursoj_prezoj_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_resursoj_prezoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoResursoPrezoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoResursoPrezoStatuso(status=status, message=message, universo_resursoj_prezoj_statusoj=universo_resursoj_prezoj_statusoj)


# Модель цен ресурсов
class RedaktuUniversoResursoPrezo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_resursoj_prezoj = graphene.Field(UniversoResursoPrezoNode, 
        description=_('Созданная/изменённая модель цен ресурсов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        resurso_id = graphene.Int(description=_('Ресурс для которого устанавливается цена'))
        posedanto_uzanto_siriuso_uzanto_id = graphene.Int(description=_('Пользователь владелец цены ресурса'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец цены ресурса'))
        prezo = graphene.Float(description=_('Значение цены, максимальное значение 15 знаков, из них 2 после точки'))
        tipo_id = graphene.Int(description=_('Тип цены ресурса'))
        speco_id = graphene.Int(description=_('Вид цены ресурса'))
        statuso_id = graphene.Int(description=_('Статус цены ресурса'))
        realeco_id = graphene.Int(description=_('ID параллельного мира Универсо'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_resursoj_prezoj = None
        resurso = None
        posedanto_uzanto = None
        posedanto_organizo = None
        tipo = None
        speco = None
        statuso = None
        realeco = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_resursoj.povas_krei_universo_resursoj_prezoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if 'resurso_id' in kwargs:
                                try:
                                    resurso = UniversoResurso.objects.get(id=kwargs.get('resurso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoResurso.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный ресурс для которого устанавливается цена'),'resurso_id')
                            else:
                                message = '{}: {}'.format(_('Не указан ресурс для которого устанавливается цена'),'resurso_id')

                        if not message:
                            if (kwargs.get('posedanto_uzanto_siriuso_uzanto_id', False)):
                                try:
                                    posedanto_uzanto = UniversoUzanto.objects.get(
                                        siriuso_uzanto__id=kwargs.get('posedanto_uzanto_siriuso_uzanto_id'), 
                                        publikigo=True
                                    )
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')

                        if not message:
                            if (kwargs.get('posedanto_organizo_uuid', False)):
                                try:
                                    posedanto_organizo = UniversoOrganizo.objects.get(
                                        uuid=kwargs.get('posedanto_organizo_uuid'), 
                                        forigo=False,
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except UniversoOrganizo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный организация владелец цены ресурса Универсо'),
                                                               'posedanto_organizo_uuid')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoResursoPrezoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                                arkivo=False, publikigo=True)
                                except UniversoResursoPrezoTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип цены ресурсов Универсо'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип цены ресурсов Универсо'),'tipo_id')

                        if not message:
                            if 'speco_id' in kwargs:
                                try:
                                    speco = UniversoResursoPrezoSpeco.objects.get(id=kwargs.get('speco_id'), forigo=False,
                                                                                  arkivo=False, publikigo=True)
                                except UniversoResursoPrezoSpeco.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный вид цены ресурсов Универсо'),'speco_id')
                            else:
                                message = '{}: {}'.format(_('Не указан вид цены ресурсов Универсо'),'speco_id')

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = UniversoResursoPrezoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                                      arkivo=False, publikigo=True)
                                except UniversoResursoPrezoStatuso.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный статус цены ресурсов Универсо'),'statuso_id')
                            else:
                                message = '{}: {}'.format(_('Не указан статус цены ресурсов Универсо'),'statuso_id')

                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                                      arkivo=False, publikigo=True)
                                except UniversoRealeco.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверная реальность Универсо'),'realeco_id')

                        if not message:
                            universo_resursoj_prezoj = UniversoResursoPrezo.objects.create(
                                forigo=False,
                                arkivo=False,
                                realeco=realeco,
                                resurso=resurso,
                                posedanto_uzanto=posedanto_uzanto,
                                posedanto_organizo=posedanto_organizo,
                                tipo=tipo,
                                prezo=kwargs.get('prezo', None),
                                speco=speco,
                                statuso=statuso,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            universo_resursoj_prezoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('resurso_id', False) or kwargs.get('posedanto_uzanto_siriuso_uzanto_id', False)
                            or kwargs.get('posedanto_organizo_uuid', False) or kwargs.get('tipo_id', False)
                            or kwargs.get('prezo', False) or kwargs.get('speco_id', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('realeco_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_resursoj_prezoj = UniversoResursoPrezo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_resursoj.povas_forigi_universo_resursoj_prezoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_resursoj.povas_shanghi_universo_resursoj_prezoj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                if 'resurso_id' in kwargs:
                                    try:
                                        resurso = UniversoResurso.objects.get(id=kwargs.get('resurso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoResurso.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный ресурс для которого устанавливается цена'),'resurso_id')

                            if not message:
                                if (kwargs.get('posedanto_uzanto_siriuso_uzanto_id', False)):
                                    try:
                                        posedanto_uzanto = UniversoUzanto.objects.get(
                                            siriuso_uzanto__id=kwargs.get('posedanto_uzanto_siriuso_uzanto_id'), 
                                            publikigo=True
                                        )
                                    except UniversoUzanto.DoesNotExist:
                                        message = _('Неверный пользователь Universo')

                            if not message:
                                if (kwargs.get('posedanto_organizo_uuid', False)):
                                    try:
                                        posedanto_organizo = UniversoOrganizo.objects.get(
                                            uuid=kwargs.get('posedanto_organizo_uuid'), 
                                            forigo=False,
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except UniversoOrganizo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный организация владелец цены ресурса Универсо'),
                                                                'posedanto_organizo_uuid')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoResursoPrezoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                                    arkivo=False, publikigo=True)
                                    except UniversoResursoPrezoTipo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный тип цены ресурсов Универсо'),'tipo_id')

                            if not message:
                                if 'speco_id' in kwargs:
                                    try:
                                        speco = UniversoResursoPrezoSpeco.objects.get(id=kwargs.get('speco_id'), forigo=False,
                                                                                    arkivo=False, publikigo=True)
                                    except UniversoResursoPrezoSpeco.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный вид цены ресурсов Универсо'),'speco_id')

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = UniversoResursoPrezoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                                        arkivo=False, publikigo=True)
                                    except UniversoResursoPrezoStatuso.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный статус цены ресурсов Универсо'),'statuso_id')

                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                                        arkivo=False, publikigo=True)
                                    except UniversoRealeco.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверная реальность Универсо'),'realeco_id')

                            if not message:

                                universo_resursoj_prezoj.forigo = kwargs.get('forigo', universo_resursoj_prezoj.forigo)
                                universo_resursoj_prezoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_resursoj_prezoj.arkivo = kwargs.get('arkivo', universo_resursoj_prezoj.arkivo)
                                universo_resursoj_prezoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_resursoj_prezoj.publikigo = kwargs.get('publikigo', universo_resursoj_prezoj.publikigo)
                                universo_resursoj_prezoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_resursoj_prezoj.resurso = resurso if kwargs.get('resurso_id', False) else universo_resursoj_prezoj.resurso
                                universo_resursoj_prezoj.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_siriuso_uzanto_id', False) else universo_resursoj_prezoj.posedanto_uzanto
                                universo_resursoj_prezoj.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) else universo_resursoj_prezoj.posedanto_organizo
                                universo_resursoj_prezoj.tipo = tipo if kwargs.get('tipo_id', False) else universo_resursoj_prezoj.tipo
                                universo_resursoj_prezoj.speco = speco if kwargs.get('speco_id', False) else universo_resursoj_prezoj.speco
                                universo_resursoj_prezoj.statuso = statuso if kwargs.get('statuso_id', False) else universo_resursoj_prezoj.statuso
                                universo_resursoj_prezoj.realeco = realeco if kwargs.get('realeco_id', False) else universo_resursoj_prezoj.realeco
                                universo_resursoj_prezoj.prezo = kwargs.get('prezo',  universo_resursoj_prezoj.prezo)

                                universo_resursoj_prezoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoResursoPrezo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoResursoPrezo(status=status, message=message, universo_resursoj_prezoj=universo_resursoj_prezoj)


# Модель типов связей ресурсов между собой
class RedaktuUniversoResursoLigiloTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_resursoj_ligiloj_tipoj = graphene.Field(UniversoResursoLigiloTipoNode, 
        description=_('Созданная/изменённая модель типов связей ресурсов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_resursoj_ligiloj_tipoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_resursoj.povas_krei_universo_resursoj_ligiloj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_resursoj_ligiloj_tipoj = UniversoResursoLigiloTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_resursoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_resursoj_ligiloj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_resursoj_ligiloj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_resursoj_ligiloj_tipoj = UniversoResursoLigiloTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_resursoj.povas_forigi_universo_resursoj_ligiloj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_resursoj.povas_shanghi_universo_resursoj_ligiloj_tipoj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_resursoj_ligiloj_tipoj.forigo = kwargs.get('forigo', universo_resursoj_ligiloj_tipoj.forigo)
                                universo_resursoj_ligiloj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_resursoj_ligiloj_tipoj.arkivo = kwargs.get('arkivo', universo_resursoj_ligiloj_tipoj.arkivo)
                                universo_resursoj_ligiloj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_resursoj_ligiloj_tipoj.publikigo = kwargs.get('publikigo', universo_resursoj_ligiloj_tipoj.publikigo)
                                universo_resursoj_ligiloj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_resursoj_ligiloj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_resursoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_resursoj_ligiloj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_resursoj_ligiloj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoResursoLigiloTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoResursoLigiloTipo(status=status, message=message, universo_resursoj_ligiloj_tipoj=universo_resursoj_ligiloj_tipoj)


# Модель связей ресурсов между собой
class RedaktuUniversoResursoLigilo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_resursoj_ligiloj = graphene.Field(UniversoResursoLigiloNode, 
        description=_('Созданная/изменённая модель связей ресурсов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        posedanto_uuid = graphene.String(description=_('Ресурс владелец связи'))
        posedanto_stokejo_uuid = graphene.String(description=_('Место хранения владельца связи'))
        ligilo_uuid = graphene.String(description=_('Связываемый ресурс'))
        tipo_id = graphene.String(description=_('Тип связи ресурсов'))
        konektilo = graphene.Int(description=_('Разъём (слот), который занимает этот ресурс у родительского ресурса'))
        priskribo = graphene.String(description=_('Описание'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_resursoj_ligiloj = None
        posedanto = None
        posedanto_stokejo = None
        ligilo = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_resursoj.povas_krei_universo_resursoj_ligiloj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if 'posedanto_uuid' in kwargs:
                                try:
                                    posedanto = UniversoResurso.objects.get(uuid=kwargs.get('posedanto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoResurso.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный ресурс владелец связи Универсо'),'posedanto_uuid')
                            else:
                                message = '{}: {}'.format(_('Не указан ресурс владелец связи Универсо'),'posedanto_uuid')

                        if not message:
                            if 'posedanto_stokejo_uuid' in kwargs:
                                try:
                                    posedanto_stokejo = UniversoResursoStokejo.objects.get(uuid=kwargs.get('posedanto_stokejo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoResursoStokejo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверное место хранения владельца связи Универсо'),'posedanto_stokejo_uuid')

                        if not message:
                            if 'ligilo_uuid' in kwargs:
                                try:
                                    ligilo = UniversoResurso.objects.get(uuid=kwargs.get('ligilo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoResurso.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный связываемый ресурс Универсо'),'ligilo_uuid')
                            else:
                                message = '{}: {}'.format(_('Не указан связываемый ресурс Универсо'),'ligilo_uuid')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoResursoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoResursoLigiloTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип связи ресурсов Универсо'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип связи ресурсов Универсо'),'tipo_id')

                        if not message:
                            universo_resursoj_ligiloj = UniversoResursoLigilo.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto=posedanto,
                                posedanto_stokejo=posedanto_stokejo,
                                ligilo=ligilo,
                                tipo=tipo,
                                konektilo=kwargs.get('konektilo', None),
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_resursoj_ligiloj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_resursoj_ligiloj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto_uuid', False) or kwargs.get('posedanto_stokejo_uuid', False)
                            or kwargs.get('ligilo_uuid', False) or kwargs.get('tipo_id', False)
                            or kwargs.get('konektilo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_resursoj_ligiloj = UniversoResursoLigilo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_resursoj.povas_forigi_universo_resursoj_ligiloj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_resursoj.povas_shanghi_universo_resursoj_ligiloj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                if 'posedanto_uuid' in kwargs:
                                    try:
                                        posedanto = UniversoResurso.objects.get(uuid=kwargs.get('posedanto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoResurso.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный ресурс владелец связи Универсо'),'posedanto_uuid')

                            if not message:
                                if 'posedanto_stokejo_uuid' in kwargs:
                                    try:
                                        posedanto_stokejo = UniversoResursoStokejo.objects.get(uuid=kwargs.get('posedanto_stokejo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoResursoStokejo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверное место хранения владельца связи Универсо'),'posedanto_stokejo_uuid')

                            if not message:
                                if 'ligilo_uuid' in kwargs:
                                    try:
                                        ligilo = UniversoResurso.objects.get(uuid=kwargs.get('ligilo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoResurso.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный связываемый ресурс Универсо'),'ligilo_uuid')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoResursoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoResursoLigiloTipo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный тип связи ресурсов Универсо'),'tipo_id')

                            if not message:

                                universo_resursoj_ligiloj.forigo = kwargs.get('forigo', universo_resursoj_ligiloj.forigo)
                                universo_resursoj_ligiloj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_resursoj_ligiloj.arkivo = kwargs.get('arkivo', universo_resursoj_ligiloj.arkivo)
                                universo_resursoj_ligiloj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_resursoj_ligiloj.publikigo = kwargs.get('publikigo', universo_resursoj_ligiloj.publikigo)
                                universo_resursoj_ligiloj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_resursoj_ligiloj.posedanto = posedanto if kwargs.get('posedanto_uuid', False) else universo_resursoj_ligiloj.posedanto
                                universo_resursoj_ligiloj.posedanto_stokejo = posedanto_stokejo if kwargs.get('posedanto_stokejo_uuid', False) else universo_resursoj_ligiloj.posedanto_stokejo
                                universo_resursoj_ligiloj.ligilo = ligilo if kwargs.get('ligilo_uuid', False) else universo_resursoj_ligiloj.ligilo
                                universo_resursoj_ligiloj.tipo = tipo if kwargs.get('tipo_id', False) else universo_resursoj_ligiloj.tipo
                                universo_resursoj_ligiloj.konektilo = kwargs.get('konektilo',  universo_resursoj_ligiloj.konektilo)

                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_resursoj_ligiloj.priskribo, 
                                            kwargs.get('priskribo'), 
                                            info.context.LANGUAGE_CODE)

                                universo_resursoj_ligiloj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoResursoLigilo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoResursoLigilo(status=status, message=message, universo_resursoj_ligiloj=universo_resursoj_ligiloj)


class UniversoResursoMutations(graphene.ObjectType):
    redaktu_universo_resurso_kategorio = RedaktuUniversoResursoKategorio.Field(
        description=_('''Создаёт или редактирует категории ресурсов Универсо''')
    )
    redaktu_universo_resurso_kategorio_ligiloj_tipo = RedaktuUniversoResursoKategorioLigiloTipo.Field(
        description=_('''Создаёт или редактирует модели типов связей категорий ресурсов между собой''')
    )
    redaktu_universo_resurso_kategorio_ligiloj = RedaktuUniversoResursoKategorioLigilo.Field(
        description=_('''Создаёт или редактирует модели связей категорий ресурсов между собой''')
    )
    redaktu_universo_resurso_tipo = RedaktuUniversoResursoTipo.Field(
        description=_('''Создаёт или редактирует типы ресурсов Универсо''')
    )
    redaktu_universo_resurso_klaso = RedaktuUniversoResursoKlaso.Field(
        description=_('''Создаёт или редактирует классы ресурсов Универсо''')
    )
    redaktu_universo_resurso_stokejoj_tipo = RedaktuUniversoResursoStokejoTipo.Field(
        description=_('''Создаёт или редактирует типы мест хранения ресурсов Универсо''')
    )
    redaktu_universo_resurso = RedaktuUniversoResurso.Field(
        description=_('''Создаёт или редактирует ресурсы Универсо''')
    )
    redaktu_universo_resurso_stokejoj = RedaktuUniversoResursoStokejo.Field(
        description=_('''Создаёт или редактирует логические места хранения ресурсов Универсо''')
    )
    redaktu_universo_resurso_prezo_tipo = RedaktuUniversoResursoPrezoTipo.Field(
        description=_('''Создаёт или редактирует модели типов цен ресурсов''')
    )
    redaktu_universo_resurso_prezo_speco = RedaktuUniversoResursoPrezoSpeco.Field(
        description=_('''Создаёт или редактирует модели видов цен ресурсов''')
    )
    redaktu_universo_resurso_prezo_statuso = RedaktuUniversoResursoPrezoStatuso.Field(
        description=_('''Создаёт или редактирует модели статусов цен ресурсов''')
    )
    redaktu_universo_resurso_prezoj = RedaktuUniversoResursoPrezo.Field(
        description=_('''Создаёт или редактирует модели цен ресурсов''')
    )
    redaktu_universo_resurso_ligiloj_tipo = RedaktuUniversoResursoLigiloTipo.Field(
        description=_('''Создаёт или редактирует модели типов связей ресурсов между собой''')
    )
    redaktu_universo_resurso_ligiloj = RedaktuUniversoResursoLigilo.Field(
        description=_('''Создаёт или редактирует модели связей ресурсов между собой''')
    )
