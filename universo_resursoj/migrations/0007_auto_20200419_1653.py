# Generated by Django 2.2.11 on 2020-04-19 16:53

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion
import siriuso.models.postgres
import siriuso.utils.modules
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('universo_uzantoj', '0002_auto_20200327_0357'),
        ('universo_resursoj', '0006_universoresursoligilo_universoresursoligilotipo_universoresursostokejo'),
    ]

    operations = [
        migrations.AddField(
            model_name='universoresursostokejo',
            name='nomo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Titolo'),
        ),
        migrations.CreateModel(
            name='UniversoResursoKategorioLigiloTipo',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(blank=True, default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('id', models.IntegerField(default=0, unique=True, verbose_name='ID')),
                ('nomo', django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Titolo')),
                ('priskribo', django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Priskribo')),
                ('autoro', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='universo_uzantoj.UniversoUzanto', verbose_name='Aŭtoro')),
            ],
            options={
                'verbose_name': 'Tipo de ligiloj de kategorioj de resursoj de Universo',
                'verbose_name_plural': 'Tipoj de ligiloj de kategorioj de resursoj de Universo',
                'db_table': 'universo_resursoj_kategorioj_resursoj_ligiloj_tipoj',
                'permissions': (('povas_vidi_universo_resursoj_kategorioj_resursoj_ligiloj_tipoj', 'Povas vidi tipoj de ligiloj de kategorioj de resursoj de Universo'), ('povas_krei_universo_resursoj_kategorioj_resursoj_ligiloj_tipoj', 'Povas krei tipoj de ligiloj de kategorioj de resursoj de Universo'), ('povas_forigi_universo_resursoj_kategorioj_resursoj_ligiloj_tipoj', 'Povas forigi tipoj de ligiloj de kategorioj de resursoj de Universo'), ('povas_sxangxi_universo_resursoj_kategorioj_resursoj_ligiloj_tipoj', 'Povas ŝanĝi tipoj de ligiloj de kategorioj de resursoj de Universo')),
            },
        ),
        migrations.CreateModel(
            name='UniversoResursoKategorioLigilo',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(blank=True, default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('ligilo', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='universo_resursoj_universoresursokategorioligilo_ligilo', to='universo_resursoj.UniversoResursoKategorio', verbose_name='Kategorio - ligilo')),
                ('posedanto', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='universo_resursoj.UniversoResursoKategorio', verbose_name='Kategorio - posedanto')),
                ('tipo', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='universo_resursoj.UniversoResursoKategorioLigiloTipo', verbose_name='Tipo')),
            ],
            options={
                'verbose_name': 'Ligilo de kategorioj de resursoj de Universo',
                'verbose_name_plural': 'Ligiloj de kategorioj de resursoj de Universo',
                'db_table': 'universo_resursoj_kategorioj_resursoj_ligiloj',
                'permissions': (('povas_vidi_universo_resursoj_kategorioj_resursoj_ligiloj', 'Povas vidi ligiloj de kategorioj de resursoj de Universo'), ('povas_krei_universo_resursoj_kategorioj_resursoj_ligiloj', 'Povas krei ligiloj de kategorioj de resursoj de Universo'), ('povas_forigi_universo_resursoj_kategorioj_resursoj_ligiloj', 'Povas forigi ligiloj de kategorioj de resursoj de Universo'), ('povas_sxangxi_universo_resursoj_kategorioj_resursoj_ligiloj', 'Povas ŝanĝi ligiloj de kategorioj de resursoj de Universo')),
            },
        ),
    ]
