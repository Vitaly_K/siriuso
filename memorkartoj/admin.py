"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from django import forms
from django.utils.translation import ugettext_lazy as _
from siriuso.utils import get_enhavo, set_enhavo
from memorkartoj.models import *
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
import json


class TekstoMixin:
    @staticmethod
    def teksto(obj, field):
        kampo = getattr(obj, field)

        if kampo is not None:
            result = get_enhavo(kampo)

            if result:
                return result[0]
        return ''

    def nomo_teksto(self, obj):
        return self.teksto(obj=obj, field='nomo')

    def priskribo_teksto(self, obj):
        return self.teksto(obj=obj, field='priskribo')

    def teksto_teksto(self, obj):
        return self.teksto(obj=obj, field='teksto')


# Форма для модулей
class MemorkartojModuloFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=True)
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=True)

    class Meta:
        model = MemorkartojModulo
        fields = [field.name for field in MemorkartojModulo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

# Модули
@admin.register(MemorkartojModulo)
class MemorkartojModuloAdmin(admin.ModelAdmin, TekstoMixin):
    form = MemorkartojModuloFormo
    list_display = ('id', 'nomo_teksto', 'priskribo_teksto')
    exclude = ('id',)

    class Meta:
        model = MemorkartojModulo


# Форма для карточек
class MemorkartojKartoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=True)
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=True)

    class Meta:
        model = MemorkartojKarto
        fields = [field.name for field in MemorkartojKarto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

# Карточки
@admin.register(MemorkartojKarto)
class MemorkartojKartoAdmin(admin.ModelAdmin, TekstoMixin):
    form = MemorkartojKartoFormo
    list_display = ('id', 'nomo_teksto', 'priskribo_teksto')
    exclude = ('id',)

    class Meta:
        model = MemorkartojKarto


# Форма для типов тестов/игр
class MemorkartojTestoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=True)
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=True)

    class Meta:
        model = MemorkartojTesto
        fields = [field.name for field in MemorkartojTesto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

# Типы тестов/игр
@admin.register(MemorkartojTesto)
class MemorkartojTestoAdmin(admin.ModelAdmin, TekstoMixin):
    form = MemorkartojTestoFormo
    list_display = ('id', 'nomo_teksto', 'priskribo_teksto')
    exclude = ('id',)

    class Meta:
        model = MemorkartojTesto


# Форма статистики тестов
class MemorkartojStatistikoFormo(forms.ModelForm):

    class Meta:
        model = MemorkartojStatistiko
        fields = [field.name for field in MemorkartojStatistiko._meta.fields if field.name not in ('krea_dato', 'uuid')]

# Статистика тестов
@admin.register(MemorkartojStatistiko)
class MemorkartojStatistikoAdmin(admin.ModelAdmin, TekstoMixin):
    form = MemorkartojStatistikoFormo
    # list_display = ('id')
    # exclude = ('id',)

    class Meta:
        model = MemorkartojStatistiko


# Форма подписки на чужие модули
class MemorkartojAbonoFormo(forms.ModelForm):

    class Meta:
        model = MemorkartojAbono
        fields = [field.name for field in MemorkartojAbono._meta.fields if field.name not in ('krea_dato', 'uuid')]

# подписка на чужие модули
@admin.register(MemorkartojAbono)
class MemorkartojAbonoAdmin(admin.ModelAdmin, TekstoMixin):
    form = MemorkartojAbonoFormo
    # list_display = ('id')
    # exclude = ('id',)

    class Meta:
        model = MemorkartojAbono

