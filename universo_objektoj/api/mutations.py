"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
16.05.2019
"""
from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from universo_uzantoj.models import UniversoUzanto
from .schema import *
from ..models import *


# Модель объектов в Универсо
class RedaktuUniversoObjekto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_objektoj = graphene.Field(UniversoObjektoNode, 
        description=_('Созданный/изменённый объект в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        posedanto_objekto_uuid = graphene.String(description=_('Объект владелец, то есть родительский объект'))
        konektilo = graphene.Int(description=_('Разъём (слот), который занимает этот объект у родительского объекта'))
        resurso_uuid = graphene.String(description=_('Ресурс Универсо на основе которого был создан этот объект'))
        kubo_id = graphene.Int(description=_('Ячейка (куб,чанк) звёздной системы в которой находится объект'))
        koordinato_x = graphene.Float(description=_('Координаты (месторасположение) объекта по оси X в кубе'))
        koordinato_y = graphene.Float(description=_('Координаты (месторасположение) объекта по оси Y в кубе'))
        koordinato_z = graphene.Float(description=_('Координаты (месторасположение) объекта по оси Z в кубе'))
        rotacia_x = graphene.Float(description=_('Вращение объекта по оси X в кубе'))
        rotacia_y = graphene.Float(description=_('Вращение объекта по оси Y в кубе'))
        rotacia_z = graphene.Float(description=_('Вращение объекта по оси Z в кубе'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        realeco = None
        posedanto_objekto = None
        resurso = None
        kubo = None
        universo_objektoj = None
        uzanto = info.context.user
        universo_uzanto = None

        if uzanto.is_authenticated:
            with transaction.atomic():
                # находим пользователя Универсо
                try:
                    universo_uzanto = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                except UniversoUzanto.DoesNotExist:
                    message = _('Неверный пользователь Universo')

                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_objektoj.povas_krei_universo_objektoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if (kwargs.get('realeco_id', False)):
                                try:
                                    realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoRealeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная реальность в Универсо'),'realeco_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco_id')

                        if not message:
                            if (kwargs.get('posedanto_objekto_uuid', False)):
                                try:
                                    posedanto_objekto = UniversoObjekto.objects.get(uuid=kwargs.get('posedanto_objekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoObjekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект владелец, то есть родительский объект'),'posedanto_objekto_uuid')

                        if not message:
                            if (kwargs.get('resurso_uuid', False)):
                                try:
                                    resurso = UniversoResurso.objects.get(uuid=kwargs.get('resurso_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoResurso.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный ресурс Универсо на основе которого был создан этот объект'),'resurso_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'resurso_uuid')

                        if not message:
                            if (kwargs.get('kubo_id', False)):
                                try:
                                    kubo = UniversoKosmoStelsistemoKubo.objects.get(id=kwargs.get('kubo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoKosmoStelsistemoKubo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная ячейка (куб,чанк) звёздной системы в которой находится объект'),'kubo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kubo_id')

                        if not message:
                            universo_objektoj = UniversoObjekto.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto_objekto = posedanto_objekto,
                                realeco = realeco,
                                resurso = resurso,
                                kubo = kubo,
                                koordinato_x=kwargs.get('koordinato_x', 0),
                                koordinato_y=kwargs.get('koordinato_y', 0),
                                koordinato_z=kwargs.get('koordinato_z', 0),
                                rotacia_x=kwargs.get('rotacia_x', 0),
                                rotacia_y=kwargs.get('rotacia_y', 0),
                                rotacia_z=kwargs.get('rotacia_z', 0),
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_objektoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_objektoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_objektoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('kubo_id', False)
                            or kwargs.get('posedanto_objekto_uuid', False) or kwargs.get('konektilo', False)
                            or kwargs.get('koordinato_x', False) or kwargs.get('koordinato_y', False)
                            or kwargs.get('koordinato_z', False) or kwargs.get('rotacia_x', False)
                            or kwargs.get('rotacia_y', False) or kwargs.get('rotacia_z', False)
                            or kwargs.get('resurso_uuid', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_objektoj = UniversoObjekto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            # если владелец данного объекта, то можно делать многое
                            posedanto = None
                            if (not uzanto.has_perm('universo_objektoj.povas_forigi_universo_objektoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_objektoj.povas_shanghi_universo_objektoj'):
                                message = _('Недостаточно прав для изменения')
                                try:
                                    posedanto = UniversoObjektoPosedanto.objects.get(objekto=universo_objektoj, 
                                                posedanto_uzanto=universo_uzanto,
                                                forigo=False, arkivo=False, publikigo=True)
                                    message = '' # это владелец, ему можно делать многое. Далее проверка на запреты при наличии posedanto
                                except UniversoObjektoPosedanto.DoesNotExist:
                                    pass


                            # проверяем наличие записей с таким кодом
                            if not message:
                                if (kwargs.get('realeco_id', False)):
                                    try:
                                        realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoRealeco.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная реальность в Универсо'),'realeco_id')

                            if not message:
                                if (kwargs.get('posedanto_objekto_uuid', False)):
                                    if posedanto:
                                        message = '{} "{}"'.format(_('Недостаточно прав для изменения данного поля'),'posedanto_objekto_uuid')
                                    else:
                                        try:
                                            posedanto_objekto = UniversoObjekto.objects.get(uuid=kwargs.get('posedanto_objekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                        except UniversoObjekto.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверный объект владелец, то есть родительский объект'),'posedanto_objekto_uuid')

                            if not message:
                                if (kwargs.get('resurso_uuid', False)):
                                    if posedanto:
                                        message = '{} "{}"'.format(_('Недостаточно прав для изменения данного поля'),'resurso_uuid')
                                    else:
                                        try:
                                            resurso = UniversoResurso.objects.get(uuid=kwargs.get('resurso_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                        except UniversoResurso.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверный ресурс Универсо на основе которого был создан этот объект'),'resurso_uuid')

                            if not message:
                                if (kwargs.get('kubo_id', False)):
                                    try:
                                        kubo = UniversoKosmoStelsistemoKubo.objects.get(id=kwargs.get('kubo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoKosmoStelsistemoKubo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная ячейка (куб,чанк) звёздной системы в которой находится объект'),'kubo_id')

                            if not message:

                                universo_objektoj.forigo = kwargs.get('forigo', universo_objektoj.forigo)
                                universo_objektoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else universo_objektoj.foriga_dato
                                universo_objektoj.arkivo = kwargs.get('arkivo', universo_objektoj.arkivo)
                                universo_objektoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else universo_objektoj.arkiva_dato
                                universo_objektoj.publikigo = kwargs.get('publikigo', universo_objektoj.publikigo)
                                universo_objektoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else universo_objektoj.publikiga_dato
                                universo_objektoj.resurso = resurso if kwargs.get('resurso_uuid', False) else universo_objektoj.resurso
                                universo_objektoj.posedanto_objekto = posedanto_objekto if kwargs.get('posedanto_objekto_uuid', False) else universo_objektoj.posedanto_objekto
                                universo_objektoj.konektilo = kwargs.get('konektilo', universo_objektoj.konektilo) 
                                universo_objektoj.realeco = realeco if kwargs.get('realeco_id', False) else universo_objektoj.realeco
                                universo_objektoj.kubo = kubo if kwargs.get('kubo_id', False) else universo_objektoj.kubo
                                universo_objektoj.koordinato_x = kwargs.get('koordinato_x', universo_objektoj.koordinato_x)
                                universo_objektoj.koordinato_y = kwargs.get('koordinato_y', universo_objektoj.koordinato_y)
                                universo_objektoj.koordinato_z = kwargs.get('koordinato_z', universo_objektoj.koordinato_z)
                                universo_objektoj.rotacia_x = kwargs.get('rotacia_x', universo_objektoj.rotacia_x)
                                universo_objektoj.rotacia_y = kwargs.get('rotacia_y', universo_objektoj.rotacia_y)
                                universo_objektoj.rotacia_z = kwargs.get('rotacia_z', universo_objektoj.rotacia_z)

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_objektoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_objektoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_objektoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoObjekto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoObjekto(status=status, message=message, universo_objektoj=universo_objektoj)


# Модель типов владельцев объектов в Универсо
class RedaktuUniversoObjektoPosedantoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_objektoj_posedantoj_tipoj = graphene.Field(UniversoObjektoPosedantoTipoNode,
        description=_('Созданный/изменённый тип владельцев объектов в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_objektoj_posedantoj_tipoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_objektoj.povas_krei_universo_objektoj_posedantoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_objektoj_posedantoj_tipoj = UniversoObjektoPosedantoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_objektoj_posedantoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_objektoj_posedantoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_objektoj_posedantoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_objektoj_posedantoj_tipoj = UniversoObjektoPosedantoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_objektoj.povas_forigi_universo_objektoj_posedantoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_objektoj.povas_shanghi_universo_objektoj_posedantoj_tipoj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_objektoj_posedantoj_tipoj.forigo = kwargs.get('forigo', universo_objektoj_posedantoj_tipoj.forigo)
                                universo_objektoj_posedantoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_objektoj_posedantoj_tipoj.arkivo = kwargs.get('arkivo', universo_objektoj_posedantoj_tipoj.arkivo)
                                universo_objektoj_posedantoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_objektoj_posedantoj_tipoj.publikigo = kwargs.get('publikigo', universo_objektoj_posedantoj_tipoj.publikigo)
                                universo_objektoj_posedantoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_objektoj_posedantoj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_objektoj_posedantoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_objektoj_posedantoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_objektoj_posedantoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoObjektoPosedantoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoObjektoPosedantoTipo(status=status, message=message, universo_objektoj_posedantoj_tipoj=universo_objektoj_posedantoj_tipoj)


# Модель статусов владельцев в рамках владения объектом Универсо
class RedaktuUniversoObjektoPosedantoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_objektoj_posedantoj_statusoj = graphene.Field(UniversoObjektoPosedantoStatusoNode,
        description=_('Созданный/изменённый тип статусов владельцев объектов в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_objektoj_posedantoj_statusoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_objektoj.povas_krei_universo_objektoj_posedantoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_objektoj_posedantoj_statusoj = UniversoObjektoPosedantoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_objektoj_posedantoj_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_objektoj_posedantoj_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_objektoj_posedantoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_objektoj_posedantoj_statusoj = UniversoObjektoPosedantoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_objektoj.povas_forigi_universo_objektoj_posedantoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_objektoj.povas_shanghi_universo_objektoj_posedantoj_statusoj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_objektoj_posedantoj_statusoj.forigo = kwargs.get('forigo', universo_objektoj_posedantoj_statusoj.forigo)
                                universo_objektoj_posedantoj_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_objektoj_posedantoj_statusoj.arkivo = kwargs.get('arkivo', universo_objektoj_posedantoj_statusoj.arkivo)
                                universo_objektoj_posedantoj_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_objektoj_posedantoj_statusoj.publikigo = kwargs.get('publikigo', universo_objektoj_posedantoj_statusoj.publikigo)
                                universo_objektoj_posedantoj_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_objektoj_posedantoj_statusoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_objektoj_posedantoj_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_objektoj_posedantoj_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_objektoj_posedantoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoObjektoPosedantoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoObjektoPosedantoStatuso(status=status, message=message, universo_objektoj_posedantoj_statusoj=universo_objektoj_posedantoj_statusoj)


# Модель владельцев объектов в Универсо
class RedaktuUniversoObjektoPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_objektoj_posedantoj = graphene.Field(UniversoObjektoPosedantoNode, 
        description=_('Созданный/изменённый владелец объектов в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        objekto_uuid = graphene.String(description=_('Объект владения'))
        posedanto_uzanto_siriuso_uzanto_id = graphene.Int(description=_('Пользователь владелец объекта Универсо'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец объекта Универсо'))
        parto = graphene.Int(description=_('Размер доли владения'))
        tipo_id = graphene.Int(description=_('Тип владельца объекта'))
        statuso_id = graphene.Int(description=_('Статус владельца объекта'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        posedanto_uzanto = None
        realeco = None
        objekto = None
        posedanto_organizo = None
        tipo = None
        statuso = None
        universo_objektoj_posedantoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_objektoj.povas_krei_universo_objektoj_posedantoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if (kwargs.get('realeco_id', False)):
                                try:
                                    realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoRealeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная реальность в Универсо'),'realeco_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco_id')

                        if not message:
                            if (kwargs.get('posedanto_uzanto_siriuso_uzanto_id', False)):
                                try:
                                    posedanto_uzanto = UniversoUzanto.objects.get(
                                        siriuso_uzanto__id=kwargs.get('posedanto_uzanto_siriuso_uzanto_id'), 
                                        publikigo=True
                                    )
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')

                        if not message:
                            if (kwargs.get('objekto_uuid', False)):
                                try:
                                    objekto = UniversoObjekto.objects.get(
                                        uuid=kwargs.get('objekto_uuid'), 
                                        forigo=False,
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except UniversoObjekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект владения'),'objekto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'objekto_uuid')

                        if not message:
                            if (kwargs.get('posedanto_organizo_uuid', False)):
                                try:
                                    posedanto_organizo = UniversoOrganizo.objects.get(
                                        uuid=kwargs.get('posedanto_organizo_uuid'), 
                                        forigo=False,
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except UniversoOrganizo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный организация владелец объекта Универсо'),
                                                               'posedanto_organizo_uuid')

                        if not message:
                            if (kwargs.get('tipo_id', False)):
                                try:
                                    tipo = UniversoObjektoPosedantoTipo.objects.get(
                                        id=kwargs.get('tipo_id'), 
                                        forigo=False,
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except UniversoObjektoPosedantoTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип владельца объекта'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if (kwargs.get('statuso_id', False)):
                                try:
                                    statuso = UniversoObjektoPosedantoStatuso.objects.get(
                                        id=kwargs.get('statuso_id'), 
                                        forigo=False,
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except UniversoObjektoPosedantoStatuso.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный статус владельца объекта'),'statuso_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')

                        if not message:
                            universo_objektoj_posedantoj = UniversoObjektoPosedanto.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto_uzanto = posedanto_uzanto,
                                objekto = objekto,
                                realeco = realeco,
                                posedanto_organizo = posedanto_organizo,
                                parto = kwargs.get('parto', 100),
                                tipo = tipo,
                                statuso = statuso,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            universo_objektoj_posedantoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('realeco_id', False) 
                            or kwargs.get('posedanto_uzanto_siriuso_uzanto_id', False)
                            or kwargs.get('objekto_uuid', False) or kwargs.get('parto', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('posedanto_organizo_uuid', False)
                            or kwargs.get('tipo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_objektoj_posedantoj = UniversoObjektoPosedanto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_objektoj.povas_forigi_universo_objektoj_posedantoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_objektoj.povas_shanghi_universo_objektoj_posedantoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if (kwargs.get('realeco_id', False)):
                                    try:
                                        realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoRealeco.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная реальность в Универсо'),'realeco_id')

                            if not message:
                                if (kwargs.get('posedanto_uzanto_siriuso_uzanto_id', False)):
                                    try:
                                        posedanto_uzanto = UniversoUzanto.objects.get(
                                            siriuso_uzanto__id=kwargs.get('posedanto_uzanto_siriuso_uzanto_id'), 
                                            publikigo=True
                                        )
                                    except UniversoUzanto.DoesNotExist:
                                        message = _('Неверный пользователь Universo')

                            if not message:
                                if (kwargs.get('objekto_uuid', False)):
                                    try:
                                        objekto = UniversoObjekto.objects.get(
                                            uuid=kwargs.get('objekto_uuid'), 
                                            forigo=False,
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except UniversoObjekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект владения'),'objekto_uuid')

                            if not message:
                                if (kwargs.get('posedanto_organizo_uuid', False)):
                                    try:
                                        posedanto_organizo = UniversoOrganizo.objects.get(
                                            uuid=kwargs.get('posedanto_organizo_uuid'), 
                                            forigo=False,
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except UniversoOrganizo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный организация владелец объекта Универсо'),
                                                                'posedanto_organizo_uuid')

                            if not message:
                                if (kwargs.get('tipo_id', False)):
                                    try:
                                        tipo = UniversoObjektoPosedantoTipo.objects.get(
                                            id=kwargs.get('tipo_id'), 
                                            forigo=False,
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except UniversoObjektoPosedantoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип владельца объекта'),'tipo_id')

                            if not message:
                                if (kwargs.get('statuso_id', False)):
                                    try:
                                        statuso = UniversoObjektoPosedantoStatuso.objects.get(
                                            id=kwargs.get('statuso_id'), 
                                            forigo=False,
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except UniversoObjektoPosedantoStatuso.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный статус владельца объекта'),'statuso_id')

                            if not message:

                                universo_objektoj_posedantoj.forigo = kwargs.get('forigo', universo_objektoj_posedantoj.forigo)
                                universo_objektoj_posedantoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else universo_objektoj_posedantoj.foriga_dato
                                universo_objektoj_posedantoj.arkivo = kwargs.get('arkivo', universo_objektoj_posedantoj.arkivo)
                                universo_objektoj_posedantoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else universo_objektoj_posedantoj.arkiva_dato
                                universo_objektoj_posedantoj.publikigo = kwargs.get('publikigo', universo_objektoj_posedantoj.publikigo)
                                universo_objektoj_posedantoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else universo_objektoj_posedantoj.publikiga_dato
                                universo_objektoj_posedantoj.realeco = realeco if kwargs.get('realeco_id', False) else universo_objektoj_posedantoj.realeco
                                universo_objektoj_posedantoj.objekto = objekto if kwargs.get('objekto_uuid', False) else universo_objektoj_posedantoj.objekto
                                universo_objektoj_posedantoj.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_siriuso_uzanto_id', False) else universo_objektoj_posedantoj.posedanto_uzanto
                                universo_objektoj_posedantoj.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) else universo_objektoj_posedantoj.posedanto_organizo
                                universo_objektoj_posedantoj.parto = kwargs.get('parto', universo_objektoj_posedantoj.parto) 
                                universo_objektoj_posedantoj.tipo = tipo if kwargs.get('tipo_id', False) else universo_objektoj_posedantoj.tipo
                                universo_objektoj_posedantoj.statuso = statuso if kwargs.get('statuso_id', False) else universo_objektoj_posedantoj.statuso

                                universo_objektoj_posedantoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoObjektoPosedanto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoObjektoPosedanto(status=status, message=message, universo_objektoj_posedantoj=universo_objektoj_posedantoj)


# Модель мест хранения в объектах Универсо
class RedaktuUniversoObjektoStokejo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_objektoj_stokejoj = graphene.Field(UniversoObjektoStokejoNode, 
        description=_('Созданное/изменённое место хранения в объектах Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        posedanto_objekto_uuid = graphene.String(description=_('Объект владелец места хранения'))
        tipo_id = graphene.Int(description=_('Тип места хранения ресурсов на основе которого создано это место хранения в объекте'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_objektoj_stokejoj = None
        tipo = None
        posedanto_objekto = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_objektoj.povas_krei_universo_objektoj_stokejoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if (kwargs.get('posedanto_objekto_uuid', False)):
                                try:
                                    posedanto_objekto = UniversoObjekto.objects.get(uuid=kwargs.get('posedanto_objekto_uuid'),
                                        forigo=False, arkivo=False, publikigo=True)
                                except UniversoObjekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект владелец места хранения'),'posedanto_objekto_uuid')

                        if not message:
                            if (kwargs.get('tipo_id', False)):
                                try:
                                    tipo = UniversoResursoStokejoTipo.objects.get(id=kwargs.get('tipo_id'),
                                        forigo=False, arkivo=False, publikigo=True)
                                except UniversoResursoStokejoTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип места хранения ресурсов на основе которого создано это место хранения в объекте'),'tipo_id')

                        if not message:
                            universo_objektoj_stokejoj = UniversoObjektoStokejo.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto_objekto = posedanto_objekto,
                                tipo = tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_objektoj_stokejoj.nomo, 
                                           kwargs.get('nomo'), 
                                           info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_objektoj_stokejoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_objektoj_stokejoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('tipo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('posedanto_objekto', False)
                            or kwargs.get('nomo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_objektoj_stokejoj = UniversoObjektoStokejo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_objektoj.povas_forigi_universo_objektoj_stokejoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_objektoj.povas_shanghi_universo_objektoj_stokejoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if (kwargs.get('posedanto_objekto_uuid', False)):
                                    try:
                                        posedanto_objekto = UniversoObjekto.objects.get(uuid=kwargs.get('posedanto_objekto_uuid'),
                                            forigo=False, arkivo=False, publikigo=True)
                                    except UniversoObjekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект владелец места хранения'),'posedanto_objekto_uuid')

                            if not message:
                                if (kwargs.get('tipo_id', False)):
                                    try:
                                        tipo = UniversoResursoStokejoTipo.objects.get(id=kwargs.get('tipo_id'),
                                            forigo=False, arkivo=False, publikigo=True)
                                    except UniversoResursoStokejoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип места хранения ресурсов на основе которого создано это место хранения в объекте'),'tipo_id')

                            if not message:
                                universo_objektoj_stokejoj.forigo = kwargs.get('forigo', universo_objektoj_stokejoj.forigo)
                                universo_objektoj_stokejoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_objektoj_stokejoj.arkivo = kwargs.get('arkivo', universo_objektoj_stokejoj.arkivo)
                                universo_objektoj_stokejoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_objektoj_stokejoj.publikigo = kwargs.get('publikigo', universo_objektoj_stokejoj.publikigo)
                                universo_objektoj_stokejoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_objektoj_stokejoj.posedanto_objekto = posedanto_objekto if kwargs.get('posedanto_objekto_uuid', False) else universo_objektoj_stokejoj.posedanto_objekto
                                universo_objektoj_stokejoj.tipo = tipo if kwargs.get('tipo_id', False) else universo_objektoj_stokejoj.tipo

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_objektoj_stokejoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_objektoj_stokejoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_objektoj_stokejoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoObjektoStokejo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoObjektoStokejo(status=status, message=message, universo_objektoj_stokejoj=universo_objektoj_stokejoj)


# Модель типов связей объектов между собой в Универсо
class RedaktuUniversoObjektoLigiloTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_objektoj_ligiloj_tipoj = graphene.Field(UniversoObjektoLigiloTipoNode,
        description=_('Созданный/изменённый тип связей объектов между собой в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_objektoj_ligiloj_tipoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_objektoj.povas_krei_universo_objektoj_ligiloj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_objektoj_ligiloj_tipoj = UniversoObjektoLigiloTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_objektoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_objektoj_ligiloj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_objektoj_ligiloj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_objektoj_ligiloj_tipoj = UniversoObjektoLigiloTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_objektoj.povas_forigi_universo_objektoj_ligiloj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_objektoj.povas_shanghi_universo_objektoj_ligiloj_tipoj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_objektoj_ligiloj_tipoj.forigo = kwargs.get('forigo', universo_objektoj_ligiloj_tipoj.forigo)
                                universo_objektoj_ligiloj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_objektoj_ligiloj_tipoj.arkivo = kwargs.get('arkivo', universo_objektoj_ligiloj_tipoj.arkivo)
                                universo_objektoj_ligiloj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_objektoj_ligiloj_tipoj.publikigo = kwargs.get('publikigo', universo_objektoj_ligiloj_tipoj.publikigo)
                                universo_objektoj_ligiloj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_objektoj_ligiloj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_objektoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_objektoj_ligiloj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_objektoj_ligiloj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoObjektoLigiloTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoObjektoLigiloTipo(status=status, message=message, universo_objektoj_ligiloj_tipoj=universo_objektoj_ligiloj_tipoj)


# Модель связи объектов между собой в Универсо
class RedaktuUniversoObjektoLigilo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_objektoj_ligiloj = graphene.Field(UniversoObjektoLigiloNode, 
        description=_('Созданная/изменённая связь объектов между собой в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        tipo_id = graphene.Int(description=_('Код типа места хранения ресурсов на основе которого создано это место хранения в объекте Универсо'))
        posedanto_uuid = graphene.String(description=_('UUID объекта владельца связи'))
        posedanto_stokejo_uuid = graphene.String(description=_('UUID места хранения владельца связи Универсо'))
        ligilo_uuid = graphene.String(description=_('UUID связываемого объекта Универсо'))
        konektilo_posedanto = graphene.Int(description=_('Разъём (слот), который занимает этот объект у родительского объекта'))
        konektilo_ligilo = graphene.Int(description=_('Разъём (слот), который занимает этот объект у связываемого объекта'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_objektoj_ligiloj = None
        posedanto = None
        tipo = None
        posedanto_stokejo = None
        ligilo = None
        uzanto = info.context.user
        universo_uzanto = None

        if uzanto.is_authenticated:
            with transaction.atomic():
                # находим пользователя Универсо
                try:
                    universo_uzanto = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                except UniversoUzanto.DoesNotExist:
                    message = _('Неверный пользователь Universo')
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    # Если запись создаётся по объекту, чьим владельцем является пользователь, то разрешаем
                    posedanto_objekto = None
                    # проверяем по полю posedanto
                    if 'posedanto_uuid' in kwargs:
                        try:
                            objekto = UniversoObjekto.objects.get(uuid=kwargs.get('posedanto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                            try:
                                posedanto_objekto = UniversoObjektoPosedanto.objects.get(objekto=objekto, 
                                            posedanto_uzanto=universo_uzanto,
                                            forigo=False, arkivo=False, publikigo=True)
                            except UniversoObjektoPosedanto.DoesNotExist:
                                pass
                        except UniversoObjekto.DoesNotExist:
                            message = '{} "{}"'.format(_('Неверный объект владелец связи Универсо'),'posedanto_uuid')
                    else:
                        message = '{} "{}"'.format(_('Не указан объект владелец связи Универсо'),'posedanto_uuid')

                    if not posedanto_objekto:
                        # проверяем по полю ligilo
                        if 'ligilo_uuid' in kwargs:
                            try:
                                objekto = UniversoObjekto.objects.get(uuid=kwargs.get('ligilo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                try:
                                    posedanto_objekto = UniversoObjektoPosedanto.objects.get(objekto=objekto, 
                                                posedanto_uzanto=universo_uzanto,
                                                forigo=False, arkivo=False, publikigo=True)
                                except UniversoObjektoPosedanto.DoesNotExist:
                                    pass
                            except UniversoObjekto.DoesNotExist:
                                message = '{} "{}"'.format(_('Неверный объект владелец связи Универсо'),'ligilo_uuid')
                    else:
                        message = '{} "{}"'.format(_('Не указан объект владелец связи Универсо'),'posedanto_uuid')
                    if uzanto.has_perm('universo_objektoj.povas_krei_universo_objektoj_ligiloj') or posedanto_objekto:
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_uuid' in kwargs:
                                try:
                                    posedanto = UniversoObjekto.objects.get(uuid=kwargs.get('posedanto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoObjekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект владелец связи Универсо'),'posedanto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Не указан объект владелец связи Универсо'),'posedanto_uuid')

                        if not message:
                            if 'posedanto_stokejo_uuid' in kwargs:
                                try:
                                    posedanto_stokejo = UniversoObjektoStokejo.objects.get(uuid=kwargs.get('posedanto_stokejo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoObjektoStokejo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверное место хранения владельца связи Универсо'),'posedanto_stokejo_uuid')

                        if not message:
                            if 'ligilo_uuid' in kwargs:
                                try:
                                    ligilo = UniversoObjekto.objects.get(uuid=kwargs.get('ligilo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoObjekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный связываемый объект Универсо'),'ligilo_uuid')
                            else:
                                message = '{} "{}"'.format(_('Не указан связываемый объект Универсо'),'ligilo_uuid')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoObjektoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoObjektoLigiloTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип места хранения ресурсов Универсо'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Не указан тип места хранения ресурсов Универсо'),'tipo_id')

                        if not message:
                            universo_objektoj_ligiloj = UniversoObjektoLigilo.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto = posedanto,
                                posedanto_stokejo = posedanto_stokejo,
                                ligilo = ligilo,
                                tipo=tipo,
                                konektilo_posedanto=kwargs.get('konektilo_posedanto', None),
                                konektilo_ligilo=kwargs.get('konektilo_ligilo', None),
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            universo_objektoj_ligiloj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto_uuid', False) or kwargs.get('posedanto_stokejo_uuid', False)
                            or kwargs.get('ligilo_uuid', False) or kwargs.get('tipo_id', False)
                            or kwargs.get('konektilo_posedanto', False) or kwargs.get('konektilo_ligilo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_objektoj_ligiloj = UniversoObjektoLigilo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            # Если редактируем запись связи по объекту, чьим владельцем является пользователь, то разрешаем
                            posedanto_objekto = None
                            # проверяем по полю posedanto
                            try:
                                posedanto_objekto = UniversoObjektoPosedanto.objects.get(
                                    objekto=universo_objektoj_ligiloj.posedanto, 
                                    posedanto_uzanto=universo_uzanto,
                                    forigo=False, arkivo=False, publikigo=True)
                            except UniversoObjektoPosedanto.DoesNotExist:
                                pass
                            if not posedanto_objekto:
                                # проверяем по полю ligilo
                                try:
                                    posedanto_objekto = UniversoObjektoPosedanto.objects.get(
                                        objekto=universo_objektoj_ligiloj.ligilo, 
                                        posedanto_uzanto=universo_uzanto,
                                        forigo=False, arkivo=False, publikigo=True)
                                except UniversoObjektoPosedanto.DoesNotExist:
                                        pass
                            if (not (uzanto.has_perm('universo_objektoj.povas_forigi_universo_objektoj_ligiloj')
                                    or posedanto_objekto)
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('universo_objektoj.povas_shanghi_universo_objektoj_ligiloj')
                                    or posedanto_objekto):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_uuid' in kwargs:
                                    try:
                                        posedanto = UniversoObjekto.objects.get(uuid=kwargs.get('posedanto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoObjekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект владелец связи Универсо'),'posedanto_uuid')

                            if not message:
                                if 'posedanto_stokejo_uuid' in kwargs:
                                    try:
                                        posedanto_stokejo = UniversoObjektoStokejo.objects.get(uuid=kwargs.get('posedanto_stokejo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoObjektoStokejo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверное место хранения владельца связи Универсо'),'posedanto_stokejo_uuid')

                            if not message:
                                if 'ligilo_uuid' in kwargs:
                                    try:
                                        ligilo = UniversoObjekto.objects.get(uuid=kwargs.get('ligilo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoObjekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный связываемый объект Универсо'),'ligilo_uuid')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoObjektoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoObjektoLigiloTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип места хранения ресурсов Универсо'),'tipo_id')

                            if not message:

                                universo_objektoj_ligiloj.forigo = kwargs.get('forigo', universo_objektoj_ligiloj.forigo)
                                universo_objektoj_ligiloj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_objektoj_ligiloj.arkivo = kwargs.get('arkivo', universo_objektoj_ligiloj.arkivo)
                                universo_objektoj_ligiloj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_objektoj_ligiloj.publikigo = kwargs.get('publikigo', universo_objektoj_ligiloj.publikigo)
                                universo_objektoj_ligiloj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_objektoj_ligiloj.posedanto = posedanto if kwargs.get('posedanto_uuid', False) else universo_objektoj_ligiloj.posedanto
                                universo_objektoj_ligiloj.posedanto_stokejo = posedanto_stokejo if kwargs.get('posedanto_stokejo_uuid', False) else universo_objektoj_ligiloj.posedanto_stokejo
                                universo_objektoj_ligiloj.ligilo = ligilo if kwargs.get('ligilo_uuid', False) else universo_objektoj_ligiloj.ligilo
                                universo_objektoj_ligiloj.tipo = tipo if kwargs.get('tipo_id', False) else universo_objektoj_ligiloj.tipo
                                universo_objektoj_ligiloj.konektilo_posedanto = kwargs.get('konektilo_posedanto', universo_objektoj_ligiloj.konektilo_posedanto) 
                                universo_objektoj_ligiloj.konektilo_ligilo = kwargs.get('konektilo_ligilo', universo_objektoj_ligiloj.konektilo_ligilo) 

                                universo_objektoj_ligiloj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoObjektoLigilo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoObjektoLigilo(status=status, message=message, universo_objektoj_ligiloj=universo_objektoj_ligiloj)


class UniversoObjektoMutations(graphene.ObjectType):
    redaktu_universo_objekto = RedaktuUniversoObjekto.Field(
        description=_('''Создаёт или редактирует объекты в Универсо''')
    )
    redaktu_universo_objekto_posedantoj_tipoj = RedaktuUniversoObjektoPosedantoTipo.Field(
        description=_('''Создаёт или редактирует типы владельцев объектов в Универсо''')
    )
    redaktu_universo_objekto_posedantoj_statusoj = RedaktuUniversoObjektoPosedantoStatuso.Field(
        description=_('''Создаёт или редактирует статусы владельца в рамках владения объектом Универсо''')
    )
    redaktu_universo_objektoj_posedantoj = RedaktuUniversoObjektoPosedanto.Field(
        description=_('''Создаёт или редактирует объекты в Универсо''')
    )
    redaktu_universo_objekto_stokejoj = RedaktuUniversoObjektoStokejo.Field(
        description=_('''Создаёт или редактирует место хранения в объектах Универсо''')
    )
    redaktu_universo_objekto_ligiloj_tipoj = RedaktuUniversoObjektoLigiloTipo.Field(
        description=_('''Создаёт или редактирует типы связей объектов между собой в Универсо''')
    )
    redaktu_universo_objekto_ligiloj = RedaktuUniversoObjektoLigilo.Field(
        description=_('''Создаёт или редактирует связи объектов между собой в Универсо''')
    )
