import graphene  # сам Graphene
import django_filters
from django.utils.translation import ugettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import *  # модели приложения
from universo_taskoj.api.schema import UniversoProjektoNode, UniversoTaskoNode
from universo_taskoj.models import UniversoTasko, UniversoTaskoPosedanto, UniversoProjekto, UniversoProjektoPosedanto


# Модель шаблонов объектов в Универсо
class UniversoObjektoSxablonoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoObjektoSxablono
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto_objekto__uuid': ['exact'],
            'resurso__uuid': ['exact'],
            'kubo__id': ['exact'],
            'kubo__stelsistemo__id': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель владельцев объектов в Универсо
class UniversoObjektoPosedantoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = UniversoObjektoPosedanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'objekto__uuid': ['exact'],
            'posedanto_organizo__uuid': ['exact'],
            'posedanto_uzanto__uuid': ['exact'],
            'posedanto_uzanto__siriuso_uzanto__id': ['exact'],
            'realeco__id': ['exact'],
            'tipo__id': ['exact'],
            'statuso__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связь объектов между собой в Универсо
class UniversoObjektoLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = UniversoObjektoLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'posedanto_stokejo__uuid':['exact'],
            'ligilo__uuid':['exact'],
            'tipo__id':['exact'],
            'tipo__uuid':['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель объектов в Универсо
class UniversoObjektoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    sxablono_sistema_priskribo = graphene.Field(SiriusoLingvo, description=_('Описание системного шаблона'))

    in_cosmo = graphene.Boolean(description=_('Наличие объекта в космосе (с координатами и вне другого объекта)'))

    posedanto = SiriusoFilterConnectionField(UniversoObjektoPosedantoNode,
        description=_('Выводит владельцев объекта'))

    posedanto_id = graphene.Int(description=_('Первый владелец объекта (ID пользователя Сириусо)'))

    # проект
    projekto =  SiriusoFilterConnectionField(UniversoProjektoNode,
        description=_('Выводит проекты, владельцами которых являются объекты'))

    # задача
    tasko =  SiriusoFilterConnectionField(UniversoTaskoNode,
        description=_('Выводит задачи, владельцами которых являются объекты'))

    # Связь объектов между собой
    ligilo =  SiriusoFilterConnectionField(UniversoObjektoLigiloNode,
        description=_('Выводит связи объектов между собой, владельцами которых являются объекты'))

    # Связь объектов между собой со стороны подчинённого
    ligilo_ligilo =  SiriusoFilterConnectionField(UniversoObjektoLigiloNode,
        description=_('Выводит связи объектов между собой, подчинёнными которых являются объекты'))

    class Meta:
        model = UniversoObjekto
        filter_fields = {
            'uuid': ['exact', 'in'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'koordinato_x': ['exact','gt','lt','range', 'isnull'],
            'koordinato_y': ['exact','gt','lt', 'isnull'],
            'koordinato_z': ['exact','gt','lt', 'isnull'],
            'posedanto_objekto__uuid': ['exact'],
            'resurso__uuid': ['exact'],
            'resurso__tipo__id': ['exact'],
            'realeco__id': ['exact'],
            'kubo': ['isnull'],
            'kubo__id': ['exact'],
            'kubo__stelsistemo__id': ['exact'],
            'universo_objektoj_universoobjektoligilo_ligilo_objekto__posedanto__uuid': ['exact'],
            'universo_objektoj_universoobjektoligilo_ligilo_objekto__tipo__id': ['exact'],
            'universo_objektoj_universoobjektoligilo_ligilo_objekto__tipo__uuid': ['exact'],
            'universo_objektoj_universoobjektoligilo_ligilo_objekto__tipo_id': ['exact'],
            'universo_objektoj_universoobjektoligilo_ligilo_objekto': ['exact', 'isnull'],
            'universo_objektoj_universoobjektoligilo_ligilo_objekto__forigo': ['exact'],
            'universo_objektoj_universoobjektoligilo_ligilo_objekto__arkivo': ['exact'],
            'universo_objektoj_universoobjektoligilo_ligilo_objekto__publikigo': ['exact'],

            'universoobjektoligilo__ligilo__uuid': ['exact'],
            'universoobjektoligilo__uuid': ['exact'],
            'universoobjektoligilo__tipo__id': ['exact'],
            'universoobjektoposedanto__uuid': ['exact'],
            'universoobjektoposedanto__posedanto_uzanto__uuid': ['exact'],
            'universoobjektoposedanto__posedanto_uzanto__siriuso_uzanto__id': ['exact'],
            'universoobjektoposedanto__posedanto_uzanto__siriuso_uzanto__uuid': ['exact'],

            'universoobjektouzanto__autoro__siriuso_uzanto__id':['exact'],
            'universoobjektouzanto__autoro__uuid':['exact'],
            'universoobjektouzanto__objekto': ['exact'],
            'universoobjektouzanto': ['isnull'],
        }
        # filterset_class = UniversoObjektoFilter
        interfaces = (graphene.relay.Node,)

    def resolve_in_cosmo(self, info):
        # в космосе, если есть координаты и не находится внутри другого объекта
        in_cosmo = False
        if ((self.koordinato_x) and (self.koordinato_y) and (self.koordinato_z) and (self.posedanto_objekto is None)):
            in_cosmo = True
        return in_cosmo

    def resolve_posedanto(self, info, **kwargs):
        return UniversoObjektoPosedanto.objects.filter(objekto=self, forigo=False, arkivo=False, publikigo=True)

    def resolve_posedanto_id(self, info):
        posed = UniversoObjektoPosedanto.objects.filter(objekto=self, forigo=False, arkivo=False, publikigo=True)
        for pos in posed:
            if (pos.posedanto_uzanto):
                return pos.posedanto_uzanto.siriuso_uzanto.id
        return None

    def resolve_tasko(self, info, **kwargs):
        # находим перечень задач, владельцами которых является объект
        taskoj = UniversoTaskoPosedanto.objects.filter(posedanto_objekto=self, forigo=False, arkivo=False, 
            publikigo=True).values('tasko')
        return UniversoTasko.objects.filter(uuid__in=taskoj, forigo=False, arkivo=False, 
            publikigo=True)

    def resolve_projekto(self, info, **kwargs):
        # находим перечень проектов, владельцами которых является объект
        projektoj = UniversoProjektoPosedanto.objects.filter(posedanto_objekto=self, forigo=False, arkivo=False, 
            publikigo=True).values('projekto')
        return UniversoProjekto.objects.filter(uuid__in=projektoj, forigo=False, arkivo=False, 
            publikigo=True)

    def resolve_ligilo(self, info, **kwargs):
        return UniversoObjektoLigilo.objects.filter(posedanto=self, forigo=False, arkivo=False, publikigo=True)

    def resolve_ligilo_ligilo(self, info, **kwargs):
        return UniversoObjektoLigilo.objects.filter(ligilo=self, forigo=False, arkivo=False, publikigo=True)


# Модель типов владельцев объектов в Универсо
class UniversoObjektoPosedantoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoObjektoPosedantoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов владельца в рамках владения объектом Универсо
class UniversoObjektoPosedantoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoObjektoPosedantoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель мест хранения в объектах Универсо
class UniversoObjektoStokejoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoObjektoStokejo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto_objekto__uuid': ['exact'],
            'tipo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов связей объектов между собой в Универсо
class UniversoObjektoLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoObjektoLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связей пользователя с управляемым объектом в соответствующем мире Универсо
class UniversoObjektoUzantoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = UniversoObjektoUzanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact'],
            'objekto__uuid': ['exact'],
            'realeco__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


class UniversoObjektojQuery(graphene.ObjectType):
    universo_objekto_sxablono = SiriusoFilterConnectionField(
        UniversoObjektoSxablonoNode,
        description=_('Выводит все доступные модели шаблонов объектов в Универсо')
    )
    universo_objekto = SiriusoFilterConnectionField(
        UniversoObjektoNode,
        description=_('Выводит все доступные модели объектов в Универсо')
    )
    universo_objekto_posedanto_tipo = SiriusoFilterConnectionField(
        UniversoObjektoPosedantoTipoNode,
        description=_('Выводит все доступные модели типов владельцев объектов в Универсо')
    )
    universo_objekto_posedanto_statuso = SiriusoFilterConnectionField(
        UniversoObjektoPosedantoStatusoNode,
        description=_('Выводит все доступные модели статусов владельца в рамках владения объектом Универсо')
    )
    universo_objekto_posedanto = SiriusoFilterConnectionField(
        UniversoObjektoPosedantoNode,
        description=_('Выводит все доступные модели владельцев объектов в Универсо')
    )
    universo_objekto_stokejo = SiriusoFilterConnectionField(
        UniversoObjektoStokejoNode,
        description=_('Выводит все доступные модели мест хранения в объектах Универсо')
    )
    universo_objekto_ligilo_tipo = SiriusoFilterConnectionField(
        UniversoObjektoLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей объектов между собой в Универсо')
    )
    universo_objekto_ligilo = SiriusoFilterConnectionField(
        UniversoObjektoLigiloNode,
        description=_('Выводит все доступные связи объектов между собой в Универсо')
    )
    universo_objekto_uzanto = SiriusoFilterConnectionField(
        UniversoObjektoUzantoNode,
        description=_('Выводит все доступные связи пользователя с управляемым объектом в соответствующем мире Универсо')
    )
    filtered_universo_objekto = SiriusoFilterConnectionField(
        UniversoObjektoNode,
        description=_('Выводит отфильтрованные модели объектов в Универсо (которые не связаны подчинёнными или в связях подчинёнными ВСЕ помечены на удаление)')
    )
    @staticmethod
    def resolve_universo_objekto(root, info, **kwargs):
        return UniversoObjekto.objects.all().distinct()

    @staticmethod
    def resolve_filtered_universo_objekto(root, info, **kwargs):
        objektoj = UniversoObjekto.objects.filter(universo_objektoj_universoobjektoligilo_ligilo_objekto__forigo=False,
            universo_objektoj_universoobjektoligilo_ligilo_objekto__arkivo=False,
            universo_objektoj_universoobjektoligilo_ligilo_objekto__publikigo=True,).values_list('uuid', flat=True)
        return UniversoObjekto.objects.exclude(uuid__in=objektoj)
        