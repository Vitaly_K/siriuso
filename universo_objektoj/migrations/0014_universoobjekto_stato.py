# Generated by Django 2.2.12 on 2020-05-17 13:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('universo_resursoj', '0017_universoresursomodifostato'),
        ('universo_objektoj', '0013_auto_20200516_1703'),
    ]

    operations = [
        migrations.AddField(
            model_name='universoobjekto',
            name='stato',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='universo_resursoj.UniversoResursoModifoStato', verbose_name='Stato'),
        ),
    ]
