"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres import fields as pgfields
from django.contrib.auth.models import Permission
from django.db.models import Max, Q

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from universo_bazo.models import UniversoBazaMaks, UniversoBazaRealeco, UniversoRealeco
from universo_uzantoj.models import UniversoUzanto
from universo_organizoj.models import UniversoOrganizo
from universo_resursoj.models import UniversoResurso, UniversoResursoModifo, UniversoResursoModifoStato, \
    UniversoResursoStokejoTipo
from universo_kosmo.models import UniversoKosmoStelsistemoKubo


# Шаблоны объектов Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoObjektoSxablono(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # шаблон объектов владелец, то есть родительский объект
    posedanto_objekto = models.ForeignKey('self', verbose_name=_('Posedanto'), null=True, blank=True,
                                          default=None, on_delete=models.CASCADE)

    # разъём (слот), который занимает этот объект у родительского объекта
    konektilo = models.IntegerField(_('Konektilo'), blank=True, null=True, default=None)

    # ресурс Универсо на основе которого был создан этот объект
    resurso = models.ForeignKey(UniversoResurso, verbose_name=_('Resurso'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # ячейка (куб,чанк) звёздной системы в которой находится объект
    kubo = models.ForeignKey(UniversoKosmoStelsistemoKubo, verbose_name=_('Kubo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_objektoj_sxablonoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ŝablono de objektoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ŝablono de objektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_objektoj_sxablonoj', _('Povas vidi ŝablonoj de objektoj de Universo')),
            ('povas_krei_universo_objektoj_sxablonoj', _('Povas krei ŝablonoj de objektoj de Universo')),
            ('povas_forigi_universo_objektoj_sxablonoj', _('Povas forigi ŝablonoj de objektoj de Universo')),
            ('povas_shangxi_universo_objektoj_sxablonoj', _('Povas ŝanĝi ŝablonoj de objektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoObjektoSxablono, self).save(force_insert=force_insert, force_update=force_update,
                                                  using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_objektoj.povas_vidi_universo_objektoj_sxablonoj',
                'universo_objektoj.povas_krei_universo_objektoj_sxablonoj',
                'universo_objektoj.povas_forigi_universo_objektoj_sxablonoj',
                'universo_objektoj.povas_shangxi_universo_objektoj_sxablonoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_objektoj.povas_vidi_universo_objektoj_sxablonoj')
                    or user_obj.has_perm('universo_objektoj.povas_vidi_universo_objektoj_sxablonoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_objektoj.povas_vidi_universo_objektoj_sxablonoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Объекты в Универсо, использует абстрактный класс UniversoBazaRealeco
class UniversoObjekto(UniversoBazaRealeco):

    # объект владелец, то есть родительский объект
    posedanto_objekto = models.ForeignKey('self', verbose_name=_('Objekto posedanto'), null=True, blank=True,
                                          default=None, on_delete=models.CASCADE)

    # разъём (слот), который занимает этот объект у родительского объекта
    konektilo = models.IntegerField(_('Konektilo'), blank=True, null=True, default=None)

    # ресурс Универсо на основе которого был создан этот объект
    resurso = models.ForeignKey(UniversoResurso, verbose_name=_('Resurso'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # модификация ресурса Универсо на основе которого был создан этот объект
    modifo = models.ForeignKey(UniversoResursoModifo, verbose_name=_('Modifo'), blank=True, null=True, default=None,
                               on_delete=models.CASCADE)

    # состояние модификации ресурса Универсо в котором сейчас находится объект
    stato = models.ForeignKey(UniversoResursoModifoStato, verbose_name=_('Stato'), blank=True, null=True, default=None,
                              on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=True, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=True, default=default_lingvo,
                                   encoder=CallableEncoder)

    # целостность объекта
    integreco = models.IntegerField(_('Integreco'), blank=True, null=True, default=None)

    # ячейка (куб,чанк) звёздной системы в которой находится объект
    kubo = models.ForeignKey(UniversoKosmoStelsistemoKubo, verbose_name=_('Kubo'), blank=True, null=True, default=None,
                             on_delete=models.CASCADE)

    # координаты (месторасположение) объекта по оси X в кубе
    koordinato_x = models.FloatField(_('Koordinato X'), blank=True, null=True, default=None)

    # координаты (месторасположение) объекта по оси Y в кубе
    koordinato_y = models.FloatField(_('Koordinato Y'), blank=True, null=True, default=None)

    # координаты (месторасположение) объекта по оси Z в кубе
    koordinato_z = models.FloatField(_('Koordinato Z'), blank=True, null=True, default=None)

    # вращение объекта по оси X в кубе
    rotacia_x = models.FloatField(_('Rotacia X'), blank=True, null=True, default=None)

    # вращение объекта по оси Y в кубе
    rotacia_y = models.FloatField(_('Rotacia Y'), blank=True, null=True, default=None)

    # вращение объекта по оси Z в кубе
    rotacia_z = models.FloatField(_('Rotacia Z'), blank=True, null=True, default=None)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_objektoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Objekto de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Objektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_objektoj', _('Povas vidi objektoj de Universo')),
            ('povas_krei_universo_objektoj', _('Povas krei objektoj de Universo')),
            ('povas_forigi_universo_objektoj', _('Povas forigi objektoj de Universo')),
            ('povas_sxangxi_universo_objektoj', _('Povas ŝanĝi objektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid, nomo и владельцы этой модели
        posedantoj = UniversoObjektoPosedanto.objects.filter(objekto=self, forigo=False, arkivo=False, publikigo=True)
        idj = "{}: {}".format(self.uuid, get_enhavo(self.nomo, empty_values=True)[0])
        if len(posedantoj)>0:
            i = True
            for user in posedantoj:
                if user.posedanto_uzanto: 
                    if i:
                        idj = "{}: {}".format(idj, user.posedanto_uzanto.siriuso_uzanto.id)
                        i = False
                    else:
                        idj = "{}; {}".format(idj, user.posedanto_uzanto.siriuso_uzanto.id)
                if user.posedanto_organizo: 
                    if i:
                        idj = "{}: {}".format(idj, get_enhavo(user.posedanto_organizo.nomo, empty_values=True)[0])
                        i = False
                    else:
                        idj = "{}; {}".format(idj, get_enhavo(user.posedanto_organizo.nomo, empty_values=True)[0])
        return idj

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(UniversoObjekto, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_objektoj.povas_vidi_universo_objektoj', 
                'universo_objektoj.povas_krei_universo_objektoj',
                'universo_objektoj.povas_forigi_universo_objektoj',
                'universo_objektoj.povas_sxangxi_universo_objektoj'
            ))

            # Добавляем прав всем на просмотр, т.к. нужно видеть других игроков
            all_perms.update(['povas_vidi_universo_objektoj',])
            # all_perms.update(['povas_vidi_universo_objektoj','povas_krei_universo_objektoj',
            #     'povas_forigi_universo_objektoj', 'povas_shanghi_universo_objektoj'])

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            # на текущий момент показываем все объекты, потом будем сокращать
            cond = Q()
            # if (perms.has_registrita_perm('universo_objektoj.povas_vidi_universo_objektoj')
            #         or user_obj.has_perm('universo_objektoj.povas_vidi_universo_objektoj')):
            #     # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
            #     cond = Q()
            # else:
            #     # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
            #     cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_objektoj.povas_vidi_universo_objektoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы владельцев объектов, использует абстрактный класс UniversoBazaMaks
class UniversoObjektoPosedantoTipo(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_objektoj_posedantoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de posedantoj de objektoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de posedantoj de objektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_objektoj_posedantoj_tipoj',
             _('Povas vidi tipoj de posedantoj de objektoj de Universo')),
            ('povas_krei_universo_objektoj_posedantoj_tipoj',
             _('Povas krei tipoj de posedantoj de objektoj de Universo')),
            ('povas_forigi_universo_objektoj_posedantoj_tipoj',
             _('Povas forigi tipoj de posedantoj de objektoj de Universo')),
            ('povas_sxangxi_universo_objektoj_posedantoj_tipoj',
             _('Povas ŝanĝi tipoj de posedantoj de objektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoObjektoPosedantoTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_objektoj.povas_vidi_universo_objektoj_posedantoj_tipoj',
                'universo_objektoj.povas_krei_universo_objektoj_posedantoj_tipoj',
                'universo_objektoj.povas_forigi_universo_objektoj_posedantoj_tipoj',
                'universo_objektoj.povas_sxangxi_universo_objektoj_posedantoj_tipoj'
            ))

            # Добавляем прав всем на просмотр, т.к. нужно видеть других игроков
            # all_perms.update(['povas_vidi_universo_objektoj_posedantoj_tipoj',])

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_objektoj.povas_vidi_universo_objektoj_posedantoj_tipoj')
                    or user_obj.has_perm('universo_objektoj.povas_vidi_universo_objektoj_posedantoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_objektoj.povas_vidi_universo_objektoj_posedantoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Статус владельца в рамках владения объектом, использует абстрактный класс UniversoBazaMaks
class UniversoObjektoPosedantoStatuso(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_objektoj_posedantoj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso de posedantoj de objektoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj de posedantoj de objektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_objektoj_posedantoj_statusoj',
             _('Povas vidi statusoj de posedantoj de objektoj de Universo')),
            ('povas_krei_universo_objektoj_posedantoj_statusoj',
             _('Povas krei statusoj de posedantoj de objektoj de Universo')),
            ('povas_forigi_universo_objektoj_posedantoj_statusoj',
             _('Povas forigi statusoj de posedantoj de objektoj de Universo')),
            ('povas_sxangxi_universo_objektoj_posedantoj_statusoj',
             _('Povas ŝanĝi statusoj de posedantoj de objektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoObjektoPosedantoStatuso, self).save(force_insert=force_insert, force_update=force_update,
                                                          using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_objektoj.povas_vidi_universo_objektoj_posedantoj_statusoj',
                'universo_objektoj.povas_krei_universo_objektoj_posedantoj_statusoj',
                'universo_objektoj.povas_forigi_universo_objektoj_posedantoj_statusoj',
                'universo_objektoj.povas_sxangxi_universo_objektoj_posedantoj_statusoj'
            ))

            # Добавляем прав всем на просмотр, т.к. нужно видеть других игроков
            # all_perms.update(['povas_vidi_universo_objektoj_posedantoj_statusoj',])

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_objektoj.povas_vidi_universo_objektoj_posedantoj_statusoj')
                    or user_obj.has_perm('universo_objektoj.povas_vidi_universo_objektoj_posedantoj_statusoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_objektoj.povas_vidi_universo_objektoj_posedantoj_statusoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Владельцы объектов в Универсо, использует абстрактный класс UniversoBazaRealeco
class UniversoObjektoPosedanto(UniversoBazaRealeco):

    # объект владения
    objekto = models.ForeignKey(UniversoObjekto, verbose_name=_('Objekto'), blank=False, null=False,
                                default=None, on_delete=models.CASCADE)

    # пользователь владелец объекта Универсо
    posedanto_uzanto = models.ForeignKey(UniversoUzanto, verbose_name=_('Posedanta uzanto'), blank=True, null=True,
                                         default=None, on_delete=models.CASCADE)

    # организация владелец объекта Универсо
    posedanto_organizo = models.ForeignKey(UniversoOrganizo, verbose_name=_('Posedanta organizo'), blank=True,
                                           null=True, default=None, on_delete=models.CASCADE)

    # размер доли владения
    parto = models.IntegerField(_('Parto'), blank=False, null=False, default=100)

    # тип владельца объекта
    tipo = models.ForeignKey(UniversoObjektoPosedantoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # статус владельца объекта
    statuso = models.ForeignKey(UniversoObjektoPosedantoStatuso, verbose_name=_('Statuso'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_objektoj_posedantoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Posedanto de objekto de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Posedantoj de objektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_objektoj_posedantoj', _('Povas vidi posedantoj de objektoj de Universo')),
            ('povas_krei_universo_objektoj_posedantoj', _('Povas krei posedantoj de objektoj de Universo')),
            ('povas_forigi_universo_objektoj_posedantoj', _('Povas forigi posedantoj de objektoj de Universo')),
            ('povas_sxangxi_universo_objektoj_posedantoj', _('Povas ŝanĝi posedantoj de objektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле id владельца Siriuso и/или nomo организации владельца этой модели
        posedanto = "{}".format(self.objekto.uuid)
        start = True
        if self.posedanto_uzanto: 
            posedanto = "{}: {}".format(posedanto, self.posedanto_uzanto.siriuso_uzanto.id)
            start = False
        if self.posedanto_organizo:
            if start: 
                posedanto = "{}: {}".format(posedanto, get_enhavo(self.posedanto_organizo.nomo, empty_values=True)[0])
            else:
                posedanto = "{}; {}".format(posedanto, get_enhavo(self.posedanto_organizo.nomo, empty_values=True)[0])
        return posedanto

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(UniversoObjektoPosedanto, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_objektoj.povas_vidi_universo_objektoj_posedantoj',
                'universo_objektoj.povas_krei_universo_objektoj_posedantoj',
                'universo_objektoj.povas_forigi_universo_objektoj_posedantoj',
                'universo_objektoj.povas_sxangxi_universo_objektoj_posedantoj'
            ))

            # Добавляем прав всем на просмотр, т.к. нужно видеть других игроков
            # all_perms.update(['povas_vidi_universo_objektoj_posedantoj',])

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_objektoj.povas_vidi_universo_objektoj_posedantoj')
                    or user_obj.has_perm('universo_objektoj.povas_vidi_universo_objektoj_posedantoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_objektoj.povas_vidi_universo_objektoj_posedantoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Места хранения в объектах, использует абстрактный класс UniversoBazaMaks
class UniversoObjektoStokejo(UniversoBazaMaks):

    # объект владелец места хранения
    posedanto_objekto = models.ForeignKey(UniversoObjekto, verbose_name=_('Objekto posedanto'), blank=False, null=False,
                                          default=None, on_delete=models.CASCADE)

    # уникальный личный ID места хранения в рамках объекта
    id = models.IntegerField(_('ID'), blank=True, null=True, default=None)

    # тип места хранения ресурсов на основе которого создано это место хранения в объекте
    tipo = models.ForeignKey(UniversoResursoStokejoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_objektoj_stokejoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Stokejo de objektoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Stokejoj de objektoj de Universo')
        # установка уникальности личного ID места хранения в рамках объекта
        unique_together = ("id", "posedanto_objekto")
        # права
        permissions = (
            ('povas_vidi_universo_objektoj_stokejoj', _('Povas vidi stokejoj de objektoj de Universo')),
            ('povas_krei_universo_objektoj_stokejoj', _('Povas krei stokejoj de objektoj de Universo')),
            ('povas_forigi_universo_objektoj_stokejoj', _('Povas forigi stokejoj de objektoj de Universo')),
            ('povas_sxangxi_universo_objektoj_stokejoj', _('Povas ŝanĝi stokejoj de objektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{} ({})'.format(self.uuid, get_enhavo(self.priskribo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(posedanto_objekto=self.posedanto_objekto).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        # реализация автоинкремента шаблона при сохранении
        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(UniversoObjektoStokejo, self).save(force_insert=force_insert, force_update=force_update,
                                                 using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_objektoj.povas_vidi_universo_objektoj_stokejoj', 
                'universo_objektoj.povas_krei_universo_objektoj_stokejoj',
                'universo_objektoj.povas_forigi_universo_objektoj_stokejoj',
                'universo_objektoj.povas_sxangxi_universo_objektoj_stokejoj'
            ))

            # Добавляем прав всем на просмотр, т.к. нужно видеть других игроков
            # all_perms.update(['povas_vidi_universo_objektoj_stokejoj',])

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_objektoj.povas_vidi_universo_objektoj_stokejoj')
                    or user_obj.has_perm('universo_objektoj.povas_vidi_universo_objektoj_stokejoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_objektoj.povas_vidi_universo_objektoj_stokejoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы связей объектов между собой, использует абстрактный класс UniversoBazaMaks
class UniversoObjektoLigiloTipo(UniversoBazaMaks):

    # уникальный личный ID типа связей объектов между собой
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_objektoj_ligiloj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ligiloj de objektoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ligiloj de objektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_objektoj_ligiloj_tipoj',
             _('Povas vidi tipoj de ligiloj de objektoj de Universo')),
            ('povas_krei_universo_objektoj_ligiloj_tipoj',
             _('Povas krei tipoj de ligiloj de objektoj de Universo')),
            ('povas_forigi_universo_objektoj_ligiloj_tipoj',
             _('Povas forigi tipoj de ligiloj de objektoj de Universo')),
            ('povas_sxangxi_universo_objektoj_ligiloj_tipoj',
             _('Povas ŝanĝi tipoj de ligiloj de objektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoObjektoLigiloTipo, self).save(force_insert=force_insert, force_update=force_update,
                                          using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_objektoj.povas_vidi_universo_objektoj_ligiloj_tipoj', 
                'universo_objektoj.povas_krei_universo_objektoj_ligiloj_tipoj',
                'universo_objektoj.povas_forigi_universo_objektoj_ligiloj_tipoj',
                'universo_objektoj.povas_sxangxi_universo_objektoj_ligiloj_tipoj'
            ))

            # Добавляем прав всем на просмотр, т.к. нужно видеть других игроков
            # all_perms.update(['povas_vidi_universo_objektoj_ligiloj_tipoj',])

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_objektoj.povas_vidi_universo_objektoj_ligiloj_tipoj')
                    or user_obj.has_perm('universo_objektoj.povas_vidi_universo_objektoj_ligiloj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_objektoj.povas_vidi_universo_objektoj_ligiloj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь объектов между собой, использует абстрактный класс UniversoBazaMaks
class UniversoObjektoLigilo(UniversoBazaMaks):

    # объект владелец связи
    posedanto = models.ForeignKey(UniversoObjekto, verbose_name=_('Objekto - posedanto'), blank=False, null=False,
                                          default=None, on_delete=models.CASCADE)

    # место хранения владельца связи
    posedanto_stokejo = models.ForeignKey(UniversoObjektoStokejo, verbose_name=_('Stokejo posedanto'), blank=True,
                                          null=True, default=None, on_delete=models.CASCADE)

    # разъём (слот), который используется у родительского объекта
    konektilo_posedanto = models.IntegerField(_('Konektilo - posedanto'), blank=True, null=True, default=None)

    # связываемый объект
    ligilo = models.ForeignKey(UniversoObjekto, verbose_name=_('Objekto - ligilo'), blank=False, null=False,
                                       default=None, related_name='%(app_label)s_%(class)s_ligilo_objekto',
                                       on_delete=models.CASCADE)

    # разъём (слот), который используется у связываемого объекта
    konektilo_ligilo = models.IntegerField(_('Konektilo - ligilo'), blank=True, null=True, default=None)

    # тип связи объектов
    tipo = models.ForeignKey(UniversoObjektoLigiloTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_objektoj_ligiloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ligilo de objektoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ligiloj de objektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_objektoj_ligiloj', _('Povas vidi ligiloj de objektoj de Universo')),
            ('povas_krei_universo_objektoj_ligiloj', _('Povas krei ligiloj de objektoj de Universo')),
            ('povas_forigi_universo_objektoj_ligiloj', _('Povas forigi ligiloj de objektoj de Universo')),
            ('povas_sxangxi_universo_objektoj_ligiloj', _('Povas ŝanĝi ligiloj de objektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(UniversoObjektoLigilo, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_objektoj.povas_vidi_universo_objektoj_ligiloj', 
                'universo_objektoj.povas_krei_universo_objektoj_ligiloj',
                'universo_objektoj.povas_forigi_universo_objektoj_ligiloj',
                'universo_objektoj.povas_sxangxi_universo_objektoj_ligiloj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_objektoj.povas_vidi_universo_objektoj_ligiloj')
                    or user_obj.has_perm('universo_objektoj.povas_vidi_universo_objektoj_ligiloj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_objektoj.povas_vidi_universo_objektoj_ligiloj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь пользователя с управляемым объектом в соответствующем мире, использует абстрактный класс UniversoBazaRealeco
class UniversoObjektoUzanto(UniversoBazaRealeco):

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # управляемый объект (корабль, андроид)
    objekto = models.ForeignKey(UniversoObjekto, verbose_name=_('Direktebla objekto'), blank=False, null=False,
                                default=None, on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_objektoj_uzantoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Uzanto de objektoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Uzantoj de objektoj de Universo')
        # уникальность связи обязательна
        unique_together = ('autoro', 'realeco')
        # права
        permissions = (
            ('povas_vidi_universo_objektoj_uzantoj', _('Povas vidi uzantoj de objektoj de Universo')),
            ('povas_krei_universo_objektoj_uzantoj', _('Povas krei uzantoj de objektoj de Universo')),
            ('povas_forigi_universo_objektoj_uzantoj', _('Povas forigi uzantoj de objektoj de Universo')),
            ('povas_sxangxi_universo_objektoj_uzantoj', _('Povas ŝanĝi uzantoj de objektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(UniversoObjektoUzanto, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_objektoj.povas_vidi_universo_objektoj_uzantoj', 
                'universo_objektoj.povas_krei_universo_objektoj_uzantoj',
                'universo_objektoj.povas_forigi_universo_objektoj_uzantoj',
                'universo_objektoj.povas_sxangxi_universo_objektoj_uzantoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_objektoj.povas_vidi_universo_objektoj_uzantoj')
                    or user_obj.has_perm('universo_objektoj.povas_vidi_universo_objektoj_uzantoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_objektoj.povas_vidi_universo_objektoj_uzantoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
