from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UniversoObjektojConfig(AppConfig):
    name = 'universo_objektoj'
    verbose_name = _('Objektoj de Universo')

