"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2020 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from django import forms
from django.utils.translation import ugettext_lazy as _
import json

from siriuso.utils import get_enhavo
from universo_objektoj.models import *
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
from universo_bazo.models import UniversoRealeco


class TekstoMixin:
    @staticmethod
    def teksto(obj, field):
        kampo = getattr(obj, field)

        if kampo is not None:
            result = get_enhavo(kampo)

            if result:
                return result[0]
        return ''

    def nomo_teksto(self, obj):
        return self.teksto(obj=obj, field='nomo')

    def priskribo_teksto(self, obj):
        return self.teksto(obj=obj, field='priskribo')

    def sxablono_priskribo_teksto(self, obj):
        return self.teksto(obj=obj, field='sxablono_sistema_priskribo')

    def informo_teksto(self, obj):
        return self.teksto(obj=obj, field='informo')

    def autoro_id(self, obj):
        return obj.autoro.siriuso_uzanto.id

    def autoro_email(self, obj):
        return obj.autoro.siriuso_uzanto.chefa_retposhto

    def posedanto_nomo(self, obj):
        return self.teksto(obj=obj.posedanto, field='nomo')

    def ligilo_nomo(self, obj):
        return self.teksto(obj=obj.ligilo, field='nomo')

    def posedantoj_(self, obj):
        posedantoj = UniversoObjektoPosedanto.objects.filter(objekto=obj, forigo=False, arkivo=False, publikigo=True)
        idj = ''
        i = True
        for user in posedantoj:
            if user.posedanto_uzanto: 
                if i:
                    idj = "{}".format(user.posedanto_uzanto.siriuso_uzanto.id)
                    i = False
                else:
                    idj = "{}; {}".format(idj, user.posedanto_uzanto.siriuso_uzanto.id)
            if user.posedanto_organizo: 
                if i:
                    idj = "{}".format(self.teksto(obj=user.posedanto_organizo, field='nomo'))
                    i = False
                else:
                    idj = "{}; {}".format(idj, self.teksto(obj=user.posedanto_organizo, field='nomo'))
        return idj

    def posedantoj__(self, obj):
        posedantoj = UniversoObjektoLigilo.objects.filter(posedanto_stokejo=obj, forigo=False, arkivo=False, publikigo=True)
        idj = ''
        i = True
        for user in posedantoj:
            if i:
                idj = "{}".format(user.posedanto)
                i = False
            else:
                idj = "{}; {}".format(idj, user.posedanto)
        return idj


# Форма мест хранения в объектах Universo
class UniversoObjektoStokejoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = UniversoObjektoStokejo
        fields = [field.name for field in UniversoObjektoStokejo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Форма мест хранения в объектах Universo
class UniversoObjektoStokejoInlineFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoTextWidget(), label=_('Titolo'), required=False)
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoObjektoStokejo
        fields = [field.name for field in UniversoObjektoStokejo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Владелец модели объекта Универсо
class UniversoObjektoPosedantoInline(admin.TabularInline):
    model = UniversoObjektoPosedanto
    fk_name = 'objekto'
    extra = 1

# Места хранения в объектах модели объекта Универсо
class UniversoObjektoStokejoInline(admin.TabularInline):
    model = UniversoObjektoStokejo
    fk_name = 'posedanto_objekto'
    # form = UniversoObjektoStokejoInlineFormo
    extra = 1

# Владелец модели объекта Универсо
class UniversoObjektoLigiloInline(admin.TabularInline):
    model = UniversoObjektoLigilo
    fk_name = 'posedanto'
    extra = 1


# Форма объектов в Universo
class UniversoObjektoSxablonoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoObjektoSxablono
        fields = [field.name for field in UniversoObjektoSxablono._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Объекты в Universo
@admin.register(UniversoObjektoSxablono)
class UniversoObjektoSxablonoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoObjektoSxablonoFormo
    list_display = ('nomo_teksto','priskribo_teksto')
    exclude = ('uuid',)
    class Meta:
        model = UniversoObjektoSxablono


# Форма объектов в Universo
class UniversoObjektoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = UniversoObjekto
        fields = [field.name for field in UniversoObjekto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Объекты в Universo
@admin.register(UniversoObjekto)
class UniversoObjektoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoObjektoFormo
    list_display = ('nomo_teksto','posedantoj_','priskribo_teksto','realeco','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    inlines = (UniversoObjektoPosedantoInline, UniversoObjektoStokejoInline, UniversoObjektoLigiloInline)

    list_filter = ('sxablono_sistema','realeco')

    class Meta:
        model = UniversoObjekto


# Форма типов владельцев объектов в Universo
class UniversoObjektoPosedantoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoObjektoPosedantoTipo
        fields = [field.name for field in UniversoObjektoPosedantoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы владельцев объектов в Universo
@admin.register(UniversoObjektoPosedantoTipo)
class UniversoObjektoPosedantoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoObjektoPosedantoTipoFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoObjektoPosedantoTipo


# Форма статусов владельца в рамках владения объектом Универсо
class UniversoObjektoPosedantoStatusoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoObjektoPosedantoStatuso
        fields = [field.name for field in UniversoObjektoPosedantoStatuso._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Статусы владельцев в рамках владения объектом Универсо
@admin.register(UniversoObjektoPosedantoStatuso)
class UniversoObjektoPosedantoStatusoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoObjektoPosedantoStatusoFormo
    list_display = ('nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoObjektoPosedantoStatuso


# Форма владельцев объектов в Universo
class UniversoObjektoPosedantoFormo(forms.ModelForm):

    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = UniversoObjektoPosedanto
        fields = [field.name for field in UniversoObjektoPosedanto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Владельцы объектов в Universo
@admin.register(UniversoObjektoPosedanto)
class UniversoObjektoPosedantoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoObjektoPosedantoFormo
    list_display = ('uuid','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema','realeco')
    class Meta:
        model = UniversoObjektoPosedanto


# Места хранения в объектах Universo
@admin.register(UniversoObjektoStokejo)
class UniversoObjektoStokejoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoObjektoStokejoFormo
    list_display = ('uuid','posedantoj__','nomo_teksto','priskribo_teksto','id','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema',)
    class Meta:
        model = UniversoObjektoStokejo


# Форма типов связей объектов между собой в Universo
class UniversoObjektoLigiloTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoObjektoLigiloTipo
        fields = [field.name for field in UniversoObjektoLigiloTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы связей объектов между собой в Universo
@admin.register(UniversoObjektoLigiloTipo)
class UniversoObjektoLigiloTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoObjektoLigiloTipoFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoObjektoLigiloTipo


# Форма связи объектов между собой в Universo
class UniversoObjektoLigiloFormo(forms.ModelForm):
    
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = UniversoObjektoLigilo
        fields = [field.name for field in UniversoObjektoLigilo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Связь объектов между собой в Universo
@admin.register(UniversoObjektoLigilo)
class UniversoObjektoLigiloAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoObjektoLigiloFormo
    list_display = ('uuid','posedanto_nomo','konektilo_posedanto','ligilo_nomo','konektilo_ligilo','tipo','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema','tipo')
    class Meta:
        model = UniversoObjektoLigilo


# Форма связи объектов между собой в Universo
class UniversoObjektoUzantoFormo(forms.ModelForm):
    
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = UniversoObjektoUzanto
        fields = [field.name for field in UniversoObjektoUzanto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Связь объектов между собой в Universo
@admin.register(UniversoObjektoUzanto)
class UniversoObjektoUzantoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoObjektoUzantoFormo
    list_display = ('uuid','autoro','objekto','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema','realeco')
    class Meta:
        model = UniversoObjektoUzanto


