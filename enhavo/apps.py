from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class EnhavoConfig(AppConfig):
    name = 'enhavo'
    verbose_name = _('Enhavo')
