"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from .models import *
from siriuso.views import SiriusoTemplateView


# Страницы
class Paxo(SiriusoTemplateView):

    template_name = 'enhavo/paxo.html'
    mobile_template_name = 'enhavo/portebla/t_paxo.html'

    http_method_names = ['get']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        queryset_paxo = EnhavoEnhavo.objects.values('uuid', 'nomo__enhavo', 'teksto__enhavo', 'id')

        paxo = get_object_or_404(queryset_paxo, id=kwargs['paxo_id'], forigo=False)

        if paxo['teksto__enhavo'] is not None:
            enhavo = {
                'nomo': paxo['nomo__enhavo'],
                'teksto': paxo['teksto__enhavo'],
            }
        else:
            enhavo = None

        context.update({'paxo': paxo,
               'enhavo': enhavo})

        return context
