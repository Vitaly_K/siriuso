"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib.sites.models import Site
from django.db import models
from django.db.models import Max
from django.contrib.postgres import fields as pgfields
from django.utils.translation import ugettext_lazy as _
from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo
from main.models import SiriusoBazaAbstraktaKomunumoj
from main.models import SiriusoTipoAbstrakta
from main.models import SiriusoSlugo
import sys


# Функционал контента
# Типы контента, использует абстрактный класс SiriusoTipoAbstrakta
class EnhavoEnhavoTipo(SiriusoTipoAbstrakta):

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'enhavo_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de enhavo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de enhavo')


# Контент, использует абстрактный класс SiriusoBazaAbstraktaKomunumoj
class EnhavoEnhavo(SiriusoBazaAbstraktaKomunumoj):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # слаг
    slugo = models.OneToOneField(SiriusoSlugo, verbose_name=_('Slugo'), blank=True, null=True,
                                 on_delete=models.CASCADE)

    # тип контента
    tipo = models.ForeignKey(EnhavoEnhavoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название в таблице названий контента, от туда будет браться название с нужным языковым тегом
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # текст в таблице текстов контента, от туда будет браться текст с нужным языковым тегом
    teksto = pgfields.JSONField(verbose_name=_('Teksto'), blank=True, null=False, default=default_lingvo,
                                encoder=CallableEncoder)

    # выбор сайтов в рамках фреймворка для поддержки нескольких сайтов
    retejoj = models.ManyToManyField(Site, verbose_name=_('Retejoj'))

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'enhavo_enhavo'
        # читабельное название модели, в единственном числе
        verbose_name = _('Enhavo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Enhavo')
        # задает поле сортировки записей по умолчанию
        ordering = ('nomo',)

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(EnhavoEnhavo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                       update_fields=update_fields)
