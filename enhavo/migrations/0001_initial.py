# Generated by Django 2.0 on 2017-12-13 08:34

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('informiloj', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='EnhavoEnhavo',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('a_foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Aŭtomata foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('id', models.IntegerField(default=0, unique=True, verbose_name='ID')),
            ],
            options={
                'verbose_name': 'Enhavo',
                'verbose_name_plural': 'Enhavo',
                'db_table': 'enhavo_enhavo',
                'ordering': ('nomo',),
            },
        ),
        migrations.CreateModel(
            name='EnhavoEnhavoTeksto',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('enhavo', models.TextField(verbose_name='Enhavo')),
                ('chefa_varianto', models.BooleanField(default=False, verbose_name='Ĉefa varianto')),
                ('lasta_renoviga_dato', models.DateTimeField(blank=True, null=True, verbose_name='Lasta renoviga dato')),
                ('lingvo', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='informiloj.InformilojLingvo', verbose_name='Lingvo')),
                ('posedanto', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='enhavo.EnhavoEnhavo')),
            ],
            options={
                'db_table': 'enhavo_enhavo_tekstoj',
            },
        ),
        migrations.CreateModel(
            name='EnhavoEnhavoTipo',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('a_foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Aŭtomata foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('kodo', models.CharField(max_length=16, verbose_name='Kodo')),
                ('speciala', models.BooleanField(default=False, verbose_name='Speciala')),
            ],
            options={
                'verbose_name': 'Tipo de enhavo',
                'verbose_name_plural': 'Tipoj de enhavo',
                'db_table': 'enhavo_tipoj',
            },
        ),
        migrations.CreateModel(
            name='EnhavoEnhavoTipoNomo',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('enhavo', models.TextField(verbose_name='Enhavo')),
                ('chefa_varianto', models.BooleanField(default=False, verbose_name='Ĉefa varianto')),
                ('lasta_renoviga_dato', models.DateTimeField(blank=True, null=True, verbose_name='Lasta renoviga dato')),
                ('lingvo', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='informiloj.InformilojLingvo', verbose_name='Lingvo')),
                ('posedanto', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='enhavo.EnhavoEnhavoTipo')),
            ],
            options={
                'verbose_name': 'Nomo',
                'verbose_name_plural': 'Nomoj',
                'db_table': 'enhavo_tipoj_nomoj',
            },
        ),
        migrations.CreateModel(
            name='EnhavoEnhavoTitolo',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('enhavo', models.TextField(verbose_name='Enhavo')),
                ('chefa_varianto', models.BooleanField(default=False, verbose_name='Ĉefa varianto')),
                ('lasta_renoviga_dato', models.DateTimeField(blank=True, null=True, verbose_name='Lasta renoviga dato')),
                ('lingvo', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='informiloj.InformilojLingvo', verbose_name='Lingvo')),
                ('posedanto', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='enhavo.EnhavoEnhavo')),
            ],
            options={
                'verbose_name': 'Titolo',
                'verbose_name_plural': 'Titoloj',
                'db_table': 'enhavo_enhavo_titoloj',
            },
        ),
        migrations.AddField(
            model_name='enhavoenhavotipo',
            name='nomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='enhavo.EnhavoEnhavoTipoNomo', verbose_name='Nomo'),
        ),
    ]
