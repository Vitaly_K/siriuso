# Generated by Django 2.0.9 on 2018-11-18 02:43

import django.contrib.postgres.fields.jsonb
from django.db import migrations
import siriuso.models.postgres
import siriuso.utils.modules


def sync_lingvo(apps, schema_editor):
    """
    Синхронизирует текстовые записи на всех языках с новыми JSON полями
    """
    sync_list = [
        ('EnhavoEnhavoTipo', 'EnhavoEnhavoTipoNomo', 'nomo'),
        ('EnhavoEnhavo', 'EnhavoEnhavoTitolo', 'nomo'),
        ('EnhavoEnhavo', 'EnhavoEnhavoTeksto', 'teksto'),
    ]

    for kom_model, lingvo_model, posedanto_kampo in sync_list:
        # Для всех записей сообщества
        for kom in apps.get_model('enhavo', kom_model).objects.all():
            enhavo = {
                'enhavo': [],
                'lingvo': {},
                'chefa_varianto': None
            }
            # Для всех языковых вариантов
            lingvoj = apps.get_model('enhavo', lingvo_model).objects.filter(posedanto=kom)

            for lingvo in lingvoj:
                enhavo['lingvo'][lingvo.lingvo.kodo] = len(enhavo['enhavo'])
                enhavo['enhavo'].append(lingvo.enhavo)

                if lingvo.chefa_varianto or lingvoj.count() == 1:
                    enhavo['chefa_varianto'] = lingvo.lingvo.kodo

            setattr(kom, posedanto_kampo, enhavo)
            kom.save()


class Migration(migrations.Migration):

    dependencies = [
        ('enhavo', '0003_auto_20180821_0447'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='enhavoenhavo',
            name='nomo',
        ),
        migrations.RemoveField(
            model_name='enhavoenhavo',
            name='teksto',
        ),
        migrations.RemoveField(
            model_name='enhavoenhavotipo',
            name='nomo',
        ),
        migrations.AddField(
            model_name='enhavoenhavo',
            name='nomo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Titolo'),
        ),
        migrations.AddField(
            model_name='enhavoenhavo',
            name='teksto',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Teksto'),
        ),
        migrations.AddField(
            model_name='enhavoenhavotipo',
            name='nomo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Nomo'),
        ),
        migrations.RunPython(sync_lingvo),
    ]
