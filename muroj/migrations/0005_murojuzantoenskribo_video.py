# Generated by Django 2.0 on 2018-01-05 14:13

from django.db import migrations
import embed_video.fields


class Migration(migrations.Migration):

    dependencies = [
        ('muroj', '0004_auto_20180105_0407'),
    ]

    operations = [
        migrations.AddField(
            model_name='murojuzantoenskribo',
            name='video',
            field=embed_video.fields.EmbedVideoField(default='', verbose_name='Video'),
        ),
    ]
