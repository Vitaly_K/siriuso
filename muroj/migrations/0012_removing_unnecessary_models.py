# Generated by Django 2.0.13 on 2019-05-15 07:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('muroj', '0011_vk_referenco'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='murojgrupoenskribo',
            name='aliro',
        ),
        migrations.RemoveField(
            model_name='murojgrupoenskribo',
            name='autoro',
        ),
        migrations.RemoveField(
            model_name='murojgrupoenskribo',
            name='komentado_aliro',
        ),
        migrations.RemoveField(
            model_name='murojgrupoenskribo',
            name='lasta_autoro',
        ),
        migrations.RemoveField(
            model_name='murojgrupoenskribo',
            name='muro',
        ),
        migrations.RemoveField(
            model_name='murojgrupoenskribo',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='murojgrupoenskribobildo',
            name='autoro',
        ),
        migrations.RemoveField(
            model_name='murojgrupoenskribobildo',
            name='lasta_autoro',
        ),
        migrations.RemoveField(
            model_name='murojgrupoenskribobildo',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='murojgrupoenskribointerese',
            name='autoro',
        ),
        migrations.RemoveField(
            model_name='murojgrupoenskribointerese',
            name='enskribo',
        ),
        migrations.RemoveField(
            model_name='murojgrupoenskribointerese',
            name='lasta_autoro',
        ),
        migrations.RemoveField(
            model_name='murojgrupoenskribokomento',
            name='autoro',
        ),
        migrations.RemoveField(
            model_name='murojgrupoenskribokomento',
            name='lasta_autoro',
        ),
        migrations.RemoveField(
            model_name='murojgrupoenskribokomento',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='murojgrupoenskribokomento',
            name='respondo',
        ),
        migrations.RemoveField(
            model_name='murojgrupomuro',
            name='aliro',
        ),
        migrations.RemoveField(
            model_name='murojgrupomuro',
            name='komentado_aliro',
        ),
        migrations.RemoveField(
            model_name='murojgrupomuro',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='murojorganizoenskribo',
            name='aliro',
        ),
        migrations.RemoveField(
            model_name='murojorganizoenskribo',
            name='autoro',
        ),
        migrations.RemoveField(
            model_name='murojorganizoenskribo',
            name='komentado_aliro',
        ),
        migrations.RemoveField(
            model_name='murojorganizoenskribo',
            name='lasta_autoro',
        ),
        migrations.RemoveField(
            model_name='murojorganizoenskribo',
            name='muro',
        ),
        migrations.RemoveField(
            model_name='murojorganizoenskribo',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='murojorganizoenskribobildo',
            name='autoro',
        ),
        migrations.RemoveField(
            model_name='murojorganizoenskribobildo',
            name='lasta_autoro',
        ),
        migrations.RemoveField(
            model_name='murojorganizoenskribobildo',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='murojorganizoenskribointerese',
            name='autoro',
        ),
        migrations.RemoveField(
            model_name='murojorganizoenskribointerese',
            name='enskribo',
        ),
        migrations.RemoveField(
            model_name='murojorganizoenskribointerese',
            name='lasta_autoro',
        ),
        migrations.RemoveField(
            model_name='murojorganizoenskribokomento',
            name='autoro',
        ),
        migrations.RemoveField(
            model_name='murojorganizoenskribokomento',
            name='lasta_autoro',
        ),
        migrations.RemoveField(
            model_name='murojorganizoenskribokomento',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='murojorganizoenskribokomento',
            name='respondo',
        ),
        migrations.RemoveField(
            model_name='murojorganizomuro',
            name='aliro',
        ),
        migrations.RemoveField(
            model_name='murojorganizomuro',
            name='komentado_aliro',
        ),
        migrations.RemoveField(
            model_name='murojorganizomuro',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektoenskribo',
            name='aliro',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektoenskribo',
            name='autoro',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektoenskribo',
            name='komentado_aliro',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektoenskribo',
            name='lasta_autoro',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektoenskribo',
            name='muro',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektoenskribo',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektoenskribobildo',
            name='autoro',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektoenskribobildo',
            name='lasta_autoro',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektoenskribobildo',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektoenskribointerese',
            name='autoro',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektoenskribointerese',
            name='enskribo',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektoenskribointerese',
            name='lasta_autoro',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektoenskribokomento',
            name='autoro',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektoenskribokomento',
            name='lasta_autoro',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektoenskribokomento',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektoenskribokomento',
            name='respondo',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektomuro',
            name='aliro',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektomuro',
            name='komentado_aliro',
        ),
        migrations.RemoveField(
            model_name='murojsociaprojektomuro',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='murojsovetoenskribo',
            name='aliro',
        ),
        migrations.RemoveField(
            model_name='murojsovetoenskribo',
            name='autoro',
        ),
        migrations.RemoveField(
            model_name='murojsovetoenskribo',
            name='komentado_aliro',
        ),
        migrations.RemoveField(
            model_name='murojsovetoenskribo',
            name='lasta_autoro',
        ),
        migrations.RemoveField(
            model_name='murojsovetoenskribo',
            name='muro',
        ),
        migrations.RemoveField(
            model_name='murojsovetoenskribo',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='murojsovetoenskribobildo',
            name='autoro',
        ),
        migrations.RemoveField(
            model_name='murojsovetoenskribobildo',
            name='lasta_autoro',
        ),
        migrations.RemoveField(
            model_name='murojsovetoenskribobildo',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='murojsovetoenskribointerese',
            name='autoro',
        ),
        migrations.RemoveField(
            model_name='murojsovetoenskribointerese',
            name='enskribo',
        ),
        migrations.RemoveField(
            model_name='murojsovetoenskribointerese',
            name='lasta_autoro',
        ),
        migrations.RemoveField(
            model_name='murojsovetoenskribokomento',
            name='autoro',
        ),
        migrations.RemoveField(
            model_name='murojsovetoenskribokomento',
            name='lasta_autoro',
        ),
        migrations.RemoveField(
            model_name='murojsovetoenskribokomento',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='murojsovetoenskribokomento',
            name='respondo',
        ),
        migrations.RemoveField(
            model_name='murojsovetomuro',
            name='aliro',
        ),
        migrations.RemoveField(
            model_name='murojsovetomuro',
            name='komentado_aliro',
        ),
        migrations.RemoveField(
            model_name='murojsovetomuro',
            name='posedanto',
        ),
        migrations.DeleteModel(
            name='MurojGrupoEnskribo',
        ),
        migrations.DeleteModel(
            name='MurojGrupoEnskriboBildo',
        ),
        migrations.DeleteModel(
            name='MurojGrupoEnskriboInterese',
        ),
        migrations.DeleteModel(
            name='MurojGrupoEnskriboKomento',
        ),
        migrations.DeleteModel(
            name='MurojGrupoMuro',
        ),
        migrations.DeleteModel(
            name='MurojOrganizoEnskribo',
        ),
        migrations.DeleteModel(
            name='MurojOrganizoEnskriboBildo',
        ),
        migrations.DeleteModel(
            name='MurojOrganizoEnskriboInterese',
        ),
        migrations.DeleteModel(
            name='MurojOrganizoEnskriboKomento',
        ),
        migrations.DeleteModel(
            name='MurojOrganizoMuro',
        ),
        migrations.DeleteModel(
            name='MurojSociaprojektoEnskribo',
        ),
        migrations.DeleteModel(
            name='MurojSociaprojektoEnskriboBildo',
        ),
        migrations.DeleteModel(
            name='MurojSociaprojektoEnskriboInterese',
        ),
        migrations.DeleteModel(
            name='MurojSociaprojektoEnskriboKomento',
        ),
        migrations.DeleteModel(
            name='MurojSociaprojektoMuro',
        ),
        migrations.DeleteModel(
            name='MurojSovetoEnskribo',
        ),
        migrations.DeleteModel(
            name='MurojSovetoEnskriboBildo',
        ),
        migrations.DeleteModel(
            name='MurojSovetoEnskriboInterese',
        ),
        migrations.DeleteModel(
            name='MurojSovetoEnskriboKomento',
        ),
        migrations.DeleteModel(
            name='MurojSovetoMuro',
        ),
    ]
