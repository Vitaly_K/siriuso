"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from uzantoj.templatecontexts.uzanto import get_kamaradoj_id
from main.models import Uzanto


def context_kamaradoj(request, **kwargs):
    context = {}

    if request.user.is_authenticated:
        context['mia_kamaradoj'] = get_kamaradoj_id(request.user.id)

    return context


def context_statistikoj(request, **kwargs):
    context = {}

    try:
        uzanto = Uzanto.objects.values('id', 'uuid', 'sekso', 'unua_nomo', 'familinomo',
                                            'uzantojavataro__bildo', 'uzantojkovrilo__bildo',
                                            'uzantojstatuso__teksto').get(id=kwargs['uzanto_id'])
        context['uzanto'] = uzanto
        context['uzanto']['username'] = '{}_{}'.format(uzanto['unua_nomo'],
                                                       uzanto['familinomo']).lower()
    except:
        pass

    return context
