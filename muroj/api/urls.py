"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""


from django.urls import path, re_path, include
from .views import *
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'komunumoj', MurojEnskribojView,  base_name='muroj_komunumoj')
router.register(r'komunumo/(?P<komunumo_uuid>[a-zA-Z0-9-]+)', MurojEnskribojView, base_name='muro_komunumo')
router.register(r'komentoj', MurojKomentojView, base_name='muroj_komentoj')
router.register(r'komento/(?P<enskribo_uuid>[a-zA-Z0-9-]+)', MurojKomentojView, base_name='muro_komentoj')

urlpatterns = [
    path('', include(router.urls)),
]
