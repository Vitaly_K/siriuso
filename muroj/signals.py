"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.utils import timezone
from komunumoj.models import Komunumo, KomunumojAliro
from .models import Muro


@receiver(post_save, sender=Komunumo)
def create_muro(sender, instance, **kwargs):
    """Создаём стену для сообщества, если её нет"""
    try:
        Muro.objects.get(posedanto=instance, forigo=False)
    except Muro.DoesNotExist:
        aliro = KomunumojAliro.objects.get(kodo='chiuj')

        Muro.objects.create(
            posedanto=instance,
            aliro=aliro,
            komentado_aliro=aliro,
            publikigo=True,
            publikiga_dato=timezone.now(),
            arkivo=False,
            forigo=False,
        )
