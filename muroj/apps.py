from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class MuroConfig(AppConfig):
    name = 'muroj'
    verbose_name = _('Muroj')

    def ready(self):
        import muroj.signals
        import muroj.tasks
