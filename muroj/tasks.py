"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from celery import shared_task
from django.contrib.sites.models import Site
from django.core import mail
from django.core.mail import EmailMessage
from django.db.models import F, Q
from django.template.loader import get_template
from django.utils.translation import ugettext_lazy as _
from django.utils.text import format_lazy
from siriuso.utils import get_enhavo

from main.models import Uzanto
from muroj.models import (MuroEnskribo, MurojUzantoEnskribo)
from uzantoj.sciigoj import sendu_sciigon


class HtmlEmailMessage(EmailMessage):
    content_subtype = 'html'


@shared_task
def email_post_notice(kom_model_name, uuid):
    kom_model = MuroEnskribo
    enskribo_query = kom_model.objects.filter(uuid=uuid)

    enskribo = (enskribo_query
                .values('id',
                        'posedanto__tipo__kodo',
                        kom_name=F('muro__posedanto__nomo'),
                        kom_id=F('muro__posedanto__id'))
                .get())

    address = F('muro__posedanto__komunumomembro__autoro__chefa_retposhto')
    settings = F('muro__posedanto__komunumomembro__autoro__agordoj')
    kom_name = get_enhavo(enskribo['kom_name'])[0]

    q = (Q(muro__posedanto__komunumomembro__muro_sciigo__kodo='powto'),
         Q(muro__posedanto__komunumomembro__forigo=False),
         Q(muro__posedanto__komunumomembro__autoro__malbona_retposhto=False),
         Q(muro__posedanto__komunumomembro__autoro__is_active=True),
         Q(muro__posedanto__komunumomembro__autoro__konfirmita=True),
         Q(muro__posedanto__komunumomembro__autoro__agordoj__has_key='abono_shlosilo'))

    if enskribo['posedanto__tipo__kodo'] == 'iniciato':
        kom_str = _('В Общественной инициативе "{}" опубликована новая запись.')
        email_subject = _('Новая запись в Общественной инициативе на Технокоме')
    elif enskribo['posedanto__tipo__kodo'] == 'grupo':
        kom_str = _('В Группе "{}" опубликована новая запись.')
        email_subject = _('Новая запись в Группе на Технокоме')
    elif enskribo['posedanto__tipo__kodo'] == 'soveto':
        kom_str = _('В Совете "{}" опубликована новая запись.')
        email_subject = _('Новая запись в Совете на Технокоме')
    else:
        kom_str = _('В Организации "{}" опубликована новая запись.')
        email_subject = _('Новая запись в Организации на Технокоме')

    recipients = (enskribo_query
                  .filter(*q)
                  .annotate(poshto=address, agordoj=settings)
                  .values('poshto', 'agordoj'))

    current_site = Site.objects.get_current()
    enskribo_full_url = "https://{}/idk{}-e{}".format(current_site, enskribo['kom_id'], enskribo['id'])

    emails = []

    for recipient in recipients:
        context = {
            'subject': email_subject,
            'kom_str': format_lazy(kom_str, kom_name),
            'enskribo_url': enskribo_full_url,
            'poshto': recipient['poshto'],
            'site_url': current_site,
            'abono_shlosilo': recipient['agordoj']['abono_shlosilo']
        }

        email_body = get_template('muroj/emails/komunumo_post_notice.html').render(context)

        email_message = {
            'subject': email_subject,
            'body': email_body,
        }

        unsubscribe_link = '<https://{}{}?poshto={}&shlosilo={}&nun=1>'.format(
            current_site,
            'malabono',
            recipient['poshto'],
            recipient['agordoj']['abono_shlosilo']
        )

        emails.append(HtmlEmailMessage(**email_message, to=(recipient['poshto'],),
                                       headers={'List-Unsubscribe': unsubscribe_link}))

    with mail.get_connection() as connection:
        result = connection.send_messages(emails)
    return result


@shared_task
def sciigi_uzantojn(kom_model_name, uuid):
    if kom_model_name == 'muroenskribo':
        enskribo = MuroEnskribo.objects.get(pk=uuid)
        uzantoj = Uzanto.objects.filter(
            ~Q(komunumomembro__autoro=enskribo.autoro),
            komunumomembro__forigo=False,
            komunumomembro__posedanto=enskribo.posedanto,
            is_active=True,
            konfirmita=True,
        )
        teksto = 'Sur la muro de la komunumo publikigis novan eniron'
        parametroj = {'nomo': {'obj': enskribo.posedanto, 'field': 'nomo'}}
        _('Sur la muro de la komunumo publikigis novan eniron')
    else:
        enskribo = MurojUzantoEnskribo.objects.get(pk=uuid)
        uzantoj = Uzanto.objects.filter(
            Q(uzantoj_uzantojgekamaradoj_gekamarado__posedanto=enskribo.posedanto,
              uzantoj_uzantojgekamaradoj_gekamarado__akceptis2=True,
              uzantoj_uzantojgekamaradoj_gekamarado__akceptis1=True,
              uzantoj_uzantojgekamaradoj_gekamarado__forigo=False)
            | Q(uzantojgekamaradoj__gekamarado=enskribo.posedanto,
                uzantojgekamaradoj__akceptis1=True,
                uzantojgekamaradoj__forigo=False),
            ~Q(id=enskribo.posedanto_id),
            is_active=True,
            konfirmita=True,
        )
        teksto = 'afiŝis novan eniron sur sian muron'
        parametroj = None
        _('afiŝis novan eniron sur sian muron')

    if uzantoj:
        uzantoj = list(uzantoj.values_list('id', flat=True))
        return sendu_sciigon(teksto, to=uzantoj, objektoj=(enskribo,), teksto_parametroj=parametroj)

    return 0
