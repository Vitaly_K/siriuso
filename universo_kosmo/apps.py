from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UniversoKosmoConfig(AppConfig):
    name = 'universo_kosmo'
    verbose_name = _('Kosmo de Universo')
