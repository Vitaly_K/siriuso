import graphene  # сам Graphene
from django.utils.translation import ugettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import *  # модели приложения


# Модель типов звёздных систем в Универсо
class UniversoKosmoStelsistemoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoKosmoStelsistemoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель звёздных систем в Универсо
class UniversoKosmoStelsistemoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoKosmoStelsistemo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact'],
            'tipo_id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель кубов (ячейки, чанки) звёздных систем в Универсо
class UniversoKosmoStelsistemoKuboNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = UniversoKosmoStelsistemoKubo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


class UniversoKosmoQuery(graphene.ObjectType):
    universo_kosmo_stelsistemo_tipo = SiriusoFilterConnectionField(
        UniversoKosmoStelsistemoTipoNode,
        description=_('Выводит все доступные модели типов звёздных систем в Универсо')
    )
    universo_kosmo_stelsistemo = SiriusoFilterConnectionField(
        UniversoKosmoStelsistemoNode,
        description=_('Выводит все доступные модели звёздных систем в Универсо')
    )
    universo_kosmo_stelsistemo_kubo = SiriusoFilterConnectionField(
        UniversoKosmoStelsistemoKuboNode,
        description=_('Выводит все доступные модели кубов (ячейки, чанки) звёздных систем в Универсо')
    )
