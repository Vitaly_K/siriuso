"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
16.05.2019
"""
from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from universo_uzantoj.models import UniversoUzanto
from .schema import *
from ..models import *


# Модель типов звёздных систем в Универсо
class RedaktuUniversoKosmoStelsistemoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_kosmo_stelsistemoj_tipoj = graphene.Field(UniversoKosmoStelsistemoTipoNode, 
        description=_('Созданный/изменённый тип звёздных систем в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_kosmo_stelsistemoj_tipoj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_kosmo.povas_krei_universo_kosmo_stelsistemoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_kosmo_stelsistemoj_tipoj = UniversoKosmoStelsistemoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_kosmo_stelsistemoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_kosmo_stelsistemoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_kosmo_stelsistemoj_tipoj.realeco.set(realeco)
                                else:
                                    universo_kosmo_stelsistemoj_tipoj.realeco.clear()

                            universo_kosmo_stelsistemoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_kosmo_stelsistemoj_tipoj = UniversoKosmoStelsistemoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_kosmo.povas_forigi_universo_kosmo_stelsistemoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_kosmo.povas_shanghi_universo_kosmo_stelsistemoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_kosmo_stelsistemoj_tipoj.forigo = kwargs.get('forigo', universo_kosmo_stelsistemoj_tipoj.forigo)
                                universo_kosmo_stelsistemoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_kosmo_stelsistemoj_tipoj.arkivo = kwargs.get('arkivo', universo_kosmo_stelsistemoj_tipoj.arkivo)
                                universo_kosmo_stelsistemoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_kosmo_stelsistemoj_tipoj.publikigo = kwargs.get('publikigo', universo_kosmo_stelsistemoj_tipoj.publikigo)
                                universo_kosmo_stelsistemoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_kosmo_stelsistemoj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_kosmo_stelsistemoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_kosmo_stelsistemoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_kosmo_stelsistemoj_tipoj.realeco.set(realeco)
                                    else:
                                        universo_kosmo_stelsistemoj_tipoj.realeco.clear()

                                universo_kosmo_stelsistemoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoKosmoStelsistemoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoKosmoStelsistemoTipo(status=status, message=message, universo_kosmo_stelsistemoj_tipoj=universo_kosmo_stelsistemoj_tipoj)


# Модель звёздных систем в Универсо
class RedaktuUniversoKosmoStelsistemo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_kosmo_stelsistemoj = graphene.Field(UniversoKosmoStelsistemoNode, 
        description=_('Созданный/изменённый модель звёздных систем в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        tipo_id = graphene.Int(description=_('Тип звёздной системы Универсо'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_kosmo_stelsistemoj = None
        tipo = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_kosmo.povas_krei_universo_kosmo_stelsistemoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoKosmoStelsistemoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoKosmoStelsistemoTipo.DoesNotExist:
                                    message = _('Неверный тип звёздной системы Универсо')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_kosmo_stelsistemoj = UniversoKosmoStelsistemo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                tipo = tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_kosmo_stelsistemoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_kosmo_stelsistemoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_kosmo_stelsistemoj_tipoj.realeco.set(realeco)
                                else:
                                    universo_kosmo_stelsistemoj_tipoj.realeco.clear()

                            universo_kosmo_stelsistemoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('tipo_id',False) or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_kosmo_stelsistemoj = UniversoKosmoStelsistemo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_kosmo.povas_forigi_universo_kosmo_stelsistemoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_kosmo.povas_shanghi_universo_kosmo_stelsistemoj'):
                                message = _('Недостаточно прав для изменения')
                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoKosmoStelsistemoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoKosmoStelsistemoTipo.DoesNotExist:
                                        message = _('Неверный тип звёздной системы Универсо')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_kosmo_stelsistemoj.forigo = kwargs.get('forigo', universo_kosmo_stelsistemoj.forigo)
                                universo_kosmo_stelsistemoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_kosmo_stelsistemoj.arkivo = kwargs.get('arkivo', universo_kosmo_stelsistemoj.arkivo)
                                universo_kosmo_stelsistemoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_kosmo_stelsistemoj.publikigo = kwargs.get('publikigo', universo_kosmo_stelsistemoj.publikigo)
                                universo_kosmo_stelsistemoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_kosmo_stelsistemoj.autoro = autoro
                                universo_kosmo_stelsistemoj.tipo = tipo if kwargs.get('tipo_id', False) else universo_kosmo_stelsistemoj.tipo

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_kosmo_stelsistemoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_kosmo_stelsistemoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_kosmo_stelsistemoj_tipoj.realeco.set(realeco)
                                    else:
                                        universo_kosmo_stelsistemoj_tipoj.realeco.clear()

                                universo_kosmo_stelsistemoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoKosmoStelsistemo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoKosmoStelsistemo(status=status, message=message, universo_kosmo_stelsistemoj=universo_kosmo_stelsistemoj)


# Модель кубов (ячейки, чанки) звёздных систем в Универсо
class RedaktuUniversoKosmoStelsistemoKubo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_kosmo_stelsistemoj_kuboj = graphene.Field(UniversoKosmoStelsistemoKuboNode, 
        description=_('Созданный/изменённый куб (ячейки, чанки) звёздных систем в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        stelsistemo_id = graphene.Int(description=_('Код типа ресурсов Универсо'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_kosmo_stelsistemoj_kuboj = None
        stelsistemo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_kosmo.povas_krei_universo_kosmo_stelsistemoj_kuboj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'stelsistemo_id' in kwargs:
                                try:
                                    stelsistemo = UniversoKosmoStelsistemo.objects.get(id=kwargs.get('stelsistemo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoKosmoStelsistemo.DoesNotExist:
                                    message = _('Неверная звёздная система Универсо')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'stelsistemo_id')

                        if not message:
                            universo_kosmo_stelsistemoj_kuboj = UniversoKosmoStelsistemoKubo.objects.create(
                                forigo=False,
                                arkivo=False,
                                stelsistemo = stelsistemo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            universo_kosmo_stelsistemoj_kuboj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('stelsistemo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_kosmo_stelsistemoj_kuboj = UniversoKosmoStelsistemoKubo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_kosmo.povas_forigi_universo_kosmo_stelsistemoj_kuboj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_kosmo.povas_shanghi_universo_kosmo_stelsistemoj_kuboj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'stelsistemo_id' in kwargs:
                                    try:
                                        stelsistemo = UniversoKosmoStelsistemo.objects.get(id=kwargs.get('stelsistemo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoKosmoStelsistemo.DoesNotExist:
                                        message = _('Неверная звёздная система Универсо')
                            if not message:

                                universo_kosmo_stelsistemoj_kuboj.forigo = kwargs.get('forigo', universo_kosmo_stelsistemoj_kuboj.forigo)
                                universo_kosmo_stelsistemoj_kuboj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_kosmo_stelsistemoj_kuboj.arkivo = kwargs.get('arkivo', universo_kosmo_stelsistemoj_kuboj.arkivo)
                                universo_kosmo_stelsistemoj_kuboj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_kosmo_stelsistemoj_kuboj.publikigo = kwargs.get('publikigo', universo_kosmo_stelsistemoj_kuboj.publikigo)
                                universo_kosmo_stelsistemoj_kuboj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_kosmo_stelsistemoj_kuboj.stelsistemo = stelsistemo if kwargs.get('stelsistemo_id', False) else universo_kosmo_stelsistemoj_kuboj.stelsistemo

                                universo_kosmo_stelsistemoj_kuboj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoKosmoStelsistemoKubo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoKosmoStelsistemoKubo(status=status, message=message, universo_kosmo_stelsistemoj_kuboj=universo_kosmo_stelsistemoj_kuboj)


class UniversoKosmoMutations(graphene.ObjectType):
    redaktu_universo_kosmo_stelsistemo_tipo = RedaktuUniversoKosmoStelsistemoTipo.Field(
        description=_('''Создаёт или редактирует модель типа звёздных систем в Универсо''')
    )
    redaktu_universo_kosmo_stelsistemoj = RedaktuUniversoKosmoStelsistemo.Field(
        description=_('''Создаёт или редактирует модель звёздных систем в Универсо''')
    )
    redaktu_universo_kosmo_stelsistemoj_kuboj = RedaktuUniversoKosmoStelsistemoKubo.Field(
        description=_('''Создаёт или редактирует модель кубов (ячейки, чанки) звёздных систем в Универсо''')
    )
