# Generated by Django 2.2.11 on 2020-04-21 12:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('universo_kosmo', '0002_auto_20200321_1828'),
    ]

    operations = [
        migrations.AddField(
            model_name='universokosmostelsistemo',
            name='sxablono_sistema',
            field=models.BooleanField(default=False, verbose_name='Sistema ŝablono'),
        ),
        migrations.AddField(
            model_name='universokosmostelsistemo',
            name='sxablono_sistema_id',
            field=models.IntegerField(blank=True, default=None, null=True, verbose_name='Sistema ŝablono ID'),
        ),
        migrations.AddField(
            model_name='universokosmostelsistemokubo',
            name='sxablono_sistema',
            field=models.BooleanField(default=False, verbose_name='Sistema ŝablono'),
        ),
        migrations.AddField(
            model_name='universokosmostelsistemokubo',
            name='sxablono_sistema_id',
            field=models.IntegerField(blank=True, default=None, null=True, verbose_name='Sistema ŝablono ID'),
        ),
        migrations.AddField(
            model_name='universokosmostelsistemotipo',
            name='sxablono_sistema',
            field=models.BooleanField(default=False, verbose_name='Sistema ŝablono'),
        ),
        migrations.AddField(
            model_name='universokosmostelsistemotipo',
            name='sxablono_sistema_id',
            field=models.IntegerField(blank=True, default=None, null=True, verbose_name='Sistema ŝablono ID'),
        ),
    ]
