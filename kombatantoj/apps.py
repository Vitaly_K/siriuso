from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class KombatantojConfig(AppConfig):
    name = 'kombatantoj'
    verbose_name = _('Kombatantoj')

    def ready(self):
        import kombatantoj.signals
