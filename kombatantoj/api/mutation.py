"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.db import transaction
import graphene

from siriuso.api.types import ErrorNode
from siriuso.utils import set_enhavo
from komunumoj.models import Komunumo
from kombatantoj.models import Kombatanto, KombatantoBonusPunkto
from main.models import Uzanto
from .scheme import KombatantoNode, KombatantoResursoNomenklaturoNode, KombatantoResursoNode
from ..models import *
from resursoj.api.scheme import ResursoNode
from resursoj.models import Resurso


class RedaktuKombatanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    kombatanto = graphene.Field(KombatantoNode)

    class Arguments:
        uuid = graphene.UUID()
        nomo = graphene.String()
        unua_nomo = graphene.String()
        dua_nomo = graphene.String()
        familinomo = graphene.String()
        sekso = graphene.String()
        publikigo = graphene.Boolean()
        arkivo = graphene.Boolean()
        forigo = graphene.Boolean()
        tipo_kodo = graphene.String()
        speco_kodo = graphene.String()
        posedanto_uuid = graphene.UUID()
        adreso = graphene.String()
        prezaj_politikoj = graphene.String()
        informoj = graphene.String()
        komunumo_uuid = graphene.UUID()
        uzanto_id = graphene.Int()

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        kombatanto = None
        user = info.context.user

        if user.has_perm('kombatantoj.povas_krei_kombatanton'):
            tipo = None
            speco = None
            posedanto = None
            komunumo = None
            uzanto = None

            # Проверяем тип
            if not kwargs.get('tipo_kodo', False):
                errors.append(ErrorNode(
                    field='tipo_kodo',
                    message=_('Bezonata kampo')
                ))
            else:
                try:
                    tipo = KombatantoTipo.objects.get(kodo=kwargs.get('tipo_kodo'))
                except KombatantoTipo.DoesNotExist:
                    errors.append(ErrorNode(
                        field='tipo_kodo',
                        message=_('Tajpu ĉi tiun kodon ne trovitan')
                    ))

            # Проверяем вид
            if not kwargs.get('speco_kodo', False):
                errors.append(ErrorNode(
                    field='speco_kodo',
                    message=_('Bezonata kampo')
                ))
            else:
                try:
                    speco = KombatantoSpeco.objects.get(kodo=kwargs.get('speco_kodo'))
                except KombatantoSpeco.DoesNotExist:
                    errors.append(ErrorNode(
                        field='speco_kodo',
                        message=_('Vido kun ĉi tiu kodo ne trovita')
                    ))

            # Проверяем владельца
            if 'posedanto_uuid' in kwargs:
                try:
                    posedanto = Kombatanto.objects.get(uuid=kwargs.get('posedanto_uuid'))
                except Kombatanto.DoesNotExist:
                    errors.append(ErrorNode(
                        field='posedanto_uuid',
                        message=_('Posedanto ne trovita')
                    ))

            # Проверяем сообщество
            if 'komunumo_uuid' in kwargs and 'uzanto_id' in kwargs:
                errors = errors + [
                    ErrorNode(
                        field='komunumo_uuid',
                        message='{} {}'.format(
                            _('Ne eblas specifi samtempe kun'),
                            'komunumo_uuid'
                        )
                    ),
                    ErrorNode(
                        field='uzanto_id',
                        message='{} {}'.format(
                            _('Ne eblas specifi samtempe kun'),
                            'uzanto_id'
                        )
                    )
                ]
            elif 'komunumo_uuid' in kwargs:
                try:
                    komunumo = Komunumo.objects.get(uuid=kwargs.get('komunumo_uuid'), forigo=False,
                                                    arkivo=False, publikigo=True)
                except Komunumo.DoesNotExist:
                    errors.append(ErrorNode(
                        field='komunumo_uuid',
                        message=_('Komunumo ne trovita')
                    ))
            elif 'uzanto_id' in kwargs:
                try:
                    uzanto = Uzanto.objects.get(uuid=kwargs.get('komunumo_uuid'), forigo=False,
                                                arkivo=False, publikigo=True)
                except Uzanto.DoesNotExist:
                    errors.append(ErrorNode(
                        field='komunumo_uuid',
                        message=_('Uzanto ne trovita')
                    ))

            if kwargs.get('forigo', False):
                errors.append(ErrorNode(
                    field='forigo',
                    message=_('Ne eblas forviŝi dum kreado')
                ))

            if kwargs.get('arkivo', False):
                errors.append(ErrorNode(
                    field='arkivo',
                    message=_('Ne eblas arkivi dum kreado')
                ))

            if 'prezaj_politikoj' in kwargs and kwargs.get('prezaj_politikoj').lower() not in (
                    'podetala', 'pogranda', 'filio'
            ):
                errors.append(ErrorNode(
                    field='prezaj_politikoj',
                    message=_('Nevalida valoro')
                ))

            if tipo:
                if tipo.kodo == 'homo':
                    if kwargs.get('nomo', False):
                        errors.append(ErrorNode(
                            field='nomo',
                            message=_('Ĉi tiu kampo ne validas por tipo "Homo"')
                        ))

                    if not kwargs.get('sekso', False):
                        errors.append(ErrorNode(
                            field='sekso',
                            message=_('Bezonata kampo')
                        ))
                    elif kwargs.get('sekso').lower() not in ('vira', 'virina'):
                        errors.append(ErrorNode(
                            field='sekso',
                            message=_('Nevalida valoro')
                        ))

                    if not kwargs.get('unua_nomo', False):
                        errors.append(ErrorNode(
                            field='unua_nomo',
                            message=_('Bezonata kampo')
                        ))

                    if not kwargs.get('familinomo', False):
                        errors.append(ErrorNode(
                            field='familinomo',
                            message=_('Bezonata kampo')
                        ))
                else:
                    if kwargs.get('unua_nomo', False):
                        errors.append(ErrorNode(
                            field='unua_nomo',
                            message=_('Ĉi tiu kampo estas nur por tipo "Homo"')
                        ))

                    if kwargs.get('familinomo', False):
                        errors.append(ErrorNode(
                            field='familinomo',
                            message=_('Ĉi tiu kampo estas nur por tipo "Homo"')
                        ))

                    if kwargs.get('sekso', False):
                        errors.append(ErrorNode(
                            field='sekso',
                            message=_('Ĉi tiu kampo estas nur por tipo "Homo"')
                        ))

                    if not kwargs.get('nomo', False):
                        errors.append(ErrorNode(
                            field='nomo',
                            message=_('Bezonata kampo')
                        ))

            if not len(errors):
                kombatanto = Kombatanto.objects.create(
                    autoro=user,
                    forigo=False,
                    arkivo=False,
                    publikigo=True,
                    publikiga_dato=timezone.now(),
                    tipo=tipo,
                    speco=speco,
                    posedanto=posedanto,
                    adreso=kwargs.get('adreso', ''),
                    prezaj_politikoj=kwargs.get('prezaj_politikoj', 'podetala').lower(),
                    komunumo=komunumo,
                    uzanto=uzanto
                )

                if tipo.kodo == 'homo':
                    set_enhavo(kombatanto.unua_nomo, kwargs.get('unua_nomo'), 'ru_RU')
                    set_enhavo(kombatanto.dua_nomo, kwargs.get('dua_nomo'), 'ru_RU')
                    set_enhavo(kombatanto.familinomo, kwargs.get('familinomo'), 'ru_RU')
                    kombatanto.sekso = kwargs.get('sekso').lower()  if 'sekso' in kwargs else None
                else:
                    set_enhavo(kombatanto.nomo, kwargs.get('nomo'), 'ru_RU')

                kombatanto.save()
                status = True
                message = _('Kombatanto sukcese kreiĝis')
                errors = None
            else:
                message = _('Nevalida argumentvaloroj')
                for error in errors:
                    message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                        error.message, _('в поле'), error.field)
        else:
            message = _('Nesufiĉaj rajtoj')

        return status, message, errors, kombatanto

    @staticmethod
    def edit(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        kombatanto = None
        user = info.context.user

        if (user.has_perm('kombatantoj.povas_shanghi_kombatanton')
                or user.has_perm('kombatantoj.povas_forigi_kombatanton')):
            pass
        else:
            message = _('Nesufiĉaj rajtoj')

        return status, message, errors, kombatanto

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        kombatanto = None
        user = info.context.user

        if user.is_authenticated:
            if 'uuid' in kwargs:
                # Изменяем запись
                status, message, errors, kombatanto = RedaktuKombatanto.edit(root, info, **kwargs)
            else:
                # Создаём запись
                status, message, errors, kombatanto = RedaktuKombatanto.create(root, info, **kwargs)
        else:
            message = _('Bezonata rajtigo')

        return RedaktuKombatanto(status=status, message=message, errors=errors, kombatanto=kombatanto)


class RedaktuKombatantoNomenklaturo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    organizo_resurso_nomenklaturo = graphene.Field(KombatantoResursoNomenklaturoNode)

    class Arguments:
        uuid = graphene.UUID()
        organizo_uuid = graphene.UUID()
        stokado_uuid = graphene.UUID()
        resurso_uuid = graphene.UUID()
        minimuma = graphene.Float()
        maksimuma = graphene.Float()
        publikigo = graphene.Boolean()
        forigo = graphene.Boolean()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = []
        organizo = None
        resurso = None
        stokado = None
        nomenklaturo = None
        user = info.context.user

        if (user.has_perm('kombatantoj.povas_krei_resurson_nomenklaturon')
                or user.has_perm('kombatantoj.povas_shanghi_resurson_nomenklaturon')
                or user.has_perm('kombatantoj.povas_forigi_resurson_nomenklaturon')):
            # Создаём новую запись
            if 'uuid' not in kwargs:
                if user.has_perm('kombatantoj.povas_krei_resurson_nomenklaturon'):
                    if 'organizo_uuid' not in kwargs:
                        errors.append(
                            ErrorNode(
                                field='organizo_uuid',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    else:
                        try:
                            organizo = Kombatanto.objects.get(uuid=kwargs.get('organizo_uuid'), forigo=False, publikigo=True)
                        except Kombatanto.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='organizo_uuid',
                                    message=_('Организация не найдена')
                                )
                            )

                    if 'stokado_uuid' not in kwargs:
                        errors.append(
                            ErrorNode(
                                field='stokado_uuid',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    elif organizo is None:
                        errors.append(
                            ErrorNode(
                                field='stokado_uuid',
                                message=_('Невозможно определить место хранения')
                            )
                        )
                    else:
                        try:
                            stokado = Kombatanto.objects.get(uuid=kwargs.get('stokado_uuid'), posedanto=organizo,
                                                           publikigo=True, forigo=False)
                        except Kombatanto.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='stokado_uuid',
                                    message=_('Место хранения не найдено')
                                )
                            )

                    if 'resurso_uuid' not in kwargs:
                        errors.append(
                            ErrorNode(
                                field='resurso_uuid',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    else:
                        try:
                            resurso = Resurso.objects.get(uuid=kwargs.get('resurso_uuid'), forigo=False, publikigo=True)
                        except Resurso.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='resurso_uuid',
                                    message=_('Ресурс не найден')
                                )
                            )

                    if 'minimuma' not in kwargs:
                        errors.append(
                            ErrorNode(
                                field='minimuma',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    elif kwargs.get('minimuma') < 0:
                        errors.append(
                            ErrorNode(
                                field='minimuma',
                                message=_('Значени должно быть положительным')
                            )
                        )

                    if 'maksimuma' in kwargs:
                        if kwargs.get('maksimuma') < 0:
                            errors.append(
                                ErrorNode(
                                    field='maksimuma',
                                    message=_('Значени должно быть положительным')
                                )
                            )
                        elif kwargs.get('maksimuma') < kwargs.get('minimuma') and kwargs.get('maksimuma'):
                            errors.append(
                                ErrorNode(
                                    field='maksimuma',
                                    message=_('Значени должно быть больше минимального объёма')
                                )
                            )

                    if 'forigo' in kwargs and kwargs.get('forigo'):
                        errors.append(
                            ErrorNode(
                                field='forigo',
                                message=_('Нельзя удалить не созданный ресурс')
                            )
                        )

                    if not len(errors):
                        nomenklaturo = KombatantoResursoNomenklaturo.objects.create(
                            organizo=organizo,
                            resurso=resurso,
                            stokado=stokado,
                            autoro=user,
                            minimuma=kwargs.get('minimuma'),
                            maksimuma=kwargs.get('maksimuma'),
                            publikigo=kwargs.get('publikigo', False),
                            forigo=kwargs.get('forigo', False)
                        )

                        status = True
                        message = _('Запись успешно создана')
                    else:
                        message = _('Неверные значения аргументов')
                        for error in errors:
                            message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                error.message, _('в поле'), error.field)
                else:
                    message = _('Недостаточно прав на создание записи')
            else:
                # Изменяем запись
                try:
                    nomenklaturo = KombatantoResursoNomenklaturo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                except:
                    errors.append(
                        ErrorNode(
                            field='uuid',
                            message=_('Запись не найдена')
                        )
                    )

                if kwargs.get('forigo', False) and not user.has_perm('kombatantoj.povas_forigi_resurson_nomenklaturon'):
                    errors.append(
                        ErrorNode(
                            field='forigo',
                            message=_('Недостаточно прав на удаление')
                        )
                    )

                if not user.has_perm('kombatantoj.povas_shanghi_resurson_nomenklaturon') and (
                    'organizo_uuid' in kwargs or 'resurso_uuid' in kwargs or 'publikigo' in kwargs
                ):
                    message = _('Недостаточно прав для изменения записи')

                else:
                    if 'organizo_uuid' in kwargs:
                        try:
                            organizo = Kombatanto.objects.get(uuid=kwargs.get('organizo_uuid'), forigo=False, publikigo=True)
                        except Kombatanto.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='organizo_uuid',
                                    message=_('Организация не найдена')
                                )
                            )
                    else:
                        organizo = nomenklaturo.organizo

                    if 'stokado_uuid' in kwargs:
                        try:
                            stokado = Kombatanto.objects.get(uuid=kwargs.get('stokado_uuid'), posedanto=organizo,
                                                           publikigo=True, forigo=False)
                        except Kombatanto.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='stokado_uuid',
                                    message=_('Место хранения не найдено')
                                )
                            )

                    if 'resurso_uuid' in kwargs:
                        try:
                            resurso = Resurso.objects.get(uuid=kwargs.get('resurso_uuid'), forigo=False, publikigo=True)
                        except Resurso.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='resurso_uuid',
                                    message=_('Ресурс не найден')
                                )
                            )

                    if kwargs.get('minimuma', 0) < 0:
                        errors.append(
                            ErrorNode(
                                field='minimuma',
                                message=_('Значени должно быть положительным')
                            )
                        )

                    if 'maksimuma' in kwargs and nomenklaturo:
                        if kwargs.get('maksimuma') < 0:
                            errors.append(
                                ErrorNode(
                                    field='maksimuma',
                                    message=_('Значени должно быть положительным')
                                )
                            )
                        elif (kwargs.get('maksimuma') and
                              kwargs.get('maksimuma') < kwargs.get('minimuma', nomenklaturo.minimuma)
                              and kwargs.get('maksimuma')):
                            errors.append(
                                ErrorNode(
                                    field='maksimuma',
                                    message=_('Значени должно быть больше минимального объёма')
                                )
                            )

                    if not len(errors):
                        nomenklaturo.organizo = organizo or nomenklaturo.organizo
                        nomenklaturo.stokado = stokado or nomenklaturo.stokado
                        nomenklaturo.resurso = resurso or nomenklaturo.resurso
                        nomenklaturo.minimuma = kwargs.get('minimuma', nomenklaturo.minimuma)
                        nomenklaturo.maksimuma = kwargs.get('maksimuma', nomenklaturo.maksimuma) or None
                        nomenklaturo.publikigo = kwargs.get('publikigo', nomenklaturo.publikigo)
                        nomenklaturo.forigo = kwargs.get('forigo', nomenklaturo.forigo)
                        nomenklaturo.save()

                        status = True
                        message = _('Запись успешно изменена')
                    else:
                        nomenklaturo = None
                        message = _('Неверные значения аргументов')
                        for error in errors:
                            message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                error.message, _('в поле'), error.field)

        else:
            message = _('Недостаточно прав')

        return RedaktuKombatantoNomenklaturo(status=status, message=message, errors=errors,
                                           organizo_resurso_nomenklaturo=nomenklaturo)


class ForigiKombatantoResurso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    organizo_resurso = graphene.Field(KombatantoResursoNode)

    class Arguments:
        resurso_uuid = graphene.UUID(required=True)
        resanigi = graphene.Boolean()

    @staticmethod
    def mutate(root, info, resurso_uuid, **kwargs):
        status = False
        message = None
        resurso = None
        user = info.context.user
        homo = None

        if user.is_authenticated:
            with transaction.atomic():
                # не верно настроены права - нужно будет исправить данные права. Сейчас еще не настроен приём товара.
                if not user.has_perm('dokumentoj.drinkejisto'):
                    message = _('Недостаточно прав для списания товара')

                try:
                    resurso = KombatantoResurso.objects.get(uuid=resurso_uuid, publikigo=True, forigo=False)

                    if resurso.elcherpita and not kwargs.get('resanigi', False):
                        message = _('Товар уже списан')

                except KombatantoResurso.DoesNotExist:
                    message = _('Товар не найден')

                if not message and 'resanigi' in kwargs:
                    # не верно настроены права - нужно будет исправить данные права. Сейчас еще не настроен приём товара.
                    if not user.has_perm('dokumentoj.organiza_administranto'):
                        message = _('Недостаточно прав для использования аргумента')

                if not message:
                    resurso.elcherpita = not kwargs.get('resanigi', False)
                    resurso.save()

                    try:
                        homo = Kombatanto.objects.get(uzanto=user)

                        if resurso.elcherpita:
                            KombatantoBonusPunkto.objects.create(
                                homo=homo,
                                resurso_nomenklaturo=resurso,
                                resurso=resurso.resurso,
                                bonusoj_punktoj=resurso.resurso.bonus_punkto,
                                publikigo=True,
                                forigo=False
                            )
                        else:
                            KombatantoBonusPunkto.objects.select_for_update().filter(
                                publikigo=True,
                                forigo=False,
                                homo=homo,
                                resurso_nomenklaturo=resurso,
                                resurso=resurso.resurso
                            ).update(forigo=True)
                    except Kombatanto.DoesNotExist:
                        pass

                    status = True
                    message = _('Товар успешно списан')

        else:
            message = _('Необходимо авторизироваться')

        return ForigiKombatantoResurso(status=status, message=message, organizo_resurso=resurso)


class KombatantoMutation(graphene.ObjectType):
    redaktu_kombatanto = RedaktuKombatanto.Field()
    redaktu_kombatanto_nomenklaturo = RedaktuKombatantoNomenklaturo.Field()
    forigi_kombatanto_resurso = ForigiKombatantoResurso.Field()
