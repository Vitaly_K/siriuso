from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class LokalizoConfig(AppConfig):
    name = 'lokalizo'
    verbose_name = _('Lokalizo')

    # def ready(self):
    #     import interfaco.signals