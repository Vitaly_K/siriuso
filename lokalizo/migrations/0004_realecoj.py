# Generated by Django 2.0.13 on 2019-07-17 09:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_realecoj'),
        ('lokalizo', '0003_auto_20190421_1709'),
    ]

    operations = [
        migrations.AddField(
            model_name='lokalizoelemento',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='lokalizoelemento',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AddField(
            model_name='lokalizoelementotipo',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='lokalizoelementotipo',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
    ]
