"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

import graphene
from graphene_django import DjangoObjectType
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions
from graphene_permissions.permissions import AllowAny
from django.db.models import Q, F, ForeignKey, Value, CharField
from django.utils.translation import ugettext_lazy as _
from siriuso.api.filters import SiriusoFilterConnectionField
from siriuso.api.types import SiriusoLingvo
from ..models import *


class LokalizoTradukistoNode(SiriusoAuthNode, DjangoObjectType):
    """
    Уровень переводчика локализации
    """
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }


    class Meta:
        model = LokalizoTradukisto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'posedanto__id': ['exact'],
            'posedanto__uuid': ['exact'],
        }
        interfaces = (graphene.relay.Node,)

class LokalizoElementoTipoNode(SiriusoAuthNode, DjangoObjectType):
    """
    Тип страницы локализации
    """
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование типа элемента локализации'))

    class Meta:
        model = LokalizoElementoTipo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)


class LokalizoElementoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    """
    Страница локализации
    """
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'teksto__enhavo': ['contains', 'icontains'],
    }

    teksto = graphene.Field(SiriusoLingvo, description=_('Текст элемента локализации'))

    class Meta:
        model = LokalizoElemento
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


class LokalizoQuery(graphene.ObjectType):
    lokalizoj_pagxoj_tipoj = SiriusoFilterConnectionField(LokalizoElementoTipoNode,
                                                          description=_('Выводит все доступные типы элементов локализации'))
    lokalizoj_pagxoj = SiriusoFilterConnectionField(LokalizoElementoNode,
                                                    description=_('Выводит все доступные элементы локализации'))
    lokalizoj_tradukisto = SiriusoFilterConnectionField(LokalizoTradukistoNode,
                                                    description=_('Выводит всех переводчиков с уровнями'))
