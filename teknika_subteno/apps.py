from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class TeknikaSubtenoConfig(AppConfig):
    name = 'teknika_subteno'
    verbose_name = _('Teknika_subteno')
