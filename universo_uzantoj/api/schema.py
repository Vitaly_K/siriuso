import graphene  # сам Graphene
from django.utils.translation import ugettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import *  # модели приложения

import re


# Модель пользователей Universo
class UniversoUzantoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = UniversoUzanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'siriuso_uzanto__id': ['exact'],
            'siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


class UniversoUzantoQuery(graphene.ObjectType):
    universo_uzanto = SiriusoFilterConnectionField(
        UniversoUzantoNode,
        description=_('Выводит все доступные модели пользователей Universo')
    )
