"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
16.05.2019
"""
from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from .schema import *
from ..models import *


# Модель пользователей Universo
class RedaktuUniversoUzanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_uzanto = graphene.Field(UniversoUzantoNode, description=_('Созданный/изменённый пользователь Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        retnomo = graphene.String(description=_('никнейм'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_uzanto = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    # Т.к. пользователь создаёт запись по себе, то проверкв наличия прав не требуется
                    # if uzanto.has_perm('universo_uzantoj.povas_krei_universo_uzantoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not kwargs.get('retnomo', False):
                            message = '{} "{}"'.format(
                                _('При создании записи поле обязательно для заполнения'),
                                'retnomo'
                            )

                        try:
                            UniversoUzanto.objects.get(retnomo=kwargs.get('retnomo'))
                            message = _('Такой никнейм уже используется')
                        except:
                            pass

                        if not message:
                            universo_uzanto = UniversoUzanto.objects.create(
                                forigo=False,
                                arkivo=False,
                                siriuso_uzanto = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now(),
                                retnomo=kwargs.get('retnomo')
                            )

                            universo_uzanto.save()

                            status = True
                            message = _('Запись создана')
                    # else:
                    #     message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('retnomo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    try:
                        UniversoUzanto.objects.get(retnomo=kwargs.get('retnomo'))
                        message = _('Такой никнейм уже используется')
                    except:
                        pass

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_uzanto = UniversoUzanto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_uzantoj.povas_forigi_universo_uzantoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_uzantoj.povas_shanghi_universo_uzantoj'):
                                if uzanto != universo_uzanto.siriuso_uzanto:
                                    message = _('Недостаточно прав для изменения')

                            if not message:
                                universo_uzanto.forigo = kwargs.get('forigo', universo_uzanto.forigo)
                                universo_uzanto.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_uzanto.arkivo = kwargs.get('arkivo', universo_uzanto.arkivo)
                                universo_uzanto.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_uzanto.publikigo = kwargs.get('publikigo', universo_uzanto.publikigo)
                                universo_uzanto.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_uzanto.retnomo = kwargs.get('retnomo', universo_uzanto.retnomo)

                                universo_uzanto.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoUzanto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoUzanto(status=status, message=message, universo_uzanto=universo_uzanto)


class UniversoUzantoMutations(graphene.ObjectType):
    redaktu_universo_uzanto = RedaktuUniversoUzanto.Field(
        description=_('''Создаёт или редактирует пользователей Universo''')
    )
