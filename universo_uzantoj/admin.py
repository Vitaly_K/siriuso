"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2020 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from django import forms
from django.utils.translation import ugettext_lazy as _
import json

from siriuso.utils import get_enhavo, set_enhavo
from universo_uzantoj.models import *
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget


class TekstoMixin:
    @staticmethod
    def teksto(obj, field):
        kampo = getattr(obj, field)

        if kampo is not None:
            result = get_enhavo(kampo)

            if result:
                return result[0]
        return ''

    def email(self, obj):
        return obj.siriuso_uzanto.chefa_retposhto

# Форма для пользователей Universo
class UniversoUzantoFormo(forms.ModelForm):
    
    class Meta:
        model = UniversoUzanto
        fields = [field.name for field in UniversoUzanto._meta.fields if field.name not in ('krea_dato', 'uuid')]


# Пользователи Universo
@admin.register(UniversoUzanto)
class UniversoUzantoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoUzantoFormo
    list_display = ('siriuso_uzanto_id','retnomo','email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoUzanto

