"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from uuid import uuid4
from django.db import models
from django.db.models import Max, Q
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres import fields as pgfields
from django.contrib.auth.models import Permission

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from main.models import Uzanto


# Пользователь Universo
class UniversoUzanto(models.Model):

    # UUID записи
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid4, editable=False)

    # дата и время создания
    krea_dato = models.DateTimeField(_('Krea dato'), auto_now_add=True, auto_now=False, blank=False)

    # помечена на удаление (да или нет)
    forigo = models.BooleanField(_('Forigo'), blank=True, default=False)

    # дата и время пометки на удаление
    foriga_dato = models.DateTimeField(_('Foriga dato'), auto_now_add=False, auto_now=False, blank=True, null=True,
                                       default=None)

    # опубликовано (да или нет)
    publikigo = models.BooleanField(_('Publikigis'), default=False)

    # дата и время публикации
    publikiga_dato = models.DateTimeField(_('Dato de publikigo'), auto_now_add=False, auto_now=False, blank=True,
                                          null=True, default=None)

    # архивная (да или нет)
    arkivo = models.BooleanField(_('Arkiva'), default=False)

    # дата и время помещения в архив
    arkiva_dato = models.DateTimeField(_('Arkiva dato'), auto_now_add=False, auto_now=False, blank=True, null=True,
                                       default=None)

    # Связь с моделью пользователя Сириусо, то есть пока с основной моделью пользователя
    siriuso_uzanto = models.OneToOneField(Uzanto, on_delete=models.CASCADE)

    # никнейм, многоязычный в JSON формате
    retnomo = models.CharField(verbose_name=_('Retnomo'), max_length=64, unique=True, default=None)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_uzanto'
        # читабельное название модели, в единственном числе
        verbose_name = _('Uzanto de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Uzantoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_uzantoj', _('Povas vidi uzantoj de Universo')),
            ('povas_krei_universo_uzantoj', _('Povas krei uzantoj de Universo')),
            ('povas_forigi_universo_uzantoj', _('Povas forigi uzantoj de Universo')),
            ('povas_shangxi_universo_uzantoj', _('Povas ŝanĝi uzantoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле retnomo этой модели
        out = '{} ({}) {} {}'.format(
            self.retnomo,
            self.siriuso_uzanto.chefa_retposhto,
            get_enhavo(self.siriuso_uzanto.familinomo, empty_values=True)[0],
            get_enhavo(self.siriuso_uzanto.unua_nomo, empty_values=True)[0]
        )
        return out

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_uzantoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_uzantoj.povas_vidi_universo_uzantoj', 'universo_uzantoj.povas_krei_universo_uzantoj',
                'universo_uzantoj.povas_forigi_universo_uzantoj', 'universo_uzantoj.povas_shangxi_universo_uzantoj'
            ))

            # Добавляем прав владельцу записи
            # if user_obj==self.siriuso_uzanto:
            # Добавляем прав всем на просмотр, т.к. нужно видеть других игроков
            # all_perms.update(['povas_vidi_universo_uzantoj','povas_krei_universo_uzantoj',
            #     'povas_forigi_universo_uzantoj', 'povas_shanghi_universo_uzantoj'])

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_uzantoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            cond = Q() # все авторизованные игроки могут смотреть существование других игроков
            # Для авторизированного пользователя
            # if (perms.has_registrita_perm('universo_uzantoj.povas_vidi_universo_uzantoj')
            #         or user_obj.has_perm('universo_uzantoj.povas_vidi_universo_uzantoj')):
            #     # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
            #     cond = Q()
            # else:
            #     # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
            #     cond = Q(uuid__isnull=True)
            #     # Просмотр вледльцу записи
            #     cond |= Q(siriuso_uzanto=user_obj)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_uzantoj.povas_vidi_universo_uzantoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
