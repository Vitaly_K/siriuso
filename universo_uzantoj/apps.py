from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UniversoUzantoConfig(AppConfig):
    name = 'universo_uzantoj'
    verbose_name = _('Uzanto de Universo')
