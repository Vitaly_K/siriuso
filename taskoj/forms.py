"""
[EO - Internacia Lingvo (Esperanto)]

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django import forms
from .models import TaskojUzantoTasko


# Задачи пользователей
class TaskojUzantoTaskoFormo(forms.ModelForm):

    # дополнительные настройки для формы
    class Meta:

        # какая модель используется для формы
        model = TaskojUzantoTasko

        # какие поля формы будут использоваться
        fields = ('etapo', 'tipo', 'speco', 'priskribo')
