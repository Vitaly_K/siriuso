"""
21.06.2019
"""
from django.utils import timezone
from django.db import transaction # создание и изменения будем делать в блокирующейтранзакции
from django.utils.translation import ugettext_lazy as _
import graphene

from siriuso.utils import set_enhavo

# Будем возращать узел типа страницы в качетсве результата
from .schema import *
from ..models import *

# Типы реализаций задач
class RedaktuTaskojTaskoRealigoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    tasko_realigo_tipo = graphene.Field(TaskojTaskoRealigoTipoNode, description=_('Созданная/изменённая запись типа реализации задачи'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа категории'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_taskojn_realigojn_tipon'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            TaskojTaskoRealigoTipo.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except TaskojTaskoRealigoTipo.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            tipo = TaskojTaskoRealigoTipo.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            tipo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tipo = TaskojTaskoRealigoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_taskojn_realigojn_tipon')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_taskojn_realigojn_tipon'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                tipo.kodo = kwargs.get('kodo', tipo.kodo)
                                tipo.forigo = kwargs.get('forigo', tipo.forigo)
                                tipo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tipo.arkivo = kwargs.get('arkivo', tipo.arkivo)
                                tipo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tipo.publikigo = kwargs.get('publikigo', tipo.publikigo)
                                tipo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                tipo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojTaskoRealigoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojTaskoRealigoTipo(status=status, message=message, tasko_realigo_tipo=tipo)

# Типы проектов (личных / групповых)
class RedaktuTaskojProjektoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projekto_tipo = graphene.Field(TaskojProjektoTipoNode, description=_('Созданная/изменённая запись типа проектов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа категории'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_projektojn_tipon'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            TaskojProjektoTipo.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except TaskojProjektoTipo.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            tipo = TaskojProjektoTipo.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            tipo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tipo = TaskojProjektoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_projektojn_tipon')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_projektojn_tipon'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                tipo.kodo = kwargs.get('kodo', tipo.kodo)
                                tipo.forigo = kwargs.get('forigo', tipo.forigo)
                                tipo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tipo.arkivo = kwargs.get('arkivo', tipo.arkivo)
                                tipo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tipo.publikigo = kwargs.get('publikigo', tipo.publikigo)
                                tipo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                tipo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojProjektoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojProjektoTipo(status=status, message=message, projekto_tipo=tipo)

# Виды проектов (личных / групповых)
class RedaktuTaskojProjektoSpeco(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projekto_speco = graphene.Field(TaskojProjektoSpecoNode, description=_('Созданная/изменённая запись вида проектов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа категории'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        speco = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_projektojn_specoj'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            TaskojProjektoSpeco.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except TaskojProjektoSpeco.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            speco = TaskojProjektoSpeco.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(speco.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            speco.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            speco = TaskojProjektoSpeco.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_projektojn_specoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_projektojn_specoj'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                speco.kodo = kwargs.get('kodo', speco.kodo)
                                speco.forigo = kwargs.get('forigo', speco.forigo)
                                speco.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                speco.arkivo = kwargs.get('arkivo', speco.arkivo)
                                speco.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                speco.publikigo = kwargs.get('publikigo', speco.publikigo)
                                speco.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(speco.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                speco.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojProjektoSpeco.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojProjektoSpeco(status=status, message=message, projekto_speco=speco)

# Типы направлений задач
class RedaktuTaskojDirektoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    direkto_tipo = graphene.Field(TaskojDirektoTipoNode, description=_('Созданная/изменённая запись типа направлений задач'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа категории'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_direktojn_tipon'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            TaskojDirektoTipo.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except TaskojDirektoTipo.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            tipo = TaskojDirektoTipo.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            tipo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tipo = TaskojDirektoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_direktojn_tipon')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_direktojn_tipon'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                tipo.kodo = kwargs.get('kodo', tipo.kodo)
                                tipo.forigo = kwargs.get('forigo', tipo.forigo)
                                tipo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tipo.arkivo = kwargs.get('arkivo', tipo.arkivo)
                                tipo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tipo.publikigo = kwargs.get('publikigo', tipo.publikigo)
                                tipo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                tipo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojDirektoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojDirektoTipo(status=status, message=message, direkto_tipo=tipo)

# Виды направлений задач
class RedaktuTaskojDirektoSpeco(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    direkto_speco = graphene.Field(TaskojDirektoSpecoNode, description=_('Созданная/изменённая запись вида направления задачи'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа категории'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        speco = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_direktojn_specoj'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            TaskojDirektoSpeco.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except TaskojDirektoSpeco.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            speco = TaskojDirektoSpeco.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(speco.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            speco.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            speco = TaskojDirektoSpeco.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_direktojn_specoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_direktojn_specoj'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                speco.kodo = kwargs.get('kodo', speco.kodo)
                                speco.forigo = kwargs.get('forigo', speco.forigo)
                                speco.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                speco.arkivo = kwargs.get('arkivo', speco.arkivo)
                                speco.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                speco.publikigo = kwargs.get('publikigo', speco.publikigo)
                                speco.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(speco.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                speco.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojDirektoSpeco.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojDirektoSpeco(status=status, message=message, direkto_speco=speco)

# Типы этапов задач
class RedaktuTaskojEtapoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    etapo_tipo = graphene.Field(TaskojEtapoTipoNode, description=_('Созданная/изменённая запись типа этапа задачи'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа категории'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_etapojn_tipon'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            TaskojEtapoTipo.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except TaskojEtapoTipo.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            tipo = TaskojEtapoTipo.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            tipo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tipo = TaskojEtapoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_etapojn_tipon')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_etapojn_tipon'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                tipo.kodo = kwargs.get('kodo', tipo.kodo)
                                tipo.forigo = kwargs.get('forigo', tipo.forigo)
                                tipo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tipo.arkivo = kwargs.get('arkivo', tipo.arkivo)
                                tipo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tipo.publikigo = kwargs.get('publikigo', tipo.publikigo)
                                tipo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                tipo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojEtapoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojEtapoTipo(status=status, message=message, etapo_tipo=tipo)

# Виды этапов задач
class RedaktuTaskojEtapoSpeco(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    etapo_speco = graphene.Field(TaskojEtapoSpecoNode, description=_('Созданная/изменённая запись вида этапов задачи'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа категории'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        speco = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_etapojn_specoj'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            TaskojEtapoSpeco.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except TaskojEtapoSpeco.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            speco = TaskojEtapoSpeco.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(speco.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            speco.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            speco = TaskojEtapoSpeco.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_etapojn_specoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_etapojn_specoj'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                speco.kodo = kwargs.get('kodo', speco.kodo)
                                speco.forigo = kwargs.get('forigo', speco.forigo)
                                speco.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                speco.arkivo = kwargs.get('arkivo', speco.arkivo)
                                speco.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                speco.publikigo = kwargs.get('publikigo', speco.publikigo)
                                speco.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(speco.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                speco.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojEtapoSpeco.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojEtapoSpeco(status=status, message=message, etapo_speco=speco)

# Типы задач
class RedaktuTaskojTaskoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    tasko_tipo = graphene.Field(TaskojTaskoTipoNode, description=_('Созданная/изменённая запись типа задачи'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа категории'))
        tipo_realigoj = graphene.List(graphene.String, description=_('Возможные типы реализации задачи, передаются kodo списком'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tipo = None
        tipo_realigo = []
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_taskojn_tipon'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            TaskojTaskoTipo.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except TaskojTaskoTipo.DoesNotExist:
                            pass

                        # проверяем наличие записей с таким кодом в списке
                        if kwargs.get('tipo_realigoj', False) and not message:
                            for realigo in kwargs.get('tipo_realigoj'):
                                try:
                                    tipo_realigo.append(TaskojTaskoRealigoTipo.objects.get(kodo=realigo, forigo=False,
                                                                     arkivo=False, publikigo=True))
                                except TaskojTaskoRealigoTipo.DoesNotExist:
                                    message = _('Неверный возможный тип реализации задачи'+realigo)

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            tipo = TaskojTaskoTipo.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            tipo.save()

                            tipo.tipo_realigo.set(tipo_realigo)

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('tipo_realigoj', False)
                            or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_realigoj' in kwargs and not message:
                        for realigo in kwargs.get('tipo_realigoj'):
                            try:
                                tipo_realigo.append(TaskojTaskoRealigoTipo.objects.get(kodo=realigo, forigo=False,
                                                                arkivo=False, publikigo=True))
                            except TaskojTaskoRealigoTipo.DoesNotExist:
                                message = _('Неверный возможный тип реализации задачи'+realigo)

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tipo = TaskojTaskoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_taskojn_tipon')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_taskojn_tipon'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                tipo.kodo = kwargs.get('kodo', tipo.kodo)
                                tipo.forigo = kwargs.get('forigo', tipo.forigo)
                                tipo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tipo.arkivo = kwargs.get('arkivo', tipo.arkivo)
                                tipo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tipo.publikigo = kwargs.get('publikigo', tipo.publikigo)
                                tipo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                tipo.save()
                                tipo.tipo_realigo.set(tipo_realigo)
                                
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojTaskoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojTaskoTipo(status=status, message=message, tasko_tipo=tipo)

# Виды задач
class RedaktuTaskojTaskoSpeco(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    tasko_speco = graphene.Field(TaskojTaskoSpecoNode, description=_('Созданная/изменённая запись вида задачи'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа категории'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        speco = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_taskojn_specoj'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            TaskojTaskoSpeco.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except TaskojTaskoSpeco.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            speco = TaskojTaskoSpeco.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(speco.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            speco.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            speco = TaskojTaskoSpeco.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_taskojn_specoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_taskojn_specoj'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                speco.kodo = kwargs.get('kodo', speco.kodo)
                                speco.forigo = kwargs.get('forigo', speco.forigo)
                                speco.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                speco.arkivo = kwargs.get('arkivo', speco.arkivo)
                                speco.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                speco.publikigo = kwargs.get('publikigo', speco.publikigo)
                                speco.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(speco.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                speco.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojTaskoSpeco.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojTaskoSpeco(status=status, message=message, tasko_speco=speco)

# Функционал задач пользователей
# Проекты пользователей
class RedaktuTaskojUzantoProjekto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    uzanto_projecto = graphene.Field(TaskojUzantoProjektoNode, description=_('Создание/изменение проектов пользователей'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_kodo = graphene.String(description=_('Код типа проекта'))
        speco_kodo = graphene.String(description=_('Код вида проекта'))
        posedanto_id = graphene.Int(description=_('ID пользователя'))
        priskribo = graphene.String(description=_('Описание проекта пользователя'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        uzanto_projecto = None
        tipo = None
        speco = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_uzantojn_projektojn'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            if not (kwargs.get('tipo_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'tipo_kodo')
                            else:
                                try:
                                    tipo = TaskojProjektoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojProjektoTipo.DoesNotExist:
                                    message = _('Неверный тип проекта')

                        if not message:
                            if not (kwargs.get('speco_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'speco_kodo')
                            else:
                                try:
                                    speco = TaskojProjektoSpeco.objects.get(kodo=kwargs.get('speco_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojProjektoSpeco.DoesNotExist:
                                    message = _('Неверный вид проекта')

                        if not (kwargs.get('priskribo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                        if not message:
                            if not (kwargs.get('posedanto_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'posedanto_id')
                            else:
                                try:
                                    posedanto = Uzanto.objects.get(id=kwargs.get('posedanto_id'), is_active=True,
                                                konfirmita=True)
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный испольнитель задачи')

                        if not message:
                            uzanto_projecto = TaskojUzantoProjekto.objects.create(
                                tipo=tipo,
                                posedanto=posedanto,
                                speco=speco,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            update_fields = ['priskribo',]
                            set_enhavo(uzanto_projecto.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                            uzanto_projecto.save(update_fields=update_fields)

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('tipo_kodo', False) or kwargs.get('speco_kodo', False) 
                            or kwargs.get('priskribo', False) or kwargs.get('publikigo', False)
                            or kwargs.get('posedanto_id', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_kodo' in kwargs and not message:
                        try:
                            tipo = TaskojProjektoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojProjektoTipo.DoesNotExist:
                            message = _('Неверный тип проекта')

                    if 'speco_kodo' in kwargs and not message:
                        try:
                            speco = TaskojProjektoSpeco.objects.get(kodo=kwargs.get('speco_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojProjektoSpeco.DoesNotExist:
                            message = _('Неверный вид проекта')

                    if 'posedanto_id' in kwargs and not message:
                        try:
                            posedanto = Uzanto.objects.get(id=kwargs.get('posedanto_id'), is_active=True,
                                            konfirmita=True)
                        except Uzanto.DoesNotExist:
                            message = _('Неверный испольнитель задачи')

                    # Ищем запись для изменения
                    if not message:
                        try:
                            uzanto_projecto = TaskojUzantoProjekto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_uzantojn_projektojn')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_uzantojn_projektojn'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                update_fields = [
                                    'tipo', 'speco', 'forigo', 'foriga_dato', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato'
                                ]
                                uzanto_projecto.tipo = tipo or uzanto_projecto.tipo
                                uzanto_projecto.speco = speco or uzanto_projecto.speco
                                uzanto_projecto.posedanto = posedanto or uzanto_projecto.posedanto
                                uzanto_projecto.forigo = kwargs.get('forigo', uzanto_projecto.forigo)
                                uzanto_projecto.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                uzanto_projecto.arkivo = kwargs.get('arkivo', uzanto_projecto.arkivo)
                                uzanto_projecto.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                uzanto_projecto.publikigo = kwargs.get('publikigo', uzanto_projecto.publikigo)
                                uzanto_projecto.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('priskribo', False):
                                    set_enhavo(uzanto_projecto.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('priskribo')

                                uzanto_projecto.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojUzantoProjekto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojUzantoProjekto(status=status, message=message, uzanto_projecto=uzanto_projecto)

# Направления задач пользователей
class RedaktuTaskojUzantoDirekto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    uzanto_direkto = graphene.Field(TaskojUzantoDirektoNode, description=_('Создание/изменение направления задач пользователей'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_kodo = graphene.String(description=_('Код типа проекта'))
        speco_kodo = graphene.String(description=_('Код вида проекта'))
        posedanto_id = graphene.Int(description=_('ID пользователя'))

        projekto_id = graphene.Int(description=_('Код проекта'))

        priskribo = graphene.String(description=_('Описание проекта пользователя'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        uzanto_direkto = None
        tipo = None
        speco = None
        projekto = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_uzantojn_direktojn'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            if not (kwargs.get('tipo_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'tipo_kodo')
                            else:
                                try:
                                    tipo = TaskojDirektoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojDirektoTipo.DoesNotExist:
                                    message = _('Неверный тип направления')

                        if not message:
                            if not (kwargs.get('speco_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'speco_kodo')
                            else:
                                try:
                                    speco = TaskojDirektoSpeco.objects.get(kodo=kwargs.get('speco_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojDirektoSpeco.DoesNotExist:
                                    message = _('Неверный вид направления')

                        if not (kwargs.get('priskribo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                        if not message:
                            if not (kwargs.get('posedanto_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'posedanto_id')
                            else:
                                try:
                                    posedanto = Uzanto.objects.get(id=kwargs.get('posedanto_id'), is_active=True,
                                                konfirmita=True)
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный испольнитель задачи')

                        if not message:
                            if not (kwargs.get('projekto_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'projekto_id')
                            else:
                                try:
                                    projekto = TaskojUzantoProjekto.objects.get(id=kwargs.get('projekto_id'),
                                            posedanto=posedanto, forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojUzantoProjekto.DoesNotExist:
                                    message = _('Неверный проект')

                        if not message:
                            uzanto_direkto = TaskojUzantoDirekto.objects.create(
                                tipo=tipo,
                                posedanto=posedanto,
                                speco=speco,
                                projekto=projekto,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            update_fields = ['priskribo',]
                            set_enhavo(uzanto_direkto.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                            uzanto_direkto.save(update_fields=update_fields)

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('tipo_kodo', False) or kwargs.get('speco_kodo', False) 
                            or kwargs.get('priskribo', False) or kwargs.get('publikigo', False)
                            or kwargs.get('projekto_id', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_kodo' in kwargs and not message:
                        try:
                            tipo = TaskojDirektoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojDirektoTipo.DoesNotExist:
                            message = _('Неверный тип направления')

                    if 'speco_kodo' in kwargs and not message:
                        try:
                            speco = TaskojDirektoSpeco.objects.get(kodo=kwargs.get('speco_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojDirektoSpeco.DoesNotExist:
                            message = _('Неверный вид направления')

                    if 'posedanto_id' in kwargs and not message:
                        try:
                            posedanto = Uzanto.objects.get(id=kwargs.get('posedanto_id'), is_active=True,
                                            konfirmita=True)
                        except Uzanto.DoesNotExist:
                            message = _('Неверный испольнитель задачи')

                    if 'projekto_id' in kwargs and not message:
                        try:
                            projekto = TaskojUzantoProjekto.objects.get(id=kwargs.get('projekto_id'),
                                    posedanto=posedanto, forigo=False,
                                    arkivo=False, publikigo=True)
                        except TaskojUzantoProjekto.DoesNotExist:
                            message = _('Неверный проект')

                    # Ищем запись для изменения
                    if not message:
                        try:
                            uzanto_direkto = TaskojUzantoDirekto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_uzantojn_projektojn')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_uzantojn_projektojn'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                update_fields = [
                                    'tipo', 'speco', 'projekto', 'forigo', 'foriga_dato', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato'
                                ]
                                uzanto_direkto.tipo = tipo or uzanto_direkto.tipo
                                uzanto_direkto.speco = speco or uzanto_direkto.speco
                                uzanto_direkto.projekto = projekto or uzanto_direkto.projekto
                                uzanto_direkto.forigo = kwargs.get('forigo', uzanto_direkto.forigo)
                                uzanto_direkto.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                uzanto_direkto.arkivo = kwargs.get('arkivo', uzanto_direkto.arkivo)
                                uzanto_direkto.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                uzanto_direkto.publikigo = kwargs.get('publikigo', uzanto_direkto.publikigo)
                                uzanto_direkto.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('priskribo', False):
                                    set_enhavo(uzanto_direkto.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('priskribo')

                                uzanto_direkto.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojUzantoDirekto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojUzantoDirekto(status=status, message=message, uzanto_direkto=uzanto_direkto)

# Этапы задач пользователей
class RedaktuTaskojUzantoEtapo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    uzanto_etapo = graphene.Field(TaskojUzantoEtapoNode, description=_('Создание/изменение направления задач пользователей'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_kodo = graphene.String(description=_('Код типа проекта'))
        speco_kodo = graphene.String(description=_('Код вида проекта'))
        direkto_id = graphene.Int(description=_('Код проекта'))
        posedanto_id = graphene.Int(description=_('ID пользователя'))
        priskribo = graphene.String(description=_('Описание проекта пользователя'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        uzanto_etapo = None
        tipo = None
        speco = None
        direkto = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_uzantojn_etapojn'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            if not (kwargs.get('tipo_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'tipo_kodo')
                            else:
                                try:
                                    tipo = TaskojEtapoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojEtapoTipo.DoesNotExist:
                                    message = _('Неверный тип этапа')

                        if not message:
                            if not (kwargs.get('speco_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'speco_kodo')
                            else:
                                try:
                                    speco = TaskojEtapoSpeco.objects.get(kodo=kwargs.get('speco_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojEtapoSpeco.DoesNotExist:
                                    message = _('Неверный вид этапа')

                        if not (kwargs.get('priskribo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                        if not message:
                            if not (kwargs.get('posedanto_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'posedanto_id')
                            else:
                                try:
                                    posedanto = Uzanto.objects.get(id=kwargs.get('posedanto_id'), is_active=True,
                                                konfirmita=True)
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный испольнитель задачи')

                        if not message:
                            if not (kwargs.get('direkto_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'direkto_id')
                            else:
                                try:
                                    direkto = TaskojUzantoDirekto.objects.get(id=kwargs.get('direkto_id'),
                                            posedanto=posedanto, forigo=False,
                                            arkivo=False, publikigo=True)
                                except TaskojUzantoDirekto.DoesNotExist:
                                    message = _('Неверное направление')

                        if not message:
                            uzanto_etapo = TaskojUzantoEtapo.objects.create(
                                tipo=tipo,
                                posedanto=uzanto,
                                speco=speco,
                                direkto=direkto,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            update_fields = ['priskribo',]
                            set_enhavo(uzanto_etapo.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                            uzanto_etapo.save(update_fields=update_fields)

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('tipo_kodo', False) or kwargs.get('speco_kodo', False) 
                            or kwargs.get('priskribo', False) or kwargs.get('publikigo', False)
                            or kwargs.get('direkto_id', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_kodo' in kwargs and not message:
                        try:
                            tipo = TaskojEtapoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojEtapoTipo.DoesNotExist:
                            message = _('Неверный тип этапа')

                    if 'speco_kodo' in kwargs and not message:
                        try:
                            speco = TaskojEtapoSpeco.objects.get(kodo=kwargs.get('speco_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojEtapoSpeco.DoesNotExist:
                            message = _('Неверный вид этапа')

                    if 'posedanto_id' in kwargs and not message:
                        try:
                            posedanto = Uzanto.objects.get(id=kwargs.get('posedanto_id'), is_active=True,
                                            konfirmita=True)
                        except Uzanto.DoesNotExist:
                            message = _('Неверный испольнитель задачи')

                    if 'direkto_id' in kwargs and not message:
                        try:
                            direkto = TaskojUzantoDirekto.objects.get(id=kwargs.get('direkto_id'),
                                            posedanto=posedanto, forigo=False,
                                            arkivo=False, publikigo=True)
                        except TaskojUzantoDirekto.DoesNotExist:
                            message = _('Неверное направление')

                    # Ищем запись для изменения
                    if not message:
                        try:
                            uzanto_etapo = TaskojUzantoEtapo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_uzantojn_etapojn')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_uzantojn_etapojn'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                update_fields = [
                                    'tipo', 'speco', 'direkto', 'forigo', 'foriga_dato', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato'
                                ]
                                uzanto_etapo.tipo = tipo or uzanto_etapo.tipo
                                uzanto_etapo.speco = speco or uzanto_etapo.speco
                                uzanto_etapo.direkto = direkto or uzanto_etapo.direkto
                                uzanto_etapo.forigo = kwargs.get('forigo', uzanto_etapo.forigo)
                                uzanto_etapo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                uzanto_etapo.arkivo = kwargs.get('arkivo', uzanto_etapo.arkivo)
                                uzanto_etapo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                uzanto_etapo.publikigo = kwargs.get('publikigo', uzanto_etapo.publikigo)
                                uzanto_etapo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('priskribo', False):
                                    set_enhavo(uzanto_etapo.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('priskribo')

                                uzanto_etapo.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojUzantoEtapo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojUzantoEtapo(status=status, message=message, uzanto_etapo=uzanto_etapo)

# Задачи пользователей
class RedaktuTaskojUzantoTasko(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    uzanto_tasko = graphene.Field(TaskojUzantoTaskoNode, description=_('Создание/изменение направления задач пользователей'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_kodo = graphene.String(description=_('Код типа проекта'))
        speco_kodo = graphene.String(description=_('Код вида проекта'))
        etapo_id = graphene.Int(description=_('Код проекта'))
        tasko_id = graphene.Int(description=_('Код главной задачи, если это подзадача'))
        posedanto_id = graphene.Int(description=_('ID пользователя'))
        priskribo = graphene.String(description=_('Описание проекта пользователя'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        uzanto_tasko = None
        tipo = None
        speco = None
        etapo = None
        tasko = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_uzantojn_taskojn'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            if not (kwargs.get('tipo_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'tipo_kodo')
                            else:
                                try:
                                    tipo = TaskojTaskoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojTaskoTipo.DoesNotExist:
                                    message = _('Неверный тип задачи')

                        if not message:
                            if not (kwargs.get('speco_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'speco_kodo')
                            else:
                                try:
                                    speco = TaskojTaskoSpeco.objects.get(kodo=kwargs.get('speco_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojTaskoSpeco.DoesNotExist:
                                    message = _('Неверный вид задачи')

                        if not message:
                            if (kwargs.get('tasko_id', False)):
                                try:
                                    tasko = TaskojUzantoTasko.objects.get(id=kwargs.get('tasko_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojUzantoTasko.DoesNotExist:
                                    message = _('Неверная главная задача')

                        if not (kwargs.get('priskribo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                        if not message:
                            if not (kwargs.get('posedanto_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'posedanto_id')
                            else:
                                try:
                                    posedanto = Uzanto.objects.get(id=kwargs.get('posedanto_id'), is_active=True,
                                                konfirmita=True)
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный испольнитель задачи')

                        if not message:
                            if not (kwargs.get('etapo_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'etapo_id')
                            else:
                                try:
                                    etapo = TaskojUzantoEtapo.objects.get(id=kwargs.get('etapo_id'),
                                            posedanto=posedanto, forigo=False,
                                            arkivo=False, publikigo=True)
                                except TaskojUzantoEtapo.DoesNotExist:
                                    message = _('Неверный этап')

                        if not message:
                            uzanto_tasko = TaskojUzantoTasko.objects.create(
                                tipo=tipo,
                                posedanto=uzanto,
                                speco=speco,
                                etapo=etapo,
                                tasko=tasko,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            update_fields = ['priskribo',]
                            set_enhavo(uzanto_tasko.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                            uzanto_tasko.save(update_fields=update_fields)

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('tipo_kodo', False) or kwargs.get('speco_kodo', False) 
                            or kwargs.get('priskribo', False) or kwargs.get('publikigo', False)
                            or kwargs.get('etapo_id', False) or kwargs.get('tasko_id', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_kodo' in kwargs and not message:
                        try:
                            tipo = TaskojTaskoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojTaskoTipo.DoesNotExist:
                            message = _('Неверный тип задачи')

                    if 'speco_kodo' in kwargs and not message:
                        try:
                            speco = TaskojTaskoSpeco.objects.get(kodo=kwargs.get('speco_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojTaskoSpeco.DoesNotExist:
                            message = _('Неверный вид задачи')

                    if 'tasko_id' in kwargs and not message:
                        try:
                            tasko = TaskojUzantoTasko.objects.get(id=kwargs.get('tasko_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojUzantoTasko.DoesNotExist:
                            message = _('Неверная главная задача')

                    if 'posedanto_id' in kwargs and not message:
                        try:
                            posedanto = Uzanto.objects.get(id=kwargs.get('posedanto_id'), is_active=True,
                                            konfirmita=True)
                        except Uzanto.DoesNotExist:
                            message = _('Неверный испольнитель задачи')

                    if 'etapo_id' in kwargs and not message:
                        try:
                            etapo = TaskojUzantoEtapo.objects.get(id=kwargs.get('etapo_id'),
                                            posedanto=posedanto, forigo=False,
                                            arkivo=False, publikigo=True)
                        except TaskojUzantoEtapo.DoesNotExist:
                            message = _('Неверный этап')

                    # Ищем запись для изменения
                    if not message:
                        try:
                            uzanto_tasko = TaskojUzantoTasko.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_uzantojn_taskojn')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_uzantojn_taskojn'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                update_fields = [
                                    'tipo', 'speco', 'etapo', 'tasko', 'forigo', 'foriga_dato', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato'
                                ]
                                uzanto_tasko.tipo = tipo or uzanto_tasko.tipo
                                uzanto_tasko.speco = speco or uzanto_tasko.speco
                                uzanto_tasko.etapo = etapo or uzanto_tasko.etapo
                                uzanto_tasko.tasko = tasko or uzanto_tasko.tasko
                                uzanto_tasko.forigo = kwargs.get('forigo', uzanto_tasko.forigo)
                                uzanto_tasko.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                uzanto_tasko.arkivo = kwargs.get('arkivo', uzanto_tasko.arkivo)
                                uzanto_tasko.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                uzanto_tasko.publikigo = kwargs.get('publikigo', uzanto_tasko.publikigo)
                                uzanto_tasko.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('priskribo', False):
                                    set_enhavo(uzanto_tasko.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('priskribo')

                                uzanto_tasko.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojUzantoTasko.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojUzantoTasko(status=status, message=message, uzanto_tasko=uzanto_tasko)

# Функционал задач соообществ
# Проекты сообществ
class RedaktuTaskojKomunumoProjekto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    komunumo_projecto = graphene.Field(TaskojKomunumoProjektoNode, description=_('Создание/изменение проектов пользователей'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_kodo = graphene.String(description=_('Код типа проекта'))
        speco_kodo = graphene.String(description=_('Код вида проекта'))
        posedanto_id = graphene.Int(description=_('Код владельца (сообщество)'))
        priskribo = graphene.String(description=_('Описание проекта пользователя'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        komunumo_projecto = None
        tipo = None
        speco = None
        posedanto = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_uzantojn_komunumoj_projektojn'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            if not (kwargs.get('tipo_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'tipo_kodo')
                            else:
                                try:
                                    tipo = TaskojProjektoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojProjektoTipo.DoesNotExist:
                                    message = _('Неверный тип проекта')

                        if not message:
                            if not (kwargs.get('speco_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'speco_kodo')
                            else:
                                try:
                                    speco = TaskojProjektoSpeco.objects.get(kodo=kwargs.get('speco_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojProjektoSpeco.DoesNotExist:
                                    message = _('Неверный вид проекта')

                        if not message:
                            if not (kwargs.get('posedanto_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'posedanto_id')
                            else:
                                try:
                                    posedanto = Komunumo.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Komunumo.DoesNotExist:
                                    message = _('Неверный владелец (сообщество)')

                        if not (kwargs.get('priskribo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                        if not message:
                            komunumo_projecto = TaskojKomunumoProjekto.objects.create(
                                tipo=tipo,
                                autoro=uzanto,
                                speco=speco,
                                posedanto=posedanto,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            update_fields = ['priskribo',]
                            set_enhavo(komunumo_projecto.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                            komunumo_projecto.save(update_fields=update_fields)

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('tipo_kodo', False) or kwargs.get('speco_kodo', False) 
                            or kwargs.get('priskribo', False) or kwargs.get('publikigo', False)
                            or kwargs.get('posedanto_id', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_kodo' in kwargs and not message:
                        try:
                            tipo = TaskojProjektoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojProjektoTipo.DoesNotExist:
                            message = _('Неверный тип проекта')

                    if 'speco_kodo' in kwargs and not message:
                        try:
                            speco = TaskojProjektoSpeco.objects.get(kodo=kwargs.get('speco_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojProjektoSpeco.DoesNotExist:
                            message = _('Неверный вид проекта')

                    if 'posedanto_id' in kwargs and not message:
                        try:
                            posedanto = Komunumo.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except Komunumo.DoesNotExist:
                            message = _('Неверный владелец (сообщество)')

                    # Ищем запись для изменения
                    if not message:
                        try:
                            komunumo_projecto = TaskojKomunumoProjekto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_komunumoj_projektojn')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_komunumoj_projektojn'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                update_fields = [
                                    'tipo', 'speco', 'posedanto', 'forigo', 'foriga_dato', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato'
                                ]
                                komunumo_projecto.tipo = tipo or komunumo_projecto.tipo
                                komunumo_projecto.speco = speco or komunumo_projecto.speco
                                komunumo_projecto.posedanto = posedanto or komunumo_projecto.posedanto
                                komunumo_projecto.forigo = kwargs.get('forigo', komunumo_projecto.forigo)
                                komunumo_projecto.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                komunumo_projecto.arkivo = kwargs.get('arkivo', komunumo_projecto.arkivo)
                                komunumo_projecto.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                komunumo_projecto.publikigo = kwargs.get('publikigo', komunumo_projecto.publikigo)
                                komunumo_projecto.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                komunumo_projecto.lasta_autoro = uzanto
                                komunumo_projecto.lasta_dato = timezone.now()

                                if kwargs.get('priskribo', False):
                                    set_enhavo(komunumo_projecto.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('priskribo')

                                komunumo_projecto.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojKomunumoProjekto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojKomunumoProjekto(status=status, message=message, komunumo_projecto=komunumo_projecto)

# Направления задач сообществ
class RedaktuTaskojKomunumoDirekto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    komunumo_direkto = graphene.Field(TaskojKomunumoDirektoNode, description=_('Создание/изменение направления задач пользователей'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_kodo = graphene.String(description=_('Код типа проекта'))
        speco_kodo = graphene.String(description=_('Код вида проекта'))
        projekto_id = graphene.Int(description=_('Код проекта'))
        posedanto_id = graphene.Int(description=_('Код проекта'))
        priskribo = graphene.String(description=_('Описание проекта пользователя'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        komunumo_direkto = None
        tipo = None
        speco = None
        projekto = None
        posedanto = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_uzantojn_komunumoj_direktojn'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            if not (kwargs.get('tipo_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'tipo_kodo')
                            else:
                                try:
                                    tipo = TaskojDirektoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojDirektoTipo.DoesNotExist:
                                    message = _('Неверный тип направления')

                        if not message:
                            if not (kwargs.get('speco_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'speco_kodo')
                            else:
                                try:
                                    speco = TaskojDirektoSpeco.objects.get(kodo=kwargs.get('speco_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojDirektoSpeco.DoesNotExist:
                                    message = _('Неверный вид направления')

                        if not message:
                            if not (kwargs.get('projekto_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'projekto_id')
                            else:
                                try:
                                    projekto = TaskojKomunumoProjekto.objects.get(id=kwargs.get('projekto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojKomunumoProjekto.DoesNotExist:
                                    message = _('Неверный проект')

                        if not message:
                            if not (kwargs.get('posedanto_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'posedanto_id')
                            else:
                                try:
                                    posedanto = Komunumo.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Komunumo.DoesNotExist:
                                    message = _('Неверный владелец (сообщество)')

                        if not (kwargs.get('priskribo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                        if not message:
                            komunumo_direkto = TaskojKomunumoDirekto.objects.create(
                                tipo=tipo,
                                autoro=uzanto,
                                speco=speco,
                                projekto=projekto,
                                posedanto=posedanto,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            update_fields = ['priskribo',]
                            set_enhavo(komunumo_direkto.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                            komunumo_direkto.save(update_fields=update_fields)

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('tipo_kodo', False) or kwargs.get('speco_kodo', False) 
                            or kwargs.get('priskribo', False) or kwargs.get('publikigo', False)
                            or kwargs.get('projekto_id', False) or kwargs.get('posedanto_id', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_kodo' in kwargs and not message:
                        try:
                            tipo = TaskojDirektoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojDirektoTipo.DoesNotExist:
                            message = _('Неверный тип направления')

                    if 'speco_kodo' in kwargs and not message:
                        try:
                            speco = TaskojDirektoSpeco.objects.get(kodo=kwargs.get('speco_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojDirektoSpeco.DoesNotExist:
                            message = _('Неверный вид направления')

                    if 'projekto_id' in kwargs and not message:
                        try:
                            projekto = TaskojKomunumoProjekto.objects.get(id=kwargs.get('projekto_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojKomunumoProjekto.DoesNotExist:
                            message = _('Неверный проект')

                    if 'posedanto_id' in kwargs and not message:
                        try:
                            posedanto = Komunumo.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except Komunumo.DoesNotExist:
                            message = _('Неверный владелец (сообщество)')

                    # Ищем запись для изменения
                    if not message:
                        try:
                            komunumo_direkto = TaskojKomunumoDirekto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_komunumoj_direktojn')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_komunumoj_direktojn'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                update_fields = [
                                    'tipo', 'speco', 'posedanto', 'projekto', 'forigo', 'foriga_dato', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato'
                                ]
                                komunumo_direkto.tipo = tipo or komunumo_direkto.tipo
                                komunumo_direkto.speco = speco or komunumo_direkto.speco
                                komunumo_direkto.projekto = projekto or komunumo_direkto.projekto
                                komunumo_direkto.posedanto = posedanto or komunumo_direkto.posedanto
                                komunumo_direkto.forigo = kwargs.get('forigo', komunumo_direkto.forigo)
                                komunumo_direkto.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                komunumo_direkto.arkivo = kwargs.get('arkivo', komunumo_direkto.arkivo)
                                komunumo_direkto.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                komunumo_direkto.publikigo = kwargs.get('publikigo', komunumo_direkto.publikigo)
                                komunumo_direkto.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                komunumo_direkto.lasta_autoro = uzanto
                                komunumo_direkto.lasta_dato = timezone.now()

                                if kwargs.get('priskribo', False):
                                    set_enhavo(komunumo_direkto.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('priskribo')

                                komunumo_direkto.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojKomunumoDirekto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojKomunumoDirekto(status=status, message=message, komunumo_direkto=komunumo_direkto)

# Этапы задач сообществ
class RedaktuTaskojKomunumoEtapo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    komunumo_etapo = graphene.Field(TaskojKomunumoEtapoNode, description=_('Создание/изменение направления задач пользователей'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_kodo = graphene.String(description=_('Код типа проекта'))
        speco_kodo = graphene.String(description=_('Код вида проекта'))
        direkto_id = graphene.Int(description=_('Код проекта'))
        posedanto_id = graphene.Int(description=_('Код владельца (сообщество)'))
        priskribo = graphene.String(description=_('Описание проекта пользователя'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        komunumo_etapo = None
        tipo = None
        speco = None
        direkto = None
        posedanto = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_uzantojn_komunumoj_etapojn'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            if not (kwargs.get('tipo_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'tipo_kodo')
                            else:
                                try:
                                    tipo = TaskojEtapoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojEtapoTipo.DoesNotExist:
                                    message = _('Неверный тип этапа')

                        if not message:
                            if not (kwargs.get('speco_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'speco_kodo')
                            else:
                                try:
                                    speco = TaskojEtapoSpeco.objects.get(kodo=kwargs.get('speco_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojEtapoSpeco.DoesNotExist:
                                    message = _('Неверный вид этапа')

                        if not message:
                            if not (kwargs.get('direkto_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'direkto_id')
                            else:
                                try:
                                    direkto = TaskojKomunumoDirekto.objects.get(id=kwargs.get('direkto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojKomunumoDirekto.DoesNotExist:
                                    message = _('Неверное направление')

                        if not message:
                            if not (kwargs.get('posedanto_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'posedanto_id')
                            else:
                                try:
                                    posedanto = Komunumo.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Komunumo.DoesNotExist:
                                    message = _('Неверный владелец (сообщество)')

                        if not (kwargs.get('priskribo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                        if not message:
                            komunumo_etapo = TaskojKomunumoEtapo.objects.create(
                                tipo=tipo,
                                posedanto=posedanto,
                                autoro=uzanto,
                                speco=speco,
                                direkto=direkto,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            update_fields = ['priskribo',]
                            set_enhavo(komunumo_etapo.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                            komunumo_etapo.save(update_fields=update_fields)

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('tipo_kodo', False) or kwargs.get('speco_kodo', False) 
                            or kwargs.get('priskribo', False) or kwargs.get('publikigo', False)
                            or kwargs.get('direkto_id', False) or kwargs.get('posedanto_id', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_kodo' in kwargs and not message:
                        try:
                            tipo = TaskojEtapoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojEtapoTipo.DoesNotExist:
                            message = _('Неверный тип этапа')

                    if 'speco_kodo' in kwargs and not message:
                        try:
                            speco = TaskojEtapoSpeco.objects.get(kodo=kwargs.get('speco_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojEtapoSpeco.DoesNotExist:
                            message = _('Неверный вид этапа')

                    if 'direkto_id' in kwargs and not message:
                        try:
                            direkto = TaskojKomunumoDirekto.objects.get(id=kwargs.get('direkto_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojKomunumoDirekto.DoesNotExist:
                            message = _('Неверное направление')

                    if 'posedanto_id' in kwargs and not message:
                        try:
                            posedanto = Komunumo.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except Komunumo.DoesNotExist:
                            message = _('Неверный владелец (сообщество)')

                    # Ищем запись для изменения
                    if not message:
                        try:
                            komunumo_etapo = TaskojKomunumoEtapo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_komunumoj_etapojn')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_komunumoj_etapojn'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                update_fields = [
                                    'tipo', 'speco', 'posedanto', 'direkto', 'forigo', 'foriga_dato', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato'
                                ]
                                komunumo_etapo.tipo = tipo or komunumo_etapo.tipo
                                komunumo_etapo.speco = speco or komunumo_etapo.speco
                                komunumo_etapo.direkto = direkto or komunumo_etapo.direkto
                                komunumo_etapo.posedanto = posedanto or komunumo_etapo.posedanto
                                komunumo_etapo.forigo = kwargs.get('forigo', komunumo_etapo.forigo)
                                komunumo_etapo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                komunumo_etapo.arkivo = kwargs.get('arkivo', komunumo_etapo.arkivo)
                                komunumo_etapo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                komunumo_etapo.publikigo = kwargs.get('publikigo', komunumo_etapo.publikigo)
                                komunumo_etapo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                komunumo_etapo.lasta_autoro = uzanto
                                komunumo_etapo.lasta_dato = timezone.now()

                                if kwargs.get('priskribo', False):
                                    set_enhavo(komunumo_etapo.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('priskribo')

                                komunumo_etapo.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojKomunumoEtapo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojKomunumoEtapo(status=status, message=message, komunumo_etapo=komunumo_etapo)

# Задачи сообществ
class RedaktuTaskojKomunumoTasko(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    komunumo_tasko = graphene.Field(TaskojKomunumoTaskoNode, description=_('Создание/изменение направления задач пользователей'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_kodo = graphene.String(description=_('Код типа проекта'))
        speco_kodo = graphene.String(description=_('Код вида проекта'))
        etapo_id = graphene.Int(description=_('Код проекта'))
        posedanto_id = graphene.Int(description=_('Код владельца (сообщества)'))
        tasko_id = graphene.Int(description=_('Код главной задачи, если это подзадача'))
        priskribo = graphene.String(description=_('Описание проекта пользователя'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        komunumo_tasko = None
        tipo = None
        speco = None
        etapo = None
        tasko = None
        posedanto = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_uzantojn_komunumoj_taskojn'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            if not (kwargs.get('tipo_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'tipo_kodo')
                            else:
                                try:
                                    tipo = TaskojTaskoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojTaskoTipo.DoesNotExist:
                                    message = _('Неверный тип задачи')

                        if not message:
                            if not (kwargs.get('speco_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'speco_kodo')
                            else:
                                try:
                                    speco = TaskojTaskoSpeco.objects.get(kodo=kwargs.get('speco_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojTaskoSpeco.DoesNotExist:
                                    message = _('Неверный вид задачи')

                        if not message:
                            if not (kwargs.get('etapo_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'etapo_id')
                            else:
                                try:
                                    etapo = TaskojKomunumoEtapo.objects.get(id=kwargs.get('etapo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojKomunumoEtapo.DoesNotExist:
                                    message = _('Неверный этап')

                        if not message:
                            if not (kwargs.get('posedanto_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'posedanto_id')
                            else:
                                try:
                                    posedanto = Komunumo.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Komunumo.DoesNotExist:
                                    message = _('Неверный владелец (организация)')

                        if not message:
                            if (kwargs.get('tasko_id', False)):
                                try:
                                    tasko = TaskojKomunumoTasko.objects.get(id=kwargs.get('tasko_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojKomunumoTasko.DoesNotExist:
                                    message = _('Неверная главная задача')

                        if not (kwargs.get('priskribo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                        if not message:
                            komunumo_tasko = TaskojKomunumoTasko.objects.create(
                                tipo=tipo,
                                posedanto=posedanto,
                                autoro=uzanto,
                                speco=speco,
                                etapo=etapo,
                                tasko=tasko,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            update_fields = ['priskribo',]
                            set_enhavo(komunumo_tasko.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                            komunumo_tasko.save(update_fields=update_fields)

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('tipo_kodo', False) or kwargs.get('speco_kodo', False) 
                            or kwargs.get('priskribo', False) or kwargs.get('publikigo', False)
                            or kwargs.get('etapo_id', False) or kwargs.get('tasko_id', False)
                            or kwargs.get('posedanto_id', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_kodo' in kwargs and not message:
                        try:
                            tipo = TaskojTaskoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojTaskoTipo.DoesNotExist:
                            message = _('Неверный тип задачи')

                    if 'speco_kodo' in kwargs and not message:
                        try:
                            speco = TaskojTaskoSpeco.objects.get(kodo=kwargs.get('speco_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojTaskoSpeco.DoesNotExist:
                            message = _('Неверный вид задачи')

                    if 'etapo_id' in kwargs and not message:
                        try:
                            etapo = TaskojKomunumoEtapo.objects.get(id=kwargs.get('etapo_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojKomunumoEtapo.DoesNotExist:
                            message = _('Неверный этап')

                    if 'posedanto_id' in kwargs and not message:
                        try:
                            posedanto = Komunumo.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except Komunumo.DoesNotExist:
                            message = _('Неверная владелец (организация)')

                    if 'tasko_id' in kwargs and not message:
                        try:
                            tasko = TaskojKomunumoTasko.objects.get(id=kwargs.get('tasko_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojKomunumoTasko.DoesNotExist:
                            message = _('Неверная главная задача')

                    # Ищем запись для изменения
                    if not message:
                        try:
                            komunumo_tasko = TaskojKomunumoTasko.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_komunumoj_taskojn')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_komunumoj_taskojn'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                update_fields = [
                                    'tipo', 'speco', 'posedanto', 'etapo', 'tasko', 'forigo', 'foriga_dato', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato'
                                ]
                                komunumo_tasko.tipo = tipo or komunumo_tasko.tipo
                                komunumo_tasko.speco = speco or komunumo_tasko.speco
                                komunumo_tasko.etapo = etapo or komunumo_tasko.etapo
                                komunumo_tasko.posedanto = posedanto or komunumo_tasko.posedanto
                                komunumo_tasko.tasko = tasko or komunumo_tasko.tasko
                                komunumo_tasko.forigo = kwargs.get('forigo', komunumo_tasko.forigo)
                                komunumo_tasko.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                komunumo_tasko.arkivo = kwargs.get('arkivo', komunumo_tasko.arkivo)
                                komunumo_tasko.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                komunumo_tasko.publikigo = kwargs.get('publikigo', komunumo_tasko.publikigo)
                                komunumo_tasko.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                komunumo_tasko.lasta_autoro = uzanto
                                komunumo_tasko.lasta_dato = timezone.now()

                                if kwargs.get('priskribo', False):
                                    set_enhavo(komunumo_tasko.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('priskribo')

                                komunumo_tasko.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojKomunumoTasko.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojKomunumoTasko(status=status, message=message, komunumo_tasko=komunumo_tasko)



class TaskojMutations(graphene.ObjectType):
  redaktu_taskoj_tasko_realigo_tipo = RedaktuTaskojTaskoRealigoTipo.Field(
    description=_('''Создаёт или редактирует типы реализаций задач''')
  )
  redaktu_taskoj_projekto_tipo = RedaktuTaskojProjektoTipo.Field(
    description=_('''Создаёт или редактирует типы проектов''')
  )
  redaktu_taskoj_projekto_speco = RedaktuTaskojProjektoSpeco.Field(
    description=_('''Создаёт или редактирует виды проектов''')
  )
  redaktu_taskoj_direkto_tipo = RedaktuTaskojDirektoTipo.Field(
    description=_('''Создаёт или редактирует типы направлений задач''')
  )
  redaktu_taskoj_direkto_speco = RedaktuTaskojDirektoSpeco.Field(
    description=_('''Создаёт или редактирует виды направлений задач''')
  )
  redaktu_taskoj_etapo_tipo = RedaktuTaskojEtapoTipo.Field(
    description=_('''Создаёт или редактирует типы этапов задач''')
  )
  redaktu_taskoj_etapo_speco = RedaktuTaskojEtapoSpeco.Field(
    description=_('''Создаёт или редактирует виды этапов задач''')
  )
  redaktu_taskoj_tasko_tipo = RedaktuTaskojTaskoTipo.Field(
    description=_('''Создаёт или редактирует типы задач''')
  )
  redaktu_taskoj_tasko_speco = RedaktuTaskojTaskoSpeco.Field(
    description=_('''Создаёт или редактирует виды задач''')
  )
  redaktu_taskoj_uzanto_projekto = RedaktuTaskojUzantoProjekto.Field(
    description=_('''Создаёт или редактирует проекты пользователей''')
  )
  redaktu_taskoj_uzanto_direkto = RedaktuTaskojUzantoDirekto.Field(
    description=_('''Создаёт или редактирует направления задач пользователей''')
  )
  redaktu_taskoj_uzanto_etapo = RedaktuTaskojUzantoEtapo.Field(
    description=_('''Создаёт или редактирует этапы задач пользователей''')
  )
  redaktu_taskoj_uzanto_tasko = RedaktuTaskojUzantoTasko.Field(
    description=_('''Создаёт или редактирует задачи пользователей''')
  )
  redaktu_taskoj_komunumo_projekto = RedaktuTaskojKomunumoProjekto.Field(
    description=_('''Создаёт или редактирует проекты сообществ''')
  )
  redaktu_taskoj_komunumo_direkto = RedaktuTaskojKomunumoDirekto.Field(
    description=_('''Создаёт или редактирует направления задач сообществ''')
  )
  redaktu_taskoj_komunumo_etapo = RedaktuTaskojKomunumoEtapo.Field(
    description=_('''Создаёт или редактирует этапы задач сообществ''')
  )
  redaktu_taskoj_komunumo_tasko = RedaktuTaskojKomunumoTasko.Field(
    description=_('''Создаёт или редактирует задачи сообществ''')
  )

