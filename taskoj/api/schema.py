"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

import graphene

from graphene_django import DjangoObjectType
from django.db.models import F, Q
from siriuso.api.types import SiriusoLingvo
from siriuso.api.filters import SiriusoFilterConnectionField
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions
from siriuso.utils import default_lingvo, get_enhavo
from graphene_permissions.permissions import AllowAny # Класс доступа к типу Graphene
from django.utils.translation import ugettext_lazy as _  # Функция перевода внутренних сообщений Django

from komunumoj.models import Komunumo, KomunumoMembro
from informiloj.api.schema import InformilojLabororoloNode
from taskoj.models import *

from versioj.models import VersioTaskojUzantoProjekto, VersioTaskojUzantoDirekto, VersioTaskojUzantoEtapo, \
    VersioTaskojUzantoTasko, VersioTaskojKomunumoProjekto, VersioTaskojKomunumoDirekto, VersioTaskojKomunumoEtapo, \
    VersioTaskojKomunumoTasko
from versioj.api.schema import VersioTaskojUzantoProjektoNode, VersioTaskojUzantoDirektoNode, VersioTaskojUzantoEtapoNode, \
    VersioTaskojUzantoTaskoNode, VersioTaskojKomunumoProjektoNode, VersioTaskojKomunumoDirektoNode, \
    VersioTaskojKomunumoEtapoNode, VersioTaskojKomunumoTaskoNode


# Функционал задач
# Типы реализаций задач
class TaskojTaskoRealigoTipoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('название типа реализации задачи'))

    class Meta:
        model = TaskojTaskoRealigoTipo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)

# Типы проектов (личных / групповых)
class TaskojProjektoTipoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('название типа проекта'))

    class Meta:
        model = TaskojProjektoTipo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)

# Виды проектов (личных / групповых)
class TaskojProjektoSpecoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('название вида проекта'))

    class Meta:
        model = TaskojProjektoSpeco
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)

# Типы направлений задач
class TaskojDirektoTipoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('название типа направления задачи'))

    class Meta:
        model = TaskojDirektoTipo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)

# Виды направлений задач
class TaskojDirektoSpecoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('название вида направления задачи'))

    class Meta:
        model = TaskojDirektoSpeco
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)

# Типы этапов задач
class TaskojEtapoTipoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('название типа этапа задачи'))

    class Meta:
        model = TaskojEtapoTipo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)

# Виды этапов задач
class TaskojEtapoSpecoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('название вида этапа задачи'))

    class Meta:
        model = TaskojEtapoSpeco
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)

# Типы задач
class TaskojTaskoTipoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('название типа задачи'))

    class Meta:
        model = TaskojTaskoTipo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)

# Виды задач
class TaskojTaskoSpecoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('название вида задачи'))

    class Meta:
        model = TaskojTaskoSpeco
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)

# Функционал задач пользователей
# Задачи пользователей
class TaskojUzantoTaskoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    priskribo = graphene.Field(SiriusoLingvo, description=_('описание в таблице задач пользователей'))
    versioj = SiriusoFilterConnectionField(VersioTaskojUzantoTaskoNode, description=_('Версии страницы'))

    class Meta:
        model = TaskojUzantoTasko
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'tipo__uuid': ['exact'],
            'speco__uuid': ['exact'],
            'posedanto__id': ['exact'],
            'posedanto__uuid': ['exact'],
            'etapo__id': ['exact'],
            'etapo__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_versioj(self, info, **kwargs):
        user = info.context.user
        model = VersioTaskojUzantoTasko

        perm_name = 'versioj.povas_vidi_taskoj_uzantojn_taskojn_version'
        lingvo = lingvo_kodo_normaligo(get_lang_kodo(info.context))

        if user.has_perm(perm_name, self):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        if ((user.is_authenticated and perms.has_registrita_perm(perm_name))
                or (not user.is_authenticated and perms.has_neregistrita_perm(perm_name))):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        return model.objects.none()

# Этапы задач пользователей
class TaskojUzantoEtapoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    priskribo = graphene.Field(SiriusoLingvo, description=_('описание в таблице этапы задач пользователей'))
    versioj = SiriusoFilterConnectionField(VersioTaskojUzantoEtapoNode, description=_('Версии страницы'))

    taskoj = SiriusoFilterConnectionField(TaskojUzantoTaskoNode, description=_('Выводит список этапов задач пользователя'))

    tuta_taskoj = graphene.Int(description=_('Общее количество этапов задач пользователя'))

    class Meta:
        model = TaskojUzantoEtapo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'tipo__uuid': ['exact'],
            'speco__uuid': ['exact'],
            'posedanto__id': ['exact'],
            'posedanto__uuid': ['exact'],
            'direkto__id': ['exact'],
            'direkto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_taskoj(self, info, **kwargs):
        return TaskojUzantoTasko.objects.filter(etapo=self)

    def resolve_tuta_taskoj(self, info, **kwargs):
        return TaskojUzantoTasko.objects.filter(etapo=self, forigo=False, arkivo=False, publikigo=True).count()

    def resolve_versioj(self, info, **kwargs):
        user = info.context.user
        model = VersioTaskojUzantoEtapo

        perm_name = 'versioj.povas_vidi_taskoj_uzantojn_etapojn_version'
        lingvo = lingvo_kodo_normaligo(get_lang_kodo(info.context))

        if user.has_perm(perm_name, self):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        if ((user.is_authenticated and perms.has_registrita_perm(perm_name))
                or (not user.is_authenticated and perms.has_neregistrita_perm(perm_name))):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        return model.objects.none()

# Направления задач пользователей
class TaskojUzantoDirektoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    priskribo = graphene.Field(SiriusoLingvo, description=_('описание в таблице направления задач пользователей'))
    versioj = SiriusoFilterConnectionField(VersioTaskojUzantoDirektoNode, description=_('Версии страницы'))

    etapoj = SiriusoFilterConnectionField(TaskojUzantoEtapoNode, description=_('Выводит список этапов задач пользователя'))

    tuta_etapoj = graphene.Int(description=_('Общее количество этапов задач пользователя'))

    class Meta:
        model = TaskojUzantoDirekto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'tipo__uuid': ['exact'],
            'speco__uuid': ['exact'],
            'posedanto__id': ['exact'],
            'posedanto__uuid': ['exact'],
            'projekto__id': ['exact'],
            'projekto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_etapoj(self, info, **kwargs):
        return TaskojUzantoEtapo.objects.filter(direkto=self)

    def resolve_tuta_etapoj(self, info, **kwargs):
        return TaskojUzantoEtapo.objects.filter(direkto=self, forigo=False, arkivo=False, publikigo=True).count()

    def resolve_versioj(self, info, **kwargs):
        user = info.context.user
        model = VersioTaskojUzantoDirekto

        perm_name = 'versioj.povas_vidi_taskoj_uzantojn_direktojn_version'
        lingvo = lingvo_kodo_normaligo(get_lang_kodo(info.context))

        if user.has_perm(perm_name, self):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        if ((user.is_authenticated and perms.has_registrita_perm(perm_name))
                or (not user.is_authenticated and perms.has_neregistrita_perm(perm_name))):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        return model.objects.none()

# Проекты пользователей
class TaskojUzantoProjektoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    priskribo = graphene.Field(SiriusoLingvo, description=_('описание в таблице проекты пользователей'))
    versioj = SiriusoFilterConnectionField(VersioTaskojUzantoProjektoNode, description=_('Версии страницы'))

    direktoj = SiriusoFilterConnectionField(TaskojUzantoDirektoNode, description=_('Выводит список направлений задач пользователя'))

    tuta_direktoj = graphene.Int(description=_('Общее количество направлений задач пользователя'))

    class Meta:
        model = TaskojUzantoProjekto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'tipo__uuid': ['exact'],
            'speco__uuid': ['exact'],
            'posedanto__id': ['exact'],
            'posedanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_direktoj(self, info, **kwargs):
        return TaskojUzantoDirekto.objects.filter(projekto=self)

    def resolve_tuta_direktoj(self, info, **kwargs):
        return TaskojUzantoDirekto.objects.filter(projekto=self, forigo=False, arkivo=False, publikigo=True).count()

    def resolve_versioj(self, info, **kwargs):
        user = info.context.user
        model = VersioTaskojUzantoProjekto

        perm_name = 'versioj.povas_vidi_taskoj_uzantojn_projektojn_version'
        lingvo = lingvo_kodo_normaligo(get_lang_kodo(info.context))

        if user.has_perm(perm_name, self):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        if ((user.is_authenticated and perms.has_registrita_perm(perm_name))
                or (not user.is_authenticated and perms.has_neregistrita_perm(perm_name))):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        return model.objects.none()


# Функционал задач соообществ
# Задачи сообществ
class TaskojKomunumoTaskoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    priskribo = graphene.Field(SiriusoLingvo, description=_('описание в таблице задач сообществ'))
    versioj = SiriusoFilterConnectionField(VersioTaskojKomunumoTaskoNode, description=_('Версии страницы'))

    class Meta:
        model = TaskojKomunumoTasko
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'tipo__uuid': ['exact'],
            'speco__uuid': ['exact'],
            'posedanto__id': ['exact'],
            'posedanto__uuid': ['exact'],
            'etapo__id': ['exact'],
            'etapo__uuid': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_versioj(self, info, **kwargs):
        user = info.context.user
        model = VersioTaskojKomunumoTasko

        perm_name = 'versioj.povas_vidi_taskoj_komunumojn_taskojn_version'
        lingvo = lingvo_kodo_normaligo(get_lang_kodo(info.context))

        if user.has_perm(perm_name, self):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        if ((user.is_authenticated and perms.has_registrita_perm(perm_name))
                or (not user.is_authenticated and perms.has_neregistrita_perm(perm_name))):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        return model.objects.none()

# Этапы задач сообществ
class TaskojKomunumoEtapoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    priskribo = graphene.Field(SiriusoLingvo, description=_('описание в таблице этапы задач сообществ'))
    versioj = SiriusoFilterConnectionField(VersioTaskojKomunumoEtapoNode, description=_('Версии страницы'))

    taskoj = SiriusoFilterConnectionField(TaskojKomunumoTaskoNode, description=_('Выводит список этапов задач пользователя'))

    tuta_taskoj = graphene.Int(description=_('Общее количество этапов задач пользователя'))

    class Meta:
        model = TaskojKomunumoEtapo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'tipo__uuid': ['exact'],
            'speco__uuid': ['exact'],
            'posedanto__id': ['exact'],
            'posedanto__uuid': ['exact'],
            'direkto__id': ['exact'],
            'direkto__uuid': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_taskoj(self, info, **kwargs):
        return TaskojKomunumoTasko.objects.filter(etapo=self)

    def resolve_tuta_taskoj(self, info, **kwargs):
        return TaskojKomunumoTasko.objects.filter(etapo=self, forigo=False, arkivo=False, publikigo=True).count()

    def resolve_versioj(self, info, **kwargs):
        user = info.context.user
        model = VersioTaskojKomunumoEtapo

        perm_name = 'versioj.povas_vidi_taskoj_komunumojn_etapojn_version'
        lingvo = lingvo_kodo_normaligo(get_lang_kodo(info.context))

        if user.has_perm(perm_name, self):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        if ((user.is_authenticated and perms.has_registrita_perm(perm_name))
                or (not user.is_authenticated and perms.has_neregistrita_perm(perm_name))):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        return model.objects.none()

# Направления задач сообществ
class TaskojKomunumoDirektoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    priskribo = graphene.Field(SiriusoLingvo, description=_('описание в таблице направления задач сообществ'))
    versioj = SiriusoFilterConnectionField(VersioTaskojKomunumoDirektoNode, description=_('Версии страницы'))

    etapoj = SiriusoFilterConnectionField(TaskojKomunumoEtapoNode, description=_('Выводит список этапов задач пользователя'))

    tuta_etapoj = graphene.Int(description=_('Общее количество этапов задач пользователя'))

    class Meta:
        model = TaskojKomunumoDirekto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'tipo__uuid': ['exact'],
            'speco__uuid': ['exact'],
            'posedanto__id': ['exact'],
            'posedanto__uuid': ['exact'],
            'projekto__id': ['exact'],
            'projekto__uuid': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_etapoj(self, info, **kwargs):
        return TaskojKomunumoEtapo.objects.filter(direkto=self)

    def resolve_tuta_etapoj(self, info, **kwargs):
        return TaskojKomunumoEtapo.objects.filter(direkto=self, forigo=False, arkivo=False, publikigo=True).count()

    def resolve_versioj(self, info, **kwargs):
        user = info.context.user
        model = VersioTaskojKomunumoDirekto

        perm_name = 'versioj.povas_vidi_taskoj_komunumojn_direktojn_version'
        lingvo = lingvo_kodo_normaligo(get_lang_kodo(info.context))

        if user.has_perm(perm_name, self):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        if ((user.is_authenticated and perms.has_registrita_perm(perm_name))
                or (not user.is_authenticated and perms.has_neregistrita_perm(perm_name))):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        return model.objects.none()

# Проекты сообществ
class TaskojKomunumoProjektoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    priskribo = graphene.Field(SiriusoLingvo, description=_('описание в таблице проекты сообществ'))
    versioj = SiriusoFilterConnectionField(VersioTaskojKomunumoProjektoNode, description=_('Версии страницы'))

    direktoj = SiriusoFilterConnectionField(TaskojKomunumoDirektoNode, description=_('Выводит список направлений задач пользователя'))

    tuta_direktoj = graphene.Int(description=_('Общее количество направлений задач пользователя'))

    class Meta:
        model = TaskojKomunumoProjekto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'tipo__uuid': ['exact'],
            'speco__uuid': ['exact'],
            'posedanto__id': ['exact'],
            'posedanto__uuid': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_direktoj(self, info, **kwargs):
        return TaskojKomunumoDirekto.objects.filter(projekto=self)

    def resolve_tuta_direktoj(self, info, **kwargs):
        return TaskojKomunumoDirekto.objects.filter(projekto=self, forigo=False, arkivo=False, publikigo=True).count()

    def resolve_versioj(self, info, **kwargs):
        user = info.context.user
        model = VersioTaskojKomunumoProjekto

        perm_name = 'versioj.povas_vidi_taskoj_komunumojn_projektojn_version'
        lingvo = lingvo_kodo_normaligo(get_lang_kodo(info.context))

        if user.has_perm(perm_name, self):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        if ((user.is_authenticated and perms.has_registrita_perm(perm_name))
                or (not user.is_authenticated and perms.has_neregistrita_perm(perm_name))):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        return model.objects.none()





class TaskojXeneralaTaskoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    priskribo = graphene.Field(SiriusoLingvo)
    autoro_labora_rolo = graphene.Field(InformilojLabororoloNode)

    class Meta:
        model = TaskojXeneralaTasko
        only_fields = ['id', 'uuid', 'autoro', 'priskribo']
        filter_fields = {
            'id': ['exact'],
            'uuid': ['exact'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_autoro_labora_rolo(self, info):
        try:
            membro = KomunumoMembro.objects.select_related('laborarolo').get(
                autoro=getattr(self, 'autoro'),
                posedanto__tipo__kodo='gvardio',
                forigo=False
            )
            return membro.laborarolo
        except (KomunumoMembro.DoesNotExist, KomunumoMembro.MultipleObjectsReturned):
            pass

        return None


class Query(graphene.ObjectType):
    xeneralaj_taskoj = SiriusoFilterConnectionField(TaskojXeneralaTaskoNode)
    taskoj_taskoj_realigo_tipo = SiriusoFilterConnectionField(TaskojTaskoRealigoTipoNode,
            description=_('Выводит все доступные типы реализаций задач'))
    taskoj_projektoj_tipo = SiriusoFilterConnectionField(TaskojProjektoTipoNode,
            description=_('Выводит все доступные типы проектов'))
    taskoj_projektoj_speco = SiriusoFilterConnectionField(TaskojProjektoSpecoNode,
            description=_('Выводит все доступные виды проектов'))
    taskoj_direktoj_tipo = SiriusoFilterConnectionField(TaskojDirektoTipoNode,
            description=_('Выводит все доступные типы направлений задач'))
    taskoj_direktoj_speco = SiriusoFilterConnectionField(TaskojDirektoSpecoNode,
            description=_('Выводит все доступные виды направлений задач'))
    taskoj_etapoj_tipo = SiriusoFilterConnectionField(TaskojEtapoTipoNode,
            description=_('Выводит все доступные типы этапов задач'))
    taskoj_etapoj_speco = SiriusoFilterConnectionField(TaskojEtapoSpecoNode,
            description=_('Выводит все доступные виды этапов задач'))
    taskoj_taskoj_tipo = SiriusoFilterConnectionField(TaskojTaskoTipoNode,
            description=_('Выводит все доступные типы задач'))
    taskoj_taskoj_speco = SiriusoFilterConnectionField(TaskojTaskoSpecoNode,
            description=_('Выводит все доступные виды задач'))
    taskoj_uzantoj_projekto = SiriusoFilterConnectionField(TaskojUzantoProjektoNode,
            description=_('Выводит все доступные проекты пользователей'))
    taskoj_uzantoj_direkto = SiriusoFilterConnectionField(TaskojUzantoDirektoNode,
            description=_('Выводит все доступные направления задач пользователей'))
    taskoj_uzantoj_etapo = SiriusoFilterConnectionField(TaskojUzantoEtapoNode,
            description=_('Выводит все доступные этапы задач пользователей'))
    taskoj_uzantoj_tasko = SiriusoFilterConnectionField(TaskojUzantoTaskoNode,
            description=_('Выводит все доступные задачи пользователей'))
    taskoj_komunumoj_projekto = SiriusoFilterConnectionField(TaskojKomunumoProjektoNode,
            description=_('Выводит все доступные проекты сообществ'))
    taskoj_komunumoj_direkto = SiriusoFilterConnectionField(TaskojKomunumoDirektoNode,
            description=_('Выводит все доступные направления задач сообществ'))
    taskoj_komunumoj_etapo = SiriusoFilterConnectionField(TaskojKomunumoEtapoNode,
            description=_('Выводит все доступные этапы задач сообществ'))
    taskoj_komunumoj_tasko = SiriusoFilterConnectionField(TaskojKomunumoTaskoNode,
            description=_('Выводит все доступные задачи сообществ'))
