"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from .models import *


# Функционал задач

# Типы реализаций задач
@admin.register(TaskojTaskoRealigoTipo)
class TaskojTaskoRealigoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = TaskojTaskoRealigoTipo


# Типы проектов (личных / групповых)
@admin.register(TaskojProjektoTipo)
class TaskojProjektoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = TaskojProjektoTipo


# Виды проектов (личных / групповых)
@admin.register(TaskojProjektoSpeco)
class TaskojProjektoSpecoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = TaskojProjektoSpeco


# Типы направлений задач
@admin.register(TaskojDirektoTipo)
class TaskojDirektoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = TaskojDirektoTipo


# Виды направлений задач
@admin.register(TaskojDirektoSpeco)
class TaskojDirektoSpecoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = TaskojDirektoSpeco


# Типы этапов задач
@admin.register(TaskojEtapoTipo)
class TaskojEtapoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = TaskojEtapoTipo


# Виды этапов задач
@admin.register(TaskojEtapoSpeco)
class TaskojEtapoSpecoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = TaskojEtapoSpeco


# Типы задач
@admin.register(TaskojTaskoTipo)
class TaskojTaskoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = TaskojTaskoTipo


# Виды задач
@admin.register(TaskojTaskoSpeco)
class TaskojTaskoSpecoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = TaskojTaskoSpeco


# Функционал задач сообществ

# Проекты сообществ
@admin.register(TaskojKomunumoProjekto)
class TaskojKomunumoProjektoAdmin(admin.ModelAdmin):
    list_display = ('id', 'priskribo','publikigo', 'uuid')
    exclude = ('id', 'priskribo',)

    class Meta:
        model = TaskojKomunumoProjekto


# Направления задач сообществ
@admin.register(TaskojKomunumoDirekto)
class TaskojKomunumoDirektoAdmin(admin.ModelAdmin):
    list_display = ('id', 'priskribo','publikigo', 'uuid')
    exclude = ('id', 'priskribo',)

    class Meta:
        model = TaskojKomunumoDirekto


# Этапы задач сообществ
@admin.register(TaskojKomunumoEtapo)
class TaskojKomunumoEtapoAdmin(admin.ModelAdmin):
    list_display = ('id', 'priskribo','publikigo', 'uuid')
    exclude = ('id', 'priskribo',)

    class Meta:
        model = TaskojKomunumoEtapo


# Задачи сообществ
@admin.register(TaskojKomunumoTasko)
class TaskojKomunumoTaskoAdmin(admin.ModelAdmin):
    list_display = ('id', 'priskribo', 'publikigo', 'uuid')
    exclude = ('id', 'priskribo',)

    class Meta:
        model = TaskojKomunumoTasko


# Глобальные задачи

# Направления глобальных задач
@admin.register(TaskojXeneralaDirekto)
class TaskojXeneralaDirektoAdmin(admin.ModelAdmin):
    list_display = ('id', 'priskribo', 'publikigo', 'uuid')
    exclude = ('id', 'priskribo',)

    class Meta:
        model = TaskojXeneralaDirekto


# Этапы глобальных задач
@admin.register(TaskojXeneralaEtapo)
class TaskojXeneralaEtapoAdmin(admin.ModelAdmin):
    list_display = ('id', 'priskribo', 'publikigo', 'uuid')
    exclude = ('id', 'priskribo',)

    class Meta:
        model = TaskojXeneralaEtapo


# Глобальные задачи
@admin.register(TaskojXeneralaTasko)
class TaskojXeneralaTaskoAdmin(admin.ModelAdmin):
    list_display = ('id', 'autoro', 'publikigo', 'uuid')
    exclude = ('id', 'priskribo',)

    class Meta:
        model = TaskojXeneralaTasko

# Реализация глобальных задач
@admin.register(TaskojXeneralaTaskoRealigo)
class TaskojXeneralaTaskoRealigoAdmin(admin.ModelAdmin):
    list_display = ('tasko', 'posedanto', 'forigo', 'uuid')

    class Meta:
        model = TaskojXeneralaTaskoRealigo
