"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

import sys
from django.db import models
from django.db.models import Max, Q
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres import fields as pgfields
from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from main.models import SiriusoBazaAbstrakta3, SiriusoBazaAbstraktaKomunumoj, SiriusoBazaAbstraktaUzanto
from main.models import SiriusoTipoAbstrakta
from main.models import Uzanto
from komunumoj.models import *
from django.contrib.auth.models import Permission


# Функционал задач
# Типы реализаций задач, использует абстрактный класс SiriusoTipoAbstrakta
class TaskojTaskoRealigoTipo(SiriusoTipoAbstrakta):

    # название многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_taskoj_realigoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de realigoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de realigoj')
        # права
        permissions = (
            ('povas_vidi_taskojn_realigojn_tipon', _('Povas vidi taskojn realigojn tipon')),
            ('povas_krei_taskojn_realigojn_tipon', _('Povas krei taskojn realigojn tipon')),
            ('povas_forigi_taskojn_realigojn_tipon', _('Povas forigu taskojn realigojn tipon')),
            ('povas_shanghi_taskojn_realigojn_tipon', _('Povas ŝanĝi taskojn realigojn tipon')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()

# Типы проектов (личных / групповых), использует абстрактный класс SiriusoTipoAbstrakta
class TaskojProjektoTipo(SiriusoTipoAbstrakta):

    # название многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_projektoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de projektoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de projektoj')
        # права
        permissions = (
            ('povas_vidi_projektojn_tipon', _('Povas vidi projektojn tipon')),
            ('povas_krei_projektojn_tipon', _('Povas krei projektojn tipon')),
            ('povas_forigi_projektojn_tipon', _('Povas forigu projektojn tipon')),
            ('povas_shanghi_projektojn_tipon', _('Povas ŝanĝi projektojn tipon')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()

# Виды проектов (личных / групповых), использует абстрактный класс SiriusoTipoAbstrakta
class TaskojProjektoSpeco(SiriusoTipoAbstrakta):

    # название многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_projektoj_specoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Speco de projektoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Specoj de projektoj')
        # права
        permissions = (
            ('povas_vidi_projektojn_specoj', _('Povas vidi projektojn specoj')),
            ('povas_krei_projektojn_specoj', _('Povas krei projektojn specoj')),
            ('povas_forigi_projektojn_specoj', _('Povas forigu projektojn specoj')),
            ('povas_shanghi_projektojn_specoj', _('Povas ŝanĝi projektojn specoj')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()

# Абстрактный класс проектов (личных / групповых), использует абстрактный класс SiriusoBazaAbstrakta3
class TaskojProjektoAbstrakta(SiriusoBazaAbstrakta3):

    # тип направления
    tipo = models.ForeignKey(TaskojProjektoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # вид направлений
    speco = models.ForeignKey(TaskojProjektoSpeco, verbose_name=_('Speco'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # указание что это абстрактный класс
        abstract = True

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле ID этой модели
        return '{}'.format(self.id)

# Типы направлений задач, использует абстрактный класс SiriusoTipoAbstrakta
class TaskojDirektoTipo(SiriusoTipoAbstrakta):

    # название многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_direktoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de direktoj de taskoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de direktoj de taskoj')
        # права
        permissions = (
            ('povas_vidi_direktojn_tipon', _('Povas vidi direktojn tipon')),
            ('povas_krei_direktojn_tipon', _('Povas krei direktojn tipon')),
            ('povas_forigi_direktojn_tipon', _('Povas forigu direktojn tipon')),
            ('povas_shanghi_direktojn_tipon', _('Povas ŝanĝi direktojn tipon')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()

# Виды направлений задач, использует абстрактный класс SiriusoTipoAbstrakta
class TaskojDirektoSpeco(SiriusoTipoAbstrakta):

    # название многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_direktoj_specoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Speco de direktoj de taskoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Specoj de direktoj de taskoj')
        # права
        permissions = (
            ('povas_vidi_direktojn_specoj', _('Povas vidi direktojn specoj')),
            ('povas_krei_direktojn_specoj', _('Povas krei direktojn specoj')),
            ('povas_forigi_direktojn_specoj', _('Povas forigu direktojn specoj')),
            ('povas_shanghi_direktojn_specoj', _('Povas ŝanĝi direktojn specoj')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()

# Абстрактный класс направлений задач, использует абстрактный класс SiriusoBazaAbstrakta3
class TaskojDirektoAbstrakta(SiriusoBazaAbstrakta3):

    # тип направления
    tipo = models.ForeignKey(TaskojDirektoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # вид направлений
    speco = models.ForeignKey(TaskojDirektoSpeco, verbose_name=_('Speco'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # указание что это абстрактный класс
        abstract = True

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле ID этой модели
        return '{}'.format(self.id)

# Типы этапов задач, использует абстрактный класс SiriusoTipoAbstrakta
class TaskojEtapoTipo(SiriusoTipoAbstrakta):

    # название многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_etapoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de etapoj de taskoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de etapoj de taskoj')
        # права
        permissions = (
            ('povas_vidi_etapojn_tipon', _('Povas vidi etapojn tipon')),
            ('povas_krei_etapojn_tipon', _('Povas krei etapojn tipon')),
            ('povas_forigi_etapojn_tipon', _('Povas forigu etapojn tipon')),
            ('povas_shanghi_etapojn_tipon', _('Povas ŝanĝi etapojn tipon')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()

# Виды этапов задач, использует абстрактный класс SiriusoTipoAbstrakta
class TaskojEtapoSpeco(SiriusoTipoAbstrakta):

    # название многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_etapoj_specoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Speco de etapoj de taskoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Specoj de etapoj de taskoj')
        # права
        permissions = (
            ('povas_vidi_etapojn_specoj', _('Povas vidi etapojn specoj')),
            ('povas_krei_etapojn_specoj', _('Povas krei etapojn specoj')),
            ('povas_forigi_etapojn_specoj', _('Povas forigu etapojn specoj')),
            ('povas_shanghi_etapojn_specoj', _('Povas ŝanĝi etapojn specoj')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()


# Абстрактный класс этапов задач, использует абстрактный класс SiriusoBazaAbstrakta3
class TaskojEtapoAbstrakta(SiriusoBazaAbstrakta3):

    # тип этапа
    tipo = models.ForeignKey(TaskojEtapoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # вид этапа
    speco = models.ForeignKey(TaskojEtapoSpeco, verbose_name=_('Speco'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # указание что это абстрактный класс
        abstract = True

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле ID этой модели
        return '{}'.format(self.id)


# Типы задач, использует абстрактный класс SiriusoTipoAbstrakta
class TaskojTaskoTipo(SiriusoTipoAbstrakta):

    # название многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # возможные типы реализации задачи
    tipo_realigo = models.ManyToManyField(TaskojTaskoRealigoTipo, verbose_name=_('Tipo'), blank=False, default=None)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_taskoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de taskoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de taskoj')
        # права
        permissions = (
            ('povas_vidi_taskojn_tipon', _('Povas vidi taskojn tipon')),
            ('povas_krei_taskojn_tipon', _('Povas krei taskojn tipon')),
            ('povas_forigi_taskojn_tipon', _('Povas forigu taskojn tipon')),
            ('povas_shanghi_taskojn_tipon', _('Povas ŝanĝi taskojn tipon')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()

# Виды задач, использует абстрактный класс SiriusoTipoAbstrakta
class TaskojTaskoSpeco(SiriusoTipoAbstrakta):

    # название многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_taskoj_specoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Speco de taskoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Specoj de taskoj')
        # права
        permissions = (
            ('povas_vidi_taskojn_specoj', _('Povas vidi taskojn specoj')),
            ('povas_krei_taskojn_specoj', _('Povas krei taskojn specoj')),
            ('povas_forigi_taskojn_specoj', _('Povas forigu taskojn specoj')),
            ('povas_shanghi_taskojn_specoj', _('Povas ŝanĝi taskojn specoj')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()

# Абстрактный класс задач, использует абстрактный класс SiriusoBazaAbstrakta3
class TaskojTaskoAbstrakta(SiriusoBazaAbstrakta3):

    # тип задачи
    tipo = models.ForeignKey(TaskojTaskoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # вид задачи
    speco = models.ForeignKey(TaskojTaskoSpeco, verbose_name=_('Speco'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # если это подзадача, то какой задачи (рекурсивная связь)
    tasko = models.ForeignKey('self', verbose_name=_('Tasko'), blank=True, null=True,
                              related_name='%(app_label)s_%(class)s_tasko', on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # указание что это абстрактный класс
        abstract = True
        # читабельное название модели, в единственном числе

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле ID этой модели
        return '{}'.format(self.id)


# Функционал задач пользователей
# Проекты пользователей, использует абстрактный класс TaskojProjektoAbstrakta
class TaskojUzantoProjekto(TaskojProjektoAbstrakta):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), default=0)

    # владелец (пользователь)
    posedanto = models.ForeignKey(Uzanto, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # описание многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_uzantoj_projektoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Projekto de uzanto')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Projektoj de uzantoj')
        unique_together = ("id","posedanto")
        # права
        permissions = (
            ('povas_vidi_uzantojn_projektojn', _('Povas vidi uzantojn projektojn')),
            ('povas_krei_uzantojn_projektojn', _('Povas krei uzantojn projektojn')),
            ('povas_forigi_uzantojn_projektojn', _('Povas forigu uzantojn projektojn')),
            ('povas_shanghi_uzantojn_projektojn', _('Povas ŝanĝi uzantojn projektojn')),
        )

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(posedanto=self.posedanto).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojUzantoProjekto, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                               update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if self.autoro == user_obj:
                all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))
            else:
                all_perms = set()

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_uzantojn_projektojn', 'taskoj.povas_krei_uzantojn_projektojn',
                'taskoj.povas_forigi_uzantojn_projektojn', 'taskoj.povas_shanghi_uzantojn_projektojn',
                'taskoj.povas_krei_uzantojn_direktojn'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_uzantojn_projektojn')
                    or user_obj.has_perm('taskoj.povas_vidi_uzantojn_projektojn')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_uzantojn_projektojn'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond

# Направления задач пользователей, использует абстрактный класс TaskojDirektoAbstrakta
class TaskojUzantoDirekto(TaskojDirektoAbstrakta):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), default=0)

    # владелец (пользователь)
    posedanto = models.ForeignKey(Uzanto, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # проект
    projekto = models.ForeignKey(TaskojUzantoProjekto, verbose_name=_('Projekto'), blank=False, default=None,
                                 on_delete=models.CASCADE)

    # описание многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_uzantoj_direktoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Direkto de taskoj de uzantoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Direktoj de taskoj de uzantoj')
        unique_together = ("id","posedanto")
        # права
        permissions = (
            ('povas_vidi_uzantojn_direktojn', _('Povas vidi uzantojn direktojn')),
            ('povas_krei_uzantojn_direktojn', _('Povas krei uzantojn direktojn')),
            ('povas_forigi_uzantojn_direktojn', _('Povas forigu uzantojn direktojn')),
            ('povas_shanghi_uzantojn_direktojn', _('Povas ŝanĝi uzantojn direktojn')),
        )

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(posedanto=self.posedanto).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojUzantoDirekto, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                              update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if self.autoro == user_obj:
                all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))
            else:
                all_perms = set()

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_uzantojn_direktojn', 'taskoj.povas_krei_uzantojn_direktojn',
                'taskoj.povas_forigi_uzantojn_direktojn', 'taskoj.povas_shanghi_uzantojn_direktojn',
                'taskoj.povas_krei_uzantojn_etapojn'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_uzantojn_direktojn')
                    or user_obj.has_perm('taskoj.povas_vidi_uzantojn_direktojn')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_uzantojn_direktojn'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond

# Этапы задач пользователей, использует абстрактный класс TaskojEtapoAbstrakta
class TaskojUzantoEtapo(TaskojEtapoAbstrakta):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), default=0)

    # владелец (пользователь)
    posedanto = models.ForeignKey(Uzanto, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # направление
    direkto = models.ForeignKey(TaskojUzantoDirekto, verbose_name=_('Direkto'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # описание многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_uzantoj_etapoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Etapo de taskoj de uzantoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Etapoj de taskoj de uzantoj')
        unique_together = ("id","posedanto")
        # права
        permissions = (
            ('povas_vidi_uzantojn_etapojn', _('Povas vidi uzantojn etapojn')),
            ('povas_krei_uzantojn_etapojn', _('Povas krei uzantojn etapojn')),
            ('povas_forigi_uzantojn_etapojn', _('Povas forigu uzantojn etapojn')),
            ('povas_shanghi_uzantojn_etapojn', _('Povas ŝanĝi uzantojn etapojn')),
        )

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(posedanto=self.posedanto).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojUzantoEtapo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if self.autoro == user_obj:
                all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))
            else:
                all_perms = set()

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_uzantojn_etapojn', 'taskoj.povas_krei_uzantojn_etapojn',
                'taskoj.povas_forigi_uzantojn_etapojn', 'taskoj.povas_shanghi_uzantojn_etapojn',
                'taskoj.povas_krei_uzantojn_taskojn'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_uzantojn_etapojn')
                    or user_obj.has_perm('taskoj.povas_vidi_uzantojn_etapojn')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_uzantojn_etapojn'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond

# Задачи пользователей, использует абстрактный класс TaskojTaskoAbstrakta
class TaskojUzantoTasko(TaskojTaskoAbstrakta):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), default=0)

    # владелец (пользователь)
    posedanto = models.ForeignKey(Uzanto, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # этап
    etapo = models.ForeignKey(TaskojUzantoEtapo, verbose_name=_('Etapo'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # описание многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_uzantoj_taskoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tasko de uzanto')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Taskoj de uzantoj')
        unique_together = ("id","posedanto")
        # права
        permissions = (
            ('povas_vidi_uzantojn_taskojn', _('Povas vidi uzantojn taskojn')),
            ('povas_krei_uzantojn_taskojn', _('Povas krei uzantojn taskojn')),
            ('povas_forigi_uzantojn_taskojn', _('Povas forigu uzantojn taskojn')),
            ('povas_shanghi_uzantojn_taskojn', _('Povas ŝanĝi uzantojn taskojn')),
        )

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(posedanto=self.posedanto).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojUzantoTasko, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)
    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if self.autoro == user_obj:
                all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))
            else:
                all_perms = set()

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_uzantojn_taskojn', 'taskoj.povas_krei_uzantojn_taskojn',
                'taskoj.povas_forigi_uzantojn_taskojn', 'taskoj.povas_shanghi_uzantojn_taskojn'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_uzantojn_taskojn')
                    or user_obj.has_perm('taskoj.povas_vidi_uzantojn_taskojn')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_uzantojn_taskojn'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Функционал задач соообществ
# Проекты сообществ, использует абстрактный класс TaskojProjektoAbstrakta
class TaskojKomunumoProjekto(TaskojProjektoAbstrakta):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), default=0)

    # владелец (сообщество)
    posedanto = models.ForeignKey(Komunumo, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # автор проекта
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None, on_delete=models.CASCADE)

    # описание многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_sovetoj_projektoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Projekto de soveto')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Projektoj de sovetoj')
        unique_together = ("id","posedanto")
        # права
        permissions = (
            ('povas_vidi_komunumoj_projektojn', _('Povas vidi komunumoj projektojn')),
            ('povas_krei_komunumoj_projektojn', _('Povas krei komunumoj projektojn')),
            ('povas_forigi_komunumoj_projektojn', _('Povas forigu komunumoj projektojn')),
            ('povas_shanghi_komunumoj_projektojn', _('Povas ŝanĝi komunumoj projektojn')),
        )

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(posedanto=self.posedanto).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojKomunumoProjekto, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                               update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения конференций
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Определяем тип членства для пользователя в родительских собществах
            membro = KomunumoMembro.objects.select_related('tipo').get(
                posedanto_id=self.posedanto_id, autoro=user_obj, forigo=False
            )

            if membro.tipo.kodo in ('administranto', 'komunumano-adm'):
                # Если есть права администратора или администратора сообщества
                all_perms |= set(perms.user_komunumo_adm_perms(apps=('taskoj',)))
            elif membro.tipo.kodo in ('moderiganto', 'komunumano-mod'):
                # Если есть права модератора или модератора сообщества
                all_perms |= set(perms.user_komunumo_mod_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_komunumoj_projektojn', 'taskoj.povas_krei_komunumoj_projektojn',
                'taskoj.povas_forigi_komunumoj_projektojn', 'taskoj.povas_shanghi_komunumoj_projektojn',
                'taskoj.povas_krei_komunumoj_direktojn'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_komunumoj_projektojn')
                    or user_obj.has_perm('taskoj.povas_vidi_komunumoj_projektojn')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

                if perms.has_komunumo_adm_perms('taskoj.povas_vidi_komunumoj_projektojn'):
                    # Однако, если есть родительские сообщества, где пользователь является администратором,
                    # получаем список uuid для этих сообществ
                    adms = KomunumoMembro.objects.filter(
                        autoro=user_obj, forigo=False, tipo__kodo__in=('administranto', 'komunumano-adm')
                    ).values_list('posedanto_id', flat=True)
                    # Добавляем условие через логическое ИЛИ (|=)
                    cond |= Q(posedanto_id__in=adms)

                if perms.has_komunumo_mod_perms('taskoj.povas_vidi_komunumoj_projektojn'):
                    # Или сообщества, где пользователь является модератором,
                    # получаем список uuid для этих сообществ
                    mods = KomunumoMembro.objects.filter(
                        autoro=user_obj, forigo=False, tipo__kodo__in=('moderiganto', 'komunumano-mod')
                    ).values_list('posedanto_id', flat=True)
                    # Добавляем условие через логическое ИЛИ (|=)
                    cond |= Q(posedanto_id__in=mods)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_komunumoj_projektojn'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond

# Направления задач сообществ, использует абстрактный класс TaskojDirektoAbstrakta
class TaskojKomunumoDirekto(TaskojDirektoAbstrakta):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), default=0)

    # владелец (сообщество)
    posedanto = models.ForeignKey(Komunumo, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # проект
    projekto = models.ForeignKey(TaskojKomunumoProjekto, verbose_name=_('Projekto'), blank=False, default=None,
                                 on_delete=models.CASCADE)

    # автор направления
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None, on_delete=models.CASCADE)

    # описание многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_sovetoj_direktoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Direkto de taskoj de soveto')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Direktoj de taskoj de sovetoj')
        unique_together = ("id","posedanto")
        # права
        permissions = (
            ('povas_vidi_komunumoj_direktojn', _('Povas vidi komunumoj direktojn')),
            ('povas_krei_komunumoj_direktojn', _('Povas krei komunumoj direktojn')),
            ('povas_forigi_komunumoj_direktojn', _('Povas forigu komunumoj direktojn')),
            ('povas_shanghi_komunumoj_direktojn', _('Povas ŝanĝi komunumoj direktojn')),
        )

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(posedanto=self.posedanto).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojKomunumoDirekto, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                              update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения конференций
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Определяем тип членства для пользователя в родительских собществах
            membro = KomunumoMembro.objects.select_related('tipo').get(
                posedanto_id=self.posedanto_id, autoro=user_obj, forigo=False
            )

            if membro.tipo.kodo in ('administranto', 'komunumano-adm'):
                # Если есть права администратора или администратора сообщества
                all_perms |= set(perms.user_komunumo_adm_perms(apps=('taskoj',)))
            elif membro.tipo.kodo in ('moderiganto', 'komunumano-mod'):
                # Если есть права модератора или модератора сообщества
                all_perms |= set(perms.user_komunumo_mod_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_komunumoj_direktojn', 'taskoj.povas_krei_komunumoj_direktojn',
                'taskoj.povas_forigi_komunumoj_direktojn', 'taskoj.povas_shanghi_komunumoj_direktojn',
                'taskoj.povas_krei_komunumoj_etapojn'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_komunumoj_direktojn')
                    or user_obj.has_perm('taskoj.povas_vidi_komunumoj_direktojn')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

                if perms.has_komunumo_adm_perms('taskoj.povas_vidi_komunumoj_direktojn'):
                    # Однако, если есть родительские сообщества, где пользователь является администратором,
                    # получаем список uuid для этих сообществ
                    adms = KomunumoMembro.objects.filter(
                        autoro=user_obj, forigo=False, tipo__kodo__in=('administranto', 'komunumano-adm')
                    ).values_list('posedanto_id', flat=True)
                    # Добавляем условие через логическое ИЛИ (|=)
                    cond |= Q(posedanto_id__in=adms)

                if perms.has_komunumo_mod_perms('taskoj.povas_vidi_komunumoj_direktojn'):
                    # Или сообщества, где пользователь является модератором,
                    # получаем список uuid для этих сообществ
                    mods = KomunumoMembro.objects.filter(
                        autoro=user_obj, forigo=False, tipo__kodo__in=('moderiganto', 'komunumano-mod')
                    ).values_list('posedanto_id', flat=True)
                    # Добавляем условие через логическое ИЛИ (|=)
                    cond |= Q(posedanto_id__in=mods)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_komunumoj_direktojn'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond

# Этапы задач сообществ, использует абстрактный класс TaskojEtapoAbstrakta
class TaskojKomunumoEtapo(TaskojEtapoAbstrakta):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), default=0)

    # владелец (сообщество)
    posedanto = models.ForeignKey(Komunumo, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # направление
    direkto = models.ForeignKey(TaskojKomunumoDirekto, verbose_name=_('Direkto'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # автор этапа
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None, on_delete=models.CASCADE)

    # описание многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_sovetoj_etapoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Etapo de taskoj de soveto')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Etapoj de taskoj de sovetoj')
        unique_together = ("id","posedanto")
        # права
        permissions = (
            ('povas_vidi_komunumoj_etapojn', _('Povas vidi komunumoj etapojn')),
            ('povas_krei_komunumoj_etapojn', _('Povas krei komunumoj etapojn')),
            ('povas_forigi_komunumoj_etapojn', _('Povas forigu komunumoj etapojn')),
            ('povas_shanghi_komunumoj_etapojn', _('Povas ŝanĝi komunumoj etapojn')),
        )

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(posedanto=self.posedanto).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojKomunumoEtapo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения конференций
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Определяем тип членства для пользователя в родительских собществах
            membro = KomunumoMembro.objects.select_related('tipo').get(
                posedanto_id=self.posedanto_id, autoro=user_obj, forigo=False
            )

            if membro.tipo.kodo in ('administranto', 'komunumano-adm'):
                # Если есть права администратора или администратора сообщества
                all_perms |= set(perms.user_komunumo_adm_perms(apps=('taskoj',)))
            elif membro.tipo.kodo in ('moderiganto', 'komunumano-mod'):
                # Если есть права модератора или модератора сообщества
                all_perms |= set(perms.user_komunumo_mod_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_komunumoj_etapojn', 'taskoj.povas_krei_komunumoj_etapojn',
                'taskoj.povas_forigi_komunumoj_etapojn', 'taskoj.povas_shanghi_komunumoj_etapojn',
                'taskoj.povas_krei_komunumoj_taskojn'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_komunumoj_etapojn')
                    or user_obj.has_perm('taskoj.povas_vidi_komunumoj_etapojn')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

                if perms.has_komunumo_adm_perms('taskoj.povas_vidi_komunumoj_etapojn'):
                    # Однако, если есть родительские сообщества, где пользователь является администратором,
                    # получаем список uuid для этих сообществ
                    adms = KomunumoMembro.objects.filter(
                        autoro=user_obj, forigo=False, tipo__kodo__in=('administranto', 'komunumano-adm')
                    ).values_list('posedanto_id', flat=True)
                    # Добавляем условие через логическое ИЛИ (|=)
                    cond |= Q(posedanto_id__in=adms)

                if perms.has_komunumo_mod_perms('taskoj.povas_vidi_komunumoj_etapojn'):
                    # Или сообщества, где пользователь является модератором,
                    # получаем список uuid для этих сообществ
                    mods = KomunumoMembro.objects.filter(
                        autoro=user_obj, forigo=False, tipo__kodo__in=('moderiganto', 'komunumano-mod')
                    ).values_list('posedanto_id', flat=True)
                    # Добавляем условие через логическое ИЛИ (|=)
                    cond |= Q(posedanto_id__in=mods)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_komunumoj_etapojn'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond

# Задачи сообществ, использует абстрактный класс TaskojTaskoAbstrakta
class TaskojKomunumoTasko(TaskojTaskoAbstrakta):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), default=0)

    # владелец (организация)
    posedanto = models.ForeignKey(Komunumo, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # этап
    etapo = models.ForeignKey(TaskojKomunumoEtapo, verbose_name=_('Etapo'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # автор задачи
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None, on_delete=models.CASCADE)

    # описание многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_sovetoj_taskoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tasko de soveto')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Taskoj de sovetoj')
        unique_together = ("id","posedanto")
        # права
        permissions = (
            ('povas_vidi_komunumoj_taskojn', _('Povas vidi komunumoj taskojn')),
            ('povas_krei_komunumoj_taskojn', _('Povas krei komunumoj taskojn')),
            ('povas_forigi_komunumoj_taskojn', _('Povas forigu komunumoj taskojn')),
            ('povas_shanghi_komunumoj_taskojn', _('Povas ŝanĝi komunumoj taskojn')),
        )

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(posedanto=self.posedanto).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojKomunumoTasko, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения конференций
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Определяем тип членства для пользователя в родительских собществах
            membro = KomunumoMembro.objects.select_related('tipo').get(
                posedanto_id=self.posedanto_id, autoro=user_obj, forigo=False
            )

            if membro.tipo.kodo in ('administranto', 'komunumano-adm'):
                # Если есть права администратора или администратора сообщества
                all_perms |= set(perms.user_komunumo_adm_perms(apps=('taskoj',)))
            elif membro.tipo.kodo in ('moderiganto', 'komunumano-mod'):
                # Если есть права модератора или модератора сообщества
                all_perms |= set(perms.user_komunumo_mod_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_komunumoj_taskojn', 'taskoj.povas_krei_komunumoj_taskojn',
                'taskoj.povas_forigi_komunumoj_taskojn', 'taskoj.povas_shanghi_komunumoj_taskojn'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_komunumoj_taskojn')
                    or user_obj.has_perm('taskoj.povas_vidi_komunumoj_taskojn')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

                if perms.has_komunumo_adm_perms('taskoj.povas_vidi_komunumoj_taskojn'):
                    # Однако, если есть родительские сообщества, где пользователь является администратором,
                    # получаем список uuid для этих сообществ
                    adms = KomunumoMembro.objects.filter(
                        autoro=user_obj, forigo=False, tipo__kodo__in=('administranto', 'komunumano-adm')
                    ).values_list('posedanto_id', flat=True)
                    # Добавляем условие через логическое ИЛИ (|=)
                    cond |= Q(posedanto_id__in=adms)

                if perms.has_komunumo_mod_perms('taskoj.povas_vidi_komunumoj_taskojn'):
                    # Или сообщества, где пользователь является модератором,
                    # получаем список uuid для этих сообществ
                    mods = KomunumoMembro.objects.filter(
                        autoro=user_obj, forigo=False, tipo__kodo__in=('moderiganto', 'komunumano-mod')
                    ).values_list('posedanto_id', flat=True)
                    # Добавляем условие через логическое ИЛИ (|=)
                    cond |= Q(posedanto_id__in=mods)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_komunumoj_taskojn'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond

# Глобальные задачи
# Шаблоны глобальных задач, использует абстрактный класс SiriusoBazaAbstrakta3
class TaskojXeneralaTaskoWablono(SiriusoBazaAbstrakta3):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор шаблона
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None, on_delete=models.CASCADE)

    # описание многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_xeneralaj_taskoj_wablono'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ŝablono de ĝeneralaj taskoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ŝablonoj de ĝeneralaj taskoj')
        # права
        permissions = (
            ('povas_vidi_xeneralaj_taskoj_wablono', _('Povas vidi xeneralaj taskoj wablono')),
            ('povas_krei_xeneralaj_taskoj_wablono', _('Povas krei xeneralaj taskoj wablono')),
            ('povas_forigi_xeneralaj_taskoj_wablono', _('Povas forigu xeneralaj taskoj wablono')),
            ('povas_shanghi_xeneralaj_taskoj_wablono', _('Povas ŝanĝi xeneralaj taskoj wablono')),
        )

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojXeneralaTaskoWablono, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                                     update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if self.autoro == user_obj:
                all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))
            else:
                all_perms = set()

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_xeneralaj_taskoj_wablono', 'taskoj.povas_krei_xeneralaj_taskoj_wablono',
                'taskoj.povas_forigi_xeneralaj_taskoj_wablono', 'taskoj.povas_shanghi_xeneralaj_taskoj_wablono'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_xeneralaj_taskoj_wablono')
                    or user_obj.has_perm('taskoj.povas_vidi_xeneralaj_taskoj_wablono')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_xeneralaj_taskoj_wablono'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond

# Направления глобальных задач, использует абстрактный класс TaskojDirektoAbstrakta
class TaskojXeneralaDirekto(TaskojDirektoAbstrakta):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор направления глобальных задач
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None, on_delete=models.CASCADE)

    # описание многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_xeneralaj_direktoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Direkto de ĝeneralaj taskoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Direktoj de ĝeneralaj taskoj')
        # права
        permissions = (
            ('povas_vidi_xeneralaj_direktoj', _('Povas vidi xeneralaj direktoj')),
            ('povas_krei_xeneralaj_direktoj', _('Povas krei xeneralaj direktoj')),
            ('povas_forigi_xeneralaj_direktoj', _('Povas forigu xeneralaj direktoj')),
            ('povas_shanghi_xeneralaj_direktoj', _('Povas ŝanĝi xeneralaj direktoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле priskribo этой модели
        return '{}'.format(get_enhavo(self.priskribo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojXeneralaDirekto, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                                update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if self.autoro == user_obj:
                all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))
            else:
                all_perms = set()

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_xeneralaj_direktoj', 'taskoj.povas_krei_xeneralaj_direktoj',
                'taskoj.povas_forigi_xeneralaj_direktoj', 'taskoj.povas_shanghi_xeneralaj_direktoj',
                'taskoj.povas_krei_xeneralaj_etapoj'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_xeneralaj_direktoj')
                    or user_obj.has_perm('taskoj.povas_vidi_xeneralaj_direktoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_xeneralaj_direktoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond

# Этапы глобальных задач, использует абстрактный класс TaskojEtapoAbstrakta
class TaskojXeneralaEtapo(TaskojEtapoAbstrakta):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # направление
    direkto = models.ForeignKey(TaskojXeneralaDirekto, verbose_name=_('Direkto'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # автор этапа глобальных задач
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None, on_delete=models.CASCADE)

    # описание многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_xeneralaj_etapoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Etapo de ĝeneralaj taskoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Etapoj de ĝeneralaj taskoj')
        # права
        permissions = (
            ('povas_vidi_xeneralaj_etapoj', _('Povas vidi xeneralaj etapoj')),
            ('povas_krei_xeneralaj_etapoj', _('Povas krei xeneralaj etapoj')),
            ('povas_forigi_xeneralaj_etapoj', _('Povas forigu xeneralaj etapoj')),
            ('povas_shanghi_xeneralaj_etapoj', _('Povas ŝanĝi xeneralaj etapoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле priskribo этой модели
        return '{}'.format(get_enhavo(self.priskribo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojXeneralaEtapo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                              update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if self.autoro == user_obj:
                all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))
            else:
                all_perms = set()

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_xeneralaj_etapoj', 'taskoj.povas_krei_xeneralaj_etapoj',
                'taskoj.povas_forigi_xeneralaj_etapoj', 'taskoj.povas_shanghi_xeneralaj_etapoj',
                'taskoj.povas_forigi_xeneralaj_taskoj'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_xeneralaj_etapoj')
                    or user_obj.has_perm('taskoj.povas_vidi_xeneralaj_etapoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_xeneralaj_etapoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond

# Глобальные задачи, использует абстрактный класс TaskojTaskoAbstrakta
class TaskojXeneralaTasko(TaskojTaskoAbstrakta):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # этап
    etapo = models.ForeignKey(TaskojXeneralaEtapo, verbose_name=_('Etapo'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # автор глобальной задачи
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None, on_delete=models.CASCADE)

    # описание многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_xeneralaj_taskoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ĝenerala tasko')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ĝeneralaj taskoj')
        # права
        permissions = (
            ('povas_vidi_xeneralaj_taskoj', _('Povas vidi xeneralaj taskoj')),
            ('povas_krei_xeneralaj_taskoj', _('Povas krei xeneralaj taskoj')),
            ('povas_forigi_xeneralaj_taskoj', _('Povas forigu xeneralaj taskoj')),
            ('povas_shanghi_xeneralaj_taskoj', _('Povas ŝanĝi xeneralaj taskoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле ID этой модели
        return '{}'.format(self.id)

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojXeneralaTasko, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                              update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if self.autoro == user_obj:
                all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))
            else:
                all_perms = set()

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_xeneralaj_taskoj', 'taskoj.povas_krei_xeneralaj_taskoj',
                'taskoj.povas_forigi_xeneralaj_taskoj', 'taskoj.povas_shanghi_xeneralaj_taskoj',
                'taskoj.povas_forigi_xeneralaj_taskoj_realigoj'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_xeneralaj_taskoj')
                    or user_obj.has_perm('taskoj.povas_vidi_xeneralaj_taskoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_xeneralaj_taskoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond

# Реализация глобальных задач, использует абстрактный класс TaskojTaskoAbstrakta
class TaskojXeneralaTaskoRealigo(SiriusoBazaAbstrakta3):

    # реализуемая задача
    tasko = models.ForeignKey(TaskojXeneralaTasko, verbose_name=_('Tasko'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # владелец реализации
    posedanto = models.ForeignKey(Uzanto, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_xeneralaj_taskoj_realigoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Realigo de ĝeneralaj taskoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Realigoj de ĝeneralaj taskoj')
        permissions = (
            ('povas_vidi_xeneralaj_taskoj_realigoj', _('Povas vidi xeneralaj taskoj realigoj')),
            ('povas_krei_xeneralaj_taskoj_realigoj', _('Povas krei xeneralaj taskoj realigoj')),
            ('povas_forigi_xeneralaj_taskoj_realigoj', _('Povas forigu xeneralaj taskoj realigoj')),
            ('povas_shanghi_xeneralaj_taskoj_realigoj', _('Povas ŝanĝi xeneralaj taskoj realigoj')),
        )

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if self.autoro == user_obj:
                all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))
            else:
                all_perms = set()

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_xeneralaj_taskoj_realigoj', 'taskoj.povas_krei_xeneralaj_taskoj_realigoj',
                'taskoj.povas_forigi_xeneralaj_taskoj_realigoj', 'taskoj.povas_shanghi_xeneralaj_taskoj_realigoj'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_xeneralaj_taskoj_realigoj')
                    or user_obj.has_perm('taskoj.povas_vidi_xeneralaj_taskoj_realigoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_xeneralaj_taskoj_realigoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
