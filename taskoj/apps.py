from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class TaskojConfig(AppConfig):
    name = 'taskoj'
    verbose_name = _('Taskoj')
