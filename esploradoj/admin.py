"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from django import forms
from django.utils.translation import ugettext_lazy as _
from siriuso.utils import get_enhavo, set_enhavo
from esploradoj.models import *
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
import json


class TekstoMixin:
    @staticmethod
    def teksto(obj, field):
        kampo = getattr(obj, field)

        if kampo is not None:
            result = get_enhavo(kampo)

            if result:
                return result[0]
        return ''

    def nomo_teksto(self, obj):
        return self.teksto(obj=obj, field='nomo')

    def priskribo_teksto(self, obj):
        return self.teksto(obj=obj, field='priskribo')

    def teksto_teksto(self, obj):
        return self.teksto(obj=obj, field='teksto')


# Форма для многоязычных названий типов категорий Конференций
class EsploradojKategorioTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=True)

    class Meta:
        model = EsploradojKategorioTipo
        fields = [field.name for field in EsploradojKategorioTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы категорий Конференций
@admin.register(EsploradojKategorioTipo)
class EsploradojKategorioTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = EsploradojKategorioTipoFormo
    list_display = ('nomo_teksto', 'kodo', 'uuid')
    exclude = ('nomo_teksto',)

    class Meta:
        model = EsploradojKategorioTipo


# Форма для многоязычнных полей категорий Конференций
class EsploradojKategorioFormo(forms.ModelForm):

    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=True)
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=True)

    class Meta:
        model = EsploradojKategorio
        fields = [field.name for field in EsploradojKategorio._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Категории Конференций
@admin.register(EsploradojKategorio)
class EsploradojKategorioAdmin(admin.ModelAdmin, TekstoMixin):
    form = EsploradojKategorioFormo
    list_display = ('id', 'nomo_teksto', 'priskribo_teksto')
    exclude = ('id',)

    class Meta:
        model = EsploradojKategorio


# Форма для многоязычных названий типов тем Конференций
class EsploradojTemoTipoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=True)

    class Meta:
        model = EsploradojTemoTipo
        fields = [field.name for field in EsploradojTemoTipo._meta.fields if
                  field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы тем Конференций
@admin.register(EsploradojTemoTipo)
class EsploradojTemoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = EsploradojTemoTipoFormo
    list_display = ('nomo_teksto', 'kodo', 'uuid')
    exclude = ('nomo_teksto',)

    class Meta:
        model = EsploradojTemoTipo


# Форма для многоязычнных полей тем Конференций
class EsploradojTemoFormo(forms.ModelForm):
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=True)
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=True)

    class Meta:
        model = EsploradojTemo
        fields = [field.name for field in EsploradojTemo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Темы Конференций
@admin.register(EsploradojTemo)
class EsploradojTemoAdmin(admin.ModelAdmin, TekstoMixin):
    form = EsploradojTemoFormo
    list_display = ('id', 'priskribo_teksto', 'nomo_teksto')
    exclude = ('id',)

    class Meta:
        model = EsploradojTemo
