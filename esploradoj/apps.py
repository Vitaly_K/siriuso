from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class EsploradojConfig(AppConfig):
    name = 'esploradoj'
    verbose_name = _('Esploradoj')

    def ready(self):
        import esploradoj.signals
        # Импортом сообщаем django, что надо проанализировать код в модуле tasks
        # При этом происходит регистрация функций заданий celery
        import esploradoj.tasks
