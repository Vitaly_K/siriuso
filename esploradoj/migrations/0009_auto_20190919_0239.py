# Generated by Django 2.0.13 on 2019-09-19 02:39

from django.conf import settings
import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion
import siriuso.models.postgres
import siriuso.utils.modules
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('komunumoj', '0028_realecoj'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('informiloj', '0011_auto_20190609_0303'),
        ('main', '0014_realecoj'),
        ('esploradoj', '0008_auto_20190917_1554'),
    ]

    operations = [
        migrations.CreateModel(
            name='EsploradojKategorio',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('a_foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Aŭtomata foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('lasta_dato', models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo')),
                ('id', models.IntegerField(default=0, unique=True, verbose_name='ID')),
                ('nomo', django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Titolo')),
                ('priskribo', django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Priskribo')),
            ],
            options={
                'verbose_name': 'Kategorio',
                'verbose_name_plural': 'Kategorioj',
                'db_table': 'esploradoj_kategorioj',
                'permissions': (('povas_vidi_kategorion', 'Povas vidi kategorion'), ('povas_krei_kategorion', 'Povas krei kategorion'), ('povas_forigi_kategorion', 'Povas forigu kategorion'), ('povas_shanghi_kategorion', 'Povas ŝanĝi kategorion')),
            },
        ),
        migrations.CreateModel(
            name='EsploradojKategorioTipo',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('a_foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Aŭtomata foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('kodo', models.CharField(max_length=16, verbose_name='Kodo')),
                ('speciala', models.BooleanField(default=False, verbose_name='Speciala')),
                ('nomo', django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Nomo')),
                ('realeco', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco')),
                ('wablomo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono')),
            ],
            options={
                'verbose_name': 'Tipo de kategorioj',
                'verbose_name_plural': 'Tipoj de kategorioj',
                'db_table': 'esploradoj_kategorioj_tipoj',
                'permissions': (('povas_vidi_kategorian_tipon', 'Povas vidi kategorian tipon'), ('povas_krei_kategorian_tipon', 'Povas krei kategorian tipon'), ('povas_forigi_kategorian_tipon', 'Povas forigu kategorian tipon'), ('povas_shanghi_kategorian_tipon', 'Povas ŝanĝi kategorian tipon')),
            },
        ),
        migrations.CreateModel(
            name='EsploradojSciigoj',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('a_foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Aŭtomata foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('lasta_dato', models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo')),
            ],
            options={
                'permissions': (('povas_vidi_sciigoj', 'Povas vidi sciigoj'), ('povas_krei_sciigoj', 'Povas krei sciigoj'), ('povas_forigi_sciigoj', 'Povas forigu sciigoj'), ('povas_shanghi_sciigoj', 'Povas ŝanĝi sciigoj')),
            },
        ),
        migrations.CreateModel(
            name='EsploradojTemo',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('a_foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Aŭtomata foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('lasta_dato', models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo')),
                ('id', models.IntegerField(default=0, unique=True, verbose_name='ID')),
                ('nomo', django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Titolo')),
                ('priskribo', django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Priskribo')),
                ('fermita', models.BooleanField(default=False, verbose_name='Fermita')),
                ('fiksa', models.BooleanField(default=False, verbose_name='Fiksa')),
                ('fiksa_listo', models.IntegerField(blank=True, null=True, verbose_name='Fiksa listo')),
            ],
            options={
                'verbose_name': 'Temo',
                'verbose_name_plural': 'Temoj',
                'db_table': 'esploradoj_temoj',
                'permissions': (('povas_vidi_temon', 'Povas vidi temon'), ('povas_krei_temon', 'Povas krei temon'), ('povas_forigi_temon', 'Povas forigu temon'), ('povas_shanghi_temon', 'Povas ŝanĝi temon')),
            },
        ),
        migrations.CreateModel(
            name='EsploradojTemoKomento',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('a_foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Aŭtomata foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('lasta_dato', models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo')),
                ('id', models.IntegerField(db_index=True, default=0, null=True, verbose_name='ID')),
                ('teksto', django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Teksto')),
            ],
            options={
                'verbose_name': 'Tema komento',
                'verbose_name_plural': 'Temaj komentoj',
                'db_table': 'esploradoj_temoj_komentoj',
                'permissions': (('povas_vidi_teman_komenton', 'Povas vidi teman komenton'), ('povas_krei_teman_komenton', 'Povas krei teman komenton'), ('povas_forigi_teman_komenton', 'Povas forigu teman komenton'), ('povas_shanghi_teman_komenton', 'Povas ŝanĝi teman komenton')),
            },
        ),
        migrations.CreateModel(
            name='EsploradojTemoTipo',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('a_foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Aŭtomata foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('kodo', models.CharField(max_length=16, verbose_name='Kodo')),
                ('speciala', models.BooleanField(default=False, verbose_name='Speciala')),
                ('nomo', django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Nomo')),
                ('realeco', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco')),
                ('wablomo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono')),
            ],
            options={
                'verbose_name': 'Tipo de temoj',
                'verbose_name_plural': 'Tipoj de temoj',
                'db_table': 'esploradoj_temoj_tipoj',
                'permissions': (('povas_vidi_teman_tipon', 'Povas vidi teman tipon'), ('povas_krei_teman_tipon', 'Povas krei teman tipon'), ('povas_forigi_teman_tipon', 'Povas forigu teman tipon'), ('povas_shanghi_teman_tipon', 'Povas ŝanĝi teman tipon')),
            },
        ),
        migrations.CreateModel(
            name='EsploradojUzantoHistorio',
            fields=[
                ('posedanto', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('historio', django.contrib.postgres.fields.jsonb.JSONField(blank=True, db_index=True, default=dict, verbose_name='История посещений')),
            ],
            options={
                'db_table': 'esploradoj_uzantoj_historioj',
            },
        ),
        migrations.AddField(
            model_name='esploradojtemokomento',
            name='autoro',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Aŭtoro'),
        ),
        migrations.AddField(
            model_name='esploradojtemokomento',
            name='komento',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='esploradoj.EsploradojTemoKomento', verbose_name='Komento temo komento'),
        ),
        migrations.AddField(
            model_name='esploradojtemokomento',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='esploradoj_esploradojtemokomento_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='esploradojtemokomento',
            name='posedanto',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='esploradoj.EsploradojTemo', verbose_name='Posedanto'),
        ),
        migrations.AddField(
            model_name='esploradojtemokomento',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='esploradojtemokomento',
            name='respondo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='esploradoj_esploradojtemokomento_respondo', to='esploradoj.EsploradojTemoKomento', verbose_name='Respondo'),
        ),
        migrations.AddField(
            model_name='esploradojtemokomento',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AddField(
            model_name='esploradojtemo',
            name='autoro',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Aŭtoro'),
        ),
        migrations.AddField(
            model_name='esploradojtemo',
            name='kategorio',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='esploradoj.EsploradojKategorio', verbose_name='Kategorio'),
        ),
        migrations.AddField(
            model_name='esploradojtemo',
            name='komentado_aliro',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='komunumoj.KomunumojAliro', verbose_name='Komentada aliro'),
        ),
        migrations.AddField(
            model_name='esploradojtemo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='esploradoj_esploradojtemo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='esploradojtemo',
            name='posedanto',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='komunumoj.Komunumo', verbose_name='Posedanto'),
        ),
        migrations.AddField(
            model_name='esploradojtemo',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='esploradojtemo',
            name='tipo',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='esploradoj.EsploradojTemoTipo', verbose_name='Tipo'),
        ),
        migrations.AddField(
            model_name='esploradojtemo',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AddField(
            model_name='esploradojsciigoj',
            name='autoro',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Aŭtoro'),
        ),
        migrations.AddField(
            model_name='esploradojsciigoj',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='esploradoj_esploradojsciigoj_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='esploradojsciigoj',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='esploradojsciigoj',
            name='sciigoj',
            field=models.ManyToManyField(blank=True, to='informiloj.InformilojSciigoTipo', verbose_name='Sciigo (muro)'),
        ),
        migrations.AddField(
            model_name='esploradojsciigoj',
            name='temo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='esploradoj.EsploradojTemo'),
        ),
        migrations.AddField(
            model_name='esploradojsciigoj',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AddField(
            model_name='esploradojkategorio',
            name='administrantoj',
            field=models.ManyToManyField(related_name='esploradojkategorio_administrantoj', to=settings.AUTH_USER_MODEL, verbose_name='Kategoriaj administrantoj'),
        ),
        migrations.AddField(
            model_name='esploradojkategorio',
            name='aliro',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='komunumoj.KomunumojAliro', verbose_name='Aliro'),
        ),
        migrations.AddField(
            model_name='esploradojkategorio',
            name='autoro',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Aŭtoro'),
        ),
        migrations.AddField(
            model_name='esploradojkategorio',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='esploradoj_esploradojkategorio_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='esploradojkategorio',
            name='moderatoroj',
            field=models.ManyToManyField(related_name='esploradojkategorio_moderatoroj', to=settings.AUTH_USER_MODEL, verbose_name='Kategoriaj moderatoroj'),
        ),
        migrations.AddField(
            model_name='esploradojkategorio',
            name='posedanto',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='komunumoj.Komunumo', verbose_name='Posedanto'),
        ),
        migrations.AddField(
            model_name='esploradojkategorio',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='esploradojkategorio',
            name='tipo',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='esploradoj.EsploradojKategorioTipo', verbose_name='Tipo'),
        ),
        migrations.AddField(
            model_name='esploradojkategorio',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
    ]
