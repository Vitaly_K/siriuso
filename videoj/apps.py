from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class VideojConfig(AppConfig):
    name = 'videoj'
    verbose_name = _('Videoj')
