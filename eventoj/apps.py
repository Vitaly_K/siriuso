from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class EventojConfig(AppConfig):
    name = 'eventoj'
    verbose_name = _('Eventoj')