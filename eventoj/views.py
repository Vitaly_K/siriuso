from django.shortcuts import render

# Create your views here.
def eventoj(request):
    return render(request, 'eventoj/eventoj.html')