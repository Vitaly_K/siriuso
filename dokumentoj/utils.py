"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.db import transaction
from django.db.models import Sum, Q
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from .models import *
from kombatantoj.models import Kombatanto, KombatantoResursoNomenklaturo, KombatantoResurso
from resursoj.models import ResursoMiksajo, ResursoKategorio

from siriuso.utils import get_enhavo
from .api.mutation import DokumentoAgo, automata_serchado


# Определяем настройку документа
def get_dok_agordoj(tipo, agordo):
    try:
        dok_agordo = DokumentaAgordo.objects.get(aktiva=True, forigo=False, dokumento_tipo__kodo=tipo)
        return getattr(dok_agordo, agordo) if hasattr(dok_agordo, agordo) else None
    except DokumentaAgordo.DoesNotExist:
        return None


# Определяем стоимость единицы ресурса
def resurso_kosto(organizo, resurso):
    query = KombatantoResurso.objects.filter(
        aktiva=True,
        forigo=False,
        organizo=organizo,
        resurso=resurso,
        rezervita=False,
        kvanto__gt=0,
        elcherpita=False
    ).aggregate(Sum('kvanto'), Sum('kosto'))

    if query['kvanto__sum']:
        return round((query['kosto__sum'] or 0) / (query['kvanto__sum']), 2)

    return 0


# Списать со склада
def forigi_varojn(request, dokumento, messages=[]):
    with transaction.atomic():
        for kordo in DokumentoKordo.objects.filter(
                aktiva=True,
                forigo=False,
                dokumento=dokumento,
        ):
            if isinstance(kordo.atributoj, dict) and ('rezervita_unika' in kordo.atributoj
                                                      or 'unika' in kordo.atributoj):
                atributo = 'rezervita_unika' if 'rezervita_unika' in kordo.atributoj else 'unika'

                # Если указаны конкретные экземпляры ресурса для списания
                KombatantoResurso.objects.select_for_update().filter(
                    aktiva=True,
                    forigo=False,
                    uuid__in=kordo.atributoj[atributo],
                    resurso=kordo.resurso,
                ).update(dokumento=dokumento, aktiva=False, organizo=dokumento.posedanto_magazeno)
            else:
                # Иначе формируем техническую запись на списание
                kosto = resurso_kosto(dokumento.celo_magazeno, kordo.resurso)
                KombatantoResurso.objects.create(
                    aktiva=True,
                    forigo=False,
                    organizo=dokumento.celo_magazeno,
                    dokumento=dokumento,
                    resurso=kordo.resurso,
                    kvanto=(-1 * kordo.kvanto),
                    kosto=round(-1 * kosto * kordo.kvanto, 2),
                    teknika=True,
                    elcherpita=False
                )

    return True


# Приходуем заявку на закупку
def krediti_acheton(request, dokumento, messages=[]):
    if dokumento.tipo.kodo in ('acheto', 'provizo', 'livero'):
        magazeno = dokumento.posedanto_magazeno
        kordoj = DokumentoKordo.objects.filter(dokumento=dokumento, aktiva=True, forigo=False)

        for kordo in kordoj:
            # Проверяем наличие ресурса в номенклатуре склада
            try:
                nom = KombatantoResursoNomenklaturo.objects.get(organizo=magazeno, resurso=kordo.resurso, aktiva=True,
                                                              forigo=False)
            except KombatantoResursoNomenklaturo.DoesNotExist:
                # Создаём запись номеклатуры
                stokado = Kombatanto.objects.filter(posedanto=magazeno, aktiva=True, forigo=False)[0]
                nom = KombatantoResursoNomenklaturo.objects.create(
                    organizo=magazeno,
                    stokado=stokado,
                    resurso=kordo.resurso,
                    minimuma=0,
                    maksimuma=None,
                    autoro=request.user,
                    aktiva=True,
                    forigo=False
                )

            # Приходуем ресурс
            if isinstance(kordo.atributoj, dict) and 'rezervita_unika' in kordo.atributoj:
                KombatantoResurso.objects.select_for_update().filter(
                    aktiva=False,
                    forigo=False,
                    dokumento=dokumento,
                    uuid__in=kordo.atributoj['rezervita_unika'],
                    organizo=dokumento.posedanto_magazeno,
                    resurso=kordo.resurso,
                    teknika=False,
                    rezervita=False
                ).update(aktiva=True, kosto=kordo.prezo)
            else:
                KombatantoResurso.objects.create(
                    organizo=magazeno,
                    dokumento=dokumento,
                    resurso=kordo.resurso,
                    kvanto=kordo.kvanto,
                    kosto=round(kordo.prezo * kordo.kvanto, 2),
                    autoro=request.user,
                    aktiva=True,
                    forigo=False,
                    elcherpita=False
                )

        return True

    return False


# Обратная связь с родительским документом Заявки на закупку
def nuligi_posedanton(request, dokumento, messages=[]):
    if dokumento.tipo.kodo in ('acheto', 'provizo', 'produktado') and dokumento.posedanto_dokumento:
        # Проверяем количество заявок у родителя
        if not Dokumento.objects.filter(
                ~Q(uuid=dokumento.uuid),
                aktiva=True,
                forigo=False,
                posedanto_dokumento=dokumento.posedanto_dokumento,
                statuso__kodo__in=['kreita', 'nova', 'en_laboro'],
        ):
            # Переводим на статус для отмены
            with transaction.atomic():
                statuso = DokumentoStatuso.objects.get(kodo='aplikoj_nuligitaj', aktiva=True,
                                                       forigo=False)
                pos_dok = Dokumento.objects.get(uuid=dokumento.posedanto_dokumento_id)
                pos_dok.statuso = statuso
                pos_dok.stadio = 6
                pos_dok.save()

            result_dok = DokumentoAgo.mutate(dokumento, request, pos_dok.uuid, 'nuligi', sistema=True).dokumento

            return result_dok and result_dok.statuso.kodo == 'en_laboro'

        return True

    else:
        return True

    return False


# Обратная связь с родительским документом
def konfirmu_posedanton(request, dokumento, messages=[]):
    if not dokumento.posedanto_dokumento:
        return True

    if dokumento.tipo.kodo == 'acheto':
        # Проверяем количество заявок у родителя
        if not Dokumento.objects.filter(
                ~Q(uuid=dokumento.uuid),
                aktiva=True,
                forigo=False,
                posedanto_dokumento=dokumento.posedanto_dokumento,
                statuso__kodo__in=['kreita', 'nova', 'en_laboro'],
        ):
            # Переводим на следующий статус
            with transaction.atomic():
                pos_dok = Dokumento.objects.get(uuid=dokumento.posedanto_dokumento_id)

                if pos_dok.tipo.kodo == 'provizo':
                    statuso = DokumentoStatuso.objects.get(kodo='preta_por_liveri', aktiva=True,
                                                           forigo=False)
                    pos_dok.statuso = statuso
                    pos_dok.stadio = 6
                    ago = 'konfirmu_acheton'
                else:
                    statuso = DokumentoStatuso.objects.get(kodo='produktado_kompletigita', aktiva=True,
                                                           forigo=False)
                    pos_dok.statuso = statuso
                    pos_dok.stadio = 6
                    ago = 'konfirmu_produktadon'
                pos_dok.save()

            result_dok = DokumentoAgo.mutate(dokumento, request, pos_dok.uuid,
                                             ago, sistema=True).dokumento

            return result_dok and result_dok.stadio == 7

        return True

    elif dokumento.tipo.kodo == 'provizo':
        # Проверяем количество заявок у родителя
        if not Dokumento.objects.filter(
                ~Q(uuid=dokumento.uuid),
                aktiva=True,
                forigo=False,
                posedanto_dokumento=dokumento.posedanto_dokumento,
                statuso__kodo__in=['kreita', 'nova', 'en_laboro'],
        ):
            # Переводим на следующий статус
            with transaction.atomic():
                statuso = DokumentoStatuso.objects.get(kodo='provizo_kompleta', aktiva=True,
                                                       forigo=False)
                pos_dok = Dokumento.objects.get(uuid=dokumento.posedanto_dokumento_id)
                pos_dok.statuso = statuso
                pos_dok.stadio = 6
                pos_dok.save()

            result_dok = DokumentoAgo.mutate(dokumento, request, pos_dok.uuid,
                                             'konfirmu_provizon', sistema=True).dokumento

            return result_dok and result_dok.stadio == 7

    elif dokumento.tipo.kodo in ('produktado', 'kompleta_aro'):
        with transaction.atomic():
            # Резервируем ресурсы из производства в нужном количестве в строки Заявки на поставку
            for kordo in DokumentoKordo.objects.select_for_update().filter(
                    aktiva=True,
                    forigo=False,
                    dokumento=dokumento.posedanto_dokumento,
                    resurso__kategorioj__kodo__contains='mikso',
                    resurso__unuo_mezuro='kvanto'
            ):
                if not isinstance(kordo.atributoj, dict):
                    kordo.atributoj = {}

                uuid_list = kordo.atributoj['rezervita_unika'] if 'rezervita_unika' in kordo.atributoj else []

                if len(uuid_list) < kordo.kvanto:
                    for sub_kordo in DokumentoKordo.objects.filter(
                            dokumento__posedanto_dokumento=dokumento.posedanto_dokumento,
                            dokumento__aktiva=True,
                            dokumento__forigo=False,
                            dokumento__statuso__kodo__in=['produkto_akceptita', 'kompletigita'],
                            dokumento__stadio=9,
                            aktiva=True,
                            forigo=False,
                            resurso=kordo.resurso
                    ):
                        if 'unika' in sub_kordo.atributoj:
                            uuid_list += sub_kordo.atributoj['unika'][0:int(kordo.kvanto - len(uuid_list))]

                        kordo.atributoj['rezervita_unika'] = uuid_list
                        kordo.save()

            statuso = DokumentoStatuso.objects.get(kodo='produktado_kompletigita', aktiva=True,
                                                   forigo=False)
            dokumento.posedanto_dokumento.statuso = statuso
            dokumento.posedanto_dokumento.stadio = 6
            dokumento.posedanto_dokumento.save()
            result_dok = DokumentoAgo.mutate(dokumento, request, dokumento.posedanto_dokumento.uuid,
                                             'konfirmu_produktadon', sistema=True).dokumento

            return result_dok and result_dok.stadio == 7

    return False


# Заявка на снабжение
# Проверяем наличие и резервируем ресурсы на складе исполнителя
def kontrolu_rezervon(request, dokumento, messages=[]):
    result = False
    if dokumento.tipo.kodo == 'provizo':
        result = True
        kordoj = DokumentoKordo.objects.select_for_update().filter(dokumento=dokumento, aktiva=True, forigo=False)
        with transaction.atomic():
            for kordo in kordoj:
                messages.append('{}:'.format(get_enhavo(kordo.resurso.nomo, empty_values=True)[0]))
                res_count = (
                                KombatantoResurso.objects.select_for_update()
                                    .filter(organizo=dokumento.celo_magazeno, resurso=kordo.resurso, aktiva=True,
                                            forigo=False, rezervita=False, elcherpita=False)
                                    .aggregate(Sum('kvanto'))['kvanto__sum']
                            ) or 0
                messages.append(' {}: {}'.format(_('Остаток'), res_count))

                res_reserved = (
                                   # Находим общее количество ресурса, зарезервированное другими заявками
                                   DokumentoKordo.objects
                                       .filter(~Q(dokumento=dokumento),
                                               dokumento__celo_magazeno=dokumento.celo_magazeno,
                                               dokumento__rezervita=True, dokumento__aktiva=True,
                                               dokumento__forigo=False,
                                               resurso=kordo.resurso, aktiva=True, forigo=False)
                                       .aggregate(Sum('kvanto'))['kvanto__sum']
                               ) or 0

                res_reserved += (
                                    KombatantoResurso.objects.select_for_update()
                                        .filter(organizo=dokumento.celo_magazeno, resurso=kordo.resurso, aktiva=True,
                                                forigo=False, rezervita=True)
                                        .aggregate(Sum('kvanto'))['kvanto__sum']
                                ) or 0
                messages.append(' {}: {}'.format(_('Зарезервировано'), res_count))

                if (res_count - res_reserved) < kordo.kvanto:
                    messages.append(' {} {}'.format(_('Превышение остатка на складе на'),
                                                    -1 * (res_count - res_reserved - kordo.kvanto)))
                    result = False

            dokumento.rezervita = True
            dokumento.save()

    return result


# Поиск поставщика ресурса
def provizanta_sercho(resurso):
    prov = KombatantoResursoNomenklaturo.objects.filter(
        organizo__posedanto__kategorioj__kodo='liveranto',
        resurso=resurso,
        aktiva=True,
        forigo=False
    )

    if prov:
        return prov[0].organizo

    return None


# Формируем заявку на закупку
def fari_acheton_peton(request, dokumento, messages=[]):
    result = False
    if dokumento.tipo.kodo == 'provizo':
        try:
            dok_tipo = DokumentoTipo.objects.get(kodo='acheto', aktiva=True, forigo=False)
            dok_statuso = DokumentoStatuso.objects.get(kodo='kreita', aktiva=True, forigo=False)
        except (DokumentoTipo.DoesNotExist, DokumentoStatuso.DoesNotExist):
            messages.append(_('Не найден тип для документа "Заявка на закупку"'))
            return False

        kordoj = DokumentoKordo.objects.select_for_update().filter(dokumento=dokumento, aktiva=True, forigo=False)
        nova_kordoj = {}

        with transaction.atomic():
            for kordo in kordoj:
                messages.append('{}:'.format(get_enhavo(kordo.resurso.nomo, empty_values=True)[0]))
                res_count = (
                                KombatantoResurso.objects.select_for_update()
                                    .filter(organizo=dokumento.celo_magazeno, resurso=kordo.resurso, aktiva=True,
                                            forigo=False, rezervita=False, elcherpita=False)
                                    .aggregate(Sum('kvanto'))['kvanto__sum']
                            ) or 0
                messages.append(' {}: {}'.format(_('Остаток'), res_count))

                res_reserved = (
                                   # Находим общее количество ресурса, зарезервированное другими заявками
                                   DokumentoKordo.objects
                                       .filter(~Q(dokumento=dokumento),
                                               dokumento__celo_magazeno=dokumento.celo_magazeno,
                                               dokumento__rezervita=True, dokumento__aktiva=True,
                                               dokumento__forigo=False,
                                               resurso=kordo.resurso, aktiva=True, forigo=False)
                                       .aggregate(Sum('kvanto'))['kvanto__sum']
                               ) or 0

                res_reserved += (
                                    KombatantoResurso.objects.select_for_update()
                                        .filter(organizo=dokumento.celo_magazeno, resurso=kordo.resurso, aktiva=True,
                                                forigo=False, rezervita=True)
                                        .aggregate(Sum('kvanto'))['kvanto__sum']
                                ) or 0
                messages.append(' {}: {}'.format(_('Зарезервировано'), res_reserved))

                if (res_count - res_reserved) < kordo.kvanto:
                    # Нашли превышение
                    messages.append(' {} {}'.format(_('Превышение остатка на складе на'),
                                                    -1 * (res_count - res_reserved)))
                    # Ищем поставщика ресурса
                    provizanto = provizanta_sercho(kordo.resurso)

                    if provizanto:
                        if provizanto in nova_kordoj:
                            nova_kordoj[provizanto].append((kordo.resurso, kordo.kvanto))
                        else:
                            nova_kordoj[provizanto] = [(kordo.resurso, kordo.kvanto)]
                    else:
                        return result

            if len(nova_kordoj):
                # Формируем Заявку на закупку
                for provizanto, nova_res_kordoj in nova_kordoj.items():
                    with transaction.atomic():
                        nova_dok = Dokumento.objects.create(
                            aktiva=True,
                            forigo=False,
                            tipo=dok_tipo,
                            dato=timezone.now().date(),
                            posedanto_dokumento=dokumento,
                            posedanto=dokumento.celo_organizo,
                            posedanto_magazeno=dokumento.celo_magazeno,
                            celo_organizo=provizanto.posedanto,
                            celo_magazeno=provizanto,
                            kreita_automate=True,
                            statuso=dok_statuso,
                            stadio=1
                        )

                        # Создаём строки новой Заявки
                        for nova_kordo in nova_res_kordoj:
                            res, res_kvanto = nova_kordo
                            DokumentoKordo.objects.create(
                                dokumento=nova_dok,
                                resurso=res,
                                kvanto=res_kvanto,
                                aktiva=True,
                                forigo=False,
                            )
                        ago = automata_serchado(nova_dok)

                        if ago:
                            DokumentoAgo.mutate(nova_dok, request, nova_dok.uuid, ago.kodo, sistema=True)

                result = True

    return result


# Отменяем заявку за закупку
def nuligi_acheton_peton(request, dokumento, messages=[]):
    if dokumento.tipo.kodo == 'provizo':
        with transaction.atomic():
            dokj = Dokumento.objects.select_for_update().filter(
                aktiva=True,
                forigo=False,
                posedanto_dokumento=dokumento,
                tipo__kodo='acheto',
                statuso__kodo__in=['kreita', 'nova', 'en_laboro'],
                stadio__in=[1, 2, 3, 4]
            )

            if dokj:
                statuso = DokumentoStatuso.objects.get(kodo='nova', aktiva=True, forigo=False)

                for dok in dokj:
                    dok.statuso = statuso
                    dok.stadio = 3
                    dok.rezervita = False
                    dok.save()

                    res = DokumentoAgo.mutate(
                        dokumento, request, dok.uuid, 'nuligi',
                        mesago=('{} {} № {} от {}'.format(
                            _('Отмена со стороны родительского документа'),
                            get_enhavo(dokumento.tipo.nomo),
                            dokumento.id,
                            dokumento.dato
                        )
                        ),
                        sistema=True
                    )

            return True

    return False


# Саписывем со склада поставщика и приходуем на склад получатеял
def prenu_liveron(request, dokumento, messages=[]):
    if dokumento.tipo.kodo == 'provizo':
        kordoj = DokumentoKordo.objects.filter(
            aktiva=True,
            forigo=False,
            dokumento=dokumento
        )
        kostoj = {}

        with transaction.atomic():
            # Определяемся, можно ли списать со склада поставщика
            if KombatantoResurso.objects.filter(
                    organizo=dokumento.celo_magazeno,
                    aktiva=True,
                    forigo=False,
                    elcherpita=False
            ).count():
                # Списываем со склада поставщика
                with transaction.atomic():
                    for kordo in kordoj:
                        kostoj[kordo.resurso] = resurso_kosto(dokumento.celo_magazeno, kordo.resurso)
                        KombatantoResurso.objects.create(
                            aktiva=True,
                            forigo=False,
                            organizo=dokumento.celo_magazeno,
                            resurso=kordo.resurso,
                            dokumento=dokumento,
                            kvanto=(-1 * kordo.kvanto),
                            kosto=round(-1 * kostoj[kordo.resurso] * kordo.kvanto, 2),
                            teknika=True,
                        )

            # Приходуем
            for kordo in kordoj:
                # Проверяем наличие в номенклатуре и создаём, если нет
                try:
                    KombatantoResursoNomenklaturo.objects.get(
                        aktiva=True,
                        forigo=False,
                        organizo=dokumento.posedanto_magazeno,
                        resurso=kordo.resurso
                    )
                except KombatantoResursoNomenklaturo.DoesNotExist:
                    stokado = Kombatanto.objects.filter(
                        posedanto=dokumento.posedanto_magazeno, aktiva=True, forigo=False
                    )[0]

                    KombatantoResursoNomenklaturo.objects.create(
                        aktiva=True,
                        forigo=False,
                        organizo=dokumento.posedanto_magazeno,
                        stokado=stokado,
                        resurso=kordo.resurso,
                        minimuma=0,
                        maksimuma=0
                    )

                # Расчитываем сумма поставляемого товара
                kosto = kostoj.get(kordo.resurso, resurso_kosto(dokumento.celo_magazeno, kordo.resurso)) * kordo.kvanto
                KombatantoResurso.objects.create(
                    aktiva=True,
                    forigo=False,
                    organizo=dokumento.posedanto_magazeno,
                    dokumento=dokumento,
                    resurso=kordo.resurso,
                    kvanto=kordo.kvanto,
                    kosto=round(kosto, 2),
                    elcherpita=False
                )

            dokumento.rezervita = False
            dokumento.save()

        return True

    return False


# Заявка на производство
def subres_rezervo(dokumento, rezervo=True):
    if rezervo:
        with transaction.atomic():
            for kordo in DokumentoKordo.objects.filter(
                    Q(resurso__kategorioj__kodo__contains='mikso') | Q(resurso__kategorioj__kodo__contains='pretkonstruitaj'),
                    aktiva=True,
                    forigo=False,
                    dokumento=dokumento,
            ):
                for subres in ResursoMiksajo.objects.filter(
                        aktiva=True,
                        forigo=False,
                        posedanto=kordo.resurso
                ):
                    KombatantoResurso.objects.create(
                        aktiva=True,
                        forigo=False,
                        organizo=dokumento.celo_magazeno,
                        dokumento=dokumento,
                        rezervita=True,
                        teknika=True,
                        resurso=subres.resurso,
                        kvanto=(subres.kbanto * kordo.kvanto),
                        kosto=0
                    )
    else:
        with transaction.atomic():
            KombatantoResurso.objects.select_for_update().filter(
                aktiva=True,
                forigo=False,
                dokumento=dokumento,
                rezervita=True
            ).delete()


# Проверяем и резервирем ресурсы на складе производства
def kontrolu_rezervu_produktadon(request, dokumento, messages=[]):
    result = False
    if dokumento.tipo.kodo == 'produktado':
        result = True
        kordoj = DokumentoKordo.objects.select_for_update().filter(dokumento=dokumento, aktiva=True, forigo=False)
        subres_rezervo(dokumento)

        with transaction.atomic():
            for kordo in kordoj:
                messages.append('{}:'.format(get_enhavo(kordo.resurso.nomo, empty_values=True)[0]))
                res_count = (
                                KombatantoResurso.objects.select_for_update()
                                    .filter(organizo=dokumento.posedanto_magazeno, resurso=kordo.resurso, aktiva=True,
                                            forigo=False, elcherpita=False)
                                    .aggregate(Sum('kvanto'))['kvanto__sum']
                            ) or 0
                messages.append(' {}: {}'.format(_('Остаток'), res_count))

                res_reserved = (
                                   # Находим общее количество ресурса, зарезервированное другими заявками
                                   DokumentoKordo.objects
                                       .filter(~Q(dokumento=dokumento),
                                               dokumento__celo_magazeno=dokumento.posedanto_magazeno,
                                               dokumento__rezervita=True, dokumento__aktiva=True,
                                               dokumento__forigo=False,
                                               resurso=kordo.resurso, aktiva=True, forigo=False)
                                       .aggregate(Sum('kvanto'))['kvanto__sum']
                               ) or 0

                # Находим общее количество ресурса, зарезервированное другими заявками через записис ресурсов
                res_reserved += (
                        KombatantoResurso.objects.filter(
                            ~Q(dokumento=dokumento),
                            aktiva=True,
                            forigo=False,
                            rezervita=True,
                            resurso=kordo.resurso,
                            organizo=dokumento.celo_magazeno
                        ).aggregate(Sum('kvanto'))['kvanto__sum'] or 0
                )

                messages.append(' {}: {}'.format(_('Зарезервировано'), res_count))

                if (res_count - res_reserved) < kordo.kvanto:
                    messages.append(' {} {}'.format(_('Превышение остатка на складе на'),
                                                    -1 * (res_count - res_reserved)))
                    result = False

            dokumento.rezervita = True
            dokumento.save()

    return result


# Формируем Заявки на снабжение
def generi_provizajn_petojn(request, dokumento, messages=[]):
    result = False
    if dokumento.tipo.kodo == 'produktado':
        try:
            dok_tipo = DokumentoTipo.objects.get(kodo='provizo', aktiva=True, forigo=False)
            dok_statuso = DokumentoStatuso.objects.get(kodo='kreita', aktiva=True, forigo=False)
        except (DokumentoTipo.DoesNotExist, DokumentoStatuso.DoesNotExist):
            messages.append(_('Не найден тип для документа "Заявка на снабжение"'))
            return False

        kordoj = DokumentoKordo.objects.select_for_update().filter(
            Q(resurso__kategorioj__kodo__contains='mikso') | Q(resurso__kategorioj__kodo__contains='pretkonstruitaj'),
            dokumento=dokumento, aktiva=True, forigo=False
        )

        resursoj = {}
        nova_kordoj = {}

        with transaction.atomic():
            # Проходим по всем строкам Заявки
            for kordo in kordoj:
                # Определяем ресурсы в составе
                sub_res = ResursoMiksajo.objects.filter(
                    aktiva=True,
                    forigo=False,
                    posedanto=kordo.resurso,
                )

                for res in sub_res:
                    # Суммируем все однотипные ресурсы
                    if res.resurso in resursoj:
                        resursoj[res.resurso] += res.kbanto * kordo.kvanto
                    else:
                        resursoj[res.resurso] = res.kbanto * kordo.kvanto

            # Проверяем наличие ресурсов на складе сырья
            for res, kvanto in resursoj.items():
                messages.append('{}:'.format(get_enhavo(res.nomo, empty_values=True)[0]))
                # Ищем склад нашей организации через наличие ресурса в номенклатуре
                magazenoj = Kombatanto.objects.filter(
                    organizoresursonomenklaturo__organizo__posedanto__propra=True,
                    organizoresursonomenklaturo__organizo__posedanto__aktiva=True,
                    organizoresursonomenklaturo__organizo__posedanto__forigo=False,
                    organizoresursonomenklaturo__aktiva=True,
                    organizoresursonomenklaturo__forigo=False,
                    organizoresursonomenklaturo__resurso=res
                )

                # Иначе ищем любой склад нашей организации
                if not magazenoj:
                    magazenoj = Kombatanto.objects.filter(
                        ~Q(posedanto__kategorioj__kodo__contains='filio'),
                        aktiva=True,
                        forigo=False,
                        tipo__kodo='magazeno',
                        posedanto__propra=True,
                        posedanto__aktiva=True,
                        posedanto__forigo=False,
                    )

                if magazenoj:
                    magazeno = magazenoj[0]
                    messages.append('   {} "{}"'.format(
                        _('Найден склад'),
                        get_enhavo(magazeno.nomo)[0]
                    ))
                else:
                    messages.append(_(' Склад сырья не найден'))
                    return result

                # Склад определен, проверяем остатки на складе производителя и сверяем
                # Находим остаток ресурса
                res_cnt = (
                              KombatantoResurso.objects.filter(
                                  organizo=dokumento.posedanto_magazeno, resurso=res, aktiva=True,
                                  forigo=False, elcherpita=False
                              )
                                  .aggregate(Sum('kvanto'))['kvanto__sum']
                          ) or 0
                messages.append('       {}: {}'.format(_('Остаток'), res_cnt))

                # находим количество зарезервированного другими заявками
                rezervita_cnt = (
                                    DokumentoKordo.objects.filter(
                                        ~Q(dokumento=dokumento),
                                        dokumento__celo_magazeno=dokumento.posedanto_magazeno,
                                        dokumento__rezervita=True, dokumento__aktiva=True,
                                        dokumento__forigo=False,
                                        resurso=res, aktiva=True, forigo=False
                                    )
                                        .aggregate(Sum('kvanto'))['kvanto__sum']
                                ) or 0

                # добавляем количество ресурсов, помеченных как зарезервированные
                rezervita_cnt += (
                                     KombatantoResurso.objects.filter(
                                         ~Q(dokumento=dokumento),
                                         aktiva=True,
                                         forigo=False,
                                         rezervita=True,
                                         resurso=res,
                                         organizo=dokumento.celo_magazeno
                                     )
                                         .aggregate(Sum('kvanto'))['kvanto__sum']
                                 ) or 0
                messages.append('       {}: {}'.format(_('Зарезервировано'), rezervita_cnt))

                if kvanto > (res_cnt - rezervita_cnt):
                    messages.append('       {} {}'.format(_('Превышение остатка на складе на'),
                                                          -1 * (res_cnt - rezervita_cnt - kvanto)))

                    # Добавляем новую строку для заявки на этот склад
                    if magazeno in nova_kordoj:
                        nova_kordoj[magazeno].append((res, kvanto))
                    else:
                        nova_kordoj[magazeno] = [(res, kvanto)]

            # Формируем заявки на снабжение
            for magazeno, nova_kordoj in nova_kordoj.items():
                nova_dok = Dokumento.objects.create(
                    aktiva=True,
                    forigo=False,
                    dato=timezone.now().date(),
                    tipo=dok_tipo,
                    statuso=dok_statuso,
                    stadio=1,
                    posedanto_dokumento=dokumento,
                    posedanto=dokumento.celo_organizo,
                    posedanto_magazeno=dokumento.celo_magazeno,
                    celo_organizo=magazeno.posedanto,
                    celo_magazeno=magazeno,
                    kreita_automate=True
                )

                # Формируем строки заявки
                for kordo in nova_kordoj:
                    res, kvanto = kordo
                    DokumentoKordo.objects.create(
                        aktiva=True,
                        forigo=False,
                        dokumento=nova_dok,
                        resurso=res,
                        kvanto=kvanto,
                        prezo=0
                    )

                ago = automata_serchado(nova_dok)

                if ago:
                    DokumentoAgo.mutate(nova_dok, request, nova_dok.uuid, ago.kodo, sistema=True)

            result = True

    return result


# Отменяем Заявки на снабжение
def nuligi_provizajn_petojn(request, dokumento, messages=[]):
    if dokumento.tipo.kodo == 'produktado':
        with transaction.atomic():
            dokj = Dokumento.objects.select_for_update().filter(
                aktiva=True,
                forigo=False,
                posedanto_dokumento=dokumento,
                tipo__kodo='provizo',
                statuso__kodo__in=['kreita', 'nova', 'en_laboro'],
                stadio__in=[1, 2, 3, 4]
            )

            if dokj:
                statuso = DokumentoStatuso.objects.get(kodo='nova', aktiva=True, forigo=False)

                for dok in dokj:
                    dok.statuso = statuso
                    dok.stadio = 3
                    dok.rezervita = False
                    dok.save()

                    res = DokumentoAgo.mutate(
                        dokumento, request, dok.uuid, 'nuligi',
                        mesago=('{} {} № {} от {}'.format(
                            _('Отмена со стороны родительского документа'),
                            get_enhavo(dokumento.tipo.nomo),
                            dokumento.id,
                            dokumento.dato
                        )
                        ),
                        sistema=True
                    )

            return True

    return False


# Произвести товар
def konduku_varojn(request, dokumento, messages=[]):
    if dokumento.tipo.kodo in ('produktado', 'kompleta_aro'):
        with transaction.atomic():
            kordoj = DokumentoKordo.objects.filter(
                Q(resurso__kategorioj__kodo__contains='mikso') | Q(resurso__kategorioj__kodo__contains='pretkonstruitaj'),
                aktiva=True,
                forigo=False,
                dokumento=dokumento,
            )

            import uuid

            for kordo in kordoj:
                # Определяем суммарную стоимость единицы,
                # а так же списываем ресурсы со склада производителя
                kosto = 0

                for res in ResursoMiksajo.objects.filter(
                        aktiva=True,
                        forigo=False,
                        posedanto=kordo.resurso
                ):
                    res_kosto = resurso_kosto(dokumento.celo_magazeno, res.resurso)
                    kosto += res_kosto * res.kbanto

                    # Списываем ресурсы микса со склада производителя
                    KombatantoResurso.objects.create(
                        aktiva=True,
                        forigo=False,
                        organizo=dokumento.celo_magazeno,
                        dokumento=dokumento,
                        resurso=res.resurso,
                        kvanto=(-1 * res.kbanto * kordo.kvanto),
                        kosto=round(-1 * res_kosto * res.kbanto * kordo.kvanto, 2),
                        teknika=True
                    )

                if kordo.resurso.unuo_mezuro == 'kvanto':
                    # Если ресурс измеряется штуками
                    atributoj = []

                    for i in range(1, int(kordo.kvanto) + 1):
                        atributoj.append(str(uuid.uuid4()))

                    if kordo.atributoj is not None:
                        kordo.atributoj['unika'] = atributoj
                    else:
                        kordo.atributoj = {
                            'unika': atributoj
                        }

                    # Создаём ресурсы на складе производителя
                    for i in range(0, int(kordo.kvanto)):
                        KombatantoResurso.objects.create(
                            uuid=atributoj[i],
                            aktiva=True,
                            forigo=False,
                            organizo=dokumento.celo_magazeno,
                            dokumento=dokumento,
                            resurso=kordo.resurso,
                            kvanto=1,
                            kosto=round(kosto, 2),
                            elcherpita=False
                        )
                else:
                    # Если ресурс не штучный
                    KombatantoResurso.objects.create(
                        aktiva=True,
                        forigo=False,
                        organizo=dokumento.celo_magazeno,
                        dokumento=dokumento,
                        resurso=kordo.resurso,
                        kvanto=kordo.kvanto,
                        kosto=round(kosto * kordo.kvanto, 2),
                        elcherpita=False
                    )

                kordo.prezo = kosto
                kordo.save()

            subres_rezervo(dokumento, False)
            dokumento.rezervita = True
            dokumento.save()

        return True

    return False


# Переместить товар
def movu_varojn(request, dokumento, messages=[]):
    if dokumento.tipo.kodo in ('produktado', 'kompleta_aro'):
        with transaction.atomic():
            # Приходуем все Миксы на склад получателя
            for kordo in DokumentoKordo.objects.filter(
                    aktiva=True,
                    forigo=False,
                    dokumento=dokumento,
                    resurso__kategorioj__kodo__contains='mikso'
            ):
                # Создаём запись в номенклатуре склада
                if not KombatantoResursoNomenklaturo.objects.filter(
                        organizo=dokumento.posedanto_magazeno,
                        aktiva=True,
                        forigo=False,
                        resurso=kordo.resurso
                ):
                    stokado = Kombatanto.objects.filter(posedanto=dokumento.posedanto_magazeno, aktiva=True,
                                                      forigo=False)[0]
                    KombatantoResursoNomenklaturo.objects.create(
                        organizo=dokumento.posedanto_magazeno,
                        aktiva=True,
                        forigo=False,
                        resurso=kordo.resurso,
                        stokado=stokado,
                        minimuma=0,
                        maksimuma=None,
                    )
                if kordo.resurso.unuo_mezuro == 'kvanto':
                    magazeno, aktiva = ((dokumento.posedanto_magazeno, False) if dokumento.tipo.kodo == 'produktado'
                                else (dokumento.celo_magazeno, True))
                    KombatantoResurso.objects.select_for_update().filter(
                        aktiva=aktiva,
                        forigo=False,
                        uuid__in=kordo.atributoj['unika'],
                        organizo=magazeno,
                        resurso=kordo.resurso,
                        teknika=False,
                        rezervita=False
                    ).update(aktiva=True, organizo=dokumento.posedanto_magazeno)
                else:
                    kosto = resurso_kosto(dokumento.celo_magazeno, kordo.resurso)

                    KombatantoResurso.objects.create(
                        aktiva=False,
                        forigo=False,
                        dokumento=dokumento,
                        organizo=dokumento.posedanto_magazeno,
                        resurso=kordo.resurso,
                        kvanto=kordo.kvanto,
                        kosto=kosto,
                        teknika=False,
                        rezervita=False,
                        elcherpita=False
                    )

        return True

    return False


# Заявка на комплектацию
# Проверить и зарезервировать
def kontrolu_rezervu_kompleta_aro(request, dokumento, messages=[]):
    result = False
    if dokumento.tipo.kodo == 'kompleta_aro':
        result = True
        kordoj = DokumentoKordo.objects.select_for_update().filter(
            dokumento=dokumento, aktiva=True, forigo=False)

        resursoj = {}

        with transaction.atomic():
            # Проходим по всем строкам Заявки
            for kordo in kordoj:
                # Определяем ресурсы в составе
                sub_res = ResursoMiksajo.objects.filter(
                    aktiva=True,
                    forigo=False,
                    posedanto=kordo.resurso,
                )

                for res in sub_res:
                    # Суммируем все однотипные ресурсы
                    if res.resurso in resursoj:
                        resursoj[res.resurso] += res.kbanto * kordo.kvanto
                    else:
                        resursoj[res.resurso] = res.kbanto * kordo.kvanto

            # Проверяем наличие ресурсов на складе сырья
            for res, kvanto in resursoj.items():
                messages.append('{}:'.format(get_enhavo(res.nomo, empty_values=True)[0]))

                # Проверяем остатки на складе полуфабрикатов
                # Находим остаток ресурса
                res_cnt = (
                              KombatantoResurso.objects.filter(
                                  organizo=dokumento.celo_magazeno, resurso=res, aktiva=True,
                                  forigo=False, elcherpita=False
                              )
                                  .aggregate(Sum('kvanto'))['kvanto__sum']
                          ) or 0
                messages.append('       {}: {}'.format(_('Остаток'), res_cnt))

                # находим количество зарезервированного другими заявками
                rezervita_cnt = (
                                    DokumentoKordo.objects.filter(
                                        ~Q(dokumento=dokumento),
                                        dokumento__celo_magazeno=dokumento.celo_magazeno,
                                        dokumento__rezervita=True, dokumento__aktiva=True,
                                        dokumento__forigo=False,
                                        resurso=res, aktiva=True, forigo=False
                                    )
                                        .aggregate(Sum('kvanto'))['kvanto__sum']
                                ) or 0

                # добавляем количество ресурсов, помеченных как зарезервированные
                rezervita_cnt += (
                                     KombatantoResurso.objects.filter(
                                         ~Q(dokumento=dokumento),
                                         aktiva=True,
                                         forigo=False,
                                         rezervita=True,
                                         resurso=res,
                                         organizo=dokumento.celo_magazeno
                                     )
                                         .aggregate(Sum('kvanto'))['kvanto__sum']
                                 ) or 0
                messages.append('       {}: {}'.format(_('Зарезервировано'), rezervita_cnt))

                if kvanto > (res_cnt - rezervita_cnt):
                    messages.append('       {} {}'.format(_('Превышение остатка на складе на'),
                                                          -1 * (res_cnt - rezervita_cnt - kvanto)))

                    result = False
                    break

            dokumento.rezervita = True
            dokumento.save()

    return result


# Поиск производителя
def produktanto_sercho(resurso):
    prod = KombatantoResursoNomenklaturo.objects.filter(
        organizo__posedanto__kategorioj__kodo='partnero',
        resurso=resurso,
        aktiva=True,
        forigo=False
    )

    if prod:
        return prod[0].organizo

    return None


# Сформировать заявки на производство
def peti_produktado(request, dokumento, messages=[]):
    result = False
    if dokumento.tipo.kodo == 'kompleta_aro':
        try:
            dok_tipo = DokumentoTipo.objects.get(kodo='produktado', aktiva=True, forigo=False)
            dok_acheto = DokumentoTipo.objects.get(kodo='acheto', aktiva=True, forigo=False)
            dok_statuso = DokumentoStatuso.objects.get(kodo='kreita', aktiva=True, forigo=False)
        except (DokumentoTipo.DoesNotExist, DokumentoStatuso.DoesNotExist):
            messages.append(_('Не найден тип для документа "Заявка на производство"'))
            return False

        kordoj = DokumentoKordo.objects.select_for_update().filter(
            dokumento=dokumento, aktiva=True, forigo=False, resurso__kategorioj__kodo__contains='mikso')
        nova_kordoj = {}
        nova_simpla_kordoj = {}

        with transaction.atomic():
            resursoj = {}

            with transaction.atomic():
                # Проходим по всем строкам Заявки
                for kordo in kordoj:
                    # Определяем ресурсы в составе
                    sub_res = ResursoMiksajo.objects.filter(
                        aktiva=True,
                        forigo=False,
                        posedanto=kordo.resurso,
                    )

                    for res in sub_res:
                        # Суммируем все однотипные ресурсы
                        if res.resurso in resursoj:
                            resursoj[res.resurso] += res.kbanto * kordo.kvanto
                        else:
                            resursoj[res.resurso] = res.kbanto * kordo.kvanto

                # Проверяем наличие ресурсов на складе
                for res, kvanto in resursoj.items():
                    messages.append('{}:'.format(get_enhavo(res.nomo, empty_values=True)[0]))

                    # Проверяем остатки на складе полуфабрикатов
                    # Находим остаток ресурса
                    res_cnt = (
                                  KombatantoResurso.objects.filter(
                                      organizo=dokumento.celo_magazeno, resurso=res, aktiva=True,
                                      forigo=False, elcherpita=False
                                  )
                                      .aggregate(Sum('kvanto'))['kvanto__sum']
                              ) or 0
                    messages.append('       {}: {}'.format(_('Остаток'), res_cnt))

                    # находим количество зарезервированного другими заявками
                    rezervita_cnt = (
                                        DokumentoKordo.objects.filter(
                                            ~Q(dokumento=dokumento),
                                            dokumento__celo_magazeno=dokumento.celo_magazeno,
                                            dokumento__rezervita=True, dokumento__aktiva=True,
                                            dokumento__forigo=False,
                                            resurso=res, aktiva=True, forigo=False
                                        )
                                            .aggregate(Sum('kvanto'))['kvanto__sum']
                                    ) or 0

                    # добавляем количество ресурсов, помеченных как зарезервированные
                    rezervita_cnt += (
                                         KombatantoResurso.objects.filter(
                                             ~Q(dokumento=dokumento),
                                             aktiva=True,
                                             forigo=False,
                                             rezervita=True,
                                             resurso=res,
                                             organizo=dokumento.celo_magazeno
                                         )
                                             .aggregate(Sum('kvanto'))['kvanto__sum']
                                     ) or 0
                    messages.append('       {}: {}'.format(_('Зарезервировано'), rezervita_cnt))

                    if kvanto > (res_cnt - rezervita_cnt):
                        messages.append('       {} {}'.format(_('Превышение остатка на складе на'),
                                                              -1 * (res_cnt - rezervita_cnt - kvanto)))

                        # Добавляем новую строку для заявки на этот склад
                        if res.grupoj.filter(kodo__contains='pretkonstruitaj'):
                            produktanto = produktanto_sercho(res)

                            if produktanto in nova_kordoj:
                                nova_kordoj[produktanto].append((res, kvanto))
                            else:
                                nova_kordoj[produktanto] = [(res, kvanto)]
                        else:
                            provizanto = provizanta_sercho(res)

                            if provizanto in nova_simpla_kordoj:
                                nova_simpla_kordoj[provizanto].append((res, kvanto))
                            else:
                                nova_simpla_kordoj[provizanto] = [(res, kvanto)]

                # Формируем заявки на производство
                for produktanto, nova_kordoj in nova_kordoj.items():
                    nova_dok = Dokumento.objects.create(
                        aktiva=True,
                        forigo=False,
                        dato=timezone.now().date(),
                        tipo=dok_tipo,
                        statuso=dok_statuso,
                        stadio=1,
                        posedanto_dokumento=dokumento,
                        posedanto=dokumento.celo_organizo,
                        posedanto_magazeno=dokumento.celo_magazeno,
                        celo_organizo=produktanto.posedanto,
                        celo_magazeno=produktanto,
                        kreita_automate=True
                    )

                    # Формируем строки заявки
                    for kordo in nova_kordoj:
                        res, kvanto = kordo
                        DokumentoKordo.objects.create(
                            aktiva=True,
                            forigo=False,
                            dokumento=nova_dok,
                            resurso=res,
                            kvanto=kvanto,
                            prezo=0
                        )

                    ago = automata_serchado(nova_dok)

                    if ago:
                        DokumentoAgo.mutate(nova_dok, request, nova_dok.uuid, ago.kodo, sistema=True)

                for provizanto, nova_kordoj in nova_simpla_kordoj.items():
                    nova_dok = Dokumento.objects.create(
                        aktiva=True,
                        forigo=False,
                        dato=timezone.now().date(),
                        tipo=dok_acheto,
                        statuso=dok_statuso,
                        stadio=1,
                        posedanto_dokumento=dokumento,
                        posedanto=dokumento.celo_organizo,
                        posedanto_magazeno=dokumento.celo_magazeno,
                        celo_organizo=provizanto.posedanto,
                        celo_magazeno=provizanto,
                        kreita_automate=True
                    )

                    # Формируем строки заявки
                    for kordo in nova_kordoj:
                        res, kvanto = kordo
                        DokumentoKordo.objects.create(
                            aktiva=True,
                            forigo=False,
                            dokumento=nova_dok,
                            resurso=res,
                            kvanto=kvanto,
                            prezo=0
                        )

                    ago = automata_serchado(nova_dok)

                    if ago:
                        DokumentoAgo.mutate(nova_dok, request, nova_dok.uuid, ago.kodo, sistema=True)

                result = True

    return result


# Отменить заявки на производство
def nuligi_produktado_peton(request, dokumento, messages=[]):
    if dokumento.tipo.kodo == 'kompleta_aro':
        with transaction.atomic():
            dokj = Dokumento.objects.select_for_update().filter(
                Q(tipo__kodo='produktado', stadio__in=[1, 2, 3, 4, 5])
                | Q(tipo__kodo='acheto', stadio__in=[1, 2, 3, 4]),
                aktiva=True,
                forigo=False,
                posedanto_dokumento=dokumento,
                statuso__kodo__in=['kreita', 'nova', 'en_laboro'],
                stadio__in=[1, 2, 3, 4, 5]
            )

            if dokj:
                statuso = DokumentoStatuso.objects.get(kodo='nova', aktiva=True, forigo=False)

                for dok in dokj:
                    dok.statuso = statuso
                    dok.stadio = 3
                    dok.rezervita = False
                    dok.save()

                    res = DokumentoAgo.mutate(
                        dokumento, request, dok.uuid, 'nuligi',
                        mesago=('{} {} № {} от {}'.format(
                            _('Отмена со стороны родительского документа'),
                            get_enhavo(dokumento.tipo.nomo),
                            dokumento.id,
                            dokumento.dato
                        )
                        ),
                        sistema=True
                    )

            return True

    return False


# Заявка на поставку
# Проверить и зарезервировать
def kontrolu_rezervu_liveron(request, dokumento, messages=[]):
    result = False
    if dokumento.tipo.kodo == 'livero':
        result = True
        kordoj = DokumentoKordo.objects.select_for_update().filter(dokumento=dokumento, aktiva=True, forigo=False)

        with transaction.atomic():
            for kordo in kordoj:
                messages.append('{}:'.format(get_enhavo(kordo.resurso.nomo, empty_values=True)[0]))
                res_count = (
                                KombatantoResurso.objects.select_for_update()
                                    .filter(organizo=dokumento.celo_magazeno, resurso=kordo.resurso, aktiva=True,
                                            forigo=False, elcherpita=False)
                                    .aggregate(Sum('kvanto'))['kvanto__sum']
                            ) or 0
                messages.append(' {}: {}'.format(_('Остаток'), res_count))

                res_reserved = (
                                   # Находим общее количество ресурса, зарезервированное другими заявками
                                   DokumentoKordo.objects
                                       .filter(~Q(dokumento=dokumento),
                                               dokumento__celo_magazeno=dokumento.celo_magazeno,
                                               dokumento__rezervita=True, dokumento__aktiva=True,
                                               dokumento__forigo=False,
                                               resurso=kordo.resurso, aktiva=True, forigo=False)
                                       .aggregate(Sum('kvanto'))['kvanto__sum']
                               ) or 0

                # Находим общее количество ресурса, зарезервированное другими заявками через записи ресурсов
                res_reserved += (
                        KombatantoResurso.objects.filter(
                            ~Q(dokumento=dokumento),
                            aktiva=True,
                            forigo=False,
                            rezervita=True,
                            resurso=kordo.resurso,
                            organizo=dokumento.celo_magazeno
                        ).aggregate(Sum('kvanto'))['kvanto__sum'] or 0
                )

                messages.append(' {}: {}'.format(_('Зарезервировано'), res_count))

                if (res_count - res_reserved) < kordo.kvanto:
                    messages.append(' {} {}'.format(_('Превышение остатка на складе на'),
                                                    -1 * (res_count - res_reserved)))
                    result = False

            dokumento.rezervita = True
            dokumento.save()

    return result


# Поиск склада с полуфабрикатами
def pretkonstruitaj_sercho(resurso):
    prod = KombatantoResursoNomenklaturo.objects.filter(
        organizo__posedanto__propra=True,
        resurso=resurso,
        aktiva=True,
        forigo=False
    )

    if prod:
        return prod[0].organizo

    return None


# Сформировать заявки на комплектацию
def petu_kompletan_aron(request, dokumento, messages=[]):
    result = False
    if dokumento.tipo.kodo == 'livero':
        try:
            dok_tipo = DokumentoTipo.objects.get(kodo='kompleta_aro', aktiva=True, forigo=False)
            dok_statuso = DokumentoStatuso.objects.get(kodo='kreita', aktiva=True, forigo=False)
        except (DokumentoTipo.DoesNotExist, DokumentoStatuso.DoesNotExist):
            messages.append(_('Не найден тип для документа "Заявка на комплектацию"'))
            return False

        kordoj = DokumentoKordo.objects.select_for_update().filter(dokumento=dokumento, aktiva=True, forigo=False)
        nova_kordoj = {}

        with transaction.atomic():
            for kordo in kordoj:
                messages.append('{}:'.format(get_enhavo(kordo.resurso.nomo, empty_values=True)[0]))
                res_count = (
                                KombatantoResurso.objects.select_for_update()
                                    .filter(organizo=dokumento.celo_magazeno, resurso=kordo.resurso, aktiva=True,
                                            forigo=False, elcherpita=False)
                                    .aggregate(Sum('kvanto'))['kvanto__sum']
                            ) or 0
                messages.append(' {}: {}'.format(_('Остаток'), res_count))

                res_reserved = (
                                   # Находим общее количество ресурса, зарезервированное другими заявками
                                   DokumentoKordo.objects
                                       .filter(~Q(dokumento=dokumento),
                                               dokumento__celo_magazeno=dokumento.celo_magazeno,
                                               dokumento__rezervita=True, dokumento__aktiva=True,
                                               dokumento__forigo=False,
                                               resurso=kordo.resurso, aktiva=True, forigo=False)
                                       .aggregate(Sum('kvanto'))['kvanto__sum']
                               ) or 0
                messages.append(' {}: {}'.format(_('Зарезервировано'), res_reserved))

                if (res_count - res_reserved) < kordo.kvanto:
                    # Нашли превышение
                    messages.append(' {} {}'.format(_('Превышение остатка на складе на'),
                                                    -1 * (res_count - res_reserved)))
                    # Ищем производителя
                    produktanto = get_dok_agordoj(dok_tipo.kodo, 'celo_magazeno') or dokumento.celo_magazeno #pretkonstruitaj_sercho(kordo.resurso)

                    if produktanto:
                        if produktanto in nova_kordoj:
                            nova_kordoj[produktanto].append((kordo.resurso, kordo.kvanto))
                        else:
                            nova_kordoj[produktanto] = [(kordo.resurso, kordo.kvanto)]
                    else:
                        messages.append('{} "{}" {}'.format(
                            _('Для ресурса'),
                            get_enhavo(kordo.resurso.nomo)[0],
                            _('не удалось определить организацию-производитедля')
                        ))
                        return result

            if len(nova_kordoj):
                # Формируем Заявку на комплектацию
                for produktanto, nova_res_kordoj in nova_kordoj.items():
                    with transaction.atomic():
                        nova_dok = Dokumento.objects.create(
                            aktiva=True,
                            forigo=False,
                            tipo=dok_tipo,
                            dato=timezone.now().date(),
                            posedanto_dokumento=dokumento,
                            posedanto=dokumento.celo_organizo,
                            posedanto_magazeno=dokumento.celo_magazeno,
                            celo_organizo=produktanto.posedanto,
                            celo_magazeno=produktanto,
                            kreita_automate=True,
                            statuso=dok_statuso,
                            stadio=1
                        )

                        # Создаём строки новой Заявки
                        for nova_kordo in nova_res_kordoj:
                            res, res_kvanto = nova_kordo
                            DokumentoKordo.objects.create(
                                dokumento=nova_dok,
                                resurso=res,
                                kvanto=res_kvanto,
                                aktiva=True,
                                forigo=False,
                            )
                        ago = automata_serchado(nova_dok)

                        if ago:
                            DokumentoAgo.mutate(nova_dok, request, nova_dok.uuid, ago.kodo, sistema=True)

                result = True

    return result


# Отменить заявки на комплектацию
def nuligi_roduktado_peton(request, dokumento, messages=[]):
    if dokumento.tipo.kodo == 'livero':
        with transaction.atomic():
            dokj = Dokumento.objects.select_for_update().filter(
                aktiva=True,
                forigo=False,
                posedanto_dokumento=dokumento,
                tipo__kodo='roduktado_peton',
                statuso__kodo__in=['kreita', 'nova', 'en_laboro'],
                stadio__in=[1, 2, 3, 4, 5]
            )

            if dokj:
                statuso = DokumentoStatuso.objects.get(kodo='nova', aktiva=True, forigo=False)

                for dok in dokj:
                    dok.statuso = statuso
                    dok.stadio = 3
                    dok.rezervita = False
                    dok.save()

                    res = DokumentoAgo.mutate(
                        dokumento, request, dok.uuid, 'nuligi',
                        mesago=('{} {} № {} от {}'.format(
                            _('Отмена со стороны родительского документа'),
                            get_enhavo(dokumento.tipo.nomo),
                            dokumento.id,
                            dokumento.dato
                        )
                        ),
                        sistema=True
                    )

            return True

    return False


# Проверка сборки
def kontrolu_konstrui(request, dokumento, messages=[]):
    result = False
    unika_nomo = 'rezervita_unika' if dokumento.tipo.kodo == 'livero' else 'unika'

    for kordo in DokumentoKordo.objects.filter(
            Q(resurso__kategorioj__kodo__contains='mikso') | Q(resurso__kategorioj__kodo__contains='pretkonstruitaj'),
            aktiva=True,
            forigo=False,
            dokumento=dokumento,
    ):
        messages.append('{} {}:'.format(_('Строка'), kordo.id))

        if kordo.atributoj:
            print(kordo.atributoj, kordo.kvanto)
            if (unika_nomo in kordo.atributoj and len(kordo.atributoj[unika_nomo]) < int(kordo.kvanto)
                    or unika_nomo not in kordo.atributoj):
                messages.append('   {}: {}/{}'.format(
                    get_enhavo(kordo.resurso.nomo),
                    len(kordo.atributoj[unika_nomo]),
                    int(kordo.kvanto)
                ))
                result = False
            elif unika_nomo in kordo.atributoj:
                messages.append('   {}: {}/{}'.format(
                    get_enhavo(kordo.resurso.nomo),
                    len(kordo.atributoj[unika_nomo]),
                    int(kordo.kvanto)
                ))
                result = True

    return result
