"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django import forms
from siriuso.utils import get_enhavo
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
import json

from .models import *


class DokTipoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Название'), required=True)

    class Meta:
        model = DokumentoTipo
        fields = [field.name for field in DokumentoTipo._meta.fields if field.name not in ('uuid', 'krea_dato')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


@admin.register(DokumentoTipo)
class DokumentoTipoAdmin(admin.ModelAdmin):
    form = DokTipoFormo
    list_display = ('nomo_enhavo', 'forigo')

    class Meta:
        model = DokumentoTipo

    def nomo_enhavo(self, obj):
        return get_enhavo(obj.nomo, empty_values=True)[0]
    nomo_enhavo.short_description = _('Наименование')


class DokStatusoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Название'), required=True)

    class Meta:
        model = DokumentoStatuso
        fields = [field.name for field in DokumentoStatuso._meta.fields if field.name not in ('uuid', 'krea_dato')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


@admin.register(DokumentoStatuso)
class DokumentoStatusoAdmin(admin.ModelAdmin):
    form = DokTipoFormo
    list_display = ('nomo_enhavo', 'forigo')

    class Meta:
        model = DokumentoStatuso

    def nomo_enhavo(self, obj):
        return get_enhavo(obj.nomo, empty_values=True)[0]
    nomo_enhavo.short_description = _('Наименование')


class DokSkemoFormo(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['ago_kodo'].strip = False

    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Название действия'), required=True)
    informoj = forms.CharField(widget=LingvoInputWidget(), label=_('Описание этапа'), required=True)
    fina_informoj = forms.CharField(widget=LingvoInputWidget(), label=_('Сообщение положительного результата'),
                                    required=True)

    eraro_informoj = forms.CharField(widget=LingvoInputWidget(), label=_('Сообщение отрицательного результата'),
                                     required=True)

    class Meta:
        model = DokumentoSkemo
        fields = [field.name for field in DokumentoSkemo._meta.fields if field.name not in ('uuid', 'krea_dato')]\
                 + ['roloj',]

    def clean_informoj(self):
        out = self.cleaned_data['informoj']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_fina_informoj(self):
        out = self.cleaned_data['fina_informoj']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_eraro_informoj(self):
        out = self.cleaned_data['eraro_informoj']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


@admin.register(DokumentoSkemo)
class DokumentoSkemoAdmin(admin.ModelAdmin):
    form = DokSkemoFormo
    list_display = ('informoj_enhavo', 'dokumento_nomo', 'komenca_statuso', 'fina_statuso', 'eraro_statuso',
                    'krea_dato')
    list_filter = ('dokumento_tipo',)
    filter_horizontal = ('roloj',)

    class Meta:
        model = DokumentoSkemo

    def informoj_enhavo(self, obj):
        return get_enhavo(obj.informoj, empty_values=True)[0]
    informoj_enhavo.short_description = _('Описание')

    def dokumento_nomo(self, obj):
        return get_enhavo(obj.dokumento_tipo.nomo, empty_values=True)[0]
    dokumento_nomo.short_description = _('Дополнительная информация')


class DokFormo(forms.ModelForm):
    informoj = forms.CharField(widget=LingvoTextWidget(), label=_('Описание'), required=True)

    class Meta:
        model = Dokumento
        fields = [field.name for field in Dokumento._meta.fields if field.name not in ('uuid', 'id', 'krea_dato')]

    def clean_informoj(self):
        out = self.cleaned_data['informoj']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


class DokumentoKordoInline(admin.TabularInline):
    model = DokumentoKordo
    extra = 1
    #exclude = ('uuid', )
    readonly_fields = ('uuid',)


@admin.register(Dokumento)
class DokumentoAdmin(admin.ModelAdmin):
    form = DokFormo
    list_display = ('id', 'tipo_enhavo',  'publikigo', 'forigo', 'krea_dato')
    list_display_links = ('id', 'tipo_enhavo')
    search_fields = ('id', 'tipo__nomo')
    inlines = (DokumentoKordoInline,)

    class Meta:
        model = Dokumento

    def informoj_enhavo(self, obj):
        return get_enhavo(obj.informoj, empty_values=True)[0]
    informoj_enhavo.short_description = _('Дополнительная информация')

    def tipo_enhavo(self, obj):
        return get_enhavo(obj.tipo.nomo, empty_values=True)[0]
    tipo_enhavo.short_description = _('Типа документа')


@admin.register(DokumentoAgordo)
class DokumentoAgordoAdmin(admin.ModelAdmin):
    list_display = ('dokumento_tipo_nomo', 'publikigo', 'forigo', 'krea_dato')
    list_display_links = ('dokumento_tipo_nomo',)
    search_fields = ('dokumento_tipo_kodo', 'dokumento_tipo__nomo__enhavo')

    class Meta:
        model = DokumentoAgordo

    def dokumento_tipo_nomo(self, obj):
        return get_enhavo(obj.dokumento_tipo.nomo, empty_values=True)[0]
    dokumento_tipo_nomo.short_description = _('Тип документа')
