"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from celery import shared_task
from django.db.models import Q, Sum
from django.db import transaction
from django.utils import timezone

from kombatantoj.models import KombatantoResursoNomenklaturo, KombatantoResurso, Kombatanto
from resursoj.models import Resurso
from dokumentoj.models import *
from dokumentoj.api.mutation import DokumentoAgo
from .utils import get_dok_agordoj


# Ищем автоматическое действие
def automata_serchado(dokumento):
    try:
        skemo = DokumentoSkemo.objects.get(dokumento_tipo=dokumento.tipo, aktiva=True, forigo=False,
                                           komenca_statuso=dokumento.statuso, komenca_stadio=dokumento.stadio,
                                           automata=True)
        return skemo
    except DokumentoSkemo.DoesNotExist:
        return None


# Поиск поставщика ресурса
def provizanta_sercho(resurso):
    prov = KombatantoResursoNomenklaturo.objects.filter(
        organizo__posedanto__grupoj__kodo='liveranto',
        resurso=resurso,
        aktiva=True,
        forigo=False
    )

    if prov:
        return prov[0].organizo

    return None


def get_ost(org, res):
    # Собираем баланс ресурсов
    res_cnt = (
                  KombatantoResurso.objects.filter(
                      organizo=org,
                      aktiva=True,
                      forigo=False,
                      resurso=res,
                      rezervita=False,
                      elcherpita=False
                  ).aggregate(Sum('kvanto'))['kvanto__sum']
              ) or 0

    res_cnt += (
                   DokumentoKordo.objects.filter(
                       dokumento__posedanto_magazeno=org,
                       dokumento__statuso__kodo__in=['nova', 'en_laboro'],
                       dokumento__rezervita=False,
                       dokumento__aktiva=True,
                       dokumento__forigo=False,
                       resurso=res
                   ).aggregate(Sum('kvanto'))['kvanto__sum']
               ) or 0

    # Определяем зарезервированные
    rezervita_cnt = (
                        DokumentoKordo.objects.filter(
                            dokumento__celo_magazeno=org,
                            dokumento__rezervita=True,
                            dokumento__aktiva=True,
                            dokumento__forigo=False,
                            resurso=res
                        ).aggregate(Sum('kvanto'))['kvanto__sum']
                    ) or 0

    rezervita_cnt += (
                         KombatantoResurso.objects.filter(
                             organizo=org,
                             aktiva=True,
                             forigo=False,
                             resurso=res,
                             rezervita=True
                         ).aggregate(Sum('kvanto'))['kvanto__sum']
                     ) or 0

    return round(res_cnt - rezervita_cnt, 3)


@shared_task
def achetokondicho():
    # Ищем строки номенклатуры для наших организаций с ненулевыми значениями минимального количества
    nomoj = (Kombatanto.objects.filter(
        ~Q(organizoresursonomenklaturo__resurso__kategorioj__kodo__contains='mikso'),
        organizoresursonomenklaturo__aktiva=True, organizoresursonomenklaturo__forigo=False,
        organizoresursonomenklaturo__minimuma__gt=0, posedanto__propra=True).distinct()
    )

    resursoj = {}

    # Проходимся по всем организациям и формируем общий список
    for nom in nomoj:
        for res in KombatantoResursoNomenklaturo.objects.filter(
            ~Q(resurso__kategorioj__kodo__contains='mikso'),
            aktiva=True, forigo=False, organizo=nom, minimuma__gt=0
        ):
            # Проверяем остатки
            ost = get_ost(nom, res.resurso)

            if ost < res.minimuma:
                cnt = round((res.maksimuma - ost) if res.maksimuma else (res.minimuma - ost), 3)

                # Ищем поставщика ресурса
                provizanto = provizanta_sercho(res.resurso)

                if provizanto:
                    if nom not in resursoj:
                        resursoj[nom] = {
                            provizanto: [(res.resurso, cnt)]
                        }
                    else:
                        if provizanto not in resursoj[nom]:
                            resursoj[nom][provizanto] = [(res.resurso, cnt)]
                        else:
                            resursoj[nom][provizanto].append((res.resurso, cnt))

    if len(resursoj):
        # Формируем Заявки на закупку
        dok_tipo = DokumentoTipo.objects.get(kodo='acheto', aktiva=True, forigo=False)
        dok_statuso = DokumentoStatuso.objects.get(kodo='kreita', aktiva=True, forigo=False)

        for posedanto, resursoj_listo in resursoj.items():
            for provizanto, nova_res_kordoj in resursoj_listo.items():
                with transaction.atomic():
                    nova_dok = Dokumento.objects.create(
                        aktiva=True,
                        forigo=False,
                        tipo=dok_tipo,
                        dato=timezone.now().date(),
                        posedanto_dokumento=None,
                        posedanto=posedanto.posedanto,
                        posedanto_magazeno=posedanto,
                        celo_organizo=provizanto.posedanto,
                        celo_magazeno=provizanto,
                        kreita_automate=True,
                        statuso=dok_statuso,
                        stadio=1
                    )

                    # Создаём строки новой Заявки
                    for nova_kordo in nova_res_kordoj:
                        res, res_kvanto = nova_kordo
                        DokumentoKordo.objects.create(
                            dokumento=nova_dok,
                            resurso=res,
                            kvanto=res_kvanto,
                            aktiva=True,
                            forigo=False,
                        )
                    ago = automata_serchado(nova_dok)

                    if ago:
                        DokumentoAgo.mutate(nova_dok, None, nova_dok.uuid, ago.kodo, sistema=True)


# Поиск поставщика ресурса
def provizanta_mikso_sercho(resurso):
    prov = KombatantoResursoNomenklaturo.objects.filter(
        ~Q(organizo__grupoj__kodo__contains='kliento'),
        organizo__posedanto__propra=True,
        resurso=resurso,
        resurso__kategorioj__kodo__contains='mikso',
        aktiva=True,
        forigo=False
    )

    if prov:
        return prov[0].organizo

    return None


@shared_task
def peto_por_livero(min, magazenoj=None):
    # Ищем строки номенклатуры для наших организаций с ненулевыми значениями минимального количества
    nomoj = (Kombatanto.objects.filter(
        organizoresursonomenklaturo__resurso__kategorioj__kodo__contains='mikso',
        organizoresursonomenklaturo__aktiva=True, organizoresursonomenklaturo__forigo=False,
        organizoresursonomenklaturo__minimuma__gt=0, posedanto__grupoj__kodo__contains='kliento').distinct())

    resursoj = {}

    # Проходимся по всем организациям-клиентам и формируем общий список
    for nom in nomoj:
        nom_resursoj = {}
        nom_cnt = 0

        for res in KombatantoResursoNomenklaturo.objects.filter(
            resurso__kategorioj__kodo__contains='mikso',
            aktiva=True, forigo=False, organizo=nom, minimuma__gt=0
        ):
            # Проверяем остатки
            ost = get_ost(nom, res.resurso)

            if ost < res.minimuma:
                cnt = round((res.maksimuma - ost) if res.maksimuma else (res.minimuma - ost), 3)

                # Ищем поставщика ресурса
                provizanto = get_dok_agordoj('livero', 'celo_magazeno') or provizanta_mikso_sercho(res.resurso)

                if isinstance(magazenoj, (list, tuple)) and str(provizanto.uuid) not in magazenoj:
                    provizanto = None

                if provizanto:
                    nom_cnt += cnt
                    if provizanto not in nom_resursoj:
                        nom_resursoj[provizanto] = [(res.resurso, cnt)]
                    else:
                        nom_resursoj[provizanto].append((res.resurso, cnt))

        if nom_cnt >= min:
            resursoj[nom] = nom_resursoj

    if len(resursoj):
        # Формируем Заявки на поставку
        dok_tipo = DokumentoTipo.objects.get(kodo='livero', aktiva=True, forigo=False)
        dok_statuso = DokumentoStatuso.objects.get(kodo='kreita', aktiva=True, forigo=False)

        for posedanto, resursoj_listo in resursoj.items():
            for provizanto, nova_res_kordoj in resursoj_listo.items():
                with transaction.atomic():
                    nova_dok = Dokumento.objects.create(
                        aktiva=True,
                        forigo=False,
                        tipo=dok_tipo,
                        dato=timezone.now().date(),
                        posedanto_dokumento=None,
                        posedanto=posedanto.posedanto,
                        posedanto_magazeno=posedanto,
                        celo_organizo=provizanto.posedanto,
                        celo_magazeno=provizanto,
                        kreita_automate=True,
                        statuso=dok_statuso,
                        stadio=1
                    )

                    # Создаём строки новой Заявки
                    for nova_kordo in nova_res_kordoj:
                        res, res_kvanto = nova_kordo
                        DokumentoKordo.objects.create(
                            dokumento=nova_dok,
                            resurso=res,
                            kvanto=res_kvanto,
                            prezo=(res.podetala_prezo or 0),
                            aktiva=True,
                            forigo=False,
                        )
                    ago = automata_serchado(nova_dok)

                    if ago:
                        DokumentoAgo.mutate(nova_dok, None, nova_dok.uuid, ago.kodo, sistema=True)
