"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

import graphene
import re
from graphene_django import DjangoObjectType
from django.db.models import Q, F
from django_filters.utils import timezone
from siriuso.api.filters import SiriusoFilterConnectionField
from siriuso.api.types import SiriusoLingvo
from siriuso.api.mixins import SiriusoObjectId
from siriuso.utils import get_enhavo, set_enhavo
from kombatantoj.api.scheme import KombatantoNode, KombatantoInterna
import itertools

from ..models import *


class DokumentoTipoNode(DjangoObjectType):
    search_fields = (
        'uuid', 'nomo__enhavo__icontains'
    )
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains']
    }

    nomo = graphene.Field(SiriusoLingvo)

    class Meta:
        model = DokumentoTipo
        filter_fields = {
            'uuid': ['exact', 'ne', 'in', 'not_in'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


class DokumentoStatusoNode(DjangoObjectType):
    search_fields = (
        'uuid', 'nomo__enhavo__icontains'
    )
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains']
    }

    nomo = graphene.Field(SiriusoLingvo)

    class Meta:
        model = DokumentoStatuso
        filter_fields = {
            'uuid': ['exact', 'ne', 'in', 'not_in'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)

    @staticmethod
    def resolve_bildo(root, info, **kwargs):
        bildo = getattr(root, 'bildo')
        if re.search(r'^/static/', str(bildo)):
            image = bildo.name
        else:
            image = getattr(bildo, 'url') if bildo else None

        if image:
            return info.context.build_absolute_uri(image) if image else None

        return None


class DokumentoSkemoNode(DjangoObjectType):
    search_fields = (
        'uuid',
        'dokumento_tipo__nomo__enhavo__icontains',
        'komenca_statuso__nomo__enhavo__icontains',
        'fina_statuso__nomo__enhavo__icontains',
        'eraro_statuso__nomo__enhavo__icontains',
    )

    nomo = graphene.Field(SiriusoLingvo)
    informoj = graphene.Field(SiriusoLingvo)
    fina_informoj = graphene.Field(SiriusoLingvo)
    eraro_informoj = graphene.Field(SiriusoLingvo)

    class Meta:
        model = DokumentoSkemo
        filter_fields = {
            'uuid': ['exact', 'ne', 'in', 'not_in'],
        }
        interfaces = (graphene.relay.Node,)


class DokumentoKordoNode(SiriusoObjectId, DjangoObjectType):
    class Meta:
        model = DokumentoKordo
        filter_fields = {
            'uuid': ['exact', 'ne', 'in', 'not_in'],
            'dokumento_id': ['exact'],
            'forigo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


class DokumentoHistorioNode(SiriusoObjectId, DjangoObjectType):
    autoro = graphene.Field(KombatantoNode)

    class Meta:
        model = DokumentoHistorio
        filter_fields = {
            'uuid': ['exact'],
            'dokumento__uuid': ['exact'],
        }
        interfaces = (graphene.relay.Node,)

    @staticmethod
    def resolve_autoro(root, info, **kwargs):
        try:
            homo = Kombatanto.objects.get(uzanto=root.autoro)
            return homo
        except Kombatanto.DoesNotExist:
            return None


class DokumentoNode(SiriusoObjectId, DjangoObjectType):
    search_fields = (
        'uuid', 'id', 'informoj__enhavo__icontains',
        'tipo__nomo__enhavo__icontains',
    )
    json_filter_fields = {
        'informoj': ['contains', 'icontains'],
    }

    informoj = graphene.Field(SiriusoLingvo)
    kordoj = SiriusoFilterConnectionField(DokumentoKordoNode)
    agoj = graphene.List(DokumentoSkemoNode)
    historio = graphene.List(DokumentoHistorioNode)
    autoro = graphene.Field(KombatantoNode)

    class Meta:
        model = Dokumento
        filter_fields = {
            'hid': ['exact', 'ne', 'in', 'not_in'],
            'tipo__uuid': ['exact', 'ne', 'in', 'not_in'],
            'uuid': ['exact', 'ne', 'in', 'not_in'],
            'forigo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)

    @staticmethod
    def resolve_autoro(root, info, **kwargs):
        try:
            homo = Kombatanto.objects.get(uzanto=root.autoro)
            return homo
        except (Kombatanto.DoesNotExist, Kombatanto.MultipleObjectsReturned):
            return None

    @staticmethod
    def resolve_historio(root, info, **kwargs):
        first = DokumentoHistorio.objects.filter(
            dokumento=root,
            publikigo=True,
            forigo=False,
        ).order_by('id')
        agoj_uuid = []

        def get_second(last=None):
            id = last.id + 1
            dok_tipo = last.dokumento.tipo
            cur_skemo = last.skemo
            out = []

            while 1:
                cur_skemo = DokumentoSkemo.objects.filter(#~Q(fina_stadio=F('komenca_stadio')),
                                                          escepta=False, dokumento_tipo=dok_tipo,
                                                          publikigo=True, forigo=False,
                                                          komenca_statuso=cur_skemo.fina_statuso,
                                                          komenca_stadio=cur_skemo.fina_stadio)

                if not cur_skemo:
                    break

                for skemo in cur_skemo:
                    if skemo.uuid not in agoj_uuid:
                        cur_skemo = skemo
                        agoj_uuid.append(skemo.uuid)

                komenca_dato = last.fina_dato if first and not len(out) else None

                out.append(
                    DokumentoHistorio(id=id, dokumento=last.dokumento, skemo=cur_skemo, komenco_dato=komenca_dato,
                                      publikigo=True, forigo=False)
                )
                id = id + 1

            return out

        if not first:
            try:
                first_skemo = DokumentoSkemo.objects.get(~Q(fina_statuso__kodo='nuligita'), dokumento_tipo=root.tipo,
                                                         publikigo=True, forigo=False, komenca_stadio=1)
                first = DokumentoHistorio(id=1, dokumento=root, skemo=first_skemo, publikigo=True, forigo=False)
                return [first] + get_second(first)
            except DokumentoSkemo.DoesNotExist:
                return list()

        return list(itertools.chain(first, [])) + get_second(first.reverse()[0])

    @staticmethod
    def resolve_kordoj(root, info, **kwargs):
        return DokumentoKordo.objects.filter(dokumento=root)

    @staticmethod
    def resolve_agoj(root, info, **kwargs):
        user = info.context.user

        try:
            homo = Kombatanto.objects.get(uzanto=user, publikigo=True, forigo=False)

            if not user.is_superuser:
                cond = Q(posedanto_ago=False, kontr_ago=False, respondeca_ago=False)

                if root.posedanto == homo.organizo or root.posedanto_magazeno == homo.organizo:
                    cond |= Q(posedanto_ago=True)
                elif root.celo_organizo == homo.organizo or root.celo_magazeno == homo.organizo:
                    cond |= Q(kontr_ago=True)
                elif root.respondeca == homo:
                    cond |= Q(respondeca_ago=True)
            else:
                cond = models.Q()

            try:
                roloj = KombatantoInterna.objects.get(posedanto=root.posedanto, kombatanto=homo).roloj.all()
            except KombatantoInterna.DoesNotExist:
                roloj = KombatantoRolo.objects.none()

            return (DokumentoSkemo.objects.filter(cond, dokumento_tipo=root.tipo, roloj__contains=roloj,
                                                 komenca_statuso=root.statuso, komenca_stadio=root.stadio,
                                                 forigo=False, publikigo=True, automata=False)
                    .distinct().order_by('-escepta'))
        except Kombatanto.DoesNotExist:
            pass

        return DokumentoSkemo.objects.none()


class DokumentoAgordoNode(DjangoObjectType):
    class Meta:
        model = DokumentoAgordo
        filter_fields = {
            'dokumento_tipo__uuid': ['exact', 'ne', 'in', 'not_in'],
            'uuid': ['exact', 'ne', 'in', 'not_in'],
            'forigo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


class DokumentoQuery(graphene.ObjectType):
    dokumentoj_tipoj = SiriusoFilterConnectionField(DokumentoTipoNode)
    dokumentoj_statusoj = SiriusoFilterConnectionField(DokumentoStatusoNode)
    dokumentoj_skemoj = SiriusoFilterConnectionField(DokumentoSkemoNode)
    dokumentoj = SiriusoFilterConnectionField(DokumentoNode)
    dokumentoj_agordoj = SiriusoFilterConnectionField(DokumentoAgordoNode)
