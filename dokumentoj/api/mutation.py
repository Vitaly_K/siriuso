"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.db import transaction
from django.db.models import Q
import graphene

from siriuso.api.types import ErrorNode
from siriuso.utils import set_enhavo
from siriuso.utils.thumbnailer import get_image_properties, get_web_image
from .scheme import (DokumentoTipoNode, DokumentoStatusoNode, DokumentoSkemoNode,
                     DokumentoNode, DokumentoKordoNode, DokumentoAgordoNode)

from ..models import *
from kombatantoj.models import Kombatanto, KombatantoResurso
from resursoj.models import Resurso, ResursoOpcion
from kombatantoj.api.scheme import KombatantoResursoNode


# Ищем автоматическое действие
def automata_serchado(dokumento):
    try:
        skemo = DokumentoSkemo.objects.get(dokumento_tipo=dokumento.tipo, publikigo=True, forigo=False,
                                           komenca_statuso=dokumento.statuso, komenca_stadio=dokumento.stadio,
                                           automata=True)
        return skemo
    except DokumentoSkemo.DoesNotExist:
        return None


class RedaktuDokumentoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    dokumento_tipo = graphene.Field(DokumentoTipoNode)

    class Arguments:
        uuid = graphene.UUID()
        kodo = graphene.String()
        nomo = graphene.String()
        publikigo = graphene.Boolean()
        forigo = graphene.Boolean()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False,
        message = None
        errors = []
        dokumento_tipo = None
        user = info.context.user

        if (user.has_perm('dokumentoj.add_dokumentotipo')
                or user.has_perm('dokumentoj.change_dokumentotipo')
                or user.has_perm('dokumentoj.delete_dokumentotipo')):

            # Создание записи
            if 'uuid' not in kwargs:
                if user.has_perm('dokumentoj.add_dokumentotipo'):
                    if 'forigo' in kwargs and kwargs.get('forigo'):
                        errors.append(
                            ErrorNode(
                                field='forigo',
                                message=_('Нельзя удалить не созданную запись')
                            )
                        )

                    if not kwargs.get('kodo', False):
                        errors.append(
                            ErrorNode(
                                field='kodo',
                                message=_('Поле обязательно для заполнения')
                            )
                        )

                    if not kwargs.get('nomo', False):
                        errors.append(
                            ErrorNode(
                                field='nomo',
                                message=_('Поле обязательно для заполнения')
                            )
                        )

                    if not len(errors):
                        dokumento_tipo = DokumentoTipo.objects.create(
                            kodo=kwargs.get('kodo'),
                            publikigo=kwargs.get('publikigo', False),
                            forigo=kwargs.get('forigo', False)
                        )
                        set_enhavo(dokumento_tipo.nomo, kwargs.get('nomo'), 'ru_RU')

                        dokumento_tipo.save()
                        status = True
                        message = _('Запись успешна создана')
                    else:
                        message = _('Неверные значения аргументов')
                else:
                    message = _('Недостаточно прав на создание записи')
            else:
                # Изменение записи
                if len(kwargs) > 1:
                    if kwargs.get('forigo', False) and not user.has_perm('dokumentoj.delete_dokumentotipo'):
                        message=_('Недостаточно прав для удаления записи')
                    elif (not user.has_perm('dokumentoj.change_dokumentotipo')
                          and ('kodo' in kwargs or 'nomo' in kwargs or 'publikigo' in kwargs)):
                        message = _('Недостаточно прав для изменения ресурса')
                    else:
                        try:
                            dokumento_tipo = DokumentoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                        except DokumentoTipo.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='uuid',
                                    message=_('Запись не найдена')
                                )
                            )

                        if 'kodo' in kwargs and not kwargs.get('kodo'):
                            errors.append(
                                ErrorNode(
                                    field='kodo',
                                    message=_('Поле не может быть пустым')
                                )
                            )

                        if 'nomo' in kwargs and not kwargs.get('nomo'):
                            errors.append(
                                ErrorNode(
                                    field='nomo',
                                    message=_('Поле не может быть пустым')
                                )
                            )

                        if not len(errors):
                            dokumento_tipo.kodo = kwargs.get('kodo', dokumento_tipo.kodo)
                            dokumento_tipo.publikigo = kwargs.get('publikigo', dokumento_tipo.publikigo)
                            dokumento_tipo.forigo = kwargs.get('forigo', dokumento_tipo.forigo)
                            dokumento_tipo.lasta_autoro = user
                            dokumento_tipo.lasta_dato = timezone.now()

                            if kwargs.get('nomo'):
                                set_enhavo(dokumento_tipo.nomo, kwargs.get('nomo'), 'ru_RU')

                            dokumento_tipo.save()
                            status = True
                            message = _('Запись успешно изменена')

                        else:
                            dokumento_tipo = None
                            message = _('Неверные значения аргументов')
                else:
                    message = _('Нет аргументов для изменения')

        else:
            message = _('Недостаточно прав')

        return RedaktuDokumentoTipo(status=status, message=message, errors=errors, dokumento_tipo=dokumento_tipo)


class RedaktuDokumentoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    dokumento_statuso = graphene.Field(DokumentoStatusoNode)

    class Arguments:
        uuid = graphene.UUID()
        kodo = graphene.String()
        nomo = graphene.String()
        bildo = graphene.String()
        icon_kodo = graphene.String()
        rezervo = graphene.Boolean()
        publikigo = graphene.Boolean()
        forigo = graphene.Boolean()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False,
        message = None
        errors = []
        bildo = None
        dokumento_statuso = None
        user = info.context.user

        if (user.has_perm('dokumentoj.add_dokumentostatuso')
                or user.has_perm('dokumentoj.change_dokumentostatuso')
                or user.has_perm('dokumentoj.delete_dokumentostatuso')):

            # Создание записи
            if 'uuid' not in kwargs:
                if user.has_perm('dokumentoj.add_dokumentostatuso'):
                    if 'forigo' in kwargs and kwargs.get('forigo'):
                        errors.append(
                            ErrorNode(
                                field='forigo',
                                message=_('Нельзя удалить не созданную запись')
                            )
                        )

                    if not kwargs.get('kodo', False):
                        errors.append(
                            ErrorNode(
                                field='kodo',
                                message=_('Поле обязательно для заполнения')
                            )
                        )

                    if not kwargs.get('nomo', False):
                        errors.append(
                            ErrorNode(
                                field='nomo',
                                message=_('Поле обязательно для заполнения')
                            )
                        )

                    if 'bildo' in info.context.FILES:
                        bildo = get_web_image(info.context.FILES['bildo'])
                        props = get_image_properties(bildo)

                        if props and (props['width'] >= '64' or props['height'] >=64):
                            errors.append(
                                ErrorNode(
                                    field='bildo',
                                    message=_('Размер картинки не должен привышать')
                                )
                            )

                    if not len(errors):
                        dokumento_statuso = DokumentoStatuso.objects.create(
                            kodo=kwargs.get('kodo'),
                            rezervo=kwargs.get('rezervo', False),
                            icon_kodo=kwargs.get('icon_kodo', None),
                            publikigo=kwargs.get('publikigo', False),
                            forigo=kwargs.get('forigo', False)
                        )
                        set_enhavo(dokumento_statuso.nomo, kwargs.get('nomo'), 'ru_RU')

                        if bildo:
                            dokumento_statuso.bildo = bildo

                        dokumento_statuso.save()
                        status = True
                        message = _('Запись успешна создана')
                    else:
                        message = _('Неверные значения аргументов')
                else:
                    message = _('Недостаточно прав на создание записи')
            else:
                # Изменение записи
                if len(kwargs) > 1:
                    if kwargs.get('forigo', False) and not user.has_perm('dokumentoj.delete_dokumentostatuso'):
                        message = _('Недостаточно прав для удаления записи')
                    elif (not user.has_perm('dokumentoj.change_dokumentotipo')
                          and ('kodo' in kwargs or 'nomo' in kwargs or 'icon_kodo' in kwargs
                               or 'publikigo' in kwargs or 'rezervo' in kwargs or 'bildo' in kwargs
                               or 'bildo' in info.context.FILES)):
                        message = _('Недостаточно прав для изменения ресурса')
                    else:
                        try:
                            dokumento_statuso = DokumentoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                        except DokumentoStatuso.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='uuid',
                                    message=_('Запись не найдена')
                                )
                            )

                        if 'kodo' in kwargs and not kwargs.get('kodo'):
                            errors.append(
                                ErrorNode(
                                    field='kodo',
                                    message=_('Поле не может быть пустым')
                                )
                            )

                        if 'nomo' in kwargs and not kwargs.get('nomo'):
                            errors.append(
                                ErrorNode(
                                    field='nomo',
                                    message=_('Поле не может быть пустым')
                                )
                            )

                        if 'bildo' in info.context.FILES:
                            bildo = get_web_image(info.context.FILES['bildo'])
                            props = get_image_properties(bildo)

                            if props and (props['width'] >= '64' or props['height'] >= 64):
                                errors.append(
                                    ErrorNode(
                                        field='bildo',
                                        message=_('Размер картинки не должен привышать')
                                    )
                                )

                        if not len(errors):
                            dokumento_statuso.kodo = kwargs.get('kodo', dokumento_statuso.kodo)
                            dokumento_statuso.rezervo = kwargs.get('rezervo', False)
                            dokumento_statuso.icon_kodo = kwargs.get('icon_kodo', dokumento_statuso.icon_kodo),
                            dokumento_statuso.publikigo = kwargs.get('publikigo', dokumento_statuso.publikigo)
                            dokumento_statuso.forigo = kwargs.get('forigo', dokumento_statuso.forigo)
                            dokumento_statuso.lasta_autoro = user
                            dokumento_statuso.lasta_dato = timezone.now()

                            if kwargs.get('nomo'):
                                set_enhavo(dokumento_statuso.nomo, kwargs.get('nomo'), 'ru_RU')

                            if 'bildo' in kwargs:
                                dokumento_statuso.bildo.delete()
                            elif bildo:
                                dokumento_statuso.bildo.delete()
                                dokumento_statuso.bildo = bildo

                            dokumento_statuso.save()
                            status = True
                            message = _('Запись успешно изменена')

                        else:
                            dokumento_statuso = None
                            message = _('Неверные значения аргументов')
                else:
                    message = _('Нет аргументов для изменения')

        else:
            message = _('Недостаточно прав')

        return RedaktuDokumentoStatuso(status=status, message=message, errors=errors,
                                       dokumento_statuso=dokumento_statuso)


class RedaktuDokumentoSkemo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    dokumento_skemo = graphene.Field(DokumentoSkemoNode)

    class Arguments:
        uuid = graphene.UUID()
        informoj = graphene.String()
        dokumento_tipo_kodo = graphene.String()
        komenca_statuso_kodo = graphene.String()
        rajtoj = graphene.List(of_type=graphene.String)
        fina_statuso_kodo = graphene.String()
        eraro_statuso_kodo = graphene.String()
        ago_kodo = graphene.String()
        sistema = graphene.Boolean()
        publikigo = graphene.Boolean()
        forigo = graphene.Boolean()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False,
        message = None
        errors = []
        dokumento_skemo = None
        dokumento_tipo = None
        komenca_statuso_kodo = None
        fina_statuso_kodo = None
        eraro_statuso_kodo = None
        rajtoj = None
        user = info.context.user

        if (user.has_perm('dokumentoj.add_dokumentoskemo')
                or user.has_perm('dokumentoj.change_dokumentoskemo')
                or user.has_perm('dokumentoj.delete_dokumentoskemo')):

            # Создание записи
            if 'uuid' not in kwargs:
                if user.has_perm('dokumentoj.add_dokumentoskemo'):
                    if 'forigo' in kwargs and kwargs.get('forigo'):
                        errors.append(
                            ErrorNode(
                                field='forigo',
                                message=_('Нельзя удалить не созданную запись')
                            )
                        )

                    if not kwargs.get('informoj', False):
                        errors.append(
                            ErrorNode(
                                field='informoj',
                                message=_('Поле обязательно для заполнения')
                            )
                        )

                    if not kwargs.get('komenca_statuso_kodo', False):
                        errors.append(
                            ErrorNode(
                                field='komenca_statuso_kodo',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    else:
                        try:
                            komenca_statuso_kodo = DokumentoStatuso.objects.get(kodo=kwargs.get('komenca_statuso_kodo'))
                        except DokumentoSkemo.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='komenca_statuso_kodo',
                                    message=_('Статус с таким кодом не найден')
                                )
                            )

                    if not kwargs.get('fina_statuso_kodo', False):
                        errors.append(
                            ErrorNode(
                                field='fina_statuso_kodo',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    else:
                        try:
                            fina_statuso_kodo = DokumentoStatuso.objects.get(kodo=kwargs.get('fina_statuso_kodo'))
                        except DokumentoSkemo.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='fina_statuso_kodo',
                                    message=_('Статус с таким кодом не найден')
                                )
                            )

                    if not kwargs.get('eraro_statuso_kodo', False):
                        errors.append(
                            ErrorNode(
                                field='eraro_statuso_kodo',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    else:
                        try:
                            eraro_statuso_kodo = DokumentoStatuso.objects.get(kodo=kwargs.get('eraro_statuso_kodo'))
                        except DokumentoSkemo.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='eraro_statuso_kodo',
                                    message=_('Статус с таким кодом не найден')
                                )
                            )

                    if not kwargs.get('dokumento_tipo_kodo', False):
                        errors.append(
                            ErrorNode(
                                field='dokumento_tipo_kodo',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    else:
                        try:
                            dokumento_tipo = DokumentoTipo.objects.get(
                                kodo=kwargs.get('dokumento_tipo_kodo'),
                                forigo=False,
                                publikigo=True
                            )
                        except DokumentoTipo.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='dokumento_tipo_kodo',
                                    message=_('Тип документа не найден')
                                )
                            )

                    if 'rajtoj' in kwargs:
                        rajtoj = Permission.objects.filter(codename__in=kwargs.get('rajtoj'))
                        dif = set(kwargs.get('rajtoj')) - set(rajtoj.values_list('codename', flat=True))

                        if len(dif):
                            errors.append(
                                ErrorNode(
                                    field='rajtoj', message='{}: {}'.format(
                                        _('Указаны несуществующие права'),
                                        str(dif)
                                    )
                                )
                            )

                    if 'ago_kodo' in kwargs and not kwargs.get('ago_kodo'):
                        errors.append(
                            ErrorNode(
                                field='ago_kodo',
                                message=_('Поле не может быть пустым')
                            )
                        )

                    if not len(errors):
                        dokumento_skemo = DokumentoSkemo.objects.create(
                            dokumento_tipo=dokumento_tipo,
                            komenca_statuso=komenca_statuso_kodo,
                            fina_statuso=fina_statuso_kodo,
                            eraro_statuso=eraro_statuso_kodo,
                            publikigo=kwargs.get('publikigo', False),
                            forigo=kwargs.get('forigo', False)
                        )
                        dokumento_skemo.rajtoj.set(rajtoj)
                        set_enhavo(dokumento_skemo.informoj, kwargs.get('informoj'), 'ru_RU')

                        dokumento_skemo.save()
                        status = True
                        message = _('Запись успешна создана')
                    else:
                        message = _('Неверные значения аргументов')
                else:
                    message = _('Недостаточно прав на создание записи')
            else:
                # Изменение записи
                if len(kwargs) > 1:
                    if kwargs.get('forigo', False) and not user.has_perm('dokumentoj.delete_dokumentoskemo'):
                        message=_('Недостаточно прав для удаления записи')
                    elif (not user.has_perm('dokumentoj.change_dokumentoskemo')
                          and ('komenca_statuso_kodo' in kwargs or 'fina_statuso_kodo' in kwargs
                               or 'eraro_statuso_kodo' in kwargs or 'publikigo' in kwargs
                               or 'dokumento_tipo_kodo' in kwargs or 'rajtoj' in kwargs or 'ago_kodo' in kwargs)):
                        message = _('Недостаточно прав для изменения ресурса')
                    else:
                        try:
                            dokumento_skemo = DokumentoSkemo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                        except DokumentoTipo.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='uuid',
                                    message=_('Запись не найдена')
                                )
                            )

                        if kwargs.get('komenca_statuso_kodo', False):
                            try:
                                komenca_statuso_kodo = DokumentoStatuso.objects.get(
                                    kodo=kwargs.get('komenca_statuso_kodo'))
                            except DokumentoSkemo.DoesNotExist:
                                errors.append(
                                    ErrorNode(
                                        field='komenca_statuso_kodo',
                                        message=_('Статус с таким кодом не найден')
                                    )
                                )

                        if kwargs.get('fina_statuso_kodo', False):
                            try:
                                fina_statuso_kodo = DokumentoStatuso.objects.get(kodo=kwargs.get('fina_statuso_kodo'))
                            except DokumentoSkemo.DoesNotExist:
                                errors.append(
                                    ErrorNode(
                                        field='fina_statuso_kodo',
                                        message=_('Статус с таким кодом не найден')
                                    )
                                )

                        if kwargs.get('eraro_statuso_kodo', False):
                            try:
                                eraro_statuso_kodo = DokumentoStatuso.objects.get(kodo=kwargs.get('eraro_statuso_kodo'))
                            except DokumentoSkemo.DoesNotExist:
                                errors.append(
                                    ErrorNode(
                                        field='eraro_statuso_kodo',
                                        message=_('Статус с таким кодом не найден')
                                    )
                                )

                        if kwargs.get('dokumento_tipo_kodo', False):
                            try:
                                dokumento_tipo = DokumentoTipo.objects.get(
                                    kodo=kwargs.get('dokumento_tipo_kodo'),
                                    forigo=False,
                                    publikigo=True
                                )
                            except DokumentoTipo.DoesNotExist:
                                errors.append(
                                    ErrorNode(
                                        field='dokumento_tipo_kodo',
                                        message=_('Тип документа не найден')
                                    )
                                )

                        if 'rajtoj' in kwargs:
                            rajtoj = Permission.objects.filter(codename__in=kwargs.get('rajtoj'))
                            dif = set(kwargs.get('rajtoj')) - set(rajtoj.values_list('codename', flat=True))

                            if len(dif):
                                errors.append(
                                    ErrorNode(
                                        field='rajtoj', message='{}: {}'.format(
                                            _('Указаны несуществующие права'),
                                            str(dif)
                                        )
                                    )
                                )

                        if 'ago_kodo' in kwargs and not kwargs.get('ago_kodo'):
                            errors.append(
                                ErrorNode(
                                    field='ago_kodo',
                                    message=_('Поле не может быть пустым')
                                )
                            )

                        if not len(errors):
                            dokumento_skemo.dokumento_tipo = dokumento_tipo or dokumento_skemo.dokumento_tipo
                            dokumento_skemo.komenca_statuso = komenca_statuso_kodo or dokumento_skemo.komenca_statuso
                            dokumento_skemo.fina_statuso = fina_statuso_kodo or dokumento_skemo.fina_statuso
                            dokumento_skemo.eraro_statuso = eraro_statuso_kodo or dokumento_skemo.eraro_statuso
                            dokumento_skemo.ago_kodo = kwargs.get('ago_kodo', dokumento_skemo.ago_kodo)
                            dokumento_skemo.publikigo = kwargs.get('publikigo', dokumento_skemo.publikigo)
                            dokumento_skemo.forigo = kwargs.get('forigo', dokumento_skemo.forigo)
                            dokumento_skemo.lasta_autoro = user
                            dokumento_skemo.lasta_dato = timezone.now()

                            if len(rajtoj):
                                dokumento_skemo.rajtoj.clear()
                                dokumento_skemo.rajtoj.set(rajtoj)

                            if kwargs.get('informoj'):
                                set_enhavo(dokumento_skemo.informoj, kwargs.get('informoj'), 'ru_RU')

                            dokumento_skemo.save()
                            status = True
                            message = _('Запись успешно изменена')

                        else:
                            dokumento_skemo = None
                            message = _('Неверные значения аргументов')
                else:
                    message = _('Нет аргументов для изменения')

        else:
            message = _('Недостаточно прав')

        return RedaktuDokumentoSkemo(status=status, message=message, errors=errors, dokumento_skemo=dokumento_skemo)


class DokumentoAgo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    dokumento = graphene.Field(DokumentoNode)

    class Arguments:
        dok_uuid = graphene.UUID(required=True)
        ago_kodo = graphene.String(required=True)
        mesago = graphene.String()

    @staticmethod
    def mutate(root, info, dok_uuid, ago_kodo, **kwargs):
        user = info.context.user if info else None
        status = False
        message = None
        errors = []
        dokumento = None
        ago = None

        try:
            dokumento = Dokumento.objects.get(uuid=dok_uuid, forigo=False, publikigo=True)
        except Dokumento.DoesNotExist:
            errors.append(
                ErrorNode(
                    field='dok_uuid',
                    message=_('Документ не найден')
                )
            )

        try:
            ago = DokumentoSkemo.objects.get(dokumento_tipo=dokumento.tipo, kodo=ago_kodo,
                                             komenca_statuso=dokumento.statuso,
                                             komenca_stadio=dokumento.stadio,
                                             forigo=False, publikigo=True)
        except DokumentoSkemo.DoesNotExist:
            errors.append(
                ErrorNode(
                    field='ago_kodo',
                    message=_('Действие не найдено')
                )
            )

        if user and not user.groups.filter(
                permissions__codename__in=ago.rajtoj.all().values_list('codename', flat=True)
        ) and not user.is_superuser and not kwargs.get('sistema', False):
            errors.append(
                ErrorNode(
                    field='ago_kodo',
                    message=_('Недостаточно прав для действия')
                )
            )
        elif dokumento and dokumento.statuso != ago.komenca_statuso:
            errors.append(
                ErrorNode(
                    field='ago_kodo',
                    message=_('Данное действие не приминимо для текущего статуса документа')
                )
            )

        if not len(errors):
            homo = Kombatanto.objects.get(uzanto=user) if user else None

            func_code = """
def ago_func():
{}
            """.format(ago.ago_kodo)
            loc = {}
            messages = []
            exec(func_code, {'dokumento': dokumento, 'request': info, 'messages': messages, 'homo': homo}, loc)
            result = loc['ago_func']()

            dokumento.statuso = ago.fina_statuso if result else ago.eraro_statuso
            dokumento.stadio = ago.fina_stadio if result else ago.eraro_stadio
            dokumento.save()

            status = True
            message = _('Действие выполнено')

            # Определяем предыдущую запись истории операций
            hists = DokumentHistorio.objects.filter(
                dokumento=dokumento,
                publikigo=True,
                forigo=False
            ).order_by('-krea_dato')

            if hists:
                last = hists[0]
                komenco_dato = last.fina_dato
            else:
                komenco_dato = timezone.now()

            # Создаём новую запись истории
            DokumentHistorio.objects.create(
                autoro=user,
                dokumento=dokumento,
                komenco_dato=komenco_dato,
                fina_dato=timezone.now(),
                skemo=ago,
                result=result,
                mesagoj='\r\n'.join(messages) if len(messages) else kwargs.get('mesago'),
                publikigo=True,
                forigo=False
            )

            automata = automata_serchado(dokumento)

            if automata:
                DokumentoAgo.mutate(root, info, dokumento.uuid, automata.kodo)

        else:
            dokumento = None
            message = _('Неверные значения аргументов')

        return DokumentoAgo(status=status, message=message, errors=errors, dokumento=dokumento)


class RedaktuDokumento(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    dokumento = graphene.Field(DokumentoNode)

    class Arguments:
        uuid = graphene.UUID()
        tipo_kodo = graphene.String()
        id = graphene.Int()
        informoj = graphene.String()
        dato = graphene.Date()
        posedanto_uuid = graphene.UUID()
        posedanto_magazeno_uuid = graphene.UUID()
        celo_organizo_uuid = graphene.UUID()
        celo_magazeno_uuid = graphene.UUID()
        respondeca_uuid = graphene.UUID()
        statuso_kodo = graphene.String()
        publikigo = graphene.Boolean()
        forigo = graphene.Boolean()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False,
        message = None
        errors = []
        tipo = None
        posedanto = None
        posedanto_magazeno = None
        celo_organizo = None
        celo_magazeno = None
        respondeca = None
        statuso = None
        dokumento = None
        user = info.context.user

        if (user.has_perm('dokumentoj.add_dokumento')
                or user.has_perm('dokumentoj.change_dokumento')
                or user.has_perm('dokumentoj.delete_dokumento')):

            # Создание записи
            if 'uuid' not in kwargs:
                if user.has_perm('dokumentoj.add_dokumento'):
                    if 'forigo' in kwargs and kwargs.get('forigo'):
                        errors.append(
                            ErrorNode(
                                field='forigo',
                                message=_('Нельзя удалить не созданную запись')
                            )
                        )

                    if not kwargs.get('tipo_kodo', False):
                        errors.append(
                            ErrorNode(
                                field='tipo_kodo',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    else:
                        try:
                            tipo = DokumentoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False, publikigo=True)
                        except DokumentoTipo.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='tipo_kodo',
                                    message=_('Тип документа с таким кодом не найден')
                                )
                            )

                    if 'id' in kwargs and kwargs.get('id', 0) <= 0:
                        errors.append(
                            ErrorNode(
                                field='id',
                                message=_('Номер документа должен быть больше ноля')
                            )
                        )

                    if 'dato' not in kwargs:
                        errors.append(
                            ErrorNode(
                                field='dato',
                                message=_('Поле обязательно для заполнения')
                            )
                        )

                    if not kwargs.get('posedanto_uuid', False):
                        errors.append(
                            ErrorNode(
                                field='posedanto_uuid',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    else:
                        try:
                            posedanto = Kombatanto.objects.get(uuid=kwargs.get('posedanto_uuid'), tipo__kodo='organizo',
                                                             forigo=False, publikigo=True)
                        except Kombatanto.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='posedanto_uuid',
                                    message=_('Организация не найдена')
                                )
                            )

                    if not kwargs.get('posedanto_magazeno_uuid', False):
                        errors.append(
                            ErrorNode(
                                field='posedanto_magazeno_uuid',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    else:
                        try:
                            posedanto_magazeno = Kombatanto.objects.get(uuid=kwargs.get('posedanto_magazeno_uuid'),
                                                                      tipo__kodo='magazeno', forigo=False, publikigo=True)
                        except Kombatanto.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='posedanto_magazeno_uuid',
                                    message=_('Организация не найдена')
                                )
                            )

                    if not kwargs.get('celo_organizo_uuid', False):
                        errors.append(
                            ErrorNode(
                                field='celo_organizo_uuid',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    else:
                        try:
                            celo_organizo = Kombatanto.objects.get(uuid=kwargs.get('celo_organizo_uuid'),
                                                                 tipo__kodo='organizo', forigo=False, publikigo=True)
                        except Kombatanto.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='celo_organizo_uuid',
                                    message=_('Организация не найдена')
                                )
                            )

                    if not kwargs.get('celo_magazeno_uuid', False):
                        errors.append(
                            ErrorNode(
                                field='celo_magazeno_uuid',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    else:
                        try:
                            celo_magazeno = Kombatanto.objects.get(uuid=kwargs.get('celo_magazeno_uuid'),
                                                                 tipo__kodo='magazeno', forigo=False, publikigo=True)
                        except Kombatanto.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='celo_magazeno_uuid',
                                    message=_('Организация не найдена')
                                )
                            )

                    if kwargs.get('respondeca_uuid', False):
                        try:
                            respondeca = Kombatanto.objects.get(uuid=kwargs.get('respondeca_uuid'), forigo=False,
                                                                  publikigo=True)
                        except Kombatanto.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='respondeca_uuid',
                                    message=_('Организация не найдена')
                                )
                            )

                    if 'statuso_kodo' in kwargs:
                        try:
                            statuso = DokumentoStatuso.objects.get(kodo='kreita', forigo=False,
                                                                   publikigo=True)
                        except DokumentoStatuso.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='statuso_kodo',
                                    message=_('Статус с таким кодом не найден')
                                )
                            )

                    if not len(errors):
                        dokumento = Dokumento.objects.create(
                            id=kwargs.get('id'),
                            tipo=tipo,
                            dato=kwargs.get('dato'),
                            posedanto=posedanto,
                            posedanto_magazeno=posedanto_magazeno,
                            celo_organizo=celo_organizo,
                            celo_magazeno=celo_magazeno,
                            respondeca=respondeca or None,
                            statuso=statuso or None,
                            stadio=1,
                            publikigo=kwargs.get('publikigo', False),
                            forigo=kwargs.get('forigo', False),
                            autoro=user
                        )

                        if 'informoj' in kwargs:
                            set_enhavo(dokumento.informoj, kwargs.get('informoj'), 'ru_RU')

                        dokumento.save()

                        # Ищем и запускаем автоматические действия
                        agoj = []
                        automata = automata_serchado(dokumento)

                        if automata:
                            DokumentoAgo.mutate(None, info, dokumento.uuid, automata.kodo)

                        status = True
                        message = _('Запись успешна создана')
                    else:
                        status = False
                        message = _('Неверные значения аргументов')
                else:
                    message = _('Недостаточно прав на создание записи')
            else:
                # Изменение записи
                if len(kwargs) > 1:
                    if kwargs.get('forigo', False) and not user.has_perm('dokumentoj.delete_dokumento'):
                        message = _('Недостаточно прав для удаления записи')
                    elif (not user.has_perm('dokumentoj.change_dokumento')
                          and ('tipo_kodo' in kwargs or 'informoj' in kwargs or 'publikigo' in kwargs
                               or 'posedanto_uuid' in kwargs or 'celo_organizo_uuid' in kwargs
                               or 'respondeca_uuid' in kwargs or 'statuso_kodo' in kwargs
                               or 'id' in kwargs or 'dato' in kwargs)):
                        message = _('Недостаточно прав для изменения ресурса')
                    else:
                        try:
                            dokumento = Dokumento.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                        except Dokumento.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='uuid',
                                    message=_('Запись не найдена')
                                )
                            )

                        if 'id' in kwargs and kwargs.get('id', 0) <= 0:
                            errors.append(
                                ErrorNode(
                                    field='id',
                                    message=_('Номер документа должен быть больше ноля')
                                )
                            )

                        if 'tipo_kodo' in kwargs:
                            try:
                                tipo = DokumentoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                 publikigo=True)
                            except DokumentoTipo.DoesNotExist:
                                errors.append(
                                    ErrorNode(
                                        field='tipo_kodo',
                                        message=_('Тип документа с таким кодом не найден')
                                    )
                                )

                        if 'posedanto_uuid' in kwargs:
                            errors.append(
                                ErrorNode(
                                    field='posedanto_uuid',
                                    message=_('Нельзя изменить организацию-заявителя')
                                )
                            )

                        if 'posedanto_magazeno_uuid' in kwargs:
                            errors.append(
                                ErrorNode(
                                    field='posedanto_magazeno_uuid',
                                    message=_('Нельзя изменить склад организации-заявителя')
                                )
                            )

                        if 'celo_organizo_uuid' in kwargs:
                            errors.append(
                                ErrorNode(
                                    field='posedanto_uuid',
                                    message=_('Нельзя изменить организацию-контрагента')
                                )
                            )

                        if 'celo_magazeno_uuid' in kwargs:
                            errors.append(
                                ErrorNode(
                                    field='celo_magazeno_uuid',
                                    message=_('Нельзя изменить склад организации-контрагента')
                                )
                            )

                        if 'respondeca_uuid' in kwargs:
                            try:
                                respondeca = Kombatanto.objects.get(uuid=kwargs.get('respondeca_uuid'), forigo=False,
                                                              publikigo=True)
                            except Kombatanto.DoesNotExist:
                                errors.append(
                                    ErrorNode(
                                        field='respondeca_uuid',
                                        message=_('Организация не найдена')
                                    )
                                )

                        if 'statuso_kodo' in kwargs:
                            try:
                                statuso = DokumentoStatuso.objects.get(kodo=kwargs.get('statuso_kodo'), forigo=False,
                                                                       publikigo=True)
                            except DokumentoStatuso.DoesNotExist:
                                errors.append(
                                    ErrorNode(
                                        field='statuso_kodo',
                                        message=_('Статус с таким кодом не найден')
                                    )
                                )

                        if not len(errors):
                            dokumento.id = kwargs.get('id', dokumento.id)
                            dokumento.tipo = tipo or dokumento.tipo
                            dokumento.dato = kwargs.get('dato', dokumento.dato)
                            dokumento.posedanto = posedanto or dokumento.posedanto
                            dokumento.posedanto_magazeno = posedanto_magazeno or dokumento.posedanto_magazeno
                            dokumento.celo_organizo = celo_organizo or dokumento.celo_organizo
                            dokumento.celo_magazeno = celo_magazeno or dokumento.celo_magazeno
                            dokumento.respondeca = respondeca or dokumento.respondeca
                            dokumento.statuso = statuso or dokumento.statuso
                            dokumento.publikigo = kwargs.get('publikigo', dokumento.publikigo)
                            dokumento.forigo = kwargs.get('forigo', dokumento.forigo)
                            dokumento.lasta_autoro = user
                            dokumento.lasta_dato = timezone.now()

                            if kwargs.get('informoj'):
                                set_enhavo(dokumento.informoj, kwargs.get('informoj'), 'ru_RU')

                            dokumento.save()
                            status = True
                            message = _('Запись успешно изменена')

                        else:
                            status = False
                            dokumento = None
                            message = _('Неверные значения аргументов')
                else:
                    message = _('Нет аргументов для изменения')

        else:
            message = _('Недостаточно прав')

        return RedaktuDokumento(status=status, message=message, errors=errors, dokumento=dokumento)


class RedaktuDokumentoKordo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    dokumento_kordo = graphene.Field(DokumentoKordoNode)

    class Arguments:
        uuid = graphene.UUID()
        id = graphene.Int()
        dokumento_uuid = graphene.UUID()
        resurso_uuid = graphene.UUID()
        resurso_opcion_uuid = graphene.UUID()
        prezo = graphene.Float()
        kvanto = graphene.Float()
        publikigo = graphene.Boolean()
        forigo = graphene.Boolean()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False,
        message = None
        errors = []
        dokumento = None
        dokumento_kordo = None
        resurso = None
        resurso_opcion = None
        user = info.context.user

        if (user.has_perm('dokumentoj.add_dokumentokordo')
                or user.has_perm('dokumentoj.change_dokumentokordo')
                or user.has_perm('dokumentoj.delete_dokumentokordo')):

            # Создание записи
            if 'uuid' not in kwargs:
                if user.has_perm('dokumentoj.add_dokumentokordo'):
                    if 'forigo' in kwargs and kwargs.get('forigo'):
                        errors.append(
                            ErrorNode(
                                field='forigo',
                                message=_('Нельзя удалить не созданную запись')
                            )
                        )

                    if not kwargs.get('dokumento_uuid', False):
                        errors.append(
                            ErrorNode(
                                field='dokumento_uuid',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    else:
                        try:
                            dokumento = Dokumento.objects.get(uuid=kwargs.get('dokumento_uuid'),
                                                              forigo=False, publikigo=True)
                        except Dokumento.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='dokumento_uuid',
                                    message=_('Документ не найден')
                                )
                            )

                    if not kwargs.get('resurso_uuid', False):
                        errors.append(
                            ErrorNode(
                                field='resurso_uuid',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    else:
                        try:
                            resurso = Resurso.objects.get(uuid=kwargs.get('resurso_uuid'), forigo=False, publikigo=True)
                        except Resurso.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='resurso_uuid',
                                    message=_('Ресурс не найден')
                                )
                            )

                    if kwargs.get('resurso_opcion_uuid', False):
                        try:
                            resurso_opcion = ResursoOpcion.objects.get(uuid=kwargs.get('resurso_opcion_uuid'),
                                                                       forigo=False, publikigo=True)
                        except ResursoOpcion.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='resurso_uuid',
                                    message=_('Ресурс не найден')
                                )
                            )

                    if 'prezo' not in kwargs:
                        errors.append(
                            ErrorNode(
                                field='prezo',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    elif kwargs.get('prezo') < 0:
                        errors.append(
                            ErrorNode(
                                field='prezo',
                                message=_('Стоимость не может быть отрицательной')
                            )
                        )

                    if 'kvanto' not in kwargs:
                        errors.append(
                            ErrorNode(
                                field='kvanto',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    elif kwargs.get('kvanto') < 0:
                        errors.append(
                            ErrorNode(
                                field='kvanto',
                                message=_('Значение не может быть отрицательным')
                            )
                        )

                    if not len(errors):
                        dokumento_kordo = DokumentoKordo.objects.create(
                            id=kwargs.get('id'),
                            dokumento=dokumento,
                            resurso=resurso,
                            resurso_opcion=resurso_opcion,
                            prezo=kwargs.get('prezo'),
                            kvanto=kwargs.get('kvanto'),
                            publikigo=kwargs.get('publikigo', False),
                            forigo=kwargs.get('forigo', False),
                            autoro=user
                        )
                        status = True
                        message = _('Запись успешна создана')
                    else:
                        status = False
                        message = _('Неверные значения аргументов')
                else:
                    message = _('Недостаточно прав на создание записи')
            else:
                # Изменение записи
                if len(kwargs) > 1:
                    if kwargs.get('forigo', False) and not user.has_perm('dokumentoj.delete_dokumentokordo'):
                        message = _('Недостаточно прав для удаления записи')
                    elif (not user.has_perm('dokumentoj.change_dokumentokordo')
                          and ('id' in kwargs or 'dokumento_uuid' in kwargs or 'publikigo' in kwargs
                               or 'resurso_uuid' in kwargs or 'resurso_opcion_uuid' in kwargs
                               or 'kvanto' in kwargs or 'prezo' in kwargs)):
                        message = _('Недостаточно прав для изменения ресурса')
                    else:
                        try:
                            dokumento_kordo = DokumentoKordo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                        except Dokumento.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='uuid',
                                    message=_('Запись не найдена')
                                )
                            )

                        if 'dokumento_uuid' in kwargs:
                            try:
                                dokumento = Dokumento.objects.get(uuid=kwargs.get('dokumento_uuid'), forigo=False,
                                                                  publikigo=True)
                            except Dokumento.DoesNotExist:
                                errors.append(
                                    ErrorNode(
                                        field='dokumento_uuid',
                                        message=_('Тип документа с таким кодом не найден')
                                    )
                                )

                        if 'resurso_uuid' in kwargs:
                            try:
                                resurso = Resurso.objects.get(uuid=kwargs.get('resurso_uuid'), forigo=False,
                                                              publikigo=True)
                            except Resurso.DoesNotExist:
                                errors.append(
                                    ErrorNode(
                                        field='resurso_uuid',
                                        message=_('Ресурс не найден')
                                    )
                                )

                        if 'resurso_opcion_uuid' in kwargs:
                            try:
                                resurso_opcion = ResursoOpcion.objects.get(uuid=kwargs.get('resurso_opcion_uuid'),
                                                                           forigo=False, publikigo=True)
                            except ResursoOpcion.DoesNotExist:
                                errors.append(
                                    ErrorNode(
                                        field='resurso_uuid',
                                        message=_('Ресурс не найден')
                                    )
                                )

                        if kwargs.get('prezo', 0) < 0:
                            errors.append(
                                ErrorNode(
                                    field='prezo',
                                    message=_('Стоимость не может быть отрицательной')
                                )
                            )

                        if 'kvanto' in kwargs:
                            if kwargs.get('kvanto') < 0:
                                errors.append(
                                    ErrorNode(
                                        field='kvanto',
                                        message=_('Значение не может быть отрицательным')
                                    )
                                )

                        if not len(errors):
                            dokumento_kordo.id = kwargs.get('id', dokumento_kordo.id)
                            dokumento_kordo.dokumento = dokumento or dokumento_kordo.dokumento
                            dokumento_kordo.resurso = resurso or dokumento_kordo.resurso
                            dokumento_kordo.resurso_opcion = resurso_opcion or dokumento_kordo.resurso_opcion
                            dokumento_kordo.prezo = kwargs.get('prezo', dokumento_kordo.prezo)
                            dokumento_kordo.kvanto = kwargs.get('kvanto', dokumento_kordo.kvanto)
                            dokumento_kordo.publikigo = kwargs.get('publikigo', dokumento_kordo.publikigo)
                            dokumento_kordo.forigo = kwargs.get('forigo', dokumento_kordo.forigo)
                            dokumento_kordo.lasta_autoro = user
                            dokumento_kordo.lasta_dato = timezone.now()
                            dokumento_kordo.save()
                            status = True
                            message = _('Запись успешно изменена')

                        else:
                            status = False
                            dokumento_kordo = None
                            message = _('Неверные значения аргументов')
                else:
                    message = _('Нет аргументов для изменения')

        else:
            message = _('Недостаточно прав')

        return RedaktuDokumentoKordo(status=status, message=message, errors=errors, dokumento_kordo=dokumento_kordo)


class RedaktuDokumentoResurso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kordo_uuid = graphene.UUID()
    resursoj = graphene.List(KombatantoResursoNode)
    resurso = graphene.Field(KombatantoResursoNode)
    kodo = graphene.Int()
    totala_bezonata = graphene.Int()

    class Arguments:
        dokumento_uuid = graphene.UUID(required=True)
        resurso_uuid = graphene.UUID(required=True)
        ekskludi = graphene.Boolean()

    @staticmethod
    def mutate(root, info, dokumento_uuid, resurso_uuid, **kwargs):
        status = False
        message = None
        resursoj = []
        resurso = None
        kordo_uuid = None
        kodo = None
        totala_bezonata = None
        user = info.context.user

        if user.has_perm('dokumentoj.specialisto_pri_vendoj') or user.is_superuser:
            dok = None
            kordo = None

            # Находим документ
            try:
                dok = Dokumento.objects.get(uuid=dokumento_uuid, publikigo=True, forigo=False)
            except Dokumento.DoesNotExist:
                message = _('Документ не найден')

            # Находим ресурс
            if dok:
                try:
                    resurso = KombatantoResurso.objects.get(
                        uuid=resurso_uuid, publikigo=True, forigo=False, organizo=dok.celo_magazeno,
                        rezervita=False, elcherpita=False, teknika=False, resurso__unuo_mezuro='kvanto'
                    )
                except KombatantoResurso.DoesNotExist:
                    message = _('Ресурс не найден')

            # Находим строку заявки для ресурса
            if resurso:
                try:
                    with transaction.atomic():
                        kordo = DokumentoKordo.objects.get(resurso=resurso.resurso, dokumento=dok, publikigo=True,
                                                           forigo=False)

                        if not kwargs.get('ekskludi', False):
                            if (not kordo.atributoj or isinstance(kordo.atributoj, dict)
                                    and len(kordo.atributoj.get('rezervita_unika', [])) < kordo.kvanto):
                                kordo.atributoj = (kordo.atributoj
                                                   if isinstance(kordo.atributoj, dict) else {'rezervita_unika': []})

                                resursoj = KombatantoResurso.objects.filter(
                                    uuid__in=kordo.atributoj.get('rezervita_unika'), publikigo=True, forigo=False,
                                    elcherpita=False, rezervita=False
                                )

                                # Проверяем, включен ли ресурс в другие заявки
                                if not DokumentoKordo.objects.filter(
                                    ~Q(dokumento=dok),
                                    publikigo=True, forigo=False, dokumento__publikigo=True, dokumento__forigo=False,
                                    resurso=resurso.resurso,
                                    atributoj__rezervita_unika__contains=str(resurso_uuid)
                                ):
                                    if not kordo.atributoj:
                                        kordo.atributoj = {
                                            'rezervita_unika': []
                                        }

                                    if str(resurso_uuid) not in kordo.atributoj['rezervita_unika']:
                                        kordo.atributoj['rezervita_unika'].append(str(resurso_uuid))
                                        kordo.save()

                                        resursoj = KombatantoResurso.objects.filter(
                                            uuid__in=kordo.atributoj.get('rezervita_unika'), publikigo=True, forigo=False,
                                            elcherpita=False, rezervita=False
                                        )

                                        kodo = 0
                                        totala_bezonata = kordo.kvanto
                                        kordo_uuid = kordo.uuid
                                        status = True
                                        message = _('Ресурс включен в заявку')
                                    else:
                                        kodo = 1
                                        totala_bezonata = kordo.kvanto
                                        message = _('Данный ресурс уже включён в данную заявку')

                                else:
                                    kodo = 2
                                    totala_bezonata = kordo.kvanto
                                    message = _('Данный ресурс уже включён в другую заявку')
                            else:
                                resursoj = KombatantoResurso.objects.filter(
                                    uuid__in=kordo.atributoj.get('rezervita_unika'), publikigo=True, forigo=False,
                                    elcherpita=False, rezervita=False
                                )
                                kodo = 3
                                totala_bezonata = kordo.kvanto
                                message = _('Невозможно добавить больше этого ресурса в данную заявку')
                        else:
                            # Проверяем наличие этого ресурса в текущей заявке
                            if DokumentoKordo.objects.filter(
                                dokumento=dok, publikigo=True, forigo=False, resurso=resurso.resurso,
                                atributoj__rezervita_unika__contains=str(resurso_uuid)
                            ):
                                idx = kordo.atributoj['rezervita_unika'].index(str(resurso_uuid))
                                del kordo.atributoj['rezervita_unika'][idx]
                                kordo.save()

                                resursoj = KombatantoResurso.objects.filter(
                                    uuid__in=kordo.atributoj.get('rezervita_unika'), publikigo=True, forigo=False,
                                    elcherpita=False, rezervita=False
                                )

                                kodo = 0
                                totala_bezonata = kordo.kvanto
                                kordo_uuid = kordo.uuid
                                status = True
                                message = _('Ресурс исключён из заявки')
                            else:
                                message = _('Ресурс не входит в состав заявки')

                except DokumentoKordo.DoesNotExist:
                    message = _('Ресурс не входит в состав заявки')

        else:
            message = _('Недостаточно прав')

        return RedaktuDokumentoResurso(status=status, message=message, resursoj=resursoj, resurso=resurso, kodo=kodo,
                                       kordo_uuid=kordo_uuid, totala_bezonata=totala_bezonata)


class RedaktuDokumentoAgordo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    dokumenta_agordo = graphene.Field(DokumentoAgordoNode)

    class Arguments:
        dokumento_tipo = graphene.String(required=True)
        posedanto_organizo_uuid = graphene.UUID()
        posedanto_magazeno_uuid = graphene.UUID()
        celo_organizo_uuid = graphene.UUID()
        celo_magazeno_uuid = graphene.UUID()
        forigo = graphene.Boolean()

    @staticmethod
    def mutate(root, info, dokumento_tipo, **kwargs):
        status = False
        message = None
        errors = None
        dokumenta_agordo = None
        user = info.context.user

        if (user.has_perm('dokumentoj.change_dokumentaagordo') or user.has_perm('dokumentoj.delete_dokumentaagordo')
                or user.has_perm('dokumentoj.add_dokumentaagordo')):
            try:
                dok_tipo = DokumentoTipo.objects.get(kodo=dokumento_tipo, publikigo=True, forigo=False)
            except DokumentoTipo.DoesNotExist:
                dok_tipo = None
                errors.append(
                    ErrorNode(
                        field='dokumento_tipo',
                        message=_('Тип документа не найден')
                    )
                )

            try:
                dokumenta_agordo = DokumentoAgordo.objects.get(publikigo=True, forigo=False, dokumento_tipo=dok_tipo)

                # Изменения в запись
                if kwargs.get('forigo', False) and user.has_perm('dokumentoj.delete_dokumentaagordo'):
                    errors.append(
                        ErrorNode(
                            field='forigo',
                            messge=_('Недостаточно прав для удаления')
                        )
                    )

            except DokumentoAgordo.DoesNotExist:
                # Создаём новую запись
                pass

        else:
            message = _('Недостаточно прав')

        return RedaktuDokumentoAgordo(status=status, message=message, errors=errors, dokumenta_agordo=dokumenta_agordo)


class DokumentoMutation(graphene.ObjectType):
    redaktu_dokumento_tipo = RedaktuDokumentoTipo.Field()
    redaktu_dokumento_statuso = RedaktuDokumentoStatuso.Field()
    redaktu_dokumento_skemo = RedaktuDokumentoSkemo.Field()
    redaktu_dokumento = RedaktuDokumento.Field()
    redaktu_dokumento_kordo = RedaktuDokumentoKordo.Field()
    redaktu_dokumento_resurso = RedaktuDokumentoResurso.Field()
    dokumento_ago = DokumentoAgo.Field()
