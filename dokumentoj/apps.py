from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class DokumentojConfig(AppConfig):
    name = 'dokumentoj'
    verbose_name = _('Dokumentoj')