"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.db import models
from django.contrib.postgres import fields as pgfields
from django.core.validators import MinValueValidator
from django.utils.translation import ugettext_lazy as _
from main.models import SiriusoBazaAbstrakta, SiriusoBazaAbstraktaKomunumoj
from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, dosiero_nomo, generate_hex_id
from kombatantoj.models import Kombatanto, KombatantoRolo
import functools


class DokumentoTipo(SiriusoBazaAbstraktaKomunumoj):
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=False, default=default_lingvo,
                              encoder=CallableEncoder)

    class Meta:
        db_table = 'dokumentoj_tipoj'
        verbose_name = _('Tipo de dokumento')
        verbose_name_plural = _('Tipoj de dokumentoj')
        permissions = (
            ('povas_vidi_dokumentan_tipon', _('Povas vidi dokumentan tipon')),
            ('povas_krei_dokumentan_tipon', _('Povas krei dokumentan tipon')),
            ('povas_forigi_dokumentan_tipon', _('Povas forigi dokumentan tipon')),
            ('povas_shanghi_dokumentan_tipon', _('Povas ŝanĝi dokumentan tipon')),
        )

    def __str__(self):
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])

    @staticmethod
    def _get_perm_cond(user):
        if user.is_staff or user.is_superuser:
            return models.Q()

        cond = models.Q()

        if user.has_perm('dokumentoj.achetada_specialisto'):
            cond |= (models.Q(kodo__in=['acheto']))

        if user.has_perm('dokumentoj.specialisto_pri_vendoj') or user.has_perm('dokumentoj.kolektanto_mikso'):
            cond |= (models.Q(kodo__in=['livero']))

        if user.has_perm('dokumentoj.logistiko'):
            cond |= (models.Q(kodo__in=['provizo']))

        if user.has_perm('dokumentoj.produktado_specialisto'):
            cond |= (models.Q(kodo__in=['produktado']))

        return cond


class DokumentoStatuso(SiriusoBazaAbstraktaKomunumoj):
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=False, default=default_lingvo,
                              encoder=CallableEncoder)
    bildo = models.ImageField(_('Statuso bildo'), blank=True, null=True,
                              upload_to=functools.partial(dosiero_nomo, subdir='dokumentoj/statusoj'))
    icon_kodo = models.CharField(_('Ikona kodo'), max_length=64, blank=True, null=True)

    class Meta:
        db_table = 'dokumentoj_statusoj'
        verbose_name = _('Statuso de dokumento')
        verbose_name_plural = _('Statusoj de dokumentoj')
        permissions = (
            ('povas_vidi_dokumentan_statuson', _('Povas vidi dokumentan statuson')),
            ('povas_krei_dokumentan_statuson', _('Povas krei dokumentan statuson')),
            ('povas_forigi_dokumentan_statuson', _('Povas forigi dokumentan statuson')),
            ('povas_shanghi_dokumentan_statuson', _('Povas ŝanĝi dokumentan statuson')),
        )

    def __str__(self):
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])


class DokumentoSkemo(SiriusoBazaAbstrakta):
    kodo = models.CharField(_('Kodo'), max_length=64, blank=False, null=False)
    nomo = pgfields.JSONField(verbose_name=_('Aga nomo'), blank=False, default=default_lingvo,
                              encoder=CallableEncoder)
    informoj = pgfields.JSONField(verbose_name=_('Aga priskribo'), blank=False, default=default_lingvo,
                                  encoder=CallableEncoder)
    dokumento_tipo = models.ForeignKey(DokumentoTipo, blank=False, null=False, on_delete=models.CASCADE,
                                       verbose_name=_('Tipo de dokumento'))
    posedanto_ago = models.BooleanField(_('Agado de dokumentposedanto'), default=False)
    kontr_ago = models.BooleanField(_('Kontraŭpartia agado'), default=False)
    respondeca_ago = models.BooleanField(_('Ago de la respondeca persono'), default=False)
    komenca_stadio = models.IntegerField(_('Komenca etapo'), blank=False, null=False)
    komenca_statuso = models.ForeignKey(DokumentoStatuso, blank=False, null=False, on_delete=models.CASCADE,
                                        related_name='komenca_statuso', verbose_name=_('Komenca statuso'))
    roloj = models.ManyToManyField(KombatantoRolo, verbose_name=_('Roloj'), blank=False)
    fina_informoj = pgfields.JSONField(verbose_name=_('Priskribo de pozitiva rezulto'),
                                       blank=False, default=default_lingvo, encoder=CallableEncoder)
    fina_stadio = models.IntegerField(_('Fina etapo'), blank=False, null=False)
    fina_statuso = models.ForeignKey(DokumentoStatuso, blank=False, null=False, related_name='fina_statuso',
                                     on_delete=models.CASCADE, verbose_name=_('Fina statuso'))
    eraro_informoj = pgfields.JSONField(verbose_name=_('Priskribo de negativa rezulto'),
                                        blank=False, default=default_lingvo, encoder=CallableEncoder)
    eraro_stadio = models.IntegerField(_('Stadio ĉe eraro'), blank=False, null=False)
    eraro_statuso = models.ForeignKey(DokumentoStatuso, blank=False, null=False, related_name='eraro_statuso',
                                      on_delete=models.CASCADE, verbose_name=_('Erara statuso'))
    ago_kodo = models.TextField(_('Plenumebla kodo'), blank=False, null=False, default=" return True")
    automata = models.BooleanField(_('Aŭtomata agado'), default=False)
    escepta = models.BooleanField(_('Escepta ago'), default=False)
    sistema = models.BooleanField(_('Sistema agado'), default=False)

    class Meta:
        db_table = 'dokumento_skemo'
        verbose_name = _('Skemo de dokumento')
        verbose_name_plural = _('Skemoj de dokumentoj')
        permissions = (
            ('povas_vidi_dokumentan_skemon', _('Povas vidi dokumentan skemon')),
            ('povas_krei_dokumentan_skemon', _('Povas krei dokumentan skemon')),
            ('povas_forigi_dokumentan_skemon', _('Povas forigi dokumentan skemon')),
            ('povas_shanghi_dokumentan_skemon', _('Povas ŝanĝi dokumentan skemon')),
        )


generate_dok_hid = functools.partial(generate_hex_id, 'dokumentoj.Dokumento')


class Dokumento(SiriusoBazaAbstraktaKomunumoj):
    id = models.IntegerField(_('Dokumentnumero'), blank=True, null=True)
    hid = models.CharField(_('Hex ID'), max_length=16, db_index=True, null=False, blank=False,
                           default=generate_dok_hid)
    tipo = models.ForeignKey(DokumentoTipo, blank=False, null=False, on_delete=models.CASCADE,
                             verbose_name=_('Tipo de dokumento'))
    dato = models.DateField(_('Дата документа'), blank=False, null=False)
    posedanto_dokumento = models.ForeignKey('self', blank=True, null=True, on_delete=models.CASCADE,
                                            verbose_name=_('Gepatra Dokumento'))
    informoj = pgfields.JSONField(verbose_name=_('Dokumentaj Informoj'), blank=False, default=default_lingvo,
                              encoder=CallableEncoder)
    posedanto = models.ForeignKey('kombatantoj.Kombatanto', blank=False, null=False, on_delete=models.CASCADE,
                                  verbose_name=_('Ricevanta organizo'))
    posedanto_magazeno = models.ForeignKey('kombatantoj.Kombatanto', blank=False, null=False, on_delete=models.CASCADE,
                                           related_name='posedanto_magazeno',
                                           verbose_name=_('Magazeno de Ricevantoj'))
    celo_organizo = models.ForeignKey('kombatantoj.Kombatanto', blank=False, null=False, on_delete=models.CASCADE,
                                      related_name='celo_organizo', verbose_name=_('Plenuma organizo'))
    celo_magazeno = models.ForeignKey('kombatantoj.Kombatanto', blank=False, null=False, on_delete=models.CASCADE,
                                      related_name='celo_magazeno', verbose_name=_('Plenuma magazeno'))
    statuso = models.ForeignKey(DokumentoStatuso, on_delete=models.CASCADE, blank=False, null=False,
                                verbose_name=_('Statuso de dokumento'))
    stadio = models.IntegerField(_('Etapo'), blank=False, null=False)
    respondeca = models.ForeignKey('kombatantoj.Kombatanto', blank=True, null=True, on_delete=models.CASCADE,
                                   verbose_name=_('Respondeca oficiro'), related_name=_('dokumentoj_respondecoj'))
    dosiero = models.FileField(_('Файл документа'), blank=True, null=True,
                               upload_to=functools.partial(dosiero_nomo, subdir='dokumentoj/dosieroj'))
    kreita_automate = models.BooleanField(_('Aŭtomate generita'), default=False)
    rezervita = models.BooleanField(_('Rezervo'), default=False)

    class Meta:
        db_table = 'dokumentoj'
        verbose_name = _('Dokumento')
        verbose_name_plural = _('Dokumentoj')
        permissions = (
            ('povas_vidi_dokumenton', _('Povas vidi dokumenton')),
            ('povas_krei_dokumenton', _('Povas krei dokumenton')),
            ('povas_forigi_dokumenton', _('Povas forigi dokumenton')),
            ('povas_shanghi_dokumenton', _('Povas ŝanĝi dokumenton')),
        )

    def __str__(self):
        return '{}: {}'.format(
            get_enhavo(self.tipo.nomo, empty_values=True)[0],
            self.id
        )

    @staticmethod
    def _get_perm_cond(user):
        if user.is_staff or user.is_superuser:
            return models.Q()

        try:
            if user.has_perm('dokumentoj.achetada_specialisto'):
                dok_tipoj = {'tipo__kodo__in': ['acheto']}
            else:
                dok_tipoj = {}

            if user.has_perm('dokumentoj.specialisto_pri_vendoj') or user.has_perm('dokumentoj.kolektanto_mikso'):
                if 'tipo__kodo__in' in dok_tipoj:
                    dok_tipoj['tipo__kodo__in'].append('livero')
                else:
                    dok_tipoj = {'tipo__kodo__in': ['livero']}

            if user.has_perm('dokumentoj.logistiko'):
                if 'tipo__kodo__in' in dok_tipoj:
                    dok_tipoj['tipo__kodo__in'].append('provizo')
                else:
                    dok_tipoj = {'tipo__kodo__in': ['provizo']}

            if user.has_perm('dokumentoj.produktado_specialisto'):
                if 'tipo__kodo__in' in dok_tipoj:
                    dok_tipoj['tipo__kodo__in'].append('produktado')
                else:
                    dok_tipoj = {'tipo__kodo__in': ['produktado']}

            if user.has_perm('dokumentoj.kolektanto_mikso'):
                if 'tipo__kodo__in' in dok_tipoj:
                    dok_tipoj['tipo__kodo__in'].append('kompleta_aro')
                else:
                    dok_tipoj = {'tipo__kodo__in': ['kompleta_aro']}

            homo = Kombatanto.objects.get(uzanto=user)
            cond = (models.Q(posedanto=homo.organizo, **dok_tipoj)
                    | models.Q(posedanto_magazeno=homo.organizo, **dok_tipoj)
                    | models.Q(celo_organizo=homo.organizo, **dok_tipoj)
                    | models.Q(celo_magazeno=homo.organizo, **dok_tipoj)
                    | models.Q(respondeca=homo, **dok_tipoj))

            if user.has_perm('dokumentoj.kuriero'):
                cond |= (models.Q(statuso__kodo='skribita', tipo__kodo='livero')
                         | models.Q(statuso__kodo='survoje', tipo__kodo='livero', respondeca=homo))

            return cond
        except Kombatanto.DoesNotExist:
            return models.Q()

    def save(self, *args, **kwargs):
        if self.id is None:
            id = (self.__class__.objects.filter(tipo=self.tipo, forigo=False)
                  .aggregate(models.Max('id')))['id__max']
            self.id = (id or 0) + 1
        super().save(*args, **kwargs)


class DokumentoKordo(SiriusoBazaAbstraktaKomunumoj):
    id = models.IntegerField(_('Numera linio'), blank=True, null=True)
    dokumento = models.ForeignKey(Dokumento, blank=False, null=False, on_delete=models.CASCADE,
                                  verbose_name=_('Gepatra Dokumento'))
    resurso = models.ForeignKey('resursoj.Resurso', blank=False, null=False, on_delete=models.CASCADE,
                                verbose_name=_('Resurso'))
    resurso_opcion = models.ForeignKey('resursoj.ResursoOpcion', blank=True, null=True, on_delete=models.CASCADE,
                                       verbose_name=_('Opcio pri resursoj'))
    prezo = models.FloatField(_('Ununura prezo'), blank=False, null=False, default=0)
    kvanto = models.FloatField(_('Nombro de'), blank=False, null=False, validators=[MinValueValidator(0),])
    reala_kvanto = models.FloatField(_('Reala kvanto'), blank=True, null=True,
                                     validators=[MinValueValidator(0), ])
    atributoj = pgfields.JSONField(_('Nedeviga atributo kampo'), blank=True, null=True)

    class Meta:
        db_table = 'dokumentoj_kordoj'
        verbose_name = _('Kordo de dokumento')
        verbose_name_plural = _('Kordoj de dokumentoj')

    def save(self, *args, **kwargs):
        if self.id is None:
            id = (self.__class__.objects.filter(dokumento=self.dokumento, forigo=False)
                  .aggregate(models.Max('id')))['id__max']
            self.id = (id or 0) + 1
        super().save(*args, **kwargs)


# Настройки документов
class DokumentoAgordo(SiriusoBazaAbstraktaKomunumoj):
    dokumento_tipo = models.ForeignKey(DokumentoTipo, verbose_name=_('Tipo de dokumento'), on_delete=models.CASCADE,
                                       blank=False, null=False)
    posedanto_organizo = models.ForeignKey('kombatantoj.Kombatanto', verbose_name=_('Ricevanta organizo'),
                                           related_name='agordo_posedanto_organizo', on_delete=models.CASCADE,
                                           blank=True, null=True)
    posedanto_magazeno = models.ForeignKey('kombatantoj.Kombatanto', verbose_name=_('Magazeno de ricevanta organizo'),
                                           related_name='agordo_posedanto_magazeno', on_delete=models.CASCADE,
                                           blank=True, null=True)
    celo_organizo = models.ForeignKey('kombatantoj.Kombatanto', verbose_name=_('Plenuma organizo'),
                                           related_name='agordo_celo_organizo', on_delete=models.CASCADE,
                                           blank=True, null=True)
    celo_magazeno = models.ForeignKey('kombatantoj.Kombatanto', verbose_name=_('Plenuma organizaĵo magazeno'),
                                           related_name='agordo_celo_magazeno', on_delete=models.CASCADE,
                                           blank=True, null=True)

    class Meta:
        db_table = 'dokumentoj_agordoj'
        verbose_name = _('Dokumenta agordo')
        verbose_name_plural = _('Dokumentaj agordoj')


class DokumentoHistorio(SiriusoBazaAbstrakta):
    dokumento = models.ForeignKey(Dokumento, blank=False, null=False, on_delete=models.CASCADE,
                                  verbose_name=_('Dokumento'))
    id = models.IntegerField(_('Nombro en ordo'), blank=True, null=True)
    komenco_dato = models.DateTimeField(_('Komenca dato'), blank=True, null=True)
    fina_dato = models.DateTimeField(_('Fina dato'), blank=True, null=True)
    skemo = models.ForeignKey(DokumentoSkemo, blank=False, null=False, on_delete=models.CASCADE,
                              verbose_name=_('Agaĵa Skemo de Dokumentoj'))
    result = models.NullBooleanField(_('Результат действия'), default=None)
    mesagoj = models.TextField(_('Agaj mesaĝoj'), blank=True, null=True)

    class Meta:
        db_table = 'dokumento_historio'
        verbose_name = _('Dokumenta historio')
        verbose_name_plural = _('Dokumentaj historioj')

    def save(self, *args, **kwargs):
        if self.id is None:
            id = (self.__class__.objects.filter(dokumento=self.dokumento, forigo=False)
                  .aggregate(models.Max('id')))['id__max']
            self.id = (id or 0) + 1
        super().save(*args, **kwargs)
