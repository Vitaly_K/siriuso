# Generated by Django 2.0.13 on 2019-07-17 09:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_realecoj'),
        ('dekretumoj', '0007_removing_unnecessary_komunumoj_models'),
    ]

    operations = [
        migrations.AddField(
            model_name='dekretumojdekretumosoveto',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='dekretumojdekretumosoveto',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AddField(
            model_name='dekretumojdekretumotipo',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='dekretumojdekretumotipo',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AddField(
            model_name='dekretumojkategoriosoveto',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='dekretumojkategoriosoveto',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AddField(
            model_name='dekretumojkategoriotipo',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='dekretumojkategoriotipo',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
    ]
