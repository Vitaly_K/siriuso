"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from .models import *


# Типы категорий декретумов
@admin.register(DekretumojKategorioTipo)
class DekretumojKategorioTipoAdmin(admin.ModelAdmin):
    list_display = [field.name for field in DekretumojKategorioTipo._meta.fields]

    class Meta:
        model = DekretumojKategorioTipo


# Категории декретумов советов
@admin.register(DekretumojKategorioSoveto)
class DekretumojKategorioSovetoAdmin(admin.ModelAdmin):
    list_display = [field.name for field in DekretumojKategorioSoveto._meta.fields]

    class Meta:
        model = DekretumojKategorioSoveto


# Типы исследований
@admin.register(DekretumojDekretumoTipo)
class DekretumojDekretumoTipoAdmin(admin.ModelAdmin):
    list_display = [field.name for field in DekretumojDekretumoTipo._meta.fields]

    class Meta:
        model = DekretumojDekretumoTipo


# Исследования
@admin.register(DekretumojDekretumoSoveto)
class DekretumojDekretumoSovetoAdmin(admin.ModelAdmin):
    list_display = [field.name for field in DekretumojDekretumoSoveto._meta.fields]

    class Meta:
        model = DekretumojDekretumoSoveto
