"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.db import models
from django.contrib.postgres import fields as pgfields
from django.utils.translation import ugettext_lazy as _
from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo
from main.models import SiriusoBazaAbstraktaKomunumoj, SiriusoKomentoAbstrakta, SiriusoTipoAbstrakta
from komunumoj.models import KomunumojAliro, Komunumo


# Функционал декретумов для сообществ
# Типы категорий декретумов, использует абстрактный класс SiriusoTipoAbstrakta
class DekretumojKategorioTipo(SiriusoTipoAbstrakta):

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'dekretumoj_kategorioj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de kategorioj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de kategorioj')



# Категории декретумов советов, использует абстрактный класс SiriusoBazaAbstraktaKomunumoj
class DekretumojKategorioSoveto(SiriusoBazaAbstraktaKomunumoj):

    # владелец (совет)
    posedanto = models.ForeignKey(Komunumo, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # тип категории
    tipo = models.ForeignKey(DekretumojKategorioTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название в таблице названий категорий, от туда будет браться название с нужным языковым тегом
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание в таблице описаний категорий, от туда будет браться описание с нужным языковым тегом
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # доступ к категории
    aliro = models.ForeignKey(KomunumojAliro, verbose_name=_('Aliro'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'dekretumoj_kategorioj_sovetoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj')


# Типы декретумов (справочник), использует абстрактный класс SiriusoTipoAbstrakta
class DekretumojDekretumoTipo(SiriusoTipoAbstrakta):

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'dekretumoj_dekretumoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de dekretumoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de dekretumoj')


# Декретумы советов, использует абстрактный класс SiriusoBazaAbstraktaKomunumoj
class DekretumojDekretumoSoveto(SiriusoBazaAbstraktaKomunumoj):

    # владелец (совет)
    posedanto = models.ForeignKey(Komunumo, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # категория
    kategorio = models.ForeignKey(DekretumojKategorioSoveto, verbose_name=_('Kategorio'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # тип декретума
    tipo = models.ForeignKey(DekretumojDekretumoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название в таблице названий декретумов, от туда будет браться название с нужным языковым тегом
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # текст в таблице текстов декретумов, от туда будет браться текст с нужным языковым тегом
    teksto = pgfields.JSONField(verbose_name=_('Teksto'), blank=True, null=False, default=default_lingvo,
                                encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'dekretumoj_dekretumoj_sovetoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Dekretumo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Dekretumoj')
