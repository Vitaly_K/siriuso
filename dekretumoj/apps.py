from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class DekretumojConfig(AppConfig):
    name = 'dekretumoj'
    verbose_name = _('Dekretumoj')
