"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres import fields as pgfields
from django.contrib.auth.models import Permission

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from universo_bazo.models import UniversoBazaMaks, UniversoBazaRealeco, UniversoRealeco
from universo_uzantoj.models import UniversoUzanto
from universo_organizoj.models import UniversoOrganizo
from universo_objektoj.models import UniversoObjekto


# Справочник валют в Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoMonoValuto(UniversoBazaMaks):

    # уникальный личный ID валют
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_mono_valutoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_mono_valutoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Valuto de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Valutoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_mono_valutoj', _('Povas vidi valutoj de Universo')),
            ('povas_krei_universo_mono_valutoj', _('Povas krei valutoj de Universo')),
            ('povas_forigi_universo_mono_valutoj', _('Povas forigi valutoj de Universo')),
            ('povas_shangxi_universo_mono_valutoj', _('Povas ŝanĝi valutoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(models.Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoMonoValuto, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_mono',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_mono.povas_vidi_universo_mono_valutoj', 
                'universo_mono.povas_krei_universo_mono_valutoj',
                'universo_mono.povas_forigi_universo_mono_valutoj',
                'universo_mono.povas_shangxi_universo_mono_valutoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_mono', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_mono.povas_vidi_universo_mono_valutoj')
                    or user_obj.has_perm('universo_mono.povas_vidi_universo_mono_valutoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_mono.povas_vidi_universo_mono_valutoj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond


# Типы счетов в Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoMonoKontoTipo(UniversoBazaMaks):

    # уникальный личный ID типа счетов
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # валюта типа счёта
    valuto = models.ForeignKey(UniversoMonoValuto, verbose_name=_('Valuto'), blank=True, null=True,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_mono_kontoj_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_mono_kontoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de monaj kontoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de monaj kontoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_mono_kontoj_tipoj', _('Povas vidi tipoj de monaj kontoj de Universo')),
            ('povas_krei_universo_mono_kontoj_tipoj', _('Povas krei tipoj de monaj kontoj de Universo')),
            ('povas_forigi_universo_mono_kontoj_tipoj', _('Povas forigi tipoj de monaj kontoj de Universo')),
            ('povas_shangxi_universo_mono_kontoj_tipoj', _('Povas ŝanĝi tipoj de monaj kontoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(models.Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoMonoKontoTipo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_mono',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_mono.povas_vidi_universo_mono_kontoj_tipoj', 
                'universo_mono.povas_krei_universo_mono_kontoj_tipoj',
                'universo_mono.povas_forigi_universo_mono_kontoj_tipoj',
                'universo_mono.povas_shangxi_universo_mono_kontoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_mono', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_mono.povas_vidi_universo_mono_kontoj_tipoj')
                    or user_obj.has_perm('universo_mono.povas_vidi_universo_mono_kontoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_mono.povas_vidi_universo_mono_kontoj_tipoj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond


# Шаблоны счетов (кошельков) в Универсо, использует абстрактный класс UniversoBazaRealeco
class UniversoMonoKontoSxablono(UniversoBazaRealeco):
    
    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)
    
    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # тип счёта
    tipo = models.ForeignKey(UniversoMonoKontoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # баланс счёта
    bilanco = models.FloatField(_('Bilanco'), blank=True, null=True, default=0)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_mono_kontoj_sxablonoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ŝablono de kontoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ŝablonoj de kontoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_mono_kontoj_sxablonoj', _('Povas vidi ŝablonoj de kontoj de Universo')),
            ('povas_krei_universo_mono_kontoj_sxablonoj', _('Povas krei ŝablonoj de kontoj de Universo')),
            ('povas_forigi_universo_mono_kontoj_sxablonoj', _('Povas forigi ŝablonoj de kontoj de Universo')),
            ('povas_shangxi_universo_mono_kontoj_sxablonoj', _('Povas ŝanĝi ŝablonoj de kontoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}) {} - {}'.format(self.id, self.tipo, self.bilanco)

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(models.Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoMonoKontoSxablono, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                                  update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_mono',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_mono.povas_vidi_universo_mono_kontoj_sxablonoj',
                'universo_mono.povas_krei_universo_mono_kontoj_sxablonoj',
                'universo_mono.povas_forigi_universo_mono_kontoj_sxablonoj',
                'universo_mono.povas_shangxi_universo_mono_kontoj_sxablonoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_mono', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_mono.povas_vidi_universo_mono_kontoj_sxablonoj')
                    or user_obj.has_perm('universo_mono.povas_vidi_universo_mono_kontoj_sxablonoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_mono.povas_vidi_universo_mono_kontoj_sxablonoj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond


# Счета (кошельки) в Универсо, использует абстрактный класс UniversoBazaRealeco
class UniversoMonoKonto(UniversoBazaRealeco):

    # тип счёта
    tipo = models.ForeignKey(UniversoMonoKontoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # пользователь владелец счёта в Универсо
    posedanto_uzanto = models.ForeignKey(UniversoUzanto, verbose_name=_('Uzanto posedanto'), blank=True, null=True,
                                         default=None, on_delete=models.CASCADE)

    # организация владелец счёта в Универсо
    posedanto_organizo = models.ForeignKey(UniversoOrganizo, verbose_name=_('Organizo posedanto'), blank=True,
                                           null=True, default=None, on_delete=models.CASCADE)

    # баланс счёта
    bilanco = models.FloatField(_('Bilanco'), blank=True, null=True, default=0)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_mono_kontoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Konto de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kontoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_mono_kontoj', _('Povas vidi kontoj de Universo')),
            ('povas_krei_universo_mono_kontoj', _('Povas krei kontoj de Universo')),
            ('povas_forigi_universo_mono_kontoj', _('Povas forigi kontoj de Universo')),
            ('povas_shangxi_universo_mono_kontoj', _('Povas ŝanĝi kontoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_mono',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_mono.povas_vidi_universo_mono_kontoj', 
                'universo_mono.povas_krei_universo_mono_kontoj',
                'universo_mono.povas_forigi_universo_mono_kontoj',
                'universo_mono.povas_shangxi_universo_mono_kontoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_mono', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_mono.povas_vidi_universo_mono_kontoj')
                    or user_obj.has_perm('universo_mono.povas_vidi_universo_mono_kontoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_mono.povas_vidi_universo_mono_kontoj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond


# Типы инвойсов (счетов на оплату), использует абстрактный класс UniversoBazaMaks
class UniversoMonoFakturoTipo(UniversoBazaMaks):

    # уникальный личный ID типа инвойса
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_mono_fakturoj_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_mono_fakturoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de fakturoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de fakturoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_mono_fakturoj_tipoj', _('Povas vidi tipoj de fakturoj de Universo')),
            ('povas_krei_universo_mono_fakturoj_tipoj', _('Povas krei tipoj de fakturoj de Universo')),
            ('povas_forigi_universo_mono_fakturoj_tipoj', _('Povas forigi tipoj de fakturoj de Universo')),
            ('povas_shangxi_universo_mono_fakturoj_tipoj', _('Povas ŝanĝi tipoj de fakturoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(models.Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoMonoFakturoTipo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                                  update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_mono',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_mono.povas_vidi_universo_mono_fakturoj_tipoj', 
                'universo_mono.povas_krei_universo_mono_fakturoj_tipoj',
                'universo_mono.povas_forigi_universo_mono_fakturoj_tipoj',
                'universo_mono.povas_shangxi_universo_mono_fakturoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_mono', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_mono.povas_vidi_universo_mono_fakturoj_tipoj')
                    or user_obj.has_perm('universo_mono.povas_vidi_universo_mono_fakturoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_mono.povas_vidi_universo_mono_fakturoj_tipoj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond


# Инвойсы (счета на оплату), использует абстрактный класс UniversoBazaRealeco
class UniversoMonoFakturo(UniversoBazaRealeco):

    # тип инвойса
    tipo = models.ForeignKey(UniversoMonoFakturoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # выставитель инвойса
    ekspedanto = models.ForeignKey(UniversoMonoKonto, verbose_name=_('Ekspedanto'), blank=False, null=False,
                                   default=None, on_delete=models.CASCADE)

    # получатель инвойса
    ricevanto = models.ForeignKey(UniversoMonoKonto, verbose_name=_('Ricevanto'), blank=False, null=False, default=None,
                                  related_name='%(app_label)s_%(class)s_ricevanto', on_delete=models.CASCADE)

    # сумма инвойса по всем позициям
    sumo = models.FloatField(_('Sumo'), blank=False, null=True, default=0)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_mono_fakturoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Fakturo de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Fakturoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_mono_fakturoj', _('Povas vidi fakturoj de Universo')),
            ('povas_krei_universo_mono_fakturoj', _('Povas krei fakturoj de Universo')),
            ('povas_forigi_universo_mono_fakturoj', _('Povas forigi fakturoj de Universo')),
            ('povas_shangxi_universo_mono_fakturoj', _('Povas ŝanĝi fakturoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_mono',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_mono.povas_vidi_universo_mono_fakturoj', 
                'universo_mono.povas_krei_universo_mono_fakturoj',
                'universo_mono.povas_forigi_universo_mono_fakturoj',
                'universo_mono.povas_shangxi_universo_mono_fakturoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_mono', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_mono.povas_vidi_universo_mono_fakturoj')
                    or user_obj.has_perm('universo_mono.povas_vidi_universo_mono_fakturoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_mono.povas_vidi_universo_mono_fakturoj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond


# Объекты указанные в инвойсах (содержимое табличной части), использует абстрактный класс UniversoBazaRealeco
class UniversoMonoFakturoObjekto(UniversoBazaRealeco):

    # инвойс к которому относится запись
    fakturo = models.ForeignKey(UniversoMonoFakturo, verbose_name=_('Fakturo'), blank=False, null=False, default=None,
                                on_delete=models.CASCADE)

    # объект инвойса (на каждый объект отдельная запись в этой моделе)
    objekto = models.ForeignKey(UniversoObjekto, verbose_name=_('Objekto'), blank=False, null=False, default=None,
                                on_delete=models.CASCADE)

    # цена этой позиции
    prezo = models.FloatField(_('Prezo'), blank=False, null=True, default=0)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_mono_fakturoj_objektoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Objekto de fakturo de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Objektoj de fakturoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_mono_fakturoj_objektoj', _('Povas vidi objektoj de fakturoj de Universo')),
            ('povas_krei_universo_mono_fakturoj_objektoj', _('Povas krei objektoj de fakturoj de Universo')),
            ('povas_forigi_universo_mono_fakturoj_objektoj', _('Povas forigi objektoj de fakturoj de Universo')),
            ('povas_shangxi_universo_mono_fakturoj_objektoj', _('Povas ŝanĝi objektoj de fakturoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_mono',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_mono.povas_vidi_universo_mono_fakturoj_objektoj', 
                'universo_mono.povas_krei_universo_mono_fakturoj_objektoj',
                'universo_mono.povas_forigi_universo_mono_fakturoj_objektoj',
                'universo_mono.povas_shangxi_universo_mono_fakturoj_objektoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_mono', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_mono.povas_vidi_universo_mono_fakturoj_objektoj')
                    or user_obj.has_perm('universo_mono.povas_vidi_universo_mono_fakturoj_objektoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_mono.povas_vidi_universo_mono_fakturoj_objektoj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond


# Типы транзакций между счетами (кошельками), использует абстрактный класс UniversoBazaMaks
class UniversoMonoTransakcioTipo(UniversoBazaMaks):

    # уникальный личный ID типа транзакции
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_mono_transakcioj_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_mono_transakcioj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de transakcioj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de transakcioj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_mono_transakcioj_tipoj', _('Povas vidi tipoj de transakcioj de Universo')),
            ('povas_krei_universo_mono_transakcioj_tipoj', _('Povas krei tipoj de transakcioj de Universo')),
            ('povas_forigi_universo_mono_transakcioj_tipoj', _('Povas forigi tipoj de transakcioj de Universo')),
            ('povas_shangxi_universo_mono_transakcioj_tipoj', _('Povas ŝanĝi tipoj de transakcioj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(models.Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoMonoTransakcioTipo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                                     update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_mono',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_mono.povas_vidi_universo_mono_transakcioj_tipoj', 
                'universo_mono.povas_krei_universo_mono_transakcioj_tipoj',
                'universo_mono.povas_forigi_universo_mono_transakcioj_tipoj',
                'universo_mono.povas_shangxi_universo_mono_transakcioj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_mono', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_mono.povas_vidi_universo_mono_transakcioj_tipoj')
                    or user_obj.has_perm('universo_mono.povas_vidi_universo_mono_transakcioj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_mono.povas_vidi_universo_mono_transakcioj_tipoj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond


# Транзакции между счетами (кошельками), использует абстрактный класс UniversoBazaRealeco
class UniversoMonoTransakcio(UniversoBazaRealeco):

    # тип транзакции
    tipo = models.ForeignKey(UniversoMonoTransakcioTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # инвойс на основе которого делается транзакция
    fakturo = models.ForeignKey(UniversoMonoFakturo, verbose_name=_('Fakturo'), blank=False, null=False,
                                default=None, on_delete=models.CASCADE)

    # отправитель платежа
    ekspedanto = models.ForeignKey(UniversoMonoKonto, verbose_name=_('Ekspedanto'), blank=False, null=False,
                                   default=None, on_delete=models.CASCADE)

    # получатель платежа
    ricevanto = models.ForeignKey(UniversoMonoKonto, verbose_name=_('Ricevanto'), blank=False, null=False, default=None,
                                  related_name='%(app_label)s_%(class)s_ricevanto', on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_mono_transakcioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Mona transakcio de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Monaj transakcioj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_mono_transakcioj', _('Povas vidi monaj transakcioj de Universo')),
            ('povas_krei_universo_mono_transakcioj', _('Povas krei monaj transakcioj de Universo')),
            ('povas_forigi_universo_mono_transakcioj', _('Povas forigi monaj transakcioj de Universo')),
            ('povas_shangxi_universo_mono_transakcioj', _('Povas ŝanĝi monaj transakcioj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_mono',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_mono.povas_vidi_universo_mono_transakcioj', 
                'universo_mono.povas_krei_universo_mono_transakcioj',
                'universo_mono.povas_forigi_universo_mono_transakcioj',
                'universo_mono.povas_shangxi_universo_mono_transakcioj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_mono', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_mono.povas_vidi_universo_mono_transakcioj')
                    or user_obj.has_perm('universo_mono.povas_vidi_universo_mono_transakcioj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_mono.povas_vidi_universo_mono_transakcioj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond
