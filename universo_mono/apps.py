from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UniversoMonoConfig(AppConfig):
    name = 'universo_mono'
    verbose_name = _('Mono de Universo')

