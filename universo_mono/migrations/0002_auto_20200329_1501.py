# Generated by Django 2.2.11 on 2020-03-29 15:01

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('universo_bazo', '0002_universorealeco_id'),
        ('universo_uzantoj', '0002_auto_20200327_0357'),
        ('universo_mono', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='universomonofakturo',
            options={'permissions': (('povas_vidi_universo_mono_fakturoj', 'Povas vidi fakturoj de Universo'), ('povas_krei_universo_mono_fakturoj', 'Povas krei fakturoj de Universo'), ('povas_forigi_universo_mono_fakturoj', 'Povas forigi fakturoj de Universo'), ('povas_shangxi_universo_mono_fakturoj', 'Povas ŝanĝi fakturoj de Universo')), 'verbose_name': 'Fakturo de Universo', 'verbose_name_plural': 'Fakturoj de Universo'},
        ),
        migrations.AlterModelOptions(
            name='universomonofakturoobjekto',
            options={'permissions': (('povas_vidi_universo_mono_fakturoj_objektoj', 'Povas vidi objektoj de fakturoj de Universo'), ('povas_krei_universo_mono_fakturoj_objektoj', 'Povas krei objektoj de fakturoj de Universo'), ('povas_forigi_universo_mono_fakturoj_objektoj', 'Povas forigi objektoj de fakturoj de Universo'), ('povas_shangxi_universo_mono_fakturoj_objektoj', 'Povas ŝanĝi objektoj de fakturoj de Universo')), 'verbose_name': 'Objekto de fakturo de Universo', 'verbose_name_plural': 'Objektoj de fakturoj de Universo'},
        ),
        migrations.AlterModelOptions(
            name='universomonofakturotipo',
            options={'permissions': (('povas_vidi_universo_mono_fakturoj_tipoj', 'Povas vidi tipoj de fakturoj de Universo'), ('povas_krei_universo_mono_fakturoj_tipoj', 'Povas krei tipoj de fakturoj de Universo'), ('povas_forigi_universo_mono_fakturoj_tipoj', 'Povas forigi tipoj de fakturoj de Universo'), ('povas_shangxi_universo_mono_fakturoj_tipoj', 'Povas ŝanĝi tipoj de fakturoj de Universo')), 'verbose_name': 'Tipo de fakturoj de Universo', 'verbose_name_plural': 'Tipoj de fakturoj de Universo'},
        ),
        migrations.AlterModelOptions(
            name='universomonokonto',
            options={'permissions': (('povas_vidi_universo_mono_kontoj', 'Povas vidi kontoj de Universo'), ('povas_krei_universo_mono_kontoj', 'Povas krei kontoj de Universo'), ('povas_forigi_universo_mono_kontoj', 'Povas forigi kontoj de Universo'), ('povas_shangxi_universo_mono_kontoj', 'Povas ŝanĝi kontoj de Universo')), 'verbose_name': 'Konto de Universo', 'verbose_name_plural': 'Kontoj de Universo'},
        ),
        migrations.AlterModelOptions(
            name='universomonokontotipo',
            options={'permissions': (('povas_vidi_universo_mono_kontoj_tipoj', 'Povas vidi tipoj de monaj kontoj de Universo'), ('povas_krei_universo_mono_kontoj_tipoj', 'Povas krei tipoj de monaj kontoj de Universo'), ('povas_forigi_universo_mono_kontoj_tipoj', 'Povas forigi tipoj de monaj kontoj de Universo'), ('povas_shangxi_universo_mono_kontoj_tipoj', 'Povas ŝanĝi tipoj de monaj kontoj de Universo')), 'verbose_name': 'Tipo de monaj kontoj de Universo', 'verbose_name_plural': 'Tipoj de monaj kontoj de Universo'},
        ),
        migrations.AlterModelOptions(
            name='universomonotransakcio',
            options={'permissions': (('povas_vidi_universo_mono_transakcioj', 'Povas vidi monaj transakcioj de Universo'), ('povas_krei_universo_mono_transakcioj', 'Povas krei monaj transakcioj de Universo'), ('povas_forigi_universo_mono_transakcioj', 'Povas forigi monaj transakcioj de Universo'), ('povas_shangxi_universo_mono_transakcioj', 'Povas ŝanĝi monaj transakcioj de Universo')), 'verbose_name': 'Mona transakcio de Universo', 'verbose_name_plural': 'Monaj transakcioj de Universo'},
        ),
        migrations.AlterModelOptions(
            name='universomonotransakciotipo',
            options={'permissions': (('povas_vidi_universo_mono_transakcioj_tipoj', 'Povas vidi tipoj de transakcioj de Universo'), ('povas_krei_universo_mono_transakcioj_tipoj', 'Povas krei tipoj de transakcioj de Universo'), ('povas_forigi_universo_mono_transakcioj_tipoj', 'Povas forigi tipoj de transakcioj de Universo'), ('povas_shangxi_universo_mono_transakcioj_tipoj', 'Povas ŝanĝi tipoj de transakcioj de Universo')), 'verbose_name': 'Tipo de transakcioj de Universo', 'verbose_name_plural': 'Tipoj de transakcioj de Universo'},
        ),
        migrations.AlterModelOptions(
            name='universomonovaluto',
            options={'permissions': (('povas_vidi_universo_mono_valutoj', 'Povas vidi valutoj de Universo'), ('povas_krei_universo_mono_valutoj', 'Povas krei valutoj de Universo'), ('povas_forigi_universo_mono_valutoj', 'Povas forigi valutoj de Universo'), ('povas_shangxi_universo_mono_valutoj', 'Povas ŝanĝi valutoj de Universo')), 'verbose_name': 'Valuto de Universo', 'verbose_name_plural': 'Valutoj de Universo'},
        ),
        migrations.CreateModel(
            name='UniversoMonoKontoSxablono',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(blank=True, default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('id', models.IntegerField(default=0, unique=True, verbose_name='ID')),
                ('bilanco', models.FloatField(blank=True, default=0, null=True, verbose_name='Bilanco')),
                ('autoro', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='universo_uzantoj.UniversoUzanto', verbose_name='Aŭtoro')),
                ('realeco', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='universo_bazo.UniversoRealeco', verbose_name='Realeco de Universo')),
                ('tipo', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='universo_mono.UniversoMonoKontoTipo', verbose_name='Tipo')),
            ],
            options={
                'verbose_name': 'Ŝablono de kontoj de Universo',
                'verbose_name_plural': 'Ŝablonoj de kontoj de Universo',
                'db_table': 'universo_mono_kontoj_sxablonoj',
                'permissions': (('povas_vidi_universo_mono_kontoj_sxablonoj', 'Povas vidi ŝablonoj de kontoj de Universo'), ('povas_krei_universo_mono_kontoj_sxablonoj', 'Povas krei ŝablonoj de kontoj de Universo'), ('povas_forigi_universo_mono_kontoj_sxablonoj', 'Povas forigi ŝablonoj de kontoj de Universo'), ('povas_shangxi_universo_mono_kontoj_sxablonoj', 'Povas ŝanĝi ŝablonoj de kontoj de Universo')),
            },
        ),
    ]
