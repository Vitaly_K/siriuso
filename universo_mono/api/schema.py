import graphene  # сам Graphene
from django.utils.translation import ugettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import *  # модели приложения


# Модель справочника валют в Универсо
class UniversoMonoValutoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoMonoValuto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов счетов в Универсо
class UniversoMonoKontoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoMonoKontoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact'],
            'valuto__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель шаблонов счетов (кошельков) в Универсо
class UniversoMonoKontoSxablonoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoMonoKontoSxablono
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact'],
            'tipo__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель счетов (кошельков) в Универсо
class UniversoMonoKontoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = UniversoMonoKonto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'posedanto_uzanto__siriuso_uzanto__id': ['exact'],
            'posedanto_uzanto__siriuso_uzanto__uuid': ['exact'],
            'tipo__id': ['exact'],
            'posedanto_organizo__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов инвойсов (счетов на оплату) Универсо
class UniversoMonoFakturoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoMonoFakturoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель инвойсов (счетов на оплату) в Универсо
class UniversoMonoFakturoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = UniversoMonoFakturo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'tipo__id': ['exact'],
            'ekspedanto__uuid': ['exact'],
            'ricevanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель объектов указанные в инвойсах (содержимое табличной части) в Универсо
class UniversoMonoFakturoObjektoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = UniversoMonoFakturoObjekto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'fakturo__uuid': ['exact'],
            'objekto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов транзакций между счетами (кошельками) Универсо
class UniversoMonoTransakcioTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoMonoTransakcioTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель транзакций между счетами (кошельками) Универсо
class UniversoMonoTransakcioNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = UniversoMonoTransakcio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'tipo_id': ['exact'],
            'fakturo__uuid': ['exact'],
            'ekspedanto__uuid': ['exact'],
            'ricevanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


class UniversoMonoQuery(graphene.ObjectType):
    universo_mono_valutoj = SiriusoFilterConnectionField(
        UniversoMonoValutoNode,
        description=_('Выводит все доступные модели справочника валют в Универсо')
    )
    universo_mono_kontoj_tipoj = SiriusoFilterConnectionField(
        UniversoMonoKontoTipoNode,
        description=_('Выводит все доступные модели типов счетов в Универсо')
    )
    universo_mono_kontoj = SiriusoFilterConnectionField(
        UniversoMonoKontoNode,
        description=_('Выводит все доступные модели счетов (кошельков) в Универсо')
    )
    universo_mono_fakturoj_tipoj = SiriusoFilterConnectionField(
        UniversoMonoFakturoTipoNode,
        description=_('Выводит все доступные модели типов инвойсов (счетов на оплату) Универсо')
    )
    universo_mono_fakturoj= SiriusoFilterConnectionField(
        UniversoMonoFakturoNode,
        description=_('Выводит все доступные модели инвойсов (счетов на оплату) в Универсо')
    )
    universo_mono_fakturoj_objektoj= SiriusoFilterConnectionField(
        UniversoMonoFakturoObjektoNode,
        description=_('Выводит все доступные модели объектов указанные в инвойсах (содержимое табличной части) в Универсо')
    )
    universo_mono_transakcioj_tipoj = SiriusoFilterConnectionField(
        UniversoMonoTransakcioTipoNode,
        description=_('Выводит все доступные модели типов транзакций между счетами (кошельками) Универсо')
    )
    universo_mono_transakcioj= SiriusoFilterConnectionField(
        UniversoMonoTransakcioNode,
        description=_('Выводит все доступные модели транзакций между счетами (кошельками) Универсо')
    )
