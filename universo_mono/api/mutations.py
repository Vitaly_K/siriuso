"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
16.05.2019
"""
from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from universo_uzantoj.models import UniversoUzanto
from .schema import *
from ..models import *


# Модель справочника валют в Универсо
class RedaktuUniversoMonoValuto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_mono_valutoj = graphene.Field(UniversoMonoValutoNode, 
        description=_('Созданный/изменённый справочник валют в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_mono_valutoj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_kosmo.povas_krei_universo_mono_valutoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_mono_valutoj = UniversoMonoValuto.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_mono_valutoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_mono_valutoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_mono_valutoj.realeco.set(realeco)
                                else:
                                    universo_mono_valutoj.realeco.clear()

                            universo_mono_valutoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_mono_valutoj = UniversoMonoValuto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_kosmo.povas_forigi_universo_mono_valutoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_kosmo.povas_shanghi_universo_mono_valutoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_mono_valutoj.forigo = kwargs.get('forigo', universo_mono_valutoj.forigo)
                                universo_mono_valutoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_mono_valutoj.arkivo = kwargs.get('arkivo', universo_mono_valutoj.arkivo)
                                universo_mono_valutoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_mono_valutoj.publikigo = kwargs.get('publikigo', universo_mono_valutoj.publikigo)
                                universo_mono_valutoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_mono_valutoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_mono_valutoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_mono_valutoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_mono_valutoj.realeco.set(realeco)
                                    else:
                                        universo_mono_valutoj.realeco.clear()

                                universo_mono_valutoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoMonoValuto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoMonoValuto(status=status, message=message, universo_mono_valutoj=universo_mono_valutoj)


# Модель типов счетов в Универсо
class RedaktuUniversoMonoKontoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_mono_kontoj_tipoj = graphene.Field(UniversoMonoKontoTipoNode, 
        description=_('Созданный/изменённый тип счетов в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))
        valuto_id = graphene.Int(description=_('Валюта типа счёта'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_mono_kontoj_tipoj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        valuto = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_kosmo.povas_krei_universo_mono_kontoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            if 'valuto_id' in kwargs:
                                try:
                                    valuto = UniversoMonoValuto.objects.get(id=kwargs.get('valuto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoMonoValuto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная валюта типа счёта Универсо'),'valuto_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'valuto_id')

                        if not message:
                            universo_mono_kontoj_tipoj = UniversoMonoKontoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                valuto = valuto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_mono_kontoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_mono_kontoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_mono_kontoj_tipoj.realeco.set(realeco)
                                else:
                                    universo_mono_kontoj_tipoj.realeco.clear()

                            universo_mono_kontoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False) or kwargs.get('valuto_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_mono_kontoj_tipoj = UniversoMonoKontoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_kosmo.povas_forigi_universo_mono_kontoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_kosmo.povas_shanghi_universo_mono_kontoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:
                                if 'valuto_id' in kwargs:
                                    try:
                                        valuto = UniversoMonoValuto.objects.get(id=kwargs.get('valuto_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoMonoValuto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная валюта типа счёта Универсо'),'valuto_id')

                            if not message:

                                universo_mono_kontoj_tipoj.forigo = kwargs.get('forigo', universo_mono_kontoj_tipoj.forigo)
                                universo_mono_kontoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_mono_kontoj_tipoj.arkivo = kwargs.get('arkivo', universo_mono_kontoj_tipoj.arkivo)
                                universo_mono_kontoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_mono_kontoj_tipoj.publikigo = kwargs.get('publikigo', universo_mono_kontoj_tipoj.publikigo)
                                universo_mono_kontoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_mono_kontoj_tipoj.autoro = autoro
                                universo_mono_kontoj_tipoj.valuto = valuto if kwargs.get('valuto_id', False) else universo_mono_kontoj_tipoj.valuto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_mono_kontoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_mono_kontoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_mono_kontoj_tipoj.realeco.set(realeco)
                                    else:
                                        universo_mono_kontoj_tipoj.realeco.clear()

                                universo_mono_kontoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoMonoKontoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoMonoKontoTipo(status=status, message=message, universo_mono_kontoj_tipoj=universo_mono_kontoj_tipoj)


# Модель шаблонов счетов (кошельков) в Универсо
class RedaktuUniversoMonoKontoSxablono(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_mono_kontoj_sxablonoj = graphene.Field(UniversoMonoKontoSxablonoNode, 
        description=_('Созданный/изменённый шаблон счета (кошелька) в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        tipo_id = graphene.Int(description=_('Тип валюты счёта'))
        bilanco = graphene.Float(description=_('Баланс счёта'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_mono_kontoj_sxablonoj = None
        autoro = None
        realeco = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_kosmo.povas_krei_universo_mono_kontoj_sxablonoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoRealeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Указана несуществующая реальность Универсо'),'realeco_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoMonoKontoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoMonoKontoTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип валюты счёта Универсо'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            universo_mono_kontoj_sxablonoj = UniversoMonoKontoSxablono.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                tipo = tipo,
                                realeco = realeco,
                                bilanco = kwargs.get('bilanco', 0.0),
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_mono_kontoj_sxablonoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_mono_kontoj_sxablonoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            universo_mono_kontoj_sxablonoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('tipo_id', False)
                            or kwargs.get('bilanco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_mono_kontoj_sxablonoj = UniversoMonoKontoSxablono.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_kosmo.povas_forigi_universo_mono_kontoj_sxablonoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_kosmo.povas_shanghi_universo_mono_kontoj_sxablonoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')

                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoRealeco.DoesNotExist:
                                        message = '{} "{}"'.format(_('Указана несуществующая реальность Универсо'),'realeco_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoMonoKontoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoMonoKontoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип валюты счёта Универсо'),'tipo_id')

                            if not message:

                                universo_mono_kontoj_sxablonoj.forigo = kwargs.get('forigo', universo_mono_kontoj_sxablonoj.forigo)
                                universo_mono_kontoj_sxablonoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_mono_kontoj_sxablonoj.arkivo = kwargs.get('arkivo', universo_mono_kontoj_sxablonoj.arkivo)
                                universo_mono_kontoj_sxablonoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_mono_kontoj_sxablonoj.publikigo = kwargs.get('publikigo', universo_mono_kontoj_sxablonoj.publikigo)
                                universo_mono_kontoj_sxablonoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_mono_kontoj_sxablonoj.realeco = realeco if kwargs.get('realeco_id', False) \
                                    else universo_mono_kontoj_sxablonoj.realeco
                                universo_mono_kontoj_sxablonoj.tipo = tipo if kwargs.get('tipo_id', False) \
                                    else universo_mono_kontoj_sxablonoj.tipo
                                universo_mono_kontoj_sxablonoj.autoro = autoro
                                universo_mono_kontoj_sxablonoj.bilanco = kwargs.get('bilanco', universo_mono_kontoj_sxablonoj.bilanco)

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_mono_kontoj_sxablonoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_mono_kontoj_sxablonoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_mono_kontoj_sxablonoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoMonoKontoSxablono.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoMonoKontoSxablono(status=status, message=message, universo_mono_kontoj_sxablonoj=universo_mono_kontoj_sxablonoj)


# Модель счетов (кошельков) в Универсо
class RedaktuUniversoMonoKonto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_mono_kontoj = graphene.Field(UniversoMonoKontoNode, 
        description=_('Созданный/изменённый счет (кошелёк) в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        tipo_id = graphene.Int(description=_('Тип валюты счёта'))
        posedanto_uzanto_uuid = graphene.String(description=_('Пользователь владелец счёта в Универсо'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец счёта в Универсо'))
        bilanco = graphene.Float(description=_('Баланс счёта'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_mono_kontoj = None
        posedanto_uzanto = None
        posedanto_organizo = None
        realeco = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_kosmo.povas_krei_universo_mono_kontoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoRealeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Указана несуществующая реальность Универсо'),'realeco_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoMonoKontoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoMonoKontoTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип валюты счёта Универсо'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'posedanto_uzanto_uuid' in kwargs:
                                try:
                                    posedanto_uzanto = UniversoUzanto.objects.get(
                                        uuid=kwargs.get('posedanto_uzanto_uuid'), 
                                        forigo=False, 
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except UniversoUzanto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный пользователь Универсо'),'posedanto_uzanto_uuid')

                        if not message:
                            if 'posedanto_organizo_uuid' in kwargs:
                                try:
                                    posedanto_organizo = UniversoOrganizo.objects.get(id=kwargs.get('posedanto_organizo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoOrganizo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная организация владелец счёта в Универсо'),'posedanto_organizo_uuid')

                        if not message:
                            universo_mono_kontoj = UniversoMonoKonto.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto_uzanto = posedanto_uzanto,
                                posedanto_organizo = posedanto_organizo,
                                tipo = tipo,
                                realeco = realeco,
                                bilanco = kwargs.get('bilanco', 0.0),
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            universo_mono_kontoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto_uzanto_uuid', False) 
                            or kwargs.get('posedanto_organizo_uuid', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('tipo_id', False)
                            or kwargs.get('bilanco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_mono_kontoj = UniversoMonoKonto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_kosmo.povas_forigi_universo_mono_kontoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_kosmo.povas_shanghi_universo_mono_kontoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoRealeco.DoesNotExist:
                                        message = '{} "{}"'.format(_('Указана несуществующая реальность Универсо'),'realeco_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoMonoKontoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoMonoKontoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип валюты счёта Универсо'),'tipo_id')

                            if not message:
                                if 'posedanto_uzanto_uuid' in kwargs:
                                    try:
                                        posedanto_uzanto = UniversoUzanto.objects.get(
                                            uuid=kwargs.get('posedanto_uzanto_uuid'), 
                                            forigo=False, 
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except UniversoUzanto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный пользователь Универсо'),'posedanto_uzanto_uuid')

                            if not message:
                                if 'posedanto_organizo_uuid' in kwargs:
                                    try:
                                        posedanto_organizo = UniversoOrganizo.objects.get(id=kwargs.get('posedanto_organizo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoOrganizo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная организация владелец счёта в Универсо'),'posedanto_organizo_uuid')

                            if not message:

                                universo_mono_kontoj.forigo = kwargs.get('forigo', universo_mono_kontoj.forigo)
                                universo_mono_kontoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_mono_kontoj.arkivo = kwargs.get('arkivo', universo_mono_kontoj.arkivo)
                                universo_mono_kontoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_mono_kontoj.publikigo = kwargs.get('publikigo', universo_mono_kontoj.publikigo)
                                universo_mono_kontoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_mono_kontoj.realeco = realeco if kwargs.get('realeco_id', False) \
                                    else universo_mono_kontoj.realeco
                                universo_mono_kontoj.tipo = tipo if kwargs.get('tipo_id', False) \
                                    else universo_mono_kontoj.tipo
                                universo_mono_kontoj.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_uuid', False) \
                                    else universo_mono_kontoj.posedanto_uzanto
                                universo_mono_kontoj.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) \
                                    else universo_mono_kontoj.posedanto_organizo
                                universo_mono_kontoj.bilanco = kwargs.get('bilanco', universo_mono_kontoj.bilanco)

                                universo_mono_kontoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoMonoKonto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoMonoKonto(status=status, message=message, universo_mono_kontoj=universo_mono_kontoj)


# Модель типов инвойсов (счетов на оплату) в Универсо
class RedaktuUniversoMonoFakturoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_mono_fakturoj_tipoj = graphene.Field(UniversoMonoFakturoTipoNode, 
        description=_('Созданный/изменённый тип инвойсов (счетов на оплату) в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_mono_fakturoj_tipoj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_kosmo.povas_krei_universo_mono_fakturoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_mono_fakturoj_tipoj = UniversoMonoFakturoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_mono_fakturoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_mono_fakturoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_mono_fakturoj_tipoj.realeco.set(realeco)
                                else:
                                    universo_mono_fakturoj_tipoj.realeco.clear()

                            universo_mono_fakturoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_mono_fakturoj_tipoj = UniversoMonoFakturoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_kosmo.povas_forigi_universo_mono_fakturoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_kosmo.povas_shanghi_universo_mono_fakturoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')

                            if not message:

                                universo_mono_fakturoj_tipoj.forigo = kwargs.get('forigo', universo_mono_fakturoj_tipoj.forigo)
                                universo_mono_fakturoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_mono_fakturoj_tipoj.arkivo = kwargs.get('arkivo', universo_mono_fakturoj_tipoj.arkivo)
                                universo_mono_fakturoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_mono_fakturoj_tipoj.publikigo = kwargs.get('publikigo', universo_mono_fakturoj_tipoj.publikigo)
                                universo_mono_fakturoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_mono_fakturoj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_mono_fakturoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_mono_fakturoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_mono_fakturoj_tipoj.realeco.set(realeco)
                                    else:
                                        universo_mono_fakturoj_tipoj.realeco.clear()

                                universo_mono_fakturoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoMonoFakturoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoMonoFakturoTipo(status=status, message=message, universo_mono_fakturoj_tipoj=universo_mono_fakturoj_tipoj)


# Модель инвойсов (счетов на оплату) в Универсо
class RedaktuUniversoMonoFakturo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_mono_fakturoj = graphene.Field(UniversoMonoFakturoNode, 
        description=_('Созданный/изменённый инвойс (счет на оплату) в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        tipo_id = graphene.Int(description=_('Тип инвойса'))
        ekspedanto_uuid = graphene.String(description=_('Выставитель инвойса в Универсо'))
        ricevanto_uuid = graphene.String(description=_('Получатель инвойса в Универсо'))
        sumo = graphene.Float(description=_('Сумма инвойса по всем позициям'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_mono_fakturoj = None
        ekspedanto = None
        ricevanto = None
        realeco = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_kosmo.povas_krei_universo_mono_fakturoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoRealeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Указана несуществующая реальность Универсо'),'realeco_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoMonoFakturoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoMonoFakturoTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип инвойса Универсо'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'ekspedanto_uuid' in kwargs:
                                try:
                                    ekspedanto = UniversoMonoKonto.objects.get(
                                        uuid=kwargs.get('ekspedanto_uuid'), 
                                        forigo=False, 
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except UniversoMonoKonto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный выставитель инвойса Универсо'),'ekspedanto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'ekspedanto_uuid')

                        if not message:
                            if 'ricevanto_uuid' in kwargs:
                                try:
                                    ricevanto = UniversoMonoKonto.objects.get(id=kwargs.get('ricevanto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoMonoKonto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный получатель инвойса Универсо'),'ricevanto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'ricevanto_uuid')

                        if not message:
                            universo_mono_fakturoj = UniversoMonoFakturo.objects.create(
                                forigo=False,
                                arkivo=False,
                                ekspedanto = ekspedanto,
                                ricevanto = ricevanto,
                                tipo = tipo,
                                realeco = realeco,
                                sumo = kwargs.get('sumo', 0.0),
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            universo_mono_fakturoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('ekspedanto_uuid', False) 
                            or kwargs.get('ricevanto_uuid', False) or kwargs.get('sumo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('tipo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_mono_fakturoj = UniversoMonoFakturo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_kosmo.povas_forigi_universo_mono_fakturoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_kosmo.povas_shanghi_universo_mono_fakturoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoRealeco.DoesNotExist:
                                        message = '{} "{}"'.format(_('Указана несуществующая реальность Универсо'),'realeco_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoMonoFakturoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoMonoFakturoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип инвойса Универсо'),'tipo_id')

                            if not message:
                                if 'ekspedanto_uuid' in kwargs:
                                    try:
                                        ekspedanto = UniversoMonoKonto.objects.get(
                                            uuid=kwargs.get('ekspedanto_uuid'), 
                                            forigo=False, 
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except UniversoMonoKonto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный выставитель инвойса Универсо'),'ekspedanto_uuid')

                            if not message:
                                if 'ricevanto_uuid' in kwargs:
                                    try:
                                        ricevanto = UniversoMonoKonto.objects.get(id=kwargs.get('ricevanto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoMonoKonto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный получатель инвойса Универсо'),'ricevanto_uuid')

                            if not message:

                                universo_mono_fakturoj.forigo = kwargs.get('forigo', universo_mono_fakturoj.forigo)
                                universo_mono_fakturoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_mono_fakturoj.arkivo = kwargs.get('arkivo', universo_mono_fakturoj.arkivo)
                                universo_mono_fakturoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_mono_fakturoj.publikigo = kwargs.get('publikigo', universo_mono_fakturoj.publikigo)
                                universo_mono_fakturoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_mono_fakturoj.sumo = kwargs.get('sumo', universo_mono_fakturoj.sumo)
                                universo_mono_fakturoj.realeco = realeco if kwargs.get('realeco_id', False) \
                                    else universo_mono_fakturoj.realeco
                                universo_mono_fakturoj.tipo = tipo if kwargs.get('tipo_id', False) \
                                    else universo_mono_fakturoj.tipo
                                universo_mono_fakturoj.ekspedanto = ekspedanto if kwargs.get('ekspedanto_uuid', False) \
                                    else universo_mono_fakturoj.ekspedanto
                                universo_mono_fakturoj.ricevanto = ricevanto if kwargs.get('ricevanto_uuid', False) \
                                    else universo_mono_fakturoj.ricevanto

                                universo_mono_fakturoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoMonoFakturo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoMonoFakturo(status=status, message=message, universo_mono_fakturoj=universo_mono_fakturoj)


# Модель объектов указанных в инвойсах (содержимое табличной части) в Универсо
class RedaktuUniversoMonoFakturoObjekto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_mono_fakturoj_objektoj = graphene.Field(UniversoMonoFakturoObjektoNode, 
        description=_('Созданный/изменённый объект указанный в инвойсах (содержимое табличной части) в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        fakturo_uuid = graphene.String(description=_('Выставитель инвойса в Универсо'))
        objekto_uuid = graphene.String(description=_('Получатель инвойса в Универсо'))
        prezo = graphene.Float(description=_('Цена этой позиции'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_mono_fakturoj_objektoj = None
        fakturo = None
        objekto = None
        realeco = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_kosmo.povas_krei_universo_mono_fakturoj_objektoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoRealeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Указана несуществующая реальность Универсо'),'realeco_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco_id')

                        if not message:
                            if 'fakturo_uuid' in kwargs:
                                try:
                                    fakturo = UniversoMonoFakturo.objects.get(
                                        uuid=kwargs.get('fakturo_uuid'), 
                                        forigo=False, 
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except UniversoMonoFakturo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный инвойс к которому относится запись Универсо'),'fakturo_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'fakturo_uuid')

                        if not message:
                            if 'objekto_uuid' in kwargs:
                                try:
                                    objekto = UniversoObjekto.objects.get(id=kwargs.get('objekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoObjekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная объект инвойса (на каждый объект отдельная запись в этой моделе) в Универсо'),'objekto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'objekto_uuid')

                        if not message:
                            universo_mono_fakturoj_objektoj = UniversoMonoFakturoObjekto.objects.create(
                                forigo=False,
                                arkivo=False,
                                fakturo = fakturo,
                                objekto = objekto,
                                realeco = realeco,
                                publikigo = kwargs.get('publikigo', False),
                                prezo =  kwargs.get('prezo', 0.0),
                                publikiga_dato=timezone.now()
                            )

                            universo_mono_fakturoj_objektoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('fakturo_uuid', False) or kwargs.get('realeco_uuid', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('tipo_id', False
                            or kwargs.get('prezo', False))):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_mono_fakturoj_objektoj = UniversoMonoFakturoObjekto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_kosmo.povas_forigi_universo_mono_fakturoj_objektoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_kosmo.povas_shanghi_universo_mono_fakturoj_objektoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoRealeco.DoesNotExist:
                                        message = '{} "{}"'.format(_('Указана несуществующая реальность Универсо'),'realeco_id')

                            if not message:
                                if 'fakturo_uuid' in kwargs:
                                    try:
                                        fakturo = UniversoMonoFakturo.objects.get(
                                            uuid=kwargs.get('fakturo_uuid'), 
                                            forigo=False, 
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except UniversoMonoFakturo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный инвойс к которому относится запись Универсо'),'fakturo_uuid')

                            if not message:
                                if 'objekto_uuid' in kwargs:
                                    try:
                                        objekto = UniversoObjekto.objects.get(id=kwargs.get('objekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoObjekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная объект инвойса (на каждый объект отдельная запись в этой моделе) в Универсо'),'objekto_uuid')

                            if not message:

                                universo_mono_fakturoj_objektoj.forigo = kwargs.get('forigo', universo_mono_fakturoj_objektoj.forigo)
                                universo_mono_fakturoj_objektoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_mono_fakturoj_objektoj.arkivo = kwargs.get('arkivo', universo_mono_fakturoj_objektoj.arkivo)
                                universo_mono_fakturoj_objektoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_mono_fakturoj_objektoj.publikigo = kwargs.get('publikigo', universo_mono_fakturoj_objektoj.publikigo)
                                universo_mono_fakturoj_objektoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_mono_fakturoj_objektoj.prezo = kwargs.get('prezo', universo_mono_fakturoj_objektoj.prezo)
                                universo_mono_fakturoj_objektoj.realeco = realeco if kwargs.get('realeco_id', False) \
                                    else universo_mono_fakturoj_objektoj.realeco
                                universo_mono_fakturoj_objektoj.fakturo = fakturo if kwargs.get('fakturo_uuid', False) \
                                    else universo_mono_fakturoj_objektoj.fakturo
                                universo_mono_fakturoj_objektoj.objekto = objekto if kwargs.get('objekto_uuid', False) \
                                    else universo_mono_fakturoj_objektoj.objekto

                                universo_mono_fakturoj_objektoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoMonoFakturoObjekto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoMonoFakturoObjekto(status=status, message=message, universo_mono_fakturoj_objektoj=universo_mono_fakturoj_objektoj)


# Модель типов транзакций между счетами (кошельками) в Универсо
class RedaktuUniversoMonoTransakcioTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_mono_transakcioj_tipoj = graphene.Field(UniversoMonoTransakcioTipoNode, 
        description=_('Созданный/изменённый тип транзакций между счетами (кошельками) в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_mono_transakcioj_tipoj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_kosmo.povas_krei_universo_mono_transakcioj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_mono_transakcioj_tipoj = UniversoMonoTransakcioTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_mono_transakcioj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_mono_transakcioj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_mono_transakcioj_tipoj.realeco.set(realeco)
                                else:
                                    universo_mono_transakcioj_tipoj.realeco.clear()

                            universo_mono_transakcioj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_mono_transakcioj_tipoj = UniversoMonoTransakcioTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_kosmo.povas_forigi_universo_mono_transakcioj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_kosmo.povas_shanghi_universo_mono_transakcioj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')

                            if not message:

                                universo_mono_transakcioj_tipoj.forigo = kwargs.get('forigo', universo_mono_transakcioj_tipoj.forigo)
                                universo_mono_transakcioj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_mono_transakcioj_tipoj.arkivo = kwargs.get('arkivo', universo_mono_transakcioj_tipoj.arkivo)
                                universo_mono_transakcioj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_mono_transakcioj_tipoj.publikigo = kwargs.get('publikigo', universo_mono_transakcioj_tipoj.publikigo)
                                universo_mono_transakcioj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_mono_transakcioj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_mono_transakcioj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_mono_transakcioj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_mono_transakcioj_tipoj.realeco.set(realeco)
                                    else:
                                        universo_mono_transakcioj_tipoj.realeco.clear()

                                universo_mono_transakcioj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoMonoTransakcioTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoMonoTransakcioTipo(status=status, message=message, universo_mono_transakcioj_tipoj=universo_mono_transakcioj_tipoj)


# Модель инвойсов (счетов на оплату) в Универсо
class RedaktuUniversoMonoTransakcio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_mono_transakcioj = graphene.Field(UniversoMonoTransakcioNode, 
        description=_('Созданный/изменённый инвойс (счет на оплату) в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        tipo_id = graphene.Int(description=_('Тип транзакции'))
        fakturo_uuid = graphene.String(description=_('Инвойс на основе которого делается транзакция в Универсо'))
        ekspedanto_uuid = graphene.String(description=_('Отправитель платежа в Универсо'))
        ricevanto_uuid = graphene.String(description=_('Получатель платежа в Универсо'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_mono_transakcioj = None
        ekspedanto = None
        ricevanto = None
        realeco = None
        tipo = None
        fakturo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_kosmo.povas_krei_universo_mono_transakcioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoRealeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Указана несуществующая реальность Универсо'),'realeco_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoMonoTransakcioTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoMonoTransakcioTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип транзакции Универсо'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'fakturo_uuid' in kwargs:
                                try:
                                    fakturo = UniversoMonoKonto.objects.get(
                                        uuid=kwargs.get('fakturo_uuid'), 
                                        forigo=False, 
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except UniversoMonoKonto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный инвойс на основе которого делается транзакция Универсо'),'fakturo_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'fakturo_uuid')
                        if not message:
                            if 'ekspedanto_uuid' in kwargs:
                                try:
                                    ekspedanto = UniversoMonoKonto.objects.get(
                                        uuid=kwargs.get('ekspedanto_uuid'), 
                                        forigo=False, 
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except UniversoMonoKonto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный отправитель платежа Универсо'),'ekspedanto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'ekspedanto_uuid')

                        if not message:
                            if 'ricevanto_uuid' in kwargs:
                                try:
                                    ricevanto = UniversoMonoKonto.objects.get(id=kwargs.get('ricevanto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoMonoKonto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный получатель платежа Универсо'),'ricevanto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'ricevanto_uuid')

                        if not message:
                            universo_mono_transakcioj = UniversoMonoTransakcio.objects.create(
                                forigo=False,
                                arkivo=False,
                                ekspedanto = ekspedanto,
                                ricevanto = ricevanto,
                                tipo = tipo,
                                realeco = realeco,
                                fakturo = fakturo,
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            universo_mono_transakcioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('ekspedanto_uuid', False) 
                            or kwargs.get('ricevanto_uuid', False) or kwargs.get('fakturo_uuid', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('tipo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_mono_transakcioj = UniversoMonoTransakcio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_kosmo.povas_forigi_universo_mono_transakcioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_kosmo.povas_shanghi_universo_mono_transakcioj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoRealeco.DoesNotExist:
                                        message = '{} "{}"'.format(_('Указана несуществующая реальность Универсо'),'realeco_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoMonoTransakcioTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoMonoTransakcioTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип транзакции Универсо'),'tipo_id')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                            if not message:
                                if 'fakturo_uuid' in kwargs:
                                    try:
                                        fakturo = UniversoMonoKonto.objects.get(
                                            uuid=kwargs.get('fakturo_uuid'), 
                                            forigo=False, 
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except UniversoMonoKonto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный инвойс на основе которого делается транзакция Универсо'),'fakturo_uuid')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'fakturo_uuid')
                            if not message:
                                if 'ekspedanto_uuid' in kwargs:
                                    try:
                                        ekspedanto = UniversoMonoKonto.objects.get(
                                            uuid=kwargs.get('ekspedanto_uuid'), 
                                            forigo=False, 
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except UniversoMonoKonto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный отправитель платежа Универсо'),'ekspedanto_uuid')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'ekspedanto_uuid')

                            if not message:
                                if 'ricevanto_uuid' in kwargs:
                                    try:
                                        ricevanto = UniversoMonoKonto.objects.get(id=kwargs.get('ricevanto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoMonoKonto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный получатель платежа Универсо'),'ricevanto_uuid')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'ricevanto_uuid')

                            if not message:

                                universo_mono_transakcioj.forigo = kwargs.get('forigo', universo_mono_transakcioj.forigo)
                                universo_mono_transakcioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_mono_transakcioj.arkivo = kwargs.get('arkivo', universo_mono_transakcioj.arkivo)
                                universo_mono_transakcioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_mono_transakcioj.publikigo = kwargs.get('publikigo', universo_mono_transakcioj.publikigo)
                                universo_mono_transakcioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_mono_transakcioj.realeco = realeco if kwargs.get('realeco_id', False) \
                                    else universo_mono_transakcioj.realeco
                                universo_mono_transakcioj.tipo = tipo if kwargs.get('tipo_id', False) \
                                    else universo_mono_transakcioj.tipo
                                universo_mono_transakcioj.ekspedanto = ekspedanto if kwargs.get('ekspedanto_uuid', False) \
                                    else universo_mono_transakcioj.ekspedanto
                                universo_mono_transakcioj.ricevanto = ricevanto if kwargs.get('ricevanto_uuid', False) \
                                    else universo_mono_transakcioj.ricevanto
                                universo_mono_transakcioj.fakturo = fakturo if kwargs.get('fakturo_uuid', False) \
                                    else universo_mono_transakcioj.fakturo

                                universo_mono_transakcioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoMonoTransakcio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoMonoTransakcio(status=status, message=message, universo_mono_transakcioj=universo_mono_transakcioj)


class UniversoMonoMutations(graphene.ObjectType):
    redaktu_universo_mono_valutoj = RedaktuUniversoMonoValuto.Field(
        description=_('''Создаёт или редактирует модель справочника валют в Универсо''')
    )
    redaktu_universo_mono_kontoj_tipoj = RedaktuUniversoMonoKontoTipo.Field(
        description=_('''Создаёт или редактирует модель типов счетов в Универсо''')
    )
    redaktu_universo_mono_kontoj_sxablonoj = RedaktuUniversoMonoKontoSxablono.Field(
        description=_('''Создаёт или редактирует модель шаблонов счетов (кошельков) в Универсо''')
    )
    redaktu_universo_mono_kontoj = RedaktuUniversoMonoKonto.Field(
        description=_('''Создаёт или редактирует модель счетов (кошельков) в Универсо''')
    )
    redaktu_universo_mono_fakturoj_tipoj = RedaktuUniversoMonoFakturoTipo.Field(
        description=_('''Создаёт или редактирует модель типов инвойсов (счетов на оплату) в Универсо''')
    )
    redaktu_universo_mono_fakturoj = RedaktuUniversoMonoFakturo.Field(
        description=_('''Создаёт или редактирует модель инвойсов (счетов на оплату) в Универсо''')
    )
    redaktu_universo_mono_fakturoj_objektoj = RedaktuUniversoMonoFakturoObjekto.Field(
        description=_('''Создаёт или редактирует модель объектов указанных в инвойсах (содержимое табличной части) в Универсо''')
    )
    redaktu_universo_mono_transakcioj_tipoj = RedaktuUniversoMonoTransakcioTipo.Field(
        description=_('''Создаёт или редактирует модель типов транзакций между счетами (кошельками) в Универсо''')
    )
    redaktu_universo_mono_transakcioj = RedaktuUniversoMonoTransakcio.Field(
        description=_('''Создаёт или редактирует модель инвойсов (счетов на оплату) в Универсо''')
    )
