from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class KomunumojConfig(AppConfig):
    name = 'komunumoj'
    verbose_name = _('Komunumoj')

    # def ready(self):
    #     import komunumoj.signals
