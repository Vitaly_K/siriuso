"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from komunumoj import models


__komunumoj_models = (models.Komunumo, )

def get_komunumo(uuid):
    """
    Принимает uuid сообщества, возвращает кортеж (tuple),
    где первый объект - найденное сообщество, а второй -
    текстовый индентификатор типа сообщества.
    Если сообщество не найдено, то возвращает None
    """
    komunumo = None
    kom_tipo = None

    for kom in __komunumoj_models:
        try:
            komunumo = kom.objects.get(uuid=uuid)
            kom_tipo = kom._meta.model_name
        except kom.DoesNotExist:
            pass
        if komunumo:
            break

    return (komunumo, kom_tipo) if komunumo else None


def get_komunumo_by_id(id, kom_tipo):
    """
    Принимает id сообщества, возвращает найденное сообщество.
    Если сообщество не найдено, то возвращает None
    """
    komunumo = None
    kom_tipo = None

    for kom in __komunumoj_models:
        if kom_tipo == kom._meta.model_name:
            try:
                komunumo = kom.objects.get(id=id)
            except kom.DoesNotExist:
                pass
            break

    return komunumo
