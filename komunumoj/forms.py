from django import forms
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _


class SearchForm(forms.Form):
    search_field = forms.CharField(widget=forms.TextInput(), max_length=50, required=False,
                                   label=_('Поиск'),
                                   help_text=_('Поиск по сообществам'))
