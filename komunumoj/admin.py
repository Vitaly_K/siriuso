"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from django import forms
from django.utils.translation import ugettext_lazy as _
from siriuso.utils import get_enhavo, set_enhavo
from komunumoj.models import *
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
import json


class TekstoMixin:
    @staticmethod
    def teksto(obj, field):
        kampo = getattr(obj, field)

        if kampo is not None:
            result = get_enhavo(kampo)

            if result:
                return result[0]
        return ''

    def nomo_teksto(self, obj):
        return self.teksto(obj=obj, field='nomo')

    def priskribo_teksto(self, obj):
        return self.teksto(obj=obj, field='priskribo')

    def statuso_teksto(self, obj):
        return self.teksto(obj=obj, field='statuso')


# Доступ (видимость) к различным сущностям привязанным к сообществу
@admin.register(KomunumojAliro)
class KomunumojAliroAdmin (admin.ModelAdmin):
    list_display = ('nomo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = KomunumojAliro


# Общие модели
# Объединения сообществ (общая модель)
@admin.register(KomunumoUnuixo)
class KomunumoUnuixoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = KomunumoUnuixo


# Типы общей модели сообществ
@admin.register(KomunumoTipo)
class KomunumoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = KomunumoTipo


# Виды общей модели сообществ
@admin.register(KomunumoSpeco)
class KomunumoSpecoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = KomunumoSpeco

# Категории сообществ (общая модель)
@admin.register(KomunumoKategorio)
class KomunumoKategorioAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'uuid')

    class Meta:
        model = KomunumoKategorio


# Аватары общей модели сообществ
class KomunumoAvataroInline(admin.TabularInline):
    model = KomunumoAvataro
    fk_name = 'posedanto'
    extra = 1


# Обложки общей модели сообществ
class KomunumoKovriloInline(admin.TabularInline):
    model = KomunumoKovrilo
    fk_name = 'posedanto'
    extra = 1


# Форма для многоязычных полей общей модели сообществ
class KomunumoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=True)
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=True)
    statuso = forms.CharField(widget=LingvoTextWidget(), label=_('Statuso'), required=False)

    class Meta:
        model = Komunumo
        fields = [field.name for field in Komunumo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_statuso(self):
        out = self.cleaned_data['statuso']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Сама общая модель сообществ
@admin.register(Komunumo)
class KomunumoAdmin(admin.ModelAdmin, TekstoMixin):
    form = KomunumoFormo
    list_display = ('id', 'grava', 'nomo_teksto', 'priskribo_teksto', 'statuso_teksto', 'tipo', 'speco', 'regiono_teknokomo')
    exclude = ('id',)
    inlines = (#KomunumoTitoloInline,
               #KomunumoPriskriboInline,
               KomunumoAvataroInline,
               KomunumoKovriloInline)

    class Meta:
        model = Komunumo


# Типы пользователей общей модели сообществ
@admin.register(KomunumoMembroTipo)
class KomunumoMembroTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid')
    #exclude = ('nomo',)
    #inlines = (KomunumoMembroTipoNomoInline,)

    class Meta:
        model = KomunumoMembroTipo


# Члены общей модели сообществ
@admin.register(KomunumoMembro)
class KomunumoMembroAdmin(admin.ModelAdmin):
    list_display = ('posedanto', 'forigo', 'autoro', 'tipo', 'laborarolo')
    list_filter = ('forigo', 'tipo')
    search_fields = ('posedanto__uuid', 'posedanto__nomo', 'autoro__unua_nomo', 'autoro__familinomo',
                     'autoro__chefa_retposhto', 'autoro__chefa_telefonanumero')

    class Meta:
        model = KomunumoMembro


# Информация о сообществах (общая модель)
class KomunumoInformoBildoInline(admin.TabularInline):
    model = KomunumoInformoBildo
    fk_name = 'posedanto'
    extra = 1


@admin.register(KomunumoInformo)
class KomunumoInformoAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'posedanto')
    #exclude = ('teksto',)
    inlines = (KomunumoInformoBildoInline,)

    class Meta:
        model = KomunumoInformo
