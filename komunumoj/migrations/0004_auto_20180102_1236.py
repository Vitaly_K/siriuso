# Generated by Django 2.0 on 2018-01-02 12:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('komunumoj', '0003_auto_20180102_1221'),
    ]

    operations = [
        migrations.AlterField(
            model_name='komunumojsovetomembro',
            name='muro_sciigo',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='informiloj.InformilojSciigoTipo', verbose_name='Sciigo (muro)'),
        ),
    ]
