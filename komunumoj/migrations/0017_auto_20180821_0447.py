# Generated by Django 2.0.8 on 2018-08-21 04:47

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('komunumoj', '0016_auto_20180809_0801'),
    ]

    operations = [
        migrations.AddField(
            model_name='komunumojforumo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojforumo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojforumo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojgrupo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojgrupo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojgrupo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojgrupoavataro',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojgrupoavataro_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojgrupoavataro',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojgrupoavatarokomento',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojgrupoavatarokomento_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojgrupoavatarokomento',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojgrupoinformo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojgrupoinformo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojgrupoinformo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojgrupoinformobildo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojgrupoinformobildo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojgrupoinformobildo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojgrupokovrilo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojgrupokovrilo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojgrupokovrilo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojgrupokovrilokomento',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojgrupokovrilokomento_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojgrupokovrilokomento',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojgrupomembro',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojgrupomembro_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojgrupomembro',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojkategoriosociaprojekto',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojkategoriosociaprojekto_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojkategoriosociaprojekto',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojkolektivablogo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojkolektivablogo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojkolektivablogo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojkolektivablogoavataro',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojkolektivablogoavataro_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojkolektivablogoavataro',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojkolektivablogoavatarokomento',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojkolektivablogoavatarokomento_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojkolektivablogoavatarokomento',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojkolektivablogokovrilo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojkolektivablogokovrilo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojkolektivablogokovrilo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojkolektivablogokovrilokomento',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojkolektivablogokovrilokomento_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojkolektivablogokovrilokomento',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojkolektivablogomembro',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojkolektivablogomembro_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojkolektivablogomembro',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojorganizo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojorganizo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojorganizo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojorganizoavataro',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojorganizoavataro_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojorganizoavataro',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojorganizoavatarokomento',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojorganizoavatarokomento_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojorganizoavatarokomento',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojorganizoinformo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojorganizoinformo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojorganizoinformo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojorganizoinformobildo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojorganizoinformobildo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojorganizoinformobildo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojorganizokovrilo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojorganizokovrilo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojorganizokovrilo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojorganizokovrilokomento',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojorganizokovrilokomento_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojorganizokovrilokomento',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojorganizomembro',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojorganizomembro_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojorganizomembro',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojsociaprojekto',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojsociaprojekto_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojsociaprojekto',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojsociaprojektoavataro',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojsociaprojektoavataro_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojsociaprojektoavataro',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojsociaprojektoavatarokomento',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojsociaprojektoavatarokomento_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojsociaprojektoavatarokomento',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojsociaprojektoinformo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojsociaprojektoinformo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojsociaprojektoinformo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojsociaprojektoinformobildo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojsociaprojektoinformobildo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojsociaprojektoinformobildo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojsociaprojektokovrilo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojsociaprojektokovrilo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojsociaprojektokovrilo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojsociaprojektokovrilokomento',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojsociaprojektokovrilokomento_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojsociaprojektokovrilokomento',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojsociaprojektomembro',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojsociaprojektomembro_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojsociaprojektomembro',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojsoveto',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojsoveto_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojsoveto',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojsovetoavataro',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojsovetoavataro_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojsovetoavataro',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojsovetoavatarokomento',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojsovetoavatarokomento_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojsovetoavatarokomento',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojsovetoinformo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojsovetoinformo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojsovetoinformo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojsovetoinformobildo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojsovetoinformobildo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojsovetoinformobildo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojsovetokovrilo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojsovetokovrilo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojsovetokovrilo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojsovetokovrilokomento',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojsovetokovrilokomento_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojsovetokovrilokomento',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojsovetomembro',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojsovetomembro_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojsovetomembro',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='komunumojsovetounuixo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='komunumoj_komunumojsovetounuixo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='komunumojsovetounuixo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
    ]
