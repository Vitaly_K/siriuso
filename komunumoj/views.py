"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

import json
import pytz
from django.shortcuts import render, redirect, get_object_or_404
from django.http.response import Http404, JsonResponse
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from .models import *
from informiloj.models import InformilojSciigoTipo
from django.template.response import TemplateResponse
from django.views.decorators.csrf import requires_csrf_token, csrf_protect
import datetime
# from django.db.models import Count
# from datetime import datetime, timedelta
# from main.models import SiriusoSlugo

from siriuso.views import SiriusoTemplateView
from siriuso.views.mixins import SiriusoPaginationMixin
from django.db.models import F, Q, Count, Max, DateTimeField
from django.db.models.functions import Coalesce, Least
from itertools import chain
from komunumoj.forms import SearchForm


# Страница сообществ
class Socio(SiriusoTemplateView, SiriusoPaginationMixin):
    template_name = 'komunumoj/komunumoj_listo.html'
    mobile_template_name = 'komunumoj/portebla/t_komunumoj_listo.html'

    ajax_template = 'komunumoj/komunumoj_listo_pago.html'
    mobile_ajax_teplate = 'komunumoj/portebla/t_komunumoj_listo_pago.html'

    http_method_names = ['get', 'post']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        ptype = self.request.POST.get('typo', 'multi')

        form = SearchForm(self.request.GET)
        if form.is_valid():
            nomo_q = Q(nomo__enhavo__icontains=form.cleaned_data['search_field'])
            priskribo_q = Q(priskribo__enhavo__icontains=form.cleaned_data['search_field'])
            search_q = nomo_q | priskribo_q
        else:
            search_q = None

        def make_query(obj, avataro, kovrilo, dato):
            # Дата последней опубликованной на предыдущей странице записи этого же типа
            last_rec = timezone.now()
            start = self.page_offset
            last_rec2 = 2

            # Если переданы данные о последней опубликованной на предыдущей сранице записи
            if self.page_multi is not None and obj.__name__ in self.page_multi:
                rec = self.page_multi.get(obj.__name__)
                last_rec = datetime.datetime.strptime(rec.get('muroj__dato')[0:26], '%Y-%m-%d %H:%M:%S.%f')
                last_rec = timezone.make_aware(last_rec, pytz.UTC)
                last_rec2 = int(rec.get('muroj__cnt'))
                start = 0

            end = start + self.page_max + 1

            return (obj.objects
                    .select_related('nomo', 'priskribo')
                    .annotate(avataro_bildo_min=avataro,
                              kovrilo_bildo=kovrilo,
                              nomo__enhavo=F('nomo__enhavo'),
                              priskribo__enhavo=F('priskribo__enhavo'),
                              muroj__dato=Coalesce(
                                  Max(dato),
                                  F('krea_dato') - timezone.timedelta(36500), output_field=DateTimeField()
                              ),
                              m__cnt=Count(dato),
                              muroj__cnt=Least(Count(dato), 2))
                    .filter(search_q, krea_dato__lte=timezone.now(), muroj__dato__lt=last_rec,
                            muroj__cnt__lte=last_rec2, forigo=False, arkivo=False)
                    .only('uuid', 'id', 'forigo', 'krea_dato', 'nomo__enhavo', 'priskribo__enhavo')
                    .order_by('-muroj__cnt', '-muroj__dato'))[start:end]

        def set_context(obj, kom_name):
            if ptype != 'multi' or ptype == 'multi' and self.is_pagination:
                ckey = 'rekordoj'
                context['more'] = len(obj) > self.page_max
                context['offset'] = len(obj) - 1
            else:
                context[kom_name + '_more'] = len(obj) > self.page_max
                ckey = kom_name

            context[ckey] = obj[0:self.page_max]

        if ptype in ['multi', 'projekto']:
            sociaprojektoj_dict = {
                'avataro': F('komunumojsociaprojektoavataro__bildo_min'),
                'kovrilo': F('komunumojsociaprojektokovrilo__bildo'),
                'dato': F('murojsociaprojektoenskribo__publikiga_dato')
            }
            sociaprojektoj = make_query(KomunumojSociaprojekto, **sociaprojektoj_dict)
            set_context(sociaprojektoj, 'sociaprojektoj')

        if ptype in ['multi', 'soveto']:
            sovetoj_dict = {
                'avataro': F('komunumojsovetoavataro__bildo_min'),
                'kovrilo': F('komunumojsovetokovrilo__bildo'),
                'dato': F('murojsovetoenskribo__publikiga_dato')
            }
            sovetoj = make_query(KomunumojSoveto, **sovetoj_dict)
            set_context(sovetoj, 'sovetoj')

        if ptype in ['multi', 'organizo']:
            organizoj_dict = {
                'avataro': F('komunumojorganizoavataro__bildo_min'),
                'kovrilo': F('komunumojorganizokovrilo__bildo'),
                'dato': F('murojorganizoenskribo__publikiga_dato')
            }
            organizoj = make_query(KomunumojOrganizo, **organizoj_dict)
            set_context(organizoj, 'organizoj')

        if ptype in ['multi', 'grupo']:
            grupoj_dict = {
                'avataro': F('komunumojgrupoavataro__bildo_min'),
                'kovrilo': F('komunumojgrupokovrilo__bildo'),
                'dato': F('murojgrupoenskribo__publikiga_dato')
            }
            grupoj = make_query(KomunumojGrupo, **grupoj_dict)
            set_context(grupoj, 'grupoj')

        if ptype == 'multi':
            komunumoj = list(chain(sociaprojektoj, sovetoj, organizoj, grupoj))
            komunumoj.sort(key=lambda x: (x.muroj__cnt, x.muroj__dato), reverse=True)

            set_context(komunumoj[0:self.page_max+1], 'komunumoj')
            self.page_multi = json.dumps(self.multiple_last_objects(komunumoj, ['muroj__dato', 'muroj__cnt']))
            context['komunumoj_multi'] = self.page_multi
            # context.update({
            #    'komunumoj': komunumoj[self.page_offset:self.page_offset + self.page_max],
            #    'komunumoj_multi': json.dumps(self.multiple_last_objects(komunumoj, ['muroj__dato']))
            #})

        context['search_form'] = form
        return context


# Информация об общественном проекте
class InformoSociaprojekto(SiriusoTemplateView):

    template_name = 'komunumoj/komunumoj_sociaprojekto_informo.html'
    mobile_template_name = 'komunumoj/portebla/t_komunumoj_sociaprojekto_informo.html'

    http_method_names = ['get']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        queryset_sociaprojekto = KomunumojSociaprojekto.objects.values \
            ('komunumojsociaprojektoinformo__teksto__enhavo', 'komunumojsociaprojektoinformo__ligilo',
             'komunumojsociaprojektoinformo__komunumojsociaprojektoinformobildo__bildo')

        sociaprojekto = get_object_or_404(queryset_sociaprojekto, id=kwargs['projekto_id'], forigo=False)

        membroj = KomunumojSociaprojektoMembro.objects.filter \
            (posedanto__id=kwargs['projekto_id'], forigo=False, autoro__is_active=True, autoro__konfirmita=True,
             autoro__is_superuser=False) \
            .values('autoro__id', 'autoro__sekso', 'autoro__unua_nomo__enhavo',
                    'autoro__familinomo__enhavo', 'autoro__uzantojavataro__bildo_min',
                    'autoro__uzantojstatuso__teksto__enhavo', 'tipo__kodo', 'tipo__nomo__enhavo')

        if sociaprojekto['komunumojsociaprojektoinformo__teksto__enhavo'] is not None:
            informo = {
                'teksto': sociaprojekto['komunumojsociaprojektoinformo__teksto__enhavo'],
                'ligilo': sociaprojekto['komunumojsociaprojektoinformo__ligilo'],
                'bildo': sociaprojekto['komunumojsociaprojektoinformo__komunumojsociaprojektoinformobildo__bildo']
            }
        else:
            informo = None

        context.update({'informo': informo,
                        'membroj': membroj})

        return context


# Информация о совете
class InformoSoveto(SiriusoTemplateView):

    template_name = 'komunumoj/komunumoj_soveto_informo.html'
    mobile_template_name = 'komunumoj/portebla/t_komunumoj_soveto_informo.html'

    http_method_names = ['get']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        queryset_soveto = KomunumojSoveto.objects.values \
            ('komunumojsovetoinformo__teksto__enhavo', 'komunumojsovetoinformo__ligilo', 'lando__nomo__enhavo',
             'regiono__nomo__enhavo', 'komunumojsovetoinformo__komunumojsovetoinformobildo__bildo')

        soveto = get_object_or_404(queryset_soveto, id=kwargs['soveto_id'], forigo=False)

        membroj = KomunumojSovetoMembro.objects.filter \
            (posedanto__id=kwargs['soveto_id'], forigo=False, autoro__is_active=True, autoro__konfirmita=True,
             autoro__is_superuser=False) \
            .values('autoro__id', 'autoro__sekso', 'autoro__unua_nomo__enhavo',
                    'autoro__familinomo__enhavo', 'autoro__uzantojavataro__bildo_min',
                    'autoro__uzantojstatuso__teksto__enhavo', 'tipo__kodo', 'tipo__nomo__enhavo')

        if soveto['komunumojsovetoinformo__teksto__enhavo'] is not None:
            informo = {
                'teksto': soveto['komunumojsovetoinformo__teksto__enhavo'],
                'ligilo': soveto['komunumojsovetoinformo__ligilo'],
                'bildo': soveto['komunumojsovetoinformo__komunumojsovetoinformobildo__bildo']
            }
        else:
            informo = None

        context.update({'informo': informo,
                        'membroj':membroj})

        return context


# Информация об организации
class InformoOrganizo(SiriusoTemplateView):

    template_name = 'komunumoj/komunumoj_organizo_informo.html'
    mobile_template_name = 'komunumoj/portebla/t_komunumoj_organizo_informo.html'

    http_method_names = ['get']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        queryset_organizo = KomunumojOrganizo.objects.values \
            ('komunumojorganizoinformo__teksto__enhavo', 'komunumojorganizoinformo__ligilo',
             'komunumojorganizoinformo__komunumojorganizoinformobildo__bildo')

        organizo = get_object_or_404(queryset_organizo, id=kwargs['organizo_id'], forigo=False)

        membroj = KomunumojOrganizoMembro.objects.filter \
            (posedanto__id=kwargs['organizo_id'], forigo=False, autoro__is_active=True, autoro__konfirmita=True,
             autoro__is_superuser=False) \
            .values('autoro__id', 'autoro__sekso', 'autoro__unua_nomo__enhavo',
                    'autoro__familinomo__enhavo', 'autoro__uzantojavataro__bildo_min',
                    'autoro__uzantojstatuso__teksto__enhavo', 'tipo__kodo', 'tipo__nomo__enhavo')

        if organizo['komunumojorganizoinformo__teksto__enhavo'] is not None:
            informo = {
                'teksto': organizo['komunumojorganizoinformo__teksto__enhavo'],
                'ligilo': organizo['komunumojorganizoinformo__ligilo'],
                'bildo': organizo['komunumojorganizoinformo__komunumojorganizoinformobildo__bildo']
            }
        else:
            informo = None

        context.update({'informo': informo,
                        'membroj':membroj})

        return context


# Информация о группе
class InformoGrupo(SiriusoTemplateView):

    template_name = 'komunumoj/komunumoj_grupo_informo.html'
    mobile_template_name = 'komunumoj/portebla/t_komunumoj_grupo_informo.html'

    http_method_names = ['get']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        queryset_grupo = KomunumojGrupo.objects.values \
            ('komunumojgrupoinformo__teksto__enhavo', 'komunumojgrupoinformo__ligilo',
             'komunumojgrupoinformo__komunumojgrupoinformobildo__bildo')

        grupo = get_object_or_404(queryset_grupo, id=kwargs['grupo_id'], forigo=False)

        membroj = KomunumojGrupoMembro.objects.filter \
            (posedanto__id=kwargs['grupo_id'], forigo=False, autoro__is_active=True, autoro__konfirmita=True,
             autoro__is_superuser=False) \
            .values('autoro__id', 'autoro__sekso', 'autoro__unua_nomo__enhavo',
                    'autoro__familinomo__enhavo', 'autoro__uzantojavataro__bildo_min',
                    'autoro__uzantojstatuso__teksto__enhavo', 'tipo__kodo', 'tipo__nomo__enhavo')

        if grupo['komunumojgrupoinformo__teksto__enhavo'] is not None:
            informo = {
                'teksto': grupo['komunumojgrupoinformo__teksto__enhavo'],
                'ligilo': grupo['komunumojgrupoinformo__ligilo'],
                'bildo': grupo['komunumojgrupoinformo__komunumojgrupoinformobildo__bildo']
            }
        else:
            informo = None

        context.update({'informo': informo,
                        'membroj':membroj})

        return context


# Вступление в Общественный проект (поддержка)
@login_required # Только для авторизированных пользователей
@csrf_protect # Требовать CSRF ключ для POST обязательно
def kunigu_sociaprojekto(request):
    if request.method == 'POST':
        response = {'status': 'err', 'err': 'Bad ajax request'}

        if 'projekto' in request.POST:
            try:
                projekto = KomunumojSociaprojekto.objects.get(uuid=request.POST['projekto'])
                aligo = KomunumojSociaprojektoMembro.objects.filter(posedanto=projekto, autoro=request.user, forigo=False)

                if aligo.count():
                    # Удаляем подписку
                    aligo = aligo[0]
                    aligo.forigo = True
                    aligo.foriga_dato = timezone.now()
                    aligo.save()
                    response = {'status': 'ok', 'ago': 'taсmento'}
                else:
                    # Создаем подписку
                    membro_tipo = KomunumojSociaprojektoMembroTipo.objects.get(kodo='subtenanta', forigo=False)
                    aligo = KomunumojSociaprojektoMembro(posedanto=projekto, tipo=membro_tipo, autoro=request.user)
                    aligo.save()
                    response = {'status': 'ok', 'ago': 'aligo'}

                    sciigo_tipo = InformilojSciigoTipo.objects.get(kodo='powto', forigo=False)
                    aligo.muro_sciigo.add(sciigo_tipo)

                # Формируем обновленную статистику
                tuta = KomunumojSociaprojektoMembro.objects.values('uuid') \
                    .filter(posedanto=projekto, forigo=False, autoro__konfirmita=True, autoro__is_superuser=False)\
                    .aggregate(total=Count('uuid'))['total']
                procentoj = 100 * tuta / 100000

                if not procentoj:
                    procentoj = 1
                elif procentoj < 2:
                    procentoj = 2
                else:
                    procentoj = "%.2f" % (procentoj)

                response['tuta'] = tuta
                response['procentoj'] = procentoj
            except (KomunumojSociaprojekto.DoesNotExist, KomunumojSociaprojektoMembroTipo.DoesNotExist,
                    InformilojSciigoTipo.DoesNotExist):
                pass

        return JsonResponse(response)

    # Если не POST, то выдам страницу 404
    raise Http404('Page not found')

# Подписка на Совет
@login_required # Только для авторизированных пользователей
@csrf_protect # Требовать CSRF ключ для POST обязательно
def kunigu_soveto(request):
    if request.method == 'POST':
        response = {'status': 'err', 'err': 'Bad ajax request'}

        if 'soveto' in request.POST:
            try:
                soveto = KomunumojSoveto.objects.get(uuid=request.POST['soveto'])
                aligo = KomunumojSovetoMembro.objects.filter(posedanto=soveto, autoro=request.user, forigo=False)

                if aligo.count() and aligo[0].tipo.kodo != 'membro':
                    aligo = aligo[0]
                    aligo.forigo = True
                    aligo.foriga_dato = timezone.now()
                    aligo.save()
                    response = {'status': 'ok', 'ago': 'taсmento'}
                else:
                    membro_tipo = KomunumojSovetoMembroTipo.objects.get(kodo='abonanto', forigo=False)
                    aligo = KomunumojSovetoMembro(posedanto=soveto, tipo=membro_tipo, autoro=request.user)
                    aligo.save()
                    response = {'status': 'ok', 'ago': 'aligo'}

                    sciigo_tipo = InformilojSciigoTipo.objects.get(kodo='powto', forigo=False)
                    aligo.muro_sciigo.add(sciigo_tipo)

                # Формируем обновленную статистику
                tuta = KomunumojSovetoMembro.objects.values('uuid') \
                    .filter(posedanto=soveto, forigo=False, autoro__konfirmita=True, autoro__is_superuser=False)\
                    .aggregate(total=Count('uuid'))['total']
                procentoj = 100 * tuta / 100000

                if not procentoj:
                    procentoj = 1
                elif procentoj < 2:
                    procentoj = 2
                else:
                    procentoj = "%.2f" % (procentoj)

                response['tuta'] = tuta
                response['procentoj'] = procentoj
            except (KomunumojSoveto.DoesNotExist, KomunumojSovetoMembroTipo.DoesNotExist,
                    InformilojSciigoTipo.DoesNotExist):
                pass

        return JsonResponse(response)

    # Если не POST, то выдам страницу 404
    raise Http404('Page not found')


# Вступление в группу
@login_required # Только для авторизированных пользователей
@csrf_protect # Требовать CSRF ключ для POST обязательно
def kunigu_grupo(request):
    if request.method == 'POST':
        response = {'status': 'err', 'err': 'Bad ajax request'}

        if 'grupo' in request.POST:
            try:
                grupo = KomunumojGrupo.objects.get(uuid=request.POST['grupo'])
                aligo = KomunumojGrupoMembro.objects.filter(posedanto=grupo, autoro=request.user, forigo=False)

                if aligo.count():
                    aligo = aligo[0]
                    aligo.forigo = True
                    aligo.foriga_dato = timezone.now()
                    aligo.save()
                    response = {'status': 'ok', 'ago': 'taсmento'}
                else:
                    membro_tipo = KomunumojGrupoMembroTipo.objects.get(kodo='komunumano', forigo=False)
                    aligo = KomunumojGrupoMembro(posedanto=grupo, tipo=membro_tipo, autoro=request.user)
                    aligo.save()
                    response = {'status': 'ok', 'ago': 'aligo'}

                    sciigo_tipo = InformilojSciigoTipo.objects.get(kodo='powto', forigo=False)
                    aligo.muro_sciigo.add(sciigo_tipo)

                # Формируем обновленную статистику
                tuta = KomunumojGrupoMembro.objects.values('uuid') \
                    .filter(posedanto=grupo, forigo=False, autoro__konfirmita=True, autoro__is_superuser=False)\
                    .aggregate(total=Count('uuid'))['total']
                procentoj = 100 * tuta / 100000

                if not procentoj:
                    procentoj = 1
                elif procentoj < 2:
                    procentoj = 2
                else:
                    procentoj = "%.2f" % (procentoj)

                response['tuta'] = tuta
                response['procentoj'] = procentoj
            except (KomunumojGrupo.DoesNotExist, KomunumojGrupoMembroTipo.DoesNotExist,
                    InformilojSciigoTipo):
                pass

        return JsonResponse(response)

    # Если не POST, то выдам страницу 404
    raise Http404('Page not found')


# Подписка на Организацию
@login_required # Только для авторизированных пользователей
@csrf_protect # Требовать CSRF ключ для POST обязательно
def kunigu_organizo(request):
    if request.method == 'POST':
        response = {'status': 'err', 'err': 'Bad ajax request'}

        if 'organizo' in request.POST:
            try:
                organizo = KomunumojOrganizo.objects.get(uuid=request.POST['organizo'])
                aligo = KomunumojOrganizoMembro.objects.filter(posedanto=organizo, autoro=request.user, forigo=False)

                if aligo.count() \
                        and aligo[0].tipo.kodo not in ['membro', 'membro-adm', 'membro-mod']:
                    aligo = aligo[0]
                    aligo.forigo = True
                    aligo.foriga_dato = timezone.now()
                    aligo.save()
                    response = {'status': 'ok', 'ago': 'taсmento'}
                else:
                    membro_tipo = KomunumojOrganizoMembroTipo.objects.get(kodo='abonanto', forigo=False)
                    aligo = KomunumojOrganizoMembro(posedanto=organizo, tipo=membro_tipo, autoro=request.user)
                    aligo.save()
                    response = {'status': 'ok', 'ago': 'aligo'}

                    sciigo_tipo = InformilojSciigoTipo.objects.get(kodo='powto', forigo=False)
                    aligo.muro_sciigo.add(sciigo_tipo)

                # Формируем обновленную статистику
                tuta = KomunumojOrganizoMembro.objects.values('uuid') \
                    .filter(posedanto=organizo, forigo=False, autoro__konfirmita=True, autoro__is_superuser=False)\
                    .aggregate(total=Count('uuid'))['total']
                procentoj = 100 * tuta / 100000

                if not procentoj:
                    procentoj = 1
                elif procentoj < 2:
                    procentoj = 2
                else:
                    procentoj = "%.2f" % (procentoj)

                response['tuta'] = tuta
                response['procentoj'] = procentoj
            except (KomunumojOrganizo.DoesNotExist, KomunumojOrganizoMembroTipo.DoesNotExist,
                    InformilojSciigoTipo):
                pass

        return JsonResponse(response)

    # Если не POST, то выдам страницу 404
    raise Http404('Page not found')


# Настройки общественных проектов
def agordoj_sociapojekto(request, sociaprojekto_id):

    queryset_sociaprojekto = KomunumojSociaprojekto.objects.values(
        'komunumojsociaprojektoinformo__teksto__enhavo',
        'komunumojsociaprojektoinformo__ligilo',
        'komunumojsociaprojektoinformo__komunumojsociaprojektoinformobildo__bildo'
    )

    sociaprojekto = get_object_or_404(queryset_sociaprojekto, id=sociaprojekto_id, forigo=False)

    if sociaprojekto['komunumojsociaprojektoinformo__teksto__enhavo'] is not None:
        informo = {
            'teksto': sociaprojekto['komunumojsociaprojektoinformo__teksto__enhavo'],
            'ligilo': sociaprojekto['komunumojsociaprojektoinformo__ligilo'],
            'bildo': sociaprojekto['komunumojsociaprojektoinformo__komunumojsociaprojektoinformobildo__bildo'],
        }
    else:
        informo = None

    datenoj = {'informo': informo, }

    return TemplateResponse(request, 'komunumoj/komunumoj_sociaprojekto_agordoj.html', datenoj)


def test(request):
    return render(request, 'komunumoj/test.html')
