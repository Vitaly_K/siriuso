from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ResursojConfig(AppConfig):
    name = 'resursoj'
    verbose_name = _('Resursoj')
