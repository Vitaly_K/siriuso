"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django import forms
from siriuso.utils import get_enhavo
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
import json

from .models import *


class ResFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Название'), required=True)
    informoj = forms.CharField(widget=LingvoTextWidget(), label=_('Дополнительная информация'), required=False)
    kategorioj = forms.ModelMultipleChoiceField(
        label=_('Группы организаций'),
        queryset=ResursoKategorio.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = Resurso
        fields = [field.name for field in Resurso._meta.fields if field.name not in ('uuid', 'krea_dato')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_informoj(self):
        out = self.cleaned_data['informoj']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


class ResKategorioFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Название'), required=True)

    class Meta:
        model = ResursoKategorio
        fields = [field.name for field in ResursoKategorio._meta.fields if field.name not in ('uuid', 'krea_dato')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


@admin.register(ResursoKategorio)
class ResKategorioAdmin(admin.ModelAdmin):
    form = ResKategorioFormo
    list_display = ('nomo_enhavo', 'kodo', 'publikigo', 'forigo', 'krea_dato')

    class Meta:
        model = ResursoKategorio

    def nomo_enhavo(self, obj):
        return get_enhavo(obj.nomo, empty_values=True)[0]
    nomo_enhavo.short_description = _('Наименование')


@admin.register(Resurso)
class OrganizoAdmin(admin.ModelAdmin):
    form = ResFormo
    list_display = ('nomo_enhavo', 'publikigo', 'forigo', 'krea_dato')

    class Meta:
        model = Resurso

    def nomo_enhavo(self, obj):
        return get_enhavo(obj.nomo)[0]
    nomo_enhavo.short_description = _('Наименование')
