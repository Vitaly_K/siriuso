"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.db import models
from django.contrib.postgres import fields as pgfields
from django.core.validators import MinValueValidator
from django.utils.translation import ugettext_lazy as _
from main.models import SiriusoBazaAbstrakta, SiriusoBazaAbstraktaKomunumoj
from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, dosiero_nomo, generate_hex_id
import functools


class ResursoKategorio(SiriusoBazaAbstraktaKomunumoj):
    kodo = models.CharField(_('Kodo'), null=False, max_length=32, blank=False)
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=False, default=default_lingvo,
                              encoder=CallableEncoder)

    class Meta:
        db_table = 'resursoj_kategorioj'
        verbose_name = _('Kategorio de resurso')
        verbose_name_plural = _('Kategorioj de resursoj')

    def __str__(self):
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])


generate_res_hid = functools.partial(generate_hex_id, 'resursoj.Resurso')


class Resurso(SiriusoBazaAbstraktaKomunumoj):
    mezuro_choices = (
        ('volumo', _('Volumo')),
        ('maso', _('Maso')),
        ('kvanto', _('Kvanto'))
    )
    hid = models.CharField(_('Hex ID'), max_length=16, db_index=True, null=False, blank=False,
                           default=generate_res_hid)
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=False, default=default_lingvo,
                              encoder=CallableEncoder)
    informoj = pgfields.JSONField(verbose_name=_('Priskribo de la rimedo'), blank=False,
                                  default=default_lingvo, encoder=CallableEncoder)
    bildo = models.ImageField(_('Bildo'),
                              upload_to=functools.partial(dosiero_nomo, subdir='resursoj/bildoj'),
                              blank=True)
    # единица измерения
    unuo_mezuro = models.CharField(_('Unueco de mezuro'), max_length=24, choices=mezuro_choices, blank=True,
                                   null=False, default='maso')
    kategorioj = models.ManyToManyField(ResursoKategorio, db_table='resursoj_kategorioj_ligiloj')
    bonus_punkto = models.FloatField(_('Bonuspunkto'), blank=True, null=True, default=0,
                                     validators=[MinValueValidator(0)])
    podetala_prezo = models.FloatField(_('Podetala prezo'), blank=True, null=True, default=None)
    pogranda_prezo = models.FloatField(_('Pogranda prezo'), blank=True, null=True, default=None)
    filia_prezo = models.FloatField(_('Filia prezo'), blank=True, null=True, default=None)

    class Meta:
        db_table = 'resursoj'
        verbose_name = _('Resurso')
        verbose_name_plural = _('Resursoj')
        permissions = (
            ('povas_vidi_resurson', _('Povas vidi resurson')),
            ('povas_krei_resurson', _('Povas krei resurson')),
            ('povas_forigi_resurson', _('Povas forigi resurson')),
            ('povas_shanghi_resurson', _('Povas ŝanĝi resurson')),
        )

    def __str__(self):
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])


class ResursoOpcion(SiriusoBazaAbstraktaKomunumoj):
    posedanto = models.ForeignKey(Resurso, blank=False, null=False, on_delete=models.CASCADE)
    opcio = pgfields.JSONField(verbose_name=_('Opcioj pri karakterizaĵoj'), blank=False, null=False)

    class Meta:
        db_table = 'resurso_opcion'
        verbose_name = _('Opcio pri resurso')
        verbose_name_plural = _('Opcioj pri resursoj')


class ResursoMiksajo(SiriusoBazaAbstraktaKomunumoj):
    posedanto = models.ForeignKey(Resurso, on_delete=models.CASCADE, blank=False, null=False)
    resurso = models.ForeignKey(Resurso, on_delete=models.CASCADE, blank=False, null=False,
                                related_name='resurso')
    kvanto = models.FloatField(_('Kvanto'), blank=True, null=True, default=None, validators=[
        MinValueValidator(0),
    ])

    class Meta:
        db_table = 'resurso_miksajo'
        verbose_name = _('Komponado de kompleksa resurso')
        verbose_name_plural = _('Komponadoj de kompleksaj rimedoj')
        unique_together = (
            'posedanto',
            'resurso'
        )
        permissions = (
            ('povas_vidi_resurson_miksajon', _('Povas vidi resurson miksajon')),
            ('povas_krei_resurson_miksajon', _('Povas krei resurson miksajon')),
            ('povas_forigi_resurson_miksajon', _('Povas forigi resurson miksajon')),
            ('povas_shanghi_resurson_miksajon', _('Povas ŝanĝi resurson miksajon')),
        )

    def __str__(self):
        return '<{} {}: {} <-- {} ({})>'.format(self.__class__.__name__, self._meta.verbose_name,
                                                get_enhavo(self.posedanto.nomo, empty_values=True),
                                                get_enhavo(self.resurso.nomo, empty_values=True),
                                                self.uuid
                                                )
