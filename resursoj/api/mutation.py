"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
import graphene

from siriuso.api.types import ErrorNode
from siriuso.utils import set_enhavo
from .scheme import *
from ..models import *

# Создаём категории ресурсов


# Создание ресурсов
class RedaktuResurso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    resurso = graphene.Field(ResursoNode)

    class Arguments:
        uuid = graphene.UUID()
        nomo = graphene.String()
        publikigo = graphene.Boolean()
        forigo = graphene.Boolean()
        informoj = graphene.String()
        unuo_mezuro = graphene.String()
        grupoj = graphene.List(graphene.String)
        bonus_punkto = graphene.Float()
        podetala_prezo = graphene.Float()
        pogranda_prezo = graphene.Float()
        filia_prezo = graphene.Float()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = []
        resurso = None
        grupoj = ResursoKategorio.objects.none()
        user = info.context.user

        if (user.has_perm('resursoj.add_resurso')
                or user.has_perm('resursoj.change_resurso')
                or user.has_perm('resursoj.delete_resurso')):

            # Создаём новый ресурс
            if 'uuid' not in kwargs:
                if user.has_perm('resursoj.add_resurso'):
                    params = {
                        'publikigo': False,
                        'forigo': False,
                        'autoro': user,
                    }

                    if 'publikigo' in kwargs:
                        params['publikigo'] = kwargs.get('publikigo')

                    if not kwargs.get('nomo'):
                        errors.append(ErrorNode(field='nomo', message=_('Поле обязательно для заполнения')))

                    if 'grupoj' not in kwargs:
                        errors.append(ErrorNode(field='grupoj', message=_('Поле обязательно для заполнения')))
                    elif not len(kwargs.get('grupoj')):
                        errors.append(ErrorNode(field='grupoj', message=_('Необходимо указать хотя бы одну группу')))
                    else:
                        grupoj_kodoj = set(kwargs.get('grupoj'))

                        if len(grupoj_kodoj):
                            grupoj = ResursoKategorio.objects.filter(kodo__in=grupoj_kodoj, forigo=False, publikigo=True)
                            dif = set(grupoj_kodoj) - set(grupoj.values_list('kodo', flat=True))

                            if len(dif):
                                errors.append(
                                    ErrorNode(
                                            field='grupoj', message='{}: {}'.format(
                                            _('Указаны несуществующие группы'),
                                            str(dif)
                                        )
                                    )
                                )

                    if 'bonus_punkto' in kwargs and kwargs.get('bonus_punkto', 0) < 0:
                        errors.append(
                            ErrorNode(
                                field='bonus_punkto',
                                message=_('Значение не может быть меньше ноля')
                            )
                        )

                    if 'podetala_prezo' in kwargs and kwargs.get('podetala_prezo', 0) < 0:
                        errors.append(
                            ErrorNode(
                                field='podetala_prezo',
                                message=_('Значение не может быть меньше ноля')
                            )
                        )

                    if 'pogranda_prezo' in kwargs and kwargs.get('pogranda_prezo', 0) < 0:
                        errors.append(
                            ErrorNode(
                                field='pogranda_prezo',
                                message=_('Значение не может быть меньше ноля')
                            )
                        )

                    if 'filia_prezo' in kwargs and kwargs.get('filia_prezo', 0) < 0:
                        errors.append(
                            ErrorNode(
                                field='filia_prezo',
                                message=_('Значение не может быть меньше ноля')
                            )
                        )

                    if 'unuo_mezuro' in kwargs:
                        if kwargs.get('unuo_mezuro').lower() not in ('volumo', 'maso', 'kvanto'):
                            errors.append(
                                ErrorNode(
                                    field='unuo_mezuro',
                                    message='{}: \'volumo\', \'maso\', \'kvanto\''
                                        .format(_('Неверное значение поля. Допустимые значения'))
                                )
                            )
                    else:
                        errors.append(
                            ErrorNode(
                                field='unuo_mezuro',
                                message=_('Поле обязательно для заполнения')
                            )
                        )

                    if not len(errors):
                        params.update({arg: val for arg, val in kwargs.items()
                                       if arg not in ('nomo', 'informoj', 'unuo_mezuro', 'grupoj')})
                        resurso = Resurso.objects.create(unuo_mezuro=kwargs.get('unuo_mezuro').lower(), **params)
                        set_enhavo(resurso.nomo, kwargs.get('nomo'), lingvo='ru_RU')

                        if 'informoj' in kwargs:
                            set_enhavo(resurso.informoj, kwargs.get('informoj'), lingvo='ru_RU')

                        if 'grupoj' in kwargs:
                            if grupoj:
                                resurso.kategorioj.set(grupoj)
                            else:
                                resurso.kategorioj.clear()

                        resurso.save()
                        status = True
                        message = _('Ресурс успешно создан')
                    else:
                        message = _('Неверные значения аргументов')
                        for error in errors:
                            message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                error.message, _('в поле'), error.field)
                else:
                    message = _('Недостаточно прав на создание ресурса')
            else:
                # Редактируем ресурс
                try:
                    resurso = Resurso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                except Resurso.DoesNotExist:
                    errors.append(
                        ErrorNode(
                            field='uuid',
                            message=_('Ресурс не найден')
                        )
                    )

                if kwargs.get('forigo', False) and not user.has_perm('resursoj.delete_resurso'):
                    errors.append(
                        ErrorNode(
                            field='forigo',
                            message=_('Недостаточно прав на удаление')
                        )
                    )

                if not user.has_perm('resursoj.change_resurso') and (
                        'uuid' in kwargs or 'nomo' in kwargs or 'informoj' in kwargs
                        or 'unuo_mezuro' in kwargs or 'grupoj' in kwargs or 'publikigo' in kwargs
                ):
                    message = _('Недостаточно прав для изменения записи')
                else:
                    if 'nomo' in kwargs and not kwargs.get('nomo'):
                        errors.append(ErrorNode(field='nomo', message=_('Поле не может быть пустым')))

                    if 'grupoj' in kwargs:
                        if not len(kwargs.get('grupoj')):
                            errors.append(
                                ErrorNode(field='grupoj', message=_('Необходимо указать хотя бы одну группу'))
                            )
                        else:
                            grupoj_kodoj = set(kwargs.get('grupoj'))

                            grupoj = ResursoKategorio.objects.filter(kodo__in=grupoj_kodoj, forigo=False, publikigo=True)
                            dif = set(grupoj_kodoj) - set(grupoj.values_list('kodo', flat=True))

                            if len(dif):
                                errors.append(
                                    ErrorNode(
                                        field='grupoj', message='{}: {}'.format(
                                            _('Указаны несуществующие группы'),
                                            str(dif)
                                        )
                                    )
                                )

                    if 'podetala_prezo' in kwargs and kwargs.get('podetala_prezo', 0) < 0:
                        errors.append(
                            ErrorNode(
                                field='podetala_prezo',
                                message=_('Значение не может быть меньше ноля')
                            )
                        )

                    if 'pogranda_prezo' in kwargs and kwargs.get('pogranda_prezo', 0) < 0:
                        errors.append(
                            ErrorNode(
                                field='pogranda_prezo',
                                message=_('Значение не может быть меньше ноля')
                            )
                        )

                    if 'filia_prezo' in kwargs and kwargs.get('filia_prezo', 0) < 0:
                        errors.append(
                            ErrorNode(
                                field='filia_prezo',
                                message=_('Значение не может быть меньше ноля')
                            )
                        )

                    if 'unuo_mezuro' in kwargs:
                        if kwargs.get('unuo_mezuro').lower() not in ('volumo', 'maso', 'kvanto'):
                            errors.append(
                                ErrorNode(
                                    field='unuo_mezuro',
                                    message='{}: \'volumo\', \'maso\', \'kvanto\''
                                        .format(_('Неверное значение поля. Допустимые значения'))
                                )
                            )

                    if 'bonus_punkto' in kwargs and kwargs.get('bonus_punkto', 0) < 0:
                        errors.append(
                            ErrorNode(
                                field='bonus_punkto',
                                message=_('Значение не может быть меньше ноля')
                            )
                        )

                    if not len(errors):
                        for field in kwargs:
                            if field not in ('uuid', 'grupoj', 'nomo', 'unuo_mezuro', 'informoj'):
                                setattr(resurso, field, kwargs.get(field))

                        if 'grupoj' in kwargs:
                            if grupoj is not None and len(grupoj):
                                resurso.grupoj.set(grupoj)
                            else:
                                resurso.grupoj.clear()

                        if 'nomo' in kwargs:
                            set_enhavo(resurso.nomo, kwargs.get('nomo'), lingvo='ru_RU')

                        if 'informoj' in kwargs:
                            set_enhavo(resurso.informoj, kwargs.get('informoj'), lingvo='ru_RU')

                        resurso.bonus_punkto = kwargs.get('bonus_punkto', resurso.bonus_punkto)
                        resurso.lasta_autoro = user
                        resurso.lasta_dato = timezone.now()
                        resurso.unuo_mezuro = kwargs.get('unuo_mezuro', resurso.unuo_mezuro).lower()
                        resurso.save()
                        message = _('Ресурс успешно изменён')
                        status = True
                        errors = None
                    else:
                        resurso = None
                        for error in errors:
                            message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                error.message, _('в поле'), error.field)
                        message = _('Неверные значения аргументов')

        else:
            message = _('Недостаточно прав')

        return RedaktuResurso(status=status, message=message, errors=errors, resurso=resurso)


class RedaktuResursoMiksajo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    resurso_miksajo = graphene.Field(ResursoMiksajoNode)

    class Arguments:
        uuid = graphene.UUID()
        posedanto_uuid = graphene.UUID()
        resurso_uuid = graphene.UUID()
        kbanto = graphene.Float()
        publikigo = graphene.Boolean()
        forigo = graphene.Boolean()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = []
        resurso_miksajo = None
        user = info.context.user

        posedanto = None
        resurso = None

        if (user.has_perm('resursoj.add_resursomiksajo')
                or user.has_perm('resursoj.change_resursomiksajo')
                or user.has_perm('resursoj.delete_resursomiksajo')):

            has_change = user.has_perm('resursoj.change_resursomiksajo')
            has_delete = user.has_perm('resursoj.delete_resursomiksajo')

            # Создаём связь
            if 'uuid' not in kwargs:
                if 'posedanto_uuid' in kwargs:
                    try:
                        posedanto = Resurso.objects.get(uuid=kwargs.get('posedanto_uuid'), publikigo=True, forigo=False)
                    except Resurso.DoesNotExist:
                        errors.append(
                            ErrorNode(
                                field='posedanto_uuid',
                                message=_('Родительский ресурс не найден')
                            )
                        )
                else:
                    errors.append(
                        ErrorNode(
                            field='posedanto_uuid',
                            message=_('Поле обязательно для заполнения')
                        )
                    )

                if 'resurso_uuid' in kwargs:
                    try:
                        resurso = Resurso.objects.get(uuid=kwargs.get('resurso_uuid'), publikigo=True, forigo=False)
                    except Resurso.DoesNotExist:
                        errors.append(
                            ErrorNode(
                                field='resurso_uuid',
                                message=_('Дочерний ресурс не найден')
                            )
                        )
                else:
                    errors.append(
                        ErrorNode(
                            field='resurso_uuid',
                            message=_('Поле обязательно для заполнения')
                        )
                    )

                if 'kbanto' not in kwargs:
                    errors.append(
                        ErrorNode(
                            field='kbanto',
                            message=_('Поле обязательно для заполнения')
                        )
                    )
                elif kwargs.get('kbanto') <= 0:
                    errors.append(
                        ErrorNode(
                            field='kbanto',
                            message=_('Значение должно быть больше ноля')
                        )
                    )

                if 'forigo' in kwargs and kwargs.get('forigo'):
                    errors.append(
                        ErrorNode(
                            field='posedanto_uuid',
                            message=_('Нельзя удалять несозданный ресурс')
                        )
                    )

                if not len(errors):
                    resurso_miksajo = ResursoMiksajo.objects.create(
                        posedanto=posedanto,
                        resurso=resurso,
                        kbanto=kwargs.get('kbanto'),
                        publikigo=kwargs.get('publikigo', False),
                        forigo=kwargs.get('forigo', False)
                    )
                    status = True
                    message = _('Связь успешно установлена')
                else:
                    for error in errors:
                        message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                            error.message, _('в поле'), error.field)
                    message = _('Неверные значения аргументов')

            else:
                # Изменение связи
                if len(kwargs) > 1:
                    if 'forigo' in kwargs and kwargs.get('forigo') and not has_delete:
                        message = _('Недостаточно прав для удаления ресурса')
                    elif not has_change and ('posedanto_uuid' in kwargs or 'resurso_uuid' in kwargs
                                           or 'kbanto' in kwargs or 'publikigo' in kwargs):
                        message = _('Недостаточно прав для изменения ресурса')
                    else:
                        try:
                            resurso_miksajo = ResursoMiksajo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                        except ResursoMiksajo.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='uuid',
                                    message=_('Запись не найдена')
                                )
                            )

                        if 'posedanto_uuid' in kwargs:
                            try:
                                posedanto = Resurso.objects.get(uuid=kwargs.get('posedanto_uuid'), forigo=False, publikigo=True)
                            except Resurso.DoesNotExist:
                                errors.append(
                                    ErrorNode(
                                        field='posedanto_uuid',
                                        message=_('Родительский ресурс не найден')
                                    )
                                )

                        if 'resurso_uuid' in kwargs:
                            try:
                                resurso = Resurso.objects.get(uuid=kwargs.get('resurso_uuid'),
                                                              forigo=False, publikigo=True)
                            except Resurso.DoesNotExist:
                                errors.append(
                                    ErrorNode(
                                        field='resurso_uuid',
                                        message=_('Дочерний ресурс не найден')
                                    )
                                )

                        if 'kbanto' in kwargs and kwargs.get('kbanto') <= 0:
                            errors.append(
                                ErrorNode(
                                    field='kbanto',
                                    message=_('Значение должно быть больше ноля')
                                )
                            )

                        if not len(errors):
                            if 'forigo' in kwargs:
                                resurso_miksajo.delete()
                            else:
                                if posedanto:
                                    resurso_miksajo.posedanto = posedanto

                                if resurso:
                                    resurso_miksajo.resurso = resurso

                                if 'kbanto' in kwargs:
                                    resurso_miksajo.kbanto = kwargs.get('kbanto')

                                if 'forigo' in kwargs:
                                    resurso_miksajo.forigo = kwargs.get('forigo')

                                resurso_miksajo.save()
                            status = True
                            message = _('Связь успешно изменена')
                        else:
                            for error in errors:
                                message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                    error.message, _('в поле'), error.field)
                            message = _('Неверные значения аргументов')
                else:
                    message = _('Нет аргументов для изменения')

        else:
            message = _('Недостаточно прав')

        return RedaktuResursoMiksajo(status=status, message=message, errors=errors, resurso_miksajo=resurso_miksajo)


class RedaktuResursoOpcion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    resurso_opcion = graphene.Field(ResursoOpcionNode)

    class Arguments:
        uuid = graphene.UUID()
        posedanto_uuid = graphene.UUID()
        opcio = graphene.JSONString()
        publikigo = graphene.Boolean()
        forigo = graphene.Boolean()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = []
        resurso_opcion = None
        user = info.context.user
        posedanto = None

        if (user.has_perm('resursoj.add_resursoopcion')
                or user.has_perm('resursoj.change_resursoopcion')
                or user.has_perm('resursoj.delete_resursoopcion')):

            has_change = user.has_perm('resursoj.change_resursoopcion')
            has_delete = user.has_perm('resursoj.delete_resursoopcion')

            # Создаём опцию
            if 'uuid' not in kwargs:
                if 'posedanto_uuid' in kwargs:
                    try:
                        posedanto = Resurso.objects.get(uuid=kwargs.get('posedanto_uuid'), publikigo=True, forigo=False)
                    except Resurso.DoesNotExist:
                        errors.append(
                            ErrorNode(
                                field='posedanto_uuid',
                                message=_('Родительский ресурс не найден')
                            )
                        )
                else:
                    errors.append(
                        ErrorNode(
                            field='posedanto_uuid',
                            message=_('Поле обязательно для заполнения')
                        )
                    )

                if 'opcio' not in kwargs:
                    errors.append(
                        ErrorNode(
                            field='opcio',
                            message=_('Поле обязательно для заполнения')
                        )
                    )

                if 'forigo' in kwargs and kwargs.get('forigo'):
                    errors.append(
                        ErrorNode(
                            field='forigo',
                            message=_('Нельзя удалять несозданный вариант')
                        )
                    )

                if not len(errors):
                    resurso_opcion = ResursoOpcion.objects.create(
                        posedanto=posedanto,
                        opcio=kwargs.get('opcio'),
                        publikigo=kwargs.get('publikigo', False),
                        forigo=kwargs.get('forigo', False)
                    )
                    status = True
                    message = _('Вариант успешно создан')
                else:
                    for error in errors:
                        message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                            error.message, _('в поле'), error.field)
                    message = _('Неверные значения аргументов')

            else:
                # Изменение опции
                if len(kwargs) > 1:
                    if 'forigo' in kwargs and kwargs.get('forigo') and not has_delete:
                        message = _('Недостаточно прав для удаления ресурса')
                    elif not has_change and ('posedanto_uuid' in kwargs or 'opcio' in kwargs or 'publikigo' in kwargs):
                        message = _('Недостаточно прав для изменения ресурса')
                    else:
                        try:
                            resurso_opcion = ResursoOpcion.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                        except ResursoOpcion.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='uuid',
                                    message=_('Запись не найдена')
                                )
                            )

                        if 'posedanto_uuid' in kwargs:
                            try:
                                posedanto = Resurso.objects.get(uuid=kwargs.get('posedanto_uuid'), forigo=False,
                                                                publikigo=True)
                            except Resurso.DoesNotExist:
                                errors.append(
                                    ErrorNode(
                                        field='posedanto_uuid',
                                        message=_('Родительский ресурс не найден')
                                    )
                                )

                        if not len(errors):
                            if posedanto:
                                resurso_opcion.posedanto = posedanto

                            if 'opcio' in kwargs:
                                resurso_opcion.opcio = kwargs.get('opcio')

                            if 'publikigo' in kwargs:
                                resurso_opcion.publikigo = kwargs.get('publikigo')

                            if 'forigo' in kwargs:
                                resurso_opcion.forigo = kwargs.get('forigo')

                            resurso_opcion.save()
                            status = True
                            message = _('Вариант успешно изменён')
                        else:
                            for error in errors:
                                message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                    error.message, _('в поле'), error.field)
                            message = _('Неверные значения аргументов')
                else:
                    message = _('Нет аргументов для изменения')

        else:
            message = _('Недостаточно прав')

        return RedaktuResursoOpcion(status=status, message=message, errors=errors, resurso_opcion=resurso_opcion)


class ResursoMutation(graphene.ObjectType):
    redaktu_resurso = RedaktuResurso.Field()
    redaktu_resurso_miksajo = RedaktuResursoMiksajo.Field()
    redaktu_resurso_opcion = RedaktuResursoOpcion.Field()
