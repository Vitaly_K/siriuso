"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from graphene import relay, Field, ObjectType, String
from graphene_django import DjangoObjectType
from siriuso.api.filters import SiriusoFilterConnectionField
from siriuso.api.types import SiriusoLingvo

from ..models import *


class ResursoKategorioNode(DjangoObjectType):
    search_fields = ('uuid', 'nomo__enhavo__icontains')
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = Field(SiriusoLingvo)

    class Meta:
        model = ResursoKategorio
        interfaces = (relay.Node,)
        filter_fields = {
            'uuid': ['exact', 'in'],
            'kodo': ['exact', 'in', 'icontains', 'startswith', 'endswith'],
            'publikigo': ['exact'],
            'forigo': ['exact'],
        }


class ResursoOpcionNode(DjangoObjectType):
    class Meta:
        model = ResursoOpcion
        interfaces = (relay.Node,)
        filter_fields = {
            'uuid': ['exact', 'in', 'ne', 'not_in'],
            'posedanto__uuid': ['exact'],
            'publikigo': ['exact'],
            'forigo': ['exact'],
        }


class ResursoNode(DjangoObjectType):
    search_fields = ('uuid', 'nomo__enhavo__icontains')
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = Field(SiriusoLingvo)
    informoj = Field(SiriusoLingvo)
    opcionoj = SiriusoFilterConnectionField(ResursoOpcionNode)

    class Meta:
        model = Resurso
        interfaces = (relay.Node,)
        exclude_fields = ('resurso', 'resursoopcion_set', 'resursomiksajo_set')
        filter_fields = {
            'hid': ['exact', 'in', 'ne', 'not_in'],
            'uuid': ['exact', 'in', 'ne', 'not_in'],
            'kategorioj__kodo': ['exact', 'in'],
            'publikigo': ['exact'],
            'forigo': ['exact'],
        }

    @staticmethod
    def resolve_opcionoj(root, info, **kwargs):
        return ResursoOpcion.objects.filter(posedanto=root)


class ResursoMiksajoNode(DjangoObjectType):
    class Meta:
        model = ResursoMiksajo
        interfaces = (relay.Node,)
        filter_fields = {
            'uuid': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'resurso__uuid': ['exact', 'in'],
            'publikigo': ['exact'],
            'forigo': ['exact'],
        }


class ResursoQuery(ObjectType):
    resursoj_kategorioj = SiriusoFilterConnectionField(ResursoKategorioNode)
    resursoj = SiriusoFilterConnectionField(ResursoNode)
    resursoj_miksajoj = SiriusoFilterConnectionField(ResursoMiksajoNode)
