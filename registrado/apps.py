from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class RegistradoConfig(AppConfig):
    name = 'registrado'
    verbose_name = _('Registrado')
