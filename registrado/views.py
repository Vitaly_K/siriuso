"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.dispatch.dispatcher import Signal
from django.contrib.auth import authenticate, logout, login
from django.forms import forms
from main.models import Uzanto
from .tokens import UserToken, send_confirmation_token
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import requires_csrf_token, csrf_protect
from django.views.decorators.cache import never_cache
from django.http.response import JsonResponse
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext as _
from django.utils.http import is_safe_url
from django.urls import reverse
from datetime import datetime
from siriuso.views.decorators import auth_user_redirect
from siriuso.views import SiriusoTemplateView
import re
from django.utils import translation

from informiloj.models import InformilojLingvo


# Сигналы при регистрации и подтверждении регистрации
uzanto_registrita = Signal(providing_args=['uzanto', 'request'])
uzanto_konfirmita = Signal(providing_args=['uzanto', 'request'])
