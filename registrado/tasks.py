"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from celery import shared_task
from django.contrib.sites.models import Site
from django.conf import settings
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _

from siriuso.utils.modules import get_enhavo
from main.models import Uzanto


@shared_task
def send_confirm_code(uzanto_id, restarigo=False):
    try:
        uzanto = Uzanto.objects.get(id=uzanto_id, is_active=True)
    except Uzanto.DoesNotExist:
        return False

    data = {'uzanto': uzanto,
            'kodo': uzanto.konfirmita_hash if not restarigo else uzanto.agordoj.get('restariga_kodo'),
            'restarigo': restarigo,
            'abono_shlosilo': None,
            'poshto': None,
            'site_url': Site.objects.get_current()}
    email_body = """%s, %s %s
            %s %s""" % (_('Saluton'), get_enhavo(uzanto.unua_nomo, uzanto.chefa_lingvo.kodo),
                        get_enhavo(uzanto.familinomo, uzanto.chefa_lingvo.kodo),
                        _(''),
                        uzanto.konfirmita_hash
                        )

    html_body = render_to_string('registrado/registrado_nova_konfirmaretposto.html', data)
    return send_mail(_('Konfirmo de la registrado') if not restarigo else
                     _('Восстановление доступа к учетной записи Техноком'),
              email_body,
              settings.EMAIL_NO_REPLY,
              [uzanto.chefa_retposhto],
              fail_silently=True,
              html_message=html_body
              )
