# Generated by Django 2.0.13 on 2019-08-07 08:18

from django.db import migrations


def kategorio_migrado(apps, schema_editor):
    ForumojKategorio = apps.get_model('forumoj', 'ForumojKategorio')
    KonferencojKategorio = apps.get_model('konferencoj', 'KonferencojKategorio')
    KonferencojKategorioTipo = apps.get_model('konferencoj', 'KonferencojKategorioTipo')
    KomunumojAliro = apps.get_model('komunumoj', 'KomunumojAliro')

    aliro = KomunumojAliro.objects.get(kodo='chiuj')
    tipo = KonferencojKategorioTipo.objects.get(kodo='baza')

    for kat in ForumojKategorio.objects.filter(forigo=False).order_by('krea_dato'):
        KonferencojKategorio.objects.create(
            uuid=kat.uuid,
            id=kat.id,
            posedanto=kat.posedanto,
            krea_dato=kat.krea_dato,
            forigo=kat.forigo,
            arkivo=kat.arkivo,
            publikigo=True,
            publikiga_dato=kat.publikiga_dato or kat.krea_dato,
            autoro=kat.autoro,
            lasta_autoro=kat.lasta_autoro,
            lasta_dato=kat.lasta_dato,
            nomo=kat.nomo,
            priskribo=kat.priskribo,
            tipo=tipo,
            aliro=aliro
        )


def temo_migrado(apps, schema_editor):
    ForumojTemo = apps.get_model('forumoj', 'ForumojTemo')
    ForumojTemoKomento = apps.get_model('forumoj', 'ForumojTemoKomento')
    KomunumojAliro = apps.get_model('komunumoj', 'KomunumojAliro')
    KonferencojTemo = apps.get_model('konferencoj', 'KonferencojTemo')
    KonferencojTemoKomento = apps.get_model('konferencoj', 'KonferencojTemoKomento')
    KonferencojTemoTipo = apps.get_model('konferencoj', 'KonferencojTemoTipo')

    aliro = KomunumojAliro.objects.get(kodo='chiuj')
    tipo = KonferencojTemoTipo.objects.get(kodo='unua')

    for tem in ForumojTemo.objects.filter(forigo=False).order_by('krea_dato'):
        temo = KonferencojTemo.objects.create(
            id=tem.id,
            posedanto=tem.posedanto,
            krea_dato=tem.krea_dato,
            kategorio_id=tem.kategorio_id,
            forigo=tem.forigo,
            arkivo=tem.arkivo,
            publikigo=True,
            publikiga_dato=tem.publikiga_dato or tem.krea_dato,
            autoro=tem.autoro,
            lasta_autoro=tem.lasta_autoro,
            lasta_dato=tem.lasta_dato,
            nomo=tem.nomo,
            fermita=tem.fermita,
            fiksa=tem.fiksa,
            fiksa_listo=tem.fiksa_listo,
            komentado_aliro=aliro,
            tipo=tipo
        )

        KonferencojTemoKomento.objects.create(
            posedanto=temo,
            id=1,
            autoro=tem.autoro,
            krea_dato=tem.krea_dato,
            lasta_autoro=tem.lasta_autoro,
            lasta_dato=tem.lasta_dato,
            publikigo=True,
            publikiga_dato=tem.publikiga_dato or tem.krea_dato,
            forigo=tem.forigo,
            arkivo=tem.arkivo,
            teksto=tem.teksto,
        )

        id = 2
        for kom in ForumojTemoKomento.objects.filter(posedanto=tem, forigo=False).order_by('krea_dato'):
            KonferencojTemoKomento.objects.create(
                uuid=kom.uuid,
                id=id,
                krea_dato=kom.krea_dato,
                posedanto=temo,
                respondo_id=kom.respondo_id,
                teksto=kom.teksto,
                autoro=kom.autoro,
                lasta_autoro=kom.lasta_autoro,
                lasta_dato=kom.lasta_dato,
                forigo=kom.forigo,
                arkivo=kom.arkivo,
                publikigo=True,
                publikiga_dato=kom.publikiga_dato or kom.krea_dato,
            )
            id += 1


class Migration(migrations.Migration):

    dependencies = [
        ('konferencoj', '0007_auto_20190807_1001'),
    ]

    operations = [
        migrations.RunPython(kategorio_migrado),
        migrations.RunPython(temo_migrado),
    ]
