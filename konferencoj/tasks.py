"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from celery import shared_task
from django.core.mail import EmailMessage
from django.core import mail
from django.template.loader import get_template
from django.utils.translation import ugettext_lazy as _
from django.contrib.sites.models import Site
from django.urls import reverse
from django.db.models import F
from komunumoj.models import *
from uzantoj.sciigoj import sendu_sciigon
from .models import KonferencojSciigoj, KonferencojTemoKomento


class HtmlEmailMessage(EmailMessage):
    content_subtype = 'html'


def type_by_instance(inst):
    if isinstance(inst, KomunumojGrupo):
        type = 'g'
    elif isinstance(inst, KomunumojOrganizo):
        type = 'o'
    elif isinstance(inst, KomunumojSoveto):
        type = 's'
    else:
        type = 'p'
    return type


@shared_task
def send_email_comment_publication(komento_uuid):
    try:
        komento = (KonferencojTemoKomento.objects.select_related('teksto', 'posedanto')
                   .get(uuid=komento_uuid, forigo=False))
        temo = komento.posedanto
        offset = int(KonferencojTemoKomento.objects
                     .filter(posedanto=temo, forigo=False, krea_dato__lt=komento.krea_dato).count() / 20) * 20
    except KonferencojTemoKomento.DoesNotExist:
        return False

    recipients = (KonferencojSciigoj.objects
                  .filter(temo=komento.posedanto, forigo=False, sciigoj__kodo='powto',
                          autoro__is_active=True, autoro__konfirmita=True, autoro__agordoj__has_key='abono_shlosilo',
                          autoro__malbona_retposhto=False)
                  .annotate(poshto=F('autoro__chefa_retposhto'), agordoj=F('autoro__agordoj'))
                  .values('poshto', 'agordoj'))

    current_site = Site.objects.get_current()
    reverse_args = [komento.posedanto.id]

    if offset:
        reverse_args.append(offset)

    url_path = reverse('konferencoj:konferencoj_temo', args=reverse_args)
    komento_full_url = "https://{}{}#{}".format(current_site, url_path, komento.uuid)

    emails = []

    for recipient in recipients:
        # Исключаем автора комментария из рассылки
        if recipient['poshto'] == komento.autoro.chefa_retposhto:
            continue

        context = {
            'poshto': recipient['poshto'],
            'titolo': _('Новый комментарий в теме конференции') + ' "%s"' % temo.nomo.enhavo,
            'autoro': '%s %s' % (komento.autoro.unua_nomo.enhavo, komento.autoro.familinomo.enhavo),
            'dato': komento.krea_dato,
            'teksto': komento.teksto.enhavo,
            'komento_url': komento_full_url,
            'abono_shlosilo': recipient['agordoj']['abono_shlosilo'],
            'site_url': Site.objects.get_current()
        }

        unsubscribe_link = '<https://{}{}?poshto={}&shlosilo={}&nun=1>'.format(
            current_site,
            reverse('uzantoj:uzantoj_malabono'),
            recipient['poshto'],
            recipient['agordoj']['abono_shlosilo']
        )

        email_body = get_template('konferencoj/emails/konferenca_ago.html').render(context)

        email_message = {
            'subject': _('Новый комментарий в теме конференции Техноком'),
            'body': email_body,
        }

        emails.append(HtmlEmailMessage(**email_message, to=(recipient['poshto'],),
                                       headers={'List-Unsubscribe': unsubscribe_link}))

    with mail.get_connection() as connection:
        result = connection.send_messages(emails)
    return result

@shared_task
def sciigi_konferencoj(komento_uuid):
    try:
        # komento = (KonferencojTemoKomento.objects.select_related('teksto', 'posedanto')
        komento = (KonferencojTemoKomento.objects.get(uuid=komento_uuid, forigo=False))
        temo = komento.posedanto
    except KonferencojTemoKomento.DoesNotExist:
        return False

    uzantoj =  Uzanto.objects.filter(
                    konferencojsciigoj__temo=temo,
                    konferencojsciigoj__sciigoj__kodo='interna',
                    is_active=True, konfirmita=True
                                ).exclude(
                                    id=komento.autoro.id
                                )
                    
    # Пришло новое сообщение в теме конференции <название_темы_конференции>
    teksto = 'Новый комментарий в теме конференции %(nomo)s'
    parametroj = {'nomo': {'obj': temo, 'field': 'nomo'}}
    _('Новый комментарий в теме конференции %(nomo)s')


    if uzantoj:
        uzantoj = list(uzantoj.values_list('id', flat=True))
        return sendu_sciigon(teksto, to=uzantoj, objektoj=(temo,), teksto_parametroj=parametroj)

    return 0
