from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class KonferencojConfig(AppConfig):
    name = 'konferencoj'
    verbose_name = _('Konferencoj')

    def ready(self):
        import konferencoj.signals
        # Импортом сообщаем django, что надо проанализировать код в модуле tasks
        # При этом происходит регистрация функций заданий celery
        import konferencoj.tasks
