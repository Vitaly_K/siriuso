# Generated by Django 2.2.11 on 2020-04-12 10:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('universo_taskoj', '0007_auto_20200410_1307'),
    ]

    operations = [
        migrations.AddField(
            model_name='universoprojekto',
            name='fin_koordinato_x',
            field=models.FloatField(blank=True, default=None, null=True, verbose_name='Fina koordinato X'),
        ),
        migrations.AddField(
            model_name='universoprojekto',
            name='fin_koordinato_y',
            field=models.FloatField(blank=True, default=None, null=True, verbose_name='Fina koordinato Y'),
        ),
        migrations.AddField(
            model_name='universoprojekto',
            name='fin_koordinato_z',
            field=models.FloatField(blank=True, default=None, null=True, verbose_name='Fina koordinato Z'),
        ),
        migrations.AddField(
            model_name='universoprojekto',
            name='kom_koordinato_x',
            field=models.FloatField(blank=True, default=None, null=True, verbose_name='Komenca koordinato X'),
        ),
        migrations.AddField(
            model_name='universoprojekto',
            name='kom_koordinato_y',
            field=models.FloatField(blank=True, default=None, null=True, verbose_name='Komenca koordinato Y'),
        ),
        migrations.AddField(
            model_name='universoprojekto',
            name='kom_koordinato_z',
            field=models.FloatField(blank=True, default=None, null=True, verbose_name='Komenca koordinato Z'),
        ),
    ]
