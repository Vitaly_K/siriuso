# Generated by Django 2.2.11 on 2020-04-10 13:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('universo_objektoj', '0005_universoobjektosxablono_autoro'),
        ('universo_taskoj', '0006_auto_20200410_0513'),
    ]

    operations = [
        migrations.AddField(
            model_name='universoprojekto',
            name='objekto',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='universo_objektoj.UniversoObjekto', verbose_name='Objekto'),
        ),
        migrations.AddField(
            model_name='universoprojektoposedanto',
            name='posedanto_objekto',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='universo_objektoj.UniversoObjekto', verbose_name='Posedanta objekto'),
        ),
        migrations.AddField(
            model_name='universotaskoposedanto',
            name='posedanto_objekto',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='universo_objektoj.UniversoObjekto', verbose_name='Posedanta objekto'),
        ),
    ]
