"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres import fields as pgfields
from django.contrib.auth.models import Permission
from django.db.models import Max, Q

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from universo_bazo.models import UniversoBazaMaks, UniversoBazaRealeco, UniversoRealeco
from universo_objektoj.models import UniversoObjekto
from universo_uzantoj.models import UniversoUzanto
from universo_organizoj.models import UniversoOrganizo


# Категории проектов в Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoProjektoKategorio(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_projekto_kategorioj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_projektoj_kategorioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio de projektoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj de projektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_projekto_kategorioj', _('Povas vidi kategorioj de projektoj de Universo')),
            ('povas_krei_universo_projekto_kategorioj', _('Povas krei kategorioj de projektoj de Universo')),
            ('povas_forigi_universo_projekto_kategorioj', _('Povas forigi kategorioj de projektoj de Universo')),
            ('povas_shangxi_universo_projekto_kategorioj', _('Povas ŝanĝi kategorioj de projektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoProjektoKategorio, self).save(force_insert=force_insert, force_update=force_update,
                                                    using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_projekto_kategorioj',
                'universo_taskoj.povas_krei_universo_projekto_kategorioj',
                'universo_taskoj.povas_forigi_universo_projekto_kategorioj',
                'universo_taskoj.povas_shangxi_universo_projekto_kategorioj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_projekto_kategorioj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_projekto_kategorioj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_projekto_kategorioj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы проектов в Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoProjektoTipo(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_projekto_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_projektoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de projektoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de projektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_projekto_tipoj', _('Povas vidi tipoj de projektoj de Universo')),
            ('povas_krei_universo_projekto_tipoj', _('Povas krei tipoj de projektoj de Universo')),
            ('povas_forigi_universo_projekto_tipoj', _('Povas forigi tipoj de projektoj de Universo')),
            ('povas_shangxi_universo_projekto_tipoj', _('Povas ŝanĝi tipoj de projektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoProjektoTipo, self).save(force_insert=force_insert, force_update=force_update,
                                               using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_projekto_tipoj',
                'universo_taskoj.povas_krei_universo_projekto_tipoj',
                'universo_taskoj.povas_forigi_universo_projekto_tipoj',
                'universo_taskoj.povas_shangxi_universo_projekto_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_projekto_tipoj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_projekto_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_projekto_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Шаблоны проектов Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoProjektoSxablono(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # категория проектов Универсо
    kategorio = models.ManyToManyField(UniversoProjektoKategorio, verbose_name=_('Kategorio'),
                                       db_table='universo_projektoj_sxablonoj_kategorioj_ligiloj')

    # тип проектов Универсо
    tipo = models.ForeignKey(UniversoProjektoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # позиция в списке
    pozicio = models.IntegerField(_('Pozicio'), blank=True, null=True, default=None)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_projektoj_sxablonoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_projektoj_sxablonoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ŝablono de projektoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ŝablonoj de projektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_projektoj_sxablonoj', _('Povas vidi ŝablonoj de projektoj de Universo')),
            ('povas_krei_universo_projektoj_sxablonoj', _('Povas krei ŝablonoj de projektoj de Universo')),
            ('povas_forigi_universo_projektoj_sxablonoj', _('Povas forigi ŝablonoj de projektoj de Universo')),
            ('povas_shangxi_universo_projektoj_sxablonoj', _('Povas ŝanĝi ŝablonoj de projektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoProjektoSxablono, self).save(force_insert=force_insert, force_update=force_update,
                                                   using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_projektoj_sxablonoj',
                'universo_taskoj.povas_krei_universo_projektoj_sxablonoj',
                'universo_taskoj.povas_forigi_universo_projektoj_sxablonoj',
                'universo_taskoj.povas_shangxi_universo_projektoj_sxablonoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_projektoj_sxablonoj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_projektoj_sxablonoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_projektoj_sxablonoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы связей шаблонов проектов между собой, использует абстрактный класс UniversoBazaMaks
class UniversoProjektoSxablonoLigiloTipo(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_projektoj_sxablonoj_ligiloj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ligiloj de ŝablonoj de projektoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ligiloj de ŝablonoj de projektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_projektoj_sxablonoj_ligiloj_tipoj',
             _('Povas vidi tipoj de ligiloj de ŝablonoj de projektoj de Universo')),
            ('povas_krei_universo_projektoj_sxablonoj_ligiloj_tipoj',
             _('Povas krei tipoj de ligiloj de ŝablonoj de projektoj de Universo')),
            ('povas_forigi_universo_projektoj_sxablonoj_ligiloj_tipoj',
             _('Povas forigi tipoj de ligiloj de ŝablonoj de projektoj de Universo')),
            ('povas_sxangxi_universo_projektoj_sxablonoj_ligiloj_tipoj',
             _('Povas ŝanĝi tipoj de ligiloj de ŝablonoj de projektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoProjektoSxablonoLigiloTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                             using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_projektoj_sxablonoj_ligiloj_tipoj',
                'universo_taskoj.povas_krei_universo_projektoj_sxablonoj_ligiloj_tipoj',
                'universo_taskoj.povas_forigi_universo_projektoj_sxablonoj_ligiloj_tipoj',
                'universo_taskoj.povas_sxangxi_universo_projektoj_sxablonoj_ligiloj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_projektoj_sxablonoj_ligiloj_tipoj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_projektoj_sxablonoj_ligiloj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_projektoj_sxablonoj_ligiloj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь шаблонов проектов между собой, использует абстрактный класс UniversoBazaMaks
class UniversoProjektoSxablonoLigilo(UniversoBazaMaks):

    # шаблон проекта владелец связи
    posedanto = models.ForeignKey(UniversoProjektoSxablono, verbose_name=_('Ŝablono de projekto - posedanto'),
                                  blank=False, null=False, default=None, on_delete=models.CASCADE)

    # связываемый шаблон проекта
    ligilo = models.ForeignKey(UniversoProjektoSxablono, verbose_name=_('Ŝablono de projekto - ligilo'), blank=False,
                               null=False, default=None, related_name='%(app_label)s_%(class)s_ligilo',
                               on_delete=models.CASCADE)

    # тип связи шаблонов проектов
    tipo = models.ForeignKey(UniversoProjektoSxablonoLigiloTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_projektoj_sxablonoj_ligiloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ligilo de ŝablonoj de projektoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ligiloj de ŝablonoj de projektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_projektoj_sxablonoj_ligiloj',
             _('Povas vidi ligiloj de ŝablonoj de projektoj de Universo')),
            ('povas_krei_universo_projektoj_sxablonoj_ligiloj',
             _('Povas krei ligiloj de ŝablonoj de projektoj de Universo')),
            ('povas_forigi_universo_projektoj_sxablonoj_ligiloj',
             _('Povas forigi ligiloj de ŝablonoj de projektoj de Universo')),
            ('povas_sxangxi_universo_projektoj_sxablonoj_ligiloj',
             _('Povas ŝanĝi ligiloj de ŝablonoj de projektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_projektoj_sxablonoj_ligiloj',
                'universo_taskoj.povas_krei_universo_projektoj_sxablonoj_ligiloj',
                'universo_taskoj.povas_forigi_universo_projektoj_sxablonoj_ligiloj',
                'universo_taskoj.povas_sxangxi_universo_projektoj_sxablonoj_ligiloj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_projektoj_sxablonoj_ligiloj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_projektoj_sxablonoj_ligiloj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_projektoj_sxablonoj_ligiloj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Статус проекта, использует абстрактный класс UniversoBazaMaks
class UniversoProjektoStatuso(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_projektoj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso de projektoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj de projektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_projekto_statusoj', _('Povas vidi statusoj de projektoj de Universo')),
            ('povas_krei_universo_projekto_statusoj', _('Povas krei statusoj de projektoj de Universo')),
            ('povas_forigi_universo_projekto_statusoj', _('Povas forigi statusoj de projektoj de Universo')),
            ('povas_shangxi_universo_projekto_statusoj', _('Povas ŝanĝi statusoj de projektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoProjektoStatuso, self).save(force_insert=force_insert, force_update=force_update,
                                                  using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_projekto_statusoj',
                'universo_taskoj.povas_krei_universo_projekto_statusoj',
                'universo_taskoj.povas_forigi_universo_projekto_statusoj',
                'universo_taskoj.povas_shangxi_universo_projekto_statusoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_projekto_statusoj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_projekto_statusoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_projekto_statusoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Проекты Универсо, использует абстрактный класс UniversoBazaRealeco
class UniversoProjekto(UniversoBazaRealeco):

    # категория проектов Универсо
    kategorio = models.ManyToManyField(UniversoProjektoKategorio, verbose_name=_('Kategorio'),
                                       db_table='universo_projektoj_kategorioj_ligiloj')

    # шаблон проекта на основе которого создан этот проект Универсо
    sxablono = models.ForeignKey(UniversoProjektoSxablono, verbose_name=_('Ŝablono'), blank=True, null=True,
                                 default=None, on_delete=models.CASCADE)

    # тип проекта Универсо
    tipo = models.ForeignKey(UniversoProjektoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # статус проекта
    statuso = models.ForeignKey(UniversoProjektoStatuso, verbose_name=_('Statuso'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # объект Универсо
    objekto = models.ForeignKey(UniversoObjekto, verbose_name=_('Objekto'), blank=True, null=True, default=None,
                                on_delete=models.CASCADE)

    # позиция в списке
    pozicio = models.IntegerField(_('Pozicio'), blank=True, null=True, default=None)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # начальные координаты выполнения проекта по оси X в кубе
    kom_koordinato_x = models.FloatField(_('Komenca koordinato X'), blank=True, null=True, default=None)

    # начальные координаты выполнения проекта по оси Y в кубе
    kom_koordinato_y = models.FloatField(_('Komenca koordinato Y'), blank=True, null=True, default=None)

    # начальные координаты выполнения проекта по оси Z в кубе
    kom_koordinato_z = models.FloatField(_('Komenca koordinato Z'), blank=True, null=True, default=None)

    # конечные координаты выполнения проекта по оси X в кубе
    fin_koordinato_x = models.FloatField(_('Fina koordinato X'), blank=True, null=True, default=None)

    # конечные координаты выполнения проекта по оси Y в кубе
    fin_koordinato_y = models.FloatField(_('Fina koordinato Y'), blank=True, null=True, default=None)

    # конечные координаты выполнения проекта по оси Z в кубе
    fin_koordinato_z = models.FloatField(_('Fina koordinato Z'), blank=True, null=True, default=None)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_projektoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Projekto de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Projektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_projektoj', _('Povas vidi projektoj de Universo')),
            ('povas_krei_universo_projektoj', _('Povas krei projektoj de Universo')),
            ('povas_forigi_universo_projektoj', _('Povas forigi projektoj de Universo')),
            ('povas_shangxi_universo_projektoj', _('Povas ŝanĝi projektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}, uuid={}'.format(get_enhavo(self.nomo, empty_values=True)[0], self.uuid)

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(UniversoProjekto, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_projektoj',
                'universo_taskoj.povas_krei_universo_projektoj',
                'universo_taskoj.povas_forigi_universo_projektoj',
                'universo_taskoj.povas_shangxi_universo_projektoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_projektoj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_projektoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_projektoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы владельцев проектов, использует абстрактный класс UniversoBazaMaks
class UniversoProjektoPosedantoTipo(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_projektoj_posedantoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de posedantoj de projektoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de posedantoj de projektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_projektoj_posedantoj_tipoj',
             _('Povas vidi tipoj de posedantoj de projektoj de Universo')),
            ('povas_krei_universo_projektoj_posedantoj_tipoj',
             _('Povas krei tipoj de posedantoj de projektoj de Universo')),
            ('povas_forigi_universo_projektoj_posedantoj_tipoj',
             _('Povas forigi tipoj de posedantoj de projektoj de Universo')),
            ('povas_shangxi_universo_projektoj_posedantoj_tipoj',
             _('Povas ŝanĝi tipoj de posedantoj de projektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoProjektoPosedantoTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                        using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_projektoj_posedantoj_tipoj',
                'universo_taskoj.povas_krei_universo_projektoj_posedantoj_tipoj',
                'universo_taskoj.povas_forigi_universo_projektoj_posedantoj_tipoj',
                'universo_taskoj.povas_shangxi_universo_projektoj_posedantoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_projektoj_posedantoj_tipoj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_projektoj_posedantoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_projektoj_posedantoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Статус владельца проекта, использует абстрактный класс UniversoBazaMaks
class UniversoProjektoPosedantoStatuso(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_projektoj_posedantoj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso de posedantoj de projektoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj de posedantoj de projektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_projektoj_posedantoj_statusoj',
             _('Povas vidi statusoj de posedantoj de projektoj de Universo')),
            ('povas_krei_universo_projektoj_posedantoj_statusoj',
             _('Povas krei statusoj de posedantoj de projektoj de Universo')),
            ('povas_forigi_universo_projektoj_posedantoj_statusoj',
             _('Povas forigi statusoj de posedantoj de projektoj de Universo')),
            ('povas_shangxi_universo_projektoj_posedantoj_statusoj',
             _('Povas ŝanĝi statusoj de posedantoj de projektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoProjektoPosedantoStatuso, self).save(force_insert=force_insert, force_update=force_update,
                                                           using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_projektoj_posedantoj_statusoj',
                'universo_taskoj.povas_krei_universo_projektoj_posedantoj_statusoj',
                'universo_taskoj.povas_forigi_universo_projektoj_posedantoj_statusoj',
                'universo_taskoj.povas_shangxi_universo_projektoj_posedantoj_statusoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_projektoj_posedantoj_statusoj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_projektoj_posedantoj_statusoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_projektoj_posedantoj_statusoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Владельцы проектов Универсо, использует абстрактный класс UniversoBazaRealeco
class UniversoProjektoPosedanto(UniversoBazaRealeco):

    # проект
    projekto = models.ForeignKey(UniversoProjekto, verbose_name=_('Projekto'), blank=False, null=False,
                                 default=None, on_delete=models.CASCADE)

    # пользователь владелец проекта Универсо
    posedanto_uzanto = models.ForeignKey(UniversoUzanto, verbose_name=_('Posedanta uzanto'), blank=True, null=True,
                                         default=None, on_delete=models.CASCADE)

    # организация владелец проекта Универсо
    posedanto_organizo = models.ForeignKey(UniversoOrganizo, verbose_name=_('Posedanta organizo'), blank=True,
                                           null=True, default=None, on_delete=models.CASCADE)

    # объект владелец проекта Универсо
    posedanto_objekto = models.ForeignKey(UniversoObjekto, verbose_name=_('Posedanta objekto'), blank=True,
                                           null=True, default=None, on_delete=models.CASCADE)

    # тип владельца проекта
    tipo = models.ForeignKey(UniversoProjektoPosedantoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # статус владельца проекта
    statuso = models.ForeignKey(UniversoProjektoPosedantoStatuso, verbose_name=_('Statuso'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_projektoj_posedantoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Posedanto de projekto de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Posedantoj de projektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_projektoj_posedantoj', _('Povas vidi posedantoj de projektoj de Universo')),
            ('povas_krei_universo_projektoj_posedantoj', _('Povas krei posedantoj de projektoj de Universo')),
            ('povas_forigi_universo_projektoj_posedantoj', _('Povas forigi posedantoj de projektoj de Universo')),
            ('povas_shangxi_universo_projektoj_posedantoj', _('Povas ŝanĝi posedantoj de projektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле projekto этой модели
        return '{}'.format(self.projekto)

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(UniversoProjektoPosedanto, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_projektoj_posedantoj',
                'universo_taskoj.povas_krei_universo_projektoj_posedantoj',
                'universo_taskoj.povas_forigi_universo_projektoj_posedantoj',
                'universo_taskoj.povas_shangxi_universo_projektoj_posedantoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_projektoj_posedantoj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_projektoj_posedantoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_projektoj_posedantoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы связей проектов между собой, использует абстрактный класс UniversoBazaMaks
class UniversoProjektoLigiloTipo(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_projektoj_ligiloj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ligiloj de projektoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ligiloj de projektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_projektoj_ligiloj_tipoj',
             _('Povas vidi tipoj de ligiloj de projektoj de Universo')),
            ('povas_krei_universo_projektoj_ligiloj_tipoj',
             _('Povas krei tipoj de ligiloj de projektoj de Universo')),
            ('povas_forigi_universo_projektoj_ligiloj_tipoj',
             _('Povas forigi tipoj de ligiloj de projektoj de Universo')),
            ('povas_sxangxi_universo_projektoj_ligiloj_tipoj',
             _('Povas ŝanĝi tipoj de ligiloj de projektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoProjektoLigiloTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                     using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_projektoj_ligiloj_tipoj',
                'universo_taskoj.povas_krei_universo_projektoj_ligiloj_tipoj',
                'universo_taskoj.povas_forigi_universo_projektoj_ligiloj_tipoj',
                'universo_taskoj.povas_sxangxi_universo_projektoj_ligiloj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_projektoj_ligiloj_tipoj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_projektoj_ligiloj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_projektoj_ligiloj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь проектов между собой, использует абстрактный класс UniversoBazaMaks
class UniversoProjektoLigilo(UniversoBazaMaks):

    # проект владелец связи
    posedanto = models.ForeignKey(UniversoProjekto, verbose_name=_('Projekto - posedanto'),
                                  blank=False, null=False, default=None, on_delete=models.CASCADE)

    # связываемый проект
    ligilo = models.ForeignKey(UniversoProjekto, verbose_name=_('Projekto - ligilo'), blank=False,
                               null=False, default=None, related_name='%(app_label)s_%(class)s_ligilo',
                               on_delete=models.CASCADE)

    # тип связи проектов
    tipo = models.ForeignKey(UniversoProjektoLigiloTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_projektoj_ligiloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ligilo de projektoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ligiloj de projektoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_projektoj_ligiloj', _('Povas vidi ligiloj de projektoj de Universo')),
            ('povas_krei_universo_projektoj_ligiloj', _('Povas krei ligiloj de projektoj de Universo')),
            ('povas_forigi_universo_projektoj_ligiloj', _('Povas forigi ligiloj de projektoj de Universo')),
            ('povas_sxangxi_universo_projektoj_ligiloj', _('Povas ŝanĝi ligiloj de projektoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(UniversoProjektoLigilo, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_projektoj_ligiloj',
                'universo_taskoj.povas_krei_universo_projektoj_ligiloj',
                'universo_taskoj.povas_forigi_universo_projektoj_ligiloj',
                'universo_taskoj.povas_sxangxi_universo_projektoj_ligiloj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_projektoj_ligiloj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_projektoj_ligiloj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_projektoj_ligiloj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Категории задач в Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoTaskoKategorio(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_taskoj_kategorioj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_taskoj_kategorioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio de taskoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj de taskoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_taskoj_kategorioj', _('Povas vidi kategorioj de taskoj de Universo')),
            ('povas_krei_universo_taskoj_kategorioj', _('Povas krei kategorioj de taskoj de Universo')),
            ('povas_forigi_universo_taskoj_kategorioj', _('Povas forigi kategorioj de taskoj de Universo')),
            ('povas_shangxi_universo_taskoj_kategorioj', _('Povas ŝanĝi kategorioj de taskoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoTaskoKategorio, self).save(force_insert=force_insert, force_update=force_update,
                                                 using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_taskoj_kategorioj',
                'universo_taskoj.povas_krei_universo_taskoj_kategorioj',
                'universo_taskoj.povas_forigi_universo_taskoj_kategorioj',
                'universo_taskoj.povas_shangxi_universo_taskoj_kategorioj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_taskoj_kategorioj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_taskoj_kategorioj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_taskoj_kategorioj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы задач в Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoTaskoTipo(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_taskoj_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_taskoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de taskoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de taskoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_taskoj_tipoj', _('Povas vidi tipoj de taskoj de Universo')),
            ('povas_krei_universo_taskoj_tipoj', _('Povas krei tipoj de taskoj de Universo')),
            ('povas_forigi_universo_taskoj_tipoj', _('Povas forigi tipoj de taskoj de Universo')),
            ('povas_shangxi_universo_taskoj_tipoj', _('Povas ŝanĝi tipoj de taskoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoTaskoTipo, self).save(force_insert=force_insert, force_update=force_update,
                                            using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_taskoj_tipoj',
                'universo_taskoj.povas_krei_universo_taskoj_tipoj',
                'universo_taskoj.povas_forigi_universo_taskoj_tipoj',
                'universo_taskoj.povas_shangxi_universo_taskoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_taskoj_tipoj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_taskoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_taskoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Шаблоны задач Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoTaskoSxablono(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # категория задач Универсо
    kategorio = models.ManyToManyField(UniversoTaskoKategorio, verbose_name=_('Kategorio'),
                                       db_table='universo_taskoj_sxablono_kategorioj_ligiloj')

    # тип проектов Универсо
    tipo = models.ForeignKey(UniversoTaskoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # шаблон проекта Универсо в который входит задача
    projekto = models.ForeignKey(UniversoProjektoSxablono, verbose_name=_('Projekto'), blank=False, default=None,
                                 on_delete=models.CASCADE)

    # позиция в списке
    pozicio = models.IntegerField(_('Pozicio'), blank=True, null=True, default=None)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_taskoj_sxablonoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_taskoj_sxablonoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ŝablono de taskoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ŝablonoj de taskoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_taskoj_sxablonoj', _('Povas vidi ŝablonoj de taskoj de Universo')),
            ('povas_krei_universo_taskoj_sxablonoj', _('Povas krei ŝablonoj de taskoj de Universo')),
            ('povas_forigi_universo_taskoj_sxablonoj', _('Povas forigi ŝablonoj de taskoj de Universo')),
            ('povas_shangxi_universo_taskoj_sxablonoj', _('Povas ŝanĝi ŝablonoj de taskoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoTaskoSxablono, self).save(force_insert=force_insert, force_update=force_update,
                                                using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_taskoj_sxablonoj',
                'universo_taskoj.povas_krei_universo_taskoj_sxablonoj',
                'universo_taskoj.povas_forigi_universo_taskoj_sxablonoj',
                'universo_taskoj.povas_shangxi_universo_taskoj_sxablonoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_taskoj_sxablonoj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_taskoj_sxablonoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_taskoj_sxablonoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы связей шаблонов задач между собой, использует абстрактный класс UniversoBazaMaks
class UniversoTaskoSxablonoLigiloTipo(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_taskoj_sxablonoj_ligiloj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ligiloj de ŝablonoj de taskoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ligiloj de ŝablonoj de taskoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_taskoj_sxablonoj_ligiloj_tipoj',
             _('Povas vidi tipoj de ligiloj de ŝablonoj de taskoj de Universo')),
            ('povas_krei_universo_taskoj_sxablonoj_ligiloj_tipoj',
             _('Povas krei tipoj de ligiloj de ŝablonoj de taskoj de Universo')),
            ('povas_forigi_universo_taskoj_sxablonoj_ligiloj_tipoj',
             _('Povas forigi tipoj de ligiloj de ŝablonoj de taskoj de Universo')),
            ('povas_sxangxi_universo_taskoj_sxablonoj_ligiloj_tipoj',
             _('Povas ŝanĝi tipoj de ligiloj de ŝablonoj de taskoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoTaskoSxablonoLigiloTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                          using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_taskoj_sxablonoj_ligiloj_tipoj',
                'universo_taskoj.povas_krei_universo_taskoj_sxablonoj_ligiloj_tipoj',
                'universo_taskoj.povas_forigi_universo_taskoj_sxablonoj_ligiloj_tipoj',
                'universo_taskoj.povas_sxangxi_universo_taskoj_sxablonoj_ligiloj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_taskoj_sxablonoj_ligiloj_tipoj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_taskoj_sxablonoj_ligiloj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_taskoj_sxablonoj_ligiloj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь шаблонов задач между собой, использует абстрактный класс UniversoBazaMaks
class UniversoTaskoSxablonoLigilo(UniversoBazaMaks):

    # шаблон задачи владелец связи
    posedanto = models.ForeignKey(UniversoTaskoSxablono, verbose_name=_('Ŝablono de tasko - posedanto'),
                                  blank=False, null=False, default=None, on_delete=models.CASCADE)

    # связываемый шаблон задачи
    ligilo = models.ForeignKey(UniversoTaskoSxablono, verbose_name=_('Ŝablono de tasko - ligilo'), blank=False,
                               null=False, default=None, related_name='%(app_label)s_%(class)s_ligilo',
                               on_delete=models.CASCADE)

    # тип связи шаблонов задачи
    tipo = models.ForeignKey(UniversoTaskoSxablonoLigiloTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_taskoj_sxablonoj_ligiloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ligilo de ŝablonoj de taskoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ligiloj de ŝablonoj de taskoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_taskoj_sxablonoj_ligiloj',
             _('Povas vidi ligiloj de ŝablonoj de taskoj de Universo')),
            ('povas_krei_universo_taskoj_sxablonoj_ligiloj',
             _('Povas krei ligiloj de ŝablonoj de taskoj de Universo')),
            ('povas_forigi_universo_taskoj_sxablonoj_ligiloj',
             _('Povas forigi ligiloj de ŝablonoj de taskoj de Universo')),
            ('povas_sxangxi_universo_taskoj_sxablonoj_ligiloj',
             _('Povas ŝanĝi ligiloj de ŝablonoj de taskoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_taskoj_sxablonoj_ligiloj',
                'universo_taskoj.povas_krei_universo_taskoj_sxablonoj_ligiloj',
                'universo_taskoj.povas_forigi_universo_taskoj_sxablonoj_ligiloj',
                'universo_taskoj.povas_sxangxi_universo_taskoj_sxablonoj_ligiloj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_taskoj_sxablonoj_ligiloj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_taskoj_sxablonoj_ligiloj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_taskoj_sxablonoj_ligiloj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Статус задачи, использует абстрактный класс UniversoBazaMaks
class UniversoTaskoStatuso(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_taskoj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso de taskoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj de taskoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_taskoj_statusoj', _('Povas vidi statusoj de taskoj de Universo')),
            ('povas_krei_universo_taskoj_statusoj', _('Povas krei statusoj de taskoj de Universo')),
            ('povas_forigi_universo_taskoj_statusoj', _('Povas forigi statusoj de taskoj de Universo')),
            ('povas_shangxi_universo_taskoj_statusoj', _('Povas ŝanĝi statusoj de taskoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoTaskoStatuso, self).save(force_insert=force_insert, force_update=force_update,
                                               using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_taskoj_statusoj',
                'universo_taskoj.povas_krei_universo_taskoj_statusoj',
                'universo_taskoj.povas_forigi_universo_taskoj_statusoj',
                'universo_taskoj.povas_shangxi_universo_taskoj_statusoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_taskoj_statusoj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_taskoj_statusoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_taskoj_statusoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Задачи Универсо, использует абстрактный класс UniversoBazaRealeco
class UniversoTasko(UniversoBazaRealeco):

    # проект Универсо в который входит задача
    projekto = models.ForeignKey(UniversoProjekto, verbose_name=_('Projekto'), blank=False, default=None,
                                 on_delete=models.CASCADE)

    # объект Универсо
    objekto = models.ForeignKey(UniversoObjekto, verbose_name=_('Objekto'), blank=True, null=True, default=None,
                                on_delete=models.CASCADE)

    # категория задач Универсо
    kategorio = models.ManyToManyField(UniversoTaskoKategorio, verbose_name=_('Kategorio'),
                                       db_table='universo_taskoj_kategorioj_ligiloj')

    # шаблон задачи на основе которого создана эта задача Универсо
    sxablono = models.ForeignKey(UniversoTaskoSxablono, verbose_name=_('Ŝablono'), blank=True, null=True, default=None,
                                 on_delete=models.CASCADE)

    # тип задачи Универсо
    tipo = models.ForeignKey(UniversoTaskoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # статус задачи
    statuso = models.ForeignKey(UniversoTaskoStatuso, verbose_name=_('Statuso'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # позиция в списке
    pozicio = models.IntegerField(_('Pozicio'), blank=True, null=True, default=None)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # начальные координаты выполнения задачи по оси X в кубе
    kom_koordinato_x = models.FloatField(_('Komenca koordinato X'), blank=True, null=True, default=None)

    # начальные координаты выполнения задачи по оси Y в кубе
    kom_koordinato_y = models.FloatField(_('Komenca koordinato Y'), blank=True, null=True, default=None)

    # начальные координаты выполнения задачи по оси Z в кубе
    kom_koordinato_z = models.FloatField(_('Komenca koordinato Z'), blank=True, null=True, default=None)

    # конечные координаты выполнения задачи по оси X в кубе
    fin_koordinato_x = models.FloatField(_('Fina koordinato X'), blank=True, null=True, default=None)

    # конечные координаты выполнения задачи по оси Y в кубе
    fin_koordinato_y = models.FloatField(_('Fina koordinato Y'), blank=True, null=True, default=None)

    # конечные координаты выполнения задачи по оси Z в кубе
    fin_koordinato_z = models.FloatField(_('Fina koordinato Z'), blank=True, null=True, default=None)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_taskoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tasko de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Taskoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_taskoj', _('Povas vidi taskoj de Universo')),
            ('povas_krei_universo_taskoj', _('Povas krei taskoj de Universo')),
            ('povas_forigi_universo_taskoj', _('Povas forigi taskoj de Universo')),
            ('povas_shangxi_universo_taskoj', _('Povas ŝanĝi taskoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(UniversoTasko, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_taskoj',
                'universo_taskoj.povas_krei_universo_taskoj',
                'universo_taskoj.povas_forigi_universo_taskoj',
                'universo_taskoj.povas_shangxi_universo_taskoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_taskoj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_taskoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_taskoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы владельцев задач, использует абстрактный класс UniversoBazaMaks
class UniversoTaskoPosedantoTipo(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_taskoj_posedantoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de posedantoj de taskoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de posedantoj de taskoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_taskoj_posedantoj_tipoj', _('Povas vidi tipoj de posedantoj de taskoj de Universo')),
            ('povas_krei_universo_taskoj_posedantoj_tipoj', _('Povas krei tipoj de posedantoj de taskoj de Universo')),
            ('povas_forigi_universo_taskoj_posedantoj_tipoj', _('Povas forigi tipoj de posedantoj de taskoj de Universo')),
            ('povas_shangxi_universo_taskoj_posedantoj_tipoj', _('Povas ŝanĝi tipoj de posedantoj de taskoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoTaskoPosedantoTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                     using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_taskoj_posedantoj_tipoj',
                'universo_taskoj.povas_krei_universo_taskoj_posedantoj_tipoj',
                'universo_taskoj.povas_forigi_universo_taskoj_posedantoj_tipoj',
                'universo_taskoj.povas_shangxi_universo_taskoj_posedantoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_taskoj_posedantoj_tipoj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_taskoj_posedantoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_taskoj_posedantoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Статус владельца задачи, использует абстрактный класс UniversoBazaMaks
class UniversoTaskoPosedantoStatuso(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_taskoj_posedantoj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso de posedantoj de taskoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj de posedantoj de taskoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_taskoj_posedantoj_statusoj',
             _('Povas vidi statusoj de posedantoj de taskoj de Universo')),
            ('povas_krei_universo_taskoj_posedantoj_statusoj',
             _('Povas krei statusoj de posedantoj de taskoj de Universo')),
            ('povas_forigi_universo_taskoj_posedantoj_statusoj',
             _('Povas forigi statusoj de posedantoj de taskoj de Universo')),
            ('povas_shangxi_universo_taskoj_posedantoj_statusoj',
             _('Povas ŝanĝi statusoj de posedantoj de taskoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoTaskoPosedantoStatuso, self).save(force_insert=force_insert, force_update=force_update,
                                                        using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_taskoj_posedantoj_statusoj',
                'universo_taskoj.povas_krei_universo_taskoj_posedantoj_statusoj',
                'universo_taskoj.povas_forigi_universo_taskoj_posedantoj_statusoj',
                'universo_taskoj.povas_shangxi_universo_taskoj_posedantoj_statusoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_taskoj_posedantoj_statusoj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_taskoj_posedantoj_statusoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_taskoj_posedantoj_statusoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Владельцы задач Универсо, использует абстрактный класс UniversoBazaRealeco
class UniversoTaskoPosedanto(UniversoBazaRealeco):

    # задача
    tasko = models.ForeignKey(UniversoTasko, verbose_name=_('Tasko'), blank=False, null=False,
                              default=None, on_delete=models.CASCADE)

    # пользователь владелец задачи Универсо
    posedanto_uzanto = models.ForeignKey(UniversoUzanto, verbose_name=_('Posedanta uzanto'), blank=True, null=True,
                                         default=None, on_delete=models.CASCADE)

    # организация владелец задачи Универсо
    posedanto_organizo = models.ForeignKey(UniversoOrganizo, verbose_name=_('Posedanta organizo'), blank=True,
                                           null=True, default=None, on_delete=models.CASCADE)

    # объект владелец задачи Универсо
    posedanto_objekto = models.ForeignKey(UniversoObjekto, verbose_name=_('Posedanta objekto'), blank=True,
                                           null=True, default=None, on_delete=models.CASCADE)

    # тип владельца задачи
    tipo = models.ForeignKey(UniversoTaskoPosedantoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)
    
    # статус владельца задачи
    statuso = models.ForeignKey(UniversoTaskoPosedantoStatuso, verbose_name=_('Statuso'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_taskoj_posedantoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Posedanto de tasko de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Posedantoj de taskoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_taskoj_posedantoj', _('Povas vidi posedantoj de taskoj de Universo')),
            ('povas_krei_universo_taskoj_posedantoj', _('Povas krei posedantoj de taskoj de Universo')),
            ('povas_forigi_universo_taskoj_posedantoj', _('Povas forigi posedantoj de taskoj de Universo')),
            ('povas_shangxi_universo_taskoj_posedantoj', _('Povas ŝanĝi posedantoj de taskoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле tasko этой модели
        return '{}'.format(self.tasko)

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(UniversoTaskoPosedanto, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_taskoj_posedantoj',
                'universo_taskoj.povas_krei_universo_taskoj_posedantoj',
                'universo_taskoj.povas_forigi_universo_taskoj_posedantoj',
                'universo_taskoj.povas_shangxi_universo_taskoj_posedantoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_taskoj_posedantoj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_taskoj_posedantoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_taskoj_posedantoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы связей задач между собой, использует абстрактный класс UniversoBazaMaks
class UniversoTaskoLigiloTipo(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_taskoj_ligiloj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ligiloj de taskoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ligiloj de taskoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_taskoj_ligiloj_tipoj',
             _('Povas vidi tipoj de ligiloj de taskoj de Universo')),
            ('povas_krei_universo_taskoj_ligiloj_tipoj',
             _('Povas krei tipoj de ligiloj de taskoj de Universo')),
            ('povas_forigi_universo_taskoj_ligiloj_tipoj',
             _('Povas forigi tipoj de ligiloj de taskoj de Universo')),
            ('povas_sxangxi_universo_taskoj_ligiloj_tipoj',
             _('Povas ŝanĝi tipoj de ligiloj de taskoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoTaskoLigiloTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                          using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_taskoj_ligiloj_tipoj',
                'universo_taskoj.povas_krei_universo_taskoj_ligiloj_tipoj',
                'universo_taskoj.povas_forigi_universo_taskoj_ligiloj_tipoj',
                'universo_taskoj.povas_sxangxi_universo_taskoj_ligiloj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_taskoj_ligiloj_tipoj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_taskoj_ligiloj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_taskoj_ligiloj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь задач между собой, использует абстрактный класс UniversoBazaMaks
class UniversoTaskoLigilo(UniversoBazaMaks):

    # задача владелец связи
    posedanto = models.ForeignKey(UniversoTasko, verbose_name=_('Tasko - posedanto'),
                                  blank=False, null=False, default=None, on_delete=models.CASCADE)

    # связываемая задача
    ligilo = models.ForeignKey(UniversoTasko, verbose_name=_('Tasko - ligilo'), blank=False,
                               null=False, default=None, related_name='%(app_label)s_%(class)s_ligilo',
                               on_delete=models.CASCADE)

    # тип связи шаблонов задачи
    tipo = models.ForeignKey(UniversoTaskoLigiloTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_taskoj_ligiloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ligilo de taskoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ligiloj de taskoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_taskoj_ligiloj', _('Povas vidi ligiloj de taskoj de Universo')),
            ('povas_krei_universo_taskoj_ligiloj', _('Povas krei ligiloj de taskoj de Universo')),
            ('povas_forigi_universo_taskoj_ligiloj', _('Povas forigi ligiloj de taskoj de Universo')),
            ('povas_sxangxi_universo_taskoj_ligiloj', _('Povas ŝanĝi ligiloj de taskoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(UniversoTaskoLigilo, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_taskoj.povas_vidi_universo_taskoj_ligiloj',
                'universo_taskoj.povas_krei_universo_taskoj_ligiloj',
                'universo_taskoj.povas_forigi_universo_taskoj_ligiloj',
                'universo_taskoj.povas_sxangxi_universo_taskoj_ligiloj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_taskoj.povas_vidi_universo_taskoj_ligiloj')
                    or user_obj.has_perm('universo_taskoj.povas_vidi_universo_taskoj_ligiloj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_taskoj.povas_vidi_universo_taskoj_ligiloj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
