"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
16.05.2019
"""
from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from universo_uzantoj.models import UniversoUzanto
from universo_objektoj.models import UniversoObjektoPosedanto
from .schema import *
from ..models import *


# Модель категорий проектов в Универсо
class RedaktuUniversoProjektoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_projekto_kategorioj = graphene.Field(UniversoProjektoKategorioNode,
        description=_('Созданная/изменённая категория проектов в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_projekto_kategorioj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_taskoj.povas_krei_universo_projekto_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_projekto_kategorioj = UniversoProjektoKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_projekto_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_projekto_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_projekto_kategorioj.realeco.set(realeco)
                                else:
                                    universo_projekto_kategorioj.realeco.clear()

                            universo_projekto_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_projekto_kategorioj = UniversoProjektoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_taskoj.povas_forigi_universo_projekto_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_taskoj.povas_shanghi_universo_projekto_kategorioj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_projekto_kategorioj.forigo = kwargs.get('forigo', universo_projekto_kategorioj.forigo)
                                universo_projekto_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_projekto_kategorioj.arkivo = kwargs.get('arkivo', universo_projekto_kategorioj.arkivo)
                                universo_projekto_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_projekto_kategorioj.publikigo = kwargs.get('publikigo', universo_projekto_kategorioj.publikigo)
                                universo_projekto_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_projekto_kategorioj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_projekto_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_projekto_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_projekto_kategorioj.realeco.set(realeco)
                                    else:
                                        universo_projekto_kategorioj.realeco.clear()

                                universo_projekto_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoProjektoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoProjektoKategorio(status=status, message=message, universo_projekto_kategorioj=universo_projekto_kategorioj)


# Модель типов проектов в Универсо
class RedaktuUniversoProjektoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_projekto_tipoj = graphene.Field(UniversoProjektoTipoNode, 
        description=_('Созданный/изменённый тип проектов в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_projekto_tipoj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_taskoj.povas_krei_universo_projekto_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_projekto_tipoj = UniversoProjektoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_projekto_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_projekto_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_projekto_tipoj.realeco.set(realeco)
                                else:
                                    universo_projekto_tipoj.realeco.clear()

                            universo_projekto_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_projekto_tipoj = UniversoProjektoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_taskoj.povas_forigi_universo_projekto_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_taskoj.povas_shanghi_universo_projekto_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_projekto_tipoj.forigo = kwargs.get('forigo', universo_projekto_tipoj.forigo)
                                universo_projekto_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_projekto_tipoj.arkivo = kwargs.get('arkivo', universo_projekto_tipoj.arkivo)
                                universo_projekto_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_projekto_tipoj.publikigo = kwargs.get('publikigo', universo_projekto_tipoj.publikigo)
                                universo_projekto_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_projekto_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_projekto_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_projekto_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_projekto_tipoj.realeco.set(realeco)
                                    else:
                                        universo_projekto_tipoj.realeco.clear()

                                universo_projekto_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoProjektoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoProjektoTipo(status=status, message=message, universo_projekto_tipoj=universo_projekto_tipoj)


# Модель шаблонов проектов в Универсо
class RedaktuUniversoProjektoSxablono(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_projekto_sxablonoj = graphene.Field(UniversoProjektoSxablonoNode, 
        description=_('Созданный/изменённый шаблон проектов в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий проектов Универсо'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))
        tipo_id = graphene.Int(description=_('Тип проектов Универсо'))
        pozicio = graphene.Int(description=_('Позиция в списке'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_projekto_sxablonoj = None
        autoro = None
        tipo = None
        kategorio = UniversoProjektoKategorio.objects.none()
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_taskoj.povas_krei_universo_projekto_sxablonoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'kategorio' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                            elif not len(kwargs.get('kategorio')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'kategorio')
                            else:
                                kategorio_id = set(kwargs.get('kategorio'))

                                if len(kategorio_id):
                                    kategorio = UniversoProjektoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие категории проектов Универсо'),
                                                    str(dif)
                                        )

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoProjektoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoProjektoTipo.DoesNotExist:
                                    message = _('Неверный тип проектов Универсо')
                            else:
                                message = _('Не указан тип проектов Универсо')

                        if not message:
                            universo_projekto_sxablonoj = UniversoProjektoSxablono.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                tipo = tipo,
                                pozicio = kwargs.get('pozicio', None),
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_projekto_sxablonoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_projekto_sxablonoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_projekto_sxablonoj.realeco.set(realeco)
                                else:
                                    universo_projekto_sxablonoj.realeco.clear()
                            if 'kategorio' in kwargs:
                                if kategorio:
                                    universo_projekto_sxablonoj.kategorio.set(kategorio)
                                else:
                                    universo_projekto_sxablonoj.kategorio.clear()

                            universo_projekto_sxablonoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False) or kwargs.get('kategorio', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('pozicio', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_projekto_sxablonoj = UniversoProjektoSxablono.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_taskoj.povas_forigi_universo_projekto_sxablonoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_taskoj.povas_shanghi_universo_projekto_sxablonoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))
                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))
                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))
                                    if len(kategorio_id):
                                        kategorio = UniversoProjektoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))
                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категории проектов Универсо'),
                                                        str(dif)
                                            )

                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoProjektoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoProjektoTipo.DoesNotExist:
                                        message = _('Неверный тип проектов Универсо')

                            if not message:

                                universo_projekto_sxablonoj.forigo = kwargs.get('forigo', universo_projekto_sxablonoj.forigo)
                                universo_projekto_sxablonoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_projekto_sxablonoj.arkivo = kwargs.get('arkivo', universo_projekto_sxablonoj.arkivo)
                                universo_projekto_sxablonoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_projekto_sxablonoj.publikigo = kwargs.get('publikigo', universo_projekto_sxablonoj.publikigo)
                                universo_projekto_sxablonoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_projekto_sxablonoj.autoro = autoro
                                universo_projekto_sxablonoj.tipo = tipo if kwargs.get('tipo_id', False) else universo_projekto_sxablonoj.tipo
                                universo_projekto_sxablonoj.pozicio = kwargs.get('pozicio', universo_projekto_sxablonoj.pozicio)

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_projekto_sxablonoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_projekto_sxablonoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_projekto_sxablonoj.realeco.set(realeco)
                                    else:
                                        universo_projekto_sxablonoj.realeco.clear()
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        universo_projekto_sxablonoj.kategorio.set(kategorio)
                                    else:
                                        universo_projekto_sxablonoj.kategorio.clear()

                                universo_projekto_sxablonoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoProjektoSxablono.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoProjektoSxablono(status=status, message=message, universo_projekto_sxablonoj=universo_projekto_sxablonoj)


# Модель статусов проектов в Универсо
class RedaktuUniversoProjektoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_projekto_statusoj = graphene.Field(UniversoProjektoStatusoNode, 
        description=_('Созданный/изменённый статусов проектов в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_projekto_statusoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_taskoj.povas_krei_universo_projekto_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_projekto_statusoj = UniversoProjektoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_projekto_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_projekto_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_projekto_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_projekto_statusoj = UniversoProjektoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_taskoj.povas_forigi_universo_projekto_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_taskoj.povas_shanghi_universo_projekto_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_projekto_statusoj.forigo = kwargs.get('forigo', universo_projekto_statusoj.forigo)
                                universo_projekto_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_projekto_statusoj.arkivo = kwargs.get('arkivo', universo_projekto_statusoj.arkivo)
                                universo_projekto_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_projekto_statusoj.publikigo = kwargs.get('publikigo', universo_projekto_statusoj.publikigo)
                                universo_projekto_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_projekto_statusoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_projekto_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_projekto_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_projekto_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoProjektoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoProjektoStatuso(status=status, message=message, universo_projekto_statusoj=universo_projekto_statusoj)


# Модель проектов в Универсо
class RedaktuUniversoProjekto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_projekto = graphene.Field(UniversoProjektoNode, 
        description=_('Созданный/изменённый проект в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир Универсо'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий проектов Универсо'))
        sxablono_id = graphene.Int(description=_('Шаблон проектов Универсо'))
        tipo_id = graphene.Int(description=_('Тип проектов Универсо'))
        statuso_id = graphene.Int(description=_('Статус проектов Универсо'))
        pozicio = graphene.Int(description=_('Позиция в списке'))
        objekto_uuid = graphene.String(description=_('Объект Универсо'))
        kom_koordinato_x = graphene.Float(description=_('Начальные координаты выполнения проекта по оси X в кубе Универсо'))
        kom_koordinato_y = graphene.Float(description=_('Начальные координаты выполнения проекта по оси Y в кубе Универсо'))
        kom_koordinato_z = graphene.Float(description=_('Начальные координаты выполнения проекта по оси Z в кубе Универсо'))
        fin_koordinato_x = graphene.Float(description=_('Конечные координаты выполнения проекта по оси X в кубе Универсо'))
        fin_koordinato_y = graphene.Float(description=_('Конечные координаты выполнения проекта по оси Y в кубе Универсо'))
        fin_koordinato_z = graphene.Float(description=_('Конечные координаты выполнения проекта по оси z в кубе Универсо'))

        # параметры для создания владельца
        posedanto_uzanto_siriuso_uzanto_id = graphene.Int(description=_('Владелец задачи Универсо'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец задачи Универсо'))
        posedanto_tipo_id = graphene.Int(description=_('Тип владельца задачи Универсо'))
        posedanto_statuso_id = graphene.Int(description=_('Статус владельца задачи Универсо'))
        # posedanto_objekto_uuid = graphene.String(description=_('Объект владелец задачи Универсо'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_projekto = None
        realeco = None
        kategorio = UniversoProjektoKategorio.objects.none()
        sxablono = None
        tipo = None
        statuso = None
        objekto = None
        posedanto_uzanto_siriuso_uzanto = None
        posedanto_organizo = None
        posedanto_tipo = None
        posedanto_statuso = None
        # posedanto_objekto = None
        uzanto = info.context.user
        posedanto_objekto_perm = None # если пользователь владелец объекта по проекту
        universo_uzanto = None

        if uzanto.is_authenticated:
            with transaction.atomic():
                # находим пользователя Универсо
                try:
                    universo_uzanto = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                except UniversoUzanto.DoesNotExist:
                    message = _('Неверный пользователь Universo')
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    # если пользователь владелец объекта, по которому создаётся проект, то можно создавать
                    # проверяем по полю posedanto
                    if 'objekto_uuid' in kwargs:
                        try:
                            objekto = UniversoObjekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                            try:
                                posedanto_objekto_perm = UniversoObjektoPosedanto.objects.get(objekto=objekto, 
                                            posedanto_uzanto=universo_uzanto,
                                            forigo=False, arkivo=False, publikigo=True)
                                if (kwargs.get('posedanto_tipo_id', False)) and posedanto_objekto_perm:
                                    if not message: # если сразу задаётся владелец, то проверяем и владельца
                                        if 'posedanto_uzanto_siriuso_uzanto_id' in kwargs:
                                            try:
                                                posedanto_uzanto_siriuso_uzanto = UniversoUzanto.objects.get(
                                                    siriuso_uzanto_id=kwargs.get('posedanto_uzanto_siriuso_uzanto_id'), 
                                                    publikigo=True
                                                )
                                                # Если владельцы разные, то права на создание проекта, как владельца объекта снимаются
                                                if posedanto_objekto_perm.posedanto_uzanto != posedanto_uzanto_siriuso_uzanto:
                                                    posedanto_objekto_perm = None
                                            except UniversoUzanto.DoesNotExist:
                                                message = _('Неверный владелец/пользователь проекта Универсо')
                            except UniversoObjektoPosedanto.DoesNotExist:
                                pass
                        except UniversoObjekto.DoesNotExist:
                            message = '{} "{}"'.format(_('Неверный объект владелец проекта Универсо'),'objekto_uuid')

                    if uzanto.has_perm('universo_taskoj.povas_krei_universo_projektoj') or posedanto_objekto_perm:
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'kategorio' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                            elif not len(kwargs.get('kategorio')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'kategorio')
                            else:
                                kategorio_id = set(kwargs.get('kategorio'))

                                if len(kategorio_id):
                                    kategorio = UniversoProjektoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие категории проектов Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'sxablono_id' in kwargs:
                                try:
                                    sxablono = UniversoProjektoSxablono.objects.get(id=kwargs.get('sxablono_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoProjektoSxablono.DoesNotExist:
                                    message = _('Неверный тип шаблона Универсо')

                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoRealeco.DoesNotExist:
                                    message = _('Неверный параллельный мир Универсо')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoProjektoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoProjektoTipo.DoesNotExist:
                                    message = _('Неверный тип проектов Универсо')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = UniversoProjektoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoProjektoStatuso.DoesNotExist:
                                    message = _('Неверный статус проектов Универсо')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')

                        if not message:
                            if 'objekto_uuid' in kwargs:
                                try:
                                    objekto = UniversoObjekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoObjekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект Универсо'),'objekto_uuid')

                        if not message:
                            universo_projekto = UniversoProjekto.objects.create(
                                forigo=False,
                                arkivo=False,
                                realeco = realeco,
                                tipo = tipo,
                                sxablono = sxablono,
                                statuso = statuso,
                                objekto = objekto,
                                kom_koordinato_x = kwargs.get('kom_koordinato_x', None),
                                kom_koordinato_y = kwargs.get('kom_koordinato_y', None),
                                kom_koordinato_z = kwargs.get('kom_koordinato_z', None),
                                fin_koordinato_x = kwargs.get('fin_koordinato_x', None),
                                fin_koordinato_y = kwargs.get('fin_koordinato_y', None),
                                fin_koordinato_z = kwargs.get('fin_koordinato_z', None),
                                pozicio = kwargs.get('pozicio', None),
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_projekto.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_projekto.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'kategorio' in kwargs:
                                if kategorio:
                                    universo_projekto.kategorio.set(kategorio)
                                else:
                                    universo_projekto.kategorio.clear()

                            universo_projekto.save()

                            status = True
                            # далее блок добавления владельца
                            if (kwargs.get('posedanto_tipo_id', False)):
                                if not message:
                                    try:
                                        posedanto_tipo = UniversoProjektoPosedantoTipo.objects.get(id=kwargs.get('posedanto_tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoProjektoPosedantoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип владельца проекта Универсо'),'posedanto_tipo_id')

                                if not message:
                                    if 'posedanto_statuso_id' in kwargs:
                                        try:
                                            posedanto_statuso = UniversoProjektoPosedantoStatuso.objects.get(id=kwargs.get('posedanto_statuso_id'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                        except UniversoProjektoPosedantoStatuso.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверный статус владельца проекта Универсо'),'posedanto_statuso_id')
                                    else:
                                        message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_statuso_id')

                                if not message:
                                    if 'posedanto_uzanto_siriuso_uzanto_id' in kwargs:
                                        try:
                                            posedanto_uzanto_siriuso_uzanto = UniversoUzanto.objects.get(
                                                siriuso_uzanto_id=kwargs.get('posedanto_uzanto_siriuso_uzanto_id'), 
                                                publikigo=True
                                            )
                                        except UniversoUzanto.DoesNotExist:
                                            message = _('Неверный владелец/пользователь проекта Универсо')

                                if not message:
                                    if 'posedanto_organizo_uuid' in kwargs:
                                        try:
                                            posedanto_organizo = UniversoOrganizo.objects.get(
                                                uuid=kwargs.get('posedanto_organizo_uuid'),
                                                forigo=False,
                                                arkivo=False,  
                                                publikigo=True
                                            )
                                        except UniversoOrganizo.DoesNotExist:
                                            message = _('Неверная организация владелец проекта Универсо')

                                if not message:
                                    universo_projekto_posedantoj = UniversoProjektoPosedanto.objects.create(
                                        forigo=False,
                                        arkivo=False,
                                        realeco=realeco,
                                        projekto=universo_projekto,
                                        posedanto_uzanto = posedanto_uzanto_siriuso_uzanto,
                                        posedanto_organizo = posedanto_organizo,
                                        tipo = posedanto_tipo,
                                        statuso = posedanto_statuso,
                                        posedanto_objekto = objekto,
                                        publikigo = kwargs.get('publikigo', False),
                                        publikiga_dato = timezone.now()
                                    )

                                    universo_projekto_posedantoj.save()

                                    status = True
                                    message = _('Обе записи созданы')
    
                            if not message:
                                message = _('Запись создана')

                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('kategorio', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('sxablono_id', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('pozicio', False)
                            or kwargs.get('kom_koordinato_x', False) or kwargs.get('kom_koordinato_y', False)
                            or kwargs.get('kom_koordinato_z', False) or kwargs.get('fin_koordinato_x', False)
                            or kwargs.get('fin_koordinato_y', False) or kwargs.get('fin_koordinato_z', False)
                            or kwargs.get('objekto', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_projekto = UniversoProjekto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            # владелец объекта, по которому проект имеет право на удаление/редактирование проекта
                            # проверяем объект из проекта и проверяем объект, указанный в параметрах
                            if 'objekto_uuid' in kwargs: # проверяем объект из параметра
                                try:
                                    objekto = UniversoObjekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                except UniversoObjekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект Универсо'),'objekto_uuid')
                            else: # в параметре нет объекта, проверяем владельца объекта из базы
                                objekto = universo_projekto.objekto
                            if objekto:
                                try:
                                    posedanto_objekto_perm = UniversoObjektoPosedanto.objects.get(objekto=objekto, 
                                                posedanto_uzanto=universo_uzanto,
                                                forigo=False, arkivo=False, publikigo=True)
                                except UniversoObjektoPosedanto.DoesNotExist:
                                    pass

                            if ((not uzanto.has_perm('universo_taskoj.povas_forigi_universo_projektoj') or
                                    not posedanto_objekto_perm)
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('universo_taskoj.povas_shanghi_universo_projektoj') or 
                                    posedanto_objekto_perm):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))
                                    if len(kategorio_id):
                                        kategorio = UniversoProjektoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))
                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категории проектов Универсо'),
                                                        str(dif)
                                            )

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'sxablono_id' in kwargs:
                                    try:
                                        sxablono = UniversoProjektoSxablono.objects.get(id=kwargs.get('sxablono_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoProjektoSxablono.DoesNotExist:
                                        message = _('Неверный тип шаблона Универсо')

                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoRealeco.DoesNotExist:
                                        message = _('Неверный параллельный мир Универсо')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoProjektoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoProjektoTipo.DoesNotExist:
                                        message = _('Неверный тип проектов Универсо')

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = UniversoProjektoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoProjektoStatuso.DoesNotExist:
                                        message = _('Неверный статус проектов Универсо')

                            if not message:

                                universo_projekto.forigo = kwargs.get('forigo', universo_projekto.forigo)
                                universo_projekto.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_projekto.arkivo = kwargs.get('arkivo', universo_projekto.arkivo)
                                universo_projekto.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_projekto.publikigo = kwargs.get('publikigo', universo_projekto.publikigo)
                                universo_projekto.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_projekto.realeco = realeco if kwargs.get('realeco_id', False) else universo_projekto.realeco
                                universo_projekto.sxablono = sxablono if kwargs.get('sxablono_id', False) else universo_projekto.sxablono
                                universo_projekto.statuso = statuso if kwargs.get('statuso_id', False) else universo_projekto.statuso
                                universo_projekto.tipo = tipo if kwargs.get('tipo_id', False) else universo_projekto.tipo
                                universo_projekto.pozicio = kwargs.get('pozicio', universo_projekto.pozicio)
                                universo_projekto.objekto = objekto if kwargs.get('objekto_uuid', False) else universo_projekto.objekto
                                universo_projekto.kom_koordinato_x = kwargs.get('kom_koordinato_x', universo_projekto.kom_koordinato_x)
                                universo_projekto.kom_koordinato_y = kwargs.get('kom_koordinato_y', universo_projekto.kom_koordinato_y)
                                universo_projekto.kom_koordinato_z = kwargs.get('kom_koordinato_z', universo_projekto.kom_koordinato_z)
                                universo_projekto.fin_koordinato_x = kwargs.get('fin_koordinato_x', universo_projekto.fin_koordinato_x)
                                universo_projekto.fin_koordinato_y = kwargs.get('fin_koordinato_y', universo_projekto.fin_koordinato_y)
                                universo_projekto.fin_koordinato_z = kwargs.get('fin_koordinato_z', universo_projekto.fin_koordinato_z)

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_projekto.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_projekto.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        universo_projekto.kategorio.set(kategorio)
                                    else:
                                        universo_projekto.kategorio.clear()

                                universo_projekto.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoProjekto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoProjekto(status=status, message=message, universo_projekto=universo_projekto)


# Модель типов владельцев проектов в Универсо
class RedaktuUniversoProjektoPosedantoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_projekto_posedantoj_tipoj = graphene.Field(UniversoProjektoPosedantoTipoNode, 
        description=_('Созданный/изменённый тип владельцев проектов в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_projekto_posedantoj_tipoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_taskoj.povas_krei_universo_projektoj_posedantoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_projekto_posedantoj_tipoj = UniversoProjektoPosedantoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_projekto_posedantoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_projekto_posedantoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_projekto_posedantoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_projekto_posedantoj_tipoj = UniversoProjektoPosedantoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_taskoj.povas_forigi_universo_projektoj_posedantoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_taskoj.povas_shanghi_universo_projektoj_posedantoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с так����м кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_projekto_posedantoj_tipoj.forigo = kwargs.get('forigo', universo_projekto_posedantoj_tipoj.forigo)
                                universo_projekto_posedantoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_projekto_posedantoj_tipoj.arkivo = kwargs.get('arkivo', universo_projekto_posedantoj_tipoj.arkivo)
                                universo_projekto_posedantoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_projekto_posedantoj_tipoj.publikigo = kwargs.get('publikigo', universo_projekto_posedantoj_tipoj.publikigo)
                                universo_projekto_posedantoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_projekto_posedantoj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_projekto_posedantoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_projekto_posedantoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_projekto_posedantoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoProjektoPosedantoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoProjektoPosedantoTipo(status=status, message=message, universo_projekto_posedantoj_tipoj=universo_projekto_posedantoj_tipoj)


# Модель статусов владельцев проектов в Универсо
class RedaktuUniversoProjektoPosedantoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_projekto_posedantoj_statusoj = graphene.Field(UniversoProjektoPosedantoStatusoNode, 
        description=_('Созданный/изменённый статус владельца проектов в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_projekto_posedantoj_statusoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_taskoj.povas_krei_universo_projektoj_posedantoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_projekto_posedantoj_statusoj = UniversoProjektoPosedantoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_projekto_posedantoj_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_projekto_posedantoj_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_projekto_posedantoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_projekto_posedantoj_statusoj = UniversoProjektoPosedantoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_taskoj.povas_forigi_universo_projektoj_posedantoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_taskoj.povas_shanghi_universo_projektoj_posedantoj_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_projekto_posedantoj_statusoj.forigo = kwargs.get('forigo', universo_projekto_posedantoj_statusoj.forigo)
                                universo_projekto_posedantoj_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_projekto_posedantoj_statusoj.arkivo = kwargs.get('arkivo', universo_projekto_posedantoj_statusoj.arkivo)
                                universo_projekto_posedantoj_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_projekto_posedantoj_statusoj.publikigo = kwargs.get('publikigo', universo_projekto_posedantoj_statusoj.publikigo)
                                universo_projekto_posedantoj_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_projekto_posedantoj_statusoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_projekto_posedantoj_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_projekto_posedantoj_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_projekto_posedantoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoProjektoPosedantoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoProjektoPosedantoStatuso(status=status, message=message, universo_projekto_posedantoj_statusoj=universo_projekto_posedantoj_statusoj)


# Модель владельцев проектов в Универсо
class RedaktuUniversoProjektoPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_projekto_posedantoj = graphene.Field(UniversoProjektoPosedantoNode, 
        description=_('Созданный/изменённый владелец проекта в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир Универсо'))
        projekto_uuid = graphene.String(description=_('Проект Универсо'))
        posedanto_uzanto_siriuso_uzanto_id = graphene.Int(description=_('Владелец проекта Универсо'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец проекта Универсо'))
        tipo_id = graphene.Int(description=_('Тип владельца проекта Универсо'))
        statuso_id = graphene.Int(description=_('Статус владельца проекта Универсо'))
        posedanto_objekto_uuid = graphene.String(description=_('Объект владелец проекта Универсо'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_projekto_posedantoj = None
        realeco = None
        projekto = None
        posedanto_uzanto = None
        posedanto_organizo = None
        tipo = None
        statuso = None
        posedanto_objekto = None
        uzanto = info.context.user
        universo_uzanto = None
        posedanto_perm = None

        if uzanto.is_authenticated:
            with transaction.atomic():
                # находим пользователя Универсо
                try:
                    universo_uzanto = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                except UniversoUzanto.DoesNotExist:
                    message = _('Неверный пользователь Universo')
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    # если пользователь создаёт владельца проекта по объекту, чьим владельцем является, то разрешаем
                    # проверяем соответствие пользователя Универсо и подключенного пользователя
                    if 'posedanto_uzanto_siriuso_uzanto_id' in kwargs:
                        try:
                            posedanto_perm = UniversoUzanto.objects.get(
                                siriuso_uzanto_id=kwargs.get('posedanto_uzanto_siriuso_uzanto_id'), 
                                publikigo=True
                            )
                            if universo_uzanto != posedanto_perm:
                                posedanto_perm = None
                        except UniversoUzanto.DoesNotExist:
                            message = _('Неверный владелец/пользователь проекта Универсо')
                    # теперь проверяем владельца объекта
                    if not message:
                        if 'posedanto_objekto_uuid' in kwargs:
                            try:
                                objekto = UniversoObjekto.objects.get(
                                    uuid=kwargs.get('posedanto_objekto_uuid'), 
                                    forigo=False,
                                    arkivo=False, 
                                    publikigo=True
                                )
                                try:

                                    posedanto_objekto = UniversoObjektoPosedanto.objects.get(objekto=objekto, 
                                                posedanto_uzanto=universo_uzanto,
                                                forigo=False, arkivo=False, publikigo=True)
                                    if posedanto_perm: # если пользователь был указан ранее
                                        if posedanto_perm != posedanto_objekto.posedanto_uzanto: # проверяем его соответствие с владельцем объекта
                                            posedanto_perm = None
                                except UniversoObjektoPosedanto.DoesNotExist:
                                    pass
                            except UniversoObjekto.DoesNotExist:
                                message = '{} "{}"'.format(_('Неверный объект владелец проекта Универсо'), 'posedanto_objekto_uuid')

                    if uzanto.has_perm('universo_taskoj.povas_krei_universo_projektoj_posedantoj') or posedanto_perm:
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом


                        if not message:
                            if 'posedanto_organizo_uuid' in kwargs:
                                try:
                                    posedanto_organizo = UniversoOrganizo.objects.get(
                                        uuid=kwargs.get('posedanto_organizo_uuid'),
                                        forigo=False,
                                        arkivo=False,  
                                        publikigo=True
                                    )
                                except UniversoOrganizo.DoesNotExist:
                                    message = _('Неверная организация владелец проекта Универсо')

                        if not message:
                            if 'projekto_uuid' in kwargs:
                                try:
                                    projekto = UniversoProjekto.objects.get(uuid=kwargs.get('projekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoProjekto.DoesNotExist:
                                    message = _('Неверный проект Универсо')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'projekto_uuid')

                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoRealeco.DoesNotExist:
                                    message = _('Неверный параллельный мир Универсо')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoProjektoPosedantoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoProjektoPosedantoTipo.DoesNotExist:
                                    message = _('Неверный тип владельца проектов Универсо')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = UniversoProjektoPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoProjektoPosedantoStatuso.DoesNotExist:
                                    message = _('Неверный статус владельца проектов Универсо')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')

                        if not message:
                            if 'posedanto_objekto_uuid' in kwargs:
                                try:
                                    posedanto_objekto = UniversoObjekto.objects.get(
                                        uuid=kwargs.get('posedanto_objekto_uuid'), 
                                        forigo=False,
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except UniversoObjekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект владелец проекта Универсо'), 'posedanto_objekto_uuid')

                        if 'posedanto_uzanto_siriuso_uzanto_id' in kwargs:
                            try:
                                posedanto_uzanto = UniversoUzanto.objects.get(
                                    siriuso_uzanto_id=kwargs.get('posedanto_uzanto_siriuso_uzanto_id'), 
                                    publikigo=True
                                )
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный владелец/пользователь проекта Универсо')

                        if not message:
                            universo_projekto_posedantoj = UniversoProjektoPosedanto.objects.create(
                                forigo = False,
                                arkivo = False,
                                realeco = realeco,
                                projekto = projekto,
                                posedanto_uzanto = posedanto_uzanto,
                                posedanto_organizo = posedanto_organizo,
                                tipo = tipo,
                                statuso = statuso,
                                posedanto_objekto = posedanto_objekto,
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_projekto_posedantoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_projekto_posedantoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_projekto_posedantoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('posedanto_uzanto_siriuso_uzanto_id', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('posedanto_organizo_uuid', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('projekto_uuid', False)
                            or kwargs.get('posedanto_objekto_uuid', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_projekto_posedantoj = UniversoProjektoPosedanto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            # проверяем на владельца проекта или владельца объекта, по которому проект
                            if universo_projekto_posedantoj.posedanto_uzanto:
                                if universo_projekto_posedantoj.posedanto_uzanto != universo_uzanto: # пользователи не совпадают
                                    posedanto_perm = None
                                else:
                                    posedanto_perm = universo_uzanto
                            if posedanto_perm: # продолжаем проверку по объекту
                                try:
                                    posedanto_objekto2 = UniversoObjektoPosedanto.objects.get(
                                            objekto=universo_projekto_posedantoj.posedanto_objekto, 
                                            posedanto_uzanto=universo_uzanto,
                                            forigo=False, arkivo=False, publikigo=True)
                                    if posedanto_objekto2: # если нашли объект
                                        if universo_uzanto != posedanto_objekto2.posedanto_uzanto: # проверяем его соответствие с владельцем объекта
                                            posedanto_perm = None
                                except UniversoObjektoPosedanto.DoesNotExist:
                                    pass

                            if (not (uzanto.has_perm('universo_taskoj.povas_forigi_universo_projektoj_posedantoj')
                                    or posedanto_perm)
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('universo_taskoj.povas_shanghi_universo_projektoj_posedantoj')
                                    or posedanto_perm):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_uzanto_siriuso_uzanto_id' in kwargs:
                                    try:
                                        posedanto_uzanto = UniversoUzanto.objects.get(
                                            siriuso_uzanto_id=kwargs.get('posedanto_uzanto_siriuso_uzanto_id'), 
                                            publikigo=True
                                        )
                                    except UniversoUzanto.DoesNotExist:
                                        message = _('Неверный владелец/пользователь проекта Универсо')

                            if not message:
                                if 'posedanto_organizo_uuid' in kwargs:
                                    try:
                                        posedanto_organizo = UniversoOrganizo.objects.get(
                                            uuid=kwargs.get('posedanto_organizo_uuid'),
                                            forigo=False,
                                            arkivo=False,  
                                            publikigo=True
                                        )
                                    except UniversoOrganizo.DoesNotExist:
                                        message = _('Неверная организация владелец проекта Универсо')

                            if not message:
                                if 'projekto_uuid' in kwargs:
                                    try:
                                        projekto = UniversoProjekto.objects.get(uuid=kwargs.get('projekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoProjekto.DoesNotExist:
                                        message = _('Неверный проект Универсо')

                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoRealeco.DoesNotExist:
                                        message = _('Неверный параллельный мир Универсо')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoProjektoPosedantoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoProjektoPosedantoTipo.DoesNotExist:
                                        message = _('Неверный тип владельца проектов Универсо')

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = UniversoProjektoPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoProjektoPosedantoStatuso.DoesNotExist:
                                        message = _('Неверный статус владельца проектов Универсо')

                            if not message:
                                if 'posedanto_objekto_uuid' in kwargs:
                                    try:
                                        posedanto_objekto = UniversoObjekto.objects.get(
                                            uuid=kwargs.get('posedanto_objekto_uuid'), 
                                            forigo=False,
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except UniversoObjekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект владелец проекта Универсо'), 'posedanto_objekto_uuid')

                            if not message:

                                universo_projekto_posedantoj.forigo = kwargs.get('forigo', universo_projekto_posedantoj.forigo)
                                universo_projekto_posedantoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_projekto_posedantoj.arkivo = kwargs.get('arkivo', universo_projekto_posedantoj.arkivo)
                                universo_projekto_posedantoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_projekto_posedantoj.publikigo = kwargs.get('publikigo', universo_projekto_posedantoj.publikigo)
                                universo_projekto_posedantoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_projekto_posedantoj.realeco = realeco if kwargs.get('realeco_id', False) else universo_projekto_posedantoj.realeco
                                universo_projekto_posedantoj.projekto = realeco if kwargs.get('projekto_uuid', False) else universo_projekto_posedantoj.projekto
                                universo_projekto_posedantoj.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_siriuso_uzanto_id', False) else universo_projekto_posedantoj.posedanto_uzanto
                                universo_projekto_posedantoj.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) else universo_projekto_posedantoj.posedanto_organizo
                                universo_projekto_posedantoj.statuso = statuso if kwargs.get('statuso_id', False) else universo_projekto_posedantoj.statuso
                                universo_projekto_posedantoj.tipo = tipo if kwargs.get('tipo_id', False) else universo_projekto_posedantoj.tipo
                                universo_projekto_posedantoj.posedanto_objekto = posedanto_objekto if kwargs.get('posedanto_objekto_uuid', False) else universo_projekto_posedantoj.posedanto_objekto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_projekto_posedantoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_projekto_posedantoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_projekto_posedantoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoProjektoPosedanto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoProjektoPosedanto(status=status, message=message, universo_projekto_posedantoj=universo_projekto_posedantoj)


# Модель категорий задач в Универсо
class RedaktuUniversoTaskoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_taskoj_kategorioj = graphene.Field(UniversoTaskoKategorioNode,
        description=_('Созданная/изменённая категория задач в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_taskoj_kategorioj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_taskoj.povas_krei_universo_taskoj_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_taskoj_kategorioj = UniversoTaskoKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_taskoj_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_taskoj_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_taskoj_kategorioj.realeco.set(realeco)
                                else:
                                    universo_taskoj_kategorioj.realeco.clear()

                            universo_taskoj_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_taskoj_kategorioj = UniversoTaskoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_taskoj.povas_forigi_universo_taskoj_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_taskoj.povas_shanghi_universo_taskoj_kategorioj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_taskoj_kategorioj.forigo = kwargs.get('forigo', universo_taskoj_kategorioj.forigo)
                                universo_taskoj_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_taskoj_kategorioj.arkivo = kwargs.get('arkivo', universo_taskoj_kategorioj.arkivo)
                                universo_taskoj_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_taskoj_kategorioj.publikigo = kwargs.get('publikigo', universo_taskoj_kategorioj.publikigo)
                                universo_taskoj_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_taskoj_kategorioj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_taskoj_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_taskoj_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_taskoj_kategorioj.realeco.set(realeco)
                                    else:
                                        universo_taskoj_kategorioj.realeco.clear()

                                universo_taskoj_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoTaskoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoTaskoKategorio(status=status, message=message, universo_taskoj_kategorioj=universo_taskoj_kategorioj)


# Модель типов задач в Универсо
class RedaktuUniversoTaskoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_taskoj_tipoj = graphene.Field(UniversoTaskoTipoNode, 
        description=_('Созданный/изменённый тип задач в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_taskoj_tipoj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_taskoj.povas_krei_universo_taskoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_taskoj_tipoj = UniversoTaskoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_taskoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_taskoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_taskoj_tipoj.realeco.set(realeco)
                                else:
                                    universo_taskoj_tipoj.realeco.clear()

                            universo_taskoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_taskoj_tipoj = UniversoTaskoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_taskoj.povas_forigi_universo_taskoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_taskoj.povas_shanghi_universo_taskoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_taskoj_tipoj.forigo = kwargs.get('forigo', universo_taskoj_tipoj.forigo)
                                universo_taskoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_taskoj_tipoj.arkivo = kwargs.get('arkivo', universo_taskoj_tipoj.arkivo)
                                universo_taskoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_taskoj_tipoj.publikigo = kwargs.get('publikigo', universo_taskoj_tipoj.publikigo)
                                universo_taskoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_taskoj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_taskoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_taskoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_taskoj_tipoj.realeco.set(realeco)
                                    else:
                                        universo_taskoj_tipoj.realeco.clear()

                                universo_taskoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoTaskoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoTaskoTipo(status=status, message=message, universo_taskoj_tipoj=universo_taskoj_tipoj)


# Модель шаблонов задач в Универсо
class RedaktuUniversoTaskoSxablono(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_taskoj_sxablonoj = graphene.Field(UniversoTaskoSxablonoNode, 
        description=_('Созданный/изменённый шаблон задач в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий задач Универсо'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))
        tipo_id = graphene.Int(description=_('Тип задач Универсо'))
        projekto_uuid = graphene.String(description=_('Шаблон проекта Универсо в который входит задача'))
        pozicio = graphene.Int(description=_('Позиция в списке'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_taskoj_sxablonoj = None
        autoro = None
        tipo = None
        kategorio = UniversoTaskoKategorio.objects.none()
        realeco = UniversoRealeco.objects.none()
        projekto = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_taskoj.povas_krei_universo_taskoj_sxablonoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'kategorio' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                            elif not len(kwargs.get('kategorio')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'kategorio')
                            else:
                                kategorio_id = set(kwargs.get('kategorio'))

                                if len(kategorio_id):
                                    kategorio = UniversoTaskoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие категории проектов Универсо'),
                                                    str(dif)
                                        )

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoTaskoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoTaskoTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип проектов Универсо'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип проектов Универсо'),'tipo_id')

                        if not message:
                            if 'projekto_uuid' in kwargs:
                                try:
                                    projekto = UniversoProjektoSxablono.objects.get(id=kwargs.get('projekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoProjektoSxablono.DoesNotExist:
                                    message = '{}: {}'.format(
                                        _('Неверный шаблон проекта Универсо в который входит задача Универсо'),
                                        'projekto_uuid')
                            else:
                                message = '{}: {}'.format(
                                    _('Не указан шаблон проекта Универсо в который входит задача Универсо'),
                                    'projekto_uuid')

                        if not message:
                            universo_taskoj_sxablonoj = UniversoTaskoSxablono.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                tipo = tipo,
                                projekto = projekto,
                                pozicio = kwargs.get('pozicio', None),
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_taskoj_sxablonoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_taskoj_sxablonoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_taskoj_sxablonoj.realeco.set(realeco)
                                else:
                                    universo_taskoj_sxablonoj.realeco.clear()
                            if 'kategorio' in kwargs:
                                if kategorio:
                                    universo_taskoj_sxablonoj.kategorio.set(kategorio)
                                else:
                                    universo_taskoj_sxablonoj.kategorio.clear()

                            universo_taskoj_sxablonoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False) or kwargs.get('kategorio', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('projekto_uuid', False)
                            or kwargs.get('pozicio', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_taskoj_sxablonoj = UniversoTaskoSxablono.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_taskoj.povas_forigi_universo_taskoj_sxablonoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_taskoj.povas_shanghi_universo_taskoj_sxablonoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))
                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))
                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))
                                    if len(kategorio_id):
                                        kategorio = UniversoTaskoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))
                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категории проектов Универсо'),
                                                        str(dif)
                                            )

                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoTaskoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoTaskoTipo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный тип проектов Универсо'),'tipo_id')

                            if not message:
                                if 'projekto_uuid' in kwargs:
                                    try:
                                        projekto = UniversoProjektoSxablono.objects.get(id=kwargs.get('projekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoProjektoSxablono.DoesNotExist:
                                        message = '{}: {}'.format(
                                            _('Неверный шаблон проекта Универсо в который входит задача Универсо'),
                                            'projekto_uuid')

                            if not message:

                                universo_taskoj_sxablonoj.forigo = kwargs.get('forigo', universo_taskoj_sxablonoj.forigo)
                                universo_taskoj_sxablonoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_taskoj_sxablonoj.arkivo = kwargs.get('arkivo', universo_taskoj_sxablonoj.arkivo)
                                universo_taskoj_sxablonoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_taskoj_sxablonoj.publikigo = kwargs.get('publikigo', universo_taskoj_sxablonoj.publikigo)
                                universo_taskoj_sxablonoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_taskoj_sxablonoj.autoro = autoro
                                universo_taskoj_sxablonoj.tipo = tipo if kwargs.get('tipo_id', False) else universo_taskoj_sxablonoj.tipo
                                universo_taskoj_sxablonoj.projekto = projekto if kwargs.get('projekto_uuid', False) else universo_taskoj_sxablonoj.projekto
                                universo_taskoj_sxablonoj.pozicio = kwargs.get('pozicio', universo_taskoj_sxablonoj.pozicio)

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_taskoj_sxablonoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_taskoj_sxablonoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_taskoj_sxablonoj.realeco.set(realeco)
                                    else:
                                        universo_taskoj_sxablonoj.realeco.clear()
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        universo_taskoj_sxablonoj.kategorio.set(kategorio)
                                    else:
                                        universo_taskoj_sxablonoj.kategorio.clear()

                                universo_taskoj_sxablonoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoTaskoSxablono.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoTaskoSxablono(status=status, message=message, universo_taskoj_sxablonoj=universo_taskoj_sxablonoj)


# Модель статусов задач в Универсо
class RedaktuUniversoTaskoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_taskoj_statusoj = graphene.Field(UniversoTaskoStatusoNode, 
        description=_('Созданный/изменённый статус задач в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_taskoj_statusoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_taskoj.povas_krei_universo_taskoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_taskoj_statusoj = UniversoTaskoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_taskoj_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_taskoj_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_taskoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_taskoj_statusoj = UniversoTaskoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_taskoj.povas_forigi_universo_taskoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_taskoj.povas_shanghi_universo_taskoj_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_taskoj_statusoj.forigo = kwargs.get('forigo', universo_taskoj_statusoj.forigo)
                                universo_taskoj_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_taskoj_statusoj.arkivo = kwargs.get('arkivo', universo_taskoj_statusoj.arkivo)
                                universo_taskoj_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_taskoj_statusoj.publikigo = kwargs.get('publikigo', universo_taskoj_statusoj.publikigo)
                                universo_taskoj_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_taskoj_statusoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_taskoj_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_taskoj_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_taskoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoTaskoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoTaskoStatuso(status=status, message=message, universo_taskoj_statusoj=universo_taskoj_statusoj)


# Модель задач в Универсо
class RedaktuUniversoTasko(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_taskoj = graphene.Field(UniversoTaskoNode, 
        description=_('Созданный/изменённый задача в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир Универсо'))
        projekto_uuid = graphene.String(description=_('Проект Универсо в который входит задача Универсо'))
        objekto_uuid = graphene.String(description=_('Объект владелец задачи Универсо'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий задач Универсо'))
        sxablono_id = graphene.Int(description=_('Шаблон задач Универсо'))
        tipo_id = graphene.Int(description=_('Тип задач Универсо'))
        statuso_id = graphene.Int(description=_('Статус задач Универсо'))
        kom_koordinato_x = graphene.Float(description=_('Начальные координаты выполнения задачи по оси X в кубе Универсо'))
        kom_koordinato_y = graphene.Float(description=_('Начальные координаты выполнения задачи по оси Y в кубе Универсо'))
        kom_koordinato_z = graphene.Float(description=_('Начальные координаты выполнения задачи по оси Z в кубе Универсо'))
        fin_koordinato_x = graphene.Float(description=_('Конечные координаты выполнения задачи по оси X в кубе Универсо'))
        fin_koordinato_y = graphene.Float(description=_('Конечные координаты выполнения задачи по оси Y в кубе Универсо'))
        fin_koordinato_z = graphene.Float(description=_('Конечные координаты выполнения задачи по оси z в кубе Универсо'))
        pozicio = graphene.Int(description=_('Позиция в списке'))

        # параметры для создания владельца
        posedanto_uzanto_siriuso_uzanto_id = graphene.Int(description=_('Владелец задачи Универсо'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец задачи Универсо'))
        posedanto_tipo_id = graphene.Int(description=_('Тип владельца задачи Универсо'))
        posedanto_statuso_id = graphene.Int(description=_('Статус владельца задачи Универсо'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_taskoj = None
        realeco = None
        projekto = None
        objekto = None
        kategorio = UniversoTaskoKategorio.objects.none()
        sxablono = None
        tipo = None
        statuso = None
        posedanto_uzanto_siriuso_uzanto = None
        posedanto_organizo = None
        posedanto_tipo = None
        posedanto_statuso = None
        uzanto = info.context.user
        posedanto_perm = None # если пользователь владелец объекта по проекту
        universo_uzanto = None

        if uzanto.is_authenticated:
            with transaction.atomic():
                # находим пользователя Универсо
                try:
                    universo_uzanto = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                except UniversoUzanto.DoesNotExist:
                    message = _('Неверный пользователь Universo')
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    # если пользователь владелец объекта, по которому создаётся проект, то можно создавать
                    # проверяем по полю posedanto
                    if 'objekto_uuid' in kwargs:
                        try:
                            objekto = UniversoObjekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                            try:
                                posedanto_perm = UniversoObjektoPosedanto.objects.get(objekto=objekto, 
                                            posedanto_uzanto=universo_uzanto,
                                            forigo=False, arkivo=False, publikigo=True)
                                if (kwargs.get('posedanto_tipo_id', False)):
                                    # если сразу задаётся владелец, то проверяем и владельца
                                    if 'posedanto_uzanto_siriuso_uzanto_id' in kwargs:
                                        try:
                                            posedanto_uzanto_siriuso_uzanto = UniversoUzanto.objects.get(
                                                siriuso_uzanto_id=kwargs.get('posedanto_uzanto_siriuso_uzanto_id'), 
                                                publikigo=True
                                            )
                                            # Если владельцы разные, то права на создание проекта, как владельца объекта снимаются
                                            if posedanto_perm.posedanto_uzanto != posedanto_uzanto_siriuso_uzanto:
                                                posedanto_perm = None
                                        except UniversoUzanto.DoesNotExist:
                                            message = _('Неверный владелец/пользователь проекта Универсо')
                            except UniversoObjektoPosedanto.DoesNotExist:
                                pass
                        except UniversoObjekto.DoesNotExist:
                            message = '{} "{}"'.format(_('Неверный объект владелец проекта Универсо'),'objekto_uuid')
                    if uzanto.has_perm('universo_taskoj.povas_krei_universo_taskoj') or posedanto_perm:
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            if 'kategorio' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                            elif not len(kwargs.get('kategorio')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'kategorio')
                            else:
                                kategorio_id = set(kwargs.get('kategorio'))

                                if len(kategorio_id):
                                    kategorio = UniversoTaskoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие категории задач Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoRealeco.DoesNotExist:
                                    message = _('Неверный параллельный мир Универсо')

                        if not message:
                            if 'projekto_uuid' in kwargs:
                                try:
                                    projekto = UniversoProjekto.objects.get(uuid=kwargs.get('projekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoProjekto.DoesNotExist:
                                    message = _('Неверный проект Универсо в который входит задача')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'projekto_uuid')

                        if not message:
                            if 'objekto_uuid' in kwargs:
                                try:
                                    objekto = UniversoObjekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoObjekto.DoesNotExist:
                                    message = _('Неверный объект Универсо')

                        if not message:
                            if 'sxablono_id' in kwargs:
                                try:
                                    sxablono = UniversoTaskoSxablono.objects.get(id=kwargs.get('sxablono_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoTaskoSxablono.DoesNotExist:
                                    message = _('Неверный шаблон задачи Универсо')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoTaskoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoTaskoTipo.DoesNotExist:
                                    message = _('Неверный тип задачи Универсо')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = UniversoTaskoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoTaskoStatuso.DoesNotExist:
                                    message = _('Неверный статус задач Универсо')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')

                        if not message:
                            universo_taskoj = UniversoTasko.objects.create(
                                forigo=False,
                                arkivo=False,
                                realeco = realeco,
                                projekto = projekto,
                                objekto = objekto,
                                tipo = tipo,
                                sxablono = sxablono,
                                statuso = statuso,
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now(),
                                kom_koordinato_x = kwargs.get('kom_koordinato_x', None),
                                kom_koordinato_y = kwargs.get('kom_koordinato_y', None),
                                kom_koordinato_z = kwargs.get('kom_koordinato_z', None),
                                fin_koordinato_x = kwargs.get('fin_koordinato_x', None),
                                fin_koordinato_y = kwargs.get('fin_koordinato_y', None),
                                fin_koordinato_z = kwargs.get('fin_koordinato_z', None),
                                pozicio = kwargs.get('pozicio', None),
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_taskoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_taskoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'kategorio' in kwargs:
                                if kategorio:
                                    universo_taskoj.kategorio.set(kategorio)
                                else:
                                    universo_taskoj.kategorio.clear()

                            universo_taskoj.save()

                            status = True

                            # далее блок добавления владельца
                            if (kwargs.get('posedanto_tipo_id', False)):
                                if not message:
                                    try:
                                        posedanto_tipo = UniversoTaskoPosedantoTipo.objects.get(id=kwargs.get('posedanto_tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoTaskoPosedantoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип владельца задачи Универсо'),'posedanto_tipo_id')

                                if not message:
                                    if 'posedanto_statuso_id' in kwargs:
                                        try:
                                            posedanto_statuso = UniversoTaskoPosedantoStatuso.objects.get(id=kwargs.get('posedanto_statuso_id'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                        except UniversoTaskoPosedantoStatuso.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверный статус владельца задач Универсо'),'posedanto_statuso_id')
                                    else:
                                        message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_statuso_id')

                                if not message:
                                    if 'posedanto_uzanto_siriuso_uzanto_id' in kwargs:
                                        try:
                                            posedanto_uzanto_siriuso_uzanto = UniversoUzanto.objects.get(
                                                siriuso_uzanto_id=kwargs.get('posedanto_uzanto_siriuso_uzanto_id'), 
                                                publikigo=True
                                            )
                                        except UniversoUzanto.DoesNotExist:
                                            message = _('Неверный владелец/пользователь задачи Универсо')

                                if not message:
                                    if 'posedanto_organizo_uuid' in kwargs:
                                        try:
                                            posedanto_organizo = UniversoOrganizo.objects.get(
                                                uuid=kwargs.get('posedanto_organizo_uuid'),
                                                forigo=False,
                                                arkivo=False,  
                                                publikigo=True
                                            )
                                        except UniversoOrganizo.DoesNotExist:
                                            message = _('Неверная организация владелец задачи Универсо')

                                if not message:
                                    universo_tasko_posedantoj = UniversoTaskoPosedanto.objects.create(
                                        forigo = False,
                                        arkivo = False,
                                        realeco = realeco,
                                        tasko = universo_taskoj,
                                        posedanto_uzanto = posedanto_uzanto_siriuso_uzanto,
                                        posedanto_organizo = posedanto_organizo,
                                        tipo = posedanto_tipo,
                                        statuso = posedanto_statuso,
                                        posedanto_objekto = objekto,
                                        publikigo = kwargs.get('publikigo', False),
                                        publikiga_dato = timezone.now()
                                    )

                                    universo_tasko_posedantoj.save()

                                    status = True
                                    message = _('Обе записи созданы')

                            if not message:
                                message = _('Запись создана')

                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('kategorio', False)
                            or kwargs.get('projekto_uuid', False) or kwargs.get('objekto_uuid', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('sxablono_id', False)
                            or kwargs.get('kom_koordinato_x', False) or kwargs.get('kom_koordinato_y', False)
                            or kwargs.get('kom_koordinato_z', False) or kwargs.get('fin_koordinato_x', False)
                            or kwargs.get('fin_koordinato_y', False) or kwargs.get('fin_koordinato_z', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('pozicio', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_taskoj = UniversoTasko.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            # владелец объекта, по которому проект имеет право на удаление/редактирование проекта
                            # проверяем объект из проекта и проверяем объект, указанный в параметрах
                            if 'objekto_uuid' in kwargs: # проверяем объект из параметра
                                try:
                                    objekto = UniversoObjekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                except UniversoObjekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект Универсо'),'objekto_uuid')
                            else: # в параметре нет объекта, проверяем владельца объекта из базы
                                objekto = universo_taskoj.objekto
                            if objekto:
                                try:
                                    posedanto_perm = UniversoObjektoPosedanto.objects.get(objekto=objekto, 
                                                posedanto_uzanto=universo_uzanto,
                                                forigo=False, arkivo=False, publikigo=True)
                                except UniversoObjektoPosedanto.DoesNotExist:
                                    pass

                            if (not (uzanto.has_perm('universo_taskoj.povas_forigi_universo_taskoj') or 
                                    posedanto_perm)
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('universo_taskoj.povas_shanghi_universo_taskoj') or 
                                    posedanto_perm):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))
                                    if len(kategorio_id):
                                        kategorio = UniversoTaskoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))
                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категории проектов Универсо'),
                                                        str(dif)
                                            )

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'projekto_uuid' in kwargs:
                                    try:
                                        projekto = UniversoProjekto.objects.get(uuid=kwargs.get('projekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoProjekto.DoesNotExist:
                                        message = _('Неверный проект Универсо в который входит задача')

                            if not message:
                                if 'objekto_uuid' in kwargs:
                                    try:
                                        objekto = UniversoObjekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoObjekto.DoesNotExist:
                                        message = _('Неверный объект Универсо')

                            if not message:
                                if 'sxablono_id' in kwargs:
                                    try:
                                        sxablono = UniversoTaskoSxablono.objects.get(id=kwargs.get('sxablono_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoTaskoSxablono.DoesNotExist:
                                        message = _('Неверный тип шаблона Универсо')

                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoRealeco.DoesNotExist:
                                        message = _('Неверный параллельный мир Универсо')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoTaskoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoTaskoTipo.DoesNotExist:
                                        message = _('Неверный тип проектов Универсо')

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = UniversoTaskoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoTaskoStatuso.DoesNotExist:
                                        message = _('Неверный статус проектов Универсо')

                            if not message:

                                universo_taskoj.forigo = kwargs.get('forigo', universo_taskoj.forigo)
                                universo_taskoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_taskoj.arkivo = kwargs.get('arkivo', universo_taskoj.arkivo)
                                universo_taskoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_taskoj.publikigo = kwargs.get('publikigo', universo_taskoj.publikigo)
                                universo_taskoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_taskoj.realeco = realeco if kwargs.get('realeco_id', False) else universo_taskoj.realeco
                                universo_taskoj.projekto = projekto if kwargs.get('projekto_uuid', False) else universo_taskoj.projekto
                                universo_taskoj.objekto = objekto if kwargs.get('objekto_uuid', False) else universo_taskoj.objekto
                                universo_taskoj.sxablono = sxablono if kwargs.get('sxablono_id', False) else universo_taskoj.sxablono
                                universo_taskoj.statuso = statuso if kwargs.get('statuso_id', False) else universo_taskoj.statuso
                                universo_taskoj.tipo = tipo if kwargs.get('tipo_id', False) else universo_taskoj.tipo
                                universo_taskoj.kom_koordinato_x = kwargs.get('kom_koordinato_x', universo_taskoj.kom_koordinato_x)
                                universo_taskoj.kom_koordinato_y = kwargs.get('kom_koordinato_y', universo_taskoj.kom_koordinato_y)
                                universo_taskoj.kom_koordinato_z = kwargs.get('kom_koordinato_z', universo_taskoj.kom_koordinato_z)
                                universo_taskoj.fin_koordinato_x = kwargs.get('fin_koordinato_x', universo_taskoj.fin_koordinato_x)
                                universo_taskoj.fin_koordinato_y = kwargs.get('fin_koordinato_y', universo_taskoj.fin_koordinato_y)
                                universo_taskoj.fin_koordinato_z = kwargs.get('fin_koordinato_z', universo_taskoj.fin_koordinato_z)
                                universo_taskoj.pozicio = kwargs.get('pozicio', universo_taskoj.pozicio)

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_taskoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_taskoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        universo_taskoj.kategorio.set(kategorio)
                                    else:
                                        universo_taskoj.kategorio.clear()

                                universo_taskoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoTasko.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoTasko(status=status, message=message, universo_taskoj=universo_taskoj)


# Модель типов владельцев задач в Универсо
class RedaktuUniversoTaskoPosedantoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_taskoj_posedantoj_tipoj = graphene.Field(UniversoTaskoPosedantoTipoNode, 
        description=_('Созданный/изменённый тип владельцев задач в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_taskoj_posedantoj_tipoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_taskoj.povas_krei_universo_taskoj_posedantoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_taskoj_posedantoj_tipoj = UniversoTaskoPosedantoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_taskoj_posedantoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_taskoj_posedantoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_taskoj_posedantoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_taskoj_posedantoj_tipoj = UniversoTaskoPosedantoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_taskoj.povas_forigi_universo_taskoj_posedantoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_taskoj.povas_shanghi_universo_taskoj_posedantoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с так����м кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_taskoj_posedantoj_tipoj.forigo = kwargs.get('forigo', universo_taskoj_posedantoj_tipoj.forigo)
                                universo_taskoj_posedantoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_taskoj_posedantoj_tipoj.arkivo = kwargs.get('arkivo', universo_taskoj_posedantoj_tipoj.arkivo)
                                universo_taskoj_posedantoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_taskoj_posedantoj_tipoj.publikigo = kwargs.get('publikigo', universo_taskoj_posedantoj_tipoj.publikigo)
                                universo_taskoj_posedantoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_taskoj_posedantoj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_taskoj_posedantoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_taskoj_posedantoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_taskoj_posedantoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoTaskoPosedantoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoTaskoPosedantoTipo(status=status, message=message, universo_taskoj_posedantoj_tipoj=universo_taskoj_posedantoj_tipoj)


# Модель статусов владельцев задач в Универсо
class RedaktuUniversoTaskoPosedantoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_tasko_posedantoj_statusoj = graphene.Field(UniversoTaskoPosedantoStatusoNode, 
        description=_('Созданный/изменённый статус владельца задач в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_tasko_posedantoj_statusoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_taskoj.povas_krei_universo_taskoj_posedantoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_tasko_posedantoj_statusoj = UniversoTaskoPosedantoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_tasko_posedantoj_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_tasko_posedantoj_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_tasko_posedantoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_tasko_posedantoj_statusoj = UniversoTaskoPosedantoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_taskoj.povas_forigi_universo_taskoj_posedantoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_taskoj.povas_shanghi_universo_taskoj_posedantoj_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_tasko_posedantoj_statusoj.forigo = kwargs.get('forigo', universo_tasko_posedantoj_statusoj.forigo)
                                universo_tasko_posedantoj_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_tasko_posedantoj_statusoj.arkivo = kwargs.get('arkivo', universo_tasko_posedantoj_statusoj.arkivo)
                                universo_tasko_posedantoj_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_tasko_posedantoj_statusoj.publikigo = kwargs.get('publikigo', universo_tasko_posedantoj_statusoj.publikigo)
                                universo_tasko_posedantoj_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_tasko_posedantoj_statusoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_tasko_posedantoj_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_tasko_posedantoj_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_tasko_posedantoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoTaskoPosedantoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoTaskoPosedantoStatuso(status=status, message=message, universo_tasko_posedantoj_statusoj=universo_tasko_posedantoj_statusoj)


# Модель владельцев задач в Универсо
class RedaktuUniversoTaskoPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_tasko_posedantoj = graphene.Field(UniversoTaskoPosedantoNode, 
        description=_('Созданный/изменённый владелец задач в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир Универсо'))
        tasko_uuid = graphene.String(description=_('Проект Универсо'))
        posedanto_uzanto_siriuso_uzanto_id = graphene.Int(description=_('Владелец задачи Универсо'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец задачи Универсо'))
        tipo_id = graphene.Int(description=_('Тип владельца задачи Универсо'))
        statuso_id = graphene.Int(description=_('Статус владельца задачи Универсо'))
        posedanto_objekto_uuid = graphene.String(description=_('Объект владелец задачи Универсо'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_tasko_posedantoj = None
        realeco = None
        tasko = None
        posedanto_uzanto = None
        posedanto_organizo = None
        tipo = None
        statuso = None
        posedanto_objekto = None
        uzanto = info.context.user
        universo_uzanto = None # пользователь Универсо
        posedanto_perm = None # для проверки владельца объекта/задачи

        if uzanto.is_authenticated:
            with transaction.atomic():
                # находим пользователя Универсо
                try:
                    universo_uzanto = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                except UniversoUzanto.DoesNotExist:
                    message = _('Неверный пользователь Universo')
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    # если пользователь создаёт владельца проекта по объекту, чьим владельцем является, то разрешаем
                    # проверяем соответствие пользователя Универсо и подключенного пользователя
                    if 'posedanto_uzanto_siriuso_uzanto_id' in kwargs:
                        try:
                            posedanto_uzanto2 = UniversoUzanto.objects.get(
                                siriuso_uzanto_id=kwargs.get('posedanto_uzanto_siriuso_uzanto_id'), 
                                publikigo=True
                            )
                            if universo_uzanto != posedanto_uzanto2:
                                posedanto_perm = None
                            else:
                                posedanto_perm = universo_uzanto
                        except UniversoUzanto.DoesNotExist:
                            message = _('Неверный владелец/пользователь проекта Универсо')
                    # теперь проверяем владельца объекта
                    if not message:
                        if 'posedanto_objekto_uuid' in kwargs:
                            try:
                                objekto = UniversoObjekto.objects.get(
                                    uuid=kwargs.get('posedanto_objekto_uuid'), 
                                    forigo=False,
                                    arkivo=False, 
                                    publikigo=True
                                )
                                try:
                                    posedanto_objekto2 = UniversoObjektoPosedanto.objects.get(objekto=objekto, 
                                                posedanto_uzanto=universo_uzanto,
                                                forigo=False, arkivo=False, publikigo=True)
                                    if posedanto_perm: # если пользователь был указан ранее
                                        if posedanto_perm != posedanto_objekto2.posedanto_uzanto: # проверяем его соответствие с владельцем объекта
                                            posedanto_perm = None
                                except UniversoObjektoPosedanto.DoesNotExist:
                                    pass
                            except UniversoObjekto.DoesNotExist:
                                message = '{} "{}"'.format(_('Неверный объект владелец проекта Универсо'), 'posedanto_objekto_uuid')

                    if uzanto.has_perm('universo_taskoj.povas_krei_universo_taskoj_posedantoj') or posedanto_perm:
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_uzanto_siriuso_uzanto_id' in kwargs:
                                try:
                                    posedanto_uzanto = UniversoUzanto.objects.get(
                                        siriuso_uzanto_id=kwargs.get('posedanto_uzanto_siriuso_uzanto_id'), 
                                        publikigo=True
                                    )
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный владелец/пользователь задачи Универсо')

                        if not message:
                            if 'posedanto_organizo_uuid' in kwargs:
                                try:
                                    posedanto_organizo = UniversoOrganizo.objects.get(
                                        uuid=kwargs.get('posedanto_organizo_uuid'),
                                        forigo=False,
                                        arkivo=False,  
                                        publikigo=True
                                    )
                                except UniversoOrganizo.DoesNotExist:
                                    message = _('Неверная организация владелец задачи Универсо')

                        if not message:
                            if 'tasko_uuid' in kwargs:
                                try:
                                    tasko = UniversoTasko.objects.get(uuid=kwargs.get('tasko_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoTasko.DoesNotExist:
                                    message = _('Неверный проект Универсо')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tasko_uuid')

                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoRealeco.DoesNotExist:
                                    message = _('Неверный параллельный мир Универсо')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoTaskoPosedantoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoTaskoPosedantoTipo.DoesNotExist:
                                    message = _('Неверный тип владельца задачи Универсо')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = UniversoTaskoPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoTaskoPosedantoStatuso.DoesNotExist:
                                    message = _('Неверный статус владельца задачи Универсо')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')

                        if not message:
                            if 'posedanto_objekto_uuid' in kwargs:
                                try:
                                    posedanto_objekto = UniversoObjekto.objects.get(
                                        uuid=kwargs.get('posedanto_objekto_uuid'), 
                                        forigo=False,
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except UniversoObjekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект владелец задачи Универсо'),'posedanto_objekto_uuid')

                        if not message:
                            universo_tasko_posedantoj = UniversoTaskoPosedanto.objects.create(
                                forigo = False,
                                arkivo = False,
                                realeco = realeco,
                                tasko = tasko,
                                posedanto_uzanto = posedanto_uzanto,
                                posedanto_organizo = posedanto_organizo,
                                tipo = tipo,
                                statuso = statuso,
                                posedanto_objekto = posedanto_objekto,
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now()
                            )

                            universo_tasko_posedantoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('posedanto_uzanto_siriuso_uzanto_id', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('posedanto_organizo_uuid', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('tasko_uuid', False)
                            or kwargs.get('posedanto_objekto_uuid', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_tasko_posedantoj = UniversoTaskoPosedanto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            # проверяем на владельца проекта или владельца объекта, по которому проект
                            if universo_tasko_posedantoj.posedanto_uzanto:
                                if universo_tasko_posedantoj.posedanto_uzanto != universo_uzanto: # пользователи не совпадают
                                    posedanto_perm = None
                                else:
                                    posedanto_perm = universo_uzanto
                            if posedanto_perm: # продолжаем проверку по объекту
                                try:
                                    posedanto_objekto2 = UniversoObjektoPosedanto.objects.get(
                                            objekto=universo_tasko_posedantoj.posedanto_objekto, 
                                            posedanto_uzanto=uzanto,
                                            forigo=False, arkivo=False, publikigo=True)
                                    if posedanto_perm != posedanto_objekto2.posedanto_uzanto: # проверяем его соответствие с владельцем объекта
                                        posedanto_perm = None
                                except UniversoObjektoPosedanto.DoesNotExist:
                                    pass

                            if (not (uzanto.has_perm('universo_taskoj.povas_forigi_universo_taskoj_posedantoj')
                                    or posedanto_perm)
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('universo_taskoj.povas_shanghi_universo_taskoj_posedantoj')
                                    or posedanto_perm):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_uzanto_siriuso_uzanto_id' in kwargs:
                                    try:
                                        posedanto_uzanto = UniversoUzanto.objects.get(
                                            siriuso_uzanto_id=kwargs.get('posedanto_uzanto_siriuso_uzanto_id'), 
                                            publikigo=True
                                        )
                                    except UniversoUzanto.DoesNotExist:
                                        message = _('Неверный владелец/пользователь задачи Универсо')

                            if not message:
                                if 'posedanto_organizo_uuid' in kwargs:
                                    try:
                                        posedanto_organizo = UniversoOrganizo.objects.get(
                                            uuid=kwargs.get('posedanto_organizo_uuid'),
                                            forigo=False,
                                            arkivo=False,  
                                            publikigo=True
                                        )
                                    except UniversoOrganizo.DoesNotExist:
                                        message = _('Неверная организация владелец задачи Универсо')

                            if not message:
                                if 'tasko_uuid' in kwargs:
                                    try:
                                        tasko = UniversoTasko.objects.get(uuid=kwargs.get('tasko_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoTasko.DoesNotExist:
                                        message = _('Неверная задача Универсо')

                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoRealeco.DoesNotExist:
                                        message = _('Неверный параллельный мир Универсо')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoTaskoPosedantoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoTaskoPosedantoTipo.DoesNotExist:
                                        message = _('Неверный тип владельца задачи Универсо')

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = UniversoTaskoPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoTaskoPosedantoStatuso.DoesNotExist:
                                        message = _('Неверный статус владельца задачи Универсо')

                            if not message:
                                if 'posedanto_objekto_uuid' in kwargs:
                                    try:
                                        posedanto_objekto = UniversoObjekto.objects.get(
                                            uuid=kwargs.get('posedanto_objekto_uuid'), 
                                            forigo=False,
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except UniversoObjekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект владелец задачи Универсо'),'posedanto_objekto_uuid')

                            if not message:

                                universo_tasko_posedantoj.forigo = kwargs.get('forigo', universo_tasko_posedantoj.forigo)
                                universo_tasko_posedantoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_tasko_posedantoj.arkivo = kwargs.get('arkivo', universo_tasko_posedantoj.arkivo)
                                universo_tasko_posedantoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_tasko_posedantoj.publikigo = kwargs.get('publikigo', universo_tasko_posedantoj.publikigo)
                                universo_tasko_posedantoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_tasko_posedantoj.realeco = realeco if kwargs.get('realeco_id', False) else universo_tasko_posedantoj.realeco
                                universo_tasko_posedantoj.tasko = realeco if kwargs.get('tasko_uuid', False) else universo_tasko_posedantoj.tasko
                                universo_tasko_posedantoj.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_siriuso_uzanto_id', False) else universo_tasko_posedantoj.posedanto_uzanto
                                universo_tasko_posedantoj.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) else universo_tasko_posedantoj.posedanto_organizo
                                universo_tasko_posedantoj.statuso = statuso if kwargs.get('statuso_id', False) else universo_tasko_posedantoj.statuso
                                universo_tasko_posedantoj.tipo = tipo if kwargs.get('tipo_id', False) else universo_tasko_posedantoj.tipo
                                universo_tasko_posedantoj.posedanto_objekto = posedanto_objekto if kwargs.get('posedanto_objekto_uuid', False) else universo_tasko_posedantoj.posedanto_objekto

                                universo_tasko_posedantoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoTaskoPosedanto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoTaskoPosedanto(status=status, message=message, universo_tasko_posedantoj=universo_tasko_posedantoj)


# Модель добавления множества задач с их владельцами в Универсо
class RedaktuKreiUniversoTaskojPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_taskoj = graphene.Field(graphene.List(UniversoTaskoNode), 
        description=_('Создание списка задач с владельцами в Universo'))

    class Arguments:
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        realeco_id = graphene.Int(description=_('Параллельный мир Универсо'))
        projekto_uuid = graphene.String(description=_('Проект Универсо в который входит задача Универсо'))
        objekto_uuid = graphene.String(description=_('Объект Универсо'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий задач Универсо'))
        sxablono_id = graphene.Int(description=_('Шаблон задач Универсо'))
        tipo_id = graphene.Int(description=_('Тип задач Универсо'))
        # статусы задача в перечне разные
        statuso_id = graphene.List(graphene.Int, description=_('Статус задач Универсо'))
        kom_koordinato_x = graphene.List(graphene.Float, description=_('Начальные координаты выполнения задачи по оси X в кубе Универсо'))
        kom_koordinato_y = graphene.List(graphene.Float, description=_('Начальные координаты выполнения задачи по оси Y в кубе Универсо'))
        kom_koordinato_z = graphene.List(graphene.Float, description=_('Начальные координаты выполнения задачи по оси Z в кубе Универсо'))
        fin_koordinato_x = graphene.List(graphene.Float, description=_('Конечные координаты выполнения задачи по оси X в кубе Универсо'))
        fin_koordinato_y = graphene.List(graphene.Float, description=_('Конечные координаты выполнения задачи по оси Y в кубе Универсо'))
        fin_koordinato_z = graphene.List(graphene.Float, description=_('Конечные координаты выполнения задачи по оси z в кубе Универсо'))
        pozicio = graphene.List(graphene.Int, description=_('Позиция в списке'))

        # Владельцы задач - объекты
        posedanto_tipo_id = graphene.Int(description=_('Тип владельца задач Универсо'))
        posedanto_statuso_id = graphene.Int(description=_('Статус владельца задач Универсо'))
        posedanto_objekto_uuid = graphene.String(description=_('Объект владелец задачи Универсо'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_taskoj = None
        universoj_taskoj = []
        realeco = None
        projekto = None
        objekto = None
        kategorio = UniversoTaskoKategorio.objects.none()
        sxablono = None
        tipo = None
        statuso = None
        posedanto_tipo = None
        posedanto_statuso = None
        posedanto_objekto = None
        uzanto = info.context.user
        universo_uzanto = None # пользователь Универсо
        posedanto_perm = None # для проверки владельца объекта/задачи

        if uzanto.is_authenticated:
            with transaction.atomic():
                # находим пользователя Универсо
                try:
                    universo_uzanto = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                except UniversoUzanto.DoesNotExist:
                    message = _('Неверный пользователь Universo')
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    # проверяем владельца объекта, по которому идёт добавление
                    # если пользователь создаёт владельца проекта по объекту, чьим владельцем является, то разрешаем
                    # проверяем владельца объекта
                    if not message:
                        if 'objekto_uuid' in kwargs:
                            try:
                                objekto_perm = UniversoObjekto.objects.get(
                                    uuid=kwargs.get('objekto_uuid'), 
                                    forigo=False,
                                    arkivo=False, 
                                    publikigo=True
                                )
                                try:
                                    posedanto_objekto_perm = UniversoObjektoPosedanto.objects.get(objekto=objekto_perm, 
                                                posedanto_uzanto=universo_uzanto,
                                                forigo=False, arkivo=False, publikigo=True)
                                    if universo_uzanto != posedanto_objekto_perm.posedanto_uzanto: # проверяем его соответствие с владельцем объекта
                                        posedanto_perm = None
                                    else:
                                        posedanto_perm = universo_uzanto
                                except UniversoObjektoPosedanto.DoesNotExist:
                                    pass
                            except UniversoObjekto.DoesNotExist:
                                message = '{} "{}"'.format(_('Неверный объект владелец проекта Универсо'), 'posedanto_objekto_uuid')
                    if not message:
                        if 'posedanto_objekto_uuid' in kwargs:
                            try:
                                objekto_perm = UniversoObjekto.objects.get(
                                    uuid=kwargs.get('posedanto_objekto_uuid'), 
                                    forigo=False,
                                    arkivo=False, 
                                    publikigo=True
                                )
                                try:
                                    posedanto_objekto_perm = UniversoObjektoPosedanto.objects.get(objekto=objekto_perm, 
                                                posedanto_uzanto=universo_uzanto,
                                                forigo=False, arkivo=False, publikigo=True)
                                    if posedanto_perm: # если пользователь был указан ранее
                                        if posedanto_perm != posedanto_objekto_perm.posedanto_uzanto: # проверяем его соответствие с владельцем объекта
                                            posedanto_perm = None
                                except UniversoObjektoPosedanto.DoesNotExist:
                                    pass
                            except UniversoObjekto.DoesNotExist:
                                message = '{} "{}"'.format(_('Неверный объект владелец проекта Универсо'), 'posedanto_objekto_uuid')

                    if uzanto.has_perm('universo_taskoj.povas_krei_universo_taskoj') or posedanto_perm:
                        # Проверяем наличие всех полей
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            if 'kategorio' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                            elif not len(kwargs.get('kategorio')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'kategorio')
                            else:
                                kategorio_id = set(kwargs.get('kategorio'))

                                if len(kategorio_id):
                                    kategorio = UniversoTaskoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие категории задач Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoRealeco.DoesNotExist:
                                    message = _('Неверный параллельный мир Универсо')

                        if not message:
                            if 'projekto_uuid' in kwargs:
                                try:
                                    projekto = UniversoProjekto.objects.get(uuid=kwargs.get('projekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoProjekto.DoesNotExist:
                                    message = _('Неверный проект Универсо в который входит задача')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'projekto_uuid')

                        if not message:
                            if 'objekto_uuid' in kwargs:
                                try:
                                    objekto = UniversoObjekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoObjekto.DoesNotExist:
                                    message = _('Неверный объект Универсо')

                        if not message:
                            if 'sxablono_id' in kwargs:
                                try:
                                    sxablono = UniversoTaskoSxablono.objects.get(id=kwargs.get('sxablono_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoTaskoSxablono.DoesNotExist:
                                    message = _('Неверный шаблон задачи Универсо')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoTaskoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoTaskoTipo.DoesNotExist:
                                    message = _('Неверный тип задачи Универсо')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')


                        if not message:
                            if 'posedanto_tipo_id' in kwargs:
                                try:
                                    posedanto_tipo = UniversoTaskoPosedantoTipo.objects.get(id=kwargs.get('posedanto_tipo_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                                except UniversoTaskoPosedantoTipo.DoesNotExist:
                                    message = _('Неверный тип владельца задачи Универсо')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_tipo_id')

                        if not message:
                            if 'posedanto_statuso_id' in kwargs:
                                try:
                                    posedanto_statuso = UniversoTaskoPosedantoStatuso.objects.get(id=kwargs.get('posedanto_statuso_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                                except UniversoTaskoPosedantoStatuso.DoesNotExist:
                                    message = _('Неверный статус владельца задачи Универсо')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_statuso_id')

                        if not message:
                            if 'posedanto_objekto_uuid' in kwargs:
                                try:
                                    posedanto_objekto = UniversoObjekto.objects.get(
                                        uuid=kwargs.get('posedanto_objekto_uuid'), 
                                        forigo=False,
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except UniversoObjekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект владелец задачи Универсо'),'posedanto_objekto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_objekto_uuid')

                        if not kwargs.get('pozicio',False):
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'pozicio')

                        if (not message) and kwargs.get('pozicio',False):
                            pozicioj = kwargs.get('pozicio')
                            statusoj = kwargs.get('statuso_id')
                            i = 0
                            for pozic in pozicioj:
                                if not message:
                                    if 'statuso_id' in kwargs:
                                        try:
                                            statuso = UniversoTaskoStatuso.objects.get(id=statusoj[i], forigo=False,
                                                                            arkivo=False, publikigo=True)
                                        except UniversoTaskoStatuso.DoesNotExist:
                                            message = _('Неверный статус задач Универсо')
                                    else:
                                        message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')
                                else:
                                    break

                                if not message:
                                    universo_taskoj = UniversoTasko.objects.create(
                                        forigo=False,
                                        arkivo=False,
                                        realeco = realeco,
                                        projekto = projekto,
                                        objekto = objekto,
                                        tipo = tipo,
                                        sxablono = sxablono,
                                        statuso = statuso,
                                        publikigo = True,
                                        publikiga_dato = timezone.now(),
                                        kom_koordinato_x = kwargs.get('kom_koordinato_x', None)[i],
                                        kom_koordinato_y = kwargs.get('kom_koordinato_y', None)[i],
                                        kom_koordinato_z = kwargs.get('kom_koordinato_z', None)[i],
                                        fin_koordinato_x = kwargs.get('fin_koordinato_x', None)[i],
                                        fin_koordinato_y = kwargs.get('fin_koordinato_y', None)[i],
                                        fin_koordinato_z = kwargs.get('fin_koordinato_z', None)[i],
                                        pozicio = pozic,
                                    )

                                    if (kwargs.get('nomo', False)):
                                        set_enhavo(universo_taskoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                                    if (kwargs.get('priskribo', False)):
                                        set_enhavo(universo_taskoj.priskribo, 
                                                kwargs.get('priskribo'), 
                                                info.context.LANGUAGE_CODE)
                                    if 'kategorio' in kwargs:
                                        if kategorio:
                                            universo_taskoj.kategorio.set(kategorio)
                                        else:
                                            universo_taskoj.kategorio.clear()

                                    universo_taskoj.save()
                                    universoj_taskoj.append(universo_taskoj)

                                    if not message:
                                        universo_tasko_posedantoj = UniversoTaskoPosedanto.objects.create(
                                            forigo = False,
                                            arkivo = False,
                                            realeco = realeco,
                                            tasko = universo_taskoj,
                                            tipo = posedanto_tipo,
                                            statuso = posedanto_statuso,
                                            posedanto_objekto = posedanto_objekto,
                                            publikigo = True,
                                            publikiga_dato = timezone.now()
                                        )

                                        universo_tasko_posedantoj.save()


                                    status = True
                                
                                i += 1

                            if status:
                                message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
        else:
            message = _('Требуется авторизация')

        return RedaktuKreiUniversoTaskojPosedanto(status=status, message=message, universo_taskoj=universoj_taskoj)


class UniversoTaskojMutations(graphene.ObjectType):
    redaktu_universo_projekto_kategorio = RedaktuUniversoProjektoKategorio.Field(
        description=_('''Создаёт или редактирует категории проектов в Универсо''')
    )
    redaktu_universo_projekto_tipo = RedaktuUniversoProjektoTipo.Field(
        description=_('''Создаёт или редактирует типы проектов в Универсо''')
    )
    redaktu_universo_projekto_sxablonoj = RedaktuUniversoProjektoSxablono.Field(
        description=_('''Создаёт или редактирует шаблоны проектов в Универсо''')
    )
    redaktu_universo_projekto_statuso = RedaktuUniversoProjektoStatuso.Field(
        description=_('''Создаёт или редактирует статусы проектов в Универсо''')
    )
    redaktu_universo_projekto = RedaktuUniversoProjekto.Field(
        description=_('''Создаёт или редактирует проекты в Универсо''')
    )
    redaktu_universo_projekto_posedantoj_tipo = RedaktuUniversoProjektoPosedantoTipo.Field(
        description=_('''Создаёт или редактирует типы владельцев проектов в Универсо''')
    )
    redaktu_universo_projekto_posedantoj_statuso = RedaktuUniversoProjektoPosedantoStatuso.Field(
        description=_('''Создаёт или редактирует статусы владельцев проектов в Универсо''')
    )
    redaktu_universo_projekto_posedantoj = RedaktuUniversoProjektoPosedanto.Field(
        description=_('''Создаёт или редактирует владельцев проекты в Универсо''')
    )
    redaktu_universo_taskoj_kategorio = RedaktuUniversoTaskoKategorio.Field(
        description=_('''Создаёт или редактирует категории задач в Универсо''')
    )
    redaktu_universo_taskoj_tipo = RedaktuUniversoTaskoTipo.Field(
        description=_('''Создаёт или редактирует типы задач в Универсо''')
    )
    redaktu_universo_taskoj_sxablonoj = RedaktuUniversoTaskoSxablono.Field(
        description=_('''Создаёт или редактирует шаблоны задач в Универсо''')
    )
    redaktu_universo_tasko_statuso = RedaktuUniversoTaskoStatuso.Field(
        description=_('''Создаёт или редактирует статусы задач в Универсо''')
    )
    redaktu_universo_taskoj = RedaktuUniversoTasko.Field(
        description=_('''Создаёт или редактирует задачи в Универсо''')
    )
    redaktu_universo_taskoj_posedantoj_tipo = RedaktuUniversoTaskoPosedantoTipo.Field(
        description=_('''Создаёт или редактирует типы владельцев задач в Универсо''')
    )
    redaktu_universo_tasko_posedantoj_statuso = RedaktuUniversoTaskoPosedantoStatuso.Field(
        description=_('''Создаёт или редактирует статусы владельцев задач в Универсо''')
    )
    redaktu_universo_tasko_posedantoj = RedaktuUniversoTaskoPosedanto.Field(
        description=_('''Создаёт или редактирует владельцев задачи в Универсо''')
    )
    redaktu_krei_universo_taskoj_posedanto = RedaktuKreiUniversoTaskojPosedanto.Field(
        description=_('''Создаёт список задач с владельцами в Универсо''')
    )

