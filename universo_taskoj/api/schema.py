import graphene  # сам Graphene
from django.utils.translation import ugettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import *  # модели приложения


# Модель задач Универсо
class UniversoTaskoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoTasko
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'objekto__uuid': ['exact'],
            'sxablono__uuid': ['exact'],
            'statuso__id': ['exact'],
            'tipo__id': ['exact'],
            'kategorio__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель категорий проектов в Универсо
class UniversoProjektoKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoProjektoKategorio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов проектов в Универсо
class UniversoProjektoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoProjektoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель шаблонов проектов Универсо
class UniversoProjektoSxablonoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoProjektoSxablono
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact'],
            'kategorio__id': ['exact'],
            'tipo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов связей шаблонов проектов между собой
class UniversoProjektoSxablonoLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoProjektoSxablonoLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель связей шаблонов проектов между собой
class UniversoProjektoSxablonoLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = UniversoProjektoSxablonoLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__id': ['exact'],
            'ligilo__id': ['exact'],
            'tipo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов проектов Универсо
class UniversoProjektoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoProjektoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель владельцев проектов Универсо
class UniversoProjektoPosedantoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = UniversoProjektoPosedanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'projekto__uuid': ['exact'],
            'posedanto_uzanto__siriuso_uzanto__id': ['exact'],
            'posedanto_organizo__uuid': ['exact'],
            'tipo__id': ['exact'],
            'statuso__id': ['exact'],
            'posedanto_objekto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель проектов Универсо
class UniversoProjektoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    # задача
    tasko =  SiriusoFilterConnectionField(UniversoTaskoNode,
        description=_('Выводит задачи, привязанные к проекту'))

    # владельцы
    posedanto =  SiriusoFilterConnectionField(UniversoProjektoPosedantoNode,
        description=_('Выводит владельцев проектов Универсо'))

    class Meta:
        model = UniversoProjekto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'sxablono__uuid': ['exact'],
            'tipo__id': ['exact'],
            'statuso__id': ['exact'],
            'objekto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_tasko(self, info, **kwargs):
        return UniversoTasko.objects.filter(projekto=self, forigo=False, arkivo=False, 
            publikigo=True)

    def resolve_posedanto(self, info, **kwargs):
        return UniversoProjektoPosedanto.objects.filter(projekto=self, forigo=False, arkivo=False, 
            publikigo=True)


# Модель типов владельцев проектов Универсо
class UniversoProjektoPosedantoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoProjektoPosedantoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов владельца проекта Универсо
class UniversoProjektoPosedantoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoProjektoPosedantoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов связей проектов между собой
class UniversoProjektoLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoProjektoLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связей проектов между собой
class UniversoProjektoLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = UniversoProjektoLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'ligilo__uuid': ['exact'],
            'tipo__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель категорий задач Универсо
class UniversoTaskoKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = UniversoTaskoKategorio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов задач в Универсо
class UniversoTaskoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = UniversoTaskoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель шаблонов задач Универсо
class UniversoTaskoSxablonoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoTaskoSxablono
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact'],
            'tipo__id':['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов связей шаблонов задач между собой
class UniversoTaskoSxablonoLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoTaskoSxablonoLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связей шаблонов задач между собой
class UniversoTaskoSxablonoLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = UniversoTaskoSxablonoLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'ligilo__uuid': ['exact'],
            'tipo__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов задач в Универсо
class UniversoTaskoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = UniversoTaskoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов владельцев задач в Универсо
class UniversoTaskoPosedantoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = UniversoTaskoPosedantoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов владельца задач в Универсо
class UniversoTaskoPosedantoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = UniversoTaskoPosedantoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель владельцев задач Универсо
class UniversoTaskoPosedantoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = UniversoTaskoPosedanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'tasko__uuid': ['exact'],
            'posedanto_uzanto__siriuso_uzanto__id': ['exact'],
            'posedanto_organizo__uuid': ['exact'],
            'statuso__id': ['exact'],
            'tipo__id': ['exact'],
            'posedanto_objekto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов связей задач между собой
class UniversoTaskoLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoTaskoLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связей задач между собой
class UniversoTaskoLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = UniversoTaskoLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'ligilo__uuid': ['exact'],
            'tipo__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


class UniversoTaskojQuery(graphene.ObjectType):
    universo_projekto_kategorio = SiriusoFilterConnectionField(
        UniversoProjektoKategorioNode,
        description=_('Выводит все доступные модели категорий проектов в Универсо')
    )
    universo_projekto_tipo = SiriusoFilterConnectionField(
        UniversoProjektoTipoNode,
        description=_('Выводит все доступные модели типов проектов в Универсо')
    )
    universo_projekto_sxablono = SiriusoFilterConnectionField(
        UniversoProjektoSxablonoNode,
        description=_('Выводит все доступные модели шаблонов проектов Универсо')
    )
    universo_projekto_sxablono_ligilo_tipo = SiriusoFilterConnectionField(
        UniversoProjektoSxablonoLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей шаблонов проектов между собой Универсо')
    )
    universo_projekto_sxablono_ligilo = SiriusoFilterConnectionField(
        UniversoProjektoSxablonoLigiloNode,
        description=_('Выводит все доступные модели связей шаблонов проектов между собой Универсо')
    )
    universo_projekto_statuso = SiriusoFilterConnectionField(
        UniversoProjektoStatusoNode,
        description=_('Выводит все доступные модели статусов проектов в Универсо')
    )
    universo_projekto = SiriusoFilterConnectionField(
        UniversoProjektoNode,
        description=_('Выводит все доступные модели проектов Универсо')
    )
    universo_projekto_posedantoj_tipo = SiriusoFilterConnectionField(
        UniversoProjektoPosedantoTipoNode,
        description=_('Выводит все доступные модели типов владельцев проектов Универсо')
    )
    universo_projekto_posedantoj_statuso = SiriusoFilterConnectionField(
        UniversoProjektoPosedantoStatusoNode,
        description=_('Выводит все доступные модели статусов владельца проекта Универсо')
    )
    universo_projekto_posedantoj = SiriusoFilterConnectionField(
        UniversoProjektoPosedantoNode,
        description=_('Выводит все доступные модели владельцев проектов Универсо')
    )
    universo_projekto_ligilo_tipo = SiriusoFilterConnectionField(
        UniversoProjektoLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей проектов между собой')
    )
    universo_projekto_ligilo = SiriusoFilterConnectionField(
        UniversoProjektoLigiloNode,
        description=_('Выводит все доступные модели связей проектов между собой')
    )
    universo_projekto = SiriusoFilterConnectionField(
        UniversoProjektoNode,
        description=_('Выводит все доступные модели владельцев проектов Универсо')
    )
    universo_tasko_kategorio = SiriusoFilterConnectionField(
        UniversoTaskoKategorioNode,
        description=_('Выводит все доступные модели категорий задач Универсо')
    )
    universo_tasko_tipo = SiriusoFilterConnectionField(
        UniversoTaskoTipoNode,
        description=_('Выводит все доступные модели типов задач в Универсо')
    )
    universo_tasko_sxablonoj = SiriusoFilterConnectionField(
        UniversoTaskoSxablonoNode,
        description=_('Выводит все доступные модели шаблонов задач Универсо')
    )
    universo_tasko_sxablonoj_ligilo_tipo = SiriusoFilterConnectionField(
        UniversoTaskoSxablonoLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей шаблонов задач между собой')
    )
    universo_tasko_sxablonoj_ligilo = SiriusoFilterConnectionField(
        UniversoTaskoSxablonoLigiloNode,
        description=_('Выводит все доступные модели связей шаблонов задач между собой')
    )
    universo_tasko_statuso = SiriusoFilterConnectionField(
        UniversoTaskoStatusoNode,
        description=_('Выводит все доступные модели статусов задач в Универсо')
    )
    universo_tasko = SiriusoFilterConnectionField(
        UniversoTaskoNode,
        description=_('Выводит все доступные модели задач Универсо')
    )
    universo_tasko_posedantoj_tipo = SiriusoFilterConnectionField(
        UniversoTaskoPosedantoTipoNode,
        description=_('Выводит все доступные модели типов владельцев задач в Универсо')
    )
    universo_tasko_posedantoj_statuso = SiriusoFilterConnectionField(
        UniversoTaskoPosedantoStatusoNode,
        description=_('Выводит все доступные модели статусов владельца задач в Универсо')
    )
    universo_tasko_posedanto = SiriusoFilterConnectionField(
        UniversoTaskoPosedantoNode,
        description=_('Выводит все доступные модели владельцев задач Универсо')
    )
    universo_tasko_ligilo_tipo = SiriusoFilterConnectionField(
        UniversoTaskoLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей задач между собой')
    )
    universo_tasko_ligilo = SiriusoFilterConnectionField(
        UniversoTaskoLigiloNode,
        description=_('Выводит все доступные модели связей задач между собой')
    )
