"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2020 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from django import forms
from django.utils.translation import ugettext_lazy as _
import json

from siriuso.utils import get_enhavo
from .models import *
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
from universo_bazo.models import UniversoRealeco


class TekstoMixin:
    @staticmethod
    def teksto(obj, field):
        kampo = getattr(obj, field)

        if kampo is not None:
            result = get_enhavo(kampo)

            if result:
                return result[0]
        return ''

    def nomo_teksto(self, obj):
        return self.teksto(obj=obj, field='nomo')

    def priskribo_teksto(self, obj):
        return self.teksto(obj=obj, field='priskribo')

    def sxablono_priskribo_teksto(self, obj):
        return self.teksto(obj=obj, field='sxablono_sistema_priskribo')

    def autoro_id(self, obj):
        return obj.autoro.siriuso_uzanto.id

    def autoro_email(self, obj):
        return obj.autoro.siriuso_uzanto.chefa_retposhto


# Форма категорий проектов в Universo
class UniversoProjektoKategorioFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=UniversoRealeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = UniversoProjektoKategorio
        fields = [field.name for field in UniversoProjektoKategorio._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Категорий проектов в Universo
@admin.register(UniversoProjektoKategorio)
class UniversoProjektoKategorioAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoProjektoKategorioFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoProjektoKategorio


# Форма типов проектов в Universo
class UniversoProjektoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=UniversoRealeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = UniversoProjektoTipo
        fields = [field.name for field in UniversoProjektoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы проектов в Universo
@admin.register(UniversoProjektoTipo)
class UniversoProjektoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoProjektoTipoFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoProjektoTipo


# Форма проектов в Universo
class UniversoProjektoSxablonoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    kategorio = forms.ModelMultipleChoiceField(
        label=_('Kategorio de projektoj de Universo'),
        queryset=UniversoProjektoKategorio.objects.filter(forigo=False, publikigo=True)
    )
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=UniversoRealeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = UniversoProjektoSxablono
        fields = [field.name for field in UniversoProjektoSxablono._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Проекты в Universo
@admin.register(UniversoProjektoSxablono)
class UniversoProjektoSxablonoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoProjektoSxablonoFormo
    list_display = ('id','nomo_teksto','autoro_id','priskribo_teksto')
    exclude = ('uuid',)
    class Meta:
        model = UniversoProjektoSxablono


# Форма типов связей шаблонов проектов между собой
class UniversoProjektoSxablonoLigiloTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoProjektoSxablonoLigiloTipo
        fields = [field.name for field in UniversoProjektoSxablonoLigiloTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы связей шаблонов проектов между собой
@admin.register(UniversoProjektoSxablonoLigiloTipo)
class UniversoProjektoSxablonoLigiloTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoProjektoSxablonoLigiloTipoFormo
    list_display = ('id','nomo_teksto','autoro_id','priskribo_teksto')
    exclude = ('uuid',)
    class Meta:
        model = UniversoProjektoSxablonoLigiloTipo


# Форма связей шаблонов проектов между собой
class UniversoProjektoSxablonoLigiloFormo(forms.ModelForm):
    
    class Meta:
        model = UniversoProjektoSxablonoLigilo
        fields = [field.name for field in UniversoProjektoSxablonoLigilo._meta.fields if field.name not in ('krea_dato', 'uuid')]


# Связь шаблонов проектов между собой
@admin.register(UniversoProjektoSxablonoLigilo)
class UniversoProjektoSxablonoLigiloAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoProjektoSxablonoLigiloFormo
    list_display = ('posedanto','tipo','ligilo')
    exclude = ('uuid',)
    class Meta:
        model = UniversoProjektoSxablonoLigilo


# Форма статусов проектов в Universo
class UniversoProjektoStatusoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoProjektoStatuso
        fields = [field.name for field in UniversoProjektoStatuso._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Статусы проектов в Universo
@admin.register(UniversoProjektoStatuso)
class UniversoProjektoStatusoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoProjektoStatusoFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoProjektoStatuso


# Владелец модели объекта Универсо
class UniversoProjektoPosedantoInline(admin.TabularInline):
    model = UniversoProjektoPosedanto
    fk_name = 'projekto'
    extra = 1

# Форма проектов в Universo
class UniversoProjektoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    kategorio = forms.ModelMultipleChoiceField(
        label=_('категория проектов Универсо'),
        queryset=UniversoProjektoKategorio.objects.filter(forigo=False, publikigo=True)
    )
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = UniversoProjekto
        fields = [field.name for field in UniversoProjekto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Проекты в Universo
@admin.register(UniversoProjekto)
class UniversoProjektoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoProjektoFormo
    list_display = ('uuid','krea_dato','statuso','nomo_teksto','priskribo_teksto','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    inlines = (UniversoProjektoPosedantoInline,)
    list_filter = ('sxablono_sistema',)
    class Meta:
        model = UniversoProjekto


# Форма типов владельцев проектов в Universo
class UniversoProjektoPosedantoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoProjektoPosedantoTipo
        fields = [field.name for field in UniversoProjektoPosedantoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы владельцев проектов в Universo
@admin.register(UniversoProjektoPosedantoTipo)
class UniversoProjektoPosedantoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoProjektoPosedantoTipoFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoProjektoPosedantoTipo


# Форма статусов владельцев проектов в Universo
class UniversoProjektoPosedantoStatusoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoProjektoPosedantoStatuso
        fields = [field.name for field in UniversoProjektoPosedantoStatuso._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Статусы владельцев проектов в Universo
@admin.register(UniversoProjektoPosedantoStatuso)
class UniversoProjektoPosedantoStatusoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoProjektoPosedantoStatusoFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoProjektoPosedantoStatuso


# Форма проектов в Universo
class UniversoProjektoPosedantoFormo(forms.ModelForm):
    
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = UniversoProjektoPosedanto
        fields = [field.name for field in UniversoProjektoPosedanto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Проекты в Universo
@admin.register(UniversoProjektoPosedanto)
class UniversoProjektoPosedantoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoProjektoPosedantoFormo
    list_display = ('uuid','projekto','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema',)
    class Meta:
        model = UniversoProjektoPosedanto


# Форма типов связей проектов между собой
class UniversoProjektoLigiloTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoProjektoLigiloTipo
        fields = [field.name for field in UniversoProjektoLigiloTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы связей проектов между собой
@admin.register(UniversoProjektoLigiloTipo)
class UniversoProjektoLigiloTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoProjektoLigiloTipoFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoProjektoLigiloTipo


# Форма связей проектов между собой
class UniversoProjektoLigiloFormo(forms.ModelForm):
    
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = UniversoProjektoLigilo
        fields = [field.name for field in UniversoProjektoLigilo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Связи проектов между собой
@admin.register(UniversoProjektoLigilo)
class UniversoProjektoLigiloAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoProjektoLigiloFormo
    list_display = ('posedanto','tipo','ligilo','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema',)
    class Meta:
        model = UniversoProjektoLigilo


# Форма категорий задач в Universo
class UniversoTaskoKategorioFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=UniversoRealeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = UniversoTaskoKategorio
        fields = [field.name for field in UniversoTaskoKategorio._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Категорий задач в Universo
@admin.register(UniversoTaskoKategorio)
class UniversoTaskoKategorioAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoTaskoKategorioFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoTaskoKategorio


# Форма типов задач в Universo
class UniversoTaskoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=UniversoRealeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = UniversoTaskoTipo
        fields = [field.name for field in UniversoTaskoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы задач в Universo
@admin.register(UniversoTaskoTipo)
class UniversoTaskoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoTaskoTipoFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoTaskoTipo


# Форма шаблонов задач в Universo
class UniversoTaskoSxablonoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    kategorio = forms.ModelMultipleChoiceField(
        label=_('Категория задач в Универсо'),
        queryset=UniversoTaskoKategorio.objects.filter(forigo=False, publikigo=True)
    )
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=UniversoRealeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = UniversoTaskoSxablono
        fields = [field.name for field in UniversoTaskoSxablono._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Шаблоны задач в Universo
@admin.register(UniversoTaskoSxablono)
class UniversoTaskoSxablonoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoTaskoSxablonoFormo
    list_display = ('id','nomo_teksto','autoro_id','priskribo_teksto')
    exclude = ('uuid',)
    class Meta:
        model = UniversoTaskoSxablono


# Форма типов связей шаблонов задач между собой
class UniversoTaskoSxablonoLigiloTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoTaskoSxablonoLigiloTipo
        fields = [field.name for field in UniversoTaskoSxablonoLigiloTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы связей шаблонов задач между собой
@admin.register(UniversoTaskoSxablonoLigiloTipo)
class UniversoTaskoSxablonoLigiloTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoTaskoSxablonoLigiloTipoFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoTaskoSxablonoLigiloTipo


# Форма связей шаблонов задач между собой
class UniversoTaskoSxablonoLigiloFormo(forms.ModelForm):
    
    class Meta:
        model = UniversoTaskoSxablonoLigilo
        fields = [field.name for field in UniversoTaskoSxablonoLigilo._meta.fields if field.name not in ('krea_dato', 'uuid')]


# Связи шаблонов задач между собой
@admin.register(UniversoTaskoSxablonoLigilo)
class UniversoTaskoSxablonoLigiloAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoTaskoSxablonoLigiloFormo
    list_display = ('posedanto','tipo','ligilo')
    exclude = ('uuid',)
    class Meta:
        model = UniversoTaskoSxablonoLigilo


# Форма статусов задач в Universo
class UniversoTaskoStatusoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoTaskoStatuso
        fields = [field.name for field in UniversoTaskoStatuso._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Статус задач в Universo
@admin.register(UniversoTaskoStatuso)
class UniversoTaskoStatusoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoTaskoStatusoFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoTaskoStatuso


# Форма задач в Universo
class UniversoTaskoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    kategorio = forms.ModelMultipleChoiceField(
        label=_('Kategorio de taskoj de Universo'),
        queryset=UniversoTaskoKategorio.objects.filter(forigo=False, publikigo=True)
    )
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = UniversoTasko
        fields = [field.name for field in UniversoTasko._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Владелец задачи Универсо
class UniversoTaskoPosedantoInline(admin.TabularInline):
    model = UniversoTaskoPosedanto
    fk_name = 'tasko'
    extra = 1

# Задачи в Universo
@admin.register(UniversoTasko)
class UniversoTaskoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoTaskoFormo
    list_display = ('uuid','krea_dato','statuso','nomo_teksto','projekto','priskribo_teksto','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    inlines = (UniversoTaskoPosedantoInline,)
    list_filter = ('sxablono_sistema',)
    class Meta:
        model = UniversoTasko


# Форма типов владельцев задач в Universo
class UniversoTaskoPosedantoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoTaskoPosedantoTipo
        fields = [field.name for field in UniversoTaskoPosedantoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы владельцев задач в Universo
@admin.register(UniversoTaskoPosedantoTipo)
class UniversoTaskoPosedantoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoTaskoPosedantoTipoFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoTaskoPosedantoTipo


# Форма статусов владельцев задач в Universo
class UniversoTaskoPosedantoStatusoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoTaskoPosedantoStatuso
        fields = [field.name for field in UniversoTaskoPosedantoStatuso._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Статусы владельцев задач в Universo
@admin.register(UniversoTaskoPosedantoStatuso)
class UniversoTaskoPosedantoStatusoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoTaskoPosedantoStatusoFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoTaskoPosedantoStatuso


# Форма владельцев задач в Universo
class UniversoTaskoPosedantoFormo(forms.ModelForm):

    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = UniversoTaskoPosedanto
        fields = [field.name for field in UniversoTaskoPosedanto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Владельцы задач в Universo
@admin.register(UniversoTaskoPosedanto)
class UniversoTaskoPosedantoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoTaskoPosedantoFormo
    list_display = ('uuid','tasko','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema',)
    class Meta:
        model = UniversoTaskoPosedanto


# Форма типов связей задач между собой
class UniversoTaskoLigiloTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoTaskoLigiloTipo
        fields = [field.name for field in UniversoTaskoLigiloTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы связей задач между собой
@admin.register(UniversoTaskoLigiloTipo)
class UniversoTaskoLigiloTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoTaskoLigiloTipoFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoTaskoLigiloTipo


# Форма связей задач между собой
class UniversoTaskoLigiloFormo(forms.ModelForm):
    
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = UniversoTaskoLigilo
        fields = [field.name for field in UniversoTaskoLigilo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Связи задач между собой
@admin.register(UniversoTaskoLigilo)
class UniversoTaskoLigiloAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoTaskoLigiloFormo
    list_display = ('posedanto','tipo','ligilo','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema',)
    class Meta:
        model = UniversoTaskoLigilo
