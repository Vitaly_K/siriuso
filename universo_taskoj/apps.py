from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UniversoTaskojConfig(AppConfig):
    name = 'universo_taskoj'
    verbose_name = _('Taskoj de Universo')
