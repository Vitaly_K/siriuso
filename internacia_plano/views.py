"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.template.response import TemplateResponse
from django.http.response import Http404, JsonResponse
from siriuso.views import SiriusoTemplateView


# Международный план
@method_decorator(login_required, name='dispatch')
class InternaciaPlano(SiriusoTemplateView):

    template_name = 'internacia_plano/internacia_plano.html'
    mobile_template_name = 'internacia_plano/portebla/t_internacia_plano.html'


# Карта активности
@method_decorator(login_required, name='dispatch')
class PlanoMapo(SiriusoTemplateView):

    template_name = 'internacia_plano/plano_mapo.html'
    mobile_template_name = 'internacia_plano/portebla/t_plano_mapo.html'
