from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class InternaciaPlanoConfig(AppConfig):
    name = 'internacia_plano'
    verbose_name = _('Internacia plano')
