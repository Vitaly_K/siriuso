"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.db import models
from django.contrib.auth.models import AbstractUser, AbstractBaseUser, BaseUserManager
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres import fields as pgfields
from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo
from informiloj.models import InformilojLingvo
from main.models import SiriusoBazaAbstraktaUzanto, SiriusoTipoAbstrakta, SiriusoKomentoAbstrakta
from main.models import Uzanto
import random, string


# Варианты доступа (видимости) к сущностям пользователя, использует абстрактный класс SiriusoTipoAbstrakta
class UzantojAliro(SiriusoTipoAbstrakta):

    # Код доступа
    kodo = models.CharField(_('Kodo'), max_length=16)

    # Обозначения доступов
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'uzantoj_aliroj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Aliro')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Aliroj')


# Типы товарищей, использует абстрактный класс SiriusoTipoAbstrakta
class UzantojGekamaradojTipo(SiriusoTipoAbstrakta):

    # код товарища
    kodo = models.CharField(_('Kodo'), max_length=10, blank=False)

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'uzantoj_gekamaradoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de gekamaradoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de gekamaradoj')


# Товарищи, использует абстрактный класс SiriusoBazaAbstraktaUzanto
class UzantojGekamaradoj(SiriusoBazaAbstraktaUzanto):

    # тип товарища
    tipo = models.ForeignKey(UzantojGekamaradojTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # # тип принят (первым пользователем)
    # akceptis_tipo1 = models.BooleanField(_('Akceptis tipo (de sendinto)'), blank=False, default=True)
    #
    # # тип принят (вторым пользователем)
    # akceptis_tipo2 = models.BooleanField(_('Akceptis tipo (de akceptinto)'), blank=False, default=False)
    
    # товарищ (пользователь)
    gekamarado = models.ForeignKey(Uzanto, verbose_name=_('Posedanto'), blank=False, default=None, 
                                   related_name='%(app_label)s_%(class)s_gekamarado', on_delete=models.CASCADE)

    # заявка принята подавшим (первым пользователем)
    akceptis1 = models.BooleanField(_('Akceptis (de sendinto)'), blank=False, default=True)

    # заявка принята принявшим (вторым пользователем)
    akceptis2 = models.NullBooleanField(_('Akceptis (de akceptinto)'), blank=True, default=None)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'uzantoj_gekamaradoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Gekamarado')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Gekamaradoj')

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)


# Генерация случайного названия и переименование загружаемых картинок аватар пользователей
def uzantoj_avataro_nomo(instance, filename):
    letters = string.ascii_letters + string.digits
    rnd_string = ''.join(random.choice(letters) for i in range(10))
    # указание папки для хранения файлов и собирание конечного названия с сохранением изначального расширения
    return 'uzantoj/avataroj/bildoj/{0}.{1}'.format(rnd_string, filename.split('.')[-1])


# Аватары пользователей, использует абстрактный класс SiriusoBazaAbstraktaUzanto
class UzantojAvataro(SiriusoBazaAbstraktaUzanto):

    # главный вариант аватара для всех языковых версий (да или нет)
    chefa_varianto = models.BooleanField(_('Ĉefa varianto'), default=False)

    # активный аватар в рамках своей языковой версии (да или нет), неактивные можно будет просмотреть
    aktiva = models.BooleanField(_('Aktiva'), default=False)

    # основное изображение (размер 180x180)
    bildo = models.ImageField(_('Avataro'), upload_to=uzantoj_avataro_nomo, blank=False, default=None)

    # миниатюра (размер 50х50)
    bildo_min = models.ImageField(_('Miniaturo'), upload_to=uzantoj_avataro_nomo, blank=False, default=None)

    # исходное изображение
    bildo_maks = models.ImageField(_('Maksimuma bildo'), upload_to=uzantoj_avataro_nomo, blank=True, null=True)

    # Изображение из фотоальбома
    avataro = models.ForeignKey('fotoj.FotojFotoUzanto', verbose_name=_('Bildo de la albumo'), blank=True,
                                null=True, on_delete=models.CASCADE)

    # выбор языкового кода из справочника
    lingvo = models.ForeignKey(InformilojLingvo, verbose_name=_('Lingvo'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # разрешено ли комментировать изображение и кому разрешено
    komentado_aliro = models.ForeignKey(UzantojAliro, verbose_name=_('Komentada aliro'), blank=False, default=None,
                                        on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'uzantoj_avataroj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Avataro')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Avataroj')

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)


# Генерация случайного названия и переименование загружаемых картинок обложек пользователей
def uzantoj_kovrilo_nomo(instance, filename):
    letters = string.ascii_letters + string.digits
    rnd_string = ''.join(random.choice(letters) for i in range(10))
    # указание папки для хранения файлов и собирание конечного названия с сохранением изначального расширения
    return 'uzantoj/kovriloj/bildoj/{0}.{1}'.format(rnd_string, filename.split('.')[-1])


# Обложки пользователей, использует абстрактный класс SiriusoBazaAbstraktaUzanto
class UzantojKovrilo(SiriusoBazaAbstraktaUzanto):

    # главный вариант обложки для всех языковых версий (да или нет)
    chefa_varianto = models.BooleanField(_('Ĉefa varianto'), default=False)

    # активная обложка в рамках своей языковой версии (да или нет), неактивные можно будет просмотреть
    aktiva = models.BooleanField(_('Aktiva'), default=False)

    # изображение (размер 820x200)
    bildo = models.ImageField(_('Kovrilo'), upload_to=uzantoj_kovrilo_nomo, blank=False, default=None)

    # исходное изображение
    bildo_maks = models.ImageField(_('Maksimuma bildo'), upload_to=uzantoj_kovrilo_nomo, blank=True, null=True)

    # Изображение из фотоальбома
    kovrilo = models.ForeignKey('fotoj.FotojFotoUzanto', verbose_name=_('Bildo de la albumo'), blank=True,
                                null=True, on_delete=models.CASCADE)

    # выбор языкового кода из справочника
    lingvo = models.ForeignKey(InformilojLingvo, verbose_name=_('Lingvo'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # разрешено ли комментировать изображение и кому разрешено
    komentado_aliro = models.ForeignKey(UzantojAliro, verbose_name=_('Komentada aliro'), blank=False, default=None,
                                        on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'uzantoj_kovriloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kovrilo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kovriloj')

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)


# Статусы пользователей, использует абстрактный класс SiriusoBazaAbstraktaUzanto
class UzantojStatuso(SiriusoBazaAbstraktaUzanto):

    # текст в таблице текстов комментариев обложек, от туда будет браться текст с нужным языковым тегом
    teksto = pgfields.JSONField(verbose_name=_('Teksto'), blank=True, null=False, default=default_lingvo,
                                encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'uzantoj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj')

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле teksto этой модели
        return '{}'.format(get_enhavo(self.teksto, empty_values=True)[0])


# Уведомления для пользователей
class UzantojSciigoj(SiriusoBazaAbstraktaUzanto):
    # текстовое содержание уведомления
    teksto = models.TextField(_('Teksto'), blank=False, null=False)

    # атрибуты объектов события (если таковые есть)
    atributoj = pgfields.JSONField(_('Atributoj'), blank=True, null=True)

    # дата прочтения
    leganta_dato = models.DateTimeField(_('Leganta dato'), blank=True, null=True)

    # признак прочтения уведомления
    vidita = models.BooleanField(_('Vidita'), blank=True, default=False)

    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'uzantoj_sciigoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Uzanto sciigo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Uzantoj sciigoj')

    def __str__(self):
        return str(self.uuid)
