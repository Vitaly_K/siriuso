from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UzantojConfig(AppConfig):
    name = 'uzantoj'
    verbose_name = _('Uzantoj')
