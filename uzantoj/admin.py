"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from uzantoj.models import *


# class HomoContactInLine(admin.TabularInline):
#     model = HomoContact
#
#
# class HomoEducationInLine(admin.StackedInline):
#     model = HomoEducation


# Доступ (видимость) к различным сущностям привязанным к пользователю
@admin.register(UzantojAliro)
class UzantojAliroAdmin (admin.ModelAdmin):
    list_display = ('nomo',)
    exclude = ('nomo',)

    class Meta:
        model = UzantojAliro


# Статусы пользователей
@admin.register(UzantojStatuso)
class UzantojStatusoAdmin (admin.ModelAdmin):
    list_display = ('uuid', 'teksto')
    exclude = ('teksto',)
    search_fields = ('teksto',)
    ordering = ('uuid', 'teksto')

    class Meta:
        model = UzantojStatuso


# Отдельный вывод всех аватар пользователей
@admin.register(UzantojAvataro)
class UzantojAvataroAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'bildo', 'bildo_min', 'bildo_maks')

    class Meta:
        model = UzantojAvataro


# Отдельный вывод всех обложек пользователей
@admin.register(UzantojKovrilo)
class UzantojKovriloAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'bildo', 'bildo_maks')

    class Meta:
        model = UzantojKovrilo


# Типы товарищей
@admin.register(UzantojGekamaradojTipo)
class UzantojGekamaradojTipoAdmin (admin.ModelAdmin):
    list_display = ('nomo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = UzantojGekamaradojTipo


# Товарищи
@admin.register(UzantojGekamaradoj)
class UzantojGekamaradojAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'posedanto', 'gekamarado', 'tipo')

    class Meta:
        model = UzantojGekamaradoj
