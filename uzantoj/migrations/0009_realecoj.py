# Generated by Django 2.0.13 on 2019-07-17 09:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_realecoj'),
        ('uzantoj', '0008_uzantoj_sciigoj'),
    ]

    operations = [
        migrations.AddField(
            model_name='uzantojaliro',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='uzantojaliro',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AddField(
            model_name='uzantojavataro',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='uzantojavataro',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AddField(
            model_name='uzantojgekamaradoj',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='uzantojgekamaradoj',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AddField(
            model_name='uzantojgekamaradojtipo',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='uzantojgekamaradojtipo',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AddField(
            model_name='uzantojkovrilo',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='uzantojkovrilo',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AddField(
            model_name='uzantojsciigoj',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='uzantojsciigoj',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AddField(
            model_name='uzantojstatuso',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='uzantojstatuso',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
    ]
