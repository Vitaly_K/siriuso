"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from celery import shared_task
from django.core.mail import EmailMessage
from django.core import mail
from django.template.loader import get_template
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.contrib.sites.models import Site
from main.models import Uzanto

from datetime import timedelta


class HtmlEmailMessage(EmailMessage):
    content_subtype = 'html'


@shared_task
def send_email_confirmcode(mailto, code, email=True):
    email_body = get_template('uzantoj/emails/confirmcode.html')\
        .render({'confirmcode': code, 'site_url': Site.objects.get_current(), 'abono_shlosilo': False, 'poshto': email})

    email_message = {
        'subject': _('Код подтверждения действия на Техноком'),
        'body': email_body,
    }

    email = (HtmlEmailMessage(**email_message, to=(mailto,)),)

    with mail.get_connection() as connection:
        result = connection.send_messages(email)
    return result


@shared_task
def forigi_nekonfirmitajn_uzantojn():
    tt = timezone.now() - timedelta(days=2)

    return Uzanto.objects.filter(
        konfirmita=False,
        krea_dato__lte=tt
    ).delete()
