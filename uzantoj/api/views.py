from rest_framework import generics, viewsets, views
from rest_framework import permissions
from rest_framework.response import Response
from django.db.models import F, Max, Count, DateTimeField
from django.db.models.functions import Coalesce, Least
from django.utils import timezone
from main.models import Uzanto
from .serializers import UzantoSerializer
from itertools import chain


class UzantoView(viewsets.ReadOnlyModelViewSet):
    queryset = Uzanto.objects.annotate(avataro_bildo=F('uzantojavataro__bildo')).all()
    serializer_class = UzantoSerializer
    # lookup_field = 'id'


class MiView(viewsets.ReadOnlyModelViewSet):
    """
    Returns the data of the current user.
    Requires authentication.
    """
    queryset = Uzanto.objects.annotate(avataro_bildo=F('uzantojavataro__bildo'))
    serializer_class = UzantoSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.get(**{self.lookup_field: self.request.user.id})
        return queryset

    def get_object(self):
        return self.get_queryset()
