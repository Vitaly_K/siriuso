from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class KodoConfig(AppConfig):
    name = 'kodo'
    verbose_name = _('Kodo')

    # def ready(self):
    #     import Kodo.signals
