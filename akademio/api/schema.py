"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

import graphene
from graphene_django import DjangoObjectType
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions
from graphene_permissions.permissions import AllowAny
from django.db.models import Q, F, ForeignKey, Value, CharField
from django.utils.translation import ugettext_lazy as _
from siriuso.api.filters import SiriusoFilterConnectionField
from siriuso.api.types import SiriusoLingvo
from ..models import *

from siriuso.utils import lingvo_kodo_normaligo, get_lang_kodo, perms

from versioj.models import VersioAkademioPagxo
from versioj.api.schema import VersioAkademioPagxoNode


class AkademioKategorioTipoNode(SiriusoAuthNode, DjangoObjectType):
    """
    Тип категории академии
    """
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains', ]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование типа категории'))

    class Meta:
        model = AkademioKategorioTipo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)


class AkademioKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    """
    Категория академии
    """
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование категории'))
    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание категории'))

    class Meta:
        model = AkademioKategorio
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'tipo__kodo': ['exact', 'icontains', 'istartswith'],
            'tipo__uuid': ['exact'],
        }
        interfaces = (graphene.relay.Node,)

class AkademioPagxoTipoNode(SiriusoAuthNode, DjangoObjectType):
    """
    Тип страницы академии
    """
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains', ]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование типа страницы'))

    class Meta:
        model = AkademioPagxoTipo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)


class AkademioPagxoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    """
    Страница академии
    """
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'teksto__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    teksto = graphene.Field(SiriusoLingvo, description=_('Текст страницы академии'))
    nomo = graphene.Field(SiriusoLingvo, description=_('Название страницы академии'))
    versioj = SiriusoFilterConnectionField(VersioAkademioPagxoNode, description=_('Версии страницы'))

    class Meta:
        model = AkademioPagxo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'tipo__kodo': ['exact', 'icontains', 'istartswith'],
            'tipo__uuid': ['exact'],
            'kategorio__id': ['exact'],
            'kategorio__uuid': ['exact'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_versioj(self, info, **kwargs):
        user = info.context.user
        model = VersioAkademioPagxo

        perm_name = 'versioj.povas_vidi_akademion_pagxon_version'
        lingvo = lingvo_kodo_normaligo(get_lang_kodo(info.context))

        if user.has_perm(perm_name, self):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        if ((user.is_authenticated and perms.has_registrita_perm(perm_name))
                or (not user.is_authenticated and perms.has_neregistrita_perm(perm_name))):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        return model.objects.none()


class AkademioQuery(graphene.ObjectType):
    akademioj_kategorio_tipoj = SiriusoFilterConnectionField(AkademioKategorioTipoNode,
                                                          description=_('Выводит все доступные типы категорий Академии'))
    akademioj_kategorioj = SiriusoFilterConnectionField(AkademioKategorioNode,
                                                    description=_('Выводит все доступные категории Академии'))
    akademioj_pagxoj_tipoj = SiriusoFilterConnectionField(AkademioPagxoTipoNode,
                                                          description=_('Выводит все доступные типы страниц Академии'))
    akademioj_pagxoj = SiriusoFilterConnectionField(AkademioPagxoNode,
                                                    description=_('Выводит все доступные страницы Академии'))
