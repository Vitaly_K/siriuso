from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class AkademioConfig(AppConfig):
    name = 'akademio'
    verbose_name = _('Akademio')

    # def ready(self):
    #     import Akademio.signals
