"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from django import forms
from django.utils.translation import ugettext_lazy as _
from siriuso.utils import get_enhavo, set_enhavo
from akademio.models import *
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
import json


class TekstoMixin:
    @staticmethod
    def teksto(obj, field):
        kampo = getattr(obj, field)

        if kampo is not None:
            result = get_enhavo(kampo)

            if result:
                return result[0]
        return ''

    def nomo_teksto(self, obj):
        return self.teksto(obj=obj, field='nomo')

    def teksto_teksto(self, obj):
        return self.teksto(obj=obj, field='teksto')


# Форма для многоязычных названий типов страниц Академии
class AkademioPagxoTipoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=True)

    class Meta:
        model = AkademioPagxoTipo
        fields = [field.name for field in AkademioPagxoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы страниц Академии
@admin.register(AkademioPagxoTipo)
class AkademioPagxoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = AkademioPagxoTipoFormo
    list_display = ('nomo_teksto', 'kodo', 'uuid')
    exclude = ('nomo_teksto',)

    class Meta:
        model = AkademioPagxoTipo


# Форма для многоязычного контента страниц Академии
class AkademioPagxoFormo(forms.ModelForm):
    teksto = forms.CharField(widget=LingvoInputWidget(), label=_('Teksto'), required=True)

    class Meta:
        model = AkademioPagxo
        fields = [field.name for field in AkademioPagxo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_teksto(self):
        out = self.cleaned_data['teksto']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Страницы Академии
@admin.register(AkademioPagxo)
class AkademioPagxoAdmin(admin.ModelAdmin, TekstoMixin):
    form = AkademioPagxoFormo
    list_display = ('id', 'kodo', 'teksto_teksto')
    exclude = ('id',)

    class Meta:
        model = AkademioPagxo
