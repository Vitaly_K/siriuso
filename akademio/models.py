"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

import sys
import random, string
from django.db import models
from django.db.models import Q, Max
from django.utils.translation import ugettext_lazy as _
from main.models import SiriusoBazaAbstrakta, SiriusoBazaAbstrakta2, SiriusoBazaAbstraktaKomunumoj, \
    SiriusoKomentoAbstrakta, SiriusoTipoAbstrakta, Uzanto, SiriusoAutomataKreinto
from django.urls import reverse
from django.contrib.postgres import fields as pgfields
from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from django.contrib.auth.models import Permission

import functools


# Академия

# Типы категорий Академии, использует абстрактный класс SiriusoTipoAbstrakta
class AkademioKategorioTipo(SiriusoTipoAbstrakta):

    # многоязычное название в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'akademio_kategorioj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de kategorioj akademio')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de kategorioj akademio')
        # права
        permissions = (
            ('povas_vidi_kategorian_tipon', _('Povas vidi kategorian tipon')),
            ('povas_krei_kategorian_tipon', _('Povas krei kategorian tipon')),
            ('povas_forigi_kategorian_tipon', _('Povas forigu kategorian tipon')),
            ('povas_shanghi_kategorian_tipon', _('Povas ŝanĝi kategorian tipon')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()


# Категории Академии
class AkademioKategorio(SiriusoBazaAbstraktaKomunumoj):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # тип категории Академии
    tipo = models.ForeignKey(AkademioKategorioTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # код
    kodo = models.CharField(_('Kodo'), max_length=32)

    # название
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание в таблице описаний категории, от туда будет браться описание с нужным языковым тегом
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # администраторы категории
    administrantoj = models.ManyToManyField('main.Uzanto', verbose_name=_('Akademio administrantoj'),
                                            related_name='akademiokategorio_administrantoj')

    # модераторы категории
    moderatoroj = models.ManyToManyField('main.Uzanto', verbose_name=_('Akademio moderatoroj'),
                                         related_name='akademiokategorio_moderatoroj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'akademio_kategorioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio akademio')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj akademio')
        # права
        permissions = (
            ('povas_vidi_kategorion', _('Povas vidi kategorion')),
            ('povas_krei_kategorion', _('Povas krei kategorion')),
            ('povas_forigi_kategorion', _('Povas forigu kategorion')),
            ('povas_shanghi_kategorion', _('Povas ŝanĝi kategorion')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле id этой модели
        return '{}'.format(self.id)

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(AkademioKategorio, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if self.autoro == user_obj:
                all_perms = set(perms.user_registrita_perms(apps=('akademio',)))
            else:
                all_perms = set()

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'akademio.povas_vidi_kategorion', 'akademio.povas_krei_kategorion',
                'akademio.povas_forigi_kategorion', 'akademio.povas_shanghi_kategorion',
                'akademio.povas_krei_pagxan'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='akademio', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('akademio.povas_vidi_kategorion')
                    or user_obj.has_perm('akademio.povas_vidi_kategorion')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('akademio.povas_vidi_kategorion'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond

# Типы страниц Академии, использует абстрактный класс SiriusoTipoAbstrakta
class AkademioPagxoTipo(SiriusoTipoAbstrakta):

    # название многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'akademio_pagxo_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de pagxoj de akademio')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de pagxoj de akademio')
        # права
        permissions = (
            ('povas_vidi_pagxan_tipon', _('Povas vidi pagxan tipon')),
            ('povas_krei_pagxan_tipon', _('Povas krei pagxan tipon')),
            ('povas_forigi_pagxan_tipon', _('Povas forigu pagxan tipon')),
            ('povas_shanghi_pagxan_tipon', _('Povas ŝanĝi pagxan tipon')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()


# Страницы Академии
class AkademioPagxo(SiriusoBazaAbstraktaKomunumoj):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # тип страниц Академии
    tipo = models.ForeignKey(AkademioPagxoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # код
    kodo = models.CharField(_('Kodo'), max_length=32)

    # название
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # текст многоязычный в JSON формате
    teksto = pgfields.JSONField(verbose_name=_('Teksto'), blank=True, null=False, default=default_lingvo,
                                encoder=CallableEncoder)

    # категория
    kategorio = models.ManyToManyField(AkademioKategorio, verbose_name=_('Kategorio'),
                                  related_name='akademiopagxo_kategorio')

    # администраторы страницы
    administrantoj = models.ManyToManyField('main.Uzanto', verbose_name=_('akademio administrantoj'),
                                            related_name='akademiopagxo_administrantoj')

    # модераторы страницы
    moderatoroj = models.ManyToManyField('main.Uzanto', verbose_name=_('akademio moderatoroj'),
                                         related_name='akademiopagxo_moderatoroj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'akademio_pagxo'
        # читабельное название модели, в единственном числе
        verbose_name = _('Pagxo de akademio')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Pagxoj de akademio')
        # права
        permissions = (
            ('povas_vidi_pagxan', _('Povas vidi pagxan')),
            ('povas_krei_pagxan', _('Povas krei pagxan')),
            ('povas_forigi_pagxan', _('Povas forigu pagxan')),
            ('povas_shanghi_pagxan', _('Povas ŝanĝi pagxan')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле id этой модели
        return '{}'.format(self.id)

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(AkademioPagxo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                        update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if self.autoro == user_obj:
                all_perms = set(perms.user_registrita_perms(apps=('akademio',)))
            else:
                all_perms = set()

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'akademio.povas_vidi_pagxan', 'akademio.povas_krei_pagxan',
                'akademio.povas_forigi_pagxan', 'akademio.povas_shanghi_pagxan'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='akademio', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('akademio.povas_vidi_pagxan')
                    or user_obj.has_perm('akademio.povas_vidi_pagxan')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('akademio.povas_vidi_kategorion'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
