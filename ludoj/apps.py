from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class LudojConfig(AppConfig):
    name = 'ludoj'
    verbose_name = _('Ludoj')
