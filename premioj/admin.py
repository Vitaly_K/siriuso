"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from .models import *


# Типы наград
@admin.register(PremiojTipo)
class PremiojTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'publikigo', 'kodo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = PremiojTipo


# Степени наград
@admin.register(PremiojGrado)
class PremiojGradoAdmin(admin.ModelAdmin):
    list_display = ('grado', 'publikigo', 'uuid')

    class Meta:
        model = PremiojGrado


# Виды наград
@admin.register(PremiojSpeco)
class PremiojSpecoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'publikigo', 'uuid')
    exclude = ('nomo', 'priskribo')

    class Meta:
        model = PremiojSpeco


# Перечень наград (справочник наград со всеми характеристиками, ими будет награждение)
@admin.register(PremiojPremio)
class PremiojPremioAdmin(admin.ModelAdmin):
    list_display = ('speco', 'tipo', 'grado', 'publikigo', 'uuid')

    class Meta:
        model = PremiojPremio


# Условия награждения (справочник)
@admin.register(PremiojKondicho)
class PremiojKondichoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'publikigo', 'uuid')
    exclude = ('nomo', 'priskribo')

    class Meta:
        model = PremiojKondicho


# Награждение (перечень всех присвоений наград пользователям)
@admin.register(PremiojPremiado)
class PremiojPremiadoAdmin(admin.ModelAdmin):
    list_display = ('premiita', 'premio', 'publikigo', 'kondicho')

    class Meta:
        model = PremiojPremiado
