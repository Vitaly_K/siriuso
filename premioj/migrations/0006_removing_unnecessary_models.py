# Generated by Django 2.0.9 on 2019-04-10 06:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('premioj', '0005_lingvo_json'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='premiojkondichonomo',
            name='lingvo',
        ),
        migrations.RemoveField(
            model_name='premiojkondichonomo',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='premiojkondichopriskribo',
            name='lingvo',
        ),
        migrations.RemoveField(
            model_name='premiojkondichopriskribo',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='premiojspeconomo',
            name='lingvo',
        ),
        migrations.RemoveField(
            model_name='premiojspeconomo',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='premiojspecopriskribo',
            name='lingvo',
        ),
        migrations.RemoveField(
            model_name='premiojspecopriskribo',
            name='posedanto',
        ),
        migrations.RemoveField(
            model_name='premiojtiponomo',
            name='lingvo',
        ),
        migrations.RemoveField(
            model_name='premiojtiponomo',
            name='posedanto',
        ),
        migrations.DeleteModel(
            name='PremiojKondichoNomo',
        ),
        migrations.DeleteModel(
            name='PremiojKondichoPriskribo',
        ),
        migrations.DeleteModel(
            name='PremiojSpecoNomo',
        ),
        migrations.DeleteModel(
            name='PremiojSpecoPriskribo',
        ),
        migrations.DeleteModel(
            name='PremiojTipoNomo',
        ),
    ]
