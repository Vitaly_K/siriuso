# Generated by Django 2.0.13 on 2019-07-17 09:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_realecoj'),
        ('premioj', '0007_auto_20190627_1356'),
    ]

    operations = [
        migrations.AddField(
            model_name='premiojgrado',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='premiojgrado',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AddField(
            model_name='premiojkondicho',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='premiojkondicho',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AddField(
            model_name='premiojpremiado',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='premiojpremiado',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AddField(
            model_name='premiojpremio',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='premiojpremio',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AddField(
            model_name='premiojspeco',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='premiojspeco',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AddField(
            model_name='premiojtipo',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoRealeco', verbose_name='Realeco'),
        ),
        migrations.AddField(
            model_name='premiojtipo',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.SiriusoWablono', verbose_name='Ĝenerala ŝablono'),
        ),
    ]
