"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.shortcuts import render, redirect, get_object_or_404
from django.http.response import Http404, JsonResponse
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from .models import *
from informiloj.models import InformilojSciigoTipo
from django.template.response import TemplateResponse
from django.views.decorators.csrf import requires_csrf_token, csrf_protect
from datetime import datetime
from django.db.models import Count
from main.models import SiriusoSlugo
from siriuso.views import SiriusoTemplateView
from django.db.models import F
from itertools import chain


# Страница сообществ
class Premio(SiriusoTemplateView):
    template_name = 'premioj/premioj_listo.html'
    mobile_template_name = 'premioj/portebla/t_premioj_listo.html'

    http_method_names = ['get']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        premioj = PremiojPremiado.objects \
            .filter(krea_dato__lte=timezone.now(), publikigo=True, arkivo=False, forigo=False) \
            .order_by('krea_dato').values('uuid', 'premiita', 'premiita__id',
                                          'premiita__unua_nomo__enhavo', 'premiita__familinomo__enhavo',
                                          'premio__bildo_min', 'premio__bildo_info', 'premio__speco__nomo__enhavo',
                                          'premio__tipo__nomo__enhavo', 'premio__grado__grado')

        context.update({'premioj': premioj})

        return context