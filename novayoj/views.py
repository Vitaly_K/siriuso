"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.shortcuts import render, redirect, get_object_or_404
from django.http.response import Http404, JsonResponse
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from muroj.models import *
from komunumoj.models import *
from informiloj.models import InformilojSciigoTipo
from django.template.response import TemplateResponse
from django.views.decorators.csrf import requires_csrf_token, csrf_protect
from datetime import datetime
from django.db.models import Count, Q
from django.utils.decorators import method_decorator
from main.models import SiriusoSlugo
from siriuso.views import SiriusoTemplateView
from siriuso.views.mixins import SiriusoPaginationMixin
from uzantoj.templatecontexts.uzanto import get_kamaradoj_id
from django.db.models import TextField, IntegerField, BooleanField, F, Value
from django.db.models.functions import Concat
from itertools import chain
import pytz
import json


# Страница сообществ
@method_decorator(login_required, name='dispatch')
class NovayojView(SiriusoTemplateView, SiriusoPaginationMixin):
    template_name = 'novayoj/novayoj_muro.html'
    mobile_template_name = 'novayoj/portebla/t_novayoj_muro.html'

    ajax_template = 'novayoj/novayoj_enskribo.html'
    mobile_ajax_teplate = 'novayoj/portebla/t_novayoj_enskribo.html'
    page_max = 15

    http_method_names = ['get', 'post']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        def make_query(obj, membro_obj, interese_obj, avataro, avataro_uzanto, bildo, komento_tuta,
                       kom_tipo, user):
            # Получаем список нужных Сообществ
            if obj.__name__ != 'MurojUzantoEnskribo':
                # Ищем подписки пользователя к Сообществу
                membro_kom_list = (membro_obj.objects
                                   .filter(autoro=self.request.user, forigo=False)
                                   .values_list('posedanto_id'))
                # Условие подписки пользователя на Сообщество или пометка Сообщества как "Важное"
                condition = (Q(muro__posedanto_id__in=membro_kom_list) | Q(muro__posedanto__grava=True))
                interese_q = Q(autoro=user)
            else:
                # Ищем список Товарищей пользователя
                membro_kom_list = get_kamaradoj_id(user)
                condition = Q(muro__posedanto_id__in=membro_kom_list, posedanto__is_active=True)
                interese_q = Q(posedanto=user)

            # Дата последней опубликованной на предыдущей странице записи этого же типа
            last_rec = timezone.now()
            start = self.page_offset
            last_rec2 = 2

            # Если переданы данные о последней опубликованной на предыдущей сранице записи
            if self.page_multi is not None and obj.__name__ in self.page_multi:
                rec = self.page_multi.get(obj.__name__)
                last_rec = datetime.strptime(rec.get('publikiga_dato')[0:26], '%Y-%m-%d %H:%M:%S.%f')
                last_rec = timezone.make_aware(last_rec, pytz.UTC)
                start = 0

            end = start + self.page_max + 1

            # Делаем запрос, исходя из типа Стены
            if obj.__name__ != 'MurojUzantoEnskribo':
                # Если это не Стена пользователя
                query = (obj.objects
                         .filter(condition, forigo=False, arkivo=False, publikiga_dato__lt=last_rec)
                         .select_related('teksto', 'posedanto', 'autoro')
                         .annotate(avataro_bildo_min=avataro,
                                   avataro_uzanto_bildo_min=avataro_uzanto,
                                   bildo=bildo,
                                   teksto_enhavo=F('teksto__enhavo'),
                                   autoro_sekso=F('autoro__sekso'),
                                   posedanto_nomo_enhavo=F('posedanto__nomo__enhavo'),
                                   autoro_unua_nomo_enhavo=F('autoro__unua_nomo__enhavo'),
                                   autoro_familinomo_enhavo=F('autoro__familinomo__enhavo'),
                                   komentoj_tuta=komento_tuta,
                                   interese_tuta=Value(0, output_field=IntegerField()),
                                   uzanto_interese=Value(0, output_field=IntegerField()),
                                   kom_tipo=kom_tipo)
                         .only('uuid', 'id', 'forigo', 'publikiga_dato', 'autoro_id', 'autoro_montri', 'teksto__enhavo',
                              'posedanto__nomo__enhavo', 'autoro__unua_nomo__enhavo', 'autoro__familinomo__enhavo', 'video',
                              'autoro__sekso')
                         .order_by(F('publikiga_dato').desc(nulls_last=True)))[start:end]
            else:
                # Если это Стена пользователя (отличается структура модели)
                query = (obj.objects
                         .filter(condition, forigo=False, arkivo=False, publikiga_dato__lt=last_rec)
                         .select_related('teksto', 'posedanto')
                         .only('uuid', 'id', 'forigo', 'publikiga_dato', 'posedanto_id', 'teksto__enhavo',
                               'posedanto__unua_nomo__enhavo', 'posedanto__familinomo__enhavo', 'video',
                               'posedanto__sekso')
                         .annotate(avataro_bildo_min=avataro,
                                   avataro_uzanto_bildo_min=avataro_uzanto,
                                   bildo=bildo,
                                   teksto_enhavo=F('teksto__enhavo'),
                                   autoro_sekso=F('posedanto__sekso'),
                                   posedanto_nomo_enhavo=Concat(F('posedanto__unua_nomo__enhavo'), Value(' '),
                                                                F('posedanto__familinomo__enhavo')),
                                   autoro_unua_nomo_enhavo=F('posedanto__unua_nomo__enhavo'),
                                   autoro_familinomo_enhavo=F('posedanto__familinomo__enhavo'),
                                   interese_tuta=Value(0, output_field=IntegerField()),
                                   uzanto_interese=Value(0, output_field=IntegerField()),
                                   komentoj_tuta=komento_tuta,
                                   posedanto_montri=Value(False, output_field=BooleanField()),
                                   kom_tipo=kom_tipo)
                         .order_by(F('publikiga_dato').desc(nulls_last=True)))[start:end]
            # Получаем статистику пометки "Интересно" для списка записей Стен
            interese_tuta = list(interese_obj.objects
                                 .filter(forigo=False, enskribo__in=query)
                                 .values('enskribo_id')
                                 .annotate(tuta=Count('enskribo_id')))
            # Если пользователь авторизирован, то получааем статистику его пометок
            # "Интересно" по списку записей Стен
            if user.is_authenticated:
                uzanto_interese = list(interese_obj.objects
                                       .filter(interese_q, forigo=False, enskribo__in=query)
                                       .values('enskribo_id')
                                       .annotate(tuta=Count('enskribo_id')))
            else:
                uzanto_interese = []

            # Раскидываем статистику по Записям Стен
            for cur in query:
                for i in range(len(interese_tuta)):
                    if interese_tuta[i]['enskribo_id'] == cur.uuid:
                        cur.interese_tuta = interese_tuta.pop(i)['tuta']
                        break

                for i in range(len(uzanto_interese)):
                    if uzanto_interese[i]['enskribo_id'] == cur.uuid:
                        cur.uzanto_interese = uzanto_interese.pop(i)['tuta']
                        break

            return query

        sociaprojektoj_dict = {
            'avataro': F('posedanto__komunumojsociaprojektoavataro__bildo_min'),
            'avataro_uzanto': F('autoro__uzantojavataro__bildo_min'),
            'bildo': F('murojsociaprojektoenskribobildo__bildo'),
            'kom_tipo': Value('projekto', output_field=TextField()),
            'komento_tuta': Count(Q(murojsociaprojektoenskribokomento__forigo=False)),
            'user': self.request.user
        }
        sociaprojektoj = make_query(MurojSociaprojektoEnskribo, KomunumojSociaprojektoMembro,
                                    MurojSociaprojektoEnskriboInterese, **sociaprojektoj_dict)

        sovetoj_dict = {
            'avataro': F('posedanto__komunumojsovetoavataro__bildo_min'),
            'avataro_uzanto': F('autoro__uzantojavataro__bildo_min'),
            'bildo': F('murojsovetoenskribobildo__bildo'),
            'kom_tipo': Value('soveto', output_field=TextField()),
            'komento_tuta': Count(Q(murojsovetoenskribokomento__forigo=False)),
            'user': self.request.user
        }
        sovetoj = make_query(MurojSovetoEnskribo, KomunumojSovetoMembro,
                             MurojSovetoEnskriboInterese, **sovetoj_dict)

        organizoj_dict = {
            'avataro': F('posedanto__komunumojorganizoavataro__bildo_min'),
            'avataro_uzanto': F('autoro__uzantojavataro__bildo_min'),
            'bildo': F('murojorganizoenskribobildo__bildo'),
            'kom_tipo': Value('organizo', output_field=TextField()),
            'komento_tuta': Count(Q(murojorganizoenskribokomento__forigo=False)),
            'user': self.request.user
        }
        organizoj = make_query(MurojOrganizoEnskribo, KomunumojOrganizoMembro,
                               MurojOrganizoEnskriboInterese, **organizoj_dict)

        grupoj_dict = {
            'avataro': F('posedanto__komunumojgrupoavataro__bildo_min'),
            'avataro_uzanto': F('autoro__uzantojavataro__bildo_min'),
            'bildo': F('murojgrupoenskribobildo__bildo'),
            'kom_tipo': Value('grupo', output_field=TextField()),
            'komento_tuta': Count(Q(murojgrupoenskribokomento__forigo=False)),
            'user': self.request.user
        }
        grupoj = make_query(MurojGrupoEnskribo, KomunumojGrupoMembro,
                            MurojGrupoEnskriboInterese, **grupoj_dict)

        uzantoj_dict = {
            'avataro': F('posedanto__uzantojavataro__bildo_min'),
            'avataro_uzanto': F('posedanto__uzantojavataro__bildo_min'),
            'bildo': F('murojuzantoenskribobildo__bildo'),
            'kom_tipo': Value('uzanto', output_field=TextField()),
            'komento_tuta': Count(Q(murojuzantoenskribokomento__forigo=False)),
            'user': self.request.user
        }
        uzantoj = make_query(MurojUzantoEnskribo, None, MurojUzantoEnskriboInterese, **uzantoj_dict)

        enskriboj = list(chain(sociaprojektoj, sovetoj, organizoj, grupoj, uzantoj))
        enskriboj.sort(key=lambda x: x.publikiga_dato, reverse=True)
        self.page_multi = json.dumps(self.multiple_last_objects(enskriboj, ['publikiga_dato']))

        context.update({
            'enskriboj': enskriboj[0:self.page_max],
            'novoj_multi': self.page_multi,
            'more': len(enskriboj) > self.page_max
        })

        if self.is_pagination:
            context['offset'] = len(enskriboj) - 1

        return context