"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.db import models
from django.db.models import Max
from django.contrib.postgres import fields as pgfield
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres import fields as pgfields
from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo
from main.models import SiriusoBazaAbstraktaKomunumoj
from main.models import SiriusoKomentoAbstrakta
from main.models import SiriusoTipoAbstrakta, Uzanto
from komunumoj.models import KomunumojAliro, KomunumojForumo
from komunumoj.models import Komunumo
from informiloj.models import InformilojSciigoTipo
import sys


# Функционал форумов групп
# Типы категорий тем (справочник), использует абстрактный класс SiriusoTipoAbstrakta
class ForumojKategorioTipo(SiriusoTipoAbstrakta):

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'forumoj_kategorioj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de kategorioj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de kategorioj')


# Категории тем групп, использует абстрактный класс SiriusoBazaAbstraktaKomunumoj
class ForumojKategorio(SiriusoBazaAbstraktaKomunumoj):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # владелец (группа)
    posedanto = models.ForeignKey(Komunumo, verbose_name=_('Posedanto'), blank=True, null=True, default=None,
                                  on_delete=models.SET_NULL)
    # форум-владелец
    forumo = models.ForeignKey(KomunumojForumo, verbose_name=_('Mastro forumon'), blank=True,
                               null=True, on_delete=models.CASCADE)

    # тип категорий
    tipo = models.ForeignKey(ForumojKategorioTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название в таблице названий категорий, от туда будет браться название с нужным языковым тегом
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание в таблице описаний категории, от туда будет браться описание с нужным языковым тегом
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # доступ к категории
    aliro = models.ForeignKey(KomunumojAliro, verbose_name=_('Aliro'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'forumoj_kategorioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj')

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(ForumojKategorio, self).save(force_insert=force_insert, force_update=force_update,
                                           using=using, update_fields=update_fields)


# Типы тем форума (справочник), использует абстрактный класс SiriusoTipoAbstrakta
class ForumojTemoTipo(SiriusoTipoAbstrakta):

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = pgfields.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'forumoj_temoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de temoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de temoj')


# Темы форумов групп, использует абстрактный класс SiriusoBazaAbstraktaKomunumoj
class ForumojTemo(SiriusoBazaAbstraktaKomunumoj):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # владелец (группа)
    posedanto = models.ForeignKey(Komunumo, verbose_name=_('Posedanto'), blank=True, null=True, default=None,
                                  on_delete=models.CASCADE)

    # категория
    kategorio = models.ForeignKey(ForumojKategorio, verbose_name=_('Kategorio'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # тип темы
    tipo = models.ForeignKey(ForumojTemoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название в таблице названий тем, от туда будет браться название с нужным языковым тегом
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # текст в таблице текстов тем, от туда будет браться текст с нужным языковым тегом
    teksto = pgfields.JSONField(verbose_name=_('Teksto'), blank=True, null=False, default=default_lingvo,
                                encoder=CallableEncoder)

    # тема закрыта (да или нет)
    fermita = models.BooleanField(_('Fermita'), blank=False, default=False)

    # кому разрешено писать в закрытую тему
    komentado_aliro = models.ForeignKey(KomunumojAliro, verbose_name=_('Komentada aliro'), blank=False, default=None,
                                        on_delete=models.CASCADE)

    # тема закреплена (да или нет)
    fiksa = models.BooleanField(_('Fiksa'), blank=False, default=False)

    # позиция среди закреплённых тем
    fiksa_listo = models.IntegerField(_('Fiksa listo'), blank=True, null=True)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'forumoj_temoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Temo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Temoj')

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(ForumojTemo, self).save(force_insert=force_insert, force_update=force_update,
                                      using=using, update_fields=update_fields)


# Комментарии тем форумов групп, использует абстрактный класс SiriusoKomentoAbstrakta
class ForumojTemoKomento(SiriusoKomentoAbstrakta):

    # владелец (тема)
    posedanto = models.ForeignKey(ForumojTemo, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # текст в таблице текстов комментариев тем, от туда будет браться текст с нужным языковым тегом
    teksto = pgfields.JSONField(verbose_name=_('Teksto'), blank=True, null=False, default=default_lingvo,
                                encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'forumoj_temoj_komentoj'


# Таблица истории посещений пользователями тем форумов
class ForumojUzantoHistorio(models.Model):
    # поле связи 1к1, одновременно и PK
    posedanto = models.OneToOneField(Uzanto, on_delete=models.CASCADE, primary_key=True)

    # JSON поле с ситорией посещений тем форумов
    historio = pgfield.JSONField(verbose_name=_('История посещений'), default=dict, blank=True)

    class Meta:
        db_table = 'forumoj_uzantoj_historioj'


# Таблица подписок пользователей на Темы форумов
class ForumojSciigoj(SiriusoBazaAbstraktaKomunumoj):
    # связь с темой форумов
    temo = models.ForeignKey(ForumojTemo, on_delete=models.CASCADE, blank=False, null=False)

    # тип способа уведомления
    sciigoj = models.ManyToManyField(InformilojSciigoTipo, verbose_name=_('Sciigo (muro)'), blank=True)
