from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ForumojConfig(AppConfig):
    name = 'forumoj'
    verbose_name = _('Forumoj')

    def ready(self):
        import forumoj.signals