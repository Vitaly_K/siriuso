$(document).ready(function($){
    let forms = [];

    moment.locale(window.navigator.userLanguage || window.navigator.language);
    forumoj_init();

    function forumoj_init() {
        forms['komento_formo'] = $('.komento-formo');
        $('form').on('submit', forumoj_submit)
            .on('reset', function (e) {
                forms['komento_formo'][0].reset();
                forms['komento_formo'].find('input[komento_id]').remove();
                forms['komento_formo'].find('input[name="ago"]').val('komento');
                forms['komento_formo'].find('input[name="komento_id"]').remove();

                for(instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].setData('');
                }

                let blocked = $('.forumoj_temo').find('li .formo-extere').parent('li');
                toggleBlockForm(blocked);
            })
            // Сбрасываем состояние редактирования формы при перезагрузке страницы
            .trigger('reset');

        $('[data-ago]:not([data-ago="redaktu"],[data-ago="temo_sciigoj"])').on('click', forumoj_post_action);
        $('[data-ago="redaktu"]').on('click', forumoj_edit);
        $('[data-ago="temo_sciigoj"]').on('click', forumoj_sciigoj);
    }

    //Подписка на тему форума
    function forumoj_sciigoj(e) {
        let target = $(e.target);

        if(!target.attr('disabled')) {
            target.attr('disabled', true);
            target.prepend($('<i class="fa fa-spinner fa-spin fa-rotate-right" style="color:black;"></i>'));

            let sciigoj = target.hasClass('malaboni') ? [] : ['powto'];
            let data = {
                csrfmiddlewaretoken: getCookie('csrftoken'),
                query: 'mutation($sciigoj: [String]!){\n' +
                '  aplikiForumojSciigojn(id: 1, sciigoj: $sciigoj) {\n' +
                '    status,\n' +
                '    message,\n' +
                '    sciigoj\n' +
                '  }\n' +
                '}',
                variables: JSON.stringify({'sciigoj': sciigoj})
            };

            $.ajax({
                url: '/api/v1.1/',
                type: 'POST',
                async: true,
                cache: false,
                processData: true,
                dataType: 'json',
                data: data
            })
                .done(function (response) {
                    let resp = response.data.aplikiForumojSciigojn;

                    if(resp.status) {
                        setTimeout(function () {
                            target.attr('disabled', false);
                            target.find('i.fa').remove();
                            if(resp.sciigoj.length) {
                                target.parent().find('.malaboni').removeClass('hidden-element');
                                target.parent().find('.aboni').addClass('hidden-element');
                            } else {
                                target.parent().find('.aboni').removeClass('hidden-element');
                                target.parent().find('.malaboni').addClass('hidden-element');
                            }
                        }, 500);
                    } else {
                        console.log('Request failed: ' + resp.message)
                    }
                })
                .fail(function (jqXHR, textStatus) {
                    console.log('Bad server response: ' + textStatus);
                    setTimeout(function () {
                        target.attr('disabled', false);
                        target.find('i.fa').remove();
                    }, 500)
                });
        }
    }


    // Подготовка формы к редактированию
    function forumoj_edit(e) {
        let target = $(e.target).parents('.forumoj_listo_komento_item');
        target = target.length ? target : $(e.target).parents('.forumoj_temo');

        if(target.prop('tagName') === 'DIV') {
            let temo_id = target.attr('data-temo_id');
            target = target.find('.forumoj_listo > li:first');
            target.attr('data-temo_id', temo_id);
        } else {
            let inf = forms['komento_formo'].find('input[name="komento_id"]');

            if(inf.length) {
                inf.val(target.attr('data-komento_id'));
            } else {
                $('<input type="hidden" name="komento_id" value="' + target.attr('data-komento_id') + '"/>')
                .appendTo(forms['komento_formo']);
            }
        }
        let blocked = target.parents('.forumoj_temo').find('li .formo-extere').parent('li');
        toggleBlockForm(blocked);
        toggleBlockForm(target);

        let teksto = target.find('.forumoj_listo_komento_item_priskribo, .forumoj_listo_item_priskribo').html();

        if(typeof CKEDITOR !== "undefined"){
            for(instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].setData(teksto);
            }
        } else {
            forms['komento_formo'].find('textarea').text(teksto);
        }

        forms['komento_formo'].find('input[name="ago"]').val('redaktu');
        scrollTo(forms['komento_formo']);
    }

    // AJAX редактирования к API
    function forumoj_edit_post(form) {
        let tipe = form.find('input[name="komento_id"]').length ? 'komento' : 'temo';

        let data = {
            csrfmiddlewaretoken: getCookie('csrftoken'),
        };


        if(tipe === 'komento') {
            data['query'] = 'mutation($uuid: UUID!, $teksto: String!) {\n' +
                '  redaktuForumoTemoKomento(uuid: $uuid, teksto: $teksto) {\n' +
                '    status, message, node {uuid, forigo, kreaDato, publikigaDato, teksto { enhavo }, ' +
                '    lastaAutoro {objId, unuaNomo {enhavo}, familinomo {enhavo}, uzantojavataroSet(chefaVarianto: true) ' +
                '    {edges{node{bildoMin}}}, sekso}, lastaDato}\n' +
                '  }\n' +
                '}';
            data['variables'] = JSON.stringify({
                uuid: form.find('input[name="komento_id"]').val(),
                teksto: form.find('textarea').val()
            });
        } else {
            data['query'] = 'mutation($uuid: UUID!, $nomo: String, $teksto: String) {\n' +
                '  redaktuForumoTemo(uuid: $uuid, nomo: $nomo, teksto: $teksto) {\n' +
                '    status, message, node {uuid, forigo, kreaDato, publikigaDato, teksto {\n' +
                '      enhavo}, nomo {enhavo}, lastaAutoro {objId, unuaNomo {enhavo}, familinomo {enhavo},' +
                '       uzantojavataroSet(chefaVarianto: true) {edges{node{bildoMin}}}, sekso},' +
                '      lastaDato}\n' +
                '  }\n' +
                '}';
            data['variables'] = JSON.stringify({
                uuid: form.find('input[name="temo"]').val(),
                nomo: null,
                teksto: form.find('textarea').val()
            });
        }

        $.ajax({
            url: '/api/v1.1/',
            type: 'POST',
            async: true,
            processData: true,
            dataType: 'json',
            cache: false,
            data: data
        })
            .done(function (response) {
                let resp = response.data.redaktuForumoTemoKomento || response.data.redaktuForumoTemo;

                if(resp.status) {
                    setTimeout(function () {
                        let target = (tipe === 'komento' ?
                                $('.forumoj_listo_komento_item[data-komento_id="' + resp.node.uuid + '"] ' +
                                    '.forumoj_listo_komento_item_priskribo')
                                :
                                $('.forumoj_komento_bloko .forumoj_listo_item_priskribo')
                        );

                        let format = moment.localeData().longDateFormat('LLL').replace(/,/, '');
                        let edit_datetime = moment(resp.node.lastaDato).format(format);
                        let avataro_url = resp.node.lastaAutoro.uzantojavataroSet.edges.length ?
                            '/media/' + resp.node.lastaAutoro.uzantojavataroSet.edges[0].node.bildoMin :
                            (resp.node.lastaAutoro.sekso === "vira" ? 'main/images/vira-avataro-min.png':
                            'main/images/virina-avataro-min.png');

                        let redaktu = $('<span class="fa fa-pencil"></span><a href="/idu' + resp.node.lastaAutoro.objId +
                            '"><img src="' + avataro_url + '">&nbsp;' +
                            resp.node.lastaAutoro.unuaNomo.enhavo + '&nbsp;' + resp.node.lastaAutoro.familinomo.enhavo +
                            '</a>'+ '&nbsp;<span>' + edit_datetime + '</span>');

                        target.html(resp.node.teksto.enhavo);
                        target.parent().find('.forumoj_listo_komento_item_redaktu').html('')
                            .append(redaktu);

                        scrollTo(target.parents().find(
                            tipe === 'komento' ? 'li[data-komento_id="' + resp.node.uuid + '"]' : '.forumoj_temo_top')
                        );
                        form.find('button[type="reset"]').trigger('reset');
                        toggleBlockForm(form);
                    }, 500);
                } else {
                    console.log("Bad server response: " + response.message);
                    form.find('button[type="reset"]').trigger('reset');
                    toggleBlockForm(form);
                }
            })
            .fail(function (jqXHR, textStatus){
                console.log("Request failed: " + textStatus);
                form.find('button[type="reset"]').trigger('reset');
                toggleBlockForm(form);
            });
    }

    function forumoj_toggle_class(obj, class_name) {
        $(obj).toggleClass(class_name);
    }

    function forumoj_get_action(obj) {
        let target = $(obj).find('input[type="hidden"][name="ago"]');

        if(target.length) {
            return target.length > 1 ? target[0].val() : target.val();
        }

        return null;
    }

    function forumoj_submit() {
        if(typeof CKEDITOR !== "undefined"){
            for(instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
        }

        let target = $(this);
        let type = forumoj_get_action(target);

        switch (type) {
            case 'temo':
            case 'komento':
                target.find('input, textarea, select, button').prop('disabled', true);
                toggleBlockForm(target);
                forumoj_post(target);
                target.find('input, textarea, select, button').prop('disabled', false);
                return false;
            case 'redaktu':
                toggleBlockForm(target);
                forumoj_edit_post(target);
                return false;
            default:
                return true;
        }
    }

    function forumoj_post(form) {
        let data = prepare_form(form);

        $.ajax({
            url: window.location.href,
            type: 'POST',
            async: true,
            data: data,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false
        })
            .done(function (result) {
                if(result.status === 'ok') {
                    setTimeout(forumoj_insert, 500, result, form);
                } else {
                    console.log("Bad server response: " + result.err);
                    toggleBlockForm(form);
                }
            })
            .fail(function (jqXHR, textStatus) {
                toggleBlockForm(form);
                console.log("Request failed: " + textStatus);
            });
    }

    function forumoj_post_action(e) {
        let target = $(e.target);

        if(target.attr('data-ago') === 'forigi' && !confirm(l10n.kom_forigi)) {
            return;
        }

        let form = target.parents('li');
        let data = new FormData();

        data.append('kom_uuid', target.parents('[data-komento_id]').attr('data-komento_id'));
        data.append('ago', target.attr('data-ago'));
        data.append('csrfmiddlewaretoken', getCookie('csrftoken'));

        toggleBlockForm(form, 48);

        $.ajax({
            url: window.location.href,
            type: 'POST',
            async: true,
            data: data,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false
        })
            .done(function (result){
                if(result.status === 'ok') {
                    forumoj_before_remove(form);
                    setTimeout(function () {
                        toggleBlockForm(form);
                        form.addClass('forumoj_hidden');
                        form.one('transitionend', function() {form.remove()});
                    }, 500);
                } else {
                    console.log("Bad server response: " + result.err);
                    toggleBlockForm(form);
                }
            })
            .fail(function (jqXHR, textStatus) {
                console.log("Request failed: " + textStatus);
                toggleBlockForm(form);
            });
    }

    function prepare_form(form) {
        if(typeof form === 'object') {
            out = new FormData();
            fields = $(form).find('input, textarea, select');

            fields.each(function (index, element) {
                let cur = $(element);
                if(cur.prop('type') === 'submit') {
                    return;
                } else if(cur.prop('type') === 'checkbox') {
                    cur.val(cur.prop('checked'));
                }

                out.append(cur.prop('name'), cur.val());
            });
            out.append('csrfmiddlewaretoken', getCookie('csrftoken'));

            return out;
        }
    }

    function forumoj_before_remove(content) {
        let jcontent = $(content);
        jcontent.css({'max-height': jcontent.outerHeight()});
    }

    function forumoj_insert(response, form) {
        if(typeof CKEDITOR !== "undefined"){
            for(instance in CKEDITOR.instances){
                CKEDITOR.instances[instance].setData('');
            }
        }

        let obj = forumoj_before_insert(response.html);

        if(form !== undefined) {
            form[0].reset();
        }

        toggleBlockForm(form);
        switch (response.type) {
            case 'temo':
                $('.feed-bl').last().after(obj);
                break;

            case 'komento':
                $('.forumoj_temo[data-temo_id=' + response.por_temo + '] .forumoj_listo_komento_bloko > ul').append(obj);
                break;

            default:
                return
        }

        forumoj_after_insert(obj)
    }

    function forumoj_before_insert(content) {
        let jcontent = $(content);
        jcontent.css({'position':'absolute', 'left': "-10000px"});
        $('body').append(jcontent);
        jcontent.css({'max-height': jcontent.outerHeight(), 'position' : '', 'left' : ''})
            .toggleClass('forumoj_hidden')
            .detach();
        return jcontent;
    }

    function forumoj_after_insert(obj) {
        obj.find('[data-ago]:not([data-ago="redaktu"])').on('click', forumoj_post_action);
        obj.find('[data-ago="redaktu"]').on('click', forumoj_edit);
        setTimeout(forumoj_toggle_class, 10, obj, 'forumoj_hidden');
        setTimeout(function(obj){obj.css('max-height','')}, 2000, obj);
    }
});

