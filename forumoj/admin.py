"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from .models import *


# Типы категорий тем форума
@admin.register(ForumojKategorioTipo)
class ForumojGrupoKategorioTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = ForumojKategorioTipo


# Категории тем форума
@admin.register(ForumojKategorio)
class ForumojGrupoKategorioAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'uuid')
    exclude = ('nomo', 'priskribo', 'id')

    class Meta:
        model = ForumojKategorio


# Типы тем форума
@admin.register(ForumojTemoTipo)
class ForumojGrupoTemoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = ForumojTemoTipo


# Темы форума
@admin.register(ForumojTemo)
class ForumojGrupoTemoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'uuid')
    exclude = ('nomo', 'teksto', 'id')

    class Meta:
        model = ForumojTemo


# Комментарии тем форума
@admin.register(ForumojTemoKomento)
class ForumojGrupoTemoKomentoAdmin(admin.ModelAdmin):
    list_display = ('uuid',)
    exclude = ('teksto',)

    class Meta:
        model = ForumojTemoKomento
