# Generated by Django 2.0.8 on 2018-08-21 04:47

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('forumoj', '0004_forumojkategorio_forumo'),
    ]

    operations = [
        migrations.AddField(
            model_name='forumojkategorio',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='forumoj_forumojkategorio_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='forumojkategorio',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='forumojtemo',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='forumoj_forumojtemo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='forumojtemo',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
        migrations.AddField(
            model_name='forumojtemokomento',
            name='lasta_autoro',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='forumoj_forumojtemokomento_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de'),
        ),
        migrations.AddField(
            model_name='forumojtemokomento',
            name='lasta_dato',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo'),
        ),
    ]
