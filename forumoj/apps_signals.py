"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""


import django.dispatch

# Сигнал о публикации нового комментария
foruma_komento_poshtita = django.dispatch.Signal(providing_args=['komento', 'ago_uzanto'])

# Сигнал об изменении опубликованного комментария
foruma_komento_modifita = django.dispatch.Signal(providing_args=['komento', 'ago_uzanto'])

# Сигнал об удалении комментария
foruma_komenti_forigita = django.dispatch.Signal(providing_args=['komento', 'ago_uzanto'])
