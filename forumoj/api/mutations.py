"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from forumoj.models import ForumojTemoKomento, ForumojSciigoj, ForumojTemo
from siriuso.api.mixins import SiriusoAuthMutation
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.db.models import Q, F
from .schema import *
import graphene


class AplikiForumajSciigojn(SiriusoAuthMutation, graphene.Mutation):
    class Arguments:
        uuid = graphene.UUID()
        id = graphene.Int()
        uzanto_id = graphene.Int()
        sciigoj = graphene.List(graphene.String, required=True)

    status = graphene.Boolean()
    message = graphene.String()
    sciigoj = graphene.List(graphene.String)

    @staticmethod
    def mutate(root, info, sciigoj, **kwargs):
        if not info.context.user.is_authenticated:
            return AplikiForumajSciigojn(status=False, message=_('Требуется авторизация'))

        status = False
        message = None
        # Тут надо будет доделать проверку прав текущего ползователя
        # изменять настройки других пользователей. Пока пользователь
        # может изменять свои настройки уведомлений
        uzanto_id = info.context.user.id

        if 'uuid' in kwargs:
            qq = Q(uuid=kwargs.get('uuid'))
        elif 'id' in kwargs:
            qq = Q(id=kwargs.get('id'))
        else:
            message = _('Не заданы обязательные параметры для определения сообщества: "uuid" или "id" темы')
            return AplikiForumajSciigojn(status=status, message=message)

        try:
            temo = ForumojTemo.objects.get(qq, forigo=False)
        except ForumojTemo.DoesNotExist:
            message = _('Не найдена тема с указанным "id"')
            return AplikiForumajSciigojn(status=status, message=message)

        try:
            nodo = ForumojSciigoj.objects.get(autoro_id=uzanto_id, temo=temo, forigo=False)
        except ForumojSciigoj.DoesNotExist:
            nodo = ForumojSciigoj(autoro_id=uzanto_id, temo=temo, forigo=False)
            nodo.save()

        novo_sciigoj = InformilojSciigoTipo.objects.filter(kodo__in=sciigoj)
        nodo.sciigoj.set(novo_sciigoj)
        status = True
        message = _('Настройки уведомлений применены')

        return AplikiForumajSciigojn(status=status, message=message, sciigoj=tuple(sc.kodo for sc in novo_sciigoj))


class RedaktuForumoTemoKomentoMutation(SiriusoAuthMutation, graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    node = graphene.Field(ForumojTemoKomentoNode)

    class Arguments:
        uuid = graphene.UUID()
        id = graphene.ID()
        teksto = graphene.String(required=True)

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        node = None

        if not info.context.user.is_authenticated:
            message = _('Требуется авторизация')
        else:
            query = []
            if 'id' in kwargs:
                query.append(Q(id=kwargs.get('id')))

            if 'uuid' in kwargs:
                query.append(Q(uuid=kwargs.get('uuid')))

            if len(query):
                node = (ForumojTemoKomentoNode._meta.model.objects
                        .select_related('teksto')
                        .get(*query, forigo=False))

                if node.autoro == info.context.user or info.context.user.is_admin or info.context.user.is_superadmin:
                    teksto = node.teksto
                    teksto.enhavo = kwargs.get('teksto')
                    node.lasta_autoro = info.context.user
                    node.lasta_dato = timezone.now()
                    teksto.save()
                    node.save()
                    status = True
                    message = _('Сообщение отредактировано')
                else:
                    node = None
                    message = _('Недостаточно прав')
            else:
                message = _('Не передан ни один из обязательных аргументов: "uuid" или "id"')

        return RedaktuForumoTemoKomentoMutation(status=status, message=message, node=node)


class RedaktuForumoTemoMutation(SiriusoAuthNode, graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    node = graphene.Field(ForumojTemoNode)

    class Arguments:
        uuid = graphene.UUID()
        id = graphene.ID()
        nomo = graphene.String()
        teksto = graphene.String()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        node = None

        if not info.context.user.is_authenticated:
            message = _('Требуется авторизация')
        else:
            query = []
            if 'id' in kwargs:
                query.append(Q(id=kwargs.get('id')))

            if 'uuid' in kwargs:
                query.append(Q(uuid=kwargs.get('uuid')))

            if len(query):
                node = (ForumojTemoNode._meta.model.objects
                        .select_related('teksto', 'nomo')
                        .get(*query, forigo=False))

                if node.autoro == info.context.user or info.context.user.is_admin or info.context.user.is_superadmin:
                    if 'teksto' in kwargs and kwargs.get('teksto') != '':
                        teksto = node.teksto
                        teksto.enhavo = kwargs.get('teksto')
                        teksto.save()

                    if 'nomo' in kwargs and kwargs.get('nomo') != '':
                        nomo = node.nomo
                        nomo.enhavo = kwargs.get('nomo')
                        nomo.save()

                    node.lasta_autoro = info.context.user
                    node.lasta_dato = timezone.now()
                    node.save()
                    status = True
                    message = _('Сообщение отредактировано')
                else:
                    node = None
                    message = _('Недостаточно прав')
            else:
                message = _('Не передан ни один из обязательных аргументов: "uuid" или "id"')

        return RedaktuForumoTemoMutation(status=status, message=message, node=node)


class ForigiForumoTemoKomentoMutation(SiriusoAuthMutation, graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()

    class Arguments:
        uuid = graphene.UUID()
        id = graphene.ID()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False

        if not info.context.user.is_authenticated:
            message = _('Требуется авторизация')
        else:
            query = []
            if 'id' in kwargs:
                query.append(Q(id=kwargs.get('id')))

            if 'uuid' in kwargs:
                query.append(Q(uuid=kwargs.get('uuid')))

            if len(query):
                node = (ForumojTemoKomentoNode._meta.model.objects
                        .get(*query, forigo=False))

                if node.autoro == info.context.user or info.context.user.is_admin or info.context.user.is_superadmin:
                    node.forigo = True
                    node.lasta_autoro = info.context.user
                    node.lasta_dato = timezone.now()
                    status = True
                    message = _('Сообщение удалено')
                else:
                    message = _('Недостаточно прав')
            else:
                message = _('Не передан ни один из обязательных аргументов: "uuid" или "id"')

        return ForigiForumoTemoKomentoMutation(status=status, message=message)


class ForigiForumoTemoMutation(SiriusoAuthMutation, graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()

    class Arguments:
        uuid = graphene.UUID()
        id = graphene.ID()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False

        if not info.context.user.is_authenticated:
            message = _('Требуется авторизация')
        else:
            query = []
            if 'id' in kwargs:
                query.append(Q(id=kwargs.get('id')))

            if 'uuid' in kwargs:
                query.append(Q(uuid=kwargs.get('uuid')))

            if len(query):
                node = (ForumojTemoNode._meta.model.objects
                        .get(*query, forigo=False))

                if node.autoro == info.context.user or info.context.user.is_admin or info.context.user.is_superadmin:
                    node.forigo = True
                    node.lasta_autoro = info.context.user
                    node.lasta_dato = timezone.now()
                    status = True
                    message = _('Тема удалена')
                else:
                    message = _('Недостаточно прав')
            else:
                message = _('Не передан ни один из обязательных аргументов: "uuid" или "id"')

        return ForigiForumoTemoMutation(status=status, message=message)


class ForumojMutations(graphene.ObjectType):
    redaktu_forumo_temo_komento = RedaktuForumoTemoKomentoMutation.Field()
    forigi_forumo_temo_komento = ForigiForumoTemoKomentoMutation.Field()

    redaktu_forumo_temo = RedaktuForumoTemoMutation.Field()
    forigi_forumo_temo = ForigiForumoTemoMutation.Field()

    apliki_forumoj_sciigojn = AplikiForumajSciigojn.Field()
