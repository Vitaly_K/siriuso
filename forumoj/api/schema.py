"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from graphene import relay, ObjectType, Field
from graphene_django import DjangoObjectType
from siriuso.api.types import SiriusoLingvo
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId
from siriuso.api.filters import SiriusoFilterConnectionField
from forumoj.models import *


class ForumojKategorioNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    nomo = Field(SiriusoLingvo)
    priskribo = Field(SiriusoLingvo)

    class Meta:
        model = ForumojKategorio
        filter_fields = {
            'uuid': ['exact'],
            'forumo': ['exact'],
            #'nomo__enhavo': ['exact', 'icontains', 'istartswith'],
            #'priskribo__enhavo': ['exact', 'icontains', 'istartswith'],
            'forigo': ['exact'],
            'krea_dato': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
        }
        interfaces = (relay.Node,)


class ForumojTemoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    nomo = Field(SiriusoLingvo)
    teksto = Field(SiriusoLingvo)

    class Meta:
        model = ForumojTemo
        filter_fields = {
            'uuid': ['exact'],
            'kategorio': ['exact'],
            'autoro': ['exact'],
            #'autoro__unua_nomo__enhavo': ['exact', 'icontains', 'istartswith'],
            #'autoro__familinomo__enhavo': ['exact', 'icontains', 'istartswith'],
            'krea_dato': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'forigo': ['exact'],
            'foriga_dato': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'arkivo': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'arkiva_dato': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'tipo__kodo': ['exact', 'icontains', 'istartswith'],
            #'nomo__enhavo': ['exact', 'icontains', 'istartswith'],
            #'teksto__enhavo': ['exact', 'icontains', 'istartswith'],
            'fermita': ['exact'],
            'komentado_aliro__kodo': ['exact', 'icontains', 'istartswith'],
            'fiksa': ['exact'],
            'fiksa_listo': ['exact']
        }
        interfaces = (relay.Node,)


class ForumojTemoKomentoNode(SiriusoAuthNode, DjangoObjectType):
    teksto = Field(SiriusoLingvo)

    class Meta:
        model = ForumojTemoKomento
        filter_fields = {
            'uuid': ['exact'],
            'respondo': ['exact'],
            'posedanto': ['exact'],
            'autoro': ['exact'],
            #'autoro__unua_nomo__enhavo': ['exact', 'icontains', 'istartswith'],
            #'autoro__familinomo__enhavo': ['exact', 'icontains', 'istartswith'],
            'forigo': ['exact'],
            'foriga_dato': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'arkivo': ['exact'],
            'arkiva_dato': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
        }
        interfaces = (relay.Node,)


class ForumoQuery(ObjectType):
    forumo_kategorio = relay.Node.Field(ForumojKategorioNode)
    forumoj_kategorioj = SiriusoFilterConnectionField(ForumojKategorioNode)

    forumo_temo = relay.Node.Field(ForumojTemoNode)
    forumoj_temoj = SiriusoFilterConnectionField(ForumojTemoNode)

    forumo_temo_komento = relay.Node.Field(ForumojTemoKomentoNode)
    forumoj_temoj_komentoj = SiriusoFilterConnectionField(ForumojTemoKomentoNode)
