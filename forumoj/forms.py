"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django import forms
from .models import *
from informiloj.models import InformilojLingvo
from komunumoj.models import *
from forumoj.models import (ForumojKategorio, ForumojKategorioTipo)
from ckeditor.widgets import CKEditorWidget
from ckeditor_uploader.widgets import CKEditorUploadingWidget


def get_kom_modeloj(kom_tipo):
    if kom_tipo == 'g':
        return (KomunumojGrupo, KomunumojGrupoMembro)
    elif kom_tipo == 'p':
        return (KomunumojSociaprojekto, KomunumojSociaprojektoMembro)
    elif kom_tipo == 's':
        return (KomunumojSoveto, KomunumojSovetoMembro)
    else:
        return (KomunumojOrganizo, KomunumojOrganizoMembro)


# Форма для тем форума групп
class ForumojTemoFormo(forms.ModelForm):

    nomo_formo = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Название темы'}), max_length=256,
                                 required=True)

    teksto_formo = forms.CharField(widget=CKEditorWidget(config_name='forumo_komento'), required=True, max_length=500*1024)

    komentado_aliro = forms.ModelChoiceField(KomunumojAliro.objects.filter(forigo=False), required=True)

    tipo = forms.ModelChoiceField(ForumojTemoTipo.objects.filter(forigo=False), required=True)

    __kom_modeloj = None
    __kom_uuid = None

    class Meta:
        model = ForumojTemo
        fields = ('komentado_aliro', 'fiksa', 'kategorio', 'autoro', 'tipo')
        exclude = ('nomo', 'teksto')
        
    def __init__(self, *args, **kwargs):
        kom_tipo = kwargs.pop('kom_tipo', None)

        if kom_tipo:
            self.__kom_modeloj = get_kom_modeloj(kom_tipo)

        super(ForumojTemoFormo, self).__init__(*args, **kwargs)
        self.fields['autoro'].widget = forms.HiddenInput()
        # self.fields['posedanto'].widget = forms.HiddenInput()
        self.fields['kategorio'].widget = forms.HiddenInput()

    def clean(self):
        data = self.cleaned_data

        # Проверка полномочий пользователя, создающего тему
        uzanto = data['autoro'] if 'autoro' in data else None

        try:
            komunumo = self.__kom_modeloj[0].objects.get(forumo_id=data.get('kategorio').forumo_id, forigo=False)

            if uzanto is not None:
                aliroj = ['moderiganto', 'administranto', 'komunumano', 'membro', 'membro-adm', 'membro-mod']
                aliro = self.__kom_modeloj[1].objects.values('tipo__kodo').filter(
                    autoro=uzanto, posedanto=komunumo, tipo__kodo__in=aliroj, forigo=False)

                if aliro.count():
                    aliro = aliro[0]

                    # Проверяем права на создание прикрепленных тем
                    if 'fiksa' in data and data['fiksa'] \
                            and aliro['tipo__kodo'] not in ['moderiganto', 'administranto']:
                        raise forms.ValidationError('Вы не можете создавать прикрепленные темы')
                    return data
        except:
            pass

        raise forms.ValidationError('Темы могут создавать только участники сообщества')

    def save(self, commit=True):
        try:
            temo = super(ForumojTemoFormo, self).save(commit=False)

            lingvo = InformilojLingvo.objects.get(kodo='ru_RU', forigo=False)
            nomo = temo.nomo if temo.nomo is not None else \
                ForumojTemoTitolo(posedanto=temo, lingvo=lingvo, enhavo=self.cleaned_data.get('nomo_formo'))
            teksto = temo.teksto if temo.teksto is not None else \
                ForumojTemoTeksto(posedanto=temo, lingvo=lingvo, enhavo=self.cleaned_data.get('teksto_formo'))

            if commit:
                temo.nomo = None
                temo.teksto = None
                temo.save()
                nomo.save()
                teksto.save()

                temo.nomo = nomo
                temo.teksto = teksto
                temo.save()
            else:
                temo.nomo = nomo
                temo.teksto = teksto
                return temo
        except InformilojLingvo.DoesNotExist:
            pass

        return self.instance

# Форма для комментариев тем форума групп
class ForumojTemoKomentoFormo(forms.ModelForm):

    #teksto_formo = forms.CharField(widget=forms.Textarea(attrs={'cols': 58, 'rows': 4}), required=True)
    teksto_formo = forms.CharField(widget=CKEditorWidget(config_name='forumo_komento'), required=True, max_length=250*1024)

    class Meta:
        model = ForumojTemoKomento
        fields = ('teksto_formo', )

    def save(self, request=None, commit=True):
        self.instance.posedanto_id = request.POST['temo']
        self.instance.autoro = request.user
        super(ForumojTemoKomentoFormo, self).save(commit=commit)

        if commit:
            kodo = InformilojLingvo.objects.filter(kodo='ru_RU')

            if not kodo is None:
                if not kodo is None:
                    teksto = ForumojTemoKomentoTeksto(lingvo=kodo[0], posedanto_id=self.instance.uuid,
                                                      enhavo=self.cleaned_data['teksto_formo'])
                    teksto.save()

        return self.instance


# Форма создания категории тем форума групп
class ForumojKategorioFormo(forms.ModelForm):

    nomo = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Название категории'}), required=True,
                           max_length=256)

    priskribo = forms.CharField(widget=forms.Textarea(attrs={'placeholder': 'Описание категории', 'cols': 58,
                                                             'rows': 4}), max_length=1024, required=False)

    aliro = forms.ModelChoiceField(KomunumojAliro.objects.filter(forigo=False), required=True)

    tipo = forms.ModelChoiceField(ForumojKategorioTipo.objects.filter(forigo=False), required=True)

    __komunumo_uuid = None
    __kom_modeloj = None

    class Meta:
        model = ForumojKategorio
        fields = ['tipo', 'aliro', 'forumo', 'autoro']

    def __init__(self, *args, **kwargs):
        if len(args):
            self.__komunumo_uuid = args[0]['posedanto']
        kom_tipo = kwargs.pop('kom_tipo', None)

        if kom_tipo:
            self.__kom_modeloj = get_kom_modeloj(kom_tipo)

        super().__init__(*args, **kwargs)

    def clean(self):
        data = self.cleaned_data

        if 'autoro' in data:
            if self.__kom_modeloj:
                membro_modelo = self.__kom_modeloj[1]
                aliroj = ['moderiganto', 'administranto', 'membro-adm', 'membro-mod']
                aliro = membro_modelo.objects.values('tipo__kodo').filter(
                    autoro=data.get('autoro'), posedanto_id=self.__komunumo_uuid, tipo__kodo__in=aliroj, forigo=False)

                if aliro.count():
                    # Проверяем наличие связи с форумом,
                    # если её нет, то создаём и устанавливаем
                    # поле формы forumo
                    kom_modelo = self.__kom_modeloj[0]

                    kom = (kom_modelo.objects.select_related('forumo')
                           .get(uuid=self.__komunumo_uuid, forigo=False))

                    if kom.forumo is None:
                        kom.forumo = KomunumojForumo(autoro=data.get('autoro'))
                        kom.forumo.save()
                        kom.save()

                    data['forumo'] = kom.forumo

                    return data
            else:
                raise forms.ValidationError('Не удалось определить владельца Категории')

        raise forms.ValidationError('У Вас нет прав для создания категории')

    def save(self, commit=True):
        try:
            kat = super(ForumojKategorioFormo, self).save(commit=False)

            lingvo = InformilojLingvo.objects.get(kodo='ru_RU', forigo=False)
            nomo = kat.nomo if kat.nomo is not None else \
                ForumojKategorioTitolo(posedanto=kat, enhavo=self.cleaned_data.get('nomo'), lingvo=lingvo)
            priskribo = kat.priskribo if kat.priskribo is not None else \
                ForumojKategorioPriskribo(posedanto=kat, enhavo=self.cleaned_data.get('priskribo'), lingvo=lingvo)

            if commit:
                kat.nomo = None
                kat.priskribo = None
                kat.save()
                nomo.save()
                priskribo.save()

                kat.nomo = nomo
                kat.priskribo = priskribo
                kat.save()
            else:
                kat.nomo = nomo
                kat.priskribo = priskribo
                return kat

        except InformilojLingvo.DoesNotExist:
            pass
