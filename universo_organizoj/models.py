"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres import fields as pgfields
from django.contrib.auth.models import Permission
from django.db.models import Max, Q

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from universo_bazo.models import UniversoBazaMaks, UniversoRealeco
from universo_uzantoj.models import UniversoUzanto


# Типы организаций в Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoOrganizoTipo(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_organizoj_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_organizoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de organizoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de organizoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_organizoj_tipoj', _('Povas vidi tipoj de organizoj de Universo')),
            ('povas_krei_universo_organizoj_tipoj', _('Povas krei tipoj de organizoj de Universo')),
            ('povas_forigi_universo_organizoj_tipoj', _('Povas forigi tipoj de organizoj de Universo')),
            ('povas_shanghi_universo_organizoj_tipoj', _('Povas ŝanĝi tipoj de organizoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoOrganizoTipo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_organizoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_organizoj.povas_vidi_universo_organizoj_tipoj', 
                'universo_organizoj.povas_krei_universo_organizoj_tipoj',
                'universo_organizoj.povas_forigi_universo_organizoj_tipoj',
                'universo_organizoj.povas_shanghi_universo_organizoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_organizoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_organizoj.povas_vidi_universo_organizoj_tipoj')
                    or user_obj.has_perm('universo_organizoj.povas_vidi_universo_organizoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_organizoj.povas_vidi_universo_organizoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Организации в Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoOrganizo(UniversoBazaMaks):

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # тип организации Универсо
    tipo = models.ForeignKey(UniversoOrganizoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_organizoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_organizoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Organizo de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Organizoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_organizoj', _('Povas vidi organizoj de Universo')),
            ('povas_krei_universo_organizoj', _('Povas krei organizoj de Universo')),
            ('povas_forigi_universo_organizoj', _('Povas forigi organizoj de Universo')),
            ('povas_shanghi_universo_organizoj', _('Povas ŝanĝi organizoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_organizoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_organizoj.povas_vidi_universo_organizoj', 
                'universo_organizoj.povas_krei_universo_organizoj',
                'universo_organizoj.povas_forigi_universo_organizoj',
                'universo_organizoj.povas_shanghi_universo_organizoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_organizoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_organizoj.povas_vidi_universo_organizoj')
                    or user_obj.has_perm('universo_organizoj.povas_vidi_universo_organizoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_organizoj.povas_vidi_universo_organizoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
