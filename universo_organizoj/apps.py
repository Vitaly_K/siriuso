from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UniversoOrganizojConfig(AppConfig):
    name = 'universo_organizoj'
    verbose_name = _('Organizoj de Universo')
