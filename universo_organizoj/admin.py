"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2020 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from django import forms
from django.utils.translation import ugettext_lazy as _
import json

from siriuso.utils import get_enhavo, set_enhavo
from universo_organizoj.models import *
from siriuso.forms.widgets import LingvoInputWidget
from universo_bazo.models import UniversoRealeco


class TekstoMixin:
    @staticmethod
    def teksto(obj, field):
        kampo = getattr(obj, field)

        if kampo is not None:
            result = get_enhavo(kampo)

            if result:
                return result[0]
        return ''

    def nomo_teksto(self, obj):
        return self.teksto(obj=obj, field='nomo')

    def priskribo_teksto(self, obj):
        return self.teksto(obj=obj, field='priskribo')

    def autoro_id(self, obj):
        return obj.autoro.siriuso_uzanto.id

    def autoro_email(self, obj):
        return obj.autoro.siriuso_uzanto.chefa_retposhto


# Форма типов организаций в Universo
class UniversoOrganizoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=UniversoRealeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = UniversoOrganizoTipo
        fields = [field.name for field in UniversoOrganizoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы организаций в Universo
@admin.register(UniversoOrganizoTipo)
class UniversoOrganizoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoOrganizoTipoFormo
    list_display = ('nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    # siriuso_uzanto_chefa_retposhto
    class Meta:
        model = UniversoOrganizoTipo


# Форма организаций в Universo
class UniversoOrganizoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=UniversoRealeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = UniversoOrganizo
        fields = [field.name for field in UniversoOrganizo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Организации в Universo
@admin.register(UniversoOrganizo)
class UniversoOrganizoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoOrganizoFormo
    list_display = ('nomo_teksto','autoro_id','priskribo_teksto')
    exclude = ('uuid',)
    class Meta:
        model = UniversoOrganizo


