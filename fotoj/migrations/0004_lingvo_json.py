# Generated by Django 2.0.9 on 2018-11-20 01:31

import django.contrib.postgres.fields.jsonb
from django.db import migrations
import siriuso.models.postgres
import siriuso.utils.modules


def sync_lingvo(apps, schema_editor):
    """
    Синхронизирует текстовые записи на всех языках с новыми JSON полями
    """
    sync_list = [
        ('FotojAlbumoUzantoTipo', 'FotojAlbumoUzantoTipoTitolo', 'nomo'),
        ('FotojAlbumoUzanto', 'FotojAlbumoUzantoTitolo', 'nomo'),
        ('FotojAlbumoUzanto', 'FotojAlbumoUzantoPriskribo', 'priskribo'),
        ('FotojAlbumoUzantoKomento', 'FotojAlbumoUzantoKomentoTeksto', 'teksto'),
        ('FotojFotoUzanto', 'FotojFotoUzantoPriskribo', 'priskribo'),
        ('FotojFotoUzantoKomento', 'FotojFotoUzantoKomentoTeksto', 'teksto'),
        ('FotojAlbumoKomunumoTipo', 'FotojAlbumoKomunumoTipoTitolo', 'nomo'),
        ('FotojAlbumoSoveto', 'FotojAlbumoSovetoTitolo', 'nomo'),
        ('FotojAlbumoSoveto', 'FotojAlbumoSovetoPriskribo', 'priskribo'),
        ('FotojAlbumoSovetoKomento', 'FotojAlbumoSovetoKomentoTeksto', 'teksto'),
        ('FotojFotoSoveto', 'FotojFotoSovetoPriskribo', 'priskribo'),
        ('FotojFotoSovetoKomento', 'FotojFotoSovetoKomentoTeksto', 'teksto'),
        ('FotojAlbumoGrupo', 'FotojAlbumoGrupoTitolo', 'nomo'),
        ('FotojAlbumoGrupo', 'FotojAlbumoGrupoPriskribo', 'priskribo'),
        ('FotojAlbumoGrupoKomento', 'FotojAlbumoGrupoKomentoTeksto', 'teksto'),
        ('FotojFotoGrupo', 'FotojFotoGrupoPriskribo', 'priskribo'),
        ('FotojFotoGrupoKomento', 'FotojFotoGrupoKomentoTeksto', 'teksto'),
        ('FotojAlbumoOrganizo', 'FotojAlbumoOrganizoTitolo', 'nomo'),
        ('FotojAlbumoOrganizo', 'FotojAlbumoOrganizoPriskribo', 'priskribo'),
        ('FotojAlbumoOrganizoKomento', 'FotojAlbumoOrganizoKomentoTeksto', 'teksto'),
        ('FotojFotoOrganizo', 'FotojFotoOrganizoPriskribo', 'priskribo'),
        ('FotojFotoOrganizoKomento', 'FotojFotoOrganizoKomentoTeksto', 'teksto'),
        ('FotojAlbumoSociaprojekto', 'FotojAlbumoSociaprojektoTitolo', 'nomo'),
        ('FotojAlbumoSociaprojekto', 'FotojAlbumoSociaprojektoPriskribo', 'priskribo'),
        ('FotojAlbumoSociaprojektoKomento', 'FotojAlbumoSociaprojektoKomentoTeksto', 'teksto'),
        ('FotojFotoSociaprojekto', 'FotojFotoSociaprojektoPriskribo', 'priskribo'),
        ('FotojFotoSociaprojektoKomento', 'FotojFotoSociaprojektoKomentoTeksto', 'teksto'),
    ]

    for kom_model, lingvo_model, posedanto_kampo in sync_list:
        # Для всех записей сообщества
        for kom in apps.get_model('fotoj', kom_model).objects.all():
            enhavo = {
                'enhavo': [],
                'lingvo': {},
                'chefa_varianto': None
            }
            # Для всех языковых вариантов
            lingvoj = apps.get_model('fotoj', lingvo_model).objects.filter(posedanto=kom)

            for lingvo in lingvoj:
                enhavo['lingvo'][lingvo.lingvo.kodo] = len(enhavo['enhavo'])
                enhavo['enhavo'].append(lingvo.enhavo)

                if lingvo.chefa_varianto or lingvoj.count() == 1:
                    enhavo['chefa_varianto'] = lingvo.lingvo.kodo

            setattr(kom, posedanto_kampo, enhavo)
            kom.save()


class Migration(migrations.Migration):

    dependencies = [
        ('fotoj', '0003_auto_20180821_0447'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='fotojalbumogrupo',
            name='nomo',
        ),
        migrations.RemoveField(
            model_name='fotojalbumogrupo',
            name='priskribo',
        ),
        migrations.RemoveField(
            model_name='fotojalbumogrupokomento',
            name='teksto',
        ),
        migrations.RemoveField(
            model_name='fotojalbumokomunumotipo',
            name='nomo',
        ),
        migrations.RemoveField(
            model_name='fotojalbumoorganizo',
            name='nomo',
        ),
        migrations.RemoveField(
            model_name='fotojalbumoorganizo',
            name='priskribo',
        ),
        migrations.RemoveField(
            model_name='fotojalbumoorganizokomento',
            name='teksto',
        ),
        migrations.RemoveField(
            model_name='fotojalbumosociaprojekto',
            name='nomo',
        ),
        migrations.RemoveField(
            model_name='fotojalbumosociaprojekto',
            name='priskribo',
        ),
        migrations.RemoveField(
            model_name='fotojalbumosociaprojektokomento',
            name='teksto',
        ),
        migrations.RemoveField(
            model_name='fotojalbumosoveto',
            name='nomo',
        ),
        migrations.RemoveField(
            model_name='fotojalbumosoveto',
            name='priskribo',
        ),
        migrations.RemoveField(
            model_name='fotojalbumosovetokomento',
            name='teksto',
        ),
        migrations.RemoveField(
            model_name='fotojalbumouzanto',
            name='nomo',
        ),
        migrations.RemoveField(
            model_name='fotojalbumouzanto',
            name='priskribo',
        ),
        migrations.RemoveField(
            model_name='fotojalbumouzantokomento',
            name='teksto',
        ),
        migrations.RemoveField(
            model_name='fotojalbumouzantotipo',
            name='nomo',
        ),
        migrations.RemoveField(
            model_name='fotojfotogrupo',
            name='priskribo',
        ),
        migrations.RemoveField(
            model_name='fotojfotogrupokomento',
            name='teksto',
        ),
        migrations.RemoveField(
            model_name='fotojfotoorganizo',
            name='priskribo',
        ),
        migrations.RemoveField(
            model_name='fotojfotoorganizokomento',
            name='teksto',
        ),
        migrations.RemoveField(
            model_name='fotojfotosociaprojekto',
            name='priskribo',
        ),
        migrations.RemoveField(
            model_name='fotojfotosociaprojektokomento',
            name='teksto',
        ),
        migrations.RemoveField(
            model_name='fotojfotosoveto',
            name='priskribo',
        ),
        migrations.RemoveField(
            model_name='fotojfotosovetokomento',
            name='teksto',
        ),
        migrations.RemoveField(
            model_name='fotojfotouzanto',
            name='priskribo',
        ),
        migrations.RemoveField(
            model_name='fotojfotouzantokomento',
            name='teksto',
        ),
        migrations.AddField(
            model_name='fotojalbumogrupo',
            name='nomo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Titolo'),
        ),
        migrations.AddField(
            model_name='fotojalbumogrupo',
            name='priskribo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Priskribo'),
        ),
        migrations.AddField(
            model_name='fotojalbumogrupokomento',
            name='teksto',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Teksto'),
        ),
        migrations.AddField(
            model_name='fotojalbumokomunumotipo',
            name='nomo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Titolo'),
        ),
        migrations.AddField(
            model_name='fotojalbumoorganizo',
            name='nomo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Titolo'),
        ),
        migrations.AddField(
            model_name='fotojalbumoorganizo',
            name='priskribo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Priskribo'),
        ),
        migrations.AddField(
            model_name='fotojalbumoorganizokomento',
            name='teksto',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Teksto'),
        ),
        migrations.AddField(
            model_name='fotojalbumosociaprojekto',
            name='nomo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Titolo'),
        ),
        migrations.AddField(
            model_name='fotojalbumosociaprojekto',
            name='priskribo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Priskribo'),
        ),
        migrations.AddField(
            model_name='fotojalbumosociaprojektokomento',
            name='teksto',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Teksto'),
        ),
        migrations.AddField(
            model_name='fotojalbumosoveto',
            name='nomo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Titolo'),
        ),
        migrations.AddField(
            model_name='fotojalbumosoveto',
            name='priskribo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Priskribo'),
        ),
        migrations.AddField(
            model_name='fotojalbumosovetokomento',
            name='teksto',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Teksto'),
        ),
        migrations.AddField(
            model_name='fotojalbumouzanto',
            name='nomo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Titolo'),
        ),
        migrations.AddField(
            model_name='fotojalbumouzanto',
            name='priskribo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Priskribo'),
        ),
        migrations.AddField(
            model_name='fotojalbumouzantokomento',
            name='teksto',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Teksto'),
        ),
        migrations.AddField(
            model_name='fotojalbumouzantotipo',
            name='nomo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Titolo'),
        ),
        migrations.AddField(
            model_name='fotojfotogrupo',
            name='priskribo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Priskribo'),
        ),
        migrations.AddField(
            model_name='fotojfotogrupokomento',
            name='teksto',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Teksto'),
        ),
        migrations.AddField(
            model_name='fotojfotoorganizo',
            name='priskribo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Priskribo'),
        ),
        migrations.AddField(
            model_name='fotojfotoorganizokomento',
            name='teksto',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Teksto'),
        ),
        migrations.AddField(
            model_name='fotojfotosociaprojekto',
            name='priskribo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Priskribo'),
        ),
        migrations.AddField(
            model_name='fotojfotosociaprojektokomento',
            name='teksto',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Teksto'),
        ),
        migrations.AddField(
            model_name='fotojfotosoveto',
            name='priskribo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Priskribo'),
        ),
        migrations.AddField(
            model_name='fotojfotosovetokomento',
            name='teksto',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Teksto'),
        ),
        migrations.AddField(
            model_name='fotojfotouzanto',
            name='priskribo',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Priskribo'),
        ),
        migrations.AddField(
            model_name='fotojfotouzantokomento',
            name='teksto',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Teksto'),
        ),
    ]
