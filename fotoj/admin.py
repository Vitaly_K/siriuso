"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from django import forms
from django.utils.translation import ugettext_lazy as _
from siriuso.utils import get_enhavo, set_enhavo
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
import json
from fotoj.models import *


#Аминка фотогалерей
class TekstoMixin:
    @staticmethod
    def teksto(obj, field):
        kampo = getattr(obj, field)

        if kampo is not None:
            result = get_enhavo(kampo)

            if result:
                return result[0]
        return ''

    def nomo_teksto(self, obj):
        return self.teksto(obj=obj, field='nomo')

    def priskribo_teksto(self, obj):
        return self.teksto(obj=obj, field='priskribo')


# Форма для многоязычных названий типов файлов изображений
class FotojFotoDosieroTipoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=True)

    class Meta:
        model = FotojFotoDosieroTipo
        fields = [field.name for field in FotojFotoDosieroTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы файлов изображений
@admin.register(FotojFotoDosieroTipo)
class FotojFotoDosieroTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = FotojFotoDosieroTipoFormo
    list_display = ('nomo_teksto', 'kodo', 'uuid')
    exclude = ('nomo_teksto',)

    class Meta:
        model = FotojFotoDosieroTipo


# Фотогалереи пользователей
# Форма для многоязычных названий типов альбомов пользователей
class FotojAlbumoUzantoTipoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=True)

    class Meta:
        model = FotojAlbumoUzantoTipo
        fields = [field.name for field in FotojAlbumoUzantoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы альбомов пользователей
@admin.register(FotojAlbumoUzantoTipo)
class FotojAlbumoUzantoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = FotojAlbumoUzantoTipoFormo
    list_display = ('nomo_teksto', 'uuid')

    class Meta:
        model = FotojAlbumoUzantoTipo


# Форма для многоязычных названий и описаний альбомов пользователей
class FotojAlbumoUzantoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=True)
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=True)

    class Meta:
        model = FotojAlbumoUzanto
        fields = [field.name for field in FotojAlbumoUzanto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

# Альбомы пользователей
@admin.register(FotojAlbumoUzanto)
class FotojAlbumoUzantoAdmin(admin.ModelAdmin, TekstoMixin):
    form = FotojAlbumoUzantoFormo
    list_display = ('nomo_teksto', 'priskribo_teksto', 'uuid')

    class Meta:
        model = FotojAlbumoUzanto


# Форма для многоязычных описаний фотографий пользователей
class FotojFotoUzantoFormo(forms.ModelForm):
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=True)

    class Meta:
        model = FotojFotoUzanto
        fields = [field.name for field in FotojFotoUzanto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Фотографии пользователей
@admin.register(FotojFotoUzanto)
class FotojFotoUzantoAdmin(admin.ModelAdmin, TekstoMixin):
    form = FotojFotoUzantoFormo
    list_display = ('priskribo_teksto', 'albumo', 'uuid')

    class Meta:
        model = FotojFotoUzanto


# Фотолалереи сообществ
# Форма для многоязычных названий типов альбомов сообществ
class FotojAlbumoKomunumoTipoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=True)

    class Meta:
        model = FotojAlbumoKomunumoTipo
        fields = [field.name for field in FotojAlbumoKomunumoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы альбомов сообществ
@admin.register(FotojAlbumoKomunumoTipo)
class FotojAlbumoKomunumoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = FotojAlbumoKomunumoTipoFormo
    list_display = ('nomo_teksto', 'uuid')

    class Meta:
        model = FotojAlbumoKomunumoTipo


# Форма для многоязычных названий и описаний альбомов сообществ
class FotojAlbumoKomunumoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=True)
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=True)

    class Meta:
        model = FotojAlbumoKomunumo
        fields = [field.name for field in FotojAlbumoKomunumo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Альбомы сообществ
@admin.register(FotojAlbumoKomunumo)
class FotojAlbumoKomunumoAdmin(admin.ModelAdmin, TekstoMixin):
    form = FotojAlbumoKomunumoFormo
    list_display = ('nomo_teksto', 'priskribo_teksto', 'uuid')

    class Meta:
        model = FotojAlbumoKomunumo


# Форма для многоязычных описаний фотографий сообществ
class FotojFotoKomunumoFormo(forms.ModelForm):
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=True)

    class Meta:
        model = FotojFotoKomunumo
        fields = [field.name for field in FotojFotoKomunumo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Фотографии сообществ
@admin.register(FotojFotoKomunumo)
class FotojFotoKomunumoAdmin(admin.ModelAdmin, TekstoMixin):
    form = FotojFotoKomunumoFormo
    list_display = ('priskribo_teksto', 'albumo', 'uuid')

    class Meta:
        model = FotojFotoKomunumo
