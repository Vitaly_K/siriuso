from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class FotojConfig(AppConfig):
    name = 'fotoj'
    verbose_name = _('Fotoj')

    def ready(self):
        import fotoj.signals
