from graphene_django import DjangoObjectType
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId
from siriuso.api.filters import SiriusoFilterConnectionField
from siriuso.api.types import SiriusoLingvo, ErrorNode
from siriuso.utils import build_absolute_uri
from graphene_permissions.permissions import AllowAny
from ..models import *
import graphene
import re


class FotoBildo(graphene.ObjectType):
    url = graphene.String()
    grandeco = graphene.List(of_type=graphene.Int)

    @staticmethod
    def __resolve_bildo(request, bildo, default=None):
        if re.search(r'^/static/', str(bildo)):
            image = bildo.name
        else:
            image = getattr(bildo, 'url') if bildo else default

        return build_absolute_uri(request, image) if image else None

    @staticmethod
    def resolve_url(root, info):
        return FotoBildo.__resolve_bildo(info.context, root)

    @staticmethod
    def resolve_grandeco(root, info):
        return root.width, root.height


class FotojFotoDosieroNode(SiriusoAuthNode, DjangoObjectType):
    """
    Файл изображения на сервере с различными размерами
    """
    permission_classes = (AllowAny,)

    bildo_baza = graphene.Field(FotoBildo)
    bildo_a = graphene.Field(FotoBildo)
    bildo_b = graphene.Field(FotoBildo)
    bildo_c = graphene.Field(FotoBildo)
    bildo_d = graphene.Field(FotoBildo)
    bildo_e = graphene.Field(FotoBildo)
    bildo_f = graphene.Field(FotoBildo)

    def resolve_bildo_a(self, info):
        return getattr(self, 'bildo_a') or getattr(self, 'bildo_baza')

    def resolve_bildo_b(self, info):
        return getattr(self, 'bildo_b') or getattr(self, 'bildo_baza')

    def resolve_bildo_c(self, info):
        return getattr(self, 'bildo_c') or getattr(self, 'bildo_baza')

    def resolve_bildo_d(self, info):
        return getattr(self, 'bildo_d') or getattr(self, 'bildo_baza')

    def resolve_bildo_e(self, info):
        return getattr(self, 'bildo_e') or getattr(self, 'bildo_baza')

    def resolve_bildo_f(self, info):
        return getattr(self, 'bildo_f') or getattr(self, 'bildo_baza')

    class Meta:
        model = FotojFotoDosiero
        filter_fields = ['uuid', 'forigo', 'publikigo', 'arkivo']
        only_fields = ['uuid', 'forigo', 'publikigo', 'arkivo', 'bildo_baza',
                       'bildo_a', 'bildo_b', 'bildo_c', 'bildo_d', 'bildo_e', 'bildo_f']
        interfaces = (graphene.relay.Node,)


class FotoBildoMixin:
    """
    Миксин разрешает файлы изображений как абсолютный URL
    """
    @staticmethod
    def __resolve_bildo(request, bildo, default=None):
        image = getattr(bildo, 'url') if bildo else default
        return request.build_absolute_uri(image) if image else None

    def resolve_foto(self, info):
        return FotoBildoMixin.__resolve_bildo(info.context, getattr(self, 'foto'))

    def resolve_foto_min(self, info):
        return FotoBildoMixin.__resolve_bildo(info.context, getattr(self, 'foto_min'))

    def resolve_foto_maks(self, info):
        return FotoBildoMixin.__resolve_bildo(info.context, getattr(self, 'foto_maks'))


class FotojAlbumoUzantoTipoNode(SiriusoAuthNode, DjangoObjectType):
    """Тип пользовательского фотоальбома"""
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo)

    class Meta:
        model = FotojAlbumoUzantoTipo
        filter_fields = ['uuid', 'forigo', 'publikigo', 'arkivo']
        interfaces = (graphene.relay.Node,)


class FotojFotoUzantoNode(SiriusoAuthNode, FotoBildoMixin, DjangoObjectType):
    """Изображение пользовательского фотоальбома"""
    permission_classes = (AllowAny,)

    class Meta:
        model = FotojFotoUzanto
        filter_fields = ['uuid', 'forigo', 'publikigo', 'arkivo',]
        interfaces = (graphene.relay.Node,)


class FotojAlbumoUzantoNode(SiriusoAuthNode, DjangoObjectType):
    """Пользовательский фотоальбом"""
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo)
    fotoj = SiriusoFilterConnectionField(FotojFotoUzantoNode)

    class Meta:
        model = FotojAlbumoUzanto
        filter_fields = {
            'uuid': ['exact'],
            'tipo__kodo': ['exact'],
            'forigo': ['exact'],
            'publikigo': ['exact'],
            'arkivo': ['exact'],
            'posedanto_id': ['exact', 'in'],
            'posedanto__id': ['exact', 'in']
        }
        interfaces = (graphene.relay.Node,)

    @staticmethod
    def resolve_fotoj(root, info, **kwargs):
        return FotojFotoUzanto.objects.filter(albumo=root)


class FotojAlbumoKomunumoTipoNode(SiriusoAuthNode, DjangoObjectType):
    """Тип фотоальбома сообщества"""
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo)

    class Meta:
        model = FotojAlbumoKomunumoTipo
        filter_fields = ['uuid', 'forigo', 'publikigo', 'arkivo']
        interfaces = (graphene.relay.Node,)


class FotojFotoKomunumoNode(SiriusoAuthNode, FotoBildoMixin, DjangoObjectType):
    """Изображение фотоальбома сообщества"""
    permission_classes = (AllowAny,)

    class Meta:
        model = FotojFotoKomunumo
        filter_fields = ['uuid', 'forigo', 'publikigo', 'arkivo']
        interfaces = (graphene.relay.Node,)


class FotojAlbumoKomunumoNode(SiriusoAuthNode, DjangoObjectType):
    """Пользовательский фотоальбом"""
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo)
    fotoj = SiriusoFilterConnectionField(FotojFotoKomunumoNode)

    class Meta:
        model = FotojAlbumoKomunumo
        filter_fields = {
            'uuid': ['exact'],
            'tipo__kodo': ['exact'],
            'forigo': ['exact'],
            'publikigo': ['exact'],
            'arkivo': ['exact'],
            'posedanto__uuid': ['exact'],
            'posedanto__id': ['exact', 'in']
        }
        interfaces = (graphene.relay.Node,)

    @staticmethod
    def resolve_fotoj(root, info, **kwargs):
        return FotojFotoKomunumo.objects.filter(albumo=root)


class FotojUnion(graphene.Union):
    class Meta:
        types = (FotojFotoUzantoNode, FotojFotoKomunumoNode)


class FotojQuery(graphene.ObjectType):
    fotoj_albumoj_komunumoj = SiriusoFilterConnectionField(FotojAlbumoKomunumoNode)
    fotoj_albumoj_uzantoj = SiriusoFilterConnectionField(FotojAlbumoUzantoNode)
