$(document).ready(function ($) {
    let forms = [];

    forumoj_init();

    function forumoj_init() {
        forms['komento_formo'] = $('.komento-formo');
        $('form').on('submit', forumoj_submit);
        $('[data-ago]').on('click', forumoj_post_action);
    }

    function forumoj_toggle_class(obj, class_name) {
        $(obj).toggleClass(class_name);
    }

    function forumoj_get_action(obj) {
        let target = $(obj).find('input[type="hidden"][name="ago"]');

        if(target.length) {
            return target.length > 1 ? target[0].val() : target.val();
        }

        return null;
    }

    function forumoj_submit() {
        let target = $(this);
        let type = forumoj_get_action(target);

        switch (type) {
            case 'ideo':
            case 'komento':
                target.find('input, textarea, select, button').prop('disabled', true);
                toggleBlockForm(target);
                forumoj_post(target, type);
                target.find('input, textarea, select, button').prop('disabled', false);
                return false;

            default:
                return true;
        }
    }

    function forumoj_post_action(e) {
        let target = $(e.target);

        if(target.attr('data-ago') === 'forigi' && !confirm(l10n.kom_forigi)) {
            return;
        }

        let form = target.parents('[data-komento_id]');
        let data = new FormData();

        data.append('kom_uuid', form.attr('data-komento_id'));
        data.append('ago', target.attr('data-ago'));
        data.append('csrfmiddlewaretoken', getCookie('csrftoken'));

        toggleBlockForm(form, 48);

        $.ajax({
            url: window.location.href,
            type: 'POST',
            async: true,
            data: data,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false
        })
            .done(function (result){
                if(result.status === 'ok') {
                    forumoj_before_remove(form);
                    setTimeout(function () {
                        toggleBlockForm(form);
                        form.addClass('forumoj_hidden');
                        form.one('transitionend', function() {form.remove()});
                    }, 500);
                } else {
                    console.log("Bad server response: " + result.err);
                    toggleBlockForm(form);
                }
            })
            .fail(function (jqXHR, textStatus) {
                console.log("Request failed: " + textStatus);
                toggleBlockForm(form);
            });
    }

    function forumoj_post(form, type) {
        let data = prepare_form(form);

        $.ajax({
            url: window.location.href,
            type: 'POST',
            async: true,
            data: data,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false
        })
            .done(function (result) {
                if(result.status === 'ok') {
                    setTimeout(forumoj_insert, 500, result, form);
                } else {
                    console.log("Bad server response: " + result.err);
                    toggleBlockForm(form);
                }
            })
            .fail(function (jqXHR, textStatus) {
                toggleBlockForm(form);
                console.log("Request failed: " + textStatus);
            });
    }

    function prepare_form(form) {
        if(typeof form === 'object') {
            out = new FormData();
            fields = $(form).find('input, textarea, select');

            fields.each(function (index, element) {
                let cur = $(element);
                if(cur.prop('type') === 'submit') {
                    return;
                } else if(cur.prop('type') === 'checkbox') {
                    cur.val(cur.prop('checked'));
                }

                out.append(cur.prop('name'), cur.val());
            });
            out.append('csrfmiddlewaretoken', getCookie('csrftoken'));

            return out;
        }
    }

    function forumoj_before_remove(content) {
        let jcontent = $(content);
        jcontent.css({'max-height': jcontent.outerHeight()});
    }

    function forumoj_insert(response, form) {
        let obj = forumoj_before_insert(response.html);

        if(form !== undefined) {
            form[0].reset();
        }

        toggleBlockForm(form);
        switch (response.type) {
            case 'ideo':
                $('.feed-bl').last().after(obj);
                break;

            case 'komento':
                $('div[data-ideo_id=' + response.por_ideo + ']').append(obj);
                break;

            default:
                return
        }

        forumoj_after_insert(obj)
    }

    function forumoj_before_insert(content) {
        let jcontent = $(content);
        jcontent.css({'position':'absolute', 'left': "-10000px"});
        $('body').append(jcontent);
        jcontent.css({'max-height': jcontent.outerHeight(), 'position' : '', 'left' : ''})
            .toggleClass('forumoj_hidden')
            .detach();
        return jcontent;
    }

    function forumoj_after_insert(obj) {
        obj.find('[data-ago]').on('click', forumoj_post_action);
        setTimeout(forumoj_toggle_class, 10, obj, 'forumoj_hidden');
        setTimeout(function(obj){obj.css('max-height','')}, 2000, obj);
    }
});