"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from .models import *


# Типы категорий идей общественных проектов
class IdeojSociaprojektoKategorioTipoAdmin (admin.ModelAdmin):
    list_display = ('nomo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = IdeojSociaprojektoKategorioTipo

admin.site.register(IdeojSociaprojektoKategorioTipo, IdeojSociaprojektoKategorioTipoAdmin)


# Категории идей общественных проектов
class IdeojSociaprojektoKategorioAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'uuid',)
    exclude = ('nomo', 'priskribo', 'id')

    class Meta:
        model = IdeojSociaprojektoKategorio

admin.site.register(IdeojSociaprojektoKategorio, IdeojSociaprojektoKategorioAdmin)


# Типы идей общественных проектов
class IdeojSociaprojektoIdeoTipoAdmin (admin.ModelAdmin):
    list_display = ('nomo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = IdeojSociaprojektoIdeoTipo

admin.site.register(IdeojSociaprojektoIdeoTipo, IdeojSociaprojektoIdeoTipoAdmin)


# Идеи общественных проектов
class IdeojSociaprojektoIdeoAdmin (admin.ModelAdmin):
    list_display = ('nomo', 'uuid',)
    exclude = ('nomo', 'teksto', 'id')

    class Meta:
        model = IdeojSociaprojektoIdeo

admin.site.register(IdeojSociaprojektoIdeo, IdeojSociaprojektoIdeoAdmin)


# Комментарии идей общественных проектов
class IdeojSociaprojektoIdeoKomentoAdmin(admin.ModelAdmin):
    list_display = ('uuid',)
    exclude = ('teksto',)

    class Meta:
        model = IdeojSociaprojektoIdeoKomento

admin.site.register(IdeojSociaprojektoIdeoKomento, IdeojSociaprojektoIdeoKomentoAdmin)


# Типы категорий идей советов
class IdeojSovetoKategorioTipoAdmin (admin.ModelAdmin):
    list_display = ('nomo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = IdeojSovetoKategorioTipo

admin.site.register(IdeojSovetoKategorioTipo, IdeojSovetoKategorioTipoAdmin)


# Категории идей советов
class IdeojSovetoKategorioAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'uuid',)
    exclude = ('nomo', 'priskribo', 'id')

    class Meta:
        model = IdeojSovetoKategorio

admin.site.register(IdeojSovetoKategorio, IdeojSovetoKategorioAdmin)


# Типы идей советов
class IdeojSovetoIdeoTipoAdmin (admin.ModelAdmin):
    list_display = ('nomo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = IdeojSovetoIdeoTipo

admin.site.register(IdeojSovetoIdeoTipo, IdeojSovetoIdeoTipoAdmin)


# Идеи советов
class IdeojSovetoIdeoAdmin (admin.ModelAdmin):
    list_display = ('nomo', 'uuid',)
    exclude = ('nomo', 'teksto', 'id')

    class Meta:
        model = IdeojSovetoIdeo

admin.site.register(IdeojSovetoIdeo, IdeojSovetoIdeoAdmin)


# Комментарии идей советов
class IdeojSovetoIdeoKomentoAdmin(admin.ModelAdmin):
    list_display = ('uuid',)
    exclude = ('teksto',)

    class Meta:
        model = IdeojSovetoIdeoKomento

admin.site.register(IdeojSovetoIdeoKomento, IdeojSovetoIdeoKomentoAdmin)
