"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django import forms
from .models import *
from informiloj.models import InformilojLingvo
from komunumoj.models import KomunumojAliro, KomunumojSociaprojektoMembro, KomunumojSovetoMembro
from ideoj.models import (IdeojSociaprojektoKategorio, IdeojSociaprojektoKategorioTipo)


# Форма для идей общественного проекта
class IdeojSociaprojektoIdeoFormo(forms.ModelForm):

    nomo_formo = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Название идеи'}), max_length=256,
                                 required=True)

    teksto_formo = forms.CharField(widget=forms.Textarea(attrs={'placeholder': 'Описание идеи','cols': 64,
                                                                'rows': 8}), required=True, max_length=500*1024)

    komentado_aliro = forms.ModelChoiceField(KomunumojAliro.objects.filter(forigo=False), required=True)

    tipo = forms.ModelChoiceField(IdeojSociaprojektoIdeoTipo.objects.filter(forigo=False), required=True)

    class Meta:
        model = IdeojSociaprojektoIdeo
        fields = ('komentado_aliro', 'fiksa', 'posedanto', 'kategorio', 'autoro', 'tipo')
        exclude = ('nomo', 'teksto')
        
    def __init__(self, *args, **kwargs):
        super(IdeojSociaprojektoIdeoFormo, self).__init__(*args, **kwargs)
        self.fields['autoro'].widget = forms.HiddenInput()
        self.fields['posedanto'].widget = forms.HiddenInput()
        self.fields['kategorio'].widget = forms.HiddenInput()

    def clean(self):
        data = self.cleaned_data

        # Проверка полномочий пользователя, создающего тему
        posedanto = data['posedanto'] if 'posedanto' in data else None
        uzanto = data['autoro'] if 'autoro' in data else None

        if posedanto is not None and uzanto is not None:
            aliroj = ['moderiganto', 'administranto', 'komunumano', 'membro', 'membro-adm', 'membro-mod']
            aliro = KomunumojSociaprojektoMembro.objects.values('tipo__kodo').filter(
                autoro=uzanto, posedanto=posedanto, tipo__kodo__in=aliroj, forigo=False)

            if aliro.count():
                aliro = aliro[0]

                # Проверяем права на создание прикрепленных тем
                if 'fiksa' in data and data['fiksa'] \
                        and aliro['tipo__kodo'] not in ['moderiganto', 'administranto', 'membro-adm', 'membro-mod']:
                    raise forms.ValidationError('Вы не можете создавать прикрепленные идеи')
                return data

        raise forms.ValidationError('Идеи могут создавать только участники сообщества')

    def save(self, commit=True):
        try:
            ideo = super(IdeojSociaprojektoIdeoFormo, self).save(commit=False)

            lingvo = InformilojLingvo.objects.get(kodo='ru_RU', forigo=False)
            #nomo = ideo.nomo if ideo.nomo is not None else \
            #    IdeojSociaprojektoIdeoTitolo(posedanto=ideo, lingvo=lingvo, enhavo=self.cleaned_data.get('nomo_formo'))
            #teksto = ideo.teksto if ideo.teksto is not None else \
            #    IdeojSociaprojektoIdeoTeksto(posedanto=ideo, lingvo=lingvo, enhavo=self.cleaned_data.get('teksto_formo'))

            if commit:
                ideo.nomo = None
                ideo.teksto = None
                ideo.save()
                # nomo.save()
                # teksto.save()

                #ideo.nomo = nomo
                #ideo.teksto = teksto
                ideo.save()
            else:
                #ideo.nomo = nomo
                #ideo.teksto = teksto
                return ideo
        except InformilojLingvo.DoesNotExist:
            pass

# Форма для комментариев идей общественного проекта
class IdeojSociaprojektoIdeoKomentoFormo(forms.ModelForm):

    teksto_formo = forms.CharField(widget=forms.Textarea(attrs={'cols': 58, 'rows': 4}), required=True)

    class Meta:
        model = IdeojSociaprojektoIdeoKomento
        fields = ('teksto_formo', )

    def save(self, request=None, sociaprojekto=None, commit=True):
        self.instance.posedanto_id = request.POST['ideo']
        self.instance.autoro = request.user
        super(IdeojSociaprojektoIdeoKomentoFormo, self).save(commit=commit)

        if commit:
            kodo = InformilojLingvo.objects.filter(kodo='ru_RU')

            if not kodo is None:
                if not kodo is None:
                    pass
                    #teksto = IdeojSociaprojektoIdeoKomentoTeksto(lingvo=kodo[0], posedanto_id=self.instance.uuid,
                    #                                          enhavo=self.cleaned_data['teksto_formo'])
                    #teksto.save()


# Форма создания категории идей общественного проекта
class IdeojSociaprojektoKategorioFormo(forms.ModelForm):

    nomo = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Название категории'}), required=True,
                           max_length=256)

    priskribo = forms.CharField(widget=forms.Textarea(attrs={'placeholder': 'Описание категории', 'cols': 58,
                                                             'rows': 4}), max_length=1024, required=False)

    aliro = forms.ModelChoiceField(KomunumojAliro.objects.filter(forigo=False), required=True)

    tipo = forms.ModelChoiceField(IdeojSociaprojektoKategorioTipo.objects.filter(forigo=False), required=True)

    class Meta:
        model = IdeojSociaprojektoKategorio
        fields = ['tipo', 'aliro', 'posedanto', 'autoro']

    def clean(self):
        data = self.cleaned_data

        if 'autoro' in data and 'posedanto' in data:
            aliroj = ['moderiganto', 'administranto', 'membro-mod', 'membro-adm']
            aliro = KomunumojSociaprojektoMembro.objects.values('tipo__kodo').filter(
                autoro=data.get('autoro'), posedanto=data.get('posedanto'), tipo__kodo__in=aliroj, forigo=False)

            if aliro.count():
                return data

        raise forms.ValidationError('У Вас нет прав для создания категории')

    def save(self, commit=True):
        try:
            kat = super(IdeojSociaprojektoKategorioFormo, self).save(commit=False)

            lingvo = InformilojLingvo.objects.get(kodo='ru_RU', forigo=False)
            #nomo = kat.nomo if kat.nomo is not None else \
            #    IdeojSociaprojektoKategorioTitolo(posedanto=kat, enhavo=self.cleaned_data.get('nomo'), lingvo=lingvo)
            #priskribo = kat.priskribo if kat.priskribo is not None else \
            #    IdeojSociaprojektoKategorioPriskribo(posedanto=kat, enhavo=self.cleaned_data.get('priskribo'), lingvo=lingvo)

            if commit:
                kat.nomo = None
                kat.priskribo = None
                kat.save()
                #nomo.save()
                #priskribo.save()

                #kat.nomo = nomo
                #kat.priskribo = priskribo
                kat.save()
            else:
                #kat.nomo = nomo
                #kat.priskribo = priskribo
                return kat

        except InformilojLingvo.DoesNotExist:
            pass


# Форма для идей совета
class IdeojSovetoIdeoFormo(forms.ModelForm):
    nomo_formo = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Название идеи'}), max_length=256,
                                 required=True)

    teksto_formo = forms.CharField(widget=forms.Textarea(attrs={'placeholder': 'Описание идеи', 'cols': 64,
                                                                'rows': 8}), required=True, max_length=500 * 1024)

    komentado_aliro = forms.ModelChoiceField(KomunumojAliro.objects.filter(forigo=False), required=True)

    tipo = forms.ModelChoiceField(IdeojSovetoIdeoTipo.objects.filter(forigo=False), required=True)

    class Meta:
        model = IdeojSovetoIdeo
        fields = ('komentado_aliro', 'fiksa', 'posedanto', 'kategorio', 'autoro', 'tipo')
        exclude = ('nomo', 'teksto')

    def __init__(self, *args, **kwargs):
        super(IdeojSovetoIdeoFormo, self).__init__(*args, **kwargs)
        self.fields['autoro'].widget = forms.HiddenInput()
        self.fields['posedanto'].widget = forms.HiddenInput()
        self.fields['kategorio'].widget = forms.HiddenInput()

    def clean(self):
        data = self.cleaned_data

        # Проверка полномочий пользователя, создающего тему
        posedanto = data['posedanto'] if 'posedanto' in data else None
        uzanto = data['autoro'] if 'autoro' in data else None

        if posedanto is not None and uzanto is not None:
            aliroj = ['moderiganto', 'administranto', 'komunumano', 'membro', 'membro-adm', 'membro-mod']
            aliro = KomunumojSovetoMembro.objects.values('tipo__kodo').filter(
                autoro=uzanto, posedanto=posedanto, tipo__kodo__in=aliroj, forigo=False)

            if aliro.count():
                aliro = aliro[0]

                # Проверяем права на создание прикрепленных тем
                if 'fiksa' in data and data['fiksa'] \
                        and aliro['tipo__kodo'] not in ['moderiganto', 'administranto']:
                    raise forms.ValidationError('Вы не можете создавать прикрепленные идеи')
                return data

        raise forms.ValidationError('Идеи могут создавать только участники сообщества')

    def save(self, commit=True):
        try:
            ideo = super(IdeojSovetoIdeoFormo, self).save(commit=False)

            lingvo = InformilojLingvo.objects.get(kodo='ru_RU', forigo=False)
            #nomo = ideo.nomo if ideo.nomo is not None else \
            #    IdeojSovetoIdeoTitolo(posedanto=ideo, lingvo=lingvo, enhavo=self.cleaned_data.get('nomo_formo'))
            #teksto = ideo.teksto if ideo.teksto is not None else \
            #    IdeojSovetoIdeoTeksto(posedanto=ideo, lingvo=lingvo,
            #                                 enhavo=self.cleaned_data.get('teksto_formo'))

            if commit:
                ideo.nomo = None
                ideo.teksto = None
                ideo.save()
                #nomo.save()
                #teksto.save()

                #ideo.nomo = nomo
                #ideo.teksto = teksto
                ideo.save()
            else:
                #ideo.nomo = nomo
                #ideo.teksto = teksto
                return ideo
        except InformilojLingvo.DoesNotExist:
            pass


# Форма для комментариев идей совета
class IdeojSovetoIdeoKomentoFormo(forms.ModelForm):
    teksto_formo = forms.CharField(widget=forms.Textarea(attrs={'cols': 58, 'rows': 4}), required=True)

    class Meta:
        model = IdeojSovetoIdeoKomento
        fields = ('teksto_formo',)

    def save(self, request=None, soveto=None, commit=True):
        self.instance.posedanto_id = request.POST['ideo']
        self.instance.autoro = request.user
        super(IdeojSovetoIdeoKomentoFormo, self).save(commit=commit)

        if commit:
            kodo = InformilojLingvo.objects.filter(kodo='ru_RU')

            if not kodo is None:
                if not kodo is None:
                    pass
                    #teksto = IdeojSovetoIdeoKomentoTeksto(lingvo=kodo[0], posedanto_id=self.instance.uuid,
                    #                                             enhavo=self.cleaned_data['teksto_formo'])
                    #teksto.save()


# Форма создания категории идей совета
class IdeojSovetoKategorioFormo(forms.ModelForm):
    nomo = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Название категории'}), required=True,
                           max_length=256)

    priskribo = forms.CharField(widget=forms.Textarea(attrs={'placeholder': 'Описание категории', 'cols': 58,
                                                             'rows': 4}), max_length=1024, required=False)

    aliro = forms.ModelChoiceField(KomunumojAliro.objects.filter(forigo=False), required=True)

    tipo = forms.ModelChoiceField(IdeojSovetoKategorioTipo.objects.filter(forigo=False), required=True)

    class Meta:
        model = IdeojSovetoKategorio
        fields = ['tipo', 'aliro', 'posedanto', 'autoro']

    def clean(self):
        data = self.cleaned_data

        if 'autoro' in data and 'posedanto' in data:
            aliroj = ['moderiganto', 'administranto', 'membro-mod', 'membro-adm']
            aliro = KomunumojSovetoMembro.objects.values('tipo__kodo').filter(
                autoro=data.get('autoro'), posedanto=data.get('posedanto'), tipo__kodo__in=aliroj, forigo=False)

            if aliro.count():
                return data

        raise forms.ValidationError('У Вас нет прав для создания категории')

    def save(self, commit=True):
        try:
            kat = super(IdeojSovetoKategorioFormo, self).save(commit=False)

            lingvo = InformilojLingvo.objects.get(kodo='ru_RU', forigo=False)
            #nomo = kat.nomo if kat.nomo is not None else \
            #    IdeojSovetoKategorioTitolo(posedanto=kat, enhavo=self.cleaned_data.get('nomo'), lingvo=lingvo)
            #priskribo = kat.priskribo if kat.priskribo is not None else \
            #    IdeojSovetoKategorioPriskribo(posedanto=kat, enhavo=self.cleaned_data.get('priskribo'),
            #                                         lingvo=lingvo)

            if commit:
                kat.nomo = None
                kat.priskribo = None
                kat.save()
                #nomo.save()
                #priskribo.save()

                #kat.nomo = nomo
                #kat.priskribo = priskribo
                kat.save()
            else:
                #kat.nomo = nomo
                #kat.priskribo = priskribo
                return kat

        except InformilojLingvo.DoesNotExist:
            pass
