from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class IdeojConfig(AppConfig):
    name = 'ideoj'
    verbose_name = _('Ideoj')
