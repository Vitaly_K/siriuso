from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class SontrakojConfig(AppConfig):
    name = 'sontrakoj'
    verbose_name = _('Sontrakoj')
