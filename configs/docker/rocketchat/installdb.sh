#!/usr/bin/env bash

res=`mongo --host mongodb rocketchat < checkdb.js --quiet`

if [ "$res" == 'N' ]; then
    echo "=========== Installing DB ==========="
    mongorestore --host mongodb -d rocketchat --archive=rocketchat.gz
    sleep 60
    echo "=========== END Installing DB ==========="
else
    echo "DB not installed!!!!!"
fi
