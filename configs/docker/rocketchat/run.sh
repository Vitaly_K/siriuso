#!/usr/bin/env bash

if [ -d $ROCKET_HOME/bundle/programs/server/node_modules ]; then
    sleep 30
else
    cd $ROCKET_HOME/bundle/programs/server
    npm install
    cd $ROCKET_HOME
fi

./installdb.sh

cd $ROCKET_HOME/bundle && node main.js
