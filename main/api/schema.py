"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from graphene import relay, ObjectType, Field
from graphene_django import DjangoObjectType
from graphene_permissions.permissions import AllowAny
from django.utils.translation import gettext_lazy as _
from siriuso.api.filters import SiriusoFilterConnectionField
from siriuso.api.types import SiriusoLingvo
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId
from ..models import SiriusoRealeco, SiriusoWablono


class SiriusoRealecoNode(SiriusoAuthNode, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    nomo = Field(SiriusoLingvo)
    priskribo = Field(SiriusoLingvo)

    class Meta:
        model = SiriusoRealeco
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro_id': ['exact', 'in']
        }
        interfaces = (relay.Node,)


class SiriusoWablonoNode(SiriusoAuthNode, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    nomo = Field(SiriusoLingvo)
    priskribo = Field(SiriusoLingvo)

    class Meta:
        model = SiriusoWablono
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro_id': ['exact', 'in']
        }
        interfaces = (relay.Node,)


class SiriusoQuery(ObjectType):
    realecoj = SiriusoFilterConnectionField(
        SiriusoRealecoNode,
        description=_('Возвращает список доступных реальностей')
    )
    realecoj_wablonoj = SiriusoFilterConnectionField(
        SiriusoWablonoNode,
        description=_('Возвращает список доступных шаблонов реальностей')
    )
