"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from siriuso.api.types import ErrorNode
from siriuso.utils import set_enhavo, get_lang_kodo
from .schema import *
from ..models import Uzanto
import graphene


class RedaktuRealeco(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    realeco = graphene.Field(SiriusoRealecoNode)

    class Arguments:
        uuid = graphene.UUID()
        nomo = graphene.String()
        priskribo = graphene.String()
        forigo = graphene.Boolean()
        arkivo = graphene.Boolean()
        publikigo = graphene.Boolean()
        montru_realon = graphene.Boolean()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False,
        message = None
        errors = list()
        realeco = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            # Редактируем запись
            if 'uuid' in kwargs:
                if (uzanto.has_perm('main.povas_shanghi_realecon') or uzanto.has_perm('main.povas_forigi_realecon')
                        or uzanto.has_perms('main.povas_arkivi_realecon')):
                    try:
                        realeco = SiriusoRealeco.objects.get(uuid=kwargs.get('uuid'), forigo=False)

                        if kwargs.get('forigo', False) and not uzanto.has_perm('main.povas_forigi_realecon'):
                            errors.append(ErrorNode(
                                field='forigo',
                                message=_('Ne sufiĉe da rajtoj')
                            ))

                        if 'arkivo' in kwargs and not uzanto.has_perm('main.povas_arkivi_realecon'):
                            errors.append(ErrorNode(
                                field='arkivo',
                                message=_('Ne sufiĉe da rajtoj')
                            ))

                        if (('nomo' in kwargs or 'publikigo' in kwargs or 'montru_realon')
                                and not uzanto.has_perm('main.povas_shanghi_realecon')):
                            errors.append(ErrorNode(
                                field='',
                                message=_('Ne sufiĉe da rajtoj')
                            ))

                        if len(errors):
                            realeco = None
                            message = _('Ne sufiĉe da rajtoj')
                        else:
                            if 'nomo' in kwargs and not kwargs.get('nomo'):
                                errors.append(ErrorNode(
                                    field='nomo',
                                    message=_('Kampo estas bezonata')
                                ))

                            if not len(errors):
                                realeco.lasta_autoro = uzanto
                                realeco.lasta_dato = timezone.now()
                                realeco.arkivo = kwargs.get('arkivo', realeco.arkivo)
                                realeco.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                realeco.forigo = kwargs.get('forigo', realeco.forigo)
                                realeco.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                realeco.publikigo = kwargs.get('publikigo', realeco.publikigo)
                                realeco.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                realeco.montru_realon = kwargs.get('montru_realon', realeco.montru_realon)

                                lingvo = get_lang_kodo(info.context)

                                if 'nomo' in kwargs:
                                    set_enhavo(realeco.nomo, kwargs.get('nomo'), lingvo=lingvo)

                                if 'priskribo' in kwargs:
                                    set_enhavo(realeco.priskribo, kwargs.get('priskribo'), lingvo=lingvo)

                                realeco.save()

                                status = True
                                message = _('Rekordo ŝanĝiĝis')
                            else:
                                realeco = None
                                message = _('Nevalida argumentvaloroj')

                    except SiriusoRealeco.DoesNotExist:
                        realeco = None
                        message = _('Realaĵo ne trovita')
                else:
                    realeco = None
                    message = _('Ne sufiĉe da rajtoj')
            else:
                if uzanto.has_perm('main.povas_krei_realecon'):
                    # Создаём новую запись
                    if 'forigo' in kwargs and kwargs.get('forigo'):
                        errors.append(ErrorNode(
                            field='forigo',
                            message=_('Ne povas esti forigita dum kreado')
                        ))

                    if not kwargs.get('nomo', ''):
                        errors.append(ErrorNode(
                            field='nomo',
                            message=_('Kampo estas bezonata')
                        ))

                    if not len(errors):
                        realeco = SiriusoRealeco.objects.create(
                            autoro=uzanto,
                            forigo=kwargs.get('forigo', False),
                            arkivo=kwargs.get('arkivo', False),
                            publikigo=kwargs.get('publikigo', False),
                            montru_realon=kwargs.get('montru_realon', False),
                        )

                        lingvo = get_lang_kodo(info.context)
                        set_enhavo(realeco.nomo, kwargs.get('nomo'), lingvo=lingvo)

                        if kwargs.get('priskribo', ''):
                            set_enhavo(realeco.priskribo, kwargs.get('priskribo'), lingvo=lingvo)

                        realeco.save()

                        message = _('Rekordo sukcese kreita')
                        status = True
                    else:
                        message = _('Nevalida argumentvaloroj')
                else:
                    message = _('Ne sufiĉe da rajtoj')
        else:
            message = _('Postulas rajtigon')

        return RedaktuRealeco(status=status, message=message, errors=errors, realeco=realeco)


#class RedaktuWablono(graphene.Mutation):
#    pass


class AplikiRealeco(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()

    class Arguments:
        user_id = graphene.Int()
        realeco_uuid = graphene.UUID()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        user = info.context.user

        if user.is_authenticated:
            if user.has_perm('main.povas_shanghi_uzantan_realecon'):
                try:
                    uzanto = Uzanto.objects.get(id=kwargs.get('user_id', user.id), is_active=True, konfirmita=True)

                    if 'realeco_uuid' in kwargs:
                        realeco = SiriusoRealeco.objects.get(uuid=kwargs.get('realeco_uuid'), forigo=False,
                                                             arkivo=False,
                                                             publikigo=True)
                    else:
                        realeco = None

                    uzanto.aktuala_realeco = realeco
                    uzanto.save()

                    status = True
                    message = _('Uzanto moviĝis al nova realaĵo')

                except Uzanto.DoesNotExist:
                    message = _('Uzanto ne trovita')
                except SiriusoRealeco.DoesNotExist:
                    message = _('Realaĵo ne trovita')

            else:
                message = _('Ne sufiĉe da rajtoj')

        else:
            message = _('Postulas rajtigon')

        return AplikiRealeco(status=status, message=message)


class SiriusoMutations(graphene.ObjectType):
    redaktu_realeco = RedaktuRealeco.Field(
        description=_('Создаёт или редактирует запись реальности')
    )
    #redaktu_wablono = RedaktuWablono.Field()
    apliki_realeco = AplikiRealeco.Field(
        description=_('Переключает реальность для пользователя')
    )
