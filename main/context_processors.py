"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.db.models import Q
from .models import Uzanto


def siriuso_context(request):
    if request.user.is_authenticated:
        a_uzanto = (Uzanto.objects
                    .filter(Q(uzantojavataro__forigo=False) | Q(uzantojavataro__forigo__isnull=True),
                         uzantojavataro__aktiva=True,
                         id=request.user.id)
                    .values('id', 'uuid', 'unua_nomo', 'dua_nomo', 'familinomo',
                            'uzantojavataro__bildo', 'uzantojavataro__bildo_min', 'sekso', 'is_admin',
                            'is_active')
                    )

        a_uzanto = a_uzanto[0] if a_uzanto.count() else None

        scontext = {'a_uzanto': a_uzanto}
    else:
        scontext = {}

    return scontext
