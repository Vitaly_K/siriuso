"""
[EO - Internacia Lingvo (Esperanto)]

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.test import TestCase, Client
from main.models import *
from django.utils import timezone
import datetime


# class HomoModelTest(TestCase):
#     def setUp(self):
#         Homo.objects.create(
#             first_name="ivan",
#             last_name="ivanov",
#             email="ivan@tehnokom.su",
#             password="ivan_ivanov",
#             phone="+79999999999",
#             country="russia"
#         )
#
#     def test_date_of_birth_with_future_date(self):
#         date = timezone.now() + datetime.timedelta(days=30)
#         ivan = Homo.objects.get(email="ivan@tehnokom.su")
#         ivan.objects.update(date_of_birth=date)
#         # print(Homo_with_future_date_of_birth)
#         # self.assertIsNone(Homo_with_future_date_of_birth)


class WebPageTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_details(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

