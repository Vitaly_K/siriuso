"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django import template
from django.urls import reverse
from django.utils.translation import ugettext as _


register = template.Library()


@register.inclusion_tag('main/pagination.html')
def pagination(args):
    pagination_context = {}
    pages = []

    if 'total' in args and args['total'] > 0 \
            and 'offset' in args and args['offset'] > 0\
            and 'view_name' in args\
            and 'view_args' in args and type(args['view_args']) == dict\
            and 'view_offset' in args and type(args['view_offset']) == str:
        total_pages = int(int(args['total'])/int(args['offset'])) + (1 if int(args['total'])%int(args['offset']) else 0)
        current_page = int(args['current_offset']/int(args['offset'])) + 1

        # start_page = {'num': '<<', 'title': _('Reen al la supro'),
        #              'url': reverse(view, kwargs=view_kwargs) }
        # pages.append(start_page)

        for page in range(0, total_pages):
            if page:
                args['view_args'][args['view_offset']] = page * args['offset']

            cur = {'num': page + 1, 'title': page + 1, 'url': reverse(args['view_name'],
                                                                             kwargs=args['view_args'])}

            if current_page == page + 1:
                cur['is_current_page'] = True

            pages.append(cur)

        pagination_context['pages'] = pages

    return pagination_context
