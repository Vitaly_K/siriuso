# Generated by Django 2.0.6 on 2018-07-01 12:34

import datetime
import django.core.validators
from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20180614_0729'),
    ]

    operations = [
        migrations.AddField(
            model_name='uzanto',
            name='malbona_retposhto',
            field=models.BooleanField(default=False, verbose_name='Malbona retpoŝta adreso'),
        ),
    ]
