"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from celery.result import AsyncResult
from celery.signals import task_prerun, task_postrun, task_failure
from django.utils import timezone
from django.db import transaction
from .models import SistemaPeto


@task_prerun.connect
def peto_prerun(task_id, *args, **kwargs):
    res = AsyncResult(task_id)

    with transaction.atomic():
        SistemaPeto.objects.select_for_update(of=('self',)).filter(tasko_id=task_id).update(
            statuso=res.state,
            komenca_dato=timezone.now()
        )


@task_postrun.connect
def peto_postrun(task_id, retval, state, *args, **kwargs):
    if not isinstance(retval, Exception):
        with transaction.atomic():
            SistemaPeto.objects.select_for_update(of=('self',)).filter(tasko_id=task_id).update(
                statuso=state,
                rezulto=retval,
                fina_dato=timezone.now()
            )


@task_failure.connect
def peto_failure(task_id, exception, einfo, *args, **kwargs):
    res = AsyncResult(task_id)

    with transaction.atomic():
        SistemaPeto.objects.select_for_update(of=('self',)).filter(tasko_id=task_id).update(
            statuso=res.state,
            rezulto={
                'error': str(exception),
                'einfo': str(einfo)
            },
            fina_dato=timezone.now()
        )
