$(document).ready(function($) {
    //Модальные окна
    //=================================================================
    //Открытие модального окна
    function popup(id, action){
        if(action === 'open'){
            $('.s_popups_bg').addClass('open');
            $('.s_popup[data-id="' + id + '"]').addClass('open');
        }
        else if(action === 'close'){
            $('.s_popups_bg').removeClass('open');
            $('.s_popup').removeClass('open');
        }
    }
    //Открытие модальных окон при клике на кнопки
    $('[data-open-popup]').each(function(){
        var	$e = $(this),
            id = $e.attr('data-open-popup');
        $e.click(function(){
            if($('.s_popup[data-id="' + id + '"][data-close]').hasClass('open')) {
                popup(id, 'close');
            } else {
                popup(id, 'open');
            }
        });
    });
    //Закрывать при клике на крестик
    $('[data-action="close_popup"]').click(function(){
        popup('all', 'close');
    });
    //Закрывать при нажатии на Esc
    $(document).keyup(function(event){
        if(event.keyCode === 27)
            popup('all', 'close');
    });
    var click = false;
    $('.nav-name').click(function(){
        $(".open-menu").toggleClass('open');
        click = !click;
        //console.log(click);
    });
    $('.main').click(function(){
        if(click === true){
            $(".open-menu").toggleClass('open');
            click = !click;
            //console.log(click);
        }
    });
    //Обработчики для полей с принудительным форматированием
});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function toggleBlockForm(form, img_size) {
    img_size = img_size === undefined ? 64 : img_size;
    var target = jQuery(form);

    if(!target.find('.formo-extere').length) {
        target.css('position','relative');
        jQuery('<div class="formo-extere">' +
            '<div><div><img src="/static/main/images/load.gif" width="' + img_size + 'px"></div>' +
            '<div></div></div>').appendTo(target);
    } else {
        target.find('.formo-extere').remove();
        target.css('position','');
    }
}

function scrollTo(obj) {
    let target = $(obj);

    $('html, body').animate({
        scrollTop: target.offset().top
    });
}
