$(document).ready(function($){
	
	//Открывает и закрывает меню
	$('[data-role="menu_trigger"]').click(function(){
		$('[data-role="mobile_sidebar"]').toggleClass('open');
	});
	
	//Закрывает меню
	$(document).click(function(event){
		if($(event.target).closest('[data-role="mobile_sidebar"]').length === 0 &&
            $(event.target).closest('[data-role="menu_trigger"]').length === 0)
			$('[data-role="mobile_sidebar"]').removeClass('open');
	});
});

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        let cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            let cookie = $.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function SiriusoFormatter() {
    let targets = jQuery('input[data-pattern]');

    if(targets.length) {
        targets.each(function (index, target) {
            let cur = jQuery(target);
            let re = cur.attr('data-pattern');
            let placeholder = re.replace(/D/g, '_');

            if(cur.val().length) {
                let val = placeholder;
                let next_pos = val.indexOf('_');

                for(let i = 0; next_pos !== -1 && i < cur.val().length; i++) {
                    if(cur.val()[i].match(/[^\d]/)) {
                        continue;
                    }
                    val = val.replace('_', cur.val()[i]);
                    next_pos = val.indexOf('_')
                }
                cur.val(val);
            } else {
                cur.val(placeholder);
                let pos = placeholder.indexOf('_');
                target.setSelectionRange(pos,pos);
            }

            cur.on('keydown', inputFormatter);
            cur.on('click', function (e) {
                if(e.target.selectionStart === e.target.selectionEnd && e.target.selectionStart !== placeholder.length
                    && placeholder[e.target.selectionStart] !== '_') {
                    let next_pos = -1;
                    for(let i = e.target.selectionStart, j = i - 1; next_pos === -1; i++, j--) {
                        if(j > -1 && placeholder[j] === '_') {
                            next_pos = j;
                        } else if(i < placeholder.length && placeholder[i] === '_') {
                            next_pos = i;
                        }
                    }
                    e.target.setSelectionRange(next_pos,next_pos);
                }
            })
        });
    }
}

function inputFormatter(e) {
    let target = jQuery(e.target);
    console.log(e.which);
    if(e.key.match(/(Escape|Tab|Insert|Pause|PageUp|PageDown|ArrowUp|Home|End|ArrowDown|F[0-9]{1,2})/i) === null
        && !e.ctrlKey && !e.altKey && !e.shiftKey && !e.metaKey) {
        let pos = e.target.selectionStart;
        let pattern = target.attr('data-pattern').replace(/D/g,'_');

        if(pattern[pos] !== '_' && pos < pattern.length) {
            pos = pattern.substring(pos).indexOf('_') + pos;
        }

        switch (e.key){
            case 'Backspace':
                if(e.target.selectionStart !== e.target.selectionEnd) {
                    deleteFormatter(e.target, pattern);
                } else {
                    let prev_pos = -1;
                    for (let i = pos - 1; i >= 0; i--) {
                        if (pattern[i] === '_') {
                            prev_pos = i;
                            let cur_val = target.val();
                            let new_val = cur_val.substring(0, prev_pos) + '_' + cur_val.substring(prev_pos + 1);
                            target.val(new_val);
                            e.target.setSelectionRange(prev_pos, prev_pos);
                            break;
                        }
                    }
                }
                return false;

            case 'Delete':
                e.target.selectionEnd = e.target.selectionEnd === e.target.selectionStart ?
                    e.target.selectionStart + 1 :
                    e.target.selectionEnd;
                deleteFormatter(e.target, pattern);
                return false;

            case 'ArrowLeft':
                for(let i = pos - 1; i > -1; i--){
                    if(pattern[i] === '_') {
                        e.target.setSelectionRange(i,i);
                        break;
                    }
                }
                if(i === -1) {
                    e.target.setSelectionRange(0,0);
                }
                return false;

            case 'ArrowRight':
                for(let i = pos + 1; i < pattern.length; i++){
                    if(pattern[i] === '_') {
                        e.target.setSelectionRange(i,i);
                        break;
                    }
                }
                if(i === pattern.length) {
                    e.target.setSelectionRange(i,i);
                }
                return false;

            default:
                break;
        }

        if(e.key.match(/[0-9]/) !== null && pattern.substring(pos).indexOf('_') !== -1) {
            let seek = pattern.substring(pos+1).indexOf('_') + 1;
            seek = seek ? seek : 1;
            target.val(target.val().substring(0,pos) + e.key + target.val().substring(pos + 1));
            e.target.setSelectionRange(pos + seek,pos + seek);
        }
        return false;
    }
}

function deleteFormatter(etarget, pattern) {
    let target = jQuery(etarget);
    let region = target.val().substring(etarget.selectionStart);
    let selection_pattern = pattern.substring(etarget.selectionStart, etarget.selectionEnd);
    let region_pattern = pattern.substring(etarget.selectionStart);
    let clear_region = '';

    for(let i = 0, j = etarget.selectionEnd - etarget.selectionStart; i < region.length; i++, j--){
        if(region[i].match(/[0-9]/) && j <= 0) {
            clear_region += region[i];
        }
    }

    etarget.selectionEnd = etarget.selectionEnd !== etarget.selectionStart ? etarget.selectionEnd :
        etarget.selectionStart + 1;

     region = '';
    for(i = 0; i < region_pattern.length; i++) {
        if(region_pattern[i] === '_' && clear_region.length) {
            region += clear_region[0];
            clear_region = clear_region.substring(1);
        } else {
            region += region_pattern[i];
        }
    }

    let pos = etarget.selectionStart;
    target.val(target.val().substring(0, etarget.selectionStart) + region);
    etarget.setSelectionRange(pos,pos);
}