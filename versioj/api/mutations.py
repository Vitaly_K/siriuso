"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

import graphene
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.db import transaction
from siriuso.api.types import ErrorNode
from siriuso.utils import set_enhavo, set_priskribo
from muroj.api.schema import MurojEnskribo
from muroj.api.mutations import get_enskribo_by_uuid
from muroj.models import MurojUzantoEnskribo, MuroEnskribo
from ..models import VersioUzantoEnskribo, VersioKomunumoEnskribo, VersioAkademioPagxo, VersioEnciklopedioPagxo, \
        VersioKodoPagxo, VersioKonferencojTemoKomento, VersioKonferencojTemo, VersioKonferencojKategorio, \
        VersioTaskojUzantoProjekto, VersioTaskojUzantoDirekto, VersioTaskojUzantoEtapo, VersioTaskojUzantoTasko, \
        VersioTaskojKomunumoProjekto, VersioTaskojKomunumoDirekto, VersioTaskojKomunumoEtapo, VersioTaskojKomunumoTasko, \
        VersioEsploradojTemoKomento, VersioEsploradojTemo, VersioEsploradojKategorio, VersioMesagxiloMesagxo
from akademio.api.schema import AkademioPagxoNode
from enciklopedio.api.schema import EnciklopedioPagxoNode
from kodo.api.schema import KodoPagxoNode
from konferencoj.api.schema import KonferencojTemoKomentoNode, KonferencojTemoNode, KonferencojKategorioNode
from esploradoj.api.schema import EsploradojTemoKomentoNode, EsploradojTemoNode, EsploradojKategorioNode
from taskoj.api.schema import TaskojUzantoProjektoNode, TaskojUzantoDirektoNode, TaskojUzantoEtapoNode, \
        TaskojUzantoTaskoNode, TaskojKomunumoProjektoNode, TaskojKomunumoDirektoNode, TaskojKomunumoEtapoNode, \
        TaskojKomunumoTaskoNode
from mesagxilo.api.schema import MesagxiloMesagxoNode


class RestarigiVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    enskribo = graphene.Field(MurojEnskribo)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        enskribo = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioUzantoEnskribo, VersioKomunumoEnskribo):
                    try:
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    enskribo = versio.posedanto
                    perm_name = ('versioj.povas_restarigi_uzantan_enskribon_version'
                                 if issubclass(enskribo._meta.model, MurojUzantoEnskribo)
                                 else 'versioj.povas_restarigi_komunuman_enskribon_version')

                    if user.has_perm(perm_name, enskribo) or user.has_perm(perm_name):
                        enskribo.lasta_dato = timezone.now()
                        enskribo.lasta_autoro = user
                        set_enhavo(enskribo.teksto, versio.valoro['teksto'], versio.lingvo.kodo)
                        set_priskribo(enskribo.teksto, versio.valoro['priskribo'], versio.lingvo.kodo)
                        enskribo.save()

                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Enskriba versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiVersion(status=status, message=message, errors=errors, enskribo=enskribo)


class RestarigiAkademioVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    # возвращаем узел страницы академии
    pagxo = graphene.Field(AkademioPagxoNode)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        pagxo = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioAkademioPagxo,):
                    try:
                        # находим требуемую версию, на которую будем переключаться
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                # если не запись версии не найдена, то выдаём ошибку
                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    pagxo = versio.posedanto
                    perm_name = 'versioj.povas_restarigi_akademio_pagxo_version'

                    # Проверяем наличие прав на переключение версий
                    if user.has_perm(perm_name, pagxo) or user.has_perm(perm_name):
                        # иобновляем поля полседнего изменения
                        pagxo.lasta_dato = timezone.now()
                        pagxo.lasta_autoro = user
                        # восстанавливаем значения нужных полей из версии
                        set_enhavo(pagxo.teksto, versio.valoro['teksto'], versio.lingvo.kodo)
                        set_enhavo(pagxo.nomo, versio.valoro['nomo'], versio.lingvo.kodo)
                        # set_priskribo(pagxo.teksto, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # сохраняем изменения в оригенальном объекте
                        pagxo.save()

                        # помечаем все вресии, как неактивные
                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        # помечаем требуемую версию, как активную
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Pagxa versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiAkademioVersion(status=status, message=message, errors=errors, pagxo=pagxo)

class RestarigiEnciklopedioVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    # возвращаем узел страницы академии
    pagxo = graphene.Field(EnciklopedioPagxoNode)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        pagxo = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioEnciklopedioPagxo,):
                    try:
                        # находим требуемую версию, на которую будем переключаться
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                # если не запись версии не найдена, то выдаём ошибку
                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    pagxo = versio.posedanto
                    perm_name = 'versioj.povas_restarigi_enciklopedio_pagxo_version'

                    # Проверяем наличие прав на переключение версий
                    if user.has_perm(perm_name, pagxo) or user.has_perm(perm_name):
                        # иобновляем поля полседнего изменения
                        pagxo.lasta_dato = timezone.now()
                        pagxo.lasta_autoro = user
                        # восстанавливаем значения нужных полей из версии
                        set_enhavo(pagxo.teksto, versio.valoro['teksto'], versio.lingvo.kodo)
                        set_enhavo(pagxo.nomo, versio.valoro['nomo'], versio.lingvo.kodo)
                        # set_priskribo(pagxo.teksto, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # сохраняем изменения в оригенальном объекте
                        pagxo.save()

                        # помечаем все вресии, как неактивные
                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        # помечаем требуемую версию, как активную
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Pagxa versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiEnciklopedioVersion(status=status, message=message, errors=errors, pagxo=pagxo)

class RestarigiKodoVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    # возвращаем узел страницы кодекса
    pagxo = graphene.Field(KodoPagxoNode)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        pagxo = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioKodoPagxo,):
                    try:
                        # находим требуемую версию, на которую будем переключаться
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                # если не запись версии не найдена, то выдаём ошибку
                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    pagxo = versio.posedanto
                    perm_name = 'versioj.povas_restarigi_kodo_pagxo_version'

                    # Проверяем наличие прав на переключение версий
                    if user.has_perm(perm_name, pagxo) or user.has_perm(perm_name):
                        # иобновляем поля полседнего изменения
                        pagxo.lasta_dato = timezone.now()
                        pagxo.lasta_autoro = user
                        # восстанавливаем значения нужных полей из версии
                        set_enhavo(pagxo.teksto, versio.valoro['teksto'], versio.lingvo.kodo)
                        set_enhavo(pagxo.nomo, versio.valoro['nomo'], versio.lingvo.kodo)
                        # set_priskribo(pagxo.teksto, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # сохраняем изменения в оригенальном объекте
                        pagxo.save()

                        # помечаем все вресии, как неактивные
                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        # помечаем требуемую версию, как активную
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Pagxa versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiKodoVersion(status=status, message=message, errors=errors, pagxo=pagxo)

class RestarigiKonferencojTemoKomentoVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    # возвращаем комментарий темы конференции
    komento = graphene.Field(KonferencojTemoKomentoNode)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        komento = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioKonferencojTemoKomento,):
                    try:
                        # находим требуемую версию, на которую будем переключаться
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                # если не запись версии не найдена, то выдаём ошибку
                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    komento = versio.posedanto
                    perm_name = 'versioj.povas_restarigi_konferencoj_temon_komenton_version'

                    # Проверяем наличие прав на переключение версий
                    if user.has_perm(perm_name, komento) or user.has_perm(perm_name):
                        # иобновляем поля полседнего изменения
                        komento.lasta_dato = timezone.now()
                        komento.lasta_autoro = user
                        # восстанавливаем значения нужных полей из версии
                        set_enhavo(komento.teksto, versio.valoro['teksto'], versio.lingvo.kodo)
                        # set_priskribo(komento.teksto, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # сохраняем изменения в оригенальном объекте
                        komento.save()

                        # помечаем все вресии, как неактивные
                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        # помечаем требуемую версию, как активную
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Komento temo versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiKonferencojTemoKomentoVersion(status=status, message=message, errors=errors, komento=komento)

class RestarigiKonferencojTemoVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    # возвращаем тему конференции
    temo = graphene.Field(KonferencojTemoNode)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        temo = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioKonferencojTemo,):
                    try:
                        # находим требуемую версию, на которую будем переключаться
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                # если запись версии не найдена, то выдаём ошибку
                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    temo = versio.posedanto
                    perm_name = 'versioj.povas_restarigi_konferencoj_temon_version'

                    # Проверяем наличие прав на переключение версий
                    if user.has_perm(perm_name, temo) or user.has_perm(perm_name):
                        # и обновляем поля последнего изменения
                        temo.lasta_dato = timezone.now()
                        temo.lasta_autoro = user
                        # восстанавливаем значения нужных полей из версии
                        set_enhavo(temo.nomo, versio.valoro['nomo'], versio.lingvo.kodo)
                        set_enhavo(temo.priskribo, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # set_priskribo(temo.teksto, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # сохраняем изменения в оригинальном объекте
                        temo.save()

                        # помечаем все вресии, как неактивные
                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        # помечаем требуемую версию, как активную
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Temo versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiKonferencojTemoVersion(status=status, message=message, errors=errors, temo=temo)

class RestarigiKonferencojKategorioVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    # возвращаем категорию конференции
    kategorio = graphene.Field(KonferencojKategorioNode)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        kategorio = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioKonferencojKategorio,):
                    try:
                        # находим требуемую версию, на которую будем переключаться
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                # если не запись версии не найдена, то выдаём ошибку
                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    kategorio = versio.posedanto
                    perm_name = 'versioj.povas_restarigi_konferencoj_kategorion_version'

                    # Проверяем наличие прав на переключение версий
                    if user.has_perm(perm_name, kategorio) or user.has_perm(perm_name):
                        # иобновляем поля полседнего изменения
                        kategorio.lasta_dato = timezone.now()
                        kategorio.lasta_autoro = user
                        # восстанавливаем значения нужных полей из версии
                        set_enhavo(kategorio.nomo, versio.valoro['nomo'], versio.lingvo.kodo)
                        set_enhavo(kategorio.priskribo, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # set_priskribo(kategorio.teksto, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # сохраняем изменения в оригенальном объекте
                        kategorio.save()

                        # помечаем все вресии, как неактивные
                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        # помечаем требуемую версию, как активную
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Kategorio versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiKonferencojKategorioVersion(status=status, message=message, errors=errors, kategorio=kategorio)

class RestarigiEsploradojTemoKomentoVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    # возвращаем комментарий темы исследования
    komento = graphene.Field(EsploradojTemoKomentoNode)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        komento = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioEsploradojTemoKomento,):
                    try:
                        # находим требуемую версию, на которую будем переключаться
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                # если не запись версии не найдена, то выдаём ошибку
                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    komento = versio.posedanto
                    perm_name = 'versioj.povas_restarigi_esploradoj_temon_komenton_version'

                    # Проверяем наличие прав на переключение версий
                    if user.has_perm(perm_name, komento) or user.has_perm(perm_name):
                        # иобновляем поля полседнего изменения
                        komento.lasta_dato = timezone.now()
                        komento.lasta_autoro = user
                        # восстанавливаем значения нужных полей из версии
                        set_enhavo(komento.teksto, versio.valoro['teksto'], versio.lingvo.kodo)
                        # set_priskribo(komento.teksto, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # сохраняем изменения в оригенальном объекте
                        komento.save()

                        # помечаем все вресии, как неактивные
                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        # помечаем требуемую версию, как активную
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Komento temo versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiEsploradojTemoKomentoVersion(status=status, message=message, errors=errors, komento=komento)

class RestarigiEsploradojTemoVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    # возвращаем тему исследования
    temo = graphene.Field(EsploradojTemoNode)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        temo = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioEsploradojTemo,):
                    try:
                        # находим требуемую версию, на которую будем переключаться
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                # если запись версии не найдена, то выдаём ошибку
                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    temo = versio.posedanto
                    perm_name = 'versioj.povas_restarigi_esploradoj_temon_version'

                    # Проверяем наличие прав на переключение версий
                    if user.has_perm(perm_name, temo) or user.has_perm(perm_name):
                        # и обновляем поля последнего изменения
                        temo.lasta_dato = timezone.now()
                        temo.lasta_autoro = user
                        # восстанавливаем значения нужных полей из версии
                        set_enhavo(temo.nomo, versio.valoro['nomo'], versio.lingvo.kodo)
                        set_enhavo(temo.priskribo, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # set_priskribo(temo.teksto, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # сохраняем изменения в оригинальном объекте
                        temo.save()

                        # помечаем все вресии, как неактивные
                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        # помечаем требуемую версию, как активную
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Temo versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiEsploradojTemoVersion(status=status, message=message, errors=errors, temo=temo)

class RestarigiEsploradojKategorioVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    # возвращаем категорию исследования
    kategorio = graphene.Field(EsploradojKategorioNode)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        kategorio = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioEsploradojKategorio,):
                    try:
                        # находим требуемую версию, на которую будем переключаться
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                # если не запись версии не найдена, то выдаём ошибку
                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    kategorio = versio.posedanto
                    perm_name = 'versioj.povas_restarigi_esploradoj_kategorion_version'

                    # Проверяем наличие прав на переключение версий
                    if user.has_perm(perm_name, kategorio) or user.has_perm(perm_name):
                        # иобновляем поля полседнего изменения
                        kategorio.lasta_dato = timezone.now()
                        kategorio.lasta_autoro = user
                        # восстанавливаем значения нужных полей из версии
                        set_enhavo(kategorio.nomo, versio.valoro['nomo'], versio.lingvo.kodo)
                        set_enhavo(kategorio.priskribo, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # set_priskribo(kategorio.teksto, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # сохраняем изменения в оригенальном объекте
                        kategorio.save()

                        # помечаем все вресии, как неактивные
                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        # помечаем требуемую версию, как активную
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Kategorio versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiEsploradojKategorioVersion(status=status, message=message, errors=errors, kategorio=kategorio)

class RestarigiTaskojUzantoProjektoVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    # возвращаем проект задачи пользователя
    tasko = graphene.Field(TaskojUzantoProjektoNode)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        tasko = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioTaskojUzantoProjekto,):
                    try:
                        # находим требуемую версию, на которую будем переключаться
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                # если не запись версии не найдена, то выдаём ошибку
                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    tasko = versio.posedanto
                    perm_name = 'versioj.povas_restarigi_taskoj_uzantojn_projektojn_version'

                    # Проверяем наличие прав на переключение версий
                    if user.has_perm(perm_name, tasko) or user.has_perm(perm_name):
                        # и обновляем поля последнего изменения
                        tasko.lasta_dato = timezone.now()
                        tasko.lasta_autoro = user
                        # восстанавливаем значения нужных полей из версии
                        set_enhavo(tasko.priskribo, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # сохраняем изменения в оригинальном объекте
                        tasko.save()

                        # помечаем все вресии, как неактивные
                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        # помечаем требуемую версию, как активную
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Taskoj uzanto projekto versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiTaskojUzantoProjektoVersion(status=status, message=message, errors=errors, tasko=tasko)

class RestarigiTaskojUzantoDirektoVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    # возвращаем направления задачи пользователя
    tasko = graphene.Field(TaskojUzantoDirektoNode)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        tasko = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioTaskojUzantoDirekto,):
                    try:
                        # находим требуемую версию, на которую будем переключаться
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                # если не запись версии не найдена, то выдаём ошибку
                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    tasko = versio.posedanto
                    perm_name = 'versioj.povas_restarigi_taskoj_uzantojn_direktojn_version'

                    # Проверяем наличие прав на переключение версий
                    if user.has_perm(perm_name, tasko) or user.has_perm(perm_name):
                        # и обновляем поля последнего изменения
                        tasko.lasta_dato = timezone.now()
                        tasko.lasta_autoro = user
                        # восстанавливаем значения нужных полей из версии
                        set_enhavo(tasko.priskribo, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # сохраняем изменения в оригинальном объекте
                        tasko.save()

                        # помечаем все вресии, как неактивные
                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        # помечаем требуемую версию, как активную
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Taskoj uzanto direkto versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiTaskojUzantoDirektoVersion(status=status, message=message, errors=errors, tasko=tasko)

class RestarigiTaskojUzantoEtapoVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    # возвращаем этап задачи пользователя
    tasko = graphene.Field(TaskojUzantoEtapoNode)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        tasko = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioTaskojUzantoEtapo,):
                    try:
                        # находим требуемую версию, на которую будем переключаться
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                # если не запись версии не найдена, то выдаём ошибку
                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    tasko = versio.posedanto
                    perm_name = 'versioj.povas_restarigi_taskoj_uzantojn_etapojn_version'

                    # Проверяем наличие прав на переключение версий
                    if user.has_perm(perm_name, tasko) or user.has_perm(perm_name):
                        # и обновляем поля последнего изменения
                        tasko.lasta_dato = timezone.now()
                        tasko.lasta_autoro = user
                        # восстанавливаем значения нужных полей из версии
                        set_enhavo(tasko.priskribo, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # сохраняем изменения в оригинальном объекте
                        tasko.save()

                        # помечаем все вресии, как неактивные
                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        # помечаем требуемую версию, как активную
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Taskoj uzanto etapo versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiTaskojUzantoEtapoVersion(status=status, message=message, errors=errors, tasko=tasko)

class RestarigiTaskojUzantoTaskoVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    # возвращаем задачу пользователя
    tasko = graphene.Field(TaskojUzantoTaskoNode)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        tasko = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioTaskojUzantoTasko,):
                    try:
                        # находим требуемую версию, на которую будем переключаться
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                # если не запись версии не найдена, то выдаём ошибку
                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    tasko = versio.posedanto
                    perm_name = 'versioj.povas_restarigi_taskoj_uzantojn_taskojn_version'

                    # Проверяем наличие прав на переключение версий
                    if user.has_perm(perm_name, tasko) or user.has_perm(perm_name):
                        # и обновляем поля последнего изменения
                        tasko.lasta_dato = timezone.now()
                        tasko.lasta_autoro = user
                        # восстанавливаем значения нужных полей из версии
                        set_enhavo(tasko.priskribo, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # сохраняем изменения в оригинальном объекте
                        tasko.save()

                        # помечаем все вресии, как неактивные
                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        # помечаем требуемую версию, как активную
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Taskoj uzanto tasko versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiTaskojUzantoTaskoVersion(status=status, message=message, errors=errors, tasko=tasko)









class RestarigiTaskojKomunumoProjektoVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    # возвращаем проект задачи пользователя
    tasko = graphene.Field(TaskojKomunumoProjektoNode)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        tasko = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioTaskojKomunumoProjekto,):
                    try:
                        # находим требуемую версию, на которую будем переключаться
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                # если не запись версии не найдена, то выдаём ошибку
                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    tasko = versio.posedanto
                    perm_name = 'versioj.povas_restarigi_taskoj_komunumojn_projektojn_version'

                    # Проверяем наличие прав на переключение версий
                    if user.has_perm(perm_name, tasko) or user.has_perm(perm_name):
                        # и обновляем поля последнего изменения
                        tasko.lasta_dato = timezone.now()
                        tasko.lasta_autoro = user
                        # восстанавливаем значения нужных полей из версии
                        set_enhavo(tasko.priskribo, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # сохраняем изменения в оригинальном объекте
                        tasko.save()

                        # помечаем все вресии, как неактивные
                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        # помечаем требуемую версию, как активную
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Taskoj komunumo projekto versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiTaskojKomunumoProjektoVersion(status=status, message=message, errors=errors, tasko=tasko)

class RestarigiTaskojKomunumoDirektoVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    # возвращаем направления задачи пользователя
    tasko = graphene.Field(TaskojKomunumoDirektoNode)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        tasko = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioTaskojKomunumoDirekto,):
                    try:
                        # находим требуемую версию, на которую будем переключаться
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                # если не запись версии не найдена, то выдаём ошибку
                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    tasko = versio.posedanto
                    perm_name = 'versioj.povas_restarigi_taskoj_komunumojn_direktojn_version'

                    # Проверяем наличие прав на переключение версий
                    if user.has_perm(perm_name, tasko) or user.has_perm(perm_name):
                        # и обновляем поля последнего изменения
                        tasko.lasta_dato = timezone.now()
                        tasko.lasta_autoro = user
                        # восстанавливаем значения нужных полей из версии
                        set_enhavo(tasko.priskribo, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # сохраняем изменения в оригинальном объекте
                        tasko.save()

                        # помечаем все вресии, как неактивные
                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        # помечаем требуемую версию, как активную
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Taskoj komunumo direkto versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiTaskojKomunumoDirektoVersion(status=status, message=message, errors=errors, tasko=tasko)

class RestarigiTaskojKomunumoEtapoVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    # возвращаем этап задачи пользователя
    tasko = graphene.Field(TaskojKomunumoEtapoNode)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        tasko = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioTaskojKomunumoEtapo,):
                    try:
                        # находим требуемую версию, на которую будем переключаться
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                # если не запись версии не найдена, то выдаём ошибку
                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    tasko = versio.posedanto
                    perm_name = 'versioj.povas_restarigi_taskoj_komunumojn_etapojn_version'

                    # Проверяем наличие прав на переключение версий
                    if user.has_perm(perm_name, tasko) or user.has_perm(perm_name):
                        # и обновляем поля последнего изменения
                        tasko.lasta_dato = timezone.now()
                        tasko.lasta_autoro = user
                        # восстанавливаем значения нужных полей из версии
                        set_enhavo(tasko.priskribo, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # сохраняем изменения в оригинальном объекте
                        tasko.save()

                        # помечаем все вресии, как неактивные
                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        # помечаем требуемую версию, как активную
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Taskoj komunumo etapo versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiTaskojKomunumoEtapoVersion(status=status, message=message, errors=errors, tasko=tasko)

class RestarigiTaskojKomunumoTaskoVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    # возвращаем задачу пользователя
    tasko = graphene.Field(TaskojKomunumoTaskoNode)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        tasko = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioTaskojKomunumoTasko,):
                    try:
                        # находим требуемую версию, на которую будем переключаться
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                # если не запись версии не найдена, то выдаём ошибку
                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    tasko = versio.posedanto
                    perm_name = 'versioj.povas_restarigi_taskoj_komunumojn_taskojn_version'

                    # Проверяем наличие прав на переключение версий
                    if user.has_perm(perm_name, tasko) or user.has_perm(perm_name):
                        # и обновляем поля последнего изменения
                        tasko.lasta_dato = timezone.now()
                        tasko.lasta_autoro = user
                        # восстанавливаем значения нужных полей из версии
                        set_enhavo(tasko.priskribo, versio.valoro['priskribo'], versio.lingvo.kodo)
                        # сохраняем изменения в оригинальном объекте
                        tasko.save()

                        # помечаем все вресии, как неактивные
                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        # помечаем требуемую версию, как активную
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Taskoj komunumo tasko versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiTaskojKomunumoTaskoVersion(status=status, message=message, errors=errors, tasko=tasko)

class RestarigiMesagxiloMesagxoVersion(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    # возвращаем задачу пользователя
    tasko = graphene.Field(MesagxiloMesagxoNode)

    class Arguments:
        versio_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, versio_uuid):
        status = False
        message = None
        errors = list()
        tasko = None
        versio = None
        user = info.context.user

        if user.is_authenticated:
            with transaction.atomic():
                for model in (VersioMesagxiloMesagxo,):
                    try:
                        # находим требуемую версию, на которую будем переключаться
                        versio = model.objects.select_for_update(of=('self',)).get(uuid=versio_uuid)
                    except model.DoesNotExist:
                        pass

                # если не запись версии не найдена, то выдаём ошибку
                if not versio:
                    errors.append(ErrorNode(
                        field='versio_uuid',
                        message=_('Versio ne trovita')
                    ))
                else:
                    tasko = versio.posedanto
                    perm_name = 'versioj.povas_restarigi_mesagxilon_mesagxon_version'

                    # Проверяем наличие прав на переключение версий
                    if user.has_perm(perm_name, tasko) or user.has_perm(perm_name):
                        # и обновляем поля последнего изменения
                        tasko.lasta_dato = timezone.now()
                        tasko.lasta_autoro = user
                        # восстанавливаем значения нужных полей из версии
                        set_enhavo(tasko.teksto, versio.valoro['teksto'], versio.lingvo.kodo)
                        # сохраняем изменения в оригинальном объекте
                        tasko.save()

                        # помечаем все вресии, как неактивные
                        versio._meta.model.objects.filter(
                            posedanto=versio.posedanto,
                            lingvo=versio.lingvo,
                            aktiva=True
                        ).update(aktiva=False)
                        # помечаем требуемую версию, как активную
                        versio.aktiva = True
                        versio.save()

                        status = True
                        message = _('Mesagxilo mesagxo versio restarigita')
                    else:
                        message = _('Ne sufiĉe da rajtoj')

            if len(errors):
                message = _('Nevalida argumentvaloroj')
        else:
            message = _('Postulas rajtigon')

        return RestarigiMesagxiloMesagxoVersion(status=status, message=message, errors=errors, tasko=tasko)



class VersioMutations(graphene.ObjectType):
    restarigi_versio = RestarigiVersion.Field()
    restarigi_akademio_versio = RestarigiAkademioVersion.Field()
    restarigi_enciklopedio_versio = RestarigiEnciklopedioVersion.Field()
    restarigi_kodo_versio = RestarigiKodoVersion.Field()
    restarigi_konferencoj_temo_komento_versio = RestarigiKonferencojTemoKomentoVersion.Field()
    restarigi_konferencoj_temo_versio = RestarigiKonferencojTemoVersion.Field()
    restarigi_konferencoj_kategorio_versio = RestarigiKonferencojKategorioVersion.Field()
    restarigi_esploradoj_temo_komento_versio = RestarigiEsploradojTemoKomentoVersion.Field()
    restarigi_esploradoj_temo_versio = RestarigiEsploradojTemoVersion.Field()
    restarigi_esploradoj_kategorio_versio = RestarigiEsploradojKategorioVersion.Field()
    restarigi_taskoj_uzanto_projekto_versio = RestarigiTaskojUzantoProjektoVersion.Field()
    restarigi_taskoj_uzanto_direkto_versio = RestarigiTaskojUzantoDirektoVersion.Field()
    restarigi_taskoj_uzanto_etapo_versio = RestarigiTaskojUzantoEtapoVersion.Field()
    restarigi_taskoj_uzanto_tasko_versio = RestarigiTaskojUzantoTaskoVersion.Field()
    restarigi_taskoj_komunumo_projekto_versio = RestarigiTaskojKomunumoProjektoVersion.Field()
    restarigi_taskoj_komunumo_direkto_versio = RestarigiTaskojKomunumoDirektoVersion.Field()
    restarigi_taskoj_komunumo_etapo_versio = RestarigiTaskojKomunumoEtapoVersion.Field()
    restarigi_taskoj_komunumo_tasko_versio = RestarigiTaskojKomunumoTaskoVersion.Field()
    restarigi_mesagxilo_mesagxo_versio = RestarigiMesagxiloMesagxoVersion.Field()
