"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

import graphene
from graphene_django import DjangoObjectType
from graphene_permissions.permissions import AllowAny
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId
from ..models import *


class VersioUzantoEnskriboNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioUzantoEnskribo
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro.get('teksto')

    def resolve_priskribo(self, info):
        return self.valoro.get('priskribo')


class VersioKomunumoEnskriboNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioKomunumoEnskribo
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']


class VersioEnskribo(graphene.Union):
    class Meta:
        types = (VersioKomunumoEnskriboNode, VersioKomunumoEnskriboNode)


class VersioEnskriboUnionConnection(graphene.relay.Connection):
    class Meta:
        node = VersioEnskribo


class VersioAkademioPagxoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioAkademioPagxo
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']


class VersioAkademio(graphene.Union):
    class Meta:
        types = (VersioAkademioPagxoNode,)


class VersioAkademioUnionConnection(graphene.relay.Connection):
    class Meta:
        node = VersioAkademio


class VersioEnciklopedioPagxoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioEnciklopedioPagxo
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']


class VersioEnciklopedio(graphene.Union):
    class Meta:
        types = (VersioEnciklopedioPagxoNode,)


class VersioEnciklopedioPagxoUnionConnection(graphene.relay.Connection):
    class Meta:
        node = VersioEnciklopedio

class VersioKodoPagxoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioKodoPagxo
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioKodo(graphene.Union):
    class Meta:
        types = (VersioKodoPagxoNode,)

class VersioKodoPagxoUnionConnection(graphene.relay.Connection):
    class Meta:
        node = VersioKodo


class VersioKonferencojTemoKomentoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioKonferencojTemoKomento
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioKonferencojTemoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioKonferencojTemo
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioKonferencojKategorioNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioKonferencojKategorio
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioEsploradojTemoKomentoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioEsploradojTemoKomento
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioEsploradojTemoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioEsploradojTemo
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioEsploradojKategorioNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioEsploradojKategorio
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioTaskojUzantoProjektoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioTaskojUzantoProjekto
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioTaskojUzantoDirektoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioTaskojUzantoDirekto
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioTaskojUzantoEtapoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioTaskojUzantoEtapo
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioTaskojUzantoTaskoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioTaskojUzantoTasko
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioTaskojKomunumoProjektoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioTaskojKomunumoProjekto
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioTaskojKomunumoDirektoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioTaskojKomunumoDirekto
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioTaskojKomunumoEtapoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioTaskojKomunumoEtapo
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioTaskojKomunumoTaskoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioTaskojKomunumoTasko
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioMesagxiloMesagxoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioMesagxiloMesagxo
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

