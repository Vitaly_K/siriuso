# Generated by Django 2.0.13 on 2019-09-19 02:39

from django.conf import settings
import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('informiloj', '0011_auto_20190609_0303'),
        ('esploradoj', '0009_auto_20190919_0239'),
        ('versioj', '0006_auto_20190911_0843'),
    ]

    operations = [
        migrations.CreateModel(
            name='VersioEsploradojKategorio',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('id', models.IntegerField(default=0, verbose_name='ID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True)),
                ('valoro', django.contrib.postgres.fields.jsonb.JSONField(verbose_name='Valoro')),
                ('aktiva', models.BooleanField(default=False, verbose_name='Aktiva versio')),
                ('autoro', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Autoro')),
                ('lingvo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='informiloj.InformilojLingvo', verbose_name='Lingvo')),
                ('posedanto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='esploradoj.EsploradojKategorio', verbose_name='Originala esploradoj kategorio')),
            ],
            options={
                'verbose_name': 'Versio esploradoj kategorio',
                'verbose_name_plural': 'Versioj esploradoj kategorioj',
                'db_table': 'versioj_esploradoj_kategorio',
                'permissions': (('povas_vidi_esploradoj_kategorion_version', 'Povas vidi esploradoj kategorion version'), ('povas_forigi_esploradoj_kategorion_version', 'Povas forigi esploradoj kategorion version'), ('povas_restarigi_esploradoj_kategorion_version', 'Povas restarigi esploradoj kategorion version')),
            },
        ),
        migrations.CreateModel(
            name='VersioEsploradojTemo',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('id', models.IntegerField(default=0, verbose_name='ID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True)),
                ('valoro', django.contrib.postgres.fields.jsonb.JSONField(verbose_name='Valoro')),
                ('aktiva', models.BooleanField(default=False, verbose_name='Aktiva versio')),
                ('autoro', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Autoro')),
                ('lingvo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='informiloj.InformilojLingvo', verbose_name='Lingvo')),
                ('posedanto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='esploradoj.EsploradojTemo', verbose_name='Originala esploradoj temo')),
            ],
            options={
                'verbose_name': 'Versio esploradoj temo',
                'verbose_name_plural': 'Versioj esploradoj temoj',
                'db_table': 'versioj_esploradoj_temo',
                'permissions': (('povas_vidi_esploradoj_temon_version', 'Povas vidi esploradoj temon version'), ('povas_forigi_esploradoj_temon_version', 'Povas forigi esploradoj temon version'), ('povas_restarigi_esploradoj_temon_version', 'Povas restarigi esploradoj temon version')),
            },
        ),
        migrations.CreateModel(
            name='VersioEsploradojTemoKomento',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('id', models.IntegerField(default=0, verbose_name='ID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True)),
                ('valoro', django.contrib.postgres.fields.jsonb.JSONField(verbose_name='Valoro')),
                ('aktiva', models.BooleanField(default=False, verbose_name='Aktiva versio')),
                ('autoro', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Autoro')),
                ('lingvo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='informiloj.InformilojLingvo', verbose_name='Lingvo')),
                ('posedanto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='esploradoj.EsploradojTemoKomento', verbose_name='Originala esploradoj temo komento')),
            ],
            options={
                'verbose_name': 'Versio esploradoj temo komento',
                'verbose_name_plural': 'Versioj esploradoj temoj komentoj',
                'db_table': 'versioj_esploradoj_temo_komento',
                'permissions': (('povas_vidi_esploradoj_temon_komenton_version', 'Povas vidi esploradoj temon komenton version'), ('povas_forigi_esploradoj_temon_komenton_version', 'Povas forigi esploradoj temon komenton version'), ('povas_restarigi_esploradoj_temon_komenton_version', 'Povas restarigi esploradoj temon komenton version')),
            },
        ),
        migrations.AlterIndexTogether(
            name='versioesploradojtemokomento',
            index_together={('posedanto', 'id', 'lingvo')},
        ),
        migrations.AlterIndexTogether(
            name='versioesploradojtemo',
            index_together={('posedanto', 'id', 'lingvo')},
        ),
        migrations.AlterIndexTogether(
            name='versioesploradojkategorio',
            index_together={('posedanto', 'id', 'lingvo')},
        ),
    ]
