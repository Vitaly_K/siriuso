from django.apps import AppConfig


class VersiojConfig(AppConfig):
    name = 'versioj'

    def ready(self):
        import versioj.signals
