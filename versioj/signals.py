"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2019 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save
from django.db import transaction
from siriuso.utils import get_enhavo, get_priskribo
from informiloj.utils import get_infolingvo_by_code
from .models import *

from muroj.models import MurojUzantoEnskribo, MuroEnskribo
from akademio.models import AkademioPagxo
from enciklopedio.models import EnciklopedioPagxo
from kodo.models import KodoPagxo
from konferencoj.models import KonferencojTemoKomento, KonferencojTemo, KonferencojKategorio
from esploradoj.models import EsploradojTemoKomento, EsploradojTemo, EsploradojKategorio
from taskoj.models import TaskojUzantoProjekto, TaskojUzantoDirekto, TaskojUzantoEtapo, TaskojUzantoTasko, \
        TaskojKomunumoProjekto, TaskojKomunumoDirekto, TaskojKomunumoEtapo, TaskojKomunumoTasko
from mesagxilo.models import MesagxiloMesagxo

@receiver(pre_save, sender=MurojUzantoEnskribo)
def savi_version_uzanto_enskribo(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.teksto['lingvo'].keys()):
        for lingvo_kodo in instance.teksto['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioUzantoEnskribo.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True
                    ).update(aktiva=False)

                VersioUzantoEnskribo.objects.create(
                    autoro=instance.posedanto,
                    posedanto=instance,
                    lingvo=lingvo,
                    valoro={
                        'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0],
                        'priskribo': get_priskribo(instance.teksto, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioUzantoEnskribo.objects.filter(posedanto=instance).delete()
    elif 'teksto' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = MurojUzantoEnskribo.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.teksto['lingvo'].keys():
                if lingvo_kodo not in aktuala.teksto['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioUzantoEnskribo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True
                            ).update(aktiva=False)

                        VersioUzantoEnskribo.objects.create(
                            autoro=instance.posedanto,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0],
                                'priskribo': get_priskribo(instance.teksto, lingvo_kodo, empty_values=True)[0]
                            }
                        )
                elif get_enhavo(instance.teksto, lingvo_kodo)[0] != get_enhavo(aktuala.teksto, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioUzantoEnskribo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True
                            ).update(aktiva=False)

                        VersioUzantoEnskribo.objects.create(
                            autoro=instance.posedanto,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0],
                                'priskribo': get_priskribo(instance.teksto, lingvo_kodo, empty_values=True)[0]
                            }
                        )

        except MurojUzantoEnskribo.DoesNotExist:
            pass


@receiver(pre_save, sender=MuroEnskribo)
def savi_version_kom_enskribo(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.teksto['lingvo'].keys()):
        for lingvo_kodo in instance.teksto['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioKomunumoEnskribo.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioKomunumoEnskribo.objects.create(
                    autoro=instance.autoro,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0],
                        'priskribo': get_priskribo(instance.teksto, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioKomunumoEnskribo.objects.filter(posedanto=instance).delete()
    elif 'teksto' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = MuroEnskribo.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.teksto['lingvo'].keys():
                if lingvo_kodo not in aktuala.teksto['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioKomunumoEnskribo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioKomunumoEnskribo.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0],
                                'priskribo': get_priskribo(instance.teksto, lingvo_kodo, empty_values=True)[0]
                            }
                        )
                elif get_enhavo(instance.teksto, lingvo_kodo)[0] != get_enhavo(aktuala.teksto, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioKomunumoEnskribo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioKomunumoEnskribo.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0],
                                'priskribo': get_priskribo(instance.teksto, lingvo_kodo, empty_values=True)[0]
                            }
                        )

        except MuroEnskribo.DoesNotExist:
            pass


@receiver(pre_save, sender=AkademioPagxo)
def savi_version_akademio_pagxo(instance, **kwargs):
    # Если объект только создан
    if kwargs.get('created', False) and len(instance.teksto['lingvo'].keys()):
        for lingvo_kodo in instance.teksto['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioAkademioPagxo.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioAkademioPagxo.objects.create(
                    autoro=instance.autoro,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0],
                        'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        # если объект удаляется, то удаляем все его версии для экономии места
        VersioAkademioPagxo.objects.filter(posedanto=instance).delete()
    elif 'teksto' in (kwargs.get('update_fields') or dict()) or 'nomo' in (kwargs.get('update_fields') or dict()):
        # если произошло изменение объекта и изменение коснулось поля 'teksto' или 'nomo'
        try:
            # находим оригенальный объект до изменения
            aktuala = AkademioPagxo.objects.get(uuid=instance.uuid)

            # сохраняем версию для всех языковых кодов поля teksto
            for lingvo_kodo in instance.teksto['lingvo'].keys():
                if lingvo_kodo not in aktuala.teksto['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioAkademioPagxo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioAkademioPagxo.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0],
                                'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0]
                            }
                        )
                elif get_enhavo(instance.teksto, lingvo_kodo)[0] != get_enhavo(aktuala.teksto, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioAkademioPagxo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioAkademioPagxo.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0],
                                'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0]
                            }
                        )

        except AkademioPagxo.DoesNotExist:
            pass


@receiver(pre_save, sender=EnciklopedioPagxo)
def savi_version_enciklopedio_pagxo(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.teksto['lingvo'].keys()):
        for lingvo_kodo in instance.teksto['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioEnciklopedioPagxo.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioEnciklopedioPagxo.objects.create(
                    autoro=instance.autoro,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0],
                        'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioEnciklopedioPagxo.objects.filter(posedanto=instance).delete()
    elif 'teksto' in (kwargs.get('update_fields') or dict()) or 'nomo' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = EnciklopedioPagxo.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.teksto['lingvo'].keys():
                if lingvo_kodo not in aktuala.teksto['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioEnciklopedioPagxo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioEnciklopedioPagxo.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0],
                                'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0]
                            }
                        )
                elif get_enhavo(instance.teksto, lingvo_kodo)[0] != get_enhavo(aktuala.teksto, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioEnciklopedioPagxo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioEnciklopedioPagxo.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0],
                                'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0]
                            }
                        )

        except EnciklopedioPagxo.DoesNotExist:
            pass

@receiver(pre_save, sender=KodoPagxo)
def savi_version_kodo_pagxo(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.teksto['lingvo'].keys()):
        for lingvo_kodo in instance.teksto['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioKodoPagxo.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioKodoPagxo.objects.create(
                    autoro=instance.autoro,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0],
                        'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioKodoPagxo.objects.filter(posedanto=instance).delete()
    elif 'teksto' in (kwargs.get('update_fields') or dict()) or 'nomo' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = KodoPagxo.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.teksto['lingvo'].keys():
                if lingvo_kodo not in aktuala.teksto['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioKodoPagxo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioKodoPagxo.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0],
                                'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0]
                            }
                        )
                elif get_enhavo(instance.teksto, lingvo_kodo)[0] != get_enhavo(aktuala.teksto, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioKodoPagxo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioKodoPagxo.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0],
                                'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0]
                            }
                        )

        except KodoPagxo.DoesNotExist:
            pass

@receiver(pre_save, sender=KonferencojTemoKomento)
def savi_version_konferencoj_temo_komento(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.teksto['lingvo'].keys()):
        for lingvo_kodo in instance.teksto['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioKonferencojTemoKomento.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioKonferencojTemoKomento.objects.create(
                    autoro=instance.autoro,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioKonferencojTemoKomento.objects.filter(posedanto=instance).delete()
    elif 'teksto' in (kwargs.get('update_fields') or dict()) or 'nomo' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = KonferencojTemoKomento.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.teksto['lingvo'].keys():
                if lingvo_kodo not in aktuala.teksto['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioKonferencojTemoKomento.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioKonferencojTemoKomento.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0]
                            }
                        )
                elif get_enhavo(instance.teksto, lingvo_kodo)[0] != get_enhavo(aktuala.teksto, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioKonferencojTemoKomento.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioKonferencojTemoKomento.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0]
                            }
                        )

        except KonferencojTemoKomento.DoesNotExist:
            pass

@receiver(pre_save, sender=KonferencojTemo)
def savi_version_konferencoj_temo(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.nomo['lingvo'].keys()):
        for lingvo_kodo in instance.nomo['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioKonferencojTemo.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioKonferencojTemo.objects.create(
                    autoro=instance.autoro,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0],
                        'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                        # 'priskribo': get_priskribo(instance.teksto, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioKonferencojTemo.objects.filter(posedanto=instance).delete()
    elif 'priskribo' in (kwargs.get('update_fields') or dict()) or 'nomo' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = KonferencojTemo.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.nomo['lingvo'].keys():
                if lingvo_kodo not in aktuala.nomo['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioKonferencojTemo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioKonferencojTemo.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0],
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                                # 'priskribo': get_priskribo(instance.teksto, lingvo_kodo, empty_values=True)[0]

                            }
                        )
                elif get_enhavo(instance.nomo, lingvo_kodo)[0] != get_enhavo(aktuala.nomo, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioKonferencojTemo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioKonferencojTemo.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0],
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                                # 'priskribo': get_priskribo(instance.teksto, lingvo_kodo, empty_values=True)[0]

                            }
                        )

        except KonferencojTemo.DoesNotExist:
            pass

@receiver(pre_save, sender=KonferencojKategorio)
def savi_version_konferencoj_kategorio(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.nomo['lingvo'].keys()):
        for lingvo_kodo in instance.nomo['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioKonferencojKategorio.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioKonferencojKategorio.objects.create(
                    autoro=instance.autoro,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0],
                        'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioKonferencojKategorio.objects.filter(posedanto=instance).delete()
    elif 'priskribo' in (kwargs.get('update_fields') or dict()) or 'nomo' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = KonferencojKategorio.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.nomo['lingvo'].keys():
                if lingvo_kodo not in aktuala.nomo['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioKonferencojKategorio.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioKonferencojKategorio.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0],
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )
                elif get_enhavo(instance.nomo, lingvo_kodo)[0] != get_enhavo(aktuala.nomo, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioKonferencojKategorio.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioKonferencojKategorio.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0],
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )

        except KonferencojKategorio.DoesNotExist:
            pass

@receiver(pre_save, sender=EsploradojTemoKomento)
def savi_version_esploradoj_temo_komento(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.teksto['lingvo'].keys()):
        for lingvo_kodo in instance.teksto['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioEsploradojTemoKomento.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioEsploradojTemoKomento.objects.create(
                    autoro=instance.autoro,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioEsploradojTemoKomento.objects.filter(posedanto=instance).delete()
    elif 'teksto' in (kwargs.get('update_fields') or dict()) or 'nomo' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = EsploradojTemoKomento.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.teksto['lingvo'].keys():
                if lingvo_kodo not in aktuala.teksto['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioEsploradojTemoKomento.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioEsploradojTemoKomento.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0]
                            }
                        )
                elif get_enhavo(instance.teksto, lingvo_kodo)[0] != get_enhavo(aktuala.teksto, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioEsploradojTemoKomento.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioEsploradojTemoKomento.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0]
                            }
                        )

        except EsploradojTemoKomento.DoesNotExist:
            pass

@receiver(pre_save, sender=EsploradojTemo)
def savi_version_esploradoj_temo(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.nomo['lingvo'].keys()):
        for lingvo_kodo in instance.nomo['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioEsploradojTemo.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioEsploradojTemo.objects.create(
                    autoro=instance.autoro,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0],
                        'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                        # 'priskribo': get_priskribo(instance.teksto, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioEsploradojTemo.objects.filter(posedanto=instance).delete()
    elif 'priskribo' in (kwargs.get('update_fields') or dict()) or 'nomo' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = EsploradojTemo.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.nomo['lingvo'].keys():
                if lingvo_kodo not in aktuala.nomo['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioEsploradojTemo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioEsploradojTemo.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0],
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                                # 'priskribo': get_priskribo(instance.teksto, lingvo_kodo, empty_values=True)[0]

                            }
                        )
                elif get_enhavo(instance.nomo, lingvo_kodo)[0] != get_enhavo(aktuala.nomo, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioEsploradojTemo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioEsploradojTemo.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0],
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                                # 'priskribo': get_priskribo(instance.teksto, lingvo_kodo, empty_values=True)[0]

                            }
                        )

        except EsploradojTemo.DoesNotExist:
            pass

@receiver(pre_save, sender=EsploradojKategorio)
def savi_version_esploradoj_kategorio(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.nomo['lingvo'].keys()):
        for lingvo_kodo in instance.nomo['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioEsploradojKategorio.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioEsploradojKategorio.objects.create(
                    autoro=instance.autoro,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0],
                        'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioEsploradojKategorio.objects.filter(posedanto=instance).delete()
    elif 'priskribo' in (kwargs.get('update_fields') or dict()) or 'nomo' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = EsploradojKategorio.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.nomo['lingvo'].keys():
                if lingvo_kodo not in aktuala.nomo['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioEsploradojKategorio.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioEsploradojKategorio.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0],
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )
                elif get_enhavo(instance.nomo, lingvo_kodo)[0] != get_enhavo(aktuala.nomo, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioEsploradojKategorio.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioEsploradojKategorio.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'nomo': get_enhavo(instance.nomo, lingvo_kodo, empty_values=True)[0],
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )

        except EsploradojKategorio.DoesNotExist:
            pass

@receiver(pre_save, sender=TaskojUzantoProjekto)
def savi_version_taskoj_uzanto_projekto(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.priskribo['lingvo'].keys()):
        for lingvo_kodo in instance.priskribo['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioTaskojUzantoProjekto.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioTaskojUzantoProjekto.objects.create(
                    autoro=instance.posedanto,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioTaskojUzantoProjekto.objects.filter(posedanto=instance).delete()
    elif 'priskribo' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = TaskojUzantoProjekto.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.priskribo['lingvo'].keys():
                if lingvo_kodo not in aktuala.priskribo['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioTaskojUzantoProjekto.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioTaskojUzantoProjekto.objects.create(
                            autoro=instance.posedanto,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )
                elif get_enhavo(instance.priskribo, lingvo_kodo)[0] != get_enhavo(aktuala.priskribo, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioTaskojUzantoProjekto.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioTaskojUzantoProjekto.objects.create(
                            autoro=instance.posedanto,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )

        except TaskojUzantoProjekto.DoesNotExist:
            pass

@receiver(pre_save, sender=TaskojUzantoDirekto)
def savi_version_taskoj_uzanto_direkto(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.priskribo['lingvo'].keys()):
        for lingvo_kodo in instance.priskribo['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioTaskojUzantoDirekto.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioTaskojUzantoDirekto.objects.create(
                    autoro=instance.posedanto,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioTaskojUzantoDirekto.objects.filter(posedanto=instance).delete()
    elif 'priskribo' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = TaskojUzantoDirekto.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.priskribo['lingvo'].keys():
                if lingvo_kodo not in aktuala.priskribo['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioTaskojUzantoDirekto.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioTaskojUzantoDirekto.objects.create(
                            autoro=instance.posedanto,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )
                elif get_enhavo(instance.priskribo, lingvo_kodo)[0] != get_enhavo(aktuala.priskribo, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioTaskojUzantoDirekto.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioTaskojUzantoDirekto.objects.create(
                            autoro=instance.posedanto,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )

        except TaskojUzantoDirekto.DoesNotExist:
            pass

@receiver(pre_save, sender=TaskojUzantoEtapo)
def savi_version_taskoj_uzanto_etapo(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.priskribo['lingvo'].keys()):
        for lingvo_kodo in instance.priskribo['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioTaskojUzantoEtapo.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioTaskojUzantoEtapo.objects.create(
                    autoro=instance.posedanto,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioTaskojUzantoEtapo.objects.filter(posedanto=instance).delete()
    elif 'priskribo' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = TaskojUzantoEtapo.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.priskribo['lingvo'].keys():
                if lingvo_kodo not in aktuala.priskribo['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioTaskojUzantoEtapo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioTaskojUzantoEtapo.objects.create(
                            autoro=instance.posedanto,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )
                elif get_enhavo(instance.priskribo, lingvo_kodo)[0] != get_enhavo(aktuala.priskribo, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioTaskojUzantoEtapo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioTaskojUzantoEtapo.objects.create(
                            autoro=instance.posedanto,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )

        except TaskojUzantoEtapo.DoesNotExist:
            pass

@receiver(pre_save, sender=TaskojUzantoTasko)
def savi_version_taskoj_uzanto_tasko(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.priskribo['lingvo'].keys()):
        for lingvo_kodo in instance.priskribo['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioTaskojUzantoTasko.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioTaskojUzantoTasko.objects.create(
                    autoro=instance.posedanto,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioTaskojUzantoTasko.objects.filter(posedanto=instance).delete()
    elif 'priskribo' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = TaskojUzantoTasko.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.priskribo['lingvo'].keys():
                if lingvo_kodo not in aktuala.priskribo['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioTaskojUzantoTasko.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioTaskojUzantoTasko.objects.create(
                            autoro=instance.posedanto,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )
                elif get_enhavo(instance.priskribo, lingvo_kodo)[0] != get_enhavo(aktuala.priskribo, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioTaskojUzantoTasko.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioTaskojUzantoTasko.objects.create(
                            autoro=instance.posedanto,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )

        except TaskojUzantoTasko.DoesNotExist:
            pass

@receiver(pre_save, sender=TaskojKomunumoProjekto)
def savi_version_taskoj_komunumo_projekto(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.priskribo['lingvo'].keys()):
        for lingvo_kodo in instance.priskribo['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioTaskojKomunumoProjekto.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioTaskojKomunumoProjekto.objects.create(
                    autoro=instance.autoro,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioTaskojKomunumoProjekto.objects.filter(posedanto=instance).delete()
    elif 'priskribo' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = TaskojKomunumoProjekto.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.priskribo['lingvo'].keys():
                if lingvo_kodo not in aktuala.priskribo['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioTaskojKomunumoProjekto.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioTaskojKomunumoProjekto.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )
                elif get_enhavo(instance.priskribo, lingvo_kodo)[0] != get_enhavo(aktuala.priskribo, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioTaskojKomunumoProjekto.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioTaskojKomunumoProjekto.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )

        except TaskojKomunumoProjekto.DoesNotExist:
            pass

@receiver(pre_save, sender=TaskojKomunumoDirekto)
def savi_version_taskoj_komunumo_direkto(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.priskribo['lingvo'].keys()):
        for lingvo_kodo in instance.priskribo['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioTaskojKomunumoDirekto.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioTaskojKomunumoDirekto.objects.create(
                    autoro=instance.autoro,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioTaskojKomunumoDirekto.objects.filter(posedanto=instance).delete()
    elif 'priskribo' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = TaskojKomunumoDirekto.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.priskribo['lingvo'].keys():
                if lingvo_kodo not in aktuala.priskribo['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioTaskojKomunumoDirekto.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioTaskojKomunumoDirekto.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )
                elif get_enhavo(instance.priskribo, lingvo_kodo)[0] != get_enhavo(aktuala.priskribo, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioTaskojKomunumoDirekto.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioTaskojKomunumoDirekto.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )

        except TaskojKomunumoDirekto.DoesNotExist:
            pass

@receiver(pre_save, sender=TaskojKomunumoEtapo)
def savi_version_taskoj_komunumo_etapo(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.priskribo['lingvo'].keys()):
        for lingvo_kodo in instance.priskribo['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioTaskojKomunumoEtapo.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioTaskojKomunumoEtapo.objects.create(
                    autoro=instance.autoro,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioTaskojKomunumoEtapo.objects.filter(posedanto=instance).delete()
    elif 'priskribo' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = TaskojKomunumoEtapo.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.priskribo['lingvo'].keys():
                if lingvo_kodo not in aktuala.priskribo['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioTaskojKomunumoEtapo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioTaskojKomunumoEtapo.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )
                elif get_enhavo(instance.priskribo, lingvo_kodo)[0] != get_enhavo(aktuala.priskribo, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioTaskojKomunumoEtapo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioTaskojKomunumoEtapo.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )

        except TaskojKomunumoEtapo.DoesNotExist:
            pass

@receiver(pre_save, sender=TaskojKomunumoTasko)
def savi_version_taskoj_komunumo_tasko(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.priskribo['lingvo'].keys()):
        for lingvo_kodo in instance.priskribo['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioTaskojKomunumoTasko.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioTaskojKomunumoTasko.objects.create(
                    autoro=instance.autoro,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioTaskojKomunumoTasko.objects.filter(posedanto=instance).delete()
    elif 'priskribo' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = TaskojKomunumoTasko.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.priskribo['lingvo'].keys():
                if lingvo_kodo not in aktuala.priskribo['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioTaskojKomunumoTasko.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioTaskojKomunumoTasko.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )
                elif get_enhavo(instance.priskribo, lingvo_kodo)[0] != get_enhavo(aktuala.priskribo, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioTaskojKomunumoTasko.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioTaskojKomunumoTasko.objects.create(
                            autoro=instance.autoro,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'priskribo': get_enhavo(instance.priskribo, lingvo_kodo, empty_values=True)[0]
                            }
                        )

        except TaskojKomunumoTasko.DoesNotExist:
            pass

@receiver(pre_save, sender=MesagxiloMesagxo)
def savi_version_taskoj_komunumo_tasko(instance, **kwargs):
    if kwargs.get('created', False) and len(instance.teksto['lingvo'].keys()):
        for lingvo_kodo in instance.teksto['lingvo'].keys():
            lingvo = get_infolingvo_by_code(lingvo_kodo)

            if lingvo:
                with transaction.atomic():
                    VersioMesagxiloMesagxo.objects.select_for_update(of=('self',)).filter(
                        posedanto=instance,
                        lingvo=lingvo,
                        aktiva=True,
                    ).update(aktiva=False)

                VersioMesagxiloMesagxo.objects.create(
                    autoro=instance.posedanto,
                    posedanto=instance,
                    lingvo=lingvo,
                    aktiva=True,
                    valoro={
                        'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0]
                    }
                )
    elif instance.forigo:
        VersioMesagxiloMesagxo.objects.filter(posedanto=instance).delete()
    elif 'teksto' in (kwargs.get('update_fields') or dict()):
        try:
            aktuala = MesagxiloMesagxo.objects.get(uuid=instance.uuid)

            for lingvo_kodo in instance.teksto['lingvo'].keys():
                if lingvo_kodo not in aktuala.teksto['lingvo']:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioMesagxiloMesagxo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioMesagxiloMesagxo.objects.create(
                            autoro=instance.posedanto,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0]
                            }
                        )
                elif get_enhavo(instance.teksto, lingvo_kodo)[0] != get_enhavo(aktuala.teksto, lingvo_kodo)[0]:
                    lingvo = get_infolingvo_by_code(lingvo_kodo)

                    if lingvo:
                        with transaction.atomic():
                            VersioMesagxiloMesagxo.objects.select_for_update(of=('self',)).filter(
                                posedanto=instance,
                                lingvo=lingvo,
                                aktiva=True,
                            ).update(aktiva=False)

                        VersioMesagxiloMesagxo.objects.create(
                            autoro=instance.posedanto,
                            posedanto=instance,
                            lingvo=lingvo,
                            aktiva=True,
                            valoro={
                                'teksto': get_enhavo(instance.teksto, lingvo_kodo, empty_values=True)[0]
                            }
                        )

        except MesagxiloMesagxo.DoesNotExist:
            pass

