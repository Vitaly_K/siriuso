"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django import forms
from siriuso.utils import get_enhavo
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
import json

from .models import *


class TekstoMixin:
    @staticmethod
    def teksto(obj, field):
        kampo = getattr(obj, field)

        if kampo is not None:
            result = get_enhavo(kampo)

            if result:
                return result[0]
        return ''

    def nomo_teksto(self, obj):
        return self.teksto(obj=obj, field='nomo')

    def priskribo_teksto(self, obj):
        return self.teksto(obj=obj, field='priskribo')

    def teksto_teksto(self, obj):
        return self.teksto(obj=obj, field='teksto')


# Форма для многоязычных чатов
class MesagxiloBabilejoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)

    class Meta:
        model = MesagxiloBabilejo
        fields = [field.name for field in MesagxiloBabilejo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Чаты
@admin.register(MesagxiloBabilejo)
class MesagxiloBabilejoAdmin(admin.ModelAdmin, TekstoMixin):
    form = MesagxiloBabilejoFormo
    list_display = ('nomo_teksto', 'uuid')
    exclude = ('uuid',)

    class Meta:
        model = MesagxiloBabilejo


# Форма участников чатов
class MesagxiloPartoprenantoFormo(forms.ModelForm):

    sciigoj = forms.ModelMultipleChoiceField(
        label=_('Тип способа уведомления'),
        queryset=InformilojSciigoTipo.objects.filter(forigo=False)
    )
    filter_horizontal = ('sciigoj', )

        # widget=forms.SelectMultiple,
        # widget=forms.CheckboxSelectMultiple,

    class Meta:
        model = MesagxiloPartoprenanto
        fields = [field.name for field in MesagxiloPartoprenanto._meta.fields if field.name not in ('krea_dato', 'uuid')]\
                 + ['sciigoj',]


# Участники чатов
@admin.register(MesagxiloPartoprenanto)
class MesagxiloPartoprenantoAdmin(admin.ModelAdmin):
    form = MesagxiloPartoprenantoFormo
    list_display = ('uuid',)
    filter_horizontal = ('sciigoj', )

    class Meta:
        model = MesagxiloPartoprenanto


# форма сообщения чатов
class MesagxiloMesagxoFormo(forms.ModelForm):
    teksto = forms.CharField(widget=LingvoInputWidget(), label=_('Teksto'), required=True)

    class Meta:
        model = MesagxiloMesagxo
        fields = [field.name for field in MesagxiloMesagxo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_teksto(self):
        out = self.cleaned_data['teksto']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Сообщения чатов
@admin.register(MesagxiloMesagxo)
class MesagxiloMesagxoAdmin(admin.ModelAdmin, TekstoMixin):
    form = MesagxiloMesagxoFormo
    # list_display = ('uuid')
    list_display = ('uuid', 'teksto_teksto')
    exclude = ('uuid',)

    class Meta:
        model = MesagxiloMesagxo


# Форма вложения к сообщениям чатов
class MesagxiloInvestojFormo(forms.ModelForm):

    class Meta:
        model = MesagxiloInvestoj
        fields = [field.name for field in MesagxiloInvestoj._meta.fields if field.name not in ('krea_dato', 'uuid')]


# вложения к сообщениям чатов
@admin.register(MesagxiloInvestoj)
class MesagxiloInvestojAdmin(admin.ModelAdmin, TekstoMixin):
    form = MesagxiloInvestojFormo
    # list_display = ('id')
    # exclude = ('id',)

    class Meta:
        model = MesagxiloInvestoj
