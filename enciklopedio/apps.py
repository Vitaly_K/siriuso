from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class EnciklopedioConfig(AppConfig):
    name = 'enciklopedio'
    verbose_name = _('Enciklopedio')

    # def ready(self):
    #     import Enciklopedio.signals
