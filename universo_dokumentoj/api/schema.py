"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene  # сам Graphene
from django.utils.translation import ugettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import *  # модели приложения


# Модель связей категорий документов между собой
class UniversoDokumentoKategorioLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = UniversoDokumentoKategorioLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__id': ['exact'],
            'ligilo__id': ['exact'],
            'tipo__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель категорий документов Универсо
class UniversoDokumentoKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    # Связь категорий документов между собой
    ligilo =  SiriusoFilterConnectionField(UniversoDokumentoKategorioLigiloNode,
        description=_('Выводит связи категорий документов между собой'))

    class Meta:
        model = UniversoDokumentoKategorio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact'],
            'universo_dokumentoj_universodokumentokategorioligilo_ligilo': ['isnull']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_ligilo(self, info, **kwargs):
        return UniversoDokumentoKategorioLigilo.objects.filter(posedanto=self, forigo=False, arkivo=False, publikigo=True)


# Модель типов связей категорий документов между собой
class UniversoDokumentoKategorioLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoDokumentoKategorioLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов документов Универсо
class UniversoDokumentoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoDokumentoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель видов документов Универсо
class UniversoDokumentoSpecoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoDokumentoSpeco
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов мест хранения документов Универсо
class UniversoDokumentoStokejoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoDokumentoStokejoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель логических мест хранения документов Универсо
class UniversoDokumentoStokejoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoDokumentoStokejo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'tipo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов цен документов Универсо
class UniversoDokumentoPosedantoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoDokumentoPosedantoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов цен документов Универсо
class UniversoDokumentoPosedantoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoDokumentoPosedantoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель цен документов Универсо
class UniversoDokumentoPosedantoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = UniversoDokumentoPosedanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель типов связей документов между собой
class UniversoDokumentoLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoDokumentoLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связей документов между собой
class UniversoDokumentoLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoDokumentoLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'posedanto_stokejo__uuid': ['exact'],
            'ligilo__uuid': ['exact'],
            'tipo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель документов Универсо
class UniversoDokumentoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    # Связь документов между собой
    ligilo =  SiriusoFilterConnectionField(UniversoDokumentoLigiloNode,
        description=_('Выводит связи документов между собой, владельцами которых является документ'))

    class Meta:
        model = UniversoDokumento
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'speco__id': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_ligilo(self, info, **kwargs):
        return UniversoDokumentoLigilo.objects.filter(posedanto=self, forigo=False, arkivo=False, 
            publikigo=True)


class UniversoDokumentojQuery(graphene.ObjectType):
    universo_dokumento_kategorio = SiriusoFilterConnectionField(
        UniversoDokumentoKategorioNode,
        description=_('Выводит все доступные модели категорий документов Универсо')
    )
    universo_dokumento_kategorio_ligiloj_tipoj = SiriusoFilterConnectionField(
        UniversoDokumentoKategorioLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей категорий документов между собой')
    )
    universo_dokumento_kategorio_ligiloj = SiriusoFilterConnectionField(
        UniversoDokumentoKategorioLigiloNode,
        description=_('Выводит все доступные модели связей категорий документов между собой')
    )
    universo_dokumento_tipo = SiriusoFilterConnectionField(
        UniversoDokumentoTipoNode,
        description=_('Выводит все доступные модели типов документов Универсо')
    )
    universo_dokumento_speco = SiriusoFilterConnectionField(
        UniversoDokumentoSpecoNode,
        description=_('Выводит все доступные модели видов документов Универсо')
    )
    universo_dokumento_stokejo_tipo = SiriusoFilterConnectionField(
        UniversoDokumentoStokejoTipoNode,
        description=_('Выводит все доступные модели типов мест хранения документов Универсо')
    )
    universo_dokumento_stokejo = SiriusoFilterConnectionField(
        UniversoDokumentoStokejoNode,
        description=_('Выводит все доступные модели логических мест хранения документов Универсо')
    )
    universo_objekto_posedanto_tipo = SiriusoFilterConnectionField(
        UniversoDokumentoPosedantoTipoNode,
        description=_('Выводит все доступные модели типов владельцев объектов в Универсо')
    )
    universo_objekto_posedanto_statuso = SiriusoFilterConnectionField(
        UniversoDokumentoPosedantoStatusoNode,
        description=_('Выводит все доступные модели статусов владельца в рамках владения объектом Универсо')
    )
    universo_objekto_posedanto = SiriusoFilterConnectionField(
        UniversoDokumentoPosedantoNode,
        description=_('Выводит все доступные модели владельцев объектов в Универсо')
    )
    universo_dokumento = SiriusoFilterConnectionField(
        UniversoDokumentoNode,
        description=_('Выводит все доступные документы Универсо')
    )
    universo_dokumento_ligilo_tipo = SiriusoFilterConnectionField(
        UniversoDokumentoLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей документов между собой')
    )
    universo_dokumento_ligilo = SiriusoFilterConnectionField(
        UniversoDokumentoLigiloNode,
        description=_('Выводит все доступные модели связей документов между собой')
    )
