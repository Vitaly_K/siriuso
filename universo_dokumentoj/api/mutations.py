"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from universo_uzantoj.models import UniversoUzanto
from .schema import *
from ..models import *


# Модель категорий документов Универсо
class RedaktuUniversoDokumentoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_dokumentoj_kategorioj = graphene.Field(UniversoDokumentoKategorioNode,
        description=_('Созданная/изменённая категория документов в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_dokumentoj_kategorioj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_dokumentoj.povas_krei_universo_dokumentoj_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_dokumentoj_kategorioj = UniversoDokumentoKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_dokumentoj_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_dokumentoj_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_dokumentoj_kategorioj.realeco.set(realeco)
                                else:
                                    universo_dokumentoj_kategorioj.realeco.clear()

                            universo_dokumentoj_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_dokumentoj_kategorioj = UniversoDokumentoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_dokumentoj.povas_forigi_universo_dokumentoj_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_dokumentoj.povas_shanghi_universo_dokumentoj_kategorioj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_dokumentoj_kategorioj.forigo = kwargs.get('forigo', universo_dokumentoj_kategorioj.forigo)
                                universo_dokumentoj_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_dokumentoj_kategorioj.arkivo = kwargs.get('arkivo', universo_dokumentoj_kategorioj.arkivo)
                                universo_dokumentoj_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_dokumentoj_kategorioj.publikigo = kwargs.get('publikigo', universo_dokumentoj_kategorioj.publikigo)
                                universo_dokumentoj_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_dokumentoj_kategorioj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_dokumentoj_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_dokumentoj_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_dokumentoj_kategorioj.realeco.set(realeco)
                                    else:
                                        universo_dokumentoj_kategorioj.realeco.clear()

                                universo_dokumentoj_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoDokumentoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoDokumentoKategorio(status=status, message=message, universo_dokumentoj_kategorioj=universo_dokumentoj_kategorioj)


# Модель типов связей категорий документов между собой
class RedaktuUniversoDokumentoKategorioLigiloTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj = graphene.Field(UniversoDokumentoKategorioLigiloTipoNode,
        description=_('Созданная/изменённая модель типов связей категорий документов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_dokumentoj.povas_krei_universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj = UniversoDokumentoKategorioLigiloTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj = UniversoDokumentoKategorioLigiloTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_dokumentoj.povas_forigi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_dokumentoj.povas_shanghi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.forigo = kwargs.get('forigo', universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.forigo)
                                universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.arkivo = kwargs.get('arkivo', universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.arkivo)
                                universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.publikigo = kwargs.get('publikigo', universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.publikigo)
                                universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoDokumentoKategorioLigiloTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoDokumentoKategorioLigiloTipo(status=status, message=message, universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj=universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj)


# Модель связей категорий документов между собой
class RedaktuUniversoDokumentoKategorioLigilo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_dokumentoj_kategorioj_dokumentoj_ligiloj = graphene.Field(UniversoDokumentoKategorioLigiloNode,
        description=_('Созданная/изменённая модель связей категорий документов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        posedanto_id = graphene.String(description=_('ID категории документов владельца связи'))
        ligilo_id = graphene.String(description=_('ID связываемой категории документов'))
        tipo_id = graphene.String(description=_('ID типа связи категорий документов'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_dokumentoj_kategorioj_dokumentoj_ligiloj = None
        posedanto = None
        ligilo = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_dokumentoj.povas_krei_universo_dokumentoj_kategorioj_dokumentoj_ligiloj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_id' in kwargs:
                                try:
                                    posedanto = UniversoDokumentoKategorio.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoDokumentoKategorio.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверная категория документов владельца связи'),'posedanto_id')
                            else:
                                message = '{}: {}'.format(_('Не указана категория документов владелец связи'),'posedanto_id')

                        if not message:
                            if 'ligilo_id' in kwargs:
                                try:
                                    ligilo = UniversoDokumentoKategorio.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoDokumentoKategorio.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверная связываемая категория документов'),'ligilo_id')
                            else:
                                message = '{}: {}'.format(_('Не указана связываемая категория документов'),'ligilo_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoDokumentoKategorioLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoDokumentoKategorioLigiloTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип связи категорий документов'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип связи категорий документов'),'tipo_id')

                        if not message:
                            universo_dokumentoj_kategorioj_dokumentoj_ligiloj = UniversoDokumentoKategorioLigilo.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto=posedanto,
                                ligilo=ligilo,
                                tipo=tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            universo_dokumentoj_kategorioj_dokumentoj_ligiloj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto', False) or kwargs.get('ligilo', False)
                            or kwargs.get('tipo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_dokumentoj_kategorioj_dokumentoj_ligiloj = UniversoDokumentoKategorioLigilo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_dokumentoj.povas_forigi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_dokumentoj.povas_shanghi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_id' in kwargs:
                                    try:
                                        posedanto = UniversoDokumentoKategorio.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoDokumentoKategorio.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверная категория документов владельца связи'),'posedanto_id')

                            if not message:
                                if 'ligilo_id' in kwargs:
                                    try:
                                        ligilo = UniversoDokumentoKategorio.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoDokumentoKategorio.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверная связываемая категория документов'),'ligilo_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoDokumentoKategorioLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoDokumentoKategorioLigiloTipo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный тип связи категорий документов'),'tipo_id')

                            if not message:

                                universo_dokumentoj_kategorioj_dokumentoj_ligiloj.forigo = kwargs.get('forigo', universo_dokumentoj_kategorioj_dokumentoj_ligiloj.forigo)
                                universo_dokumentoj_kategorioj_dokumentoj_ligiloj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_dokumentoj_kategorioj_dokumentoj_ligiloj.arkivo = kwargs.get('arkivo', universo_dokumentoj_kategorioj_dokumentoj_ligiloj.arkivo)
                                universo_dokumentoj_kategorioj_dokumentoj_ligiloj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_dokumentoj_kategorioj_dokumentoj_ligiloj.publikigo = kwargs.get('publikigo', universo_dokumentoj_kategorioj_dokumentoj_ligiloj.publikigo)
                                universo_dokumentoj_kategorioj_dokumentoj_ligiloj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_dokumentoj_kategorioj_dokumentoj_ligiloj.posedanto = posedanto if kwargs.get('posedanto_id', False) else universo_dokumentoj_kategorioj_dokumentoj_ligiloj.posedanto
                                universo_dokumentoj_kategorioj_dokumentoj_ligiloj.ligilo = ligilo if kwargs.get('ligilo_id', False) else universo_dokumentoj_kategorioj_dokumentoj_ligiloj.ligilo
                                universo_dokumentoj_kategorioj_dokumentoj_ligiloj.tipo = tipo if kwargs.get('tipo_id', False) else universo_dokumentoj_kategorioj_dokumentoj_ligiloj.tipo

                                universo_dokumentoj_kategorioj_dokumentoj_ligiloj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoDokumentoKategorioLigilo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoDokumentoKategorioLigilo(status=status, message=message, universo_dokumentoj_kategorioj_dokumentoj_ligiloj=universo_dokumentoj_kategorioj_dokumentoj_ligiloj)


# Модель типов документов Универсо
class RedaktuUniversoDokumentoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_dokumentoj_tipoj = graphene.Field(UniversoDokumentoTipoNode, 
        description=_('Созданный/изменённый тип документов в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_dokumentoj_tipoj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_dokumentoj.povas_krei_universo_dokumentoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_dokumentoj_tipoj = UniversoDokumentoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_dokumentoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_dokumentoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_dokumentoj_tipoj.realeco.set(realeco)
                                else:
                                    universo_dokumentoj_tipoj.realeco.clear()

                            universo_dokumentoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_dokumentoj_tipoj = UniversoDokumentoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_dokumentoj.povas_forigi_universo_dokumentoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_dokumentoj.povas_shanghi_universo_dokumentoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_dokumentoj_tipoj.forigo = kwargs.get('forigo', universo_dokumentoj_tipoj.forigo)
                                universo_dokumentoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_dokumentoj_tipoj.arkivo = kwargs.get('arkivo', universo_dokumentoj_tipoj.arkivo)
                                universo_dokumentoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_dokumentoj_tipoj.publikigo = kwargs.get('publikigo', universo_dokumentoj_tipoj.publikigo)
                                universo_dokumentoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_dokumentoj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_dokumentoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_dokumentoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_dokumentoj_tipoj.realeco.set(realeco)
                                    else:
                                        universo_dokumentoj_tipoj.realeco.clear()

                                universo_dokumentoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoDokumentoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoDokumentoTipo(status=status, message=message, universo_dokumentoj_tipoj=universo_dokumentoj_tipoj)


# Модель видов документов Универсо
class RedaktuUniversoDokumentoSpeco(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_dokumentoj_specoj = graphene.Field(UniversoDokumentoSpecoNode,
        description=_('Созданный/изменённый вид документов в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_dokumentoj_specoj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_dokumentoj.povas_krei_universo_dokumentoj_specoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_dokumentoj_specoj = UniversoDokumentoSpeco.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_dokumentoj_specoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_dokumentoj_specoj.priskribo,
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_dokumentoj_specoj.realeco.set(realeco)
                                else:
                                    universo_dokumentoj_specoj.realeco.clear()

                            universo_dokumentoj_specoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_dokumentoj_specoj = UniversoDokumentoSpeco.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_dokumentoj.povas_forigi_universo_dokumentoj_specoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_dokumentoj.povas_shanghi_universo_dokumentoj_specoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_dokumentoj_specoj.forigo = kwargs.get('forigo', universo_dokumentoj_specoj.forigo)
                                universo_dokumentoj_specoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_dokumentoj_specoj.arkivo = kwargs.get('arkivo', universo_dokumentoj_specoj.arkivo)
                                universo_dokumentoj_specoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_dokumentoj_specoj.publikigo = kwargs.get('publikigo', universo_dokumentoj_specoj.publikigo)
                                universo_dokumentoj_specoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_dokumentoj_specoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_dokumentoj_specoj.nomo, kwargs.get('nomo'),
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_dokumentoj_specoj.priskribo, kwargs.get('priskribo'),
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_dokumentoj_specoj.realeco.set(realeco)
                                    else:
                                        universo_dokumentoj_specoj.realeco.clear()

                                universo_dokumentoj_specoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoDokumentoSpeco.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoDokumentoSpeco(status=status, message=message, universo_dokumentoj_specoj=universo_dokumentoj_specoj)


# Модель типов мест хранения документов Универсо
class RedaktuUniversoDokumentoStokejoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_dokumentoj_stokejoj_tipoj = graphene.Field(UniversoDokumentoStokejoTipoNode, 
        description=_('Созданный/изменённый тип мест хранения документов Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_dokumentoj_stokejoj_tipoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_dokumentoj.povas_krei_universo_dokumentoj_stokejoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_dokumentoj_stokejoj_tipoj = UniversoDokumentoStokejoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_dokumentoj_stokejoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_dokumentoj_stokejoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_dokumentoj_stokejoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_dokumentoj_stokejoj_tipoj = UniversoDokumentoStokejoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_dokumentoj.povas_forigi_universo_dokumentoj_stokejoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_dokumentoj.povas_shanghi_universo_dokumentoj_stokejoj_tipoj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_dokumentoj_stokejoj_tipoj.forigo = kwargs.get('forigo', universo_dokumentoj_stokejoj_tipoj.forigo)
                                universo_dokumentoj_stokejoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_dokumentoj_stokejoj_tipoj.arkivo = kwargs.get('arkivo', universo_dokumentoj_stokejoj_tipoj.arkivo)
                                universo_dokumentoj_stokejoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_dokumentoj_stokejoj_tipoj.publikigo = kwargs.get('publikigo', universo_dokumentoj_stokejoj_tipoj.publikigo)
                                universo_dokumentoj_stokejoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_dokumentoj_stokejoj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_dokumentoj_stokejoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_dokumentoj_stokejoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_dokumentoj_stokejoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoDokumentoStokejoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoDokumentoStokejoTipo(status=status, message=message, universo_dokumentoj_stokejoj_tipoj=universo_dokumentoj_stokejoj_tipoj)


# Модель документов Универсо
class RedaktuUniversoDokumento(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_dokumentoj = graphene.Field(UniversoDokumentoNode, 
        description=_('Созданный/изменённый документ Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий документов Универсо'))
        tipo_id = graphene.Int(description=_('Код типа документов Универсо'))
        speco_id = graphene.Int(description=_('Код вида документов Универсо'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_dokumentoj = None
        autoro = None
        realeco = UniversoRealeco.objects.none()
        kategorio = UniversoDokumentoKategorio.objects.none()
        tipo = None
        speco = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_dokumentoj.povas_krei_universo_dokumentoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одно значение'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            if 'kategorio' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                            elif not len(kwargs.get('kategorio')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одно значение'),'kategorio')
                            else:
                                kategorio_id = set(kwargs.get('kategorio'))

                                if len(kategorio_id):
                                    kategorio = UniversoDokumentoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие категория документов Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoDokumentoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoDokumentoTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип документов Универсо'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип документов Универсо'),'tipo_id')

                        if not message:
                            if 'speco_id' in kwargs:
                                try:
                                    speco = UniversoDokumentoSpeco.objects.get(id=kwargs.get('speco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoDokumentoSpeco.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный вид документов Универсо'),'speco_id')
                            else:
                                message = '{}: {}'.format(_('Не указан вид документов Универсо'),'speco_id')

                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_dokumentoj = UniversoDokumento.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                tipo = tipo,
                                speco = speco,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_dokumentoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_dokumentoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    universo_dokumentoj.realeco.set(realeco)
                                else:
                                    universo_dokumentoj.realeco.clear()

                            if 'kategorio' in kwargs:
                                if kategorio:
                                    universo_dokumentoj.kategorio.set(kategorio)
                                else:
                                    universo_dokumentoj.kategorio.clear()

                            universo_dokumentoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False) or kwargs.get('kategorio', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('speco_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_dokumentoj = UniversoDokumento.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_dokumentoj.povas_forigi_universo_dokumentoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_dokumentoj.povas_shanghi_universo_dokumentoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = UniversoRealeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))

                                    if len(kategorio_id):
                                        kategorio = UniversoDokumentoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категория документов Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoDokumentoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoDokumentoTipo.DoesNotExist:
                                        message = _('Неверный тип документов Универсо')
                            if not message:
                                if 'speco_id' in kwargs:
                                    try:
                                        speco = UniversoDokumentoSpeco.objects.get(id=kwargs.get('speco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoDokumentoSpeco.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный вид документов Универсо'),'speco_id')

                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_dokumentoj.forigo = kwargs.get('forigo', universo_dokumentoj.forigo)
                                universo_dokumentoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_dokumentoj.arkivo = kwargs.get('arkivo', universo_dokumentoj.arkivo)
                                universo_dokumentoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_dokumentoj.publikigo = kwargs.get('publikigo', universo_dokumentoj.publikigo)
                                universo_dokumentoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_dokumentoj.autoro = autoro
                                universo_dokumentoj.tipo = tipo if kwargs.get('tipo_id', False) else universo_dokumentoj.tipo
                                universo_dokumentoj.speco = speco if kwargs.get('speco_id', False) else universo_dokumentoj.speco

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_dokumentoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_dokumentoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        universo_dokumentoj.realeco.set(realeco)
                                    else:
                                        universo_dokumentoj.realeco.clear()
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        universo_dokumentoj.kategorio.set(kategorio)
                                    else:
                                        universo_dokumentoj.kategorio.clear()

                                universo_dokumentoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoDokumento.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoDokumento(status=status, message=message, universo_dokumentoj=universo_dokumentoj)


# Модель логических мест хранения документов Универсо
class RedaktuUniversoDokumentoStokejo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_dokumentoj_stokejoj = graphene.Field(UniversoDokumentoStokejoNode, 
        description=_('Созданные/изменённые логические места хранения документов Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        posedanto_uuid = graphene.String(description=_('Ресурс владелец места хранения'))
        tipo_id = graphene.Int(description=_('Тип места хранения документов на основе которого создано это место хранения в документе'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        posedanto = None
        tipo = None
        universo_dokumentoj_stokejoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_dokumentoj.povas_krei_universo_dokumentoj_stokejoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')
                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoDokumentoStokejoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoDokumentoStokejoTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип места хранения документов Универсо'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип места хранения документов Универсо'),'tipo_id')

                        if not message:
                            if 'posedanto_uuid' in kwargs:
                                try:
                                    posedanto = UniversoDokumento.objects.get(id=kwargs.get('posedanto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoDokumento.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный документ владелец места хранения Универсо'),'posedanto_uuid')
                            else:
                                message = '{}: {}'.format(_('Не указан документ владелец места хранения Универсо'),'posedanto_uuid')

                        if not message:
                            universo_dokumentoj_stokejoj = UniversoDokumentoStokejo.objects.create(
                                forigo=False,
                                arkivo=False,
                                tipo=tipo,
                                posedanto=posedanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_dokumentoj_stokejoj.nomo, 
                                           kwargs.get('nomo'), 
                                           info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_dokumentoj_stokejoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_dokumentoj_stokejoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('priskribo', False)
                            or kwargs.get('posedanto_uuid', False)
                            or kwargs.get('nomo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_dokumentoj_stokejoj = UniversoDokumentoStokejo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_dokumentoj.povas_forigi_universo_dokumentoj_stokejoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_dokumentoj.povas_shanghi_universo_dokumentoj_stokejoj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoDokumentoStokejoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoDokumentoStokejoTipo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный тип места хранения документов Универсо'),'tipo_id')
                                else:
                                    message = '{}: {}'.format(_('Не указан тип места хранения документов Универсо'),'tipo_id')

                            if not message:
                                if 'posedanto_uuid' in kwargs:
                                    try:
                                        posedanto = UniversoDokumento.objects.get(id=kwargs.get('posedanto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoDokumento.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный документ владелец места хранения Универсо'),'posedanto_uuid')
                                else:
                                    message = '{}: {}'.format(_('Не указан документ владелец места хранения Универсо'),'posedanto_uuid')

                            if not message:

                                universo_dokumentoj_stokejoj.forigo = kwargs.get('forigo', universo_dokumentoj_stokejoj.forigo)
                                universo_dokumentoj_stokejoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_dokumentoj_stokejoj.arkivo = kwargs.get('arkivo', universo_dokumentoj_stokejoj.arkivo)
                                universo_dokumentoj_stokejoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_dokumentoj_stokejoj.publikigo = kwargs.get('publikigo', universo_dokumentoj_stokejoj.publikigo)
                                universo_dokumentoj_stokejoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_dokumentoj_stokejoj.tipo = tipo if kwargs.get('tipo_id', False) else universo_dokumentoj_stokejoj.tipo
                                universo_dokumentoj_stokejoj.posedanto = posedanto if kwargs.get('posedanto_uuid', False) else universo_dokumentoj_stokejoj.posedanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_dokumentoj_stokejoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_dokumentoj_stokejoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_dokumentoj_stokejoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoDokumentoStokejo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoDokumentoStokejo(status=status, message=message, universo_dokumentoj_stokejoj=universo_dokumentoj_stokejoj)


# Модель типов владельцев документов в Универсо
class RedaktuUniversoDokumentoPosedantoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_dokumentoj_posedantoj_tipoj = graphene.Field(UniversoDokumentoPosedantoTipoNode,
        description=_('Созданный/изменённый тип владельцев документов в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_dokumentoj_posedantoj_tipoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_dokumentoj.povas_krei_universo_dokumentoj_posedantoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'), 'nomo')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_dokumentoj_posedantoj_tipoj = UniversoDokumentoPosedantoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro=autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_dokumentoj_posedantoj_tipoj.nomo, kwargs.get('nomo'),
                                           info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_dokumentoj_posedantoj_tipoj.priskribo,
                                           kwargs.get('priskribo'),
                                           info.context.LANGUAGE_CODE)

                            universo_dokumentoj_posedantoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_dokumentoj_posedantoj_tipoj = UniversoDokumentoPosedantoTipo.objects.get(
                                uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_dokumentoj.povas_forigi_universo_dokumentoj_posedantoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm(
                                    'universo_dokumentoj.povas_shanghi_universo_dokumentoj_posedantoj_tipoj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_dokumentoj_posedantoj_tipoj.forigo = kwargs.get('forigo',
                                                                                       universo_dokumentoj_posedantoj_tipoj.forigo)
                                universo_dokumentoj_posedantoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo',
                                                                                                              False) else None
                                universo_dokumentoj_posedantoj_tipoj.arkivo = kwargs.get('arkivo',
                                                                                       universo_dokumentoj_posedantoj_tipoj.arkivo)
                                universo_dokumentoj_posedantoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo',
                                                                                                              False) else None
                                universo_dokumentoj_posedantoj_tipoj.publikigo = kwargs.get('publikigo',
                                                                                          universo_dokumentoj_posedantoj_tipoj.publikigo)
                                universo_dokumentoj_posedantoj_tipoj.publikiga_dato = timezone.now() if kwargs.get(
                                    'publikigo', False) else None
                                universo_dokumentoj_posedantoj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_dokumentoj_posedantoj_tipoj.nomo, kwargs.get('nomo'),
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_dokumentoj_posedantoj_tipoj.priskribo, kwargs.get('priskribo'),
                                               info.context.LANGUAGE_CODE)

                                universo_dokumentoj_posedantoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoDokumentoPosedantoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoDokumentoPosedantoTipo(status=status, message=message,
                                                   universo_dokumentoj_posedantoj_tipoj=universo_dokumentoj_posedantoj_tipoj)


# Модель статусов владельца в рамках владения документом Универсо
class RedaktuUniversoDokumentoPosedantoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_dokumentoj_posedantoj_statusoj = graphene.Field(UniversoDokumentoPosedantoStatusoNode,
        description=_('Созданный/изменённый тип статусов владельцев документов в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_dokumentoj_posedantoj_statusoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_dokumentoj.povas_krei_universo_dokumentoj_posedantoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'), 'nomo')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_dokumentoj_posedantoj_statusoj = UniversoDokumentoPosedantoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro=autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_dokumentoj_posedantoj_statusoj.nomo, kwargs.get('nomo'),
                                           info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_dokumentoj_posedantoj_statusoj.priskribo,
                                           kwargs.get('priskribo'),
                                           info.context.LANGUAGE_CODE)

                            universo_dokumentoj_posedantoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_dokumentoj_posedantoj_statusoj = UniversoDokumentoPosedantoStatuso.objects.get(
                                uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm(
                                    'universo_dokumentoj.povas_forigi_universo_dokumentoj_posedantoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm(
                                    'universo_dokumentoj.povas_shanghi_universo_dokumentoj_posedantoj_statusoj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_dokumentoj_posedantoj_statusoj.forigo = kwargs.get('forigo',
                                                                                          universo_dokumentoj_posedantoj_statusoj.forigo)
                                universo_dokumentoj_posedantoj_statusoj.foriga_dato = timezone.now() if kwargs.get(
                                    'forigo', False) else None
                                universo_dokumentoj_posedantoj_statusoj.arkivo = kwargs.get('arkivo',
                                                                                          universo_dokumentoj_posedantoj_statusoj.arkivo)
                                universo_dokumentoj_posedantoj_statusoj.arkiva_dato = timezone.now() if kwargs.get(
                                    'arkivo', False) else None
                                universo_dokumentoj_posedantoj_statusoj.publikigo = kwargs.get('publikigo',
                                                                                             universo_dokumentoj_posedantoj_statusoj.publikigo)
                                universo_dokumentoj_posedantoj_statusoj.publikiga_dato = timezone.now() if kwargs.get(
                                    'publikigo', False) else None
                                universo_dokumentoj_posedantoj_statusoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_dokumentoj_posedantoj_statusoj.nomo, kwargs.get('nomo'),
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_dokumentoj_posedantoj_statusoj.priskribo, kwargs.get('priskribo'),
                                               info.context.LANGUAGE_CODE)

                                universo_dokumentoj_posedantoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoDokumentoPosedantoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoDokumentoPosedantoStatuso(status=status, message=message,
                                                      universo_dokumentoj_posedantoj_statusoj=universo_dokumentoj_posedantoj_statusoj)


# Модель владельцев документов в Универсо
class RedaktuUniversoDokumentoPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_dokumentoj_posedantoj = graphene.Field(UniversoDokumentoPosedantoNode, 
        description=_('Созданный/изменённый владелец документов в Универсо'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        dokumento_uuid = graphene.String(description=_('Объект владения'))
        posedanto_uzanto_siriuso_uzanto_id = graphene.Int(description=_('Пользователь владелец документа Универсо'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец документа Универсо'))
        parto = graphene.Int(description=_('Размер доли владения'))
        tipo_id = graphene.Int(description=_('Тип владельца документа'))
        statuso_id = graphene.Int(description=_('Статус владельца документа'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        posedanto_uzanto = None
        realeco = None
        dokumento = None
        posedanto_organizo = None
        tipo = None
        statuso = None
        universo_dokumentoj_posedantoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_dokumentoj.povas_krei_universo_dokumentoj_posedantoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if (kwargs.get('realeco_id', False)):
                                try:
                                    realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                          arkivo=False, publikigo=True)
                                except UniversoRealeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная реальность в Универсо'), 'realeco_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'), 'realeco_id')

                        if not message:
                            if (kwargs.get('posedanto_uzanto_siriuso_uzanto_id', False)):
                                try:
                                    posedanto_uzanto = UniversoUzanto.objects.get(
                                        siriuso_uzanto__id=kwargs.get('posedanto_uzanto_siriuso_uzanto_id'),
                                        publikigo=True
                                    )
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')

                        if not message:
                            if (kwargs.get('dokumento_uuid', False)):
                                try:
                                    dokumento = UniversoDokumento.objects.get(
                                        uuid=kwargs.get('dokumento_uuid'),
                                        forigo=False,
                                        arkivo=False,
                                        publikigo=True
                                    )
                                except UniversoDokumento.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный документ владения'), 'dokumento_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'), 'dokumento_uuid')

                        if not message:
                            if (kwargs.get('posedanto_organizo_uuid', False)):
                                try:
                                    posedanto_organizo = UniversoOrganizo.objects.get(
                                        uuid=kwargs.get('posedanto_organizo_uuid'),
                                        forigo=False,
                                        arkivo=False,
                                        publikigo=True
                                    )
                                except UniversoOrganizo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная организация владелец документа Универсо'),
                                                               'posedanto_organizo_uuid')

                        if not message:
                            if (kwargs.get('tipo_id', False)):
                                try:
                                    tipo = UniversoDokumentoPosedantoTipo.objects.get(
                                        id=kwargs.get('tipo_id'),
                                        forigo=False,
                                        arkivo=False,
                                        publikigo=True
                                    )
                                except UniversoDokumentoPosedantoTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип владельца документа'), 'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'), 'tipo_id')

                        if not message:
                            if (kwargs.get('statuso_id', False)):
                                try:
                                    statuso = UniversoDokumentoPosedantoStatuso.objects.get(
                                        id=kwargs.get('statuso_id'),
                                        forigo=False,
                                        arkivo=False,
                                        publikigo=True
                                    )
                                except UniversoDokumentoPosedantoStatuso.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный статус владельца документа'), 'statuso_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'), 'statuso_id')

                        if not message:
                            universo_dokumentoj_posedantoj = UniversoDokumentoPosedanto.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto_uzanto=posedanto_uzanto,
                                dokumento=dokumento,
                                realeco=realeco,
                                posedanto_organizo=posedanto_organizo,
                                parto=kwargs.get('parto', 100),
                                tipo=tipo,
                                statuso=statuso,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            universo_dokumentoj_posedantoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('realeco_id', False)
                            or kwargs.get('posedanto_uzanto_siriuso_uzanto_id', False)
                            or kwargs.get('dokumento_uuid', False) or kwargs.get('parto', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('posedanto_organizo_uuid', False)
                            or kwargs.get('tipo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_dokumentoj_posedantoj = UniversoDokumentoPosedanto.objects.get(uuid=kwargs.get('uuid'),
                                                                                                forigo=False)
                            if (not uzanto.has_perm('universo_dokumentoj.povas_forigi_universo_dokumentoj_posedantoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_dokumentoj.povas_shanghi_universo_dokumentoj_posedantoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if (kwargs.get('realeco_id', False)):
                                    try:
                                        realeco = UniversoRealeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                              arkivo=False, publikigo=True)
                                    except UniversoRealeco.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная реальность в Универсо'), 'realeco_id')

                            if not message:
                                if (kwargs.get('posedanto_uzanto_siriuso_uzanto_id', False)):
                                    try:
                                        posedanto_uzanto = UniversoUzanto.objects.get(
                                            siriuso_uzanto__id=kwargs.get('posedanto_uzanto_siriuso_uzanto_id'),
                                            publikigo=True
                                        )
                                    except UniversoUzanto.DoesNotExist:
                                        message = _('Неверный пользователь Universo')

                            if not message:
                                if (kwargs.get('dokumento_uuid', False)):
                                    try:
                                        dokumento = UniversoDokumento.objects.get(
                                            uuid=kwargs.get('dokumento_uuid'),
                                            forigo=False,
                                            arkivo=False,
                                            publikigo=True
                                        )
                                    except UniversoDokumento.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный документ владения'), 'dokumento_uuid')

                            if not message:
                                if (kwargs.get('posedanto_organizo_uuid', False)):
                                    try:
                                        posedanto_organizo = UniversoOrganizo.objects.get(
                                            uuid=kwargs.get('posedanto_organizo_uuid'),
                                            forigo=False,
                                            arkivo=False,
                                            publikigo=True
                                        )
                                    except UniversoOrganizo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная организация владелец документа Универсо'),
                                                                   'posedanto_organizo_uuid')

                            if not message:
                                if (kwargs.get('tipo_id', False)):
                                    try:
                                        tipo = UniversoDokumentoPosedantoTipo.objects.get(
                                            id=kwargs.get('tipo_id'),
                                            forigo=False,
                                            arkivo=False,
                                            publikigo=True
                                        )
                                    except UniversoDokumentoPosedantoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип владельца документа'), 'tipo_id')

                            if not message:
                                if (kwargs.get('statuso_id', False)):
                                    try:
                                        statuso = UniversoDokumentoPosedantoStatuso.objects.get(
                                            id=kwargs.get('statuso_id'),
                                            forigo=False,
                                            arkivo=False,
                                            publikigo=True
                                        )
                                    except UniversoDokumentoPosedantoStatuso.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный статус владельца документа'), 'statuso_id')

                            if not message:
                                universo_dokumentoj_posedantoj.forigo = kwargs.get('forigo', universo_dokumentoj_posedantoj.forigo)
                                universo_dokumentoj_posedantoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else universo_dokumentoj_posedantoj.foriga_dato
                                universo_dokumentoj_posedantoj.arkivo = kwargs.get('arkivo', universo_dokumentoj_posedantoj.arkivo)
                                universo_dokumentoj_posedantoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else universo_dokumentoj_posedantoj.arkiva_dato
                                universo_dokumentoj_posedantoj.publikigo = kwargs.get('publikigo', universo_dokumentoj_posedantoj.publikigo)
                                universo_dokumentoj_posedantoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else universo_dokumentoj_posedantoj.publikiga_dato
                                universo_dokumentoj_posedantoj.realeco = realeco if kwargs.get('realeco_id', False) else universo_dokumentoj_posedantoj.realeco
                                universo_dokumentoj_posedantoj.dokumento = dokumento if kwargs.get('dokumento_uuid', False) else universo_dokumentoj_posedantoj.dokumento
                                universo_dokumentoj_posedantoj.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_siriuso_uzanto_id', False) else universo_dokumentoj_posedantoj.posedanto_uzanto
                                universo_dokumentoj_posedantoj.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) else universo_dokumentoj_posedantoj.posedanto_organizo
                                universo_dokumentoj_posedantoj.parto = kwargs.get('parto', universo_dokumentoj_posedantoj.parto)
                                universo_dokumentoj_posedantoj.tipo = tipo if kwargs.get('tipo_id', False) else universo_dokumentoj_posedantoj.tipo
                                universo_dokumentoj_posedantoj.statuso = statuso if kwargs.get('statuso_id', False) else universo_dokumentoj_posedantoj.statuso

                                universo_dokumentoj_posedantoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoDokumentoPosedanto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoDokumentoPosedanto(status=status, message=message,
                                               universo_dokumentoj_posedantoj=universo_dokumentoj_posedantoj)
    

# Модель типов связей документов между собой
class RedaktuUniversoDokumentoLigiloTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_dokumentoj_ligiloj_tipoj = graphene.Field(UniversoDokumentoLigiloTipoNode, 
        description=_('Созданная/изменённая модель типов связей документов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_dokumentoj_ligiloj_tipoj = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_dokumentoj.povas_krei_universo_dokumentoj_ligiloj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_dokumentoj_ligiloj_tipoj = UniversoDokumentoLigiloTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_dokumentoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_dokumentoj_ligiloj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_dokumentoj_ligiloj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_dokumentoj_ligiloj_tipoj = UniversoDokumentoLigiloTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_dokumentoj.povas_forigi_universo_dokumentoj_ligiloj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_dokumentoj.povas_shanghi_universo_dokumentoj_ligiloj_tipoj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                try:
                                    autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                                except UniversoUzanto.DoesNotExist:
                                    message = _('Неверный пользователь Universo')
                            if not message:

                                universo_dokumentoj_ligiloj_tipoj.forigo = kwargs.get('forigo', universo_dokumentoj_ligiloj_tipoj.forigo)
                                universo_dokumentoj_ligiloj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_dokumentoj_ligiloj_tipoj.arkivo = kwargs.get('arkivo', universo_dokumentoj_ligiloj_tipoj.arkivo)
                                universo_dokumentoj_ligiloj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_dokumentoj_ligiloj_tipoj.publikigo = kwargs.get('publikigo', universo_dokumentoj_ligiloj_tipoj.publikigo)
                                universo_dokumentoj_ligiloj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_dokumentoj_ligiloj_tipoj.autoro = autoro

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_dokumentoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_dokumentoj_ligiloj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_dokumentoj_ligiloj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoDokumentoLigiloTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoDokumentoLigiloTipo(status=status, message=message, universo_dokumentoj_ligiloj_tipoj=universo_dokumentoj_ligiloj_tipoj)


# Модель связей документов между собой
class RedaktuUniversoDokumentoLigilo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_dokumentoj_ligiloj = graphene.Field(UniversoDokumentoLigiloNode, 
        description=_('Созданная/изменённая модель связей документов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        posedanto_uuid = graphene.String(description=_('Ресурс владелец связи'))
        posedanto_stokejo_uuid = graphene.String(description=_('Место хранения владельца связи'))
        ligilo_uuid = graphene.String(description=_('Связываемый документ'))
        tipo_id = graphene.String(description=_('Тип связи документов'))
        konektilo = graphene.Int(description=_('Разъём (слот), который занимает этот документ у родительского документа'))
        priskribo = graphene.String(description=_('Описание'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_dokumentoj_ligiloj = None
        posedanto = None
        posedanto_stokejo = None
        ligilo = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_dokumentoj.povas_krei_universo_dokumentoj_ligiloj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if 'posedanto_uuid' in kwargs:
                                try:
                                    posedanto = UniversoDokumento.objects.get(uuid=kwargs.get('posedanto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoDokumento.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный документ владелец связи Универсо'),'posedanto_uuid')
                            else:
                                message = '{}: {}'.format(_('Не указан документ владелец связи Универсо'),'posedanto_uuid')

                        if not message:
                            if 'posedanto_stokejo_uuid' in kwargs:
                                try:
                                    posedanto_stokejo = UniversoDokumentoStokejo.objects.get(uuid=kwargs.get('posedanto_stokejo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoDokumentoStokejo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверное место хранения владельца связи Универсо'),'posedanto_stokejo_uuid')

                        if not message:
                            if 'ligilo_uuid' in kwargs:
                                try:
                                    ligilo = UniversoDokumento.objects.get(uuid=kwargs.get('ligilo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoDokumento.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный связываемый документ Универсо'),'ligilo_uuid')
                            else:
                                message = '{}: {}'.format(_('Не указан связываемый документ Универсо'),'ligilo_uuid')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoDokumentoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoDokumentoLigiloTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип связи документов Универсо'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип связи документов Универсо'),'tipo_id')

                        if not message:
                            universo_dokumentoj_ligiloj = UniversoDokumentoLigilo.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto=posedanto,
                                posedanto_stokejo=posedanto_stokejo,
                                ligilo=ligilo,
                                tipo=tipo,
                                konektilo=kwargs.get('konektilo', None),
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_dokumentoj_ligiloj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_dokumentoj_ligiloj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto_uuid', False) or kwargs.get('posedanto_stokejo_uuid', False)
                            or kwargs.get('ligilo_uuid', False) or kwargs.get('tipo_id', False)
                            or kwargs.get('konektilo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_dokumentoj_ligiloj = UniversoDokumentoLigilo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_dokumentoj.povas_forigi_universo_dokumentoj_ligiloj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_dokumentoj.povas_shanghi_universo_dokumentoj_ligiloj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                if 'posedanto_uuid' in kwargs:
                                    try:
                                        posedanto = UniversoDokumento.objects.get(uuid=kwargs.get('posedanto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoDokumento.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный документ владелец связи Универсо'),'posedanto_uuid')

                            if not message:
                                if 'posedanto_stokejo_uuid' in kwargs:
                                    try:
                                        posedanto_stokejo = UniversoDokumentoStokejo.objects.get(uuid=kwargs.get('posedanto_stokejo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoDokumentoStokejo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверное место хранения владельца связи Универсо'),'posedanto_stokejo_uuid')

                            if not message:
                                if 'ligilo_uuid' in kwargs:
                                    try:
                                        ligilo = UniversoDokumento.objects.get(uuid=kwargs.get('ligilo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoDokumento.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный связываемый документ Универсо'),'ligilo_uuid')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = UniversoDokumentoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except UniversoDokumentoLigiloTipo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный тип связи документов Универсо'),'tipo_id')

                            if not message:

                                universo_dokumentoj_ligiloj.forigo = kwargs.get('forigo', universo_dokumentoj_ligiloj.forigo)
                                universo_dokumentoj_ligiloj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_dokumentoj_ligiloj.arkivo = kwargs.get('arkivo', universo_dokumentoj_ligiloj.arkivo)
                                universo_dokumentoj_ligiloj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_dokumentoj_ligiloj.publikigo = kwargs.get('publikigo', universo_dokumentoj_ligiloj.publikigo)
                                universo_dokumentoj_ligiloj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_dokumentoj_ligiloj.posedanto = posedanto if kwargs.get('posedanto_uuid', False) else universo_dokumentoj_ligiloj.posedanto
                                universo_dokumentoj_ligiloj.posedanto_stokejo = posedanto_stokejo if kwargs.get('posedanto_stokejo_uuid', False) else universo_dokumentoj_ligiloj.posedanto_stokejo
                                universo_dokumentoj_ligiloj.ligilo = ligilo if kwargs.get('ligilo_uuid', False) else universo_dokumentoj_ligiloj.ligilo
                                universo_dokumentoj_ligiloj.tipo = tipo if kwargs.get('tipo_id', False) else universo_dokumentoj_ligiloj.tipo
                                universo_dokumentoj_ligiloj.konektilo = kwargs.get('konektilo',  universo_dokumentoj_ligiloj.konektilo)

                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_dokumentoj_ligiloj.priskribo, 
                                            kwargs.get('priskribo'), 
                                            info.context.LANGUAGE_CODE)

                                universo_dokumentoj_ligiloj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoDokumentoLigilo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoDokumentoLigilo(status=status, message=message, universo_dokumentoj_ligiloj=universo_dokumentoj_ligiloj)


class UniversoDokumentoMutations(graphene.ObjectType):
    redaktu_universo_dokumento_kategorio = RedaktuUniversoDokumentoKategorio.Field(
        description=_('''Создаёт или редактирует категории документов Универсо''')
    )
    redaktu_universo_dokumento_kategorio_ligiloj_tipo = RedaktuUniversoDokumentoKategorioLigiloTipo.Field(
        description=_('''Создаёт или редактирует модели типов связей категорий документов между собой''')
    )
    redaktu_universo_dokumento_kategorio_ligiloj = RedaktuUniversoDokumentoKategorioLigilo.Field(
        description=_('''Создаёт или редактирует модели связей категорий документов между собой''')
    )
    redaktu_universo_dokumento_tipo = RedaktuUniversoDokumentoTipo.Field(
        description=_('''Создаёт или редактирует типы документов Универсо''')
    )
    redaktu_universo_dokumento_speco = RedaktuUniversoDokumentoSpeco.Field(
        description=_('''Создаёт или редактирует виды документов Универсо''')
    )
    redaktu_universo_dokumento_stokejoj_tipo = RedaktuUniversoDokumentoStokejoTipo.Field(
        description=_('''Создаёт или редактирует типы мест хранения документов Универсо''')
    )
    redaktu_universo_dokumento = RedaktuUniversoDokumento.Field(
        description=_('''Создаёт или редактирует документы Универсо''')
    )
    redaktu_universo_dokumento_stokejoj = RedaktuUniversoDokumentoStokejo.Field(
        description=_('''Создаёт или редактирует логические места хранения документов Универсо''')
    )
    redaktu_universo_dokumento_posedanto_tipo = RedaktuUniversoDokumentoPosedantoTipo.Field(
        description=_('''Создаёт или редактирует модели типов владельцев документов''')
    )
    redaktu_universo_dokumento_posedanto_statuso = RedaktuUniversoDokumentoPosedantoStatuso.Field(
        description=_('''Создаёт или редактирует модели статусов владельцев документов''')
    )
    redaktu_universo_dokumento_posedantoj = RedaktuUniversoDokumentoPosedanto.Field(
        description=_('''Создаёт или редактирует модели владельцев документов''')
    )
    redaktu_universo_dokumento_ligiloj_tipo = RedaktuUniversoDokumentoLigiloTipo.Field(
        description=_('''Создаёт или редактирует модели типов связей документов между собой''')
    )
    redaktu_universo_dokumento_ligiloj = RedaktuUniversoDokumentoLigilo.Field(
        description=_('''Создаёт или редактирует модели связей документов между собой''')
    )
