from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UniversoDokumentojConfig(AppConfig):
    name = 'universo_dokumentoj'
    verbose_name = _('Dokumentoj de Universo')
