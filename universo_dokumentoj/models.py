"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres import fields as pgfields
from django.contrib.auth.models import Permission
from django.db.models import Max, Q

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from universo_bazo.models import UniversoBazaMaks, UniversoBazaRealeco, UniversoRealeco
from universo_uzantoj.models import UniversoUzanto
from universo_organizoj.models import UniversoOrganizo


# Категории документов Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoDokumentoKategorio(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_dokumentoj_kategorioj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_dokumentoj_kategorioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio de dokumentoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj de dokumentoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_dokumentoj_kategorioj', _('Povas vidi kategorioj de dokumentoj de Universo')),
            ('povas_krei_universo_dokumentoj_kategorioj', _('Povas krei kategorioj de dokumentoj de Universo')),
            ('povas_forigi_universo_dokumentoj_kategorioj', _('Povas forigi kategorioj de dokumentoj de Universo')),
            ('povas_shangxi_universo_dokumentoj_kategorioj', _('Povas ŝanĝi kategorioj de dokumentoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoDokumentoKategorio, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_dokumentoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_dokumentoj.povas_vidi_universo_dokumentoj_kategorioj', 
                'universo_dokumentoj.povas_krei_universo_dokumentoj_kategorioj',
                'universo_dokumentoj.povas_forigi_universo_dokumentoj_kategorioj',
                'universo_dokumentoj.povas_shangxi_universo_dokumentoj_kategorioj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_dokumentoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_kategorioj')
                    or user_obj.has_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_kategorioj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_kategorioj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы связей категорий документов между собой, использует абстрактный класс UniversoBazaMaks
class UniversoDokumentoKategorioLigiloTipo(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ligiloj de kategorioj de dokumentoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ligiloj de kategorioj de dokumentoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj',
             _('Povas vidi tipoj de ligiloj de kategorioj de dokumentoj de Universo')),
            ('povas_krei_universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj',
             _('Povas krei tipoj de ligiloj de kategorioj de dokumentoj de Universo')),
            ('povas_forigi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj',
             _('Povas forigi tipoj de ligiloj de kategorioj de dokumentoj de Universo')),
            ('povas_sxangxi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj',
             _('Povas ŝanĝi tipoj de ligiloj de kategorioj de dokumentoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoDokumentoKategorioLigiloTipo, self).save(force_insert=force_insert, force_update=force_update,
                                          using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_dokumentoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_dokumentoj.povas_vidi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj',
                'universo_dokumentoj.povas_krei_universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj',
                'universo_dokumentoj.povas_forigi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj',
                'universo_dokumentoj.povas_sxangxi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_dokumentoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj')
                    or user_obj.has_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь категорий документов между собой, использует абстрактный класс UniversoBazaMaks
class UniversoDokumentoKategorioLigilo(UniversoBazaMaks):

    # категория документов владелец связи
    posedanto = models.ForeignKey(UniversoDokumentoKategorio, verbose_name=_('Kategorio - posedanto'), blank=False,
                                  null=False, default=None, on_delete=models.CASCADE)

    # связываемая категория документов
    ligilo = models.ForeignKey(UniversoDokumentoKategorio, verbose_name=_('Kategorio - ligilo'), blank=False, null=False,
                               default=None, related_name='%(app_label)s_%(class)s_ligilo', on_delete=models.CASCADE)

    # тип связи категорий документов
    tipo = models.ForeignKey(UniversoDokumentoKategorioLigiloTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_dokumentoj_kategorioj_dokumentoj_ligiloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ligilo de kategorioj de dokumentoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ligiloj de kategorioj de dokumentoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj',
             _('Povas vidi ligiloj de kategorioj de dokumentoj de Universo')),
            ('povas_krei_universo_dokumentoj_kategorioj_dokumentoj_ligiloj',
             _('Povas krei ligiloj de kategorioj de dokumentoj de Universo')),
            ('povas_forigi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj',
             _('Povas forigi ligiloj de kategorioj de dokumentoj de Universo')),
            ('povas_sxangxi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj',
             _('Povas ŝanĝi ligiloj de kategorioj de dokumentoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_dokumentoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_dokumentoj.povas_vidi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj',
                'universo_dokumentoj.povas_krei_universo_dokumentoj_kategorioj_dokumentoj_ligiloj',
                'universo_dokumentoj.povas_forigi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj',
                'universo_dokumentoj.povas_sxangxi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_dokumentoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj')
                    or user_obj.has_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_kategorioj_dokumentoj_ligiloj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы документов в Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoDokumentoTipo(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_dokumentoj_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_dokumentoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de dokumentoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de dokumentoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_dokumentoj_tipoj', _('Povas vidi tipoj de dokumentoj de Universo')),
            ('povas_krei_universo_dokumentoj_tipoj', _('Povas krei tipoj de dokumentoj de Universo')),
            ('povas_forigi_universo_dokumentoj_tipoj', _('Povas forigi tipoj de dokumentoj de Universo')),
            ('povas_shangxi_universo_dokumentoj_tipoj', _('Povas ŝanĝi tipoj de dokumentoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoDokumentoTipo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_dokumentoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_dokumentoj.povas_vidi_universo_dokumentoj_tipoj', 
                'universo_dokumentoj.povas_krei_universo_dokumentoj_tipoj',
                'universo_dokumentoj.povas_forigi_universo_dokumentoj_tipoj',
                'universo_dokumentoj.povas_shangxi_universo_dokumentoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_dokumentoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_tipoj')
                    or user_obj.has_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Виды документов в Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoDokumentoSpeco(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_dokumentoj_specoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_dokumentoj_specoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Speco de dokumentoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Specoj de dokumentoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_dokumentoj_specoj', _('Povas vidi specoj de dokumentoj de Universo')),
            ('povas_krei_universo_dokumentoj_specoj', _('Povas krei specoj de dokumentoj de Universo')),
            ('povas_forigi_universo_dokumentoj_specoj', _('Povas forigi specoj de dokumentoj de Universo')),
            ('povas_shangxi_universo_dokumentoj_specoj', _('Povas ŝanĝi specoj de dokumentoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoDokumentoSpeco, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_dokumentoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_dokumentoj.povas_vidi_universo_dokumentoj_specoj',
                'universo_dokumentoj.povas_krei_universo_dokumentoj_specoj',
                'universo_dokumentoj.povas_forigi_universo_dokumentoj_specoj',
                'universo_dokumentoj.povas_shangxi_universo_dokumentoj_specoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_dokumentoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_specoj')
                    or user_obj.has_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_specoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_specoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Справочник документов в Универсо, использует абстрактный класс UniversoBazaMaks
class UniversoDokumento(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # категория документов Универсо
    kategorio = models.ManyToManyField(UniversoDokumentoKategorio, verbose_name=_('Kategorio'),
                                       db_table='universo_dokumentoj_kategorioj_ligiloj')

    # тип документов Универсо
    tipo = models.ForeignKey(UniversoDokumentoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # вид документов Универсо
    speco = models.ForeignKey(UniversoDokumentoSpeco, verbose_name=_('Speco'), blank=True, null=True, default=None,
                             on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(UniversoRealeco, verbose_name=_('Realeco'),
                                     db_table='universo_dokumentoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_dokumentoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Dokumento de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Dokumentoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_dokumentoj', _('Povas vidi dokumentoj de Universo')),
            ('povas_krei_universo_dokumentoj', _('Povas krei dokumentoj de Universo')),
            ('povas_forigi_universo_dokumentoj', _('Povas forigi dokumentoj de Universo')),
            ('povas_shangxi_universo_dokumentoj', _('Povas ŝanĝi dokumentoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoDokumento, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                          update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_dokumentoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_dokumentoj.povas_vidi_universo_dokumentoj', 
                'universo_dokumentoj.povas_krei_universo_dokumentoj',
                'universo_dokumentoj.povas_forigi_universo_dokumentoj',
                'universo_dokumentoj.povas_shangxi_universo_dokumentoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_dokumentoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj')
                    or user_obj.has_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы мест хранения документов, использует абстрактный класс UniversoBazaMaks
class UniversoDokumentoStokejoTipo(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_dokumentoj_stokejoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de stokejoj de dokumentoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de stokejoj de dokumentoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_dokumentoj_stokejoj_tipoj',
             _('Povas vidi tipoj de stokejoj de dokumentoj de Universo')),
            ('povas_krei_universo_dokumentoj_stokejoj_tipoj',
             _('Povas krei tipoj de stokejoj de dokumentoj de Universo')),
            ('povas_forigi_universo_dokumentoj_stokejoj_tipoj',
             _('Povas forigi tipoj de stokejoj de dokumentoj de Universo')),
            ('povas_shangxi_universo_dokumentoj_stokejoj_tipoj',
             _('Povas ŝanĝi tipoj de stokejoj de dokumentoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoDokumentoStokejoTipo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_dokumentoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_dokumentoj.povas_vidi_universo_dokumentoj_stokejoj_tipoj',
                'universo_dokumentoj.povas_krei_universo_dokumentoj_stokejoj_tipoj',
                'universo_dokumentoj.povas_forigi_universo_dokumentoj_stokejoj_tipoj',
                'universo_dokumentoj.povas_shangxi_universo_dokumentoj_stokejoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_dokumentoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_stokejoj_tipoj')
                    or user_obj.has_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_stokejoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_stokejoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Места хранения документов, использует абстрактный класс UniversoBazaMaks
class UniversoDokumentoStokejo(UniversoBazaMaks):

    # документ
    posedanto = models.ForeignKey(UniversoDokumento, verbose_name=_('Dokumento posedanto'), blank=False, null=False,
                                  default=None, on_delete=models.CASCADE)

    # уникальный личный ID места хранения
    id = models.IntegerField(_('ID'), blank=True, null=True, default=None)

    # тип места хранения документов на основе которого создано это место хранения документов
    tipo = models.ForeignKey(UniversoDokumentoStokejoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_dokumentoj_stokejoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Stokejo de dokumentoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Stokejoj de dokumentoj de Universo')
        # установка уникальности личного ID места хранения в рамках объекта
        unique_together = ("id", "posedanto")
        # права
        permissions = (
            ('povas_vidi_universo_dokumentoj_stokejoj', _('Povas vidi stokejoj de dokumentoj de Universo')),
            ('povas_krei_universo_dokumentoj_stokejoj', _('Povas krei stokejoj de dokumentoj de Universo')),
            ('povas_forigi_universo_dokumentoj_stokejoj', _('Povas forigi stokejoj de dokumentoj de Universo')),
            ('povas_sxangxi_universo_dokumentoj_stokejoj', _('Povas ŝanĝi stokejoj de dokumentoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{} ({})'.format(self.uuid, get_enhavo(self.priskribo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(posedanto=self.posedanto).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoDokumentoStokejo, self).save(force_insert=force_insert, force_update=force_update,
                                                 using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_dokumentoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_dokumentoj.povas_vidi_universo_dokumentoj_stokejoj',
                'universo_dokumentoj.povas_krei_universo_dokumentoj_stokejoj',
                'universo_dokumentoj.povas_forigi_universo_dokumentoj_stokejoj',
                'universo_dokumentoj.povas_sxangxi_universo_dokumentoj_stokejoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_dokumentoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_stokejoj')
                    or user_obj.has_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_stokejoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_stokejoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы владельцев документов, использует абстрактный класс UniversoBazaMaks
class UniversoDokumentoPosedantoTipo(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_dokumentoj_posedantoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de posedantoj de dokumentoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de posedantoj de dokumentoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_dokumentoj_posedantoj_tipoj', _('Povas vidi tipoj de posedantoj de dokumentoj de Universo')),
            ('povas_krei_universo_dokumentoj_posedantoj_tipoj', _('Povas krei tipoj de posedantoj de dokumentoj de Universo')),
            ('povas_forigi_universo_dokumentoj_posedantoj_tipoj', _('Povas forigi tipoj de posedantoj de dokumentoj de Universo')),
            ('povas_sxangxi_universo_dokumentoj_posedantoj_tipoj', _('Povas ŝanĝi tipoj de posedantoj de dokumentoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoDokumentoPosedantoTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                   using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_dokumentoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_dokumentoj.povas_vidi_universo_dokumentoj_posedantoj_tipoj',
                'universo_dokumentoj.povas_krei_universo_dokumentoj_posedantoj_tipoj',
                'universo_dokumentoj.povas_forigi_universo_dokumentoj_posedantoj_tipoj',
                'universo_dokumentoj.povas_sxangxi_universo_dokumentoj_posedantoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_dokumentoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_posedantoj_tipoj')
                    or user_obj.has_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_posedantoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_posedantoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Статус владельца документа, использует абстрактный класс UniversoBazaMaks
class UniversoDokumentoPosedantoStatuso(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_dokumentoj_posedantoj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso de posedantoj de dokumentoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj de posedantoj de dokumentoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_dokumentoj_posedantoj_statusoj',
             _('Povas vidi statusoj de posedantoj de dokumentoj de Universo')),
            ('povas_krei_universo_dokumentoj_posedantoj_statusoj',
             _('Povas krei statusoj de posedantoj de dokumentoj de Universo')),
            ('povas_forigi_universo_dokumentoj_posedantoj_statusoj',
             _('Povas forigi statusoj de posedantoj de dokumentoj de Universo')),
            ('povas_sxangxi_universo_dokumentoj_posedantoj_statusoj',
             _('Povas ŝanĝi statusoj de posedantoj de dokumentoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoDokumentoPosedantoStatuso, self).save(force_insert=force_insert, force_update=force_update,
                                                      using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_dokumentoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_dokumentoj.povas_vidi_universo_dokumentoj_posedantoj_statusoj',
                'universo_dokumentoj.povas_krei_universo_dokumentoj_posedantoj_statusoj',
                'universo_dokumentoj.povas_forigi_universo_dokumentoj_posedantoj_statusoj',
                'universo_dokumentoj.povas_sxangxi_universo_dokumentoj_posedantoj_statusoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_dokumentoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_posedantoj_statusoj')
                    or user_obj.has_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_posedantoj_statusoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_posedantoj_statusoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Владельцы документов, использует абстрактный класс UniversoBazaRealeco
class UniversoDokumentoPosedanto(UniversoBazaRealeco):

    # документ владения
    dokumento = models.ForeignKey(UniversoDokumento, verbose_name=_('Dokumento'), blank=False, null=False,
                                  default=None, on_delete=models.CASCADE)

    # пользователь владелец документа Универсо
    posedanto_uzanto = models.ForeignKey(UniversoUzanto, verbose_name=_('Posedanta uzanto'), blank=True, null=True,
                                         default=None, on_delete=models.CASCADE)

    # организация владелец документа Универсо
    posedanto_organizo = models.ForeignKey(UniversoOrganizo, verbose_name=_('Posedanta organizo'), blank=True,
                                           null=True, default=None, on_delete=models.CASCADE)

    # тип владельца документа
    tipo = models.ForeignKey(UniversoDokumentoPosedantoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # статус владельца документа
    statuso = models.ForeignKey(UniversoDokumentoPosedantoStatuso, verbose_name=_('Statuso'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_dokumentoj_posedantoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Posedanto de dokumento de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Posedantoj de dokumentoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_dokumentoj_posedantoj', _('Povas vidi posedantoj de dokumentoj de Universo')),
            ('povas_krei_universo_dokumentoj_posedantoj', _('Povas krei posedantoj de dokumentoj de Universo')),
            ('povas_forigi_universo_dokumentoj_posedantoj', _('Povas forigi posedantoj de dokumentoj de Universo')),
            ('povas_sxangxi_universo_dokumentoj_posedantoj', _('Povas ŝanĝi posedantoj de dokumentoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле id владельца Siriuso и/или nomo организации владельца этой модели
        posedanto = "{}".format(self.dokumento.uuid)
        start = True
        if self.posedanto_uzanto:
            posedanto = "{}: {}".format(posedanto, self.posedanto_uzanto.siriuso_uzanto.id)
            start = False
        if self.posedanto_organizo:
            if start:
                posedanto = "{}: {}".format(posedanto, get_enhavo(self.posedanto_organizo.nomo, empty_values=True)[0])
            else:
                posedanto = "{}; {}".format(posedanto, get_enhavo(self.posedanto_organizo.nomo, empty_values=True)[0])
        return posedanto

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(UniversoDokumentoPosedanto, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_dokumentoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_dokumentoj.povas_vidi_universo_dokumentoj_posedantoj',
                'universo_dokumentoj.povas_krei_universo_dokumentoj_posedantoj',
                'universo_dokumentoj.povas_forigi_universo_dokumentoj_posedantoj',
                'universo_dokumentoj.povas_sxangxi_universo_dokumentoj_posedantoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_dokumentoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_posedantoj')
                    or user_obj.has_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_posedantoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_posedantoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы связей документов между собой, использует абстрактный класс UniversoBazaMaks
class UniversoDokumentoLigiloTipo(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_dokumentoj_ligiloj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ligiloj de dokumentoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ligiloj de dokumentoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_dokumentoj_ligiloj_tipoj',
             _('Povas vidi tipoj de ligiloj de dokumentoj de Universo')),
            ('povas_krei_universo_dokumentoj_ligiloj_tipoj',
             _('Povas krei tipoj de ligiloj de dokumentoj de Universo')),
            ('povas_forigi_universo_dokumentoj_ligiloj_tipoj',
             _('Povas forigi tipoj de ligiloj de dokumentoj de Universo')),
            ('povas_sxangxi_universo_dokumentoj_ligiloj_tipoj',
             _('Povas ŝanĝi tipoj de ligiloj de dokumentoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoDokumentoLigiloTipo, self).save(force_insert=force_insert, force_update=force_update,
                                          using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_dokumentoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_dokumentoj.povas_vidi_universo_dokumentoj_ligiloj_tipoj', 
                'universo_dokumentoj.povas_krei_universo_dokumentoj_ligiloj_tipoj',
                'universo_dokumentoj.povas_forigi_universo_dokumentoj_ligiloj_tipoj',
                'universo_dokumentoj.povas_sxangxi_universo_dokumentoj_ligiloj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_dokumentoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_ligiloj_tipoj')
                    or user_obj.has_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_ligiloj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_ligiloj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь документов между собой, использует абстрактный класс UniversoBazaMaks
class UniversoDokumentoLigilo(UniversoBazaMaks):

    # документ владелец связи
    posedanto = models.ForeignKey(UniversoDokumento, verbose_name=_('Dokumento - posedanto'), blank=False, null=False,
                                          default=None, on_delete=models.CASCADE)

    # место хранения владельца связи
    posedanto_stokejo = models.ForeignKey(UniversoDokumentoStokejo, verbose_name=_('Stokejo de posedanto'), blank=True,
                                          null=True, default=None, on_delete=models.CASCADE)

    # связываемый документ
    ligilo = models.ForeignKey(UniversoDokumento, verbose_name=_('Dokumento - ligilo'), blank=False, null=False,
                                       default=None, related_name='%(app_label)s_%(class)s_ligilo',
                                       on_delete=models.CASCADE)

    # тип связи документов
    tipo = models.ForeignKey(UniversoDokumentoLigiloTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_dokumentoj_ligiloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ligilo de dokumentoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ligiloj de dokumentoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_dokumentoj_ligiloj', _('Povas vidi ligiloj de dokumentoj de Universo')),
            ('povas_krei_universo_dokumentoj_ligiloj', _('Povas krei ligiloj de dokumentoj de Universo')),
            ('povas_forigi_universo_dokumentoj_ligiloj', _('Povas forigi ligiloj de dokumentoj de Universo')),
            ('povas_sxangxi_universo_dokumentoj_ligiloj', _('Povas ŝanĝi ligiloj de dokumentoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_dokumentoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_dokumentoj.povas_vidi_universo_dokumentoj_ligiloj', 
                'universo_dokumentoj.povas_krei_universo_dokumentoj_ligiloj',
                'universo_dokumentoj.povas_forigi_universo_dokumentoj_ligiloj',
                'universo_dokumentoj.povas_sxangxi_universo_dokumentoj_ligiloj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_dokumentoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_ligiloj')
                    or user_obj.has_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_ligiloj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_dokumentoj.povas_vidi_universo_dokumentoj_ligiloj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
