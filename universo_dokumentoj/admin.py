"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from django import forms
from django.utils.translation import ugettext_lazy as _
import json

from siriuso.utils import get_enhavo
from .models import *
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
from universo_bazo.models import UniversoRealeco


class TekstoMixin:
    @staticmethod
    def teksto(obj, field):
        kampo = getattr(obj, field)

        if kampo is not None:
            result = get_enhavo(kampo)

            if result:
                return result[0]
        return ''

    def nomo_teksto(self, obj):
        return self.teksto(obj=obj, field='nomo')

    def priskribo_teksto(self, obj):
        return self.teksto(obj=obj, field='priskribo')

    def sxablono_priskribo_teksto(self, obj):
        return self.teksto(obj=obj, field='sxablono_sistema_priskribo')

    def autoro_id(self, obj):
        return obj.autoro.siriuso_uzanto.id

    def autoro_email(self, obj):
        return obj.autoro.siriuso_uzanto.chefa_retposhto

    def posedantoj_(self, obj):
        posedantoj = UniversoDokumentoPosedanto.objects.filter(dokumento=obj, forigo=False, arkivo=False, publikigo=True)
        idj = ''
        i = True
        for user in posedantoj:
            if user.posedanto_uzanto: 
                if i:
                    idj = "{}".format(user.posedanto_uzanto.siriuso_uzanto.id)
                    i = False
                else:
                    idj = "{}; {}".format(idj, user.posedanto_uzanto.siriuso_uzanto.id)
            if user.posedanto_organizo: 
                if i:
                    idj = "{}".format(self.teksto(obj=user.posedanto_organizo, field='nomo'))
                    i = False
                else:
                    idj = "{}; {}".format(idj, self.teksto(obj=user.posedanto_organizo, field='nomo'))
        return idj


# Форма категорий документов Universo
class UniversoDokumentoKategorioFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoDokumentoKategorio
        fields = [field.name for field in UniversoDokumentoKategorio._meta.fields if field.name not in ('krea_dato', 'uuid')]\
                 + ['realeco',]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Категорий документов Universo
@admin.register(UniversoDokumentoKategorio)
class UniversoDokumentoKategorioAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoDokumentoKategorioFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    filter_horizontal = ('realeco',)
    class Meta:
        model = UniversoDokumentoKategorio


# Форма типов связей категорий документов между собой
class UniversoDokumentoKategorioLigiloTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoDokumentoKategorioLigiloTipo
        fields = [field.name for field in UniversoDokumentoKategorioLigiloTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы связей категорий документов между собой
@admin.register(UniversoDokumentoKategorioLigiloTipo)
class UniversoDokumentoKategorioLigiloTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoDokumentoKategorioLigiloTipoFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoDokumentoKategorioLigiloTipo


# Форма связей категорий документов между собой
class UniversoDokumentoKategorioLigiloFormo(forms.ModelForm):
    
    class Meta:
        model = UniversoDokumentoKategorioLigilo
        fields = [field.name for field in UniversoDokumentoKategorioLigilo._meta.fields if field.name not in ('krea_dato', 'uuid')]


# Связи категорий документов между собой
@admin.register(UniversoDokumentoKategorioLigilo)
class UniversoDokumentoKategorioLigiloAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoDokumentoKategorioLigiloFormo
    list_display = ('uuid','posedanto','tipo','ligilo')
    exclude = ('uuid',)
    class Meta:
        model = UniversoDokumentoKategorioLigilo


# Форма типов документов Universo
class UniversoDokumentoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=UniversoRealeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = UniversoDokumentoTipo
        fields = [field.name for field in UniversoDokumentoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы документов Universo
@admin.register(UniversoDokumentoTipo)
class UniversoDokumentoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoDokumentoTipoFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoDokumentoTipo


# Форма видов документов Universo
class UniversoDokumentoSpecoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=UniversoRealeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = UniversoDokumentoSpeco
        fields = [field.name for field in UniversoDokumentoSpeco._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Виды документов Universo
@admin.register(UniversoDokumentoSpeco)
class UniversoDokumentoSpecoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoDokumentoSpecoFormo
    list_display = ('nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoDokumentoSpeco


# Форма типов мест хранения документов Universo
class UniversoDokumentoStokejoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoDokumentoStokejoTipo
        fields = [field.name for field in UniversoDokumentoStokejoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы мест хранения документов Universo
@admin.register(UniversoDokumentoStokejoTipo)
class UniversoDokumentoStokejoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoDokumentoStokejoTipoFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoDokumentoStokejoTipo


# Форма мест хранения документов Universo
class UniversoDokumentoStokejoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoDokumentoStokejo
        fields = [field.name for field in UniversoDokumentoStokejo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Места хранения документов Universo
@admin.register(UniversoDokumentoStokejo)
class UniversoDokumentoStokejoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoDokumentoStokejoFormo
    list_display = ('nomo_teksto','priskribo_teksto','posedanto','id','tipo')
    exclude = ('uuid',)
    class Meta:
        model = UniversoDokumentoStokejo


# Места хранения документов Универсо
class UniversoDokumentoStokejoInline(admin.TabularInline):
    model = UniversoDokumentoStokejo
    fk_name = 'posedanto'
    # form = UniversoDokumentoStokejoFormo
    extra = 1


# Владелец документа из таблицы связей
class UniversoDokumentoLigiloInline(admin.TabularInline):
    model = UniversoDokumentoLigilo
    fk_name = 'posedanto'
    extra = 1


# Форма документов Universo
class UniversoDokumentoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    kategorio = forms.ModelMultipleChoiceField(
        label=_('Категория документов Универсо'),
        queryset=UniversoDokumentoKategorio.objects.filter(forigo=False, publikigo=True)
    )
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=UniversoRealeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = UniversoDokumento
        fields = [field.name for field in UniversoDokumento._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Документы в Universo
@admin.register(UniversoDokumento)
class UniversoDokumentoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoDokumentoFormo
    list_display = ('id','nomo_teksto','posedantoj_','autoro_id','priskribo_teksto')
    exclude = ('uuid',)
    inlines = ( UniversoDokumentoStokejoInline, UniversoDokumentoLigiloInline)
    class Meta:
        model = UniversoDokumento


# Форма типов связей документов Universo
class UniversoDokumentoLigiloTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoDokumentoLigiloTipo
        fields = [field.name for field in UniversoDokumentoLigiloTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы связей документов Universo
@admin.register(UniversoDokumentoLigiloTipo)
class UniversoDokumentoLigiloTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoDokumentoLigiloTipoFormo
    list_display = ('id','nomo_teksto','autoro_id','autoro_email')
    exclude = ('uuid',)
    class Meta:
        model = UniversoDokumentoLigiloTipo


# Форма типов владельцев документов в Universo
class UniversoDokumentoPosedantoTipoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoDokumentoPosedantoTipo
        fields = [field.name for field in UniversoDokumentoPosedantoTipo._meta.fields if
                  field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы владельцев документов в Universo
@admin.register(UniversoDokumentoPosedantoTipo)
class UniversoDokumentoPosedantoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoDokumentoPosedantoTipoFormo
    list_display = ('id', 'nomo_teksto', 'autoro_id', 'autoro_email')
    exclude = ('uuid',)

    class Meta:
        model = UniversoDokumentoPosedantoTipo


# Форма статусов владельца в рамках владения документом Универсо
class UniversoDokumentoPosedantoStatusoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoDokumentoPosedantoStatuso
        fields = [field.name for field in UniversoDokumentoPosedantoStatuso._meta.fields if
                  field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Статусы владельцев в рамках владения документом Универсо
@admin.register(UniversoDokumentoPosedantoStatuso)
class UniversoDokumentoPosedantoStatusoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoDokumentoPosedantoStatusoFormo
    list_display = ('nomo_teksto', 'autoro_id', 'autoro_email')
    exclude = ('uuid',)

    class Meta:
        model = UniversoDokumentoPosedantoStatuso


# Форма владельцев документов в Universo
class UniversoDokumentoPosedantoFormo(forms.ModelForm):
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(),
        label=_('Priskribo de sistema ŝablono'),
        required=False
    )

    class Meta:
        model = UniversoDokumentoPosedanto
        fields = [field.name for field in UniversoDokumentoPosedanto._meta.fields if
                  field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Владельцы документов в Universo
@admin.register(UniversoDokumentoPosedanto)
class UniversoDokumentoPosedantoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoDokumentoPosedantoFormo
    list_display = ('uuid', 'sxablono_sistema_id', 'sxablono_sistema', 'sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema',)

    class Meta:
        model = UniversoDokumentoPosedanto


# Форма мест хранения документов Universo
class UniversoDokumentoLigiloFormo(forms.ModelForm):
    
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoDokumentoLigilo
        fields = [field.name for field in UniversoDokumentoLigilo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы мест хранения документов Universo
@admin.register(UniversoDokumentoLigilo)
class UniversoDokumentoLigiloAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoDokumentoLigiloFormo
    list_display = ('uuid','posedanto','posedanto_stokejo','ligilo','tipo','priskribo_teksto')
    exclude = ('uuid',)
    class Meta:
        model = UniversoDokumentoLigilo


