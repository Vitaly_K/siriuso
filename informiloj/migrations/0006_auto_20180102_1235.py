# Generated by Django 2.0 on 2018-01-02 12:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('informiloj', '0005_auto_20180102_1233'),
    ]

    operations = [
        migrations.AlterField(
            model_name='informilojsciigotipo',
            name='nomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='informiloj.InformilojSciigoTipoNomo', verbose_name='Nomo'),
        ),
    ]
