"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from .models import *

# Языковые коды для мультиязычного контента (справочник)
class InformilojLingvoAdmin (admin.ModelAdmin):
    list_display = [field.name for field in InformilojLingvo._meta.fields]

    class Meta:
        model = InformilojLingvo

admin.site.register(InformilojLingvo, InformilojLingvoAdmin)


# Справочник стран мира
class InformilojLandoNomoInline(admin.TabularInline):
    model = InformilojLandoNomo
    fk_name = 'posedanto'
    extra = 1


class InformilojLandoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'forigo', 'kodo', 'telefonakodo', 'flago', 'uuid',)
    exclude = ('nomo',)
    inlines = [InformilojLandoNomoInline]

    class Meta:
        model = InformilojLando

admin.site.register(InformilojLando, InformilojLandoAdmin)


# Справочник регионов
class InformilojRegionoNomoInline(admin.TabularInline):
    model = InformilojRegionoNomo
    fk_name = 'posedanto'
    extra = 1


class InformilojRegionoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'lando', 'forigo', 'uuid',)
    exclude = ('nomo',)
    inlines = [InformilojRegionoNomoInline]

    class Meta:
        model = InformilojRegiono

admin.site.register(InformilojRegiono, InformilojRegionoAdmin)


# Справочник типов небесных тел
class InformilojAstroTipoNomoInline(admin.TabularInline):
    model = InformilojAstroTipoNomo
    fk_name = 'posedanto'
    extra = 1


@admin.register(InformilojAstroTipo)
class InformilojAstroTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'speciala', 'forigo', 'uuid',)
    exclude = ('nomo',)
    inlines = [InformilojAstroTipoNomoInline]

    class Meta:
        model = InformilojAstroTipo


# Справочник небесных тел
class InformilojAstroNomoInline(admin.TabularInline):
    model = InformilojAstroNomo
    fk_name = 'posedanto'
    extra = 1


@admin.register(InformilojAstro)
class InformilojAstroTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'tipo', 'forigo', 'uuid',)
    exclude = ('nomo',)
    inlines = [InformilojAstroNomoInline]

    class Meta:
        model = InformilojAstro


# Справочник макрорегионов Технокома
class InformilojMakroregionoTeknokomoNomoInline(admin.TabularInline):
    model = InformilojMakroregionoTeknokomoNomo
    fk_name = 'posedanto'
    extra = 1


@admin.register(InformilojMakroregionoTeknokomo)
class InformilojMakroregionoTeknokomoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'forigo', 'uuid',)
    exclude = ('nomo',)
    inlines = [InformilojMakroregionoTeknokomoNomoInline]

    class Meta:
        model = InformilojMakroregionoTeknokomo


# Справочник мезорегионов Технокома
class InformilojMezoregionoTeknokomoNomoInline(admin.TabularInline):
    model = InformilojMezoregionoTeknokomoNomo
    fk_name = 'posedanto'
    extra = 1


@admin.register(InformilojMezoregionoTeknokomo)
class InformilojMezoregionoTeknokomoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'makroregiono', 'forigo', 'uuid',)
    exclude = ('nomo',)
    inlines = [InformilojMezoregionoTeknokomoNomoInline]

    class Meta:
        model = InformilojMezoregionoTeknokomo


# Справочник регионов Технокома
class InformilojRegionoTeknokomoNomoInline(admin.TabularInline):
    model = InformilojRegionoTeknokomoNomo
    fk_name = 'posedanto'
    extra = 1


@admin.register(InformilojRegionoTeknokomo)
class InformilojRegionoTeknokomoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'makroregiono', 'forigo', 'uuid',)
    exclude = ('nomo',)
    inlines = [InformilojRegionoTeknokomoNomoInline]

    class Meta:
        model = InformilojRegionoTeknokomo


# Типы мест (населённых пунктов)
class InformilojLokoTipoNomoInline(admin.TabularInline):
    model = InformilojLokoTipoNomo
    fk_name = 'posedanto'
    extra = 1


class InformilojLokoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'forigo', 'uuid',)
    exclude = ('nomo',)
    inlines = [InformilojLokoTipoNomoInline]

    class Meta:
        model = InformilojLokoTipo

admin.site.register(InformilojLokoTipo, InformilojLokoTipoAdmin)


# Справочник мест (населённых пунктов)
class InformilojLokoNomoInline(admin.TabularInline):
    model = InformilojLokoNomo
    fk_name = 'posedanto'
    extra = 1


class InformilojLokoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'forigo', 'uuid',)
    exclude = ('nomo',)
    inlines = [InformilojLokoNomoInline]

    class Meta:
        model = InformilojLoko

admin.site.register(InformilojLoko, InformilojLokoAdmin)


# Типы административных округов
class InformilojArondismentoTipoNomoInline(admin.TabularInline):
    model = InformilojArondismentoTipoNomo
    fk_name = 'posedanto'
    extra = 1


class InformilojArondismentoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'forigo', 'uuid',)
    exclude = ('nomo',)
    inlines = [InformilojArondismentoTipoNomoInline]

    class Meta:
        model = InformilojArondismentoTipo

admin.site.register(InformilojArondismentoTipo, InformilojArondismentoTipoAdmin)


# Справочник административных округов
class InformilojArondismentoNomoInline(admin.TabularInline):
    model = InformilojArondismentoNomo
    fk_name = 'posedanto'
    extra = 1


class InformilojArondismentoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'forigo', 'uuid',)
    exclude = ('nomo',)
    inlines = [InformilojArondismentoNomoInline]

    class Meta:
        model = InformilojArondismento

admin.site.register(InformilojArondismento, InformilojArondismentoAdmin)


# Справочник семейных положений
class InformilojFamiliaStatoNomoInline(admin.TabularInline):
    model = InformilojFamiliaStatoNomo
    fk_name = 'posedanto'
    extra = 1


class InformilojFamiliaStatoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'uuid',)
    exclude = ('nomo',)
    inlines = [InformilojFamiliaStatoNomoInline]

    class Meta:
        model = InformilojFamiliaStato

admin.site.register(InformilojFamiliaStato, InformilojFamiliaStatoAdmin)


# Типы рабочих ролей
class InformilojLaboraroloTipoNomoInline(admin.TabularInline):
    model = InformilojLaboraroloTipoNomo
    fk_name = 'posedanto'
    extra = 1


class InformilojLaboraroloTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'forigo', 'uuid',)
    exclude = ('nomo',)
    inlines = [InformilojLaboraroloTipoNomoInline]

    class Meta:
        model = InformilojLaboraroloTipo

admin.site.register(InformilojLaboraroloTipo, InformilojLaboraroloTipoAdmin)


# Справочник рабочих ролей
class InformilojLaboraroloNomoInline(admin.TabularInline):
    model = InformilojLaboraroloNomo
    fk_name = 'posedanto'
    extra = 1


class InformilojLaboraroloAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'speciala', 'forigo', 'uuid',)
    exclude = ('nomo',)
    inlines = [InformilojLaboraroloNomoInline]

    class Meta:
        model = InformilojLaborarolo

admin.site.register(InformilojLaborarolo, InformilojLaboraroloAdmin)


# Типы уведомлений
class InformilojSciigoTipoNomoInline(admin.TabularInline):
    model = InformilojSciigoTipoNomo
    fk_name = 'posedanto'
    extra = 1


class InformilojSciigoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'forigo', 'uuid',)
    exclude = ('nomo',)
    inlines = [InformilojSciigoTipoNomoInline]

    class Meta:
        model = InformilojSciigoTipo

admin.site.register(InformilojSciigoTipo, InformilojSciigoTipoAdmin)
