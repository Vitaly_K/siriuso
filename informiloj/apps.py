from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class InformilojConfig(AppConfig):
    name = 'informiloj'
    verbose_name = _('Informiloj')