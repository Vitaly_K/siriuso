from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UniversoBazoConfig(AppConfig):
    name = 'universo_bazo'
    verbose_name = _('Bazo en Universo')
