import graphene  # сам Graphene
from django.utils.translation import ugettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import UniversoRealeco, UniversoAplikoTipo, UniversoAplikoVersio  # модели приложения


# Модель реальности Universo
class UniversoRealecoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoRealeco
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов приложений Universo
class UniversoAplikoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = UniversoAplikoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель версий приложения Universo
class UniversoAplikoVersioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = UniversoAplikoVersio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'tipo__id': ['exact'],
            'autoro__siriuso_uzanto__id': ['exact'],
            'autoro__siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


class UniversoBazoQuery(graphene.ObjectType):
    universo_realeco = SiriusoFilterConnectionField(
        UniversoRealecoNode,
        description=_('Выводит все доступные модели реальностей Universo')
    )
    universo_apliko_tipo = SiriusoFilterConnectionField(
        UniversoAplikoTipoNode,
        description=_('Выводит все доступные модели типов приложений Universo')
    )
    universo_apliko_versio = SiriusoFilterConnectionField(
        UniversoAplikoVersioNode,
        description=_('Выводит все доступные модели версий приложения Universo')
    )
