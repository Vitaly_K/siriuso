"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
16.05.2019
"""
from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from universo_uzantoj.models import UniversoUzanto
from .schema import *
from ..models import UniversoRealeco


# Модель реальности Universo
class RedaktuUniversoRealeco(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_realeco = graphene.Field(UniversoRealecoNode,
        description=_('Созданная/изменённая реальность в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название реальности'))
        priskribo = graphene.String(description=_('Описание реальности'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_realeco = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_bazo.povas_krei_universo_realeco'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            try:
                                autoro = UniversoUzanto.objects.get(siriuso_uzanto=uzanto, publikigo=True)
                            except UniversoUzanto.DoesNotExist:
                                message = _('Неверный пользователь Universo')

                        if not message:
                            universo_realeco = UniversoRealeco.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = autoro,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(universo_realeco.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(universo_realeco.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_realeco.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_realeco = UniversoRealeco.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_bazo.povas_forigi_universo_realeco')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_bazo.povas_shanghi_universo_realeco'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                universo_realeco.forigo = kwargs.get('forigo', universo_realeco.forigo)
                                universo_realeco.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_realeco.arkivo = kwargs.get('arkivo', universo_realeco.arkivo)
                                universo_realeco.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_realeco.publikigo = kwargs.get('publikigo', universo_realeco.publikigo)
                                universo_realeco.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(universo_realeco.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_realeco.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_realeco.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoRealeco.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoRealeco(status=status, message=message, universo_realeco=universo_realeco)


class UniversoBazoMutations(graphene.ObjectType):
    redaktu_universo_realeco = RedaktuUniversoRealeco.Field(
        description=_('''Создаёт или редактирует реальности Universo''')
    )
