"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from uuid import uuid4
from django.db import models
from django.db.models import Max, Q
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres import fields as pgfields
from django.contrib.auth.models import Permission

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from universo_uzantoj.models import UniversoUzanto


# Реальности Universo
class UniversoRealeco(models.Model):

    # UUID записи
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid4, editable=False)

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # дата и время создания
    krea_dato = models.DateTimeField(_('Krea dato'), auto_now_add=True, auto_now=False, blank=False)

    # помечена на удаление (да или нет)
    forigo = models.BooleanField(_('Forigo'), blank=True, default=False)

    # дата и время пометки на удаление
    foriga_dato = models.DateTimeField(_('Foriga dato'), auto_now_add=False, auto_now=False, blank=True, null=True,
                                       default=None)

    # дата и время автоудаления
    a_foriga_dato = models.DateTimeField(_('Aŭtomata foriga dato'), auto_now_add=False, auto_now=False, blank=True,
                                         null=True, default=None)

    # опубликовано (да или нет)
    publikigo = models.BooleanField(_('Publikigis'), default=False)

    # дата и время публикации
    publikiga_dato = models.DateTimeField(_('Dato de publikigo'), auto_now_add=False, auto_now=False, blank=True,
                                          null=True, default=None)

    # архивная (да или нет)
    arkivo = models.BooleanField(_('Arkiva'), default=False)

    # дата и время помещения в архив
    arkiva_dato = models.DateTimeField(_('Arkiva dato'), auto_now_add=False, auto_now=False, blank=True, null=True,
                                       default=None)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # ссылка на пользователя последней модификации
    lasta_autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Modifita de'), blank=True, null=True,
                                     related_name='%(app_label)s_%(class)s_lasta_autoro',
                                     on_delete=models.SET_NULL)

    # дата последнего изменения
    lasta_dato = models.DateTimeField(_('Dato de lasta modifo'), blank=True, null=True)

    # название многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_realeco'
        # читабельное название модели, в единственном числе
        verbose_name = _('Realeco de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Realecoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_realeco', _('Povas vidi universo realeco')),
            ('povas_krei_universo_realeco', _('Povas krei universo realeco')),
            ('povas_forigi_universo_realeco', _('Povas forigu universo realeco')),
            ('povas_shanghi_universo_realeco', _('Povas ŝanĝi universo realeco')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoRealeco, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_bazo',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_bazo.povas_vidi_universo_realeco', 'universo_bazo.povas_krei_universo_realeco',
                'universo_bazo.povas_forigi_universo_realeco', 'universo_bazo.povas_shanghi_universo_realeco'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_realeco', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_bazo.povas_vidi_universo_realeco')
                    or user_obj.has_perm('universo_bazo.povas_vidi_universo_realeco')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_bazo.povas_vidi_universo_realeco'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Основной абстрактный класс Universo
class UniversoBaza(models.Model):
    
    # UUID записи
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid4, editable=False)

    # дата и время создания
    krea_dato = models.DateTimeField(_('Krea dato'), auto_now_add=True, auto_now=False, blank=False)

    # помечена на удаление (да или нет)
    forigo = models.BooleanField(_('Forigo'), blank=True, default=False)

    # дата и время пометки на удаление
    foriga_dato = models.DateTimeField(_('Foriga dato'), auto_now_add=False, auto_now=False, blank=True, null=True,
                                       default=None)

    # маркер что запись является системным шаблоном
    sxablono_sistema = models.BooleanField(_('Sistema ŝablono'), default=False)

    # ID системного шаблона, есть только у записей шаблонов
    sxablono_sistema_id = models.IntegerField(_('Sistema ŝablono ID'), blank=True, null=True, default=None)

    # описание системного шаблона, применяется только у записей шаблонов
    sxablono_sistema_priskribo = pgfields.JSONField(verbose_name=_('Priskribo de sistema ŝablono'), blank=True,
                                                    null=True, default=default_lingvo, encoder=CallableEncoder)
    
    # дополнительные настройки для модели
    class Meta:
        # указание что это абстрактный класс
        abstract = True


# Средний по наполненности абстрактный класс Universo
class UniversoBazaMeza(UniversoBaza):

    # опубликовано (да или нет)
    publikigo = models.BooleanField(_('Publikigis'), default=False)

    # дата и время публикации
    publikiga_dato = models.DateTimeField(_('Dato de publikigo'), auto_now_add=False, auto_now=False, blank=True,
                                          null=True, default=None)

    # дополнительные настройки для модели
    class Meta:
        # указание что это абстрактный класс
        abstract = True


# Максимальный по наполненности абстрактный класс Universo
class UniversoBazaMaks(UniversoBazaMeza):

    # архивная (да или нет)
    arkivo = models.BooleanField(_('Arkiva'), default=False)

    # дата и время помещения в архив
    arkiva_dato = models.DateTimeField(_('Arkiva dato'), auto_now_add=False, auto_now=False, blank=True, null=True,
                                       default=None)

    # дополнительные настройки для модели
    class Meta:
        # указание что это абстрактный класс
        abstract = True


# Максимальный по наполненности абстрактный класс Universo с указанием параллельного мира
class UniversoBazaRealeco(UniversoBazaMaks):

    # к какому параллельному миру относится сущность
    realeco = models.ForeignKey(UniversoRealeco, verbose_name=_('Realeco de Universo'), blank=True, null=True,
                                on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # указание что это абстрактный класс
        abstract = True


# Типы приложений, использует абстрактный класс UniversoBazaMaks
class UniversoAplikoTipo(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = pgfields.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = pgfields.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_aplikoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de aplikoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de aplikoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_aplikoj_tipoj', _('Povas vidi tipoj de aplikoj de Universo')),
            ('povas_krei_universo_aplikoj_tipoj', _('Povas krei tipoj de aplikoj de Universo')),
            ('povas_forigi_universo_aplikoj_tipoj', _('Povas forigi tipoj de aplikoj de Universo')),
            ('povas_sxangxi_universo_aplikoj_tipoj', _('Povas ŝanĝi tipoj de aplikoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoAplikoTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_bazo',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_bazo.povas_vidi_universo_aplikoj_tipoj',
                'universo_bazo.povas_krei_universo_aplikoj_tipoj',
                'universo_bazo.povas_forigi_universo_aplikoj_tipoj',
                'universo_bazo.povas_sxangxi_universo_aplikoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_bazo', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_bazo.povas_vidi_universo_aplikoj_tipoj')
                    or user_obj.has_perm('universo_bazo.povas_vidi_universo_aplikoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_bazo.povas_vidi_universo_aplikoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Версии приложения, использует абстрактный класс UniversoBazaMaks
class UniversoAplikoVersio(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(UniversoUzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # тип приложжения
    tipo = models.ForeignKey(UniversoAplikoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # актуальная версия (да или нет)
    aktuala = models.BooleanField(_('Aktuala'), default=False)

    # номер версии
    numero_versio = models.IntegerField(_('Numero de versio'), blank=False, default=None, null=True)

    # номер подверсии
    numero_subversio = models.IntegerField(_('Numero de subversio'), blank=False, default=None, null=True)

    # номер исправления
    numero_korektado = models.IntegerField(_('Numero de korektado'), blank=False, default=None, null=True)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_aplikoj_versioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Versio de aplikoj de Universo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Versioj de aplikoj de Universo')
        # права
        permissions = (
            ('povas_vidi_universo_aplikoj_versioj', _('Povas vidi versioj de aplikoj de Universo')),
            ('povas_krei_universo_aplikoj_versioj', _('Povas krei versioj de aplikoj de Universo')),
            ('povas_forigi_universo_aplikoj_versioj', _('Povas forigi versioj de aplikoj de Universo')),
            ('povas_shangxi_universo_aplikoj_versioj', _('Povas ŝanĝi versioj de aplikoj de Universo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}.{}.{}  {}'.format(self.id, self.numero_versio, self.numero_subversio, self.numero_korektado,
            self.aktuala)

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(UniversoAplikoVersio, self).save(force_insert=force_insert, force_update=force_update,
                                               using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('universo_bazo',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_bazo.povas_vidi_universo_aplikoj_versioj',
                'universo_bazo.povas_krei_universo_aplikoj_versioj',
                'universo_bazo.povas_forigi_universo_aplikoj_versioj',
                'universo_bazo.povas_shangxi_universo_aplikoj_versioj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_bazo', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_bazo.povas_vidi_universo_aplikoj_versioj')
                    or user_obj.has_perm('universo_bazo.povas_vidi_universo_aplikoj_versioj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_bazo.povas_vidi_universo_aplikoj_versioj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond

#  TODO В будущем нужно будет ещё сделать модель приложений, чтобы каждая новая копия приложения получала свой
#   уникальный UUID.
