"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from django.contrib import admin
from .models import *


# Типы Тамагочи
@admin.register(TamagocxiTipo)
class TamagocxiTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'publikigo', 'kodo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = TamagocxiTipo


# Степени Тамагочи
@admin.register(TamagocxiGrado)
class TamagocxiGradoAdmin(admin.ModelAdmin):
    list_display = ('grado', 'publikigo', 'uuid')

    class Meta:
        model = TamagocxiGrado


# Виды Тамагочи
@admin.register(TamagocxiSpeco)
class TamagocxiSpecoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'publikigo', 'uuid')
    exclude = ('nomo', 'priskribo')

    class Meta:
        model = TamagocxiSpeco


# Перечень Тамагочи (справочник Тамагочи со всеми характеристиками, ими будет награждение)
@admin.register(TamagocxiPremio)
class TamagocxiPremioAdmin(admin.ModelAdmin):
    list_display = ('speco', 'tipo', 'grado', 'publikigo', 'uuid')

    class Meta:
        model = TamagocxiPremio


# Условия награждения (справочник)
@admin.register(TamagocxiKondicho)
class TamagocxiKondichoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'publikigo', 'uuid')
    exclude = ('nomo', 'priskribo')

    class Meta:
        model = TamagocxiKondicho


# Награждение (перечень всех присвоений Тамагочи пользователям)
@admin.register(TamagocxiPremiado)
class TamagocxiPremiadoAdmin(admin.ModelAdmin):
    list_display = ('premiita', 'premio', 'publikigo', 'kondicho')

    class Meta:
        model = TamagocxiPremiado
