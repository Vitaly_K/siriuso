"""
Internacia Lingvo (Esperanto) https://tehnokom.su/il

Kopirajto © 2017 Teknokom https://tehnokom.su
Ĉi tiu dosiero estas parto de Teknokom Siriuso https://tehnokom.su/siriuso

Tiu ĉi programaro estas libera: vi povas distribui ĝin kaj/aŭ ŝanĝi ĝin laŭ
kondiĉoj de Ĝenerala Publika Licenco GNU publikigita per Libera Programara
Fonduso, aŭ laŭ la versio 3, aŭ (laŭ via bontrovo) laŭ ajna pli posta versio.

La programo estas distribuata esperante, ke ĝi utilos,
Sed SEN AJNAJ GARANTIOJ; sen anticipa garantio de KOMERCA VALORO aŭ TAŬGECO
POR CERTA CELO. Vidu GNU Ĝenerala Publika Licenco por ricevo de plua informo.

Vi devis ricevi la kopion de la publika ĝenerala licenco GNU kun tiu ĉi programo.
Se ne, do vidu https://www.gnu.org/licenses
"""

from celery import shared_task
from django.utils.translation import ugettext_lazy as _
from uzantoj.sciigoj import sendu_sciigon

from .models import TamagocxiPremiado

@shared_task
def sciigi_tamagocxi(premiado_uuid):
    try:
        premiado = (TamagocxiPremiado.objects.get(uuid=premiado_uuid, forigo=False))
        kondicho = premiado.kondicho
    except TamagocxiPremiado.DoesNotExist:
        return False

    uzantoj = {premiado.premiita.id,}
    
    # Получен новый Тамагочи за <условия награждения>
    teksto = 'Получен новый Тамагочи за %(nomo)s'
    parametroj = {'nomo': {'obj': kondicho, 'field': 'nomo'}}
    _('Получен новый Тамагочи за %(nomo)s')


    return sendu_sciigon(teksto, to=uzantoj, objektoj=(premiado,kondicho,), teksto_parametroj=parametroj)

    return 0
