from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class TamagocxiConfig(AppConfig):
    name = 'tamagocxi'
    verbose_name = _('Tamagocxi')

    # Метод срабатывает при регистрации приложения в системе django
    def ready(self):
        # Импортом сообщаем django, что надо проанализировать кодв в модуле tasks
        # При этом происходит регистрация функций заданий celery
        import muroj.tasks